'use strict';
angular.module('secondarySalesApp').controller('cobudgetsViewCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal) {
    $scope.isCreateView = true;
   // $scope.spmbudgetyears = Restangular.all('spmbudgetyears?filter[where][deleteflag]=false').getList().$object;
    Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=false').getList().then(function (Allspmres) {
        $scope.yearArray = [];
       for(var b=0;b<Allspmres.length;b++){
           $scope.yearArray.push(Allspmres[b].year);
       } 
        //console.log('$scope.yearArray',$scope.yearArray)
        Restangular.all('spmbudgetyears?filter={"where":{"id":{"inq":[' + $scope.yearArray + ']}}}').getList().then(function(cobyearRes){
           $scope.spmbudgetyears = cobyearRes;
        });
    });
    
    
    
   /* Restangular.all('cobudgets?filter[where][deleteflag]=false' + '&filter[where][comember]=' + $window.sessionStorage.UserEmployeeId).getList().then(function (part) {
        $scope.cobudgets = part;
        $scope.AllBudget = part;
    });*/
    $scope.YearFunction = function (selyr) {
        $scope.current_YEAR = selyr;
        console.log('selyr', selyr);
        console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId)
        Restangular.one('spmbudgetyears', selyr).get().then(function (yearResa) {
            $scope.Current_Year = yearResa.name;
        });
        Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + selyr).getList().then(function (res) {
            $scope.spmBudget = res[0].totalamount;
            $scope.Balance_amount = $scope.spmBudget - $scope.Total_CO_HumanResource;
            //console.log('$scope.spmBudget', $scope.spmBudget)
          /* for(var b=0; b<res.length;b++){
               $
           } */
            
        });
       // console.log('$scope.cobudgetsArray',$scope.cobudgetsArray)
       //$scope.cobudgetsArray = $scope.cobudgetsArray;
        angular.forEach($scope.cobudgetsArray, function (member, index) {
        member.index = index + 1;
         member.totalamounts = $scope.getTotalAmount(member.id,selyr);
        $scope.TotalData = [];
        $scope.TotalData.push(member);
    });
    }
    $scope.getTotalAmount = function (id,selyr) {
        //console.log('MonthID,YEar', id,selyr);
        //return Restangular.all('cobudgets?filter={"where":{"month":{"inq":[' + id + ']}}}').getList().$object;
       // return Restangular.all('cobudgets?filter={"where":{"month":{"inq":[' + id + ']}}}'+'&filter[where][year]=' + selyr).getList().$object;return Restangular.all('cobudgets?filter={"where":{"month":{"inq":[' + id + ']}}}'+'&filter[where][year]=' + selyr).getList().$object;
        return Restangular.all('cobudgets?filter[where][month]='+id+'&filter[where][year]=' + selyr).getList().$object;
       };
    $scope.cobudgetsArray = [];
    $scope.cobudgetsArray.push({
        mymonth: 'APRIL'
        , id: 4
    }, {
        mymonth: 'MAY'
        , id: 5
    }, {
        mymonth: 'JUNE'
        , id: 6
    }, {
        mymonth: 'JULY'
        , id: 7
    }, {
        mymonth: 'AUGUST'
        , id: 8
    }, {
        mymonth: 'SEPTEMBER'
        , id: 9
    }, {
        mymonth: 'OCTOBER'
        , id: 10
    }, {
        mymonth: 'NOVEMBER'
        , id: 11
    }, {
        mymonth: 'DECEMBER'
        , id: 12
    }, {
        mymonth: 'JANURARY'
        , id: 1
    }, {
        mymonth: 'FEBRUARY'
        , id: 2
    }, {
        mymonth: 'MARCH'
        , id: 3
    });
   
   /* angular.forEach($scope.cobudgetsArray, function (member, index) {
        member.index = index + 1;
         member.totalamounts = $scope.getTotalAmount(member.id);
        $scope.TotalData = [];
        $scope.TotalData.push(member);
    });*/
    /******************************EDIT **************************/
    $scope.BudgetEdit = function (budID) {
        $scope.isCreateView = false;
        console.log('BudgetId', budID);
        Restangular.one('cobudgets?filter[where][month]=' + budID).get().then(function (resp) {
            //console.log('cobudgets', resp[0])
            $scope.currentBudgetId = resp[0].id;
            // console.log('$scope.currentBudgetId',$scope.currentBudgetId);
            Restangular.one('cobudgets', $scope.currentBudgetId).get().then(function (aprl) {
                //console.log('cobudgets', aprl);
                $scope.original = aprl;
                $scope.cobudget = Restangular.copy($scope.original);
                //$scope.Displaymonth = $scope.original.month;
                for (var i = 0; i < $scope.cobudgetsArray.length; i++) {
                    //console.log('scope.cobudgetsArray.length',$scope.cobudgetsArray.length)
                    if ($scope.cobudgetsArray[i].id == $scope.original.month) {
                        // console.log('$scope.cobudgetsArray[i].id',$scope.cobudgetsArray[i].mymonth)
                        $scope.Displaymonth = $scope.cobudgetsArray[i].mymonth;
                    }
                }
                Restangular.one('spmbudgetyears', $scope.original.year).get().then(function (yr) {
                    $scope.Current_Year = yr.name;
                });
                $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
                $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
                $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
            });
        });
        $scope.BudgetEditModal1();
    }
    $scope.Update = function () {
        document.getElementById('swastihumanresources').style.borderColor = "";
        document.getElementById('cohumanresources').style.borderColor = "";
        document.getElementById('swastitravelandcommunication').style.borderColor = "";
        document.getElementById('cotravelandcommunication').style.borderColor = "";
        document.getElementById('swastigenericactivity').style.borderColor = "";
        document.getElementById('cogenericactivity').style.borderColor = "";
        document.getElementById('swastispecificactivity').style.borderColor = "";
        document.getElementById('cospecificactivity').style.borderColor = "";
        document.getElementById('swastimsgtgactivity').style.borderColor = "";
        document.getElementById('comsgtgactivity').style.borderColor = "";
        document.getElementById('swastiadministrativeexpense').style.borderColor = "";
        document.getElementById('coadministrativeexpense').style.borderColor = "";
        document.getElementById('swastimonitoringevaluation').style.borderColor = "";
        document.getElementById('comonitoringevaluation').style.borderColor = "";
        if ($scope.cobudget.swastihumanresources == '' || $scope.cobudget.swastihumanresources == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By Swasti';
            document.getElementById('swastihumanresources').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cohumanresources == '' || $scope.cobudget.cohumanresources == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By CO';
            document.getElementById('cohumanresources').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastitravelandcommunication == '' || $scope.cobudget.swastitravelandcommunication == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By Swasti';
            document.getElementById('swastitravelandcommunication').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cotravelandcommunication == '' || $scope.cobudget.cotravelandcommunication == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By CO';
            document.getElementById('cotravelandcommunication').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastigenericactivity == '' || $scope.cobudget.swastigenericactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By Swasti';
            document.getElementById('swastigenericactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cogenericactivity == '' || $scope.cobudget.cogenericactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By CO';
            document.getElementById('cogenericactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastispecificactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By Swasti';
            document.getElementById('swastispecificactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.cospecificactivity == '' || $scope.cobudget.cospecificactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By CO';
            document.getElementById('cospecificactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastimsgtgactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By Swasti';
            document.getElementById('swastimsgtgactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.comsgtgactivity == '' || $scope.cobudget.comsgtgactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By CO';
            document.getElementById('comsgtgactivity').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastiadministrativeexpense == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By Swasti';
            document.getElementById('swastiadministrativeexpense').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.coadministrativeexpense == '' || $scope.cobudget.coadministrativeexpense == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By CO';
            document.getElementById('coadministrativeexpense').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.swastimonitoringevaluation == '' || $scope.cobudget.swastimonitoringevaluation == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By Swasti';
            document.getElementById('swastimonitoringevaluation').style.borderColor = "#FF0000";
        }
        else if ($scope.cobudget.comonitoringevaluation == '' || $scope.cobudget.comonitoringevaluation == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By CO';
            document.getElementById('comonitoringevaluation').style.borderColor = "#FF0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            //console.log('SAVE', $scope.cobudget);
            // Restangular.one('spmbudgets', $scope.cobudget.year).get().then(function (res) {
            Restangular.all('spmbudgets?filter[where][comember]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][year]=' + $scope.cobudget.year).getList().then(function (res) {
                $scope.spmBudget = res[0].totalamount;
                //console.log('$scope.spmBudget',$scope.spmBudget)
                $scope.cobudget.balancetilldate = $scope.spmBudget - $scope.cobudget.totalamount;
                console.log('$scope.currentBudgetId', $scope.currentBudgetId)
                Restangular.one('cobudgets', $scope.currentBudgetId).customPUT($scope.cobudget).then(function (updateResponse) {
                    console.log('updateResponse', updateResponse);
                    $scope.modalAW.close();
                    location.reload();
                });
            });
        }
    }
    $scope.BudgetEditModal1 = function () {
        $scope.modalAW = $modal.open({
            animation: true
            , templateUrl: 'template/spmbudgetView.html'
            , scope: $scope
            , backdrop: 'static'
            , size: 'lg'
        });
    };
    $scope.Cancel = function () {
            $scope.modalAW.close();
            location.reload();
        }
        /****************ADD BUDGET*******************/
    $scope.cobudget = {
        deleteflag: false
        , comember: $window.sessionStorage.UserEmployeeId
        , lastmodifiedby: $window.sessionStorage.UserEmployeeId
        , lastmodifiedtime: new Date()
        , lastmodifiedbyrole: $window.sessionStorage.roleId
        , facility: $window.sessionStorage.coorgId
    , };
    $scope.callMsg = function () {
        alert('You have already created budget for this month');
    }
    $scope.validatestring = ''
    $scope.AddBudget = function (nam, monthid) {
        //console.log('name', nam)
        $scope.AlreadyCreated = '';
        if ($scope.cobudget.year == '' || $scope.cobudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Year';
        }
        else {
            Restangular.one('cobudgets?filter[where][year]=' + $scope.cobudget.year + '&filter[where][month]=' + monthid + '&filter[where][deleteflag]=false').get().then(function (coRes) {
                //$scope.AlreadyCreated = coRes.month;
                $scope.AlreadyCreated = coRes.length;
                console.log('coRes.length', $scope.AlreadyCreated)
                    // console.log('coRes',coRes)
                if ($scope.AlreadyCreated == 1) {
                    $scope.modalAW.close();
                    console.log('Shi h')
                        //alert('Created')
                        //$scope.callMsg();
                    $scope.toggleValidation2();
                    //$scope.validatestring = $scope.validatestring + 'You have already created budget for this month';
                }
                /* else {
                     console.log('Shi NHI h')
                 }*/
            });
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            $scope.Displaymonth = nam;
            $scope.cobudget.month = monthid;
            $scope.modalAW = $modal.open({
                animation: true
                , templateUrl: 'template/spmbudgetView.html'
                , scope: $scope
                , backdrop: 'static'
                , size: 'lg'
            });
        }
    }
    $scope.modalTitle = 'Thank You';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true
            , templateUrl: 'template/validation.html'
            , scope: $scope
            , backdrop: 'static'
            , size: 'sm'
        });
    };
    $scope.toggleValidation2 = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true
            , templateUrl: 'template/validation2.html'
            , scope: $scope
            , backdrop: 'static'
            , size: 'sm'
        });
    };
    $scope.OkValidation = function () {
        $scope.modalInstance1.close();
    };
    $scope.Save = function () {
            document.getElementById('swastihumanresources').style.borderColor = "";
            document.getElementById('cohumanresources').style.borderColor = "";
            document.getElementById('swastitravelandcommunication').style.borderColor = "";
            document.getElementById('cotravelandcommunication').style.borderColor = "";
            document.getElementById('swastigenericactivity').style.borderColor = "";
            document.getElementById('cogenericactivity').style.borderColor = "";
            document.getElementById('swastispecificactivity').style.borderColor = "";
            document.getElementById('cospecificactivity').style.borderColor = "";
            document.getElementById('swastimsgtgactivity').style.borderColor = "";
            document.getElementById('comsgtgactivity').style.borderColor = "";
            document.getElementById('swastiadministrativeexpense').style.borderColor = "";
            document.getElementById('coadministrativeexpense').style.borderColor = "";
            document.getElementById('swastimonitoringevaluation').style.borderColor = "";
            document.getElementById('comonitoringevaluation').style.borderColor = "";
            if ($scope.cobudget.swastihumanresources == '' || $scope.cobudget.swastihumanresources == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By Swasti';
                document.getElementById('swastihumanresources').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.cohumanresources == '' || $scope.cobudget.cohumanresources == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount By CO';
                document.getElementById('cohumanresources').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.swastitravelandcommunication == '' || $scope.cobudget.swastitravelandcommunication == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By Swasti';
                document.getElementById('swastitravelandcommunication').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.cotravelandcommunication == '' || $scope.cobudget.cotravelandcommunication == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount By CO';
                document.getElementById('cotravelandcommunication').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.swastigenericactivity == '' || $scope.cobudget.swastigenericactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By Swasti';
                document.getElementById('swastigenericactivity').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.cogenericactivity == '' || $scope.cobudget.cogenericactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount By CO';
                document.getElementById('cogenericactivity').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.swastispecificactivity == '' || $scope.cobudget.swastispecificactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By Swasti';
                document.getElementById('swastispecificactivity').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.cospecificactivity == '' || $scope.cobudget.cospecificactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount By CO';
                document.getElementById('cospecificactivity').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.swastimsgtgactivity == '' || $scope.cobudget.swastimsgtgactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By Swasti';
                document.getElementById('swastimsgtgactivity').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.comsgtgactivity == '' || $scope.cobudget.comsgtgactivity == null) {
                $scope.validatestring = $scope.validatestring + 'Enter MSM/TG Activity Amount By CO';
                document.getElementById('comsgtgactivity').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.swastiadministrativeexpense == '' || $scope.cobudget.swastiadministrativeexpense == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By Swasti';
                document.getElementById('swastiadministrativeexpense').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.coadministrativeexpense == '' || $scope.cobudget.coadministrativeexpense == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount By CO';
                document.getElementById('coadministrativeexpense').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.swastimonitoringevaluation == '' || $scope.cobudget.swastimonitoringevaluation == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By Swasti';
                document.getElementById('swastimonitoringevaluation').style.borderColor = "#FF0000";
            }
            else if ($scope.cobudget.comonitoringevaluation == '' || $scope.cobudget.comonitoringevaluation == null) {
                $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount By CO';
                document.getElementById('comonitoringevaluation').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            }
            else {
                console.log('SAVE', $scope.cobudget);
                $scope.cobudget.balancetilldate = $scope.spmBudget - $scope.cobudget.totalamount;
                Restangular.all('cobudgets').post($scope.cobudget).then(function (cobudgetsRes) {
                    console.log('cobudgetsRes', cobudgetsRes);
                    $scope.modalAW.close();
                    $route.reload();
                });
            }
        }
        /************************************GET TOTAL *******************************/
    $scope.HumanresourceTotal = function () {};
    $scope.HumanresourceTotal1 = function () {
        $scope.cobudget.humanresources = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.cohumanresources);
    }
    $scope.TravelandCommunication = function () {}
    $scope.TravelandCommunication1 = function () {
        $scope.cobudget.travelandcommunication = parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.cotravelandcommunication);
    }
    $scope.Genericactivity = function () {}
    $scope.Genericactivity1 = function () {
        $scope.cobudget.genericactivity = parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.cogenericactivity);
    }
    $scope.Specificactivity = function () {}
    $scope.Specificactivity1 = function () {
        $scope.cobudget.specificactivity = parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.cospecificactivity);
    }
    $scope.Msgtgactivity = function () {}
    $scope.Msgtgactivity1 = function () {
        $scope.cobudget.msgtgactivity = parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.comsgtgactivity);
    }
    $scope.Administrativeexpense = function () {}
    $scope.Administrativeexpense1 = function () {
        $scope.cobudget.administrativeexpense = parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.coadministrativeexpense);
    }
    $scope.Monitoringevaluation = function () {}
    $scope.Monitoringevaluation1 = function () {
        $scope.cobudget.monitoringevaluation = parseInt($scope.cobudget.swastimonitoringevaluation) + parseInt($scope.cobudget.comonitoringevaluation);
    }
    $scope.SwastiSum1 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum2 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum3 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum4 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum5 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum6 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.SwastiSum7 = function () {
        $scope.Swasti_Total = parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    }
    $scope.COSum1 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum2 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum3 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum4 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum5 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum6 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum7 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    /***************************************CO SUM *********************************/
    $scope.COSum1 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum2 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum3 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum4 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum5 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum6 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    $scope.COSum7 = function () {
        $scope.CO_Total = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation);
        $scope.Total_Sum = parseInt($scope.cobudget.cohumanresources) + parseInt($scope.cobudget.cotravelandcommunication) + parseInt($scope.cobudget.cogenericactivity) + parseInt($scope.cobudget.cospecificactivity) + parseInt($scope.cobudget.comsgtgactivity) + parseInt($scope.cobudget.coadministrativeexpense) + parseInt($scope.cobudget.comonitoringevaluation) + parseInt($scope.cobudget.swastihumanresources) + parseInt($scope.cobudget.swastitravelandcommunication) + parseInt($scope.cobudget.swastigenericactivity) + parseInt($scope.cobudget.swastispecificactivity) + parseInt($scope.cobudget.swastimsgtgactivity) + parseInt($scope.cobudget.swastiadministrativeexpense) + parseInt($scope.cobudget.swastimonitoringevaluation);
        $scope.cobudget.totalamount = $scope.Total_Sum;
    };
    /************************************************************************************/
});