'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout, $modal) {
        /*********/
        $rootScope.clickFW();
        $scope.ConditionLanguage = {};
        $scope.ConditionList = [];
        $window.sessionStorage.MemberFilter = '';

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
        });

        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.someFocusVariable = true;
        $scope.FocusMe = true;


        $scope.searchbulkupdate = '';
        $window.sessionStorage.MemberId = '';
        $window.sessionStorage.fullName = '';
        $window.sessionStorage.relation = '';
        $window.sessionStorage.conditionId = '';
        $window.sessionStorage.conditionIdnew = '';
        /*****************************************/
        $scope.openMemberPouupOpen = function () {
            $scope.modalInstance2 = $modal.open({
                animation: true,
                templateUrl: 'template/memberpopup.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.disableAddButton = false;
        $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
         if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
            $scope.disableAddButton = true;
            $scope.roles.enableCondition = false;
        }



        $scope.okMemberpopup = function (fullname) {

            $scope.modalInstance2.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okMemberpopup', $rootScope.fullname);
            $location.path("/condition/create");
            $route.reload();
        };

        $scope.cancelMemberPopup = function () {
            $scope.modalInstance2.close();
        };


        /*********************** List call *****************************************/

        $scope.usersdisplay = Restangular.all('users?filter[where][deleteflag]=false').getList().$object;

        $scope.countrydisplay = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;

        $scope.statedisplay = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;

        $scope.districtdsply = Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().$object;

        $scope.citydsply = Restangular.all('cities?filter[where][deleteflag]=false').getList().$object;

        $scope.sitedsply = Restangular.all('distribution-routes?filter[where][deleteflag]=false').getList().$object;

        $scope.roledsply = Restangular.all('roles').getList().$object;

        $scope.conditiondsplyNew = Restangular.all('conditions?filter[where][deleteflag]=false').getList().$object;
        $scope.stepsdsplyNew = Restangular.all('conditionsteps?filter[where][deleteflag]=false').getList().$object;
        $scope.statusesdsplyNew = Restangular.all('conditionstatuses?filter[where][deleteflag]=false').getList().$object;
        $scope.followupsdsply = Restangular.all('conditionfollowups?filter[where][deleteflag]=false').getList().$object;
        $scope.reasonsdsply = Restangular.all('reasonforcancellations?filter[where][deleteflag]=false').getList().$object;





        Restangular.all('conditionsteps?filter[where][language]=' + $window.sessionStorage.language + '&filter[order]=orderNo%20ASC&filter[where][deleteflag]=false').getList().then(function (condStepRes) {
            $scope.conditionsteps = condStepRes;
            //            if ($scope.UserLanguage == 1) {
            //             $scope.stepId = condStepRes[3].id;
            //            } else if ($scope.UserLanguage != 1) {
            //                 $scope.stepId = condStepRes[3].parentId;
            //            }

        });

        Restangular.all('conditionstatuses?filter[where][language]=' + $window.sessionStorage.language + '&filter[order]=orderNo%20ASC&filter[where][deleteflag]=false').getList().then(function (condResst) {
            $scope.conditionstatuses = condResst;
            //            if ($scope.UserLanguage == 1) {
            //                 $scope.statusId = condResst[1].id;
            //            } else if ($scope.UserLanguage != 1) {
            //                 $scope.statusId = condResst[1].parentId;
            //            }

        });




        $scope.filterFields = ['conditionname', 'membername', 'fullname', 'tiId', 'phonenumber'];
        //$scope.searchbulkupdate = ['fullname', 'tiId', 'phonenumber'];
        $scope.searchInput = '';
    
      /*  
      if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {
            $scope.searchInput = '';
            Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {
                    if (member.completedflag == true) {
                        member.backgroundColor = "#00ff00"
                    } else {
                        member.backgroundColor = "#000000"
                    }

                    member.index = index + 1;
                });
            });

        } else if ($window.sessionStorage.roleId + "" === "13") {
            $scope.searchInput = '';
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });
            });

        } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {
            $scope.searchInput = '';
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });
            });


        } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
            $scope.searchInput = '';
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });
            });

        }
   */     
    
    /*export list starts*/

        if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {
            $scope.hideEdit = true;
            $scope.searchInput = '';
            $scope.conditionId = '';
            $scope.filterCall = 'conditionheaders?filter[where][deleteflag]=false&filter[where][facility]=' + +$window.sessionStorage.coorgId;

            $scope.myTrailerCall = 'conditiontrailers?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId;

            Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {
                    if (member.completedflag == true) {
                        member.backgroundColor = "#00ff00"
                    } else {
                        member.backgroundColor = "#000000"
                    }

                    member.index = index + 1;
                });
            });

            Restangular.all($scope.myTrailerCall).getList().then(function (ctns) {

                $scope.contrailerdisplay = ctns;

                //console.log('ctns', ctns);
                $scope.modalInstanceLoad.close();
                Restangular.all($scope.filterCall).getList().then(function (cns) {
                    //  console.log('cns', cns);

                    $scope.conditionheaders = cns;

                    // $scope.ConditionArray = [];

                    // $scope.ConditionList = [];

                    $scope.exportArray = [];


                    angular.forEach($scope.conditionheaders, function (value, index) {

                        //console.log('value', value);
                        $scope.contrailerdisplay = [];


                        var data1 = $scope.partners.filter(function (arr) {
                            return arr.id == value.memberId
                        })[0];

                        if (data1 != undefined) {
                            value.membername = data1.fullname;
                            value.NewtiId = data1.tiId;
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                return arr.id == value.condition
                            })[0];

                            if (data2 != undefined) {
                                value.conditionname = data2.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                return arr.parentId == value.condition
                            })[0];

                            if (data2 != undefined) {
                                value.conditionname = data2.name;
                            }
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                return arr.id == value.step
                            })[0];

                            if (data3 != undefined) {
                                value.conditionstepname = data3.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                return arr.parentId == value.step
                            })[0];

                            if (data3 != undefined) {
                                value.conditionstepname = data3.name;
                            }
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                return arr.id == value.status
                            })[0];

                            if (data4 != undefined) {
                                value.conditionstatusname = data4.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                return arr.parentId == value.status
                            })[0];

                            if (data4 != undefined) {
                                value.conditionstatusname = data4.name;
                            }
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                return arr.id == value.followup
                            })[0];

                            if (data5 != undefined) {
                                value.conditionfollowupname = data5.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                return arr.parentId == value.followup
                            })[0];

                            if (data5 != undefined) {
                                value.conditionfollowupname = data5.name;
                            }
                        }

                        if ($window.sessionStorage.language == 1) {

                            var data6 = $scope.reasonsdsply.filter(function (arr) {
                                return arr.id == value.reasonForCancel
                            })[0];

                            if (data6 != undefined) {
                                value.reasonforcancellationname = data6.name;
                            }

                        } else if ($window.sessionStorage.language != 1) {

                            var data6 = $scope.reasonsdsply.filter(function (arr) {
                                return arr.parentId == value.reasonForCancel
                            })[0];

                            if (data6 != undefined) {
                                value.reasonforcancellationname = data6.name;
                            }
                        }


                        var data13 = $scope.countrydisplay.filter(function (arr) {
                            return arr.id == value.countryId
                        })[0];

                        if (data13 != undefined) {
                            value.countryname = data13.name;
                        }

                        var data14 = $scope.statedisplay.filter(function (arr) {
                            return arr.id == value.state
                        })[0];

                        if (data14 != undefined) {
                            value.statename = data14.name;
                        }

                        var data15 = $scope.districtdsply.filter(function (arr) {
                            return arr.id == value.district
                        })[0];

                        if (data15 != undefined) {
                            value.districtname = data15.name;
                        }

                        var data16 = $scope.citydsply.filter(function (arr) {
                            return arr.id == value.town
                        })[0];

                        if (data16 != undefined) {
                            value.townname = data16.name;
                        }



                        var data18 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == value.lastModifiedBy
                        })[0];

                        if (data18 != undefined) {
                            value.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == value.lastModifiedByRole
                        })[0];

                        if (data19 != undefined) {
                            value.lastModifiedByRolename = data19.name;
                        }
                        var data20 = $scope.roledsply.filter(function (arr) {
                            return arr.id == value.createdByRole
                        })[0];

                        if (data20 != undefined) {
                            value.createdByRolename = data20.name;
                        }

                        var data21 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == value.createdBy
                        })[0];

                        if (data21 != undefined) {
                            value.createdByname = data21.username;
                        }

                        value.index = index + 1;

                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                        value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                        value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                        if (value.syncouttime == null) {
                            value.syncouttime = '';
                        }

                        if (value.conditionfollowupname == null) {
                            value.conditionfollowupname = '';
                        }

                        if (value.followupdateDsply == null) {
                            value.followupdateDsply = '';
                        }

                        if (value.reasonforcancellationname == null) {
                            value.reasonforcancellationname = '';
                        }



                        value.trailerDetails = "";
                        value.trailerDetails1 = "";
                        value.trailerDetails2 = "";
                        value.trailerDetails3 = "";
                        value.trailerDetails4 = "";
                        value.trailerDetails5 = "";
                        $scope.myArray = [];

                        angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                            if (mydata.status > 0) {

                                mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                                for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                    if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                        for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                            if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                                value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                                $scope.myArray.push(value.trailerDetails);

                                                value.trailerDetails1 = $scope.myArray[0];
                                                value.trailerDetails2 = $scope.myArray[1];
                                                value.trailerDetails3 = $scope.myArray[2];
                                                value.trailerDetails4 = $scope.myArray[3];
                                                value.trailerDetails5 = $scope.myArray[4];

                                                //console.log(value.trailerDetails1);
                                            }
                                        }
                                    }
                                }
                            }

                        });

                        // $scope.ConditionList.push(value);

                        $scope.exportArray.push(value);
                    });
                });
            });


        } else if ($window.sessionStorage.roleId + "" === "13") {

            $scope.hideEdit = false;
            $scope.searchInput = '';
            $scope.conditionId = '';
            // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });

                Restangular.all('conditiontrailers?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (ctns) {

                    $scope.contrailerdisplay = ctns;

                    // console.log('ctns', ctns);
                    $scope.modalInstanceLoad.close();
                    Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (cns) {
                        //console.log('cns', cns);

                        $scope.conditionheaders = cns;

                        $scope.ConditionArray = [];

                        $scope.ConditionList = [];

                        $scope.exportArray = [];


                        angular.forEach($scope.conditionheaders, function (value, index) {

                            //console.log('value', value);
                            $scope.contrailerdisplay = [];


                            var data1 = $scope.partners.filter(function (arr) {
                                return arr.id == value.memberId
                            })[0];

                            if (data1 != undefined) {
                                value.membername = data1.fullname;
                                value.NewtiId = data1.tiId;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.parentId == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.id == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.parentId == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.id == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.parentId == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }
                            }


                            var data13 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == value.countryId
                            })[0];

                            if (data13 != undefined) {
                                value.countryname = data13.name;
                            }

                            var data14 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == value.state
                            })[0];

                            if (data14 != undefined) {
                                value.statename = data14.name;
                            }

                            var data15 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == value.district
                            })[0];

                            if (data15 != undefined) {
                                value.districtname = data15.name;
                            }

                            var data16 = $scope.citydsply.filter(function (arr) {
                                return arr.id == value.town
                            })[0];

                            if (data16 != undefined) {
                                value.townname = data16.name;
                            }



                            var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.lastModifiedBy
                            })[0];

                            if (data18 != undefined) {
                                value.lastModifiedByname = data18.username;
                            }

                            var data19 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.lastModifiedByRole
                            })[0];

                            if (data19 != undefined) {
                                value.lastModifiedByRolename = data19.name;
                            }
                            var data20 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.createdByRole
                            })[0];

                            if (data20 != undefined) {
                                value.createdByRolename = data20.name;
                            }

                            var data21 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.createdBy
                            })[0];

                            if (data21 != undefined) {
                                value.createdByname = data21.username;
                            }

                            value.index = index + 1;

                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                            value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                            value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                            if (value.syncouttime == null) {
                                value.syncouttime = '';
                            }

                            if (value.conditionfollowupname == null) {
                                value.conditionfollowupname = '';
                            }

                            if (value.followupdateDsply == null) {
                                value.followupdateDsply = '';
                            }

                            if (value.reasonforcancellationname == null) {
                                value.reasonforcancellationname = '';
                            }



                            value.trailerDetails = "";
                            value.trailerDetails1 = "";
                            value.trailerDetails2 = "";
                            value.trailerDetails3 = "";
                            value.trailerDetails4 = "";
                            value.trailerDetails5 = "";
                            $scope.myArray = [];

                            angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                                if (mydata.status > 0) {

                                    mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                                    for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                        if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                            for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                                if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                                    value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                                    $scope.myArray.push(value.trailerDetails);

                                                    value.trailerDetails1 = $scope.myArray[0];
                                                    value.trailerDetails2 = $scope.myArray[1];
                                                    value.trailerDetails3 = $scope.myArray[2];
                                                    value.trailerDetails4 = $scope.myArray[3];
                                                    value.trailerDetails5 = $scope.myArray[4];

                                                    //console.log(value.trailerDetails1);
                                                }
                                            }
                                        }
                                    }
                                }

                            });

                            //$scope.ConditionList.push(value);

                            $scope.exportArray.push(value);
                        });
                    });
                });


            });


        } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {

            $scope.hideEdit = false;
            $scope.searchInput = '';
            $scope.conditionId = '';
            // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });

                Restangular.all('conditiontrailers?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (ctns) {

                    $scope.contrailerdisplay = ctns;

                    // console.log('ctns', ctns);
                    $scope.modalInstanceLoad.close();
                    Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (cns) {
                        //console.log('cns', cns);

                        $scope.conditionheaders = cns;

                        $scope.ConditionArray = [];

                        $scope.ConditionList = [];

                        $scope.exportArray = [];


                        angular.forEach($scope.conditionheaders, function (value, index) {

                            //console.log('value', value);
                            $scope.contrailerdisplay = [];


                            var data1 = $scope.partners.filter(function (arr) {
                                return arr.id == value.memberId
                            })[0];

                            if (data1 != undefined) {
                                value.membername = data1.fullname;
                                value.NewtiId = data1.tiId;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.parentId == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.id == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.parentId == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.id == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.parentId == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }
                            }


                            var data13 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == value.countryId
                            })[0];

                            if (data13 != undefined) {
                                value.countryname = data13.name;
                            }

                            var data14 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == value.state
                            })[0];

                            if (data14 != undefined) {
                                value.statename = data14.name;
                            }

                            var data15 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == value.district
                            })[0];

                            if (data15 != undefined) {
                                value.districtname = data15.name;
                            }

                            var data16 = $scope.citydsply.filter(function (arr) {
                                return arr.id == value.town
                            })[0];

                            if (data16 != undefined) {
                                value.townname = data16.name;
                            }



                            var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.lastModifiedBy
                            })[0];

                            if (data18 != undefined) {
                                value.lastModifiedByname = data18.username;
                            }

                            var data19 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.lastModifiedByRole
                            })[0];

                            if (data19 != undefined) {
                                value.lastModifiedByRolename = data19.name;
                            }
                            var data20 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.createdByRole
                            })[0];

                            if (data20 != undefined) {
                                value.createdByRolename = data20.name;
                            }

                            var data21 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.createdBy
                            })[0];

                            if (data21 != undefined) {
                                value.createdByname = data21.username;
                            }

                            value.index = index + 1;

                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                            value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                            value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                            if (value.syncouttime == null) {
                                value.syncouttime = '';
                            }

                            if (value.conditionfollowupname == null) {
                                value.conditionfollowupname = '';
                            }

                            if (value.followupdateDsply == null) {
                                value.followupdateDsply = '';
                            }

                            if (value.reasonforcancellationname == null) {
                                value.reasonforcancellationname = '';
                            }



                            value.trailerDetails = "";
                            value.trailerDetails1 = "";
                            value.trailerDetails2 = "";
                            value.trailerDetails3 = "";
                            value.trailerDetails4 = "";
                            value.trailerDetails5 = "";
                            $scope.myArray = [];

                            angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                                if (mydata.status > 0) {

                                    mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                                    for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                        if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                            for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                                if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                                    value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                                    $scope.myArray.push(value.trailerDetails);

                                                    value.trailerDetails1 = $scope.myArray[0];
                                                    value.trailerDetails2 = $scope.myArray[1];
                                                    value.trailerDetails3 = $scope.myArray[2];
                                                    value.trailerDetails4 = $scope.myArray[3];
                                                    value.trailerDetails5 = $scope.myArray[4];

                                                    //console.log(value.trailerDetails1);
                                                }
                                            }
                                        }
                                    }
                                }

                            });

                            //$scope.ConditionList.push(value);

                            $scope.exportArray.push(value);
                        });
                    });
                });


            });


        } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {

            $scope.hideEdit = false;
            $scope.searchInput = '';
            $scope.conditionId = '';
            // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });

                Restangular.all('conditiontrailers?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}}]}}').getList().then(function (ctns) {

                    $scope.contrailerdisplay = ctns;

                    // console.log('ctns', ctns);
                    $scope.modalInstanceLoad.close();
                    Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}}]}}').getList().then(function (cns) {
                        //console.log('cns', cns);

                        $scope.conditionheaders = cns;

                        $scope.ConditionArray = [];

                        $scope.ConditionList = [];

                        $scope.exportArray = [];


                        angular.forEach($scope.conditionheaders, function (value, index) {

                            //console.log('value', value);
                            $scope.contrailerdisplay = [];


                            var data1 = $scope.partners.filter(function (arr) {
                                return arr.id == value.memberId
                            })[0];

                            if (data1 != undefined) {
                                value.membername = data1.fullname;
                                value.NewtiId = data1.tiId;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.parentId == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.id == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.parentId == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.id == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.parentId == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }
                            }


                            var data13 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == value.countryId
                            })[0];

                            if (data13 != undefined) {
                                value.countryname = data13.name;
                            }

                            var data14 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == value.state
                            })[0];

                            if (data14 != undefined) {
                                value.statename = data14.name;
                            }

                            var data15 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == value.district
                            })[0];

                            if (data15 != undefined) {
                                value.districtname = data15.name;
                            }

                            var data16 = $scope.citydsply.filter(function (arr) {
                                return arr.id == value.town
                            })[0];

                            if (data16 != undefined) {
                                value.townname = data16.name;
                            }



                            var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.lastModifiedBy
                            })[0];

                            if (data18 != undefined) {
                                value.lastModifiedByname = data18.username;
                            }

                            var data19 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.lastModifiedByRole
                            })[0];

                            if (data19 != undefined) {
                                value.lastModifiedByRolename = data19.name;
                            }
                            var data20 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.createdByRole
                            })[0];

                            if (data20 != undefined) {
                                value.createdByRolename = data20.name;
                            }

                            var data21 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.createdBy
                            })[0];

                            if (data21 != undefined) {
                                value.createdByname = data21.username;
                            }

                            value.index = index + 1;

                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                            value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                            value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                            if (value.syncouttime == null) {
                                value.syncouttime = '';
                            }

                            if (value.conditionfollowupname == null) {
                                value.conditionfollowupname = '';
                            }

                            if (value.followupdateDsply == null) {
                                value.followupdateDsply = '';
                            }

                            if (value.reasonforcancellationname == null) {
                                value.reasonforcancellationname = '';
                            }



                            value.trailerDetails = "";
                            value.trailerDetails1 = "";
                            value.trailerDetails2 = "";
                            value.trailerDetails3 = "";
                            value.trailerDetails4 = "";
                            value.trailerDetails5 = "";
                            $scope.myArray = [];

                            angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                                if (mydata.status > 0) {

                                    mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                                    for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                        if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                            for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                                if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                                    value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                                    $scope.myArray.push(value.trailerDetails);

                                                    value.trailerDetails1 = $scope.myArray[0];
                                                    value.trailerDetails2 = $scope.myArray[1];
                                                    value.trailerDetails3 = $scope.myArray[2];
                                                    value.trailerDetails4 = $scope.myArray[3];
                                                    value.trailerDetails5 = $scope.myArray[4];

                                                    //console.log(value.trailerDetails1);
                                                }
                                            }
                                        }
                                    }
                                }

                            });

                            //$scope.ConditionList.push(value);

                            $scope.exportArray.push(value);
                        });
                    });
                });


            });


        } else if ($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 9) {

            $scope.hideEdit = false;
            $scope.searchInput = '';
            $scope.conditionId = '';

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                //console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}}]}}').getList().then(function (part2) {

                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });
                
                  Restangular.all('conditiontrailers?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}}]}}').getList().then(function (ctns) {

                    $scope.contrailerdisplay = ctns;

                    // console.log('ctns', ctns);
                    $scope.modalInstanceLoad.close();
                    Restangular.all('conditionheaders?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}}]}}').getList().then(function (cns) {
                        //console.log('cns', cns);

                        $scope.conditionheaders = cns;

                        $scope.ConditionArray = [];

                        $scope.ConditionList = [];

                        $scope.exportArray = [];


                        angular.forEach($scope.conditionheaders, function (value, index) {

                            //console.log('value', value);
                            $scope.contrailerdisplay = [];


                            var data1 = $scope.partners.filter(function (arr) {
                                return arr.id == value.memberId
                            })[0];

                            if (data1 != undefined) {
                                value.membername = data1.fullname;
                                value.NewtiId = data1.tiId;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.parentId == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.id == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.parentId == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.id == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.parentId == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }
                            }


                            var data13 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == value.countryId
                            })[0];

                            if (data13 != undefined) {
                                value.countryname = data13.name;
                            }

                            var data14 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == value.state
                            })[0];

                            if (data14 != undefined) {
                                value.statename = data14.name;
                            }

                            var data15 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == value.district
                            })[0];

                            if (data15 != undefined) {
                                value.districtname = data15.name;
                            }

                            var data16 = $scope.citydsply.filter(function (arr) {
                                return arr.id == value.town
                            })[0];

                            if (data16 != undefined) {
                                value.townname = data16.name;
                            }



                            var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.lastModifiedBy
                            })[0];

                            if (data18 != undefined) {
                                value.lastModifiedByname = data18.username;
                            }

                            var data19 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.lastModifiedByRole
                            })[0];

                            if (data19 != undefined) {
                                value.lastModifiedByRolename = data19.name;
                            }
                            var data20 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.createdByRole
                            })[0];

                            if (data20 != undefined) {
                                value.createdByRolename = data20.name;
                            }

                            var data21 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.createdBy
                            })[0];

                            if (data21 != undefined) {
                                value.createdByname = data21.username;
                            }

                            value.index = index + 1;

                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                            value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                            value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                            if (value.syncouttime == null) {
                                value.syncouttime = '';
                            }

                            if (value.conditionfollowupname == null) {
                                value.conditionfollowupname = '';
                            }

                            if (value.followupdateDsply == null) {
                                value.followupdateDsply = '';
                            }

                            if (value.reasonforcancellationname == null) {
                                value.reasonforcancellationname = '';
                            }



                            value.trailerDetails = "";
                            value.trailerDetails1 = "";
                            value.trailerDetails2 = "";
                            value.trailerDetails3 = "";
                            value.trailerDetails4 = "";
                            value.trailerDetails5 = "";
                            $scope.myArray = [];

                            angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                                if (mydata.status > 0) {

                                    mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                                    for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                        if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                            for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                                if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                                    value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                                    $scope.myArray.push(value.trailerDetails);

                                                    value.trailerDetails1 = $scope.myArray[0];
                                                    value.trailerDetails2 = $scope.myArray[1];
                                                    value.trailerDetails3 = $scope.myArray[2];
                                                    value.trailerDetails4 = $scope.myArray[3];
                                                    value.trailerDetails5 = $scope.myArray[4];

                                                    //console.log(value.trailerDetails1);
                                                }
                                            }
                                        }
                                    }
                                }

                            });
                            $scope.exportArray.push(value);
                        });
                    });
                });
            });
        } else if ($window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17 || $window.sessionStorage.roleId == 10) {

            $scope.hideEdit = false;
            $scope.searchInput = '';
            $scope.conditionId = '';

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                //console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}}]}}').getList().then(function (part2) {

                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });
                });
                
                 Restangular.all('conditiontrailers?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}}]}}').getList().then(function (ctns) {

                    $scope.contrailerdisplay = ctns;

                    // console.log('ctns', ctns);
                    $scope.modalInstanceLoad.close();
                    Restangular.all('conditionheaders?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}}]}}').getList().then(function (cns) {
                        //console.log('cns', cns);

                        $scope.conditionheaders = cns;

                        $scope.ConditionArray = [];

                        $scope.ConditionList = [];

                        $scope.exportArray = [];


                        angular.forEach($scope.conditionheaders, function (value, index) {

                            //console.log('value', value);
                            $scope.contrailerdisplay = [];


                            var data1 = $scope.partners.filter(function (arr) {
                                return arr.id == value.memberId
                            })[0];

                            if (data1 != undefined) {
                                value.membername = data1.fullname;
                                value.NewtiId = data1.tiId;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.parentId == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.id == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.parentId == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.id == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.parentId == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }
                            }


                            var data13 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == value.countryId
                            })[0];

                            if (data13 != undefined) {
                                value.countryname = data13.name;
                            }

                            var data14 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == value.state
                            })[0];

                            if (data14 != undefined) {
                                value.statename = data14.name;
                            }

                            var data15 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == value.district
                            })[0];

                            if (data15 != undefined) {
                                value.districtname = data15.name;
                            }

                            var data16 = $scope.citydsply.filter(function (arr) {
                                return arr.id == value.town
                            })[0];

                            if (data16 != undefined) {
                                value.townname = data16.name;
                            }



                            var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.lastModifiedBy
                            })[0];

                            if (data18 != undefined) {
                                value.lastModifiedByname = data18.username;
                            }

                            var data19 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.lastModifiedByRole
                            })[0];

                            if (data19 != undefined) {
                                value.lastModifiedByRolename = data19.name;
                            }
                            var data20 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.createdByRole
                            })[0];

                            if (data20 != undefined) {
                                value.createdByRolename = data20.name;
                            }

                            var data21 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.createdBy
                            })[0];

                            if (data21 != undefined) {
                                value.createdByname = data21.username;
                            }

                            value.index = index + 1;

                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                            value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                            value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                            if (value.syncouttime == null) {
                                value.syncouttime = '';
                            }

                            if (value.conditionfollowupname == null) {
                                value.conditionfollowupname = '';
                            }

                            if (value.followupdateDsply == null) {
                                value.followupdateDsply = '';
                            }

                            if (value.reasonforcancellationname == null) {
                                value.reasonforcancellationname = '';
                            }



                            value.trailerDetails = "";
                            value.trailerDetails1 = "";
                            value.trailerDetails2 = "";
                            value.trailerDetails3 = "";
                            value.trailerDetails4 = "";
                            value.trailerDetails5 = "";
                            $scope.myArray = [];

                            angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                                if (mydata.status > 0) {

                                    mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                                    for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                        if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                            for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                                if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                                    value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                                    $scope.myArray.push(value.trailerDetails);

                                                    value.trailerDetails1 = $scope.myArray[0];
                                                    value.trailerDetails2 = $scope.myArray[1];
                                                    value.trailerDetails3 = $scope.myArray[2];
                                                    value.trailerDetails4 = $scope.myArray[3];
                                                    value.trailerDetails5 = $scope.myArray[4];

                                                    //console.log(value.trailerDetails1);
                                                }
                                            }
                                        }
                                    }
                                }

                            });
                            $scope.exportArray.push(value);
                        });
                    });
                });
                
            });
        } else if ($window.sessionStorage.roleId == 8) {

            $scope.hideEdit = false;
            $scope.searchInput = '';
            $scope.conditionId = '';

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                //console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}}]}}').getList().then(function (part2) {

                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {

                        if (member.completedflag == true) {
                            member.backgroundColor = "#00ff00"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                        member.index = index + 1;

                    });

                });
                
                  Restangular.all('conditiontrailers?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}}]}}').getList().then(function (ctns) {

                    $scope.contrailerdisplay = ctns;

                    // console.log('ctns', ctns);
                    $scope.modalInstanceLoad.close();
                    Restangular.all('conditionheaders?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}}]}}').getList().then(function (cns) {
                        //console.log('cns', cns);

                        $scope.conditionheaders = cns;

                        $scope.ConditionArray = [];

                        $scope.ConditionList = [];

                        $scope.exportArray = [];


                        angular.forEach($scope.conditionheaders, function (value, index) {

                            //console.log('value', value);
                            $scope.contrailerdisplay = [];


                            var data1 = $scope.partners.filter(function (arr) {
                                return arr.id == value.memberId
                            })[0];

                            if (data1 != undefined) {
                                value.membername = data1.fullname;
                                value.NewtiId = data1.tiId;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.parentId == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.id == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.parentId == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.id == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.parentId == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }
                            }


                            var data13 = $scope.countrydisplay.filter(function (arr) {
                                return arr.id == value.countryId
                            })[0];

                            if (data13 != undefined) {
                                value.countryname = data13.name;
                            }

                            var data14 = $scope.statedisplay.filter(function (arr) {
                                return arr.id == value.state
                            })[0];

                            if (data14 != undefined) {
                                value.statename = data14.name;
                            }

                            var data15 = $scope.districtdsply.filter(function (arr) {
                                return arr.id == value.district
                            })[0];

                            if (data15 != undefined) {
                                value.districtname = data15.name;
                            }

                            var data16 = $scope.citydsply.filter(function (arr) {
                                return arr.id == value.town
                            })[0];

                            if (data16 != undefined) {
                                value.townname = data16.name;
                            }



                            var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.lastModifiedBy
                            })[0];

                            if (data18 != undefined) {
                                value.lastModifiedByname = data18.username;
                            }

                            var data19 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.lastModifiedByRole
                            })[0];

                            if (data19 != undefined) {
                                value.lastModifiedByRolename = data19.name;
                            }
                            var data20 = $scope.roledsply.filter(function (arr) {
                                return arr.id == value.createdByRole
                            })[0];

                            if (data20 != undefined) {
                                value.createdByRolename = data20.name;
                            }

                            var data21 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == value.createdBy
                            })[0];

                            if (data21 != undefined) {
                                value.createdByname = data21.username;
                            }

                            value.index = index + 1;

                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                            value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                            value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                            if (value.syncouttime == null) {
                                value.syncouttime = '';
                            }

                            if (value.conditionfollowupname == null) {
                                value.conditionfollowupname = '';
                            }

                            if (value.followupdateDsply == null) {
                                value.followupdateDsply = '';
                            }

                            if (value.reasonforcancellationname == null) {
                                value.reasonforcancellationname = '';
                            }



                            value.trailerDetails = "";
                            value.trailerDetails1 = "";
                            value.trailerDetails2 = "";
                            value.trailerDetails3 = "";
                            value.trailerDetails4 = "";
                            value.trailerDetails5 = "";
                            $scope.myArray = [];

                            angular.forEach($scope.contrailerdisplay, function (mydata, index) {
                                if (mydata.status > 0) {

                                    mydata.createdDate = $filter('date')(mydata.createdDate, 'dd/MM/yyyy');

                                    for (var eee = 0; eee < $scope.statusesdsplyNew.length; eee++) {

                                        if ($scope.statusesdsplyNew[eee].id + "" === mydata.status + "") {

                                            for (var ddd = 0; ddd < $scope.stepsdsplyNew.length; ddd++) {

                                                if ($scope.stepsdsplyNew[ddd].id + "" === mydata.step + "") {

                                                    value.trailerDetails = $scope.statusesdsplyNew[eee].name + ' - ' + mydata.createdDate;

                                                    $scope.myArray.push(value.trailerDetails);

                                                    value.trailerDetails1 = $scope.myArray[0];
                                                    value.trailerDetails2 = $scope.myArray[1];
                                                    value.trailerDetails3 = $scope.myArray[2];
                                                    value.trailerDetails4 = $scope.myArray[3];
                                                    value.trailerDetails5 = $scope.myArray[4];

                                                    //console.log(value.trailerDetails1);
                                                }
                                            }
                                        }
                                    }
                                }

                            });
                            $scope.exportArray.push(value);
                        });
                    });
                });
            });
        }


        /*export list ends*/




        /************************** Condition Filter ******************************/
        //$scope.condition = '';
        Restangular.all('conditions?filter[where][language]=' + $window.sessionStorage.language + '&filter[order]=orderNo%20ASC&filter[where][deleteflag]=false').getList().then(function (condRes) {
            //console.log('condRes', condRes);
            $scope.conditionsdsply = condRes;
            if ($scope.UserLanguage == 1) {
                $scope.conditionId = condRes[0].id;
            } else if ($scope.UserLanguage != 1) {
                $scope.conditionId = condRes[0].parentId;
            }

        });

        $scope.$watch('conditionId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue == '' || newValue == oldValue || newValue == undefined) {
                return;
            } else {

                $scope.stepId = '';
                $scope.statusId = '';

                // $scope.ConditionList = [];

                if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {

                    $scope.hideEdit = true;
                    $scope.ConditionList = [];
                    $scope.filterCall1 = 'conditionheaders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][condition]=' + newValue;
                    // + '&filter[where][step]=' + $scope.stepId + '&filter[where][status]=' + $scope.status

                    $scope.PatientCall = 'patientrecords?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId;

                    Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                        $scope.PatientDispaly = Patient;


                        Restangular.all($scope.filterCall1).getList().then(function (cns) {
                            //console.log('cns', cns);
                            $scope.conditionheaders = cns;
                            $scope.modalInstanceLoad.close();

                            angular.forEach($scope.conditionheaders, function (value, index) {

                                value.index = index + 1;

                                value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                if (value.syncouttime == null) {
                                    value.syncouttime = '';
                                }

                                var data1 = $scope.partners.filter(function (arr) {
                                    return arr.id == value.memberId
                                })[0];

                                if (data1 != undefined) {
                                    value.membername = data1.fullname;
                                    value.NewtiId = data1.tiId;
                                }

                                var data101 = $scope.PatientDispaly.filter(function (arr) {
                                    return (arr.condition == value.condition && arr.memberId == value.memberId)
                                })[0];


                                if (data101 != undefined) {
                                    value.patientId = data101.id;
                                    value.patientFlag = true;
                                } else {
                                    value.patientFlag = false;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.id == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.parentId == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.id == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.id == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.id == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.parentId == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.id == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.parentId == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }
                                }

                                $scope.ConditionList.push(value);
                            });
                        });
                    });
                } else if ($window.sessionStorage.roleId + "" === "13") {
                    $scope.hideEdit = false;
                    $scope.ConditionList = [];

                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}').getList().then(function (cns) {
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                // console.log('cns', cns);
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];


                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    });
                } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {
                    $scope.hideEdit = true;
                    $scope.ConditionList = [];
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    /*if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                        $scope.hideEdit = false;
                                    } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                        $scope.hideEdit = false;
                                    } else {
                                        $scope.hideEdit = true;
                                    }*/

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    //console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    });
                } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                    $scope.hideEdit = true;
                    $scope.ConditionList = [];


                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}').getList().then(function (cns) {
                                
                                 console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    });
                }  else if($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 9) {
             
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}},{"deleteflag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    
                });
            }
    else if($window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17 || $window.sessionStorage.roleId == 10) {
                
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}},{"deleteflag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                   
                });
            } else if($window.sessionStorage.roleId == 8 ) {
                
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}},{"deleteflag":{"inq":[false]}},{"condition":{"inq":[' + newValue + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                   
                });
            }


                $scope.condition = +newValue;


            }
        });


        /************************** Step Filter ******************************/

        $scope.$watch('stepId', function (newValue, oldValue) {
            if (newValue == '' || newValue == oldValue) {
                return;
            } else {

                $scope.statusId = '';
                // $scope.condition = '';

                $scope.ConditionList = [];

                if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {

                    $scope.hideEdit = true;
                    //  $scope.ConditionList = [];

                    $scope.filterCall2 = 'conditionheaders?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][step]=' + newValue + '&filter[where][condition]=' + $scope.condition;

                    $scope.PatientCall = 'patientrecords?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId;

                    Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                        $scope.PatientDispaly = Patient;
                    });

                    Restangular.all($scope.filterCall2).getList().then(function (cns) {
                        // console.log('step', cns);
                        $scope.conditionheaders = cns;
                        $scope.modalInstanceLoad.close();

                        angular.forEach($scope.conditionheaders, function (value, index) {

                            value.index = index + 1;

                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                            value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                            value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                            if (value.syncouttime == null) {
                                value.syncouttime = '';
                            }

                            var data101 = $scope.PatientDispaly.filter(function (arr) {
                                return (arr.condition == value.condition && arr.memberId == value.memberId)
                            })[0];
                            //console.log('data101', data101);

                            if (data101 != undefined) {
                                value.patientId = data101.id;
                                value.patientFlag = true;
                            } else {
                                value.patientFlag = false;
                            }


                            var data1 = $scope.partners.filter(function (arr) {
                                return arr.id == value.memberId
                            })[0];

                            if (data1 != undefined) {
                                value.membername = data1.fullname;
                                value.NewtiId = data1.tiId;
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.id == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                    return arr.parentId == value.condition
                                })[0];

                                if (data2 != undefined) {
                                    value.conditionname = data2.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.id == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.step
                                })[0];

                                if (data3 != undefined) {
                                    value.conditionstepname = data3.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.id == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                    return arr.parentId == value.status
                                })[0];

                                if (data4 != undefined) {
                                    value.conditionstatusname = data4.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.id == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                    return arr.parentId == value.followup
                                })[0];

                                if (data5 != undefined) {
                                    value.conditionfollowupname = data5.name;
                                }
                            }

                            if ($window.sessionStorage.language == 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.id == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }

                            } else if ($window.sessionStorage.language != 1) {

                                var data6 = $scope.reasonsdsply.filter(function (arr) {
                                    return arr.parentId == value.reasonForCancel
                                })[0];

                                if (data6 != undefined) {
                                    value.reasonforcancellationname = data6.name;
                                }
                            }
                            //   console.log('value', value);

                            $scope.ConditionList.push(value);
                        });
                    });

                } else if ($window.sessionStorage.roleId + "" === "13") {
                    $scope.hideEdit = false;
                    //$scope.ConditionList = [];
                    //Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {


                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;
                        });

                        Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                            // console.log('step', cns);
                            $scope.conditionheaders = cns;
                            $scope.modalInstanceLoad.close();

                            angular.forEach($scope.conditionheaders, function (value, index) {

                                value.index = index + 1;

                                value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                if (value.syncouttime == null) {
                                    value.syncouttime = '';
                                }

                                var data101 = $scope.PatientDispaly.filter(function (arr) {
                                    return (arr.condition == value.condition && arr.memberId == value.memberId)
                                })[0];
                                //console.log('data101', data101);

                                if (data101 != undefined) {
                                    value.patientId = data101.id;
                                    value.patientFlag = true;
                                } else {
                                    value.patientFlag = false;
                                }


                                var data1 = $scope.partners.filter(function (arr) {
                                    return arr.id == value.memberId
                                })[0];

                                if (data1 != undefined) {
                                    value.membername = data1.fullname;
                                    value.NewtiId = data1.tiId;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.id == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.parentId == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.id == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.id == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.id == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.parentId == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.id == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.parentId == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }
                                }
                                //   console.log('value', value);

                                $scope.ConditionList.push(value);
                            });
                        });

                    });

                } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {
                    $scope.hideEdit = true;
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                        //  $scope.ConditionList = [];

                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;
                        });

                        Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                            // console.log('step', cns);
                            $scope.conditionheaders = cns;
                            $scope.modalInstanceLoad.close();

                            angular.forEach($scope.conditionheaders, function (value, index) {

                                value.index = index + 1;

                               /* if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                    $scope.hideEdit = false;
                                } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                    $scope.hideEdit = false;
                                } else {
                                    $scope.hideEdit = true;
                                }
*/
                                value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                if (value.syncouttime == null) {
                                    value.syncouttime = '';
                                }

                                var data101 = $scope.PatientDispaly.filter(function (arr) {
                                    return (arr.condition == value.condition && arr.memberId == value.memberId)
                                })[0];
                                //console.log('data101', data101);

                                if (data101 != undefined) {
                                    value.patientId = data101.id;
                                    value.patientFlag = true;
                                } else {
                                    value.patientFlag = false;
                                }


                                var data1 = $scope.partners.filter(function (arr) {
                                    return arr.id == value.memberId
                                })[0];

                                if (data1 != undefined) {
                                    value.membername = data1.fullname;
                                    value.NewtiId = data1.tiId;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.id == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.parentId == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.id == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.id == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.id == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.parentId == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.id == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.parentId == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }
                                }
                                //   console.log('value', value);

                                $scope.ConditionList.push(value);
                            });
                        });

                    });

                } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                    $scope.hideEdit = true;

                    //$scope.ConditionList = [];
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {


                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;
                        });

                        Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                            // console.log('step', cns);
                            $scope.conditionheaders = cns;
                            $scope.modalInstanceLoad.close();

                            angular.forEach($scope.conditionheaders, function (value, index) {

                                value.index = index + 1;

                                value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                if (value.syncouttime == null) {
                                    value.syncouttime = '';
                                }

                                var data101 = $scope.PatientDispaly.filter(function (arr) {
                                    return (arr.condition == value.condition && arr.memberId == value.memberId)
                                })[0];
                                //console.log('data101', data101);

                                if (data101 != undefined) {
                                    value.patientId = data101.id;
                                    value.patientFlag = true;
                                } else {
                                    value.patientFlag = false;
                                }


                                var data1 = $scope.partners.filter(function (arr) {
                                    return arr.id == value.memberId
                                })[0];

                                if (data1 != undefined) {
                                    value.membername = data1.fullname;
                                    value.NewtiId = data1.tiId;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.id == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.parentId == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.id == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.id == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.id == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.parentId == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.id == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.parentId == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }
                                }
                                //   console.log('value', value);

                                $scope.ConditionList.push(value);
                            });
                        });

                    });

                } 
                

else if($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 9) {
             
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    
                });
            }
    else if($window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17 || $window.sessionStorage.roleId == 10) {
                
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                   
                });
            } else if($window.sessionStorage.roleId == 8 ) {
                
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                   
                });
            }



                $scope.stepId = +newValue;
            }
        });

        /************************** Status Filter ******************************/

        $scope.$watch('statusId', function (newValue, oldValue) {
            if (newValue == '' || newValue == oldValue) {
                return;
            } else {
                //  $scope.condition = '';

                $scope.ConditionList = [];

                if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {
                    $scope.hideEdit = true;
                    // $scope.ConditionList = [];

                    $scope.filterCall3 = 'conditionheaders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][step]=' + $scope.stepId + '&filter[where][status]=' + newValue + '&filter[where][condition]=' + $scope.condition;
                    //  '&filter[where][condition]=' + $scope.condition +

                    $scope.PatientCall = 'patientrecords?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId;

                    Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                        $scope.PatientDispaly = Patient;


                        Restangular.all($scope.filterCall3).getList().then(function (cns) {
                            // console.log('status', cns);
                            $scope.conditionheaders = cns;
                            $scope.modalInstanceLoad.close();
                            angular.forEach($scope.conditionheaders, function (value, index) {

                                value.index = index + 1;

                                value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                if (value.syncouttime == null) {
                                    value.syncouttime = '';
                                }

                                var data1 = $scope.partners.filter(function (arr) {
                                    return arr.id == value.memberId
                                })[0];

                                if (data1 != undefined) {
                                    value.membername = data1.fullname;
                                    value.NewtiId = data1.tiId;
                                }

                                var data101 = $scope.PatientDispaly.filter(function (arr) {
                                    return (arr.condition == value.condition && arr.memberId == value.memberId)
                                })[0];
                                console.log('data101', data101);

                                if (data101 != undefined) {
                                    value.patientId = data101.id;
                                    value.patientFlag = true;
                                } else {
                                    value.patientFlag = false;
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.id == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.parentId == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.id == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.id == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.id == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.parentId == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.id == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data6 = $scope.reasonsdsply.filter(function (arr) {
                                        return arr.parentId == value.reasonForCancel
                                    })[0];

                                    if (data6 != undefined) {
                                        value.reasonforcancellationname = data6.name;
                                    }
                                }


                                $scope.ConditionList.push(value);
                            });
                        });
                    });

                } else if ($window.sessionStorage.roleId + "" === "13") {
                    $scope.hideEdit = false;
                    //  $scope.ConditionList = [];
                    //  Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;



                            Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('status', cns);[condition]=' + $scope.condition
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }


                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    });
                } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {
                    $scope.hideEdit = true;
                    // $scope.ConditionList = [];
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;



                            Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('status', cns);[condition]=' + $scope.condition
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    /*if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                        $scope.hideEdit = false;
                                    } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                        $scope.hideEdit = false;
                                    } else {
                                        $scope.hideEdit = true;
                                    }*/

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }


                                    $scope.ConditionList.push(value);
                                });
                            });
                        });
                    });
                } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                    $scope.hideEdit = true;
                    // $scope.ConditionList = [];

                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                        $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;



                            Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('status', cns);[condition]=' + $scope.condition
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }


                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    });
                } 
                

else if($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 9) {
             
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                    
                });
            }
    else if($window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17 || $window.sessionStorage.roleId == 10) {
                
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                   
                });
            } else if($window.sessionStorage.roleId == 8 ) {
                
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                
                $scope.PatientCall = 'patientrecords?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}},{"deleteflag":{"inq":[false]}}]}}';

                        Restangular.all($scope.PatientCall).getList().then(function (Patient) {
                            $scope.PatientDispaly = Patient;


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}},{"deleteflag":{"inq":[false]}},{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + newValue + ']}},{"condition":{"inq":[' + $scope.condition + ']}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                //,{"step":{"inq":[' + $scope.stepId + ']}},{"status":{"inq":[' + $scope.status + ']}}
                                $scope.conditionheaders = cns;
                                $scope.modalInstanceLoad.close();
                                angular.forEach($scope.conditionheaders, function (value, index) {

                                    value.index = index + 1;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd/MM/yyyy');
                                    value.createdDate = $filter('date')(value.createdDate, 'dd/MM/yyyy');
                                    value.lastModifiedDate = $filter('date')(value.lastModifiedDate, 'dd/MM/yyyy');

                                    if (value.syncouttime == null) {
                                        value.syncouttime = '';
                                    }

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.NewtiId = data1.tiId;
                                    }

                                    var data101 = $scope.PatientDispaly.filter(function (arr) {
                                        return (arr.condition == value.condition && arr.memberId == value.memberId)
                                    })[0];
                                    console.log('data101', data101);

                                    if (data101 != undefined) {
                                        value.patientId = data101.id;
                                        value.patientFlag = true;
                                    } else {
                                        value.patientFlag = false;
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.id == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data6 = $scope.reasonsdsply.filter(function (arr) {
                                            return arr.parentId == value.reasonForCancel
                                        })[0];

                                        if (data6 != undefined) {
                                            value.reasonforcancellationname = data6.name;
                                        }
                                    }

                                    $scope.ConditionList.push(value);
                                });
                            });

                        });
                   
                });
            }


                $scope.status = +newValue;
            }
        });

        /**************************Export data to excel sheet ***************/

        $scope.DisableExport = false;
        $scope.valConCount = 0;
        $scope.exportData = function () {
            console.log("inside export func");
            //$scope.exportArray = [];
            $scope.ConditionArray = [];
            $scope.valConCount = 0;
            if ($scope.exportArray.length == 0) {
                alert('No data found');
            } else {
                for (var arr = 0; arr < $scope.exportArray.length; arr++) {
                    $scope.ConditionArray.push({
                        'Sr No': $scope.exportArray[arr].index,
                        'COUNTRY': $scope.exportArray[arr].countryname,
                        'STATE': $scope.exportArray[arr].statename,
                        'DISTRICT': $scope.exportArray[arr].districtname,
                        'TOWN': $scope.exportArray[arr].townname,
                        'MEMBER NAME': $scope.exportArray[arr].membername,
                        //'HEAD OF THE HOUSEHOLD': $scope.exportArray[arr].householdname,
                        'TI ID': $scope.exportArray[arr].NewtiId,
                        //'ASSIGNED TO': $scope.exportArray[arr].associatedhfname,
                        'CONDITION': $scope.exportArray[arr].conditionname,
                        'STEP': $scope.exportArray[arr].conditionstepname,
                        'STATUS': $scope.exportArray[arr].conditionstatusname,
                        //'CHECK LIST': $scope.exportArray[arr].checkListName,
                        'CASE CLOSED': $scope.exportArray[arr].caseClosed,
                        //'DIAGNOSIS': $scope.exportArray[arr].diagnosisName,
                        //'RISK ASSESSMENT': $scope.exportArray[arr].trailerDetails1,
                        //'SCREENING': $scope.exportArray[arr].trailerDetails2,
                        //'TESTING': $scope.exportArray[arr].trailerDetails3,
                        // 'TREATMENT': $scope.exportArray[arr].trailerDetails4,
                        //'RETESTING': $scope.exportArray[arr].trailerDetails5,
                        'FOLLOW UP': $scope.exportArray[arr].conditionfollowupname,
                        'FOLLOW UP DATE': $scope.exportArray[arr].followupdateDsply,
                        'REASON FOR CANCELLING': $scope.exportArray[arr].reasonforcancellationname,
                        'CREATED BY': $scope.exportArray[arr].createdByname,
                        'CREATED DATE': $scope.exportArray[arr].createdDate,
                        'CREATED ROLE': $scope.exportArray[arr].createdByRolename,
                        'LAST MODIFIED BY': $scope.exportArray[arr].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.exportArray[arr].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.exportArray[arr].lastModifiedByRolename,
                        'DELETE FLAG': $scope.exportArray[arr].deleteflag
                    });

                    $scope.valConCount++;
                    if ($scope.exportArray.length == $scope.valConCount) {
                        alasql('SELECT * INTO XLSX("conditions.xlsx",{headers:true}) FROM ?', [$scope.ConditionArray]);
                    }
                }
            }
        };

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

    });
