'use strict';

angular.module('secondarySalesApp')
    .controller('ZonesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
    
    
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.modalTitle = 'Thank You';

        $scope.disablecountry = false;
      $scope.someFocusVariable = true;
    var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");

        $scope.showForm = function () {
            var visible = $location.path() === '/state/create' || $location.path() === '/state/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/state/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/state/create' || $location.path() === '/state/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/state/create' || $location.path() === '/state/' + $routeParams.id;
            return visible;
        };
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }


        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/zones") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*********************************** INDEX *******************************************/
        var someInput = document.querySelector('#someInput');
        someInput.addEventListener('input', function () {
            someInput.value = someInput.value.toUpperCase();
        });

        //$scope.searchZone = $scope.name;
      //for search by fist letter with first column
    Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (sal) {
            $scope.zones = sal; 
       //console.log("$scope.zones", $scope.zones);
           $scope.TotalData = $scope.zones.sort();
      
      //console.log("$scope.TotalData", $scope.TotalData);
    });
    
    $scope.search = '';
       $scope.startsWith = function (actual, expected) {
           // console.log("inside stratswith");
        var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
                }
      

        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (part) {
            $scope.zones = part;
              Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                  $scope.countries = con;
           // $scope.zoneId = $window.sessionStorage.sales_zoneId;
            angular.forEach($scope.zones, function (member, index) {

                for (var a = 0; a < $scope.countries.length; a++) {
                    if (member.countryId == $scope.countries[a].id) {
                        member.CountryName = $scope.countries[a].name;
                        break;
                    }
                }

                member.index = index + 1;
                $scope.TotalTodos = [];
                $scope.TotalTodos.push(member);
                // console.log('$scope.TotalTodos', $scope.TotalTodos);
                //  console.log('member', member);

            });
        });
});

        $("#someInput").keydown(function (e) {
            var k = e.keyCode || e.which;
            var ok = k >= 65 && k <= 90 || // A-Z
                k >= 96 && k <= 105 || // a-z
                k >= 35 && k <= 40 || // arrows
                k == 9 || //tab
                k == 46 || //del
                k == 8 || // backspaces
                (!e.shiftKey && k >= 48 && k <= 57); // only 0-9 (ignore SHIFT options)

            if (!ok || (e.ctrlKey && e.altKey)) {
                e.preventDefault();
            }
        });


     var timeoutPromise;
        var delayInMs = 1200;

        $scope.orderChange = function (code) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {
                console.log(code);

                 var data =  $scope.zones.filter(function (arr) {
                    return arr.code == code
                })[0];
                 if (data != undefined) {
                    $scope.zone.code = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'State code Already Exist';
                 }
            }, delayInMs);
        };
    
    $scope.CheckDuplicate = function (name) {
           // console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.zones.filter(function (arr) {
                    return arr.name == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.zonename = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'State Name Already Exist';
                 }
            }, delayInMs);
        };
    

        /*-------------------------------------------------------------------------------------*/
        $scope.zone = {
            lastmodifiedtime: new Date(),
            deleteflag: false,
        };



        $scope.statecodeDisable = false;
        $scope.membercountDisable = true;
        if ($routeParams.id) {
            $scope.disablecountry = true;
            $scope.message = 'State has been Updated!';
            $scope.statecodeDisable = true;
            $scope.membercountDisable = true;
            Restangular.one('zones', $routeParams.id).get().then(function (zone) {
                $scope.original = zone;
                $scope.zone = Restangular.copy($scope.original);
                $scope.zonename = $scope.zone.name;
            });
        } else {
            $scope.message = 'State has been created!';
        }
        /************* SAVE *******************************************/
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.SaveZone = function (clicked) {
            document.getElementById('name').style.border = "";
            document.getElementById('someInput').style.border = "";
            // document.getElementById('count').style.border = "";
            if ($scope.zonename == '' || $scope.zonename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter State Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } else if ($scope.zone.code == '' || $scope.zone.code == null) {
                document.getElementById('someInput').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter State Code';
            }
            else if ($scope.zone.code == '' || $scope.zone.code.length != 2) {
                document.getElementById('someInput').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter 2-Digit State Code';
            }
            else if ($scope.zone.countryId == '' || $scope.zone.countryId == null) {
                $scope.zone.countryId = '';
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                 var xyz = $scope.zonename;
                $scope.zone.name = xyz.toUpperCase();
                
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                //toaster.pop('success', "State has been created", null, null, 'trustedHtml');
                Restangular.all('zones').post($scope.zone).then(function (znResponse) {
                    console.log('znResponse', znResponse);
                    window.location = '/state';
                });
            }
        };

        /***************************** UPDATE *******************************************/
        $scope.UpdateZone = function () {
            document.getElementById('name').style.border = "";
            if ($scope.zonename == '' || $scope.zonename == null) {
                $scope.validatestring = $scope.validatestring + 'Plese Enter State Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                 var xyz = $scope.zonename;
                $scope.zone.name = xyz.toUpperCase();
                
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.one('zones', $routeParams.id).customPUT($scope.zone).then(function () {
                    //$location.path('/zones');
                    window.location = '/state';
                });
            }
        };
        /*---------------------------Delete---------------------------------------------------*/



        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }









        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('zones/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



    })
/*.filter('search', function () {
        return function (list, query, fields) {

            if (!query) {
                return list;
            }

            query = query.toLowerCase().split(' ');

            if (!angular.isArray(fields)) {
                fields = [fields.toString()];
            }

            return list.filter(function (item) {
                return query.every(function (needle) {
                    return fields.some(function (field) {
                        var content = item[field] != null ? item[field] : '';

                        if (!angular.isString(content)) {
                            content = '' + content;
                        }

                        return content.toLowerCase().indexOf(needle) > -1;
                    });
                });
            });
        };
    });*/

.filter('startsWith', function() {
        return function(array, search) {
            var matches = [];
            for(var i = 0; i < array.length; i++) {
                if (array[i].indexOf(search) === 0 &&
                    search.length < array[i].length) {
                    matches.push(array[i]);
                }
            }
            return matches;
        };
    });

    

