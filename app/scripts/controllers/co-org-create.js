'use strict';

angular.module('secondarySalesApp')
    .controller('COCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        $scope.isCreateView = true;
        $scope.fscodedisable = false;
        $scope.fsnamedisable = false;
        $scope.hfdflagDisabled = false;
        $scope.heading = 'TI Create';

        $scope.ti = {
            deleteflag: false,
            usercreated: false,
            latitude: '',
            longitude: '',
            address: '',
            lastmodifiedtime: new Date(),
            membercount: 1,
            lastmodifiedby: $window.sessionStorage.userId,
            assignedflag: false
        };

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        /* Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function (mc) {
                                $scope.memcount = mc;
                            });*/

        $scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;

        $scope.typologies = Restangular.all('typologies?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=1').getList().$object;


        $("#code").keydown(function (e) {
            var k = e.keyCode || e.which;
            var ok = k >= 65 && k <= 90 || // A-Z
                k >= 96 && k <= 105 || // a-z
                k >= 35 && k <= 40 || // arrows
                k == 9 || //tab
                k == 46 || //del
                k == 8 || // backspaces
                (!e.shiftKey && k >= 48 && k <= 57); // only 0-9 (ignore SHIFT options)

            if (!ok || (e.ctrlKey && e.altKey)) {
                e.preventDefault();
            }
        });

       

        /********************* WATCH ****************************/
       

        $scope.$watch('ti.countryId', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.ti.state = '';
                $scope.ti.district = '';
                $scope.ti.town = '';
                $scope.ti.ictcId = '';
                $scope.ti.artId = '';

                $scope.zones = Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
            $scope.countryid = +newValue;
        });

        $scope.$watch('ti.state', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.ti.district = '';
                $scope.ti.town = '';
                $scope.ti.ictcId = '';
                $scope.ti.artId = '';

                $scope.salesareas = Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
            $scope.stateid = +newValue;
        });

        $scope.$watch('ti.district', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.ti.town = '';
                $scope.ti.ictcId = '';
                $scope.ti.artId = ''; 

                $scope.cities = Restangular.all('cities?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
               // console.log('$scope.cities', $scope.cities);
            }
            $scope.districtid = +newValue;
        });



        $scope.$watch('ti.town', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.ti.ictcId = '';
                $scope.ti.artId = '';

                Restangular.all('ictccenters?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ICTCresp) {
                    $scope.ictccenters = ICTCresp;
                    //$scope.ti.ictcName = ICTCresp[0].name;
                    //$scope.ti.ictcId = ICTCresp[0].id;
                    //console.log('$scope.ictccenters', $scope.ictccenters);

                    Restangular.all('artcenters?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ARTresp) {
                        $scope.artcenters = ARTresp;
                        // $scope.ti.artName = ARTresp[0].name;
                        // $scope.ti.artId = ARTresp[0].id;
                        // console.log('$scope.artcenters', $scope.artcenters);

                    });
                });
            }
        });

        /***new changes for unique Code*****/
        Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.TIdisply = zn;
        });


        var timeoutPromise;
         var delayInMs = 1500;
    
        $scope.CheckDuplicate = function (name) {
            console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.TIdisply.filter(function (arr) {
                    return arr.firstName == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.tifirstName = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'TI Name Already Exist';
                     
                 }
            }, delayInMs);
        };

       
         $scope.IctcChange = function (salesCode) {

                 var data = $scope.TIdisply.filter(function (arr) {
                     return arr.salesCode == salesCode
                 })[0];

                 if (data != undefined) {
                     $scope.ti.salesCode = '';
                     $scope.toggleValidation();
                     $scope.validatestring1 = 'TI code Already Exist';
                      console.log('data', data);
                 }
            
         };


        $scope.membercountDisable = false;
        $scope.modalTitle = 'Thank You';
        $scope.message = 'TI has been Created!';
        /******************************** SAVE *******************************************/
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";
            document.getElementById('latitude').style.border = "";
            document.getElementById('longitude').style.border = "";
            //document.getElementById('address').style.border = "";
            //document.getElementById('latitude').style.border = "";
            //document.getElementById('longitude').style.border = "";
            //document.getElementById('mobile').style.border = "";
            //document.getElementById('email').style.border = "";

            //var regEmail = /\S+@\S+\.\S+/;
            
            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;

            if ($scope.ti.salesCode == '' || $scope.ti.salesCode == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter TI Code';
                document.getElementById('code').style.borderColor = "#FF0000";
            } 
             else if ($scope.ti.salesCode == '' || $scope.ti.salesCode.length != 3) {
                document.getElementById('code').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter 3-Digit TI Code';
            }
            else if ($scope.tifirstName == '' || $scope.tifirstName == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter  TI Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            else if (!regEmail.test($scope.tifirstName)) {
                    $scope.tifirstName = '';
                    $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                    document.getElementById('name').style.border = "1px solid #ff0000";
                }

            /* else if ($scope.ti.address == '' || $scope.ti.address == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Address';
				document.getElementById('address').style.borderColor = "#FF0000";
			} 
             else if ($scope.ti.latitude == '' || $scope.ti.latitude == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Latitude';
				document.getElementById('latitude').style.borderColor = "#FF0000";
			} 
             else if ($scope.ti.longitude == '' || $scope.ti.longitude == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Longitude';
				document.getElementById('longitude').style.borderColor = "#FF0000";
			} 
            else if ($scope.ti.mobile == '' || $scope.ti.mobile == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Mobile';
				document.getElementById('mobile').style.borderColor = "#FF0000";
			} 
            else if ($scope.ti.email == '' || $scope.ti.email == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter your valid Email Id';
				document.getElementById('email').style.borderColor = "#FF0000";
			}
            else if (!regEmail.test($scope.ti.email)) {
                $scope.ti.email = '';
               $scope.validatestring = $scope.validatestring + 'Plese Enter your valid Email Id';
                document.getElementById('email').style.border = "1px solid #FF0000";
            }*/
            else if ($scope.ti.countryId == '' || $scope.ti.countryId == null) {
                $scope.ti.countryId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            } else if ($scope.ti.state == '' || $scope.ti.state == null) {
                $scope.ti.state == '';
                $scope.validatestring = $scope.validatestring + 'Please Select State';
            } else if ($scope.ti.district == '' || $scope.ti.district == null) {
                $scope.ti.district == '';
                $scope.validatestring = $scope.validatestring + 'Please Select District';
            } else if ($scope.ti.town == '' || $scope.ti.town == null) {
                $scope.ti.town == ''
                $scope.validatestring = $scope.validatestring + 'Please Select Town';
            } 
                
            else if ($scope.ti.ictcId == '' || $scope.ti.ictcId == null) {
                $scope.ti.ictcId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select ICTC Center';
            } else if ($scope.ti.artId == '' || $scope.ti.artId == null) {
                $scope.ti.artId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select ART Center';
            } else if ($scope.ti.typologyId == '' || $scope.ti.typologyId == null) {
                $scope.ti.typologyId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Typology';
            }
            else if ($scope.ti.latitude == '' || $scope.ti.latitude == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter latitude';
                    document.getElementById('latitude').style.borderColor = "#FF0000";

                } else if ($scope.ti.longitude == '' || $scope.ti.longitude == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter longitude';
                    document.getElementById('longitude').style.borderColor = "#FF0000";

                } 

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                
                var xyz = $scope.tifirstName;
                $scope.ti.firstName = xyz.toUpperCase();
                
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;

                //$scope.submitpartners.post($scope.partner).then(function () {
                Restangular.all('employees').post($scope.ti).then(function (resp) {
                    console.log('$scope.ti', $scope.ti);
                   
                        window.location = '/ti';
                   
                });
            }
        };

        
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');

        };
        //$scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        /*  $scope.disabled = function (date, mode) {
              return (mode === 'day' && (date.getDay() === 0));
          };*/

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened2 = true;
        };

        $scope.open3 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened3 = true;
        };

        $scope.open4 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened4 = true;
        };
        $scope.open5 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened5 = true;
        };
        $scope.open6 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened6 = true;
        };
        $scope.open7 = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened7 = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end


        /************************** Map *************************/

        $scope.mapdataModal = false;

        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.ti.latitude = latLng.lat();
                $scope.ti.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };
    
    $scope.CLOSEBUTTON1 = function (){
        console.log('cancle');
         $scope.ti.latitude = '';
                $scope.ti.longitude = '';
         $scope.mapdataModal = false;
    }




    });
