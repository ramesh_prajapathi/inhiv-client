'use strict';

angular.module('secondarySalesApp')
    .controller('MyToDoStatusesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/todofilters/create' || $location.path() === '/todofilters/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todofilters/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todofilters/create' || $location.path() === '/todofilters/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todofilters/create' || $location.path() === '/todofilters/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }


        if ($window.sessionStorage.prviousLocation != "partials/mytodostatuses") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /******************************************************************************************************/

        $scope.showenglishLang = true;

        $scope.$watch('duetodostatus.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {


                    $scope.duetodostatus.name = '';
                    $scope.duetodostatus.parentId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });


        /******************************************************************************************************/

        if ($routeParams.id) {
            $scope.message = 'Todo filter has been Updated!';
            Restangular.one('duetodostatuses', $routeParams.id).get().then(function (duetodostatus) {
                $scope.original = duetodostatus;
                $scope.duetodostatus = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Todo filter has been Created!';
            
        }
        
    
    //new changes for search box
    $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.searchduetodostatus = '';
       
    $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /************************************************************************** INDEX *******************************************/

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('duetodostatuses?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.duetodostatuses = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.duetodostatuses, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.todofillanguages = sev;
            });

        } else {

            Restangular.all('duetodostatuses').getList().then(function (mt) {
                $scope.duetodostatuses = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.duetodostatuses, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });
           
            
            Restangular.all('duetodostatuses?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.todofillanguages = sev;
           
                      angular.forEach($scope.todofillanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                          console.log('member', member);
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.todofillanguages = sev;
                       });
                  }
              });
        }

        /***new changes*****/
        Restangular.all('duetodostatuses?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.todofildisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };


        /*************************************************************************** SAVE *******************************************/
        $scope.duetodostatus = {
            "name": '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Saveduetodostatus = function () {
            document.getElementById('name').style.border = "";
            if ($scope.duetodostatus.language == '' || $scope.duetodostatus.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.duetodostatus.language == 1) {

                if ($scope.duetodostatus.name == '' || $scope.duetodostatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select todofilter';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            } else if ($scope.duetodostatus.language != 1) {

                if ($scope.duetodostatus.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select todofilter in English';

                } else if ($scope.duetodostatus.name == '' || $scope.duetodostatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select todofilter';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            }


            //
            //            $scope.duetodostatuses.post($scope.duetodostatus).then(function () {
            //                console.log('duetodostatus Saved');
            //                window.location = '/todofilters';
            //            });

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.duetodostatus.parentId === '') {
                    delete $scope.duetodostatus['parentId'];
                }
                $scope.submitDisable = true;
                $scope.duetodostatus.parentFlag = $scope.showenglishLang;
                $scope.duetodostatuses.post($scope.duetodostatus).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/todofilters';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'duetodostatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }

        };
        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        /*************************************************************************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updateduetodostatus = function () {
            document.getElementById('name').style.border = "";
            if ($scope.duetodostatus.name == '' || $scope.duetodostatus.name == null) {
                $scope.duetodostatus.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your duetodostatus name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            //            if ($scope.validatestring != '') {
            //                alert($scope.validatestring);
            //                $scope.validatestring = '';
            //            } else {
            //                $scope.duetodostatuses.customPUT($scope.duetodostatus).then(function () {
            //                    console.log('duetodostatus Saved');
            //                    window.location = '/todofilters';
            //                });
            //            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.duetodostatus.parentId === '') {
                    delete $scope.duetodostatus['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('duetodostatuses', $routeParams.id).customPUT($scope.duetodostatus).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('Todofilter Saved');
                    setTimeout(function () {
                        window.location = '/todofilters';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'duetodostatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }


        };
        /********************************* DELETE *******************************************/



        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }









        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('duetodostatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('duetodostatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };



        $scope.getPillar = function (pillarId) {
            return Restangular.one('pillars', pillarId).get().$object;
        };

    });
