'use strict';

angular.module('secondarySalesApp')
    .controller('ToDoAddCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        console.log("im in todoadd controller");
      
    
       $scope.UserLanguage = $window.sessionStorage.language;
    
      $scope.todosone = {
          //  createdByRole: $window.sessionStorage.roleId,
          //  createdDate: new Date(),
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            countryId: $window.sessionStorage.countryId,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            town: $window.sessionStorage.distributionAreaId,
            facility: $window.sessionStorage.coorgId,
             roleId:$window.sessionStorage.roleId,
            deleteflag: false

        };
    
    $scope.disabletodo = false;
    
    
     Restangular.one('todolanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.TodoLanguage = langResponse[0];
         $scope.selecttodo = $scope.TodoLanguage.seltodo;
         $scope.selectfollowupdate = $scope.TodoLanguage.enterdate;
         $scope.selecttodostatus = $scope.TodoLanguage.selstatus;
        });
    
 Restangular.all('todostatuses?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][type]=Todo' + '&filter[order]=orderNo%20ASC').getList().then(function(todoResp){
     $scope.todostatuses = todoResp;
     console.log('todoResp', todoResp);
     
      if ($scope.UserLanguage == 1) {
                $scope.todosone.status = todoResp[0].id;
            } else if ($scope.UserLanguage != 1) {
                $scope.todosone.status = todoResp[0].parentId;
            }
 });
    
 Restangular.all('todotypes?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][type]=Todo' + '&filter[order]=orderNo%20ASC').getList().then(function(resp){
     $scope.todotypes = resp;
//     if ($scope.UserLanguage == 1) {
//                $scope.todosone.todotype = resp[0].id;
//            } else if ($scope.UserLanguage != 1) {
//                $scope.todosone.todotype = resp[0].parentId;
//            }
     
 });
    
     $scope.$watch('todosone.todotype', function (newValue, oldValue) {
        if (newValue == oldValue || newValue == '' || newValue == null) {
            return;
        } else {
            
            Restangular.one('todotypes', newValue).get().then(function (cntn) {
              
                var myDate = new Date();
               
                myDate.setDate(myDate.getDate() + parseInt(cntn.dueInDay));
                
                $scope.todosone.datetime = myDate;
            });
        }
    });
 
    
    
  
    $scope.model = {
    isDisabled:''
        }; 
    
                                                      
    
        /***************/
      
    
     Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (memberResp) {
            $scope.memberNameId = memberResp.fullname + '-' + memberResp.tiId;
           $scope.todosone.beneficiaryid = memberResp.id;
           $scope.todosone.site = memberResp.site;
          
     });
        
               /********************************************** UPDATE ****************************/

        $scope.submitDisable = false;

       
    
     $scope.validatestring = '';
        $scope.SaveTodo = function () {
            
            //document.getElementById('name').style.border = "";
           // document.getElementById('code').style.border = "";

             if ($scope.todosone.todotype == '' || $scope.todosone.todotype == null) {
                    $scope.validatestring =  $scope.validatestring + $scope.selecttodo;
                   // document.getElementById('todotype').style.borderColor = "#FF0000";

                }
         else if ($scope.todosone.status == '' || $scope.todosone.status == null) {
                    $scope.validatestring = $scope.validatestring + $scope.selecttodostatus;
                    //document.getElementById('status').style.borderColor = "#FF0000";

                }
             else if ($scope.todosone.datetime == '' || $scope.todosone.datetime == null) { 
                    $scope.validatestring = $scope.validatestring + $scope.selectfollowupdate;
                    //document.getElementById('name').style.borderColor = "#FF0000";

                }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
               $scope.validatestring = '';
            	
            } else {
               
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
               $scope.submitDisable = true;
                Restangular.all('todos').post($scope.todosone).then(function () {
                    console.log('$scope.todosone', $scope.todosone);
                    window.location = '/';
                });
            }
        };

    
    
      

        $scope.modalTitle = 'Thank You';
        $scope.message = 'Todo has been Saved!';
        $scope.showValidation = false;
        $scope.showUniqueValidation = false;
        $scope.toggleValidation = function () {
            $scope.showUniqueValidation = !$scope.showUniqueValidation;
        };

     
    
     //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.dt1 = new Date();

        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        //        $scope.clear = function () {
        //            $scope.dt = null;
        //        };

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();
        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.todosone.followupopened = true;
        };

        $scope.conditionfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.todosone.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///
    

    });
