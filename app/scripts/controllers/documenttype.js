'use strict';

angular.module('secondarySalesApp')
  .controller('documenttypeCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams,$timeout,baseUrl, $route, $window) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/documenttypes/create' || $location.path() === '/documenttypes/' + $routeParams.id;
      return visible;
    };
    
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/documenttypes/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/documenttypes/create'|| $location.path() === '/documenttypes/' + $routeParams.id;
        return visible;
      };

    
    $scope.hideSearchFilter = function (){
        var visible = $location.path() === '/documenttypes/create'|| $location.path() === '/documenttypes/' + $routeParams.id;
        return visible;
      };

    
    /*********/

  //  $scope.documenttypes = Restangular.all('documenttypes').getList().$object;
    
    if($routeParams.id){
      Restangular.one('documenttypes', $routeParams.id).get().then(function(documenttype){
				$scope.original = documenttype;
				$scope.documenttype = Restangular.copy($scope.original);
      });
    }
    $scope.Search = $scope.name;
    
/*************************** INDEX *******************************************/
       $scope.zn = Restangular.all('documenttypes').getList().then(function (zn) {
        $scope.documenttypes = zn;
        angular.forEach($scope.documenttypes, function (member, index) {
            member.index = index + 1;
        });
    });
    
/***************************** SAVE *******************************************/
     $scope.validatestring = '';
        $scope.Save = function () {
          
                    $scope.documenttypes.post($scope.documenttype).then(function () {
                        console.log('document Type Saved');
                        window.location = '/documenttypes';
                    });
        };
/******************************* UPDATE *******************************************/ 
     $scope.validatestring = '';
        $scope.Update = function () {
                document.getElementById('name').style.border = "";
          if ($scope.documenttype.name == '' || $scope.documenttype.name == null) {
                $scope.documenttype.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your document Type';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.documenttypes.customPUT($scope.documenttype).then(function () {
                        console.log('documenttype Saved');
                        window.location = '/documenttypes';
                    });
                }


        };
/************************** DELETE *******************************************/ 
   $scope.Delete = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('documenttypes/' + id).remove($scope.documenttype).then(function () {
                $route.reload();
            });

        } else {

        }

    }
    
  });


