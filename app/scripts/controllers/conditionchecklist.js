'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionCheckListCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/conditionchecklist/create' || $location.path() === '/conditionchecklist/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/conditionchecklist/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/conditionchecklist/create' || $location.path() === '/conditionchecklist/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/conditionchecklist/create' || $location.path() === '/conditionchecklist/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/conditionchecklist-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('conditionchecklist.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;

                    Restangular.all('conditions?filter[where][deleteFlag]=false').getList().then(function (cns) {
                        $scope.conditions = cns;
                    });

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;

                    Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (cns) {
                        $scope.conditions = cns;
                    });
                }
            }
        });

        $scope.$watch('conditionchecklist.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('conditionchecklists', newValue).get().then(function (sts) {
                    $scope.conditionchecklist.orderNo = sts.orderNo;
                    $scope.conditionchecklist.condition = sts.condition;
                    // console.log($scope.conditionchecklist);
                });
            }
        });

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Checklist has been Updated!';
            Restangular.one('conditionchecklists', $routeParams.id).get().then(function (conditionchecklist) {
                $scope.original = conditionchecklist;
                $scope.conditionchecklist = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Checklist has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/
        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (ck) {
            $scope.cklanguages = ck;
        });

        Restangular.all('conditionchecklists?filter[where][deleteFlag]=false').getList().then(function (cfs) {
            $scope.conditionchecklists = cfs;
            angular.forEach($scope.conditionchecklists, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    Restangular.one('conditions', member.condition).get().then(function (cnts) {
                        member.langname = lng.name;
                        member.conditionName = cnts.name;
                    });
                });
            });
        });

        Restangular.all('conditionchecklists?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (cn) {
            $scope.englishconditionchecklists = cn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };
        /********************************************* SAVE *******************************************/
        $scope.conditionchecklist = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditionchecklist.language == '' || $scope.conditionchecklist.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionchecklist.condition == '' || $scope.conditionchecklist.condition == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.conditionchecklist.name == '' || $scope.conditionchecklist.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Checklist';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditionchecklist.orderNo == '' || $scope.conditionchecklist.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.conditionchecklist.parentId === '') {
                    delete $scope.conditionchecklist['parentId'];
                }
                $scope.submitDisable = true;
                $scope.conditionchecklist.parentFlag = $scope.showenglishLang;
                $scope.conditionchecklists.post($scope.conditionchecklist).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    window.location = '/conditionchecklist-list';
                }, function (error) {
                    if (error.data.error.constraint === 'conditionchecklist_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditionchecklist.language == '' || $scope.conditionchecklist.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionchecklist.condition == '' || $scope.conditionchecklist.condition == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.conditionchecklist.name == '' || $scope.conditionchecklist.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Step';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditionchecklist.orderNo == '' || $scope.conditionchecklist.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.conditionchecklist.parentId === '') {
                    delete $scope.conditionchecklist['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('conditionchecklists', $routeParams.id).customPUT($scope.conditionchecklist).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    // console.log('Step Saved');
                    window.location = '/conditionchecklist-list';
                }, function (error) {
                    if (error.data.error.constraint === 'conditionchecklist_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

                var sort = $scope.sort;

                if (sort.active == column) {
                    return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
                }
            }
            /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('conditionchecklists/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });