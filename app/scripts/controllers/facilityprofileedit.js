'use strict';
angular.module('secondarySalesApp')
    .controller('FacilityProfileEditCtrl', function ($scope, Restangular, $routeParams, $window, $filter, $timeout, $location, $modal, $route) {

   
    $scope.isCreateView = false;
    $scope.fscodedisable = true;
    $scope.fsnamedisable = true;
    $scope.hfdflagDisabled = true;
    $scope.heading = 'TI Edit';

    $scope.modalTitle = 'Thank You';
    $scope.message = 'TI has been Updated!';
    Restangular.one('tilanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.TILanguage = langResponse[0];
        $scope.plsenterfirstname = $scope.TILanguage.enterfirstname;
        $scope.plscorrectnameformat = $scope.TILanguage.selactionplan;
    });

    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle = 'Thank You';
    } else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle = 'धन्यवाद';
    } else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle = 'ಧನ್ಯವಾದ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle = 'நன்றி';
    }



    $scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;

    Restangular.all('typologies?filter[where][deleteflag]=false').getList().then(function (symptom) {
        $scope.typologies = symptom;
        $scope.ti.typologyId = $scope.ti.typologyId;

    });

    Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (art) {
        $scope.artcenters = art;
        $scope.ti.artId = $scope.ti.artId;

    });

    Restangular.all('ictccenters?filter[where][deleteflag]=false').getList().then(function (ictc) {
        $scope.ictccenters = ictc;
        $scope.ti.ictcId = $scope.ti.ictcId;

    });
      Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function (tiresp) {
       $scope.TIdisply = tiresp;

    });
    


    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comemberResponse) {
        $scope.getFacilityId = comemberResponse.tiId;
        console.log(' $scope.getFacilityId',  $scope.getFacilityId);
        Restangular.one('employees', $scope.getFacilityId).get().then(function (ti) {
            $scope.original = ti;

            Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (respzone) {
                $scope.zones = respzone;
                Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (respDist) {
                    $scope.salesareas = respDist;
                    Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (respCity) {
                        $scope.cities = respCity;
                        $scope.ti = Restangular.copy($scope.original);
                        $scope.ti.typologyId = $scope.ti.typologyId.split(',');
                        $scope.tifirstName = $scope.ti.firstName;
                    });
                });
            });


        });

    });
    
    /*******************Check duplicate***********/
     var timeoutPromise;
        var delayInMs = 1000;
    
        $scope.CheckDuplicate = function (name) {
            console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.TIdisply.filter(function (arr) {
                    return arr.firstName == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.tifirstName = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'TI Name Already Exist';
                     
                 }
            }, delayInMs);
        };

    
    
    $scope.ti = {};


    $scope.objEmpti = {};
   

    /************************************ Update *******************************************/

$scope.validatestring = '';
    $scope.Update = function () {

    
         document.getElementById('name').style.border = "";
            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;

        if ($scope.tifirstName == '' || $scope.tifirstName == null) {
            $scope.validatestring = $scope.validatestring + $scope.plsenterfirstname;
            document.getElementById('name').style.borderColor = "#FF0000";
            
        } 
       else if (!regEmail.test($scope.tifirstName)) {
                    $scope.tifirstName = '';
                    $scope.validatestring = $scope.validatestring + $scope.plscorrectnameformat;
                    document.getElementById('name').style.border = "1px solid #ff0000";
                }

           if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
           
        } else {
            
            var xyz = $scope.tifirstName;
         
                $scope.objEmpti.salesCode = $scope.ti.salesCode;
                $scope.objEmpti.lastName = $scope.ti.lastName;
                $scope.objEmpti.latitude = $scope.ti.latitude;
                $scope.objEmpti.longitude = $scope.ti.longitude;
                $scope.objEmpti.address = $scope.ti.address;
               // $scope.objEmpti.zip = $scope.ti.zip;
               // $scope.objEmpti.mobile = $scope.ti.mobile;
                $scope.objEmpti.typologyId = $scope.ti.typologyId;
                $scope.objEmpti.lastmodifiedtime = new Date();
                $scope.objEmpti.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.objEmpti.firstName =  xyz.toUpperCase();
                
            
        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
        $scope.submitDisable = true;
        Restangular.one('employees',  $scope.getFacilityId).customPUT($scope.objEmpti).then(function (test) {
            
             console.log("test", test);
            window.location = '/';
           
        });
        }
    };

    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };



    //Datepicker settings start
    $scope.today = function () {
        $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
    };
    $scope.today();

    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function (date, mode) {
        return (mode === 'day' && (date.getDay() === 0));
    };

    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();

        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.opened = true;
    };


    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end



    /************************** Map *************************/

    $scope.mapdataModal = false;

    $scope.LocateMe = function () {
        $scope.mapdataModal = true;

        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 4,

            center: new google.maps.LatLng(12.9538477, 77.3507369),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        });
        var marker, i;

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(12.9538477, 77.3507369),
            map: map,
            html: ''
        });

        $scope.toggleMapModal();
    };

    $scope.toggleMapModal = function () {
        $scope.mapcount = 0;

        ///////////////////////////////////////////////////////MAP//////////////////////////

        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {
            //  console.log(latLng);
            $scope.ti.latitude = latLng.lat();
            $scope.ti.longitude = latLng.lng();

            //  console.log('$scope.updatepromotion', $scope.updatepromotion);

            document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
        }

        function updateMarkerAddress(str) {
            document.getElementById('mapaddress').innerHTML = str;
        }
        var map;

        function initialize() {

            $scope.latitude = 12.9538477;
            $scope.longitude = 77.3507369;
            navigator.geolocation.getCurrentPosition(function (location) {
                //                    console.log(location.coords.latitude);
                //                    console.log(location.coords.longitude);
                //                    console.log(location.coords.accuracy);
                $scope.latitude = location.coords.latitude;
                $scope.longitude = location.coords.longitude;
                //                });

                // console.log('$scope.address', $scope.address);

                var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                map = new google.maps.Map(document.getElementById('mapCanvas'), {
                    zoom: 4,
                    center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                });
                var marker = new google.maps.Marker({
                    position: latLng,
                    title: 'Point A',
                    map: map,
                    draggable: true
                });

                // Update current position info.
                updateMarkerPosition(latLng);
                geocodePosition(latLng);

                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function () {
                    updateMarkerAddress('Dragging...');
                });

                google.maps.event.addListener(marker, 'drag', function () {
                    updateMarkerStatus('Dragging...');
                    updateMarkerPosition(marker.getPosition());
                });

                google.maps.event.addListener(marker, 'dragend', function () {
                    updateMarkerStatus('Drag ended');
                    geocodePosition(marker.getPosition());
                });
            });


        }

        // Onload handler to fire off the app.
        //google.maps.event.addDomListener(window, 'load', initialize);
        initialize();

        window.setTimeout(function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
            map.setZoom(10);
        }, 1000);


        $scope.SaveMap = function () {
            $scope.showMapModal = !$scope.showMapModal;
            //  console.log($scope.reportincident);
        };

        //console.log('fdfd');
        $scope.showMapModal = !$scope.showMapModal;
    };

});
