'use strict';

angular.module('secondarySalesApp')
    .controller('HealthCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/



        /*********/

        $scope.partners = Restangular.all('partners?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][groupId]=10&filter[where][coorgId]=' + $window.sessionStorage.coorgId).getList().$object;
    
    $scope.physicalappearances = Restangular.all('physicalappearances').getList().$object;
    $scope.emotionalstates = Restangular.all('emotionalstates').getList().$object;

        $scope.health = {
            "currentlypregnant": false,
            "reportingstisymptoms": false,
            "reportingotherhealthissues": false,
            "requiredcondomsavailable": false,
            "physicalappearance": null,
            "emotionalstate": null,
            "moreinformation": null,
            "zoneId": $window.sessionStorage.zoneId,
            "salesAreaId": $window.sessionStorage.salesAreaId,
            "coorgId": $window.sessionStorage.coorgId,
            "partnerId": null
        };

        if ($routeParams.id) {
            Restangular.one('healths', $routeParams.id).get().then(function (health) {
                $scope.original = health;
                $scope.health = Restangular.copy($scope.original);
            });
        }
        $scope.searchhealth = $scope.name;

        /************************************************************************** INDEX *******************************************/
        $scope.zn = Restangular.all('healths').getList().then(function (zn) {
            $scope.healths = zn;
            angular.forEach($scope.healths, function (member, index) {
                member.index = index + 1;
            });
        });

        /*************************************************************************** SAVE *******************************************/
        $scope.validatestring = '';
        $scope.SaveHealth = function () {

            $scope.healths.post($scope.health).then(function () {
                console.log('health Saved');
                window.location = '/health';
            });
        };
        /*************************************************************************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updatehealth = function () {
            document.getElementById('name').style.border = "";
            if ($scope.health.name == '' || $scope.health.name == null) {
                $scope.health.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your health name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.healths.customPUT($scope.health).then(function () {
                    console.log('health Saved');
                    window.location = '/health';
                });
            }


        };
        /*************************************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('healths/' + id).remove($scope.health).then(function () {
                    $route.reload();
                });

            } else {

            }

        }

    });