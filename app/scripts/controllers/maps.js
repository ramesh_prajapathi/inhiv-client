'use strict';
angular.module('secondarySalesApp')
    .controller('TestCtrl', function ($scope, Restangular, $http, baseUrl, $timeout, $filter, $window) {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5,

            center: new google.maps.LatLng(21.0000, 78.0000),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        map.setOptions({
            minZoom: 4
        });


        $scope.retailerlocationviews = '';
        var comarkers = [];
        $scope.partners = Restangular.all('partners').getList().$object;
        $scope.cos = [];
        $scope.colocation = [];
        Restangular.all('partners?filter[where][latitude][nlike]=0%&filter[where][groupId]=8').getList().then(function (retailerlocationviews) {

            //  console.log('retailerlocationviews: ' + retailerlocationviews);
            for (var i = 0; i < retailerlocationviews.length; i++) {
                $scope.la = retailerlocationviews[i].latitude;
                $scope.lo = retailerlocationviews[i].longitude;
            }
            var marker, i;
            var lineSymbol = {
                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                strokeColor: '#292',
                fillColor: '#292',
                fillOpacity: 1,
                scale: 5,

            };
            var rou = [];
            var infowindow = new google.maps.InfoWindow();
            for (i = 0; i < retailerlocationviews.length; i++) {
                $scope.latitude = retailerlocationviews[i].latitude;
                $scope.longitude = retailerlocationviews[i].longitude;
                $scope.name = retailerlocationviews[i].firstName;

                $scope.cos.push(retailerlocationviews[i].id);
                $scope.colocation.push({
                    latitude: retailerlocationviews[i].latitude,
                    longitude: retailerlocationviews[i].longitude
                });

                rou.push(new google.maps.LatLng($scope.latitude, $scope.longitude))
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    map: map,
                    icon: 'images/agent.png',
                    html: $scope.name
                });
                infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, "mouseover", function (e) {
                    infowindow.setContent(this.html);
                    infowindow.open(map, this);
                });
                google.maps.event.addListener(marker, "mouseout", function (e) {
                    infowindow.close(map, this);
                });
                comarkers.push(marker);
                if (i == retailerlocationviews.length - 1) {
                    $scope.GetSites();
                    map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                    //map.setZoom(8); 
                }
            }
        });


        $scope.sites = [];
        $scope.sitelocation = [];
        $scope.GetSites = function () {
            Restangular.all('distribution-routes?filter[where][latitude][nlike]=0%').getList().then(function (retailerlocationviews) {

                // console.log('retailerlocationviews: ' + retailerlocationviews);
                for (var i = 0; i < retailerlocationviews.length; i++) {
                    $scope.la = retailerlocationviews[i].latitude;
                    $scope.lo = retailerlocationviews[i].longitude;
                }
                var marker, i;
                var lineSymbol = {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    strokeColor: '#292',
                    fillColor: '#292',
                    fillOpacity: 1,
                    scale: 5,

                };
                var rou = [];
                var infowindow = new google.maps.InfoWindow();
                for (i = 0; i < retailerlocationviews.length; i++) {
                    $scope.latitude = retailerlocationviews[i].latitude;
                    $scope.longitude = retailerlocationviews[i].longitude;
                    $scope.name = retailerlocationviews[i].name;


                    $scope.sites.push(retailerlocationviews[i].id);
                    $scope.sitelocation.push({
                        latitude: retailerlocationviews[i].latitude,
                        longitude: retailerlocationviews[i].longitude
                    });

                    rou.push(new google.maps.LatLng($scope.latitude, $scope.longitude))
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        map: map,
                        icon: 'images/pointer.png',
                        html: $scope.name
                    });
                    infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, "mouseover", function (e) {
                        infowindow.setContent(this.html);
                        infowindow.open(map, this);
                    });
                    google.maps.event.addListener(marker, "mouseout", function (e) {
                        infowindow.close(map, this);
                    });

                    comarkers.push(marker);

                    if ($scope.cos.indexOf(retailerlocationviews[i].partnerId) != -1) {

                        // console.log('retailerlocationviews[i].partnerId', retailerlocationviews[i].partnerId);
                        var pos = $scope.cos.indexOf(retailerlocationviews[i].partnerId)
                            // console.log('$scope.cos', $scope.cos[pos]);


                        ////////////////////Draw Line Start//////////////
                        var line = new google.maps.Polyline({
                            path: [
                            new google.maps.LatLng(retailerlocationviews[i].latitude, retailerlocationviews[i].longitude),
                            new google.maps.LatLng($scope.colocation[pos].latitude, $scope.colocation[pos].longitude)
                            ],
                            strokeColor: "#FF0000",
                            strokeOpacity: 1.0,
                            strokeWeight: 2,
                            map: map
                        });


                        ////////////////////Draw Line End//////////

                    }

                    if (i == retailerlocationviews.length - 1) {
                        $scope.GetBeneficiaries();
                    }
                }

            });
        }

        $scope.GetBeneficiaries = function () {

            Restangular.all('partners?filter[where][latitude][nlike]=0%&filter[where][groupId]=10').getList().then(function (retailerlocationviews) {

                //  console.log('retailerlocationviews: ' + retailerlocationviews);
                for (var i = 0; i < retailerlocationviews.length; i++) {
                    $scope.la = retailerlocationviews[i].latitude;
                    $scope.lo = retailerlocationviews[i].longitude;
                }
                var marker, i;
                var lineSymbol = {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    strokeColor: '#292',
                    fillColor: '#292',
                    fillOpacity: 1,
                    scale: 5,

                };
                var rou = [];
                var infowindow = new google.maps.InfoWindow();
                for (i = 0; i < retailerlocationviews.length; i++) {
                    $scope.latitude = retailerlocationviews[i].latitude;
                    $scope.longitude = retailerlocationviews[i].longitude;
                    $scope.name = retailerlocationviews[i].firstName;


                    rou.push(new google.maps.LatLng($scope.latitude, $scope.longitude))
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        map: map,
                        icon: 'images/client.png',
                        html: $scope.name
                    });
                    infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, "mouseover", function (e) {
                        infowindow.setContent(this.html);
                        infowindow.open(map, this);
                    });
                    google.maps.event.addListener(marker, "mouseout", function (e) {
                        infowindow.close(map, this);
                    });
                    comarkers.push(marker);


                    if ($scope.sites.indexOf(retailerlocationviews[i].distributionRouteId) != -1) {

                        // console.log('retailerlocationviews[i].partnerId', retailerlocationviews[i].partnerId);
                        var pos = $scope.sites.indexOf(retailerlocationviews[i].distributionRouteId)
                            // console.log('$scope.cos', $scope.cos[pos]);


                        ////////////////////Draw Line Start//////////////
                        var line = new google.maps.Polyline({
                            path: [
                            new google.maps.LatLng(retailerlocationviews[i].latitude, retailerlocationviews[i].longitude),
                            new google.maps.LatLng($scope.sitelocation[pos].latitude, $scope.sitelocation[pos].longitude)
                            ],
                            strokeColor: "#006600",
                            strokeOpacity: 1.0,
                            strokeWeight: 2,
                            map: map
                        });


                        ////////////////////Draw Line End//////////

                    }


                    if (i == retailerlocationviews.length - 1) {
                        var mcOptions = {
                            gridSize: 40,
                            maxZoom: 15
                        };
                        var mc = new MarkerClusterer(map, comarkers, mcOptions);
                    }
                }
            });
        }

        // });




        /*
                $scope.latitude = 12.898360;
                $scope.longitude = 77.617947;
                //$scope.name = retailerlocationviews[i].date;


                var marker, i;
                var lineSymbol = {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    strokeColor: '#292',
                    fillColor: '#292',
                    fillOpacity: 1,
                    scale: 5,

                };

                var rou = [];
                var marker;
                var infowindow = new google.maps.InfoWindow();
                rou.push(new google.maps.LatLng($scope.latitude, $scope.longitude))
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    map: map,
                    icon: 'images/retail.png',
                    html: $scope.name
                });
                infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, "mouseover", function (e) {
                    infowindow.setContent(this.html);
                    infowindow.open(map, this);
                });
                google.maps.event.addListener(marker, "mouseout", function (e) {
                    infowindow.close(map, this);
                });
                // } 
                var polyline = new google.maps.Polyline({
                    path: rou,
                    strokeColor: "#FF0000",
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                    geodesic: true,
                    map: map,
                    icons: [{
                        icon: lineSymbol,
                        offset: '0%'
                      }]
                });

                polyline.setMap(map);
                animateCircle();

                function animateCircle() {
                    var k = 0;
                    window.setInterval(function () {
                        k = (k + 1) % 200
                        var icons = polyline.get('icons');
                        icons[0].offset = (k / 2) + '%';
                        polyline.set('icons', icons);
                    }, 60);
                }
        */


        /*
    
    
       $scope.$watch('retailerlocationviews', function(newValue, oldValue){
          console.log('newValue: ' + newValue);
          console.log('oldValue: ' + oldValue);
            $scope.retailers = Restangular.all('Distribution-routes').getList().$object;
          Restangular.one('locationdetails?filter[where][user]='+newValue+'&filter[where][date][like]='+$scope.routedate+'%'+'&filter[where][latitude][nlike]=0%').get().then(function(retailerlocationviews){
        
                      for (var i = 0; i < retailerlocationviews.length; i++) {
                      $scope.la = retailerlocationviews[i].latitude;
                        $scope.lo = retailerlocationviews[i].longitude;
                      
                   map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 13,
                  
                  center: new google.maps.LatLng($scope.la, $scope.lo),
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                      }    
                      var marker, i;
                var lineSymbol = {
                path:google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                strokeColor: '#292',
               fillColor:'#292',
                fillOpacity: 1,
                 scale: 5,
                      
                };  
                       var rou= [];
                     var infowindow = new google.maps.InfoWindow();
                 for (i = 0; i < retailerlocationviews.length; i++) {
                      $scope.latitude = retailerlocationviews[i].latitude;
                        $scope.longitude = retailerlocationviews[i].longitude;
                        $scope.name = retailerlocationviews[i].date;
                     
                     
                     rou.push(new google.maps.LatLng($scope.latitude, $scope.longitude))
                        marker = new google.maps.Marker({
                    position: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    map: map,
                    icon: 'images/c46e6fa9.retail.png',
                    html:$scope.name
                 });
                     infowindow = new google.maps.InfoWindow();
                google.maps.event.addListener(marker, "mouseover", function (e) {
                    infowindow.setContent(this.html);
                    infowindow.open(map, this);
                });
                google.maps.event.addListener(marker, "mouseout", function (e) { 
                    infowindow.close(map, this);
                    }); 
                 } 
                       var polyline = new google.maps.Polyline({
                          path: rou,
                          strokeColor: "#FF0000",
                          strokeOpacity: 1.0,
                          strokeWeight: 2,
                           geodesic: true,
                            map: map,
                   icons: [{
                 icon: lineSymbol,
              offset: '0%'
              }]
                       });
                  
                polyline.setMap(map);
                animateCircle(); 
             function animateCircle() {
                var k=0;
                window.setInterval(function() {
                  k=(k+1) % 200
                    var icons = polyline.get('icons');
                  icons[0].offset = (k/2) + '%';
                  polyline.set('icons', icons);
              }, 60);
            }      
                      }); 
  });
    
       $scope.$watch('journeydate', function(newValue, oldValue){
          console.log('newValue: ' + newValue);
          console.log('oldValue: ' + oldValue);
      $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
           console.log('$scope.routedate: ' + $scope.routedate);
            });
      $scope.salesagents = Restangular.all('employees').getList().$object;
    /*  $scope.partnerviews='';
          $scope.salesagents = Restangular.all('employees').getList().$object;
      $scope.$watch('partnerviews', function(newValue, oldValue){
          console.log('newValue: ' + newValue);
          console.log('oldValue: ' + oldValue);
         
          //$http.get(baseUrl +'/retailerlocationviews?filter[where][salesagentid]='+newValue).success(function(retailerlocationviews) {
          Restangular.one('retailerlocationviews?filter[where][salesagentid]='+newValue).get().then(function(retailerlocationviews){
       var i;
          for (i = 0; i < retailerlocationviews.length; i++) {
          $scope.la = retailerlocationviews[i].latitude;
            $scope.lo = retailerlocationviews[i].longitude;
              $scope.Id = retailerlocationviews[i].id;
       map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      
      center: new google.maps.LatLng($scope.la, $scope.lo),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
          
              
              var j=[];
          j.push($scope.Id)   
 // $http.get(baseUrl +'/retailerlocationviews?filter[where][id]='+j).success(function(retailerlocationviews) {
          Restangular.one('retailerlocationviews?filter[where][id]='+j).get().then(function(retailerlocationviews){
      
          var marker, i;
       var lineSymbol = {
   path: google.maps.SymbolPath.CIRCLE,
    scale: 8,
    strokeColor: '#393'
       };   
           var rou= [];
   
     for (i = 0; i < retailerlocationviews.length; i++) {
         //var infowindow = new google.maps.InfoWindow();
          $scope.latitude = retailerlocationviews[i].latitude;
            $scope.longitude = retailerlocationviews[i].longitude;
     $scope.name = retailerlocationviews[i].retailername;
         
         rou.push(new google.maps.LatLng($scope.latitude, $scope.longitude))
            marker = new google.maps.Marker({
        position: new google.maps.LatLng($scope.latitude, $scope.longitude),
        map: map,
                icon: 'images/c46e6fa9.retail.png'
      });
         
     } 
           var polyline = new google.maps.Polyline({
              path: rou,
              strokeColor: "#FF0000",
              strokeOpacity: 1.0,
              strokeWeight: 2,
                map: map,
       icons: [{
      icon: lineSymbol,
  offset: '0%'
  }]

        });
      
    polyline.setMap(map);
  
          
    animateCircle();    
         
           
        
 function animateCircle(symbol) {
  
    var k=0;
  
    window.setInterval(function() {
        
      
      k=(k+1) % 200;
        
        var icons = polyline.get('icons');
       
      icons[0].offset = (k/2) + '%';
      polyline.set('icons', icons);
  }, 100);
       
}     
         }); 
            
           } 
     });
  });*/

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0));
        };

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
    });