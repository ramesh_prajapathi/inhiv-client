'use strict';

angular.module('secondarySalesApp')
    .controller('MedicineCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }
        $scope.showForm = function () {
            var visible = $location.path() === '/drugs/create' || $location.path() === '/drugs/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/drugs/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/drugs/create' || $location.path() === '/drugs/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/drugs/create' || $location.path() === '/drugs/edit/' + $routeParams.id;
            return visible;
        };

        $scope.disabledCOndition = false;
        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/drugs-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }
        /***new changes*****/
        $scope.showenglishLang = true;

        $scope.$watch('medicine.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {


                    $scope.medicine.name = '';
                    $scope.medicine.parentId = '';
                    $scope.medicine.conditionId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });

        $scope.$watch('medicine.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {

                Restangular.one('medicines', newValue).get().then(function (resp) {
                    console.log('resp', resp);
                    $scope.medicine.conditionId = resp.conditionId;
                });
            }
        });
        /***new changes*****/

        if ($routeParams.id) {
            $scope.disabledCOndition = true;
            $scope.message = 'Drug Name has been Updated!';
            Restangular.one('medicines', $routeParams.id).get().then(function (medicine) {
                $scope.original = medicine;
                $scope.medicine = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Drug Name has been Created!';
        }

        //$scope.Search = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];

        $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
        
    Restangular.all('conditions?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (con) {
                $scope.conditions = con;
                $scope.medicine.conditionId = $scope.medicine.conditionId;
            });

        /******************************** INDEX *******************************************/
        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.medilanguages = sev;
            });

            Restangular.all('medicines?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.medicines = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.medicines, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }



                        member.index = index + 1;
                    });
                });
            });


        } else {



            Restangular.all('medicines').getList().then(function (mt) {
                //?filter[where][deleteflag]=false
                $scope.medicines = mt;

               Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.medicines, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }



                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('medicines?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;

                        angular.forEach($scope.medilanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;
                    });
                }
            });


        }

        /***new changes*****/
        Restangular.all('medicines?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.medicinedisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        /***new changes*****/
        /********************************************* SAVE *******************************************/
        $scope.medicine = {
            "name": '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false,
            partiallydeleted: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            /**document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";***/

            if ($scope.medicine.language == '' || $scope.medicine.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.medicine.language == 1) {
                if ($scope.medicine.conditionId == '' || $scope.medicine.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                } else if ($scope.medicine.name == '' || $scope.medicine.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Drug Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            } else if ($scope.medicine.language != 1) {

                if ($scope.medicine.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Drug Name in English';

                } else if ($scope.medicine.conditionId == '' || $scope.medicine.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                } else if ($scope.medicine.name == '' || $scope.medicine.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Drug Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.medicine.parentId === '') {
                    delete $scope.medicine['parentId'];
                }
                $scope.submitDisable = true;
                $scope.medicine.parentFlag = $scope.showenglishLang;
                $scope.medicines.post($scope.medicine).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/drugs-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'medicine_lang_parenrid') {
                        alert('Value already exists for this language');

                    }
                });
            }

        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.medicine.language == '' || $scope.medicine.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.medicine.language == 1) {
                if ($scope.medicine.conditionId == '' || $scope.medicine.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                } else if ($scope.medicine.name == '' || $scope.medicine.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Drug Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            } else if ($scope.medicine.language != 1) {

                if ($scope.medicine.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Drug Name in English';

                } else if ($scope.medicine.conditionId == '' || $scope.medicine.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                } else if ($scope.medicine.name == '' || $scope.medicine.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Drug Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.medicine.parentId === '') {
                    delete $scope.medicine['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('medicines', $routeParams.id).customPUT($scope.medicine).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('Medicine Name Saved');
                    setTimeout(function () {
                        window.location = '/drugs-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'medicine_lang_parenrid') {
                        alert('Value already exists for this language');

                    }
                });
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /***************** DELETE *****************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('medicines/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

        /***************** Archive *****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('medicines/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };




    });
