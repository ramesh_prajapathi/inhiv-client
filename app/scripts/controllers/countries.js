'use strict';

angular.module('secondarySalesApp')
    .controller('CountriesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $timeout) {
        /*********/
    
    if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }
    
        $scope.modalTitle = 'Thank You';
    
        $scope.someFocusVariable = true;

        $scope.showForm = function () {
            var visible = $location.path() === '/countries/create' || $location.path() === '/countries/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/countries/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/countries/create' || $location.path() === '/countries/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/countries/create' || $location.path() === '/countries/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/countries") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /*******************
    
    /****************************************************************************************INDEX****************************/

      
    
    $scope.search = '';
       $scope.startsWith = function (actual, expected) {
           // console.log("inside stratswith");
        var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
                }

        Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
            
            $scope.countries = con;
            angular.forEach($scope.countries, function (member, index) {
                member.index = index + 1;
            });
        });
        $scope.country = {
            deleteflag: false
        };

    
     var timeoutPromise;
        var delayInMs = 1200;

        $scope.CheckDuplicate = function (name) {
           // console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.countries.filter(function (arr) {
                    return arr.name == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.countryname = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Country Name Already Exist';
                 }
            }, delayInMs);
        };
    
        /****************************************************************************CREATE************************************/
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
           // document.getElementById('code').style.border = "";

            if ($scope.countryname == '' || $scope.countryname == null) {
                $scope.countryname = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your country name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            } 
              //  else if ($scope.country.sequence == '' || $scope.country.sequence == null) {
//                $scope.country.sequence = null;
//                $scope.validatestring = $scope.validatestring + 'Plese enter TI Count';
//                document.getElementById('code').style.border = "1px solid #ff0000";
//            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                var xyz = $scope.countryname;
                
                $scope.country.name = xyz.toUpperCase();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
               $scope.submitDisable = true;
                $scope.countries.post($scope.country).then(function () {
                    console.log('$scope.country', $scope.country.name);
                    window.location = '/countries';
                });
            }
        };

        /***********************************************************************************UPDATE****************************/

        $scope.validatestring = '';
        $scope.Update = function () {
           
             document.getElementById('name').style.border = "";
            //document.getElementById('code').style.border = "";

            if ($scope.countryname == '' || $scope.countryname == null) {
                $scope.countryname = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your country name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            } 
//            else if ($scope.country.sequence == '' || $scope.country.sequence == null) {
//                $scope.country.sequence = null;
//                $scope.validatestring = $scope.validatestring + 'Plese enter TI Count';
//                document.getElementById('code').style.border = "1px solid #ff0000";
//            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                 var xyz = $scope.countryname;
                
                $scope.country.name = xyz.toUpperCase();
                
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.countries.customPUT($scope.country).then(function () {
                    console.log('$scope.country', $scope.country);
                    window.location = '/countries';
                });
            }
        };


        if ($routeParams.id) {
            $scope.message = 'Country has been Updated!';
            Restangular.one('countries', $routeParams.id).get().then(function (country) {
                $scope.original = country;
                $scope.country = Restangular.copy($scope.original);
                $scope.countryname = $scope.country.name;
            });
        } else {
            $scope.message = 'Country has been created!';
        }






        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

       
        
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };/**********************************************************************************************DELETE*************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('countries/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }


        //        $scope.Delete = function (id) {
        //            if (confirm("Are you sure want to delete..!") == true) {
        //                Restangular.one('countries/' + id).remove($scope.country).then(function () {
        //                    $route.reload();
        //                });
        //
        //            } else {
        //
        //            }
        //
        //        }

    })

.filter('startsWith', function() {
        return function(array, search) {
            var matches = [];
            for(var i = 0; i < array.length; i++) {
                if (array[i].indexOf(search) === 0 &&
                    search.length < array[i].length) {
                    matches.push(array[i]);
                }
            }
            return matches;
        };
    });

    
