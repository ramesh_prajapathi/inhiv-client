'use strict';
angular.module('secondarySalesApp').controller('LoanTrackingCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $modal) {
    /*********/
    $scope.showForm = function () {
        var visible = $location.path() === '/loantracking/create' || $location.path() === '/loantracking/' + $routeParams.id;
        return visible;
    };
   
    /********************************** INDEX *******************************************/
    $scope.zn = Restangular.all('noofmonthrepay?filter[where][deleteflag]=false').getList().then(function (monthrepay) {
        //$scope.noofmonthrepay = monthrepay[0];
        $scope.original = monthrepay[0];
        $scope.monthrepay = Restangular.copy($scope.original);
    });
    $scope.monthrepay = {
        "name": ''
        , "deleteflag": false
    };
    /************************************* UPDATE *******************************************/
    $scope.Update = function () {
        var arr2 = [];
        var str = $scope.monthrepay.name;
        var arr2 = str.split('-');     
        if (arr2.length > 2) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.monthrepay = Restangular.copy($scope.original);
            return;
        }
        if ((isNaN(arr2[0])) || (isNaN(arr2[1]))) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.monthrepay = Restangular.copy($scope.original);
            return;
        }
        if (parseFloat(arr2[0]) > parseInt(arr2[1])) {
            
             $scope.AlertMessage = 'Start number should be lesser than the end number';
            $scope.openOneAlert();
            //console.log('Condition3');
            $scope.monthrepay = Restangular.copy($scope.original);
            return;
        }
        if (arr2[0].length == 0 || arr2[1].length == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.monthrepay = Restangular.copy($scope.original);
            return;
        }
         if (arr2[0] == 0 || arr2[1] == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.monthrepay = Restangular.copy($scope.original);
            return;
        }
        Restangular.one('noofmonthrepay', $scope.monthrepay.id).customPUT($scope.monthrepay).then(function () {
           $scope.AlertMessage = 'Number of month to repay has been Updated!';
            $scope.openOneAlert2();
            window.location = '/';
        });
        
    }
     
     $scope.openOneAlert = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true
                , templateUrl: 'template/AlertModal.html'
                , scope: $scope
                , backdrop: 'static'
                , keyboard: false
                , size: 'sm'
                , windowClass: 'modal-danger'
            });
        };
     $scope.openOneAlert2 = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true
                , templateUrl: 'template/AlertModal2.html'
                , scope: $scope
                , backdrop: 'static'
                , keyboard: false
                , size: 'sm'
                , windowClass: 'modal-sucess'
            });
        };
        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };
    
});