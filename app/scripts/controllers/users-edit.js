'use strict';

angular.module('secondarySalesApp')
    .controller('UsersEditCtrl', function ($scope, Restangular, $routeParams, $window, AnalyticsRestangular) {

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
            $scope.usernameDisable = true;
        } else {
            $scope.usernameDisable = false;
        }
        $scope.heading = 'User Update';
        $scope.Saved = true;
        $scope.Updated = false;
        $scope.roleDisable = true;
        $scope.userDisable = true;
        $scope.userDisable1 = true;
        $scope.usernameDisable = true;


        /*****hide and show Form ******/
        $scope.editShow = true;
        $scope.createShow = false;
        /*****hide and show Form ******/

        $scope.message = 'Users has been Updated!';
        $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        $scope.roleDisplays = Restangular.all('roles?filter[where][deleteflag]=false' + '&filter[where][rolefalg]=A').getList().$object;

        // $scope.groups = Restangular.all('groups').getList().$object;

        $scope.users = Restangular.all('users').getList().$object;
        /* $scope.employees = Restangular.all('employees?filter[where][deleteflag]=false').getList().$object;
         $scope.userDisplays = Restangular.all('employees?filter[where][deleteflag]=false').getList().$object;
         $scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
         //$scope.roles = Restangular.all('roles').getList().$object;
         $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
         $scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;*/


        $scope.fieldworkers = Restangular.all('comembers?filter[where][deleteflag]=false' + '&filter[where][usercreated]=false').getList().$object;


        Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function (coResp) {
            $scope.comembers = coResp;
            $scope.user.tiId = $scope.user.tiId;
        });
        Restangular.all('ictccenters?filter[where][deleteflag]=false').getList().then(function (ictcResp) {
            $scope.ictccenterdisplay = ictcResp;
            $scope.user.ictcId = $scope.user.ictcId;
        });

        Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (artResp) {
            $scope.artcenterdisplay = artResp;
            $scope.user.artId = $scope.user.artId;
        });

        Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (responseCountry) {
            $scope.countries = responseCountry;
            $scope.user.countryId = $scope.user.countryId;
        });

        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (responseZone) {
            $scope.zones = responseZone;
            $scope.user.state = $scope.user.state;
        });

        Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (responsesalesAreas) {
            $scope.salesareas = responsesalesAreas;
            $scope.user.district = $scope.user.district;
        });

        Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (responseTown) {
            $scope.cities = responseTown;
            $scope.user.town = $scope.user.town;
        });

        Restangular.all('ictccenters?filter[where][deleteflag]=false').getList().then(function (ictc) {
            $scope.ictccenters = ictc;
            $scope.user.ictcId = $scope.user.ictcId;
        });

        Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (art) {
            $scope.artcenters = art;
            $scope.user.artId = $scope.user.artId;
        });

        $scope.userDisplay = Restangular.all('users?filter[where][deleteflag]=false').getList().$object;
        /***************check username********/



        $scope.user = {};
        $scope.partner = {
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.userId
        };



        if ($routeParams.id) {
            Restangular.one('users', $routeParams.id).get().then(function (user) {
                 
                $scope.original = user;
                $scope.profileUpdateId = user.profileId;
                
                $scope.user = Restangular.copy($scope.original);
 

                if (user.roleId == 2 || $scope.user.roleId == 20) {

                    Restangular.all('employees?filter[where][id]=' + user.tiId).getList().then(function (comem) {
                        console.log('comem', comem)
                        //$scope.user.usrName = JSON.stringify(comem);
                        $scope.user.tiId = comem[0].id;
                        $scope.user.countryId = comem[0].countryId;
                        $scope.user.state = comem[0].state;
                        $scope.user.district = comem[0].district;
                        $scope.user.town = comem[0].town;
                        $scope.user.ictcId = comem[0].ictcId;
                        $scope.user.artId = comem[0].artId;


                    });

                }
                if (user.roleId == 11 || $scope.user.roleId == 18) {

                    Restangular.all('ictccenters?filter[where][id]=' + user.profileId).getList().then(function (comem) {
                        // $scope.user.usrName = JSON.stringify(comem);
                        $scope.user.ictcId = comem[0].id;
                        $scope.user.countryId = comem[0].countryId;
                        $scope.user.state = comem[0].state;
                        $scope.user.district = comem[0].district;
                        $scope.user.town = comem[0].town;
                    });

                }

                if (user.roleId == 12 || $scope.user.roleId == 19) {

                    Restangular.all('artcenters?filter[where][id]=' + user.profileId).getList().then(function (comem) {
                        // $scope.user.usrName = JSON.stringify(comem);
                        $scope.user.artId = comem[0].id;
                        $scope.user.countryId = comem[0].countryId;
                        $scope.user.state = comem[0].state;
                        $scope.user.district = comem[0].district;
                        $scope.user.town = comem[0].town;

                    });

                }


            });
        };

        $scope.hideCountry = true;
        $scope.hideState = true;
        $scope.hideDistrict = true;
        $scope.hideTown = true;
        $scope.hideTI = true;
        $scope.hideICTC = true;
        $scope.hideART = true;
        $scope.hideartdata = true;
        $scope.hideictcdata = true;

     $scope.disableButton = false;
    
        $scope.$watch('user.roleId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                /* $scope.user.usrName = '';
                 $scope.user.countryId = '';
                 $scope.user.state = '';
                 $scope.user.district = '';
                 $scope.user.town = '';
                 $scope.user.ictcId = '';
                 $scope.user.artId = '';*/
               if (newValue == 1) {

                    $scope.hideCountry = true;
                    $scope.hideState = true;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;

                } else if (newValue == 2 || newValue == 20) {
                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = false;
                    $scope.hideTI = false;
                    $scope.hideartdata = false;
                    $scope.hideictcdata = false;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.userDisable = true;
                    $scope.ItSelect ='Please select TI';
                } else if (newValue == 4) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 8) {

                    $scope.hideCountry = false;
                    $scope.hideState = true;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 9 || newValue == 14 || newValue == 15) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 10 || newValue == 16 || newValue == 17) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 11 || newValue == 18) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = false;
                    $scope.hideTI = true;
                    $scope.hideICTC = false;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = true;
                    $scope.ItSelect ='Please select ICTC';

                } else if (newValue == 12 || newValue == 19) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = false;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = false;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = true;
                    $scope.ItSelect ='Please select ART';

                }

            }
        });


        /********************************************** UPDATE ****************************/

        Restangular.all('users?filter[where][id]=' + $routeParams.id).getList().then(function (submitcomem) {
            $scope.getUserId = submitcomem[0].employeeid;
        });

        $scope.comember = {};
        $scope.fwworker = {};
        $scope.validatestring = '';
        $scope.Update = function () {
             var regEmail = /\S+@\S+\.\S+/;

            document.getElementById('name').style.border = "";
            document.getElementById('mobile').style.border = "";
            document.getElementById('email').style.border = "";
           
            if ($scope.user.firstname == '' || $scope.user.firstname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Mobile Number';
                document.getElementById('mobile').style.borderColor = "#FF0000";
            } else if ($scope.user.mobile.length != 10 ) {
                $scope.validatestring = $scope.validatestring + 'Please Enter valid Mobile Number';
                document.getElementById('mobile').style.borderColor = "#FF0000";
            }
            else if ($scope.user.email == '' || $scope.user.email == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
                document.getElementById('email').style.borderColor = "#FF0000";
            } else if (!regEmail.test($scope.user.email)) {
                $scope.user.email = '';
                $scope.validatestring = $scope.validatestring + 'Please Enter Your valid Email Id';
                document.getElementById('email').style.border = "1px solid #ff0000";
            } else if ($scope.user.language == '' || $scope.user.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Your Language';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring2 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
               /**********new changes****************/
                $scope.disableButton = true;
                $scope.partner.name = $scope.user.firstname;
                $scope.partner.helplineone = $scope.user.mobile;
                $scope.partner.email = $scope.user.email;
               
                Restangular.all('comembers/' + $scope.profileUpdateId).customPUT($scope.partner).then(function (submitpartner) {

                
                Restangular.one('users/' + $routeParams.id).customPUT($scope.user).then(function (subResponse) {
                    console.log('subResponse', subResponse);
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                   
                    if (subResponse.roleId == 13) {
                        $scope.fwworker.mobile = subResponse.mobile;
                        Restangular.one('comembers/' + $scope.getUserId).customPUT($scope.fwworker).then(function (submitfwer) {
                            console.log('submitfwer', submitfwer);
                            setTimeout(function () {
                                window.location = '/users';
                            }, 350);

                        });
                    } else {
                        setTimeout(function () {
                            window.location = '/users';
                        }, 350);

                    }
                    //  });
                }, function (error) {
                    //console.log('error', error);
                    $scope.showValidation = !$scope.showValidation;
                    /*if (response.data.error.constraint == "uniqueness") {
                    	$scope.validatestring1 = 'Email' + ' ' + $scope.user.email + ' ' + 'Already Exists';
                    } else {
                    	$scope.validatestring1 = response.data.error.detail;
                    }*/
                      $scope.disableButton = false;
                    $scope.validatestring1 = 'Username/Email Already Exists';
                    //console.error('console.error', response);
                });
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.message = 'User has been Updated!';
        $scope.showValidation = false;
        $scope.showUniqueValidation = false;
        $scope.toggleValidation = function () {
            $scope.showUniqueValidation = !$scope.showUniqueValidation;
        };


    });
