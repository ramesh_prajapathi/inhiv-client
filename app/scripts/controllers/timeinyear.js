'use strict';
angular.module('secondarySalesApp').controller('TimeinYearCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $modal) {
    /*********/
    if ($window.sessionStorage.roleId != 1) {
        window.location = "/";
    }
    $scope.showForm = function () {
        var visible = $location.path() === '/timeinyear/create' || $location.path() === '/timeinyear/' + $routeParams.id;
        return visible;
    };
    /* $scope.isCreateView = function () {
         if ($scope.showForm()) {
             var visible = $location.path() === '/timeinyear/create';
             return visible;
         }
     };
     $scope.hideCreateButton = function () {
         var visible = $location.path() === '/timeinyear/create' || $location.path() === '/timeinyear/' + $routeParams.id;
         return visible;
     };
     $scope.hideSearchFilter = function () {
         var visible = $location.path() === '/timeinyear/create' || $location.path() === '/timeinyear/' + $routeParams.id;
         return visible;
     };
     /*********************************** Pagination *******************************************
     if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
         $window.sessionStorage.myRoute = null;
         $window.sessionStorage.myRoute_currentPage = 1;
         $window.sessionStorage.myRoute_currentPagesize = 25;
     }
     else {
         $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
         $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
     }
     if ($window.sessionStorage.prviousLocation != "partials/timeinyear") {
         $window.sessionStorage.myRoute_currentPage = 1;
         $window.sessionStorage.myRoute_currentPagesize = 25;
     }
     $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
     $scope.PageChanged = function (newPage, oldPage) {
         $scope.currentpage = newPage;
         $window.sessionStorage.myRoute_currentPage = newPage;
     };
     $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
     $scope.pageFunction = function (mypage) {
         $scope.pageSize = mypage;
         $window.sessionStorage.myRoute_currentPagesize = mypage;
     };
     /*********/
    // $scope.submitstakeholdertypes = Restangular.all('financialgoals').getList().$object;
    //    if ($routeParams.id) {
    //        $scope.message = 'timeinyear type has been Updated!';
    //        Restangular.one('timeinyears', $routeParams.id).get().then(function (stakeholdertype) {
    //            $scope.original = stakeholdertype;
    //            $scope.timeinyear = Restangular.copy($scope.original);
    //        });
    //    }
    //    else {
    //        $scope.message = 'timeinyear type has been Created!';
    //    }
    //    $scope.searchstakeholdertype = $scope.name;
    $scope.message = 'Time in year has been Updated!';
    /***************************** INDEX *******************************************/
    $scope.zn = Restangular.all('timeinyears?filter[where][deleteflag]=false').getList().then(function (zn) {
        $scope.timeinyears = zn[0];
        $scope.original = zn[0];
        $scope.timeinyear = Restangular.copy($scope.original);
    });
    $scope.timeinyear = {
        name: '',
        deleteflag: false
    };
    /************************************ SAVE *******************************************
    $scope.validatestring = '';
    $scope.Savestakeholdertype = function () {
        document.getElementById('name').style.border = "";
      
        if ($scope.timeinyear.name == '' || $scope.timeinyear.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter timeinyear name';
            document.getElementById('name').style.borderColor = "#FF0000";
        }
      
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.timeinyears.post($scope.timeinyear).then(function () {
                console.log('timeinyear Saved');
                window.location = '/timeinyear';
            });
        }
    };
    /********************************* UPDATE *******************************************/
    $scope.Update = function () {
        var arr2 = [];
        var str = $scope.timeinyear.name;
        var arr2 = str.split('-');
        if (arr2.length > 2) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.timeinyear = Restangular.copy($scope.original);
            return;
        }
        if ((isNaN(arr2[0])) || (isNaN(arr2[1]))) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.timeinyear = Restangular.copy($scope.original);
            return;
        }
        if (parseFloat(arr2[0]) > parseInt(arr2[1])) {

            $scope.AlertMessage = 'Start number should be lesser than the end number';
            $scope.openOneAlert();
            //console.log('Condition3');
            $scope.timeinyear = Restangular.copy($scope.original);
            return;
        }
        if (arr2[0].length == 0 || arr2[1].length == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.timeinyear = Restangular.copy($scope.original);
            return;
        }
        if (arr2[0] == 0 || arr2[1] == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.timeinyear = Restangular.copy($scope.original);
            return;
        }
        Restangular.one('timeinyears', $scope.timeinyear.id).customPUT($scope.timeinyear).then(function () {
            $scope.AlertMessage = 'Time in year has been Updated!';
            $scope.openOneAlert2();
            setTimeout(function(){
                window.location = '/';
            },1000);
        });

    }

    $scope.openOneAlert = function () {
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-danger'
        });
    };
    $scope.openOneAlert2 = function () {
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal2.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-sucess'
        });
    };
    $scope.okAlert = function () {
        $scope.modalOneAlert.close();
    };

});
