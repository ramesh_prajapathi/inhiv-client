'use strict';

angular.module('secondarySalesApp')
    .controller('EditDocumentCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout) {

        /* $scope.showForm = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };

		 $scope.isCreateView = function () {
		     if ($scope.showForm()) {
		         var visible = $location.path() === '/applyforschemes/create';
		         return visible;
		     }
		 };
		 $scope.hideCreateButton = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };

		 $scope.hideSearchFilter = function () {
		     var visible = $location.path() === '/applyforschemes/create' || $location.path() === '/applyforschemes/' + $routeParams.id;
		     return visible;
		 };*/
        $scope.HideCreateButton = true;

        /*******************************************************************************************************************/
        $scope.documentmaster = {
            attendees: [],
            documentflag: 'yes'
        };
        $scope.schemestages = Restangular.all('schemestages').getList().$object;
        $scope.states = Restangular.all('zones').getList().$object;
        $scope.genders = Restangular.all('genders').getList().$object;
        $scope.educations = Restangular.all('educations').getList().$object;
        $scope.submitdocumentmasters = Restangular.all('schememasters').getList().$object;
        $scope.submittodos = Restangular.all('todos').getList().$object;
        $scope.beneficiaries = Restangular.all('beneficiaries').getList().$object;
        // $scope.modaldocumentmasters = Restangular.all('schemes?filter[where][deleteflag]=null').getList().$object;

        $scope.sm = Restangular.all('documenttypes').getList().then(function (scheme) {
            $scope.printschemes = scheme;
            //  $scope.groupmeeting.attend = partner.id;
            // console.log('$scope.groupmeeting.attendees.id', $scope.groupmeeting.attendees);
            angular.forEach($scope.printschemes, function (member, index) {
                member.index = index;
                member.enabled = false;
            });
        });


        $scope.getGender = function (genderId) {
            return Restangular.one('genders', genderId).get().$object;
        };

        if ($routeParams.id) {
            Restangular.one('todos', $routeParams.id).get().then(function (todo) {
                Restangular.one('schememasters', todo.reportincidentid).get().then(function (schememaster) {
                    $scope.original = schememaster;
                    $scope.documentmaster = Restangular.copy($scope.original);
                    // $scope.schememaster.datetime = $filter('date')(schememaster.datetime, 'y-MM-dd');
                    console.log('$scope.documentmaster', $scope.documentmaster);
                    //$scope.schememaster.categ = schememaster.category.split(",");
                    //$scope.schememaster.agegrp = schememaster.agegroup.split(",");
                });
            });
        }

        $scope.DeleteFlag = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('schemes/' + id).remove($scope.documentmasters).then(function () {
                    $route.reload();
                });

            } else {

            }

        }

        $scope.Update = function () {

            $scope.todo = {
                "datetime": $scope.documentmaster.datetime, //$filter('date')(new Date(), 'y-MM-dd'),
                "stage": $scope.documentmaster.stage,
                "id": $routeParams.id
            };
            $scope.submitdocumentmasters.customPUT($scope.documentmaster).then(function (resp) {
                console.log('$scope.documentmasters', $scope.documentmaster);
                $scope.submittodos.customPUT($scope.todo).then(function () {
                    console.log('$scope.todo', $scope.todo);
                    window.location = '/trackapplications';
                });

                ///window.location = '/applyforschemes';
            });

        };

       /* $scope.Update = function () {
            $scope.submitdocumentmasters.customPUT($scope.documentmaster).then(function () {
                console.log('$scope.documentmasters', $scope.documentmasters);
                window.location = '/applyforschemes';
            });
        };*/


        //Datepicker settings start
        $scope.today = function () {
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
           // $scope.documentmaster.datetime = $filter('date')(sevendays, 'y-MM-dd')
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.dtmin = new Date();

        // Disable weekend selection
        /*  $scope.disabled = function (date, mode) {
		      return (mode === 'day' && (date.getDay() === 0));
		  };*/

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end



        $scope.reponserec = true;
        $scope.delaydis = true;
        $scope.$watch('documentmaster.stage', function (newValue, oldValue) {
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 15);
            if (newValue === oldValue) {
                return;
            } else if (newValue === '4') {
                $scope.reponserec = false;
                $scope.delaydis = true;
            } else if (newValue === '5') {
                $scope.delaydis = false;
                $scope.reponserec = true;
                //$scope.documentmaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
            } else if (newValue === '3') {
                //$scope.documentmaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
            } else {
                $scope.reponserec = true;
                $scope.delaydis = true;
                $scope.documentmaster.responserecieve = null;
                $scope.documentmaster.delay = null;
                //$scope.documentmaster.datetime = '';
            }
        });


        $scope.rejectdis = true;
        $scope.$watch('documentmaster.responserecieve', function (newValue, oldValue) {
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 180);
            if (newValue === oldValue) {
                return;
            } else if (newValue === 'Approved') {
                //$scope.documentmaster.datetime = $filter('date')(sevendays, 'y-MM-dd');
            } else if (newValue === 'Rejected') {
                $scope.rejectdis = false;
                //$scope.documentmaster.datetime = '';
            } else {
                //$scope.documentmaster.datetime = '';
                $scope.rejectdis = true;
                $scope.documentmaster.rejection = null;
            }
        });

        /************************************************ Search On Modal *************************/

        $scope.schemename = $scope.name;
        $scope.$watch('documentmaster.agegrp', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][agegroup]=' + newValue).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.$watch('documentmaster.gender', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][gender]=' + newValue).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.$watch('documentmaster.stateid', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=null' + '&filter[where][state]=' + newValue).getList().then(function (scheme) {
                    $scope.schemes = scheme;

                    angular.forEach($scope.schemes, function (member, index) {
                        member.index = index;
                        member.enabled = false;
                    });
                });
            }
        });

        $scope.showschemesModal = false;
        $scope.toggleschemesModal = function () {
            $scope.SaveScheme = function () {
                $scope.newArray = [];
                $scope.documentmaster.attendees = [];
                for (var i = 0; i < $scope.printschemes.length; i++) {
                    if ($scope.printschemes[i].enabled == true) {
                        $scope.newArray.push($scope.printschemes[i].index);
                    }
                }
                $scope.documentmaster.attendees = $scope.newArray
                $scope.showschemesModal = !$scope.showschemesModal;
            };


            $scope.showschemesModal = !$scope.showschemesModal;
        };

        $scope.CancelScheme = function () {
            $scope.showschemesModal = !$scope.showschemesModal;
        };


      /*  $scope.$watch('documentmaster.attendees', function (newValue, oldValue) {
            //console.log('watch.attendees', newValue);

            if (newValue === oldValue) {
                return;
            } else {
                if (newValue.length > oldValue.length) {
                    // something was added
                    var Array1 = newValue;
                    var Array2 = oldValue;

                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));

                            }
                        }
                    }
                    $scope.printschemes[Array1[0]].enabled = true;

                    // do stuff
                } else if (newValue.length < oldValue.length) {
                    // something was removed
                    var Array1 = oldValue;
                    var Array2 = newValue;

                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                            }
                        }
                    }
                    $scope.printschemes[Array1[0]].enabled = false;

                }
            }
        });*/


    });