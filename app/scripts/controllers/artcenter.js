'use strict';

angular.module('secondarySalesApp')
    .controller('artcenterCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter) {

        $scope.someFocusVariable = true;
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            // $scope.countryId = $window.sessionStorage.myRoute;
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        if ($window.sessionStorage.prviousLocation != "partials/artcenter" || $window.sessionStorage.prviousLocation != "partials/artcenter-form") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }
        /***********************************************************************************/

        
       // $scope.searchArt = $scope.name;
    
    //for search by fist letter with first column
  /*  Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (sal) {
            $scope.artcenters = sal; 
       //console.log("$scope.zones", $scope.zones);
           $scope.artcentersx = $scope.artcenters.sort();
      
      //console.log("$scope.TotalData", $scope.TotalData);
    });*/
    
    $scope.search = '';
       $scope.startsWith = function (actual, expected) {
           // console.log("inside stratswith");
        var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
                }

        /************************************ Index ***************************************/
        $scope.znes = Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (znes) {
            $scope.countries = znes;
        });
        $scope.zoneDisplay = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
        $scope.salesareaDispaly = Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().$object;
        $scope.cityDispaly = Restangular.all('cities?filter[where][deleteflag]=false').getList().$object;


        $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;


        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('artcenters?filter[where][languageId]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.artcenters = mt;
                
                
                angular.forEach($scope.artcenters, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }


                    /*var data141 = $scope.Displaylanguages.filter(function (arr) {
                        return arr.id == member.languageId
                    })[0];

                    if (data141 != undefined) {
                        member.langName = data141.name;
                    }*/


                    var data14 = $scope.countries.filter(function (arr) {
                        return arr.id == member.countryId
                    })[0];

                    if (data14 != undefined) {
                        member.countryName = data14.name;
                    }
                    var data15 = $scope.zoneDisplay.filter(function (arr) {
                        return arr.id == member.state
                    })[0];

                    if (data15 != undefined) {
                        member.stateName = data15.name;
                    }

                    var data16 = $scope.salesareaDispaly.filter(function (arr) {
                        return arr.id == member.district
                    })[0];

                    if (data16 != undefined) {
                        member.districtName = data16.name;
                    }
                    var data17 = $scope.cityDispaly.filter(function (arr) {
                        return arr.id == member.town
                    })[0];

                    if (data17 != undefined) {
                        member.cityName = data17.name;
                    }
                    member.index = index + 1;
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.medilanguages = sev;
            });

        } else {

            Restangular.all('artcenters').getList().then(function (mt) {
                $scope.artcenters = mt;
                angular.forEach($scope.artcenters, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }

                    /*var data141 = $scope.Displaylanguages.filter(function (arr) {
                        return arr.id == member.languageId
                    })[0];

                    if (data141 != undefined) {
                        member.langName = data141.name;
                    }
*/

                    var data14 = $scope.countries.filter(function (arr) {
                        return arr.id == member.countryId
                    })[0];

                    if (data14 != undefined) {
                        member.countryName = data14.name;
                    }
                    var data15 = $scope.zoneDisplay.filter(function (arr) {
                        return arr.id == member.state
                    })[0];

                    if (data15 != undefined) {
                        member.stateName = data15.name;
                    }

                    var data16 = $scope.salesareaDispaly.filter(function (arr) {
                        return arr.id == member.district
                    })[0];

                    if (data16 != undefined) {
                        member.districtName = data16.name;
                    }
                    var data17 = $scope.cityDispaly.filter(function (arr) {
                        return arr.id == member.town
                    })[0];

                    if (data17 != undefined) {
                        member.cityName = data17.name;
                    }
                    member.index = index + 1;
                });
            });


            Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;

                        angular.forEach($scope.medilanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;
                    });
                }
            });
        }

        /***new changes*****/

        Restangular.all('artcenters?filter[where][deleteflag]=false' + '&filter[where][languageId]=1').getList().then(function (zn) {
            $scope.disaplyactcenetrs = zn;
        });

        $scope.CountryID = '';
        $scope.stateId = '';
        $scope.districtId = '';
        $scope.townId = '';

        $scope.$watch('CountryID', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                 $scope.stateId = '';
        $scope.districtId = '';
        $scope.townId = '';

                Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceSt) {
                    $scope.dispalyZones = responceSt;

                });

                Restangular.all('artcenters?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
                    console.log('countryfilter');
                    $scope.artcenters = ctyRes1;
                    angular.forEach($scope.artcenters, function (member, index) {
                        member.index = index + 1;

                       /* var data141 = $scope.Displaylanguages.filter(function (arr) {
                            return arr.id == member.languageId
                        })[0];

                        if (data141 != undefined) {
                            member.langName = data141.name;
                        }*/

                        var data14 = $scope.countries.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data14 != undefined) {
                            member.countryName = data14.name;
                        }
                        var data15 = $scope.zoneDisplay.filter(function (arr) {
                            return arr.id == member.state
                        })[0];

                        if (data15 != undefined) {
                            member.stateName = data15.name;
                        }

                        var data16 = $scope.salesareaDispaly.filter(function (arr) {
                            return arr.id == member.district
                        })[0];

                        if (data16 != undefined) {
                            member.districtName = data16.name;
                        }
                        var data17 = $scope.cityDispaly.filter(function (arr) {
                            return arr.id == member.town
                        })[0];

                        if (data17 != undefined) {
                            member.cityName = data17.name;
                        }
                    });
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                 $scope.districtId = '';
        $scope.townId = '';

                Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceState) {
                    $scope.displaySalesareas = responceState;

                });

                Restangular.all('artcenters?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
                    console.log('statefilter');
                    $scope.artcenters = ctyRes1;
                    angular.forEach($scope.artcenters, function (member, index) {
                        member.index = index + 1;

                        /*var data141 = $scope.Displaylanguages.filter(function (arr) {
                            return arr.id == member.languageId
                        })[0];

                        if (data141 != undefined) {
                            member.langName = data141.name;
                        }*/

                        var data14 = $scope.countries.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data14 != undefined) {
                            member.countryName = data14.name;
                        }
                        var data15 = $scope.zoneDisplay.filter(function (arr) {
                            return arr.id == member.state
                        })[0];

                        if (data15 != undefined) {
                            member.stateName = data15.name;
                        }

                        var data16 = $scope.salesareaDispaly.filter(function (arr) {
                            return arr.id == member.district
                        })[0];

                        if (data16 != undefined) {
                            member.districtName = data16.name;
                        }
                        var data17 = $scope.cityDispaly.filter(function (arr) {
                            return arr.id == member.town
                        })[0];

                        if (data17 != undefined) {
                            member.cityName = data17.name;
                        }
                    });
                });
                $scope.stateid = +newValue;
            }
        });

        $scope.$watch('districtId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                 $scope.townId = '';

                Restangular.all('cities?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][twon]' + '&filter[where][deleteflag]=false').getList().then(function (responceCity) {
                    $scope.displayCities = responceCity;

                });

                Restangular.all('artcenters?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
                    console.log('districtfilter');
                    $scope.artcenters = ctyRes1;
                    angular.forEach($scope.artcenters, function (member, index) {
                        member.index = index + 1;

                       /* var data141 = $scope.Displaylanguages.filter(function (arr) {
                            return arr.id == member.languageId
                        })[0];

                        if (data141 != undefined) {
                            member.langName = data141.name;
                        }*/

                        var data14 = $scope.countries.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data14 != undefined) {
                            member.countryName = data14.name;
                        }
                        var data15 = $scope.zoneDisplay.filter(function (arr) {
                            return arr.id == member.state
                        })[0];

                        if (data15 != undefined) {
                            member.stateName = data15.name;
                        }

                        var data16 = $scope.salesareaDispaly.filter(function (arr) {
                            return arr.id == member.district
                        })[0];

                        if (data16 != undefined) {
                            member.districtName = data16.name;
                        }
                        var data17 = $scope.cityDispaly.filter(function (arr) {
                            return arr.id == member.town
                        })[0];

                        if (data17 != undefined) {
                            member.cityName = data17.name;
                        }
                    });
                });
                $scope.districtid = +newValue;
            }
        });
        $scope.$watch('townId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {


                Restangular.all('artcenters?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
                    $scope.artcenters = ctyRes1;
                    console.log('Townfilter');
                    angular.forEach($scope.artcenters, function (member, index) {
                        member.index = index + 1;


                        /*var data141 = $scope.Displaylanguages.filter(function (arr) {
                            return arr.id == member.languageId
                        })[0];

                        if (data141 != undefined) {
                            member.langName = data141.name;
                        }*/

                        var data14 = $scope.countries.filter(function (arr) {
                            return arr.id == member.countryId
                        })[0];

                        if (data14 != undefined) {
                            member.countryName = data14.name;
                        }
                        var data15 = $scope.zoneDisplay.filter(function (arr) {
                            return arr.id == member.state
                        })[0];

                        if (data15 != undefined) {
                            member.stateName = data15.name;
                        }

                        var data16 = $scope.salesareaDispaly.filter(function (arr) {
                            return arr.id == member.district
                        })[0];

                        if (data16 != undefined) {
                            member.districtName = data16.name;
                        }
                        var data17 = $scope.cityDispaly.filter(function (arr) {
                            return arr.id == member.town
                        })[0];

                        if (data17 != undefined) {
                            member.cityName = data17.name;
                        }
                    });
                });

            }
        });



        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }



        /*********************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('artcenters/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }


        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('artcenters/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


    })
.filter('startsWith', function() {
        return function(array, search) {
            var matches = [];
            for(var i = 0; i < array.length; i++) {
                if (array[i].indexOf(search) === 0 &&
                    search.length < array[i].length) {
                    matches.push(array[i]);
                }
            }
            return matches;
        };
    });



