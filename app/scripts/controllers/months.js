'use strict';

angular.module('secondarySalesApp')
    .controller('MonthCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/months/create' || $location.path() === '/months/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/months/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/months/create' || $location.path() === '/months/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/months/create' || $location.path() === '/months/' + $routeParams.id;
            return visible;
        };



        /*********************************** Pagination *******************************************/
        /*************************************INDEX****************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }
        /*else {
        		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        		$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        	}
        	*/

        if ($window.sessionStorage.prviousLocation != "partials/months") {
            //$window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }
        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };



        /*********/

        //$scope.submitstakeholdertypes = Restangular.all('financialgoals').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Months has been Updated!';
            Restangular.one('cobudgetmonths', $routeParams.id).get().then(function (stakeholdertype) {
                $scope.original = stakeholdertype;
                $scope.cobudgetmonth = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Months has been Created!';
        }
        $scope.searchstakeholdertype = $scope.name;

        /***************************** INDEX *******************************************/
        $scope.zn = Restangular.all('cobudgetmonths?filter[where][deleteflag]=false').getList().then(function (zn) {
            //
            $scope.cobudgetmonths = zn;
            // console.log('$scope.financialgoals', $scope.financialgoals);
            angular.forEach($scope.cobudgetmonths, function (member, index) {
                member.index = index + 1;
            });
        });

        $scope.cobudgetmonth = {
            name: '',
            deleteflag: false
        };
        /************************************ SAVE *******************************************/
        $scope.validatestring = '';
        $scope.Savestakeholdertype = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.cobudgetmonth.name == '' || $scope.cobudgetmonth.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.hnname == '' || $scope.cobudgetmonth.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.knname == '' || $scope.cobudgetmonth.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.taname == '' || $scope.cobudgetmonth.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.tename == '' || $scope.cobudgetmonth.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.mrname == '' || $scope.cobudgetmonth.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.cobudgetmonths.post($scope.cobudgetmonth).then(function () {
                    console.log('Months Saved');
                    window.location = '/months';
                });
            }
        };
        /********************************* UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updatestakeholdertype = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.cobudgetmonth.name == '' || $scope.cobudgetmonth.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.hnname == '' || $scope.cobudgetmonth.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.knname == '' || $scope.cobudgetmonth.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.taname == '' || $scope.cobudgetmonth.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.tename == '' || $scope.cobudgetmonth.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.cobudgetmonth.mrname == '' || $scope.cobudgetmonth.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Month name in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.cobudgetmonth.customPUT($scope.cobudgetmonth).then(function () {
                    console.log('months Saved');
                    window.location = '/months';
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        /********************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('cobudgetmonths /' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
