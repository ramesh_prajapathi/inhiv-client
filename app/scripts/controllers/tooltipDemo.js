'use strict';

angular.module('secondarySalesApp')


	.controller('tooltipDemoCtrl', function ($scope, toaster,$timeout) {


		$scope.user = {
			id: 10017,
			email: 'foo',
			name: 'John Doe',
			balance: 999,
			isAdmin: false,
			isActive: true,
			gender: 'unknown'
		};

		$scope.save = function () {

			if ($scope.form.$invalid) {
				alert('');
				return;
			}

			$scope.form.$setPristine();
		};

		$timeout(function () {
			$scope.form.forceValidation(true);
		}, 0);
	});
