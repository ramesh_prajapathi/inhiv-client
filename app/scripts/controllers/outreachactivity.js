'use strict';

angular.module('secondarySalesApp')
    .controller('OutReachActivityCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/



        /*********/

        $scope.partners = Restangular.all('partners?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][groupId]=10&filter[where][coorgId]=' + $window.sessionStorage.coorgId).getList().$object;

        $scope.physicalappearances = Restangular.all('physicalappearances').getList().$object;
        $scope.emotionalstates = Restangular.all('emotionalstates').getList().$object;

        $scope.outreachactivity = {
            "sp": false,
            "ssj": false,
            "ids": false,
            "outreachactivity": false,
            "savings": false,
            "credit": false,
            "pension": false,
            "insurance": false,
            "financialsupport": false,
            "zoneId": $window.sessionStorage.zoneId,
            "salesAreaId": $window.sessionStorage.salesAreaId,
            "coorgId": $window.sessionStorage.coorgId,
            "partnerId": null
        };

        if ($routeParams.id) {
            Restangular.one('outreachactivities', $routeParams.id).get().then(function (outreachactivity) {
                $scope.original = outreachactivity;
                $scope.outreachactivity = Restangular.copy($scope.original);
            });
        }
        $scope.searchoutreachactivity = $scope.name;

        /************************************************************************** INDEX *******************************************/
        $scope.zn = Restangular.all('outreachactivities').getList().then(function (zn) {
            $scope.outreachactivities = zn;
            angular.forEach($scope.outreachactivities, function (member, index) {
                member.index = index + 1;
            });
        });

        /*************************************************************************** SAVE *******************************************/
        $scope.validatestring = '';
        $scope.SaveOutReachActivity = function () {

            $scope.outreachactivities.post($scope.outreachactivity).then(function () {
                console.log('outreachactivity Saved');
                window.location = '/outreachactivity';
            });
        };
        /*************************************************************************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Updateoutreachactivity = function () {
            document.getElementById('name').style.border = "";
            if ($scope.outreachactivity.name == '' || $scope.outreachactivity.name == null) {
                $scope.outreachactivity.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your outreachactivity name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.outreachactivities.customPUT($scope.outreachactivity).then(function () {
                    console.log('outreachactivity Saved');
                    window.location = '/outreachactivity';
                });
            }


        };
        /*************************************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('outreachactivities/' + id).remove($scope.outreachactivity).then(function () {
                    $route.reload();
                });

            } else {

            }

        }

    });