'use strict';

angular.module('secondarySalesApp')
    .controller('TiManagerCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.myRoute;
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        if ($window.sessionStorage.prviousLocation != "partials/ti-manager" || $window.sessionStorage.prviousLocation != "partials/ti-manager-form") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }
        /***********************************************************************************/
        $scope.znes = Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (znes) {
            $scope.countries = znes;
        });

        //$scope.searchPart = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','id','name','tiName','helpline'];
        $scope.searchPart = '';


        

        Restangular.all('comembers?filter[where][deleteflag]=false').getList().then(function (part) {
            $scope.partners = part;
           Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function(resp){
                
            $scope.tidisplay = resp;
            angular.forEach($scope.partners, function (member, index) {
               // console.log('member',member)
                
                if (member.usercreated + '' === 'true') {
                    member.colour = "#29DF66";
                } else {
                    member.colour = "#000000";
                }
                
                 for (var b = 0; b < $scope.tidisplay.length; b++) {
                            if (member.tiId == $scope.tidisplay[b].id) {
                                member.tiName = $scope.tidisplay[b].firstName;
                                break;
                            }
                        }

               
                member.index = index + 1;

            });
                 });
        });

        $scope.CountryID = '';
        $scope.stateId = '';
        $scope.districtId = '';
        $scope.twonId = '';

        $scope.$watch('CountryID', function (newValue, oldValue) {
           // console.log('newValue', newValue);
           // console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {


                Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceSt) {
                    $scope.dispalyZones = responceSt;

                });


                Restangular.all('comembers?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (part) {
                      Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function(resp){
                
            $scope.tidisplay = resp;
                    $scope.partners = part;
                    angular.forEach($scope.partners, function (member, index) {

                        if (member.usercreated + '' === 'true') {
                            member.colour = "#29DF66";
                        } else {
                            member.colour = "#000000";
                        }

                       for (var b = 0; b < $scope.tidisplay.length; b++) {
                            if (member.tiId == $scope.tidisplay[b].id) {
                                member.tiName = $scope.tidisplay[b].firstName;
                                break;
                            }
                        }

                        member.index = index + 1;

                    });
                      });
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {


                Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceState) {
                    $scope.displaySalesareas = responceState;

                });

                console.log('statefilter');
                Restangular.all('comembers?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (part) {
                    Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function(resp){
                
            $scope.tidisplay = resp;
                    $scope.partners = part;
                    angular.forEach($scope.partners, function (member, index) {

                        if (member.usercreated + '' === 'true') {
                            member.colour = "#29DF66";
                        } else {
                            member.colour = "#000000";
                        }

                          for (var b = 0; b < $scope.tidisplay.length; b++) {
                            if (member.tiId == $scope.tidisplay[b].id) {
                                member.tiName = $scope.tidisplay[b].firstName;
                                break;
                            }
                        }

                        member.index = index + 1;

                    });
                    });
                });
                $scope.stateid = +newValue;
            }
        });

        $scope.$watch('districtId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {


                Restangular.all('cities?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][twon]' + '&filter[where][deleteflag]=false').getList().then(function (responceCity) {
                    $scope.displayCities = responceCity;

                });

                console.log('districtfilter');

                Restangular.all('comembers?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (part) {
                    
                    Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function(resp){
                
            $scope.tidisplay = resp;
                    $scope.partners = part;
                    angular.forEach($scope.partners, function (member, index) {

                        if (member.usercreated + '' === 'true') {
                            member.colour = "#29DF66";
                        } else {
                            member.colour = "#000000";
                        }

                          for (var b = 0; b < $scope.tidisplay.length; b++) {
                            if (member.tiId == $scope.tidisplay[b].id) {
                                member.tiName = $scope.tidisplay[b].firstName;
                                break;
                            }
                        }

                        member.index = index + 1;

                    });
                    });
                });

                $scope.districtid = +newValue;
            }
        });
        $scope.$watch('townId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                console.log('Townfilter');

                Restangular.all('comembers?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (part) {
                    Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function(resp){
                
            $scope.tidisplay = resp;
                    $scope.partners = part;
                    angular.forEach($scope.partners, function (member, index) {

                        if (member.usercreated + '' === 'true') {
                            member.colour = "#29DF66";
                        } else {
                            member.colour = "#000000";
                        }

                         for (var b = 0; b < $scope.tidisplay.length; b++) {
                            if (member.tiId == $scope.tidisplay[b].id) {
                                member.tiName = $scope.tidisplay[b].firstName;
                                break;
                            }
                        }

                        member.index = index + 1;

                    });
                });
                });
            }
        });


        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }



        /*********************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('comembers/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }


    });
