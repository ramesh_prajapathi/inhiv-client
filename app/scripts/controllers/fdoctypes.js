'use strict';

angular.module('secondarySalesApp')
	.controller('FDocTypCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/fdocumenttypes/create' || $location.path() === '/fdocumenttypes/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/fdocumenttypes/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/fdocumenttypes/create' || $location.path() === '/fdocumenttypes/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/fdocumenttypes/create' || $location.path() === '/fdocumenttypes/' + $routeParams.id;
			return visible;
		};


		$scope.financialdocumenttype = {
			multipleflag: true,
			deleteflag: false
			
		}

		/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 5;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
		if ($window.sessionStorage.prviousLocation != "partials/fdoctypes") {
			//$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 5;
			//$scope.currentpage = 1;
			//$scope.pageSize = 5;
		}
		/*********/

		//  $scope.documenttypes = Restangular.all('documenttypes').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Financial Security has been Updated!';
			Restangular.one('financialdocumenttypes', $routeParams.id).get().then(function (financialdocumenttype) {
				$scope.original = financialdocumenttype;
				$scope.financialdocumenttype = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Financial Security has been Created!';
		}
		$scope.Search = $scope.name;

		/********************************* INDEX *******************************************/
		$scope.zn = Restangular.all('financialdocumenttypes?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.financialdocumenttypes = zn;
			angular.forEach($scope.financialdocumenttypes, function (member, index) {
				member.index = index + 1;
			});
		});

		/*************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.financialdocumenttype.name == '' || $scope.financialdocumenttype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter financial security';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.financialdocumenttypes.post($scope.financialdocumenttype).then(function () {
					console.log('document Type Saved');
					window.location = '/fdocumenttypes';
				});
			}
		};
		/***************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.financialdocumenttype.name == '' || $scope.financialdocumenttype.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter financial security';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.financialdocumenttypes.customPUT($scope.financialdocumenttype).then(function () {
					console.log('documenttype Saved');
					window.location = '/fdocumenttypes';
				});
			}
		};
		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/************************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('financialdocumenttypes/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}
	});
