'use strict';

angular.module('secondarySalesApp')
    .controller('COEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

        $scope.isCreateView = false;
        $scope.fscodedisable = true;
        $scope.fsnamedisable = true;
        $scope.hfdflagDisabled = true;
        $scope.heading = 'TI Edit';


        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.modalTitle = 'Thank You';
        $scope.message = 'TI has been Updated!';

        $scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;

        Restangular.all('typologies?filter[where][deleteflag]=false' + '&filter[where][language]=1').getList().then(function (symptom) {
            $scope.typologies = symptom;
            $scope.ti.typologyId = $scope.ti.typologyId;

        });

        Restangular.all('employees?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.TIdisply = zn;
        });
        if ($routeParams.id) {
            Restangular.one('employees', $routeParams.id).get().then(function (ti) {
                $scope.original = ti;
                 $scope.membercountDisable = true;
                Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (respzone) {
                    $scope.zones = respzone;
                    Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (respDist) {
                        $scope.salesareas = respDist;
                        Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (respCity) {
                            $scope.cities = respCity;
                             Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (artcenter) {
                                 $scope.artcenters = artcenter;
                                 Restangular.all('ictccenters?filter[where][deleteflag]=false').getList().then(function (ictccenter) {
                                 $scope.ictccenters = ictccenter;
                            $scope.ti = Restangular.copy($scope.original);
                            $scope.ti.typologyId = $scope.ti.typologyId.split(',');
                                     $scope.tifirstName = $scope.ti.firstName;
                        });
                             });
                        });
                    });
                });


            });

        };

    
     $("#code").keydown(function (e){
		var k = e.keyCode || e.which;
		var ok = k >= 65 && k <= 90 || // A-Z
			k >= 96 && k <= 105 || // a-z
			k >= 35 && k <= 40 || // arrows
			k == 9 || //tab
			k == 46 || //del
			k == 8 || // backspaces
			(!e.shiftKey && k >= 48 && k <= 57); // only 0-9 (ignore SHIFT options)

		if(!ok || (e.ctrlKey && e.altKey)){
			e.preventDefault();
		}
	});

    var timeoutPromise;
        var delayInMs = 1000;
    
        $scope.CheckDuplicate = function (name) {
            console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.TIdisply.filter(function (arr) {
                    return arr.firstName == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.tifirstName = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'TI Name Already Exist';
                     
                 }
            }, delayInMs);
        };


        /********************* WATCH ****************************/

     /*   $scope.$watch('ti.countryId', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.zones = Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
            $scope.countryid = +newValue;
        });

        $scope.$watch('ti.state', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.salesareas = Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
            $scope.stateid = +newValue;
        });

        $scope.$watch('ti.district', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.cities = Restangular.all('cities?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
                console.log('$scope.cities', $scope.cities);
            }
            $scope.districtid = +newValue;
        });



        $scope.$watch('ti.town', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                Restangular.all('ictccenters?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ICTCresp) {
                    $scope.ictccenters = ICTCresp;
                    $scope.ti.ictcName = ICTCresp[0].name;
                    $scope.ti.ictcId = ICTCresp[0].id;
                    //console.log('$scope.ictccenters', $scope.ictccenters);

                    Restangular.all('artcenters?filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ARTresp) {
                        $scope.artcenters = ARTresp;
                        $scope.ti.artName = ARTresp[0].name;
                        $scope.ti.artId = ARTresp[0].id;
                        // console.log('$scope.artcenters', $scope.artcenters);

                    });
                });
            }
        });*/

        $scope.ti = {
            lastmodifiedtime: new Date()
           // lastmodifiedby: $window.sessionStorage.UserEmployeeId
        };


        /************************************ Update *******************************************/
         $scope.validatestring = '';

        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('latitude').style.border = "";
            document.getElementById('longitude').style.border = "";
            
            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
            
            if ($scope.tifirstName == '' || $scope.tifirstName == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter  TI Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            else if (!regEmail.test($scope.tifirstName)) {
                    $scope.tifirstName = '';
                    $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                    document.getElementById('name').style.border = "1px solid #ff0000";
                }

             else if ($scope.ti.latitude == '' || $scope.ti.latitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter latitude';
                document.getElementById('latitude').style.borderColor = "#FF0000";

            } else if ($scope.ti.longitude == '' || $scope.ti.longitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter longitude';
                document.getElementById('longitude').style.borderColor = "#FF0000";

            } else if ($scope.ti.typologyId == '' || $scope.ti.typologyId == null) {
                $scope.ti.typologyId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Typology';
            }
            
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                
                 var xyz = $scope.tifirstName;
               
              
                
              // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
            
            console.log('$scope.ti', $scope.ti);
                $scope.objEmp = {};
            $scope.objEmp.firstName = $scope.ti.firstName;
            $scope.objEmp.lastName = $scope.ti.lastName;
            $scope.objEmp.latitude = $scope.ti.latitude;
            $scope.objEmp.longitude = $scope.ti.longitude;
            $scope.objEmp.address = $scope.ti.address;
            $scope.objEmp.zip = $scope.ti.zip;
            //$scope.objEmp.mobile = $scope.ti.mobile;
            $scope.objEmp.typologyId = $scope.ti.typologyId;
            //$scope.objEmp.email = $scope.ti.email;
            $scope.objEmp.lastmodifiedtime = new Date();
                  $scope.objEmp.firstName = xyz.toUpperCase();
            //$scope.objEmp.membercount= $scope.ti.membercount;
           // console.log('$scope.ti.membercount',$scope.ti.membercount);            
            
                 Restangular.one('employees', $routeParams.id).customPUT($scope.objEmp).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('ti updated');
                    setTimeout(function () {
                        window.location = '/ti';
                    }, 350);
                }, function (error) {

                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
    


        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0));
        };

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end



        /************************** Map *************************/

        $scope.mapdataModal = false;

        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
               $scope.ti.latitude = latLng.lat();
                $scope.ti.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };

    });
