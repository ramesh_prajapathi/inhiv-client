'use strict';
angular.module('secondarySalesApp')
    .controller('ReportIncidentEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/

        /*********/
        $scope.submittodos = Restangular.all('todos').getList().$object;

        $scope.partners = Restangular.all('partners?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][groupId]=10&filter[where][coorgId]=' + $window.sessionStorage.coorgId).getList().$object;

        $scope.beneficiaries = Restangular.all('beneficiaries').getList().$object;
        //$scope.onebeneficiaries = Restangular.all('beneficiaries', $window.sessionStorage.fullName).getList().$object;
        $scope.submitrepostincident = Restangular.all('reportincidents').getList().$object;


        $scope.mem = Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (member) {
            $scope.memberfullname = member;
        });

        $scope.HideSubmitButton = true;

        $scope.reportincident = {
            // "referredby":"no",
            "physical": "no",
            "emotional": "no",
            "sexual": "no",
            "propertyrelated": "no",
            "childrelated": "no",
            //"other": false,
            "police": "no",
            "goons": "no",
            "clients": "no",
            "partners": "no",
            "serviceprovider": "no",
            "familymember": "no",
            "otherkp": "no",
            "perpetratorother": "no",
            "neighbour": "no",
            "authorities": "no",
            "co": "no",
            "timetorespond": "no",
            //"incidentlocation": "no",
            "resolved": "no",

            "reported": "no",
            "reportedpolice": "no",
            "reportedngos": "no",
            "reportedfriends": "no",
            "reportedplv": "no",
            "reportedcoteam": "no",

            "reportedchampions": "no",
            "reportedotherkp": "no",
            "reportedlegalaid": "no",

            "referredcouncelling": "no",
            "referredmedicalcare": "no",
            "referredcomanager": "no",
            "referredplv": "no",
            "referredlegalaid": "no",
            "referredboardmember": "no",
            "multiplemembers": ['1', '2'],

            "stateid": $window.sessionStorage.zoneId,
            "districtid": $window.sessionStorage.salesAreaId,
            "coid": $window.sessionStorage.coorgId,
            "beneficiaryid": null
        };
        // $scope.todo = {};
        //$scope.reportincident.multiplemembers.push($window.sessionStorage.fullName);



        /*$scope.reportincident = [{
			// "referredby":"no",
			"physical": false,
			"emotional": false,
			"sexual": false,
			"propertyrelated": false,
			"childrelated": false,
			//"other": false,
			"police": false,
			"goons": false,
			"clients": false,
			"partners": false,
			"serviceprovider": false,
			"familymember": false,
			"otherkp": false,
			"perpetratorother": false,
			"neighbour": false,
			"authorities": false,
			"co": false,
			"timetorespond": false,
			//"incidentlocation": "no",
			"resolved": false,

			"reported": false,
			"reportedpolice": false,
			"reportedngos": false,
			"reportedfriends": false,
			"reportedplv": false,
			"reportedcoteam": false,

			"reportedchampions": false,
			"reportedotherkp": false,
			"reportedlegalaid": false,

			"referredcouncelling": false,
			"referredmedicalcare": false,
			"referredcomanager": false,
			"referredplv": false,
			"referredlegalaid": false,
			"referredboardmember": false,


			"stateid": $window.sessionStorage.zoneId,
			"districtid": $window.sessionStorage.salesAreaId,
			"coid": $window.sessionStorage.coorgId
			}];*/


        /*************************************************** INDEX *******************************************/
        $scope.zn = Restangular.all('reportincidents').getList().then(function (zn) {
            $scope.reportincidents = zn;
            angular.forEach($scope.reportincidents, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/

        /* $scope.reportincident = {
             multiplemembers: [],
             follow: []
         };*/
        //$scope.reportincident.multiplemembers = [];
        $scope.reportincident.follow = [];
        // $scope.todo.multiplemembers = [];
        $scope.todo = {
            multiplemembers: []
        };

        console.log('$scope.reportincident.multiplemembers.length', $scope.reportincident.multiplemembers.length);
        /******************************* SAVE *****************************************/

        $scope.reportincident.reported = 'no';
        $scope.report = true;
        $scope.$watch('reportincident.reported', function (newValue, oldValue) {
            //console.log('Report Too');
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.reported === 'yes') {
                $scope.report = false;
            } else {
                $scope.report = true;
            }
        });

        $scope.reportincident.reportedcoteam = 'no';
        $scope.timetorespond = true;
        $scope.$watch('reportincident.reportedcoteam', function (newValue, oldValue) {
            //console.log('reportincident.reportedcoteam');
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.reportedcoteam === 'yes') {
                $scope.timetorespond = false;
            } else {
                $scope.timetorespond = true;
            }
        });



        //$scope.reportincident.co = 'no';
        $scope.member = true;
        $scope.memberone = true;
        $scope.$watch('reportincident.co', function (newValue, oldValue) {
            console.log('reportincident.co', newValue);
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.co === 'no') {
                $scope.member = true;
                $scope.memberone = true;
            } else if ($scope.reportincident.co === 'yes') {
                $scope.member = false;
                $scope.memberone = false;

            } else {
                $scope.member = true;
                $scope.memberone = true;
            }

        });

        $scope.dateofclosure = true;
        $scope.$watch('reportincident.currentstatus', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else if ($scope.reportincident.currentstatus === 'Resolved') {
                $scope.dateofclosure = false;
            } else if ($scope.reportincident.currentstatus === 'Closed') {
                $scope.dateofclosure = false;
            } else {
                $scope.dateofclosure = true;
            }

        });



        /*
				if ($routeParams.id) {
					Restangular.one('reportincidents', $routeParams.id).get().then(function (reportincident) {
						$scope.original = reportincident;
						$scope.reportincident = Restangular.copy($scope.original);
					});
				}
				$scope.searchreportincident = $scope.name;

				/******************************************************* UPDATE *******************************************/

        $scope.validatestring = '';
        $scope.Updatereportincident = function () {
            document.getElementById('name').style.border = "";
            if ($scope.reportincident.name == '' || $scope.reportincident.name == null) {
                $scope.reportincident.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your reportincident name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.submitrepostincident.customPUT($scope.reportincident).then(function () {
                    console.log('reportincident Saved');
                    window.location = '/reportincident';
                });
            }


        };

        /*********************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('reportincidents/' + id).remove($scope.reportincident).then(function () {
                    $route.reload();
                });

            } else {

            }

        }

        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        $scope.clear = function () {
            $scope.dt = null;
        };

        // $scope.dtmin = new Date();

        // Disable weekend selection
        /*  $scope.disabled = function (date, mode) {
		      return (mode === 'day' && (date.getDay() === 0));
		  };*/

        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.followupopened = true;
        };






        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
        /************************************************************* MOdal ******************************/
        $scope.reportincidentfollowups = Restangular.all('reportincidentfollowups').getList().$object;
        $scope.addedtodos = [];

        $scope.$watch('reportincident.follow', function (newValue, oldValue) {
            //console.log('followupneeded.followupneeded', newValue);
            $scope.newtodo = {};



            $scope.newtodo.status = 1;
            $scope.newtodo.todotype = 27;
            $scope.newtodo.datetime = $filter('date')(new Date(), 'dd-MMMM-yyyy');
            $scope.showfollowupModal = false;


            $scope.oldFollowUp = oldValue;
            $scope.newFollow = newValue;
            if (newValue === oldValue) {
                return;
            } else {
                $scope.SavetodoFollow = function () {
                    $scope.attendeespurpose = newValue;
                    $scope.addedtodos.push($scope.newtodo);
                    $scope.showfollowupModal = !$scope.showfollowupModal;
                    console.log('$scope.addedtodos', $scope.addedtodos);
                };

                console.log('newValue', newValue);
                console.log('oldValue', oldValue);

                var array3 = newValue.filter(function (obj) {
                    return oldValue.indexOf(obj) == -1;
                });
                if (array3.length > 0) {
                    console.log('unique', array3);
                    for (var i = 0; i < $scope.reportincidentfollowups.length; i++) {
                        if ($scope.reportincidentfollowups[i].id == array3[0]) {
                            $scope.printpurpose = $scope.reportincidentfollowups[i].name;
                            $scope.newtodo.purpose = $scope.reportincidentfollowups[i].id;
                        }
                    }
                }


                if (oldValue != undefined) {
                    if (oldValue.length < newValue.length) {
                        $scope.showfollowupModal = !$scope.showfollowupModal;
                    }
                } else {
                    $scope.showfollowupModal = !$scope.showfollowupModal;
                }
            };
        });




        $scope.CancelFollow = function () {
            $scope.reportincident.follow = $scope.oldFollowUp;
            //console.log('$scope.groupmeeting.follow', $scope.groupmeeting.follow);
            $scope.showfollowupModal = !$scope.showfollowupModal;
        };



        $scope.todocount = 0;
        $scope.todocount1 = 0;
        $scope.SaveToDo = function () {
            $scope.addedtodocount = 0;
            $scope.addedtodocount1 = 0;
            for (var i = 0; i < $scope.addedtodos.length; i++) {
                $scope.addedtodos[i].beneficiaryid = $scope.attendeestodo[$scope.todocount];
                //$scope.addedtodos[i].purpose = $scope.attendeespurpose[$scope.todocount1];
                //console.log('$scope.addedtodos[i].purpose', $scope.addedtodos[i].purpose);
                $scope.submittodos.post($scope.addedtodos[i]).then(function (resp) {
                    $scope.addedtodocount++;
                    $scope.addedtodocount1++;
                    if ($scope.addedtodocount >= $scope.addedtodos.length && $scope.addedtodocount1 >= $scope.addedtodos.length) {
                        $scope.todocount++;
                        $scope.todocount1++;
                        if ($scope.todocount < $scope.attendeestodo.length && $scope.todocount1 < $scope.attendeespurpose.length) {
                            $scope.SaveToDo();
                        } else if ($scope.SaveToDo.length <= $scope.addedtodos.length) {
                            $route.reload();
                            //window.location = '/groupmeeting';
                        }
                    }
                });

            }
        };
        console.log('$scope.reportincident.multiplemembers.length;', $scope.reportincident.multiplemembers.length);
        //$scope.reportincident = [];
        $scope.reportadd = 0;
        $scope.SaveReportIncident = function () {
            $scope.reportcount = 0;
            //$scope.reportincident.beneficiaryid = $window.sessionStorage.fullName;
            //$scope.reportincidents.post($scope.reportincident).then(function (onemember) {
            for (var i = 0; i < $scope.reportincident.follow.length; i++) {
                if (i == 0) {
                    $scope.reportincident.followupneeded = $scope.reportincident.follow[i];
                } else {
                    $scope.reportincident.followupneeded = $scope.reportincident.followupneeded + ',' + $scope.reportincident.follow[i];
                }
            }

            $scope.SaveIncident();
        };

        $scope.newtodocount = 0;
        $scope.SaveIncident = function () {
            $scope.reportincident.beneficiaryid = $scope.reportincident.multiplemembers[$scope.reportadd];
            console.log('$scope.reportadd', $scope.reportadd)
            console.log('$scope.reportincident.multiplemembers', $scope.reportincident.multiplemembers[$scope.reportadd])
            console.log('$scope.reportincident', $scope.reportincident)
            $scope.reportincidents.post($scope.reportincident).then(function (allmember) {
                $scope.reportadd++;

                //$scope.SaveIncident();

                for (var i = 0; i < $scope.addedtodos.length; i++) {
                    $scope.addedtodos[i].beneficiaryid = $scope.reportincident.beneficiaryid;
                    $scope.submittodos.post($scope.addedtodos[i]).then(function (resp) {
                        $scope.newtodocount++;
                        if ($scope.newtodocount >= $scope.addedtodos.length) {
                            $scope.newtodocount = 0;
                            if ($scope.reportadd < $scope.reportincident.multiplemembers.length) {
                                $scope.SaveIncident();

                            } else {
                                $route.reload();
                            }
                        }
                    });

                }
                /*  if ($scope.reportcount >= $scope.reportincident.multiplemembers.length) {
                        $scope.reportadd++;
                        if ($scope.reportadd < $scope.reportincident.multiplemembers.length) {
                            $scope.SaveToDo();
                            console.log('$scope.reportadd', $scope.reportadd)
                        } else {
                            //$route.reload();
                            //window.location = '/groupmeeting';

                        }
                    }
                console.log('All member', allmember);
                console.log('Save Todo');*/
            });
        };
        console.log('reportincidentbofore', $scope.reportincident);

        $scope.$watch('reportincident.multiplemembers', function (newValue, oldValue) {
            console.log('reportincident', $scope.reportincident);
            //console.log('reportincident.multiplemembers oldValue', oldValue);
            $scope.attendeestodo = newValue;
            console.log('$scope.attendeestodo', $scope.attendeestodo);
        });


    });