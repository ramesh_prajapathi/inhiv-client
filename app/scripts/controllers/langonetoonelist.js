'use strict';

angular.module('secondarySalesApp')
    .controller('LangOnetoonectrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {


        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }
    
    
     $scope.showForm = function () {
            var visible = $location.path() === '/LangOnetoone/create' || $location.path() === '/LangOnetoone/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/LangOnetoone/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/LangOnetoone/create' || $location.path() === '/LangOnetoone/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/LangOnetoone/create' || $location.path() === '/LangOnetoone/edit/' + $routeParams.id;
            return visible;
        };
    /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
            //console.log('$scope.pillarid', $scope.pillarid);
        }

        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
        if ($window.sessionStorage.prviousLocation != "partials/lang-onetoone") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };
    
    // new changes for seacrh box
    
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','languageName','referenceno','date','currenthivstatus','facilityhelplineno'];
        $scope.Search = '';
    

    // $scope.Dislanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
    
    
    if($window.sessionStorage.roleId == 4){
        
         Restangular.all('onetoonelanguages?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (response) {
            $scope.onetoonelanguages = response;
             
             Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (reslang) {
                $scope.Dislanguages = reslang;

            angular.forEach($scope.onetoonelanguages, function (member, index) {
                member.index = index + 1;

                 for (var a = 0; a < $scope.Dislanguages.length; a++) {
                    if (member.language == $scope.Dislanguages[a].id) {
                        member.languageName = $scope.Dislanguages[a].name;
                        break;
                    }
                }
                // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
            });
         });
        });
         $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().$object;

        
    } else{
        
         Restangular.all('onetoonelanguages?filter[where][deleteflag]=false').getList().then(function (response) {
            $scope.onetoonelanguages = response;
             
             Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (reslang) {
                $scope.Dislanguages = reslang;

            angular.forEach($scope.onetoonelanguages, function (member, index) {
                member.index = index + 1;

                 for (var a = 0; a < $scope.Dislanguages.length; a++) {
                    if (member.language == $scope.Dislanguages[a].id) {
                        member.languageName = $scope.Dislanguages[a].name;
                        break;
                    }
                }
                // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
            });
         });
        });
         $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
        
    }
    
    ///

    
    /*************************************** save & update ************************************/
    
   //  $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        $scope.$watch('onetonelist.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id) {
                return;
            } else {

                Restangular.one('onetoonelanguages?filter[where][language]=' + 1 + '&filter[where][deleteflag]=false').get().then(function (memblanguage) {
                    console.log('memblanguage', memblanguage);
                    $scope.original = memblanguage[0];
                    //$scope.memblanguage.language = newValue;
                    delete $scope.original['language'];
                    delete $scope.original['id'];
                    $scope.original.language = newValue;
                    $scope.onetonelist = Restangular.copy($scope.original);
                });
                
                Restangular.all('onetoonelanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    console.log('response', response);
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                       $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update'; 

                    }
                });

            }
        });
    
        $scope.OKBUTTON = function () {
            $scope.toggleCheck();
             window.location = '/LangOnetooneList';
           
        };
        $scope.onetonelist = {
            deleteflag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId
        };

        $scope.validatestring = '';
        $scope.Save = function () {
            /*Restangular.all('conditionLanguages').post($scope.condition).then(function (conResponse) {
                console.log('conResponse', conResponse);
                window.location = '/LangOnetooneList';
            });*/
            if ($scope.onetonelist.language == '' || $scope.onetonelist.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            }   if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                Restangular.all('onetoonelanguages').post($scope.onetonelist).then(function (otoResponse) {
                console.log('otoResponse', otoResponse);
                     $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                        window.location = '/LangOnetooneList';
                    }, 350);
            });
            }

        };
    $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

    $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

        $scope.Update = function () {
            Restangular.one('onetoonelanguages', $routeParams.id).customPUT($scope.onetonelist).then(function (otoResponse) {
                console.log('otoResponse', otoResponse);
               $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                        window.location = '/LangOnetooneList';
                    }, 350);
            });
        };

        if ($routeParams.id) {
            $scope.HideCreateButton = false;
            $scope.langdisable = true;
             $scope.message = 'One To One Langauge has been Updated!';
            Restangular.one('onetoonelanguages', $routeParams.id).get().then(function (onetonelist) {
//                  Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (znes) {
//                    $scope.languages = znes;
//                    $scope.onetonelist.language = onetonelist.language;
                $scope.original = onetonelist;
                $scope.onetonelist = Restangular.copy($scope.original);
           // });
            });
        } else {
            $scope.message = 'One To One Langauge has been Created!';
        }
    
      /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /************************************************/

    
});