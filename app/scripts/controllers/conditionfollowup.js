'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionFollowupCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {


        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/conditionfollowup/create' || $location.path() === '/conditionfollowup/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/conditionfollowup/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/conditionfollowup/create' || $location.path() === '/conditionfollowup/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/conditionfollowup/create' || $location.path() === '/conditionfollowup/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/conditionfollowup-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;




        $scope.$watch('conditionfollowup.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {

                    $scope.conditionfollowup.orderNo = '';
                    $scope.conditionfollowup.name = '';
                    $scope.conditionfollowup.parentId = '';
                    $scope.conditionfollowup.followUpDays = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;

                }

            }
        });



        $scope.$watch('conditionfollowup.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('conditionfollowups', newValue).get().then(function (zn) {
                    $scope.conditionfollowup.orderNo = zn.orderNo;
                    $scope.conditionfollowup.followUpDays = zn.followUpDays;
                });
            }
        });

        /*****************************************************************************/

        if ($routeParams.id) {
            //$scope.OtherLang = true;
            $scope.message = 'Followup has been Updated!';
            Restangular.one('conditionfollowups', $routeParams.id).get().then(function (conditionfollowup) {
                $scope.original = conditionfollowup;
                $scope.conditionfollowup = Restangular.copy($scope.original);
                $scope.condiionoldvalue = $scope.conditionfollowup.orderNo;
            });
            Restangular.all('conditionfollowups?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;

            });
        } else {
            $scope.message = 'Followup has been Created!';
        }

        //$scope.Search = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /******************************** INDEX *******************************************/

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('conditionfollowups?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {

                $scope.conditionfollowups = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditionfollowups, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.condifollowlanguages = sev;
            });

        } else {

            Restangular.all('conditionfollowups').getList().then(function (mt) {
                $scope.conditionfollowups = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditionfollowups, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('conditionfollowups?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condifollowlanguages = sev;

                        angular.forEach($scope.condifollowlanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condifollowlanguages = sev;
                    });
                }
            });


        }


        Restangular.all('conditionfollowups?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.conditionfollowdisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.conditionfollowdisply.filter(function (arr) {
                    return arr.orderNo == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.conditionfollowup.language != '') {
                    $scope.conditionfollowup.orderNo = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };





        /********************************************* SAVE *******************************************/
        $scope.conditionfollowup = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('days').style.borderColor = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditionfollowup.language == '' || $scope.conditionfollowup.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionfollowup.language == 1) {
                if ($scope.conditionfollowup.name == '' || $scope.conditionfollowup.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Followup';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.conditionfollowup.followUpDays == '' || $scope.conditionfollowup.followUpDays == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Followup Days';
                    document.getElementById('days').style.borderColor = "#FF0000";

                } else if ($scope.conditionfollowup.orderNo == '' || $scope.conditionfollowup.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }
            } else if ($scope.conditionfollowup.language != 1) {
                if ($scope.conditionfollowup.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition Status in English';

                } else if ($scope.conditionfollowup.name == '' || $scope.conditionfollowup.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Followup';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.conditionfollowup.followUpDays == '' || $scope.conditionfollowup.followUpDays == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Followup Days';
                    document.getElementById('days').style.borderColor = "#FF0000";

                } else if ($scope.conditionfollowup.orderNo == '' || $scope.conditionfollowup.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }
            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.conditionfollowup.parentId === '') {
                    delete $scope.conditionfollowup['parentId'];
                }
                $scope.submitDisable = true;
                $scope.conditionfollowup.parentFlag = $scope.showenglishLang;
                $scope.conditionfollowups.post($scope.conditionfollowup).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/conditionfollowup-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'conditionfollowup_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                })


            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('days').style.borderColor = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditionfollowup.language == '' || $scope.conditionfollowup.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionfollowup.name == '' || $scope.conditionfollowup.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Followup';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditionfollowup.followUpDays == '' || $scope.conditionfollowup.followUpDays == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Followup Days';
                document.getElementById('days').style.borderColor = "#FF0000";

            } else if ($scope.conditionfollowup.orderNo == '' || $scope.conditionfollowup.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.conditionfollowup.parentId === '') {
                    delete $scope.conditionfollowup['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('conditionfollowups', $routeParams.id).customPUT($scope.conditionfollowup).then(function (updateResp) {
                    //$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('conditionfollowup  Saved');
                    $scope.MandatoryUpdate(updateResp);

                }, function (error) {
                    if (error.data.error.constraint === 'conditionfollowup_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };



        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderNo = myResponse.orderNo;

                $scope.updateallRecord.followUpDays = myResponse.followUpDays;
                Restangular.one('conditionfollowups', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                    window.location = '/conditionfollowup-list';
                }, 350);
            }
        };







        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('conditionfollowups/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('conditionfollowups/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


    });
