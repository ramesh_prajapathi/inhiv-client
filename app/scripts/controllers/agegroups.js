'use strict';

angular.module('secondarySalesApp')

	.controller('AgeGroupCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/

		$scope.showForm = function () {
			var visible = $location.path() === '/agegroup/create' || $location.path() === '/agegroup/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/agegroup/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/agegroup/create' || $location.path() === '/agegroup/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/agegroup/create' || $location.path() === '/agegroup/' + $routeParams.id;
			return visible;
		};


		/*********/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		if ($window.sessionStorage.prviousLocation != "partials/agegroups") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		/***************************************/

		if ($routeParams.id) {
			$scope.message = 'Age Group has been Updated!';
			Restangular.one('agegroups', $routeParams.id).get().then(function (agegroup) {
				$scope.original = agegroup;
				$scope.agegroup = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Age Group has been Created!';
		}
		$scope.Search = '';
		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/***************************** INDEX *******************************************/
		$scope.zn = Restangular.all('agegroups?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.agegroups = zn;
			angular.forEach($scope.agegroups, function (member, index) {
				member.index = index + 1;
			});
		});

		/************************************ SAVE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.agegroup = {
			"name": '',
			"deleteflag": false
		};
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.agegroup.name == '' || $scope.agegroup.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Age Group';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitdataModal = !$scope.submitdataModal;
				$scope.submitDisable = true;
				Restangular.all('agegroups').post($scope.agegroup).then(function () {
					window.location = '/agegroup';
				});
			}
		};
		/******************************* UPDATE *******************************************/
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.agegroup.name == '' || $scope.agegroup.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Age Group';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitdataModal = !$scope.submitdataModal;
				$scope.submitDisable = true;
				Restangular.one('agegroups', $routeParams.id).customPUT($scope.agegroup).then(function () {
					window.location = '/agegroup';
				});
			}
		};
		/********************************* DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('agegroups/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
