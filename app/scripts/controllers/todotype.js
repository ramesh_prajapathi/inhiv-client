'use strict';

angular.module('secondarySalesApp')

    .controller('ToDoTypesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/todotypes/create' || $location.path() === '/todotypes/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todotypes/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todotypes/create' || $location.path() === '/todotypes/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todotypes/create' || $location.path() === '/todotypes/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        if ($window.sessionStorage.prviousLocation != "partials/todotype") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        /****************************************************************/
        $scope.showenglishLang = true;

        $scope.$watch('todotype.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    console.log("iim in if route");
                    //$scope.todotype.orderNo = '';
                    //$scope.todotype.name = '';
                    $scope.todotype.parentId = '';
                    $scope.todotype.dueInDay = '';
                    $scope.todotype.type = '';
                    $scope.todotype.messagetype = '';
                }
                if (newValue + "" != "1") {
                    console.log("iim in if ");
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;
                    //$scope.disabledtype = true;
                    $scope.todotype.orderNo = '';
                    $scope.todotype.name = '';
                   
                } else {
                    $scope.todotype.orderNo = '';
                    $scope.todotype.name = '';
                    console.log("iim in else");
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;
                    $scope.disabledtype = false;

                }

            }
        });
        $scope.$watch('todotype.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                
                return;
            } else {
                $scope.disableorder = true;
                 $scope.disabledtype = true;
                 $scope.MsgShow = true;
                 
                Restangular.one('todotypes', newValue).get().then(function (zn) {
                    $scope.todotype.orderNo = zn.orderNo;
                    $scope.todotype.dueInDay = zn.dueInDay;
                    $scope.todotype.type = zn.type;
                    $scope.todotype.messagetype = zn.messagetype;
                    
                });
            }
        });



    
    /****************new chngaes******************/
    $scope.chnageType = function(type) {
     if (type == 'Todo') {
                $scope.todotype.name = '';
                $scope.todotype.orderNo = ''; 
                $scope.todotype.dueInDay = '';
                console.log("im in if Todo");
                $scope.DueShow = true;
                $scope.MsgShow = false;
                
               
                 Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=1' + '&filter[where][type]=Todo').getList().then(function (zn) {
            $scope.todotypedisplyfilter = zn;
        }); 
            }
        
            else if (type == 'Message') {
             console.log("im in elseif Message now");
                 $scope.todotype.name = '';
                $scope.todotype.orderNo = ''; 
                 $scope.todotype.messagetype = '';
                $scope.MsgShow = true;
                $scope.DueShow = false;
                $scope.todotype.dueInDay = 'NA';
                $scope.Flag = true;
                
                 Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=1'  + '&filter[where][type]=Message').getList().then(function (zn) {
                $scope.todotypedisplyfilter = zn;
        });
            }
         
         else {  
              $scope.todotype.messagetype = '';
             $scope.todotype.dueInDay = '';
             $scope.Flag = true;
             $scope.todotype.name = '';
             $scope.todotype.orderNo = '';
                }
};
    /****************new chngaes******************/


        

        if ($routeParams.id) {
            $scope.disabledtype = true;
            $scope.message = 'Todo type has been Updated!';
            Restangular.one('todotypes', $routeParams.id).get().then(function (todotype) {
                $scope.original = todotype;
                $scope.todotype = Restangular.copy($scope.original);
                 $scope.condiionoldvalue = $scope.todotype.orderNo;
            });
            Restangular.all('todotypes?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;

            });
        } else {
            $scope.disabledtype = false;
            $scope.message = 'Todo type has been created!';
        }
    
    //new changes for search box
    $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name','dueInDay'];
        $scope.searchToDoType = '';
       
   

        /************************* INDEX *******************************************/

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('todotypes?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.todotypes = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.todotypes, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.todotypelanguages = sev;
            });

        } else {

            Restangular.all('todotypes').getList().then(function (mt) {
                $scope.todotypes = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.todotypes, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });


            
            
             Restangular.all('todotypes?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.todotypelanguages = sev;
           
                      angular.forEach($scope.todotypelanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                          console.log('member', member);
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.todotypelanguages = sev;
                       });
                  }
              });
        }


        Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.todotypedisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };
    
    var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.todotypedisplyfilter.filter(function (arr) {
                    return arr.orderNo == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.todotype.language != '') {
                    $scope.todotype.orderNo = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };




        /************************** SAVE *******************************************/
        $scope.todotype = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.SaveToDoType = function () {
           // document.getElementById('type').style.border = "";
            document.getElementById('name').style.border = "";

            document.getElementById('order').style.border = "";
            document.getElementById('due').style.border = "";

            if ($scope.todotype.language == '' || $scope.todotype.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.todotype.language == 1) {
                if ($scope.todotype.type == '' || $scope.todotype.type == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Type';
                   // document.getElementById('type').style.borderColor = "#FF0000";

                } else if ($scope.todotype.name == '' || $scope.todotype.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Value';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.todotype.orderNo == '' || $scope.todotype.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.todotype.dueInDay == '' || $scope.todotype.dueInDay == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Due In days';
                    document.getElementById('due').style.borderColor = "#FF0000";

                }
            } else if ($scope.todotype.language != 1) {
                if ($scope.todotype.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select ToDo Type in English';

                } else if ($scope.todotype.type == '' || $scope.todotype.type == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Type';
                    document.getElementById('type').style.borderColor = "#FF0000";

                } else if ($scope.todotype.name == '' || $scope.todotype.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Value';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.todotype.orderNo == '' || $scope.todotype.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.todotype.dueInDay == '' || $scope.todotype.dueInDay == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Due In days';
                    document.getElementById('due').style.borderColor = "#FF0000";

                }
            }

            /* else if ($scope.todotype.pillarid == '' || $scope.todotype.pillarid == null) {
				$scope.validatestring = $scope.validatestring + 'Please select a pillar';

			} */

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                

                if ($scope.todotype.parentId === '') {
                    delete $scope.todotype['parentId'];
                }
                
                if($scope.Flag == true && ($scope.todotype.messagetype == '' || $scope.todotype.messagetype == null)){
                    $scope.todotype.messagetype = 'M';
                }
                $scope.submitDisable = true;
                $scope.todotype.parentFlag = $scope.showenglishLang;
                $scope.todotypes.post($scope.todotype).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/todotypes';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'todotype_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                })
            }
        };
        /************************ UPDATE *******************************************/


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };



        /**************************Updating all palce flag value**************************/

   


        /************************* DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('todotypes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }


        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('todotypes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };



    });
