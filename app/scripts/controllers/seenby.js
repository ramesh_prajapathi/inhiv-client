'use strict';

angular.module('secondarySalesApp')
    .controller('seenByCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }


        $scope.showForm = function () {
            var visible = $location.path() === '/purposeofvisit/create' || $location.path() === '/purposeofvisit/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/purposeofvisit/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/purposeofvisit/create' || $location.path() === '/purposeofvisit/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/purposeofvisit/create' || $location.path() === '/purposeofvisit/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/purposeofvisit-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }


        /***new changes*****/
        $scope.showenglishLang = true;

        $scope.$watch('seenby.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    $scope.seenby.name = '';
                    $scope.seenby.parentId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });
        /***new changes*****/



        if ($routeParams.id) {
            $scope.message = 'Purpose of Visit has been Updated!';
            Restangular.one('seenbys', $routeParams.id).get().then(function (seenby) {
                $scope.original = seenby;
                $scope.seenby = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Purpose of Visit has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];
     $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /******************************** INDEX *******************************************/
        //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('seenbys?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.seenbys = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.seenbys, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.purposelanguages = sev;
            });

        } else {

            Restangular.all('seenbys').getList().then(function (mt) {
                $scope.seenbys = mt;
               Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.seenbys, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

           
            
             Restangular.all('seenbys?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.purposelanguages = sev;
           
                      angular.forEach($scope.purposelanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                          console.log('member', member);
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.purposelanguages = sev;
                       });
                  }
              });
        }

        /***new changes*****/

        Restangular.all('seenbys?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.purposedisply = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        /***new changes*****/




        //        Restangular.all('seenbys?filter[where][deleteflag]=false').getList().then(function (mt) {
        //            $scope.seenbys = mt;
        //            angular.forEach($scope.seenbys, function (member, index) {
        //                member.index = index + 1;
        //
        //                //                Restangular.one('languages', member.language).get().then(function (lng) {
        //                //                    member.langname = lng.name;
        //                //                });
        //            });
        //        });
        //
        //
        //        /***new changes*****/
        //        Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
        //            $scope.purposelanguages = sev;
        //        });
        //
        //        Restangular.all('seenbys?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
        //            $scope.purposedisply = zn;
        //        });
        //
        //        $scope.getLanguage = function (languageId) {
        //            return Restangular.one('languages', languageId).get().$object;
        //        };
        //
        //        /***new changes*****/
        //

        /********************************************* SAVE *******************************************/
        $scope.seenby = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            //            if ($scope.seenby.language == '' || $scope.seenby.language == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Select Language';
            //                //  document.getElementById('language').style.borderColor = "#FF0000";
            //
            //            }
            //            else if ($scope.seenby.name == '' || $scope.seenby.name == null) {
            //                $scope.validatestring = $scope.validatestring + ' Please Enter Name of seenby';
            //                document.getElementById('name').style.borderColor = "#FF0000";
            //
            //            }
            //            

            if ($scope.seenby.language == '' || $scope.seenby.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.seenby.language == 1) {
                if ($scope.seenby.name == '' || $scope.seenby.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Purpose of visit';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            } else if ($scope.seenby.language != 1) {
                if ($scope.seenby.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Purpose of visit in English';

                } else if ($scope.seenby.name == '' || $scope.seenby.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Purpose of visit';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                //                if ($scope.seenby.parentId === '') {
                //                    delete $scope.seenby['parentId'];
                //                }
                //                $scope.submitDisable = true;
                //                $scope.seenby.parentFlag = $scope.showenglishLang;
                //                $scope.seenbys.post($scope.seenby).then(function () {
                //                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                    window.location = '/seenby-list';
                //                }, function (error) {
                //                    if (error.data.error.constraint === 'seenby_lang_parenrid') {
                //                        alert('Value already exists for this language');
                //                    }
                //                });

                //                $scope.submitDisable = true;
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                $scope.seenbys.post($scope.seenby).then(function () {
                //                    window.location = '/purposeofvisit-list';
                //                });

                if ($scope.seenby.parentId === '') {
                    delete $scope.seenby['parentId'];
                }
                $scope.submitDisable = true;
                $scope.seenby.parentFlag = $scope.showenglishLang;
                $scope.seenbys.post($scope.seenby).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/purposeofvisit-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'seenby_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });


            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
             document.getElementById('name').style.border = "";

            //            if ($scope.seenby.language == '' || $scope.seenby.language == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Select Language';
            //                //  document.getElementById('language').style.borderColor = "#FF0000";
            //
            //            }
            //            else if ($scope.seenby.name == '' || $scope.seenby.name == null) {
            //                $scope.validatestring = $scope.validatestring + ' Please Enter Name of seenby';
            //                document.getElementById('name').style.borderColor = "#FF0000";
            //
            //            }
            //            

            if ($scope.seenby.language == '' || $scope.seenby.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.seenby.language == 1) {
                if ($scope.seenby.name == '' || $scope.seenby.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Purpose of visit';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            } else if ($scope.seenby.language != 1) {
                if ($scope.seenby.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Purpose of visit in English';

                } else if ($scope.seenby.name == '' || $scope.seenby.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Purpose of visit';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                //                if ($scope.severityofincident.parentId === '') {
                //                    delete $scope.severityofincident['parentId'];
                //                }
                //                $scope.submitDisable = true;
                //                Restangular.one('seenbys', $routeParams.id).customPUT($scope.seenby).then(function () {
                //                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                    console.log('Seenby  Saved');
                //                    window.location = '/seenby-list';
                //                }, function (error) {
                //                    if (error.data.error.constraint === 'seenby_lang_parenrid') {
                //                        alert('Value already exists for this language');
                //                    }
                //                });

                //                $scope.submitDisable = true;
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                Restangular.one('seenbys', $routeParams.id).customPUT($scope.seenby).then(function () {
                //                    console.log('Step Saved');
                //                    window.location = '/purposeofvisit-list';
                //
                //
                //                });

                if ($scope.seenby.parentId === '') {
                    delete $scope.seenby['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('seenbys', $routeParams.id).customPUT($scope.seenby).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('seenby Name Saved');
                    setTimeout(function () {
                        window.location = '/purposeofvisit-list';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'seenby_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });


            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('seenbys/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        
         /***************** Archive *****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('seenbys/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };
    });
