

//angular.module('main', ['ngAnimate', 'toaster'])

///.controller('myController', function($scope, toaster, $window) {
 'use strict';
angular.module('secondarySalesApp')
	.controller('toastdemoCtrl', function ($scope, toaster, $window) {   
    $scope.pop = function(){
        toaster.pop('success', "title", '<ul><li>Render html1</li></ul>', 5000, 'trustedHtml');
        toaster.pop('error', "title", '<ul><li>Render html2</li></ul>', null, 'trustedHtml');
        toaster.pop('warning', "title", null, null, 'template');
        toaster.pop('note', "title", "text");
    };
});