'use strict';

angular.module('secondarySalesApp')
	.controller('EmployeesCtrl', function ($scope, Restangular, $window, $route, $routeParams) {

		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
			$window.sessionStorage.facility_zoneId = null;
			$window.sessionStorage.facility_stateId = null;
			$window.sessionStorage.facility_currentPage = 1;
			$window.sessionStorage.facility_currentPageSize = 25;
		} else {
			$scope.countryId = $window.sessionStorage.facility_zoneId;
			$scope.stateId = $window.sessionStorage.facility_stateId;
			$scope.currentpage = $window.sessionStorage.facility_currentPage;
			$scope.pageSize = $window.sessionStorage.facility_currentPageSize;
		}


		if ($window.sessionStorage.prviousLocation != "partials/employees-form") {
			$window.sessionStorage.facility_zoneId = '';
			$window.sessionStorage.facility_stateId = '';
			$window.sessionStorage.facility_currentPage = 1;
			$scope.currentpage = 1;
			$window.sessionStorage.facility_currentPageSize = 25;
			$scope.pageSize = 25;
		}

		$scope.pageSize = $window.sessionStorage.facility_currentPageSize;
		$scope.pageFunction = function (mypage) {
			//console.log('mypage', mypage);
			$scope.pageSize = mypage;
			$window.sessionStorage.facility_currentPageSize = mypage;
		};

		//$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
		$scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
			$scope.zones = znes;
			$scope.countryId = $window.sessionStorage.facility_zoneId;
		});

		$scope.searchEmp = $scope.firstName;

		/************************** DELETE *******************************************/


		/*********************************** DELETE ********************************/
		$scope.Delete = function (id) {
			$scope.toggleLoading();
			$scope.deleteCount = 0;
			$scope.deleteCount1 = 0;
			$scope.deleteCount2 = 0;
			$scope.deleteCount3 = 0;
			$scope.deleteCount4 = 0;
			$scope.deleteCount5 = 0;
			$scope.deleteReportCount = 0;
			$scope.deleteGroupMeetingCount = 0;
			$scope.deleteTIFacilityCount = 0;
			$scope.deleteNonTIFacilityCount = 0;
			$scope.item = [{
				partiallyarchivedflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
        }]
			Restangular.one('employees/' + id).customPUT($scope.item[0]).then(function () {
				Restangular.all('comembers?filter[where][facility]=' + id + '&filter[where][deleteflag]=false').getList().then(function (tds) {
					if (tds.length > 0) {
						$scope.COMembersToDelete = tds;
						$scope.deleteCoMember(id);
					} else {
						Restangular.all('distribution-routes?filter[where][partnerId]=' + id + '&filter[where][deleteflag]=false').getList().then(function (distribution) {
							if (distribution.length > 0) {
								$scope.distributionRoutesDelete = distribution;
								$scope.deleteRoutes(id);
							} else {
								Restangular.all('fieldworkers?filter[where][facility]=' + id + '&filter[where][deleteflag]=false').getList().then(function (fworker) {
									if (fworker.length > 0) {
										$scope.fieldWorkersToDelete = fworker;
										$scope.deleteFWs(id);
									} else {
										Restangular.all('routelinks?filter[where][facility]=' + id).getList().then(function (rlink) {
											if (rlink.length > 0) {
												$scope.routelinksToDelete = rlink;
												$scope.deleteRouteLinks(id);
											} else {
												Restangular.all('users?filter[where][coorgId]=' + id + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
													if (rprtincdnts.length > 0) {
														$scope.usersToDelete = rprtincdnts;
														$scope.deleteUsers(id);
													} else {
														$scope.Fetchtodo(id);
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			});
		}



		$scope.deleteCoMember = function (benId) {
			//console.log('I m comembers', benId);
			$scope.itemtodo = [{
				deleteflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
    }]
			Restangular.one('comembers/' + $scope.COMembersToDelete[$scope.deleteCount].id).customPUT($scope.itemtodo[0]).then(function (delco) {
				//console.log('deletecomember', delco);
				$scope.deleteCount++;
				if ($scope.deleteCount < $scope.COMembersToDelete.length) {
					$scope.deleteCoMember(benId);
				} else {
					Restangular.all('distribution-routes?filter[where][partnerId]=' + benId + '&filter[where][deleteflag]=false').getList().then(function (distribution) {
						if (distribution.length > 0) {
							$scope.distributionRoutesDelete = distribution;
							$scope.deleteRoutes(benId);
						} else {
							Restangular.all('fieldworkers?filter[where][facility]=' + benId + '&filter[where][deleteflag]=false').getList().then(function (fworker) {
								if (fworker.length > 0) {
									$scope.fieldWorkersToDelete = fworker;
									$scope.deleteFWs(benId);
								} else {
									Restangular.all('routelinks?filter[where][facility]=' + benId).getList().then(function (rlink) {
										if (rlink.length > 0) {
											$scope.routelinksToDelete = rlink;
											$scope.deleteRouteLinks(benId);
										} else {
											Restangular.all('users?filter[where][coorgId]=' + benId + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
												if (rprtincdnts.length > 0) {
													$scope.usersToDelete = rprtincdnts;
													$scope.deleteUsers(benId);
												} else {
													$scope.Fetchtodo(benId);
												}
											});
										};
									});
								};
							});
						}
					});
				}
			});
		}

		$scope.deleteRoutes = function (benId1) {
			//console.log('I m distribution-routes');
			$scope.itemtodo1 = [{
				deleteflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
    }]
			Restangular.one('distribution-routes/' + $scope.distributionRoutesDelete[$scope.deleteCount1].id).customPUT($scope.itemtodo1[0]).then(function (delroute) {
				//console.log('deleteroutes', delroute);
				$scope.deleteCount1++;
				if ($scope.deleteCount1 < $scope.distributionRoutesDelete.length) {
					$scope.deleteRoutes(benId1);
				} else {
					Restangular.all('fieldworkers?filter[where][facility]=' + benId1 + '&filter[where][deleteflag]=false').getList().then(function (fworker) {
						if (fworker.length > 0) {
							$scope.fieldWorkersToDelete = fworker;
							$scope.deleteFWs(benId1);
						} else {
							Restangular.all('routelinks?filter[where][facility]=' + benId1).getList().then(function (rlink) {
								if (rlink.length > 0) {
									$scope.routelinksToDelete = rlink;
									$scope.deleteRouteLinks(benId1);
								} else {
									Restangular.all('users?filter[where][coorgId]=' + benId1 + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
										if (rprtincdnts.length > 0) {
											$scope.usersToDelete = rprtincdnts;
											$scope.deleteUsers(benId1);
										} else {
											$scope.Fetchtodo(benId1);
										}
									});
								}
							});
						}
					});
				}
			});
		}

		$scope.deleteFWs = function (benId2) {
			//console.log('I m fieldworkers');
			$scope.itemtodo2 = [{
				deleteflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date(),
				sitesassigned: null
    }]
			Restangular.one('fieldworkers/' + $scope.fieldWorkersToDelete[$scope.deleteCount2].id).customPUT($scope.itemtodo2[0]).then(function (delfw) {
				//console.log('fieldworkers', delfw);
				$scope.deleteCount2++;
				if ($scope.deleteCount2 < $scope.fieldWorkersToDelete.length) {
					$scope.deleteFWs(benId2);
				} else {
					Restangular.all('routelinks?filter[where][facility]=' + benId2).getList().then(function (rlink) {
						if (rlink.length > 0) {
							$scope.routelinksToDelete = rlink;
							$scope.deleteRouteLinks(benId2);
						} else {
							Restangular.all('users?filter[where][coorgId]=' + benId2 + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
								if (rprtincdnts.length > 0) {
									$scope.usersToDelete = rprtincdnts;
									$scope.deleteUsers(benId2);
								} else {
									$scope.Fetchtodo(benId2);
								}
							});
						}
					});
				}
			});

		}

		$scope.deleteRouteLinks = function (benId4) {
			//console.log('I m routelink');
			Restangular.one('routelinks/' + $scope.routelinksToDelete[$scope.deleteCount4].id).remove().then(function (delroutelink) {
				//console.log('routelinks', delroutelink);
				$scope.deleteCount4++;
				if ($scope.deleteCount4 < $scope.routelinksToDelete.length) {
					$scope.deleteRouteLinks(benId4);
				} else {
					Restangular.all('users?filter[where][coorgId]=' + benId4 + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
						if (rprtincdnts.length > 0) {
							$scope.usersToDelete = rprtincdnts;
							$scope.deleteUsers(benId4);
						} else {
							$scope.Fetchtodo(benId4);
						}
					});
				}
			});

		}
		$scope.deleteUsers = function (benId3) {
			//console.log('I m user', benId3);
			$scope.itemtodo3 = [{
				deleteflag: 'true',
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
    }]
			Restangular.one('users/' + $scope.usersToDelete[$scope.deleteCount3].id).customPUT($scope.itemtodo3[0]).then(function (delusr) {
				//console.log('user', delusr);
				$scope.deleteCount3++;
				if ($scope.deleteCount3 < $scope.usersToDelete.length) {
					$scope.deleteUsers(benId3);
				} else {
					$scope.Fetchtodo(benId3);
				}
			});
		}



		$scope.Fetchtodo = function (benId5) {
			//console.log('I m calling by', benId5);
			$scope.deleteCount51 = 0;
			Restangular.all('todos?filter[where][facility]=' + benId5 + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (todores) {
				$scope.alltodo = todores;
				if (todores.length > 0) {
					$scope.mainTodosToDelete = todores;
					$scope.deleteMainTodos(benId5);
					//$scope.Fetchtodo(benId5);
				} else {
					$scope.FetchreportIncidents(benId5);

				}
			});
		}

		$scope.FetchreportIncidents = function (benId5) {
			//console.log('I m calling by', benId5);
			$scope.deletereportIncidentCount = 0;
			Restangular.all('reportincidents?filter[where][facility]=' + benId5 + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (reportres) {
				$scope.allReportIncidents = reportres;
				if (reportres.length > 0) {
					$scope.deleteReportIncident(benId5);
					//$scope.Fetchtodo(benId5);
				} else {
					$scope.FetchSchemeMasters(benId5);
				}
			});
		}




		$scope.FetchSchemeMasters = function (benId5) {
			//console.log('I m calling by', benId5);
			$scope.deleteschemeMasterCount = 0;
			Restangular.all('schememasters?filter[where][facility]=' + benId5 + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (schememasters) {
				$scope.allSchemeMasters = schememasters;
				if (schememasters.length > 0) {
					$scope.deleteSchemeMaster(benId5);
					//$scope.Fetchtodo(benId5);
				} else {
					/* $scope.itemben5 = [{
					     partiallyarchivedflag: false,
					     deleteflag: true,
					     lastmodifiedby: $window.sessionStorage.UserEmployeeId,
					     lastmodifiedtime: new Date(),
					 }]
					 Restangular.one('employees/' + benId5).customPUT($scope.itemben5[0]).then(function (respo5) {
					     $scope.modalInstanceLoad.close();
					     $route.reload();
					     console.log('employees6delete', respo5);
					     //window.location = "/employees";
					 });*/
					$scope.FetchTIFacility(benId5);
				}
			});
		}

		$scope.FetchTIFacility = function (benId5) {
			//console.log('I m TIFAcility calling by', benId5);
			$scope.deleteTIFacilityCount = 0;
			Restangular.all('tifacilities?filter[where][facilityid]=' + benId5 + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (tifacilityres) {
				$scope.allTIFacility = tifacilityres;
				if (tifacilityres.length > 0) {
					$scope.deleteTIFacility(benId5);
					//$scope.Fetchtodo(benId5);
				} else {
					$scope.FetchNonTIFacility(benId5);
				}
			});
		}

		$scope.FetchNonTIFacility = function (benId5) {
			//console.log('I m NONTIFAcility calling by', benId5);
			$scope.deleteNonTIFacilityCount = 0;
			Restangular.all('nontifacilities?filter[where][facilityid]=' + benId5 + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (nontifacilityres) {
				$scope.allNonTIFacility = nontifacilityres;
				if (nontifacilityres.length > 0) {
					$scope.deleteNonTIFacility(benId5);
					//$scope.Fetchtodo(benId5);
				} else {
					// $scope.FetchNonTIFacility(benId5);
					$scope.itemben5 = [{
						partiallyarchivedflag: false,
						deleteflag: true,
						lastmodifiedby: $window.sessionStorage.UserEmployeeId,
						lastmodifiedtime: new Date(),
						nooftis: 0,
						noofnonti: 0
                    }]
					Restangular.one('employees/' + benId5).customPUT($scope.itemben5[0]).then(function (respo5) {
						$scope.modalInstanceLoad.close();
						$route.reload();
						//console.log('employees6delete', respo5);
						//window.location = "/employees";
					});
				}
			});
		}

		$scope.deleteMainTodos = function (benId5) {
			//console.log('I m deleteMainTodos', benId5);
			$scope.itemtodo51 = [{
				deleteflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
            }]
			Restangular.one('todos/' + $scope.mainTodosToDelete[$scope.deleteCount51].id).customPUT($scope.itemtodo51[0]).then(function (deltodo) {
				//console.log('todo delete', deltodo);
				$scope.deleteCount51++;
				if ($scope.deleteCount51 < $scope.mainTodosToDelete.length) {
					$scope.deleteMainTodos(benId5);
				} else {
					$scope.Fetchtodo(benId5);
				}
			});
		}


		$scope.deleteReportIncident = function (benId5) {
			$scope.itemreportincident = [{
				deleteflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
            }]
			Restangular.one('reportincidents/' + $scope.allReportIncidents[$scope.deletereportIncidentCount].id).customPUT($scope.itemreportincident[0]).then(function (deltodo) {
				$scope.deletereportIncidentCount++;
				if ($scope.deletereportIncidentCount < $scope.allReportIncidents.length) {
					$scope.deleteReportIncident(benId5);
				} else {
					$scope.FetchreportIncidents(benId5);
				}
			});
		}


		$scope.deleteSchemeMaster = function (benId5) {
			$scope.itemschememaster = [{
				deleteflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
            }]
			Restangular.one('schememasters/' + $scope.allSchemeMasters[$scope.deleteschemeMasterCount].id).customPUT($scope.itemschememaster[0]).then(function (delscheme) {
				$scope.deleteschemeMasterCount++;
				if ($scope.deleteschemeMasterCount < $scope.allSchemeMasters.length) {
					$scope.deleteSchemeMaster(benId5);
				} else {
					$scope.FetchSchemeMasters(benId5);
				}
			});
		}

		$scope.deleteTIFacility = function (benId5) {
				$scope.itemtifacility = [{
					deleteflag: true,
					lastmodifiedby: $window.sessionStorage.UserEmployeeId,
					lastmodifiedtime: new Date()
            }]
				Restangular.one('tifacilities/' + $scope.allTIFacility[$scope.deleteTIFacilityCount].id).customPUT($scope.itemtifacility[0]).then(function (delti) {
					$scope.deleteTIFacilityCount++;
					if ($scope.deleteTIFacilityCount < $scope.allTIFacility.length) {
						$scope.deleteTIFacility(benId5);
					} else {
						$scope.FetchTIFacility(benId5);
					}
				});
			}
		$scope.deleteNonTIFacility = function (benId5) {
				$scope.itemnontifacility = [{
					deleteflag: true,
					lastmodifiedby: $window.sessionStorage.UserEmployeeId,
					lastmodifiedtime: new Date()
            }]
				Restangular.one('nontifacilities/' + $scope.allNonTIFacility[$scope.deleteNonTIFacilityCount].id).customPUT($scope.itemnontifacility[0]).then(function (delnonti) {
					$scope.deleteNonTIFacilityCount++;
					if ($scope.deleteNonTIFacilityCount < $scope.allNonTIFacility.length) {
						$scope.deleteNonTIFacility(benId5);
					} else {
						$scope.FetchNonTIFacility(benId5);
					}
				});
			}
			/**************************************** UNDELETE ******************************/
		$scope.UnDelete = function (id) {
			$scope.toggleLoading();
			$scope.undeleteCount = 0;
			$scope.undeleteCount1 = 0;
			$scope.undeleteCount2 = 0;
			$scope.undeleteCount3 = 0;
			$scope.undeleteCount4 = 0;
			$scope.unitem = [{
				deleteflag: false,
				partiallyunarchivedflag: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
        }]
			Restangular.one('employees/' + id).customPUT($scope.unitem[0]).then(function () {
				Restangular.all('comembers?filter[where][facility]=' + id + '&filter[where][deleteflag]=true').getList().then(function (tds) {
					if (tds.length > 0) {
						$scope.untodosToDelete = tds;
						$scope.undeleteTodo(id);
					} else {
						Restangular.all('distribution-routes?filter[where][partnerId]=' + id + '&filter[where][deleteflag]=true').getList().then(function (distribution) {
							if (distribution.length > 0) {
								$scope.undistributionRoutesDelete = distribution;
								$scope.undeleteRoutes(id);
							} else {
								Restangular.all('fieldworkers?filter[where][facility]=' + id + '&filter[where][deleteflag]=true').getList().then(function (fworker) {
									if (fworker.length > 0) {
										$scope.unfieldWorkersToDelete = fworker;
										$scope.undeleteFWs(id);
									} else {
										Restangular.all('users?filter[where][coorgId]=' + id + '&filter[where][deleteflag]=true').getList().then(function (rprtincdnts) {
											if (rprtincdnts.length > 0) {
												$scope.unusersToDelete = rprtincdnts;
												$scope.undeleteUsers(id);
											} else {
												$scope.unitemben3 = [{
													partiallyunarchivedflag: false,
													deleteflag: false,
													lastmodifiedby: $window.sessionStorage.UserEmployeeId,
													lastmodifiedtime: new Date(),
   }]
												Restangular.one('employees/' + id).customPUT($scope.unitemben3[0]).then(function (respo3) {
													$scope.modalInstanceLoad.close();
													console.log('All Delete', respo3);
													window.location = "/employees";
												});

											}
										});
									}
								});
							}
						});
					}
				});
			});
		}




		$scope.undeleteTodo = function (benId) {
			$scope.unditemtodo = [{
				deleteflag: false,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
    }]
			Restangular.one('comembers/' + $scope.untodosToDelete[$scope.undeleteCount].id).customPUT($scope.unditemtodo[0]).then(function (delco) {
				//console.log('delete comember', delco);
				$scope.undeleteCount++;
				if ($scope.undeleteCount < $scope.untodosToDelete.length) {
					$scope.undeleteTodo(benId);
				} else {
					Restangular.all('distribution-routes?filter[where][partnerId]=' + benId + '&filter[where][deleteflag]=true').getList().then(function (distribution) {
						if (distribution.length > 0) {
							$scope.undistributionRoutesDelete = distribution;
							$scope.undeleteRoutes(benId);
						} else {
							Restangular.all('fieldworkers?filter[where][facility]=' + benId + '&filter[where][deleteflag]=true').getList().then(function (fworker) {
								if (fworker.length > 0) {
									$scope.unfieldWorkersToDelete = fworker;
									$scope.undeleteFWs(benId);
								} else {
									Restangular.all('users?filter[where][coorgId]=' + benId + '&filter[where][deleteflag]=true').getList().then(function (rprtincdnts) {
										if (rprtincdnts.length > 0) {
											$scope.unusersToDelete = rprtincdnts;
											$scope.undeleteUsers(id);
										} else {
											$scope.unitemben3 = [{
												partiallyunarchivedflag: false,
												deleteflag: false,
												lastmodifiedby: $window.sessionStorage.UserEmployeeId,
												lastmodifiedtime: new Date(),
   }]
											Restangular.one('employees/' + benId3).customPUT($scope.unitemben3[0]).then(function (respo3) {
												$scope.modalInstanceLoad.close();
												console.log('All Delete', respo3);
												window.location = "/employees";
											});
										}
									});
								}
							});
						}
					});
				};

			});
		}

		$scope.undeleteRoutes = function (benId1) {
			//console.log('I m distribution-routes', benId1);
			$scope.unitemtodo1 = [{
				deleteflag: false,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
    }]
			Restangular.one('distribution-routes/' + $scope.undistributionRoutesDelete[$scope.undeleteCount1].id).customPUT($scope.unitemtodo1[0]).then(function (delroute) {
				//console.log('distribution-routes', delroute);
				$scope.undeleteCount1++;
				if ($scope.undeleteCount1 < $scope.undistributionRoutesDelete.length) {
					$scope.undeleteRoutes(benId1);
				} else {
					Restangular.all('fieldworkers?filter[where][facility]=' + benId1 + '&filter[where][deleteflag]=true').getList().then(function (fworker) {
						if (fworker.length > 0) {
							$scope.unfieldWorkersToDelete = fworker;
							$scope.undeleteFWs(benId1);
						} else {
							Restangular.all('users?filter[where][coorgId]=' + benId1 + '&filter[where][deleteflag]=true').getList().then(function (rprtincdnts) {
								if (rprtincdnts.length > 0) {
									$scope.unusersToDelete = rprtincdnts;
									$scope.undeleteUsers(benId1);
								} else {
									$scope.unitemben3 = [{
										partiallyunarchivedflag: false,
										deleteflag: false,
										lastmodifiedby: $window.sessionStorage.UserEmployeeId,
										lastmodifiedtime: new Date(),
   }]
									Restangular.one('employees/' + benId3).customPUT($scope.unitemben3[0]).then(function (respo3) {
										$scope.modalInstanceLoad.close();
										console.log('All Delete', respo3);
										window.location = "/employees";
									});

								}
							});
						}
					});
				}
			});
		}


		$scope.undeleteFWs = function (benId2) {
			//console.log('I m fieldworkers');
			$scope.unitemtodo2 = [{
				deleteflag: false,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date()
    }]
			Restangular.one('fieldworkers/' + $scope.unfieldWorkersToDelete[$scope.undeleteCount2].id).customPUT($scope.unitemtodo2[0]).then(function (delfw) {
				//console.log('fieldworkers', delfw);
				$scope.undeleteCount2++;
				if ($scope.undeleteCount2 < $scope.unfieldWorkersToDelete.length) {
					$scope.undeleteFWs(benId2);
				} else {
					Restangular.all('users?filter[where][coorgId]=' + benId2 + '&filter[where][deleteflag]=true').getList().then(function (rprtincdnts) {
						if (rprtincdnts.length > 0) {
							$scope.unusersToDelete = rprtincdnts;
							$scope.undeleteUsers(benId2);
						} else {
							$scope.unitemben3 = [{
								partiallyunarchivedflag: false,
								deleteflag: false,
								lastmodifiedby: $window.sessionStorage.UserEmployeeId,
								lastmodifiedtime: new Date(),
   }]
							Restangular.one('employees/' + benId3).customPUT($scope.unitemben3[0]).then(function (respo3) {
								$scope.modalInstanceLoad.close();
								//console.log('All Delete', respo3);
								window.location = "/employees";
							});
						}
					});
				}
			});
		}

		$scope.undeleteUsers = function (benId3) {
				$scope.unitemtodo33 = [{
					deleteflag: 'false',
					lastmodifiedby: $window.sessionStorage.UserEmployeeId,
					lastmodifiedtime: new Date()
    }]
				Restangular.one('users/' + $scope.unusersToDelete[$scope.undeleteCount3].id).customPUT($scope.unitemtodo33[0]).then(function (delusr) {
					//console.log('user', delusr);
					$scope.undeleteCount3++;
					if ($scope.undeleteCount3 < $scope.unusersToDelete.length) {
						$scope.undeleteUsers(benId3);
					} else {
						$scope.unitemben3 = [{
							partiallyunarchivedflag: false,
							deleteflag: false,
							lastmodifiedby: $window.sessionStorage.UserEmployeeId,
							lastmodifiedtime: new Date(),
   }]
						Restangular.one('employees/' + benId3).customPUT($scope.unitemben3[0]).then(function (respo3) {
							$scope.modalInstanceLoad.close();
							//console.log('All Delete', respo3);
							window.location = "/employees";
						});
					}
				});
			}
			/**************************************CALLING FUNCTION**********************/
		$scope.countryId = '';
		$scope.stateId = '';
		$scope.statesid = '';
		$scope.countiesid = '';

		$scope.$watch('countryId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$window.sessionStorage.facility_zoneId = newValue;
				$scope.displaysalesareas12 = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceSt) {
					$scope.displaysalesareas = responceSt;
					$scope.stateId = $window.sessionStorage.facility_stateId;
				});

				$scope.employees1 = Restangular.all('employees?filter[where][stateId]=' + newValue).getList().then(function (empRes) {
					$scope.employees = empRes;
					angular.forEach($scope.employees, function (member, index) {
						member.index = index + 1;
						if (member.partiallyarchivedflag == true || member.partiallyunarchivedflag == true) {
							member.backgroundColor = "#C7196E"
						} else if (member.deleteflag == true) {
							member.backgroundColor = "#ff0000"
						} else {
							member.backgroundColor = "#000000"
						}
					});
				});
				$scope.countiesid = +newValue;
			}
		});

		$scope.$watch('stateId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$window.sessionStorage.facility_stateId = newValue;
				$scope.employees1 = Restangular.all('employees?filter[where][district]=' + newValue + '&filter[where][stateId]=' + $scope.countiesid).getList().then(function (empRes) {
					$scope.employees = empRes;
					angular.forEach($scope.employees, function (member, index) {
						member.index = index + 1;
					});
				});
			}
		});
		$scope.employees = [];
		$scope.currentpage = $window.sessionStorage.facility_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.facility_currentPage = newPage;
		};

	});
