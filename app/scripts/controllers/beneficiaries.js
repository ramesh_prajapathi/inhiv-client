'use strict';

angular.module('secondarySalesApp')

    .controller('BnCtrl', function ($scope, Restangular, $route, $window, $filter, $modal, $rootScope) {

            $window.sessionStorage.MemberFilter = '';
            $rootScope.clickFW();

            Restangular.one('memberlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
                $scope.memberLang = langResponse[0];
                $scope.headingName = $scope.memberLang.houseHoldCreate;
               // $scope.modalTitle = $scope.memberLang.thankYou;
                 $scope.printcancel = $scope.memberLang.cancel;
                $scope.okbutton = $scope.memberLang.ok;
                $scope.message = $scope.memberLang.thankYouCreated;
                $scope.activeDis = $scope.memberLang.activemember;
                $scope.archiveDis = $scope.memberLang.paidmember;
               
                 $scope.status = 'active';
            });
            /*********************************** Pagination *******************************************/

            if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
                $window.sessionStorage.myRoute = null;
                $window.sessionStorage.myRoute_currentPage = 1;
                $window.sessionStorage.myRoute_currentPagesize = 25;
            }

            if ($window.sessionStorage.prviousLocation != "partials/beneficiaries" || $window.sessionStorage.prviousLocation != "partials/beneficiaries-form") {
                $window.sessionStorage.myRoute_currentPage = 1;
                $window.sessionStorage.myRoute_currentPagesize = 25;
            }

            $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
            $scope.PageChanged = function (newPage, oldPage) {
                $scope.currentpage = newPage;
                $window.sessionStorage.myRoute_currentPage = newPage;
            };

            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.pageFunction = function (mypage) {
                $scope.pageSize = mypage;
                $window.sessionStorage.myRoute_currentPagesize = mypage;
            };
            /*************************************************************************************************/
            $scope.auditlog = {
                modifiedbyroleid: $window.sessionStorage.roleId,
                modifiedby: $window.sessionStorage.UserEmployeeId,
                lastmodifiedtime: new Date(),
                entityroleid: 49,
                state: $window.sessionStorage.zoneId,
                district: $window.sessionStorage.salesAreaId,
                facility: $window.sessionStorage.coorgId,
                lastmodifiedby: $window.sessionStorage.UserEmployeeId,

            };
    
    /********** newChnages **********/
     Restangular.one('roles', $window.sessionStorage.roleId).get().then(function (respRole) {
                $scope.roles = respRole;
                //console.log('$scope.roles', respRole.enableMember);
     });
    /******new changes for export data*******/
         $scope.hidePrevData = true;
            $scope.roledsply = Restangular.all('roles',$window.sessionStorage.roleId).getList().$object;
    
            $scope.usersdisplay = Restangular.all('users?filter[where][deleteflag]=false').getList().$object;
    
            $scope.towns = Restangular.all('cities?filter[where][deleteflag]=false').getList().$object;

            $scope.typologies = Restangular.all('typologies?filter[where][deleteflag]=false').getList().$object;

            $scope.sites = Restangular.all('distribution-routes?filter[where][deleteflag]=false').getList().$object;

            if ($window.sessionStorage.roleId == 2 ||$window.sessionStorage.roleId == 20) {
                $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                    $scope.exportArray = [];
                    //console.log('part1', part1);
                    $scope.beneficiaries = part1;
                    $scope.modalInstanceLoad.close();
                    angular.forEach($scope.beneficiaries, function (member, index) {
                        member.index = index + 1;

                        var data14 = $scope.towns.filter(function (arr) {
                            return arr.id == member.town
                        })[0];

                        if (data14 != undefined) {
                            member.townName = data14.name;
                        }
                      
                        if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }

                        var data16 = $scope.sites.filter(function (arr) {
                            return arr.id == member.site
                        })[0];

                        if (data16 != undefined) {
                            member.siteName = data16.name;
                        }
                        
                          var data18 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.lastmodifiedby
                        })[0];

                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastmodifiedbyrole
                        })[0];

                        if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }
                        

                        

                        member.index = index + 1;

                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');
                        
                         if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                        $scope.TotalData = [];
                        $scope.TotalData.push(member);
                        $scope.exportArray.push(member);
                    });
                });
            }
    else if ($window.sessionStorage.roleId == 13) {
                //console.log('fieldworkers');
               // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    //console.log('fw', fw);
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (part2) {
                          //console.log('part2', part2);
                        $scope.exportArray = [];
                        $scope.beneficiaries = part2;
                        $scope.modalInstanceLoad.close();
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            member.index = index + 1;

                            if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                            var data14 = $scope.towns.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data14 != undefined) {
                                member.townName = data14.name;
                            }
                            if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                            })[0];

                            if (data15 != undefined) {
                            member.typologyName = data15.name;
                            }
                            } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }
                            var data16 = $scope.sites.filter(function (arr) {
                                return arr.id == member.site
                            })[0];

                            if (data16 != undefined) {
                                member.siteName = data16.name;
                            }
                              var data18 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.lastmodifiedby
                        })[0];

                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastmodifiedbyrole
                        })[0];

                        if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }
                      

                        member.index = index + 1;

                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');
                        

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                            $scope.exportArray.push(member);
                        });
                    });
                });
            }
    else if($window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 18) {
               // console.log('fieldworkers');
               // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    //console.log('fw', fw);
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (part2) {
                          //console.log('part2', part2);
                        $scope.exportArray = [];
                        $scope.beneficiaries = part2;
                        $scope.modalInstanceLoad.close();
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            member.index = index + 1;

                            if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                            var data14 = $scope.towns.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data14 != undefined) {
                                member.townName = data14.name;
                            }
                              if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }

                            var data16 = $scope.sites.filter(function (arr) {
                                return arr.id == member.site
                            })[0];

                            if (data16 != undefined) {
                                member.siteName = data16.name;
                            }
                            
                             var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.lastmodifiedby
                            })[0];
                            
                              if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastmodifiedbyrole
                        })[0];

                        if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }
                        

                        member.index = index + 1;

                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');
                          


                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                            $scope.exportArray.push(member);
                        });
                    });
                });
            }else if($window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 19) {
                //console.log('fieldworkers');
               // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    //console.log('fw', fw);
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}}]}}').getList().then(function (part2) {
                          //console.log('part2', part2);
                        $scope.exportArray = [];
                        $scope.beneficiaries = part2;
                        $scope.modalInstanceLoad.close();
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            member.index = index + 1;

                            if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                            var data14 = $scope.towns.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data14 != undefined) {
                                member.townName = data14.name;
                            }
                            
                            if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                            })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }

                            var data16 = $scope.sites.filter(function (arr) {
                                return arr.id == member.site
                            })[0];

                            if (data16 != undefined) {
                                member.siteName = data16.name;
                            }

                           var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.lastmodifiedby
                            })[0]; 
                            
                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastmodifiedbyrole
                        })[0];
                            
                            if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }

                       
                        member.index = index + 1;

                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');
                        

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                            $scope.exportArray.push(member);
                        });
                    });
                });
            } else if($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 9) {
                //console.log('fieldworkers');
               // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    //console.log('fw', fw);
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}}]}}').getList().then(function (part2) {
                          //console.log('part2', part2);
                        $scope.exportArray = [];
                        $scope.beneficiaries = part2;
                        $scope.modalInstanceLoad.close();
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            member.index = index + 1;

                            if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                            var data14 = $scope.towns.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data14 != undefined) {
                                member.townName = data14.name;
                            }
                            
                            if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                            })[0];

                            if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }

                            var data16 = $scope.sites.filter(function (arr) {
                                return arr.id == member.site
                            })[0];

                            if (data16 != undefined) {
                                member.siteName = data16.name;
                            }

                           var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.lastmodifiedby
                            })[0]; 
                            
                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastmodifiedbyrole
                        })[0];
                            
                            if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }

                       
                        member.index = index + 1;

                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');
                        

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                            $scope.exportArray.push(member);
                        });
                    });
                });
            }
    else if($window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17 || $window.sessionStorage.roleId == 10) {
                //console.log('fieldworkers');
               // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    //console.log('fw', fw);
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}}]}}').getList().then(function (part2) {
                          //console.log('part2', part2);
                        $scope.exportArray = [];
                        $scope.beneficiaries = part2;
                        $scope.modalInstanceLoad.close();
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            member.index = index + 1;

                            if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                            var data14 = $scope.towns.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data14 != undefined) {
                                member.townName = data14.name;
                            }
                        if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }

                            var data16 = $scope.sites.filter(function (arr) {
                                return arr.id == member.site
                            })[0];

                            if (data16 != undefined) {
                                member.siteName = data16.name;
                            }

                           var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.lastmodifiedby
                            })[0]; 
                            
                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastmodifiedbyrole
                        })[0];
                            
                            if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }

                       
                        member.index = index + 1;

                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');
                        

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                            $scope.exportArray.push(member);
                        });
                    });
                });
            } else if($window.sessionStorage.roleId == 8 ) {
                //console.log('fieldworkers');
               // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    //console.log('fw', fw);
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}}]}}').getList().then(function (part2) {
                          //console.log('part2', part2);
                        $scope.exportArray = [];
                        $scope.beneficiaries = part2;
                        $scope.modalInstanceLoad.close();
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            member.index = index + 1;

                            if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                            var data14 = $scope.towns.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data14 != undefined) {
                                member.townName = data14.name;
                            }
                            if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }

                            var data16 = $scope.sites.filter(function (arr) {
                                return arr.id == member.site
                            })[0];

                            if (data16 != undefined) {
                                member.siteName = data16.name;
                            }

                           var data18 = $scope.usersdisplay.filter(function (arr) {
                                return arr.id == member.lastmodifiedby
                            })[0]; 
                            
                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastmodifiedbyrole
                        })[0];
                            
                            if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }

                       
                        member.index = index + 1;

                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');
                        

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                            $scope.exportArray.push(member);
                        });
                    });
                });
            }

    /********new changes export data ends***********/
    /*export data into excel sheet******/
                $scope.DisableExport = false;
        $scope.valConCount = 0;
        $scope.exportData = function () {
            console.log("inside export func");
             //$scope.exportArray = [];
            $scope.TotalData = [];
            $scope.valConCount = 0;
            if ($scope.exportArray.length == 0) {
                alert('No data found');
            } else {
                for (var arr = 0; arr < $scope.exportArray.length; arr++) {
                    $scope.TotalData.push({
                        'Sr No': $scope.exportArray[arr].index,
                        'Name': $scope.exportArray[arr].fullname,
                        'MEMBER ID': $scope.exportArray[arr].tiId,
                        'TYPOLOGY': $scope.exportArray[arr].typologyName,
                        'SITE': $scope.exportArray[arr].siteName,
                        'TOWN': $scope.exportArray[arr].townName,
                        'HOTSPOT': $scope.exportArray[arr].hotspot,
                        'CREATED DATE': $scope.exportArray[arr].createdDate, 
                        'LAST MODIFIED ROLE': $scope.exportArray[arr].lastModifiedByRolename,
                        'DELETE FLAG': $scope.exportArray[arr].deleteflag
                        /*'MEMBER NAME': $scope.exportArray[arr].membername,
                        //'HEAD OF THE HOUSEHOLD': $scope.exportArray[arr].householdname,
                        'TI ID': $scope.exportArray[arr].tiId,
                        //'ASSIGNED TO': $scope.exportArray[arr].associatedhfname,
                        'CONDITION': $scope.exportArray[arr].conditionname,
                        'STEP': $scope.exportArray[arr].conditionstepname,
                        'STATUS': $scope.exportArray[arr].conditionstatusname,
                        //'CHECK LIST': $scope.exportArray[arr].checkListName,
                        'CASE CLOSED': $scope.exportArray[arr].caseClosed,
                        //'DIAGNOSIS': $scope.exportArray[arr].diagnosisName,
                        //'RISK ASSESSMENT': $scope.exportArray[arr].trailerDetails1,
                        //'SCREENING': $scope.exportArray[arr].trailerDetails2,
                        //'TESTING': $scope.exportArray[arr].trailerDetails3,
                        // 'TREATMENT': $scope.exportArray[arr].trailerDetails4,
                        //'RETESTING': $scope.exportArray[arr].trailerDetails5,
                        'FOLLOW UP': $scope.exportArray[arr].conditionfollowupname,
                        'FOLLOW UP DATE': $scope.exportArray[arr].followupdateDsply,
                        'REASON FOR CANCELLING': $scope.exportArray[arr].reasonforcancellationname,
                        'CREATED BY': $scope.exportArray[arr].createdByname,
                        'CREATED DATE': $scope.exportArray[arr].createdDate,
                        'CREATED ROLE': $scope.exportArray[arr].createdByRolename,
                        'LAST MODIFIED BY': $scope.exportArray[arr].lastModifiedByname,
                        'LAST MODIFIED DATE': $scope.exportArray[arr].lastModifiedDate,
                        'LAST MODIFIED ROLE': $scope.exportArray[arr].lastModifiedByRolename,
                        'DELETE FLAG': $scope.exportArray[arr].deleteflag*/
                    });

                    $scope.valConCount++;
                    if ($scope.exportArray.length == $scope.valConCount) {
                        alasql('SELECT * INTO XLSX("memberlist.xlsx",{headers:true}) FROM ?', [$scope.TotalData]);
                    }
                }
            }
        };
                /*export data into ecxet sheet ends********/
                
                
    
            $scope.status = $scope.activeDis;
   

            $scope.$watch('status', function (newValue, oldValue) {
                    if (newValue === oldValue || newValue === '' || newValue === null) {
                        return;
                    } else {
                        $scope.exportArray = [];
                        if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {
                            $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"facility":{"inq":[' + $window.sessionStorage.coorgId + ']}},{"deleteflag":{"inq":[false]}}]}}';
                            
                        } else if ($window.sessionStorage.roleId + "" === "13"){

                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"transferred_flag":{"inq":[false]}}]}}';
                            });
                        }else if($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {
                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}},{"transferred_flag":{"inq":[false]}}]}}';
                            });
                        } else if($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"transferred_flag":{"inq":[false]}}]}}';
                            });
                        } else if($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 9) {
                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}},{"transferred_flag":{"inq":[false]}}]}}';
                            });
                        }  else if($window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17 || $window.sessionStorage.roleId == 10) {
                            
                             Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}},{"transferred_flag":{"inq":[false]}}]}}';
                            });
                            
                        } else if($window.sessionStorage.roleId == 8 ) { 
                            
                             Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}},{"transferred_flag":{"inq":[false]}}]}}';
                            });
                        }
                        
                            Restangular.all($scope.filterCall).getList().then(function (household) {
                                $scope.beneficiaries = household;
                                angular.forEach($scope.beneficiaries, function (member, index) {

                                    member.index = index + 1;

                                    var data14 = $scope.towns.filter(function (arr) {
                                        return arr.id == member.town
                                    })[0];

                                    if (data14 != undefined) {
                                        member.townName = data14.name;
                                    }
                        if ($window.sessionStorage.language == 1) {

                            var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        } else if ($window.sessionStorage.language != 1) {

                           var data15 = $scope.typologies.filter(function (arr) {
                            return arr.parentId == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }
                        }

                                    var data16 = $scope.sites.filter(function (arr) {
                                        return arr.id == member.site
                                    })[0];

                                    if (data16 != undefined) {
                                        member.siteName = data16.name;
                                    }
                                    var data18 = $scope.usersdisplay.filter(function (arr) {
                            return arr.id == member.lastModifiedBy
                        })[0];

                        if (data18 != undefined) {
                            member.lastModifiedByname = data18.username;
                        }

                        var data19 = $scope.roledsply.filter(function (arr) {
                            return arr.id == member.lastModifiedByRole
                        })[0];

                        if (data19 != undefined) {
                            member.lastModifiedByRolename = data19.name;
                        }
                       
                        member.followupdateDsply = $filter('date')(member.lastmodifiedtime, 'dd/MM/yyyy');
                        member.createdDate = $filter('date')(member.createddatetime, 'dd/MM/yyyy');


                                    $scope.TotalData = [];
                                    $scope.TotalData.push(member);
                                      $scope.exportArray.push(member);
                                });
                            });

                        }
                    });





                /***************************FILTER *********************/
                /* $scope.SiteFilter = function (fl) {
                      // console.log('LevelFilter', fl);
                      //$scope.mycolor = '#4169e1';
                      $scope.backgroundCol1 = "blue";
                      $scope.backgroundCol2 = "";
                      if ($window.sessionStorage.roleId == 5) {

                          $scope.fws = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fws) {
                              $scope.fieldwork = fws;
                              $scope.dsr = Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (dsRes) {
                                  $scope.routes = dsRes;
                                  $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=site').getList().then(function (part1) {
                                      $scope.beneficiaries = part1;
                                      $scope.modalInstanceLoad.close();
                                      angular.forEach($scope.beneficiaries, function (member, index) {
                                          member.index = index + 1;

                                          for (var i = 0; i < $scope.routes.length; i++) {
                                              if (member.site == $scope.routes[i].id) {
                                                  member.SiteName = $scope.routes[i];
                                                  break;
                                              }
                                          }

                                          for (var j = 0; j < $scope.fieldwork.length; j++) {
                                              if (member.fieldworker == $scope.fieldwork[j].id) {
                                                  member.FWName = $scope.fieldwork[j];
                                                  break;
                                              }
                                          }

                                          if (member.partiallydeleted == true) {
                                              member.backgroundColor = "#C7196E"
                                          } else if (member.deleteflag == true) {
                                              member.backgroundColor = "#ff0000"
                                          } else {
                                              member.backgroundColor = "#000000"
                                          }
                                          $scope.TotalData = [];
                                          $scope.TotalData.push(member);

                                      });
                                  });
                              })
                          });
                      } else if ($window.sessionStorage.roleId == 15) {
                          $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
                              $scope.beneficiaries = part1;
                              $scope.modalInstanceLoad.close();
                              angular.forEach($scope.beneficiaries, function (member, index) {
                                  member.index = index + 1;

                                  if (member.partiallydeleted == true) {
                                      member.backgroundColor = "#C7196E"
                                  } else if (member.deleteflag == true) {
                                      member.backgroundColor = "#ff0000"
                                  } else {
                                      member.backgroundColor = "#000000"
                                  }

                                  $scope.TotalData = [];
                                  $scope.TotalData.push(member);
                              });
                          });
                      } else {
                          Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                              $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (part2) {
                                  $scope.beneficiaries = part2;
                                  $scope.modalInstanceLoad.close();
                                  angular.forEach($scope.beneficiaries, function (member, index) {
                                      member.index = index + 1;

                                      if (member.partiallydeleted == true) {
                                          member.backgroundColor = "#C7196E"
                                      } else if (member.deleteflag == true) {
                                          member.backgroundColor = "#ff0000"
                                      } else {
                                          member.backgroundColor = "#000000"
                                      }

                                      $scope.TotalData = [];
                                      $scope.TotalData.push(member);
                                  });
                              });
                          });
                      }

                  };

                  $scope.HotspotFilter = function (fl) {
                      // console.log('LevelFilter', fl);
                      $scope.backgroundCol1 = "";
                      $scope.backgroundCol2 = "blue";
                      if ($window.sessionStorage.roleId == 5) {

                          $scope.fws = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fws) {
                              $scope.fieldwork = fws;
                              $scope.dsr = Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (dsRes) {
                                  $scope.routes = dsRes;
                                  $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=hotspot%20ASC').getList().then(function (part1) {
                                      $scope.beneficiaries = part1;
                                      $scope.modalInstanceLoad.close();
                                      angular.forEach($scope.beneficiaries, function (member, index) {
                                          member.index = index + 1;

                                          for (var i = 0; i < $scope.routes.length; i++) {
                                              if (member.site == $scope.routes[i].id) {
                                                  member.SiteName = $scope.routes[i];
                                                  break;
                                              }
                                          }

                                          for (var j = 0; j < $scope.fieldwork.length; j++) {
                                              if (member.fieldworker == $scope.fieldwork[j].id) {
                                                  member.FWName = $scope.fieldwork[j];
                                                  break;
                                              }
                                          }

                                          if (member.partiallydeleted == true) {
                                              member.backgroundColor = "#C7196E"
                                          } else if (member.deleteflag == true) {
                                              member.backgroundColor = "#ff0000"
                                          } else {
                                              member.backgroundColor = "#000000"
                                          }
                                          $scope.TotalData = [];
                                          $scope.TotalData.push(member);

                                      });
                                  });
                              })
                          });
                      } else if ($window.sessionStorage.roleId == 15) {
                          $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
                              $scope.beneficiaries = part1;
                              $scope.modalInstanceLoad.close();
                              angular.forEach($scope.beneficiaries, function (member, index) {
                                  member.index = index + 1;

                                  if (member.partiallydeleted == true) {
                                      member.backgroundColor = "#C7196E"
                                  } else if (member.deleteflag == true) {
                                      member.backgroundColor = "#ff0000"
                                  } else {
                                      member.backgroundColor = "#000000"
                                  }

                                  $scope.TotalData = [];
                                  $scope.TotalData.push(member);
                              });
                          });
                      } else {
                          Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                              $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (part2) {
                                  $scope.beneficiaries = part2;
                                  $scope.modalInstanceLoad.close();
                                  angular.forEach($scope.beneficiaries, function (member, index) {
                                      member.index = index + 1;

                                      if (member.partiallydeleted == true) {
                                          member.backgroundColor = "#C7196E"
                                      } else if (member.deleteflag == true) {
                                          member.backgroundColor = "#ff0000"
                                      } else {
                                          member.backgroundColor = "#000000"
                                      }

                                      $scope.TotalData = [];
                                      $scope.TotalData.push(member);
                                  });
                              });
                          });
                      }

                  };*/
                /********************************************* INDEX *******************************************
	if ($window.sessionStorage.roleId == 5) {
		$scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
			$scope.beneficiaries = part1;
			$scope.modalInstanceLoad.close();
			angular.forEach($scope.beneficiaries, function (member, index) {
				member.index = index + 1;

				$scope.TotalData = [];
				$scope.TotalData.push(member);

			});
		});
	} else if ($window.sessionStorage.roleId == 15) {
		$scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
			$scope.beneficiaries = part1;
			$scope.modalInstanceLoad.close();
			angular.forEach($scope.beneficiaries, function (member, index) {
				member.index = index + 1;

				$scope.TotalData = [];
				$scope.TotalData.push(member);
			});
		});
	} else {
		Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
			$scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
				$scope.beneficiaries = part2;
				$scope.modalInstanceLoad.close();
				angular.forEach($scope.beneficiaries, function (member, index) {
					member.index = index + 1;

					$scope.TotalData = [];
					$scope.TotalData.push(member);
				});
			});
		});
	}

	/************************** DELETE *******************************************
	$scope.Delete = function (id) {
		//$scope.OK();
		//$scope.documentdataModal = !$scope.documentdataModal;
		$scope.toggleLoading();
		$scope.deleteCount = 0;
		$scope.deleteReportCount = 0;
		$scope.deleteGroupMeetingCount = 0;
		$scope.auditlog.entityid = id;
		var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
		$scope.auditlog.description = 'Member Aarchived With Following Details: ' + 'Archived By UserId - ' + $window.sessionStorage.userId + ', ' + 'memberId - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

		Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
			$scope.item = [{
				partiallydeleted: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date(),
				lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
			Restangular.one('beneficiaries/' + id).customPUT($scope.item[0]).then(function () {
				//$route.reload();
				Restangular.all('todos?filter[where][beneficiaryid]=' + id + '&filter[where][deleteflag]=false').getList().then(function (tds) {
					if (tds.length > 0) {
						$scope.todosToDelete = tds;
						$scope.deleteTodo(id);
					} else {
						Restangular.all('reportincidents?filter[where][beneficiaryid]=' + id + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
							if (rprtincdnts.length > 0) {
								$scope.reportsToDelete = rprtincdnts;
								$scope.deleteReportIncident(id);
							} else {
								$scope.itemben = [{
									partiallydeleted: false,
									deleteflag: true,
									lastmodifiedby: $window.sessionStorage.UserEmployeeId,
									lastmodifiedtime: new Date(),
									lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
								Restangular.one('beneficiaries/' + id).customPUT($scope.itemben[0]).then(function () {
									$scope.modalInstanceLoad.close();
									//$route.reload();
									window.location = "/members";
								});
							}
						});
					}
				});
			});
		});
	}
*/
                $scope.beneficiary = {
                    "reasonforarchive": false
                }
                //console.log('$scope.beneficiary.reasonforarchive', $scope.reasonforarchive)
                $scope.modalTitle = 'Reason For Archive Member'
                $scope.memberdataModal = false; $scope.documentdataModal = false;



                $scope.Delete11 = function (id) {
                    //console.log('i m delete1');
                    $scope.archivMemId = id;
                    $scope.documentdataModal = !$scope.documentdataModal;
                }

                $scope.Delete1 = function (id) {
                    $scope.archivMemId = id;
                    $scope.modalInstance1 = $modal.open({
                        animation: true,
                        templateUrl: 'template/option.html',
                        scope: $scope,
                        backdrop: 'static'

                    });
                };

                $scope.Cancelmodal = function () {
                    $scope.modalInstance1.close();
                }; $scope.Delete2 = function (id) {
                    //console.log('i m delete2');
                    //$scope.toggleLoading();
                    $scope.toggleLoading();
                    $scope.deleteCount = 0;
                    $scope.deleteReportCount = 0;
                    $scope.deleteGroupMeetingCount = 0;
                    $scope.auditlog.entityid = id;
                    var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
                    $scope.auditlog.description = 'Member Aarchived With Following Details: ' + 'Archived By UserId - ' + $window.sessionStorage.userId + ', ' + 'memberId - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

                    Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                        $scope.item = [{
                            partiallydeleted: true,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                        Restangular.one('beneficiaries/' + id).customPUT($scope.item[0]).then(function () {
                            //$route.reload();
                            Restangular.all('conditionheaders?filter[where][memberId]=' + id + '&filter[where][deleteflag]=false').getList().then(function (tds) {
                                if (tds.length > 0) {
                                    $scope.todosToDelete = tds;
                                    $scope.deleteTodo(id);
                                } else {
                                    Restangular.all('patientrecords?filter[where][memberId]=' + id + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
                                        if (rprtincdnts.length > 0) {
                                            $scope.reportsToDelete = rprtincdnts;
                                            $scope.deleteReportIncident(id);
                                        } else {
                                            $scope.itemben = [{
                                                partiallydeleted: false,
                                                deleteflag: true,
                                                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                                lastmodifiedtime: new Date(),
                                                lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                                            Restangular.one('beneficiaries/' + id).customPUT($scope.itemben[0]).then(function () {
                                                $scope.modalInstanceLoad.close();
                                                //$route.reload();
                                                window.location = "/members";
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    });
                }

                $scope.showValidation = false; $scope.toggleValidation = function () {
                    $scope.showValidation = !$scope.showValidation;
                }; $scope.okAlert = function () {
                    $scope.modalOneAlert.close();
                };

                //$scope.reasonforarchive = '';
                //console.log('$scope.documentdataModal', $scope.documentdataModal); 
    $scope.okdone = false; $scope.validatestring = ''; 
                $scope.OK = function () {
                    //console.log('I m Ok');
                    //console.log('$scope.documentdataModal',$scope.documentdataModal);
                    if ($scope.beneficiary.reasonforarchive == false) {
                        /*	$scope.validatestring = $scope.validatestring + 'Please Enter State Count';
                        }
                        if ($scope.validatestring != '') {
                        	$scope.toggleValidation();
                        	$scope.validatestring1 = $scope.validatestring;
                        	$scope.validatestring = '';
                        	
                        	*/
                        //alert('Choose an Option');

                        $scope.modalOneAlert = $modal.open({
                            animation: true,
                            templateUrl: 'template/AlertModal.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sm',
                            windowClass: 'modal-danger'

                        });

                    } else {
                        $scope.modalInstance1.close();
                        //$scope.documentdataModal = !$scope.documentdataModal;
                        $scope.item2 = [{
                            reasonforarchive: $scope.beneficiary.reasonforarchive
            }]
                        Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.item2[0]).then(function (reasonforarchive) {
                            console.log('reasonforarchive', reasonforarchive);
                        });
                        $scope.okdone = true;
                        console.log('$scope.reasonforarchive', $scope.beneficiary.reasonforarchive);
                        console.log('$scope.archivMemId', $scope.archivMemId);
                        if ($scope.okdone == true) {
                            $scope.Delete2($scope.archivMemId);
                        }
                    }
                }
                $scope.OK1 = function () {
                    //console.log('$scope.reasonforarchive', $scope.beneficiary.reasonforarchive);
                    //console.log('$scope.archivMemId', $scope.archivMemId);
                    var id = $scope.archivMemId;
                    $scope.item = [{
                        partiallydeleted: true,
                        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                        lastmodifiedtime: new Date(),
                        lastmodifiedbyrole: $window.sessionStorage.roleId,
                        reasonforarchive: $scope.beneficiary.reasonforarchive
            }]
                    Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.item[0]).then(function () {
                        //$route.reload();
                        Restangular.all('conditionheaders?filter[where][memberId]=' + $scope.archivMemId + '&filter[where][deleteflag]=false').getList().then(function (tds) {
                            if (tds.length > 0) {
                                $scope.todosToDelete = tds;
                                $scope.deleteTodo($scope.archivMemId);
                            } else {
                                Restangular.all('patientrecords?filter[where][memberId]=' + $scope.archivMemId + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
                                    if (rprtincdnts.length > 0) {
                                        $scope.reportsToDelete = rprtincdnts;
                                        $scope.deleteReportIncident($scope.archivMemId);
                                    } else {
                                        $scope.itemben = [{
                                            partiallydeleted: false,
                                            deleteflag: true,
                                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                            lastmodifiedtime: new Date(),
                                            lastmodifiedbyrole: $window.sessionStorage.roleId,
                                            reasonforarchive: $scope.beneficiary.reasonforarchive
            }]
                                        Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.itemben[0]).then(function () {
                                            $scope.modalInstanceLoad.close();
                                            //$route.reload();
                                            window.location = "/members";
                                        });
                                    }
                                });
                            }
                        });
                    });


                }

                $scope.deleteTodo = function (benId) {
                    $scope.itemtodo = [{
                        deleteflag: true,
                        lastModifiedBy: $window.sessionStorage.userId,
                        lastModifiedDate: new Date(),
                        lastModifiedByRole: $window.sessionStorage.roleId
            }]
                    Restangular.one('conditionheaders/' + $scope.todosToDelete[$scope.deleteCount].id).customPUT($scope.itemtodo[0]).then(function () {
                        $scope.deleteCount++;
                        if ($scope.deleteCount < $scope.todosToDelete.length) {
                            $scope.deleteTodo(benId);
                        } else {
                            Restangular.all('reportincidents?filter[where][beneficiaryid]=' + benId + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
                                if (rprtincdnts.length > 0) {
                                    $scope.reportsToDelete = rprtincdnts;
                                    $scope.deleteReportIncident(benId);
                                } else {
                                    $scope.itemben = [{
                                        partiallydeleted: false,
                                        deleteflag: true,
                                        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                        lastmodifiedtime: new Date(),
                                        lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                                    Restangular.one('beneficiaries/' + benId).customPUT($scope.itemben[0]).then(function () {
                                        $scope.modalInstanceLoad.close();
                                        //$route.reload();
                                        window.location = "/members";
                                    });
                                }
                            });
                        }
                    });
                }

                $scope.deleteReportIncident = function (benId) {
                    $scope.itemreport = [{
                        deleteflag: true,
                        lastModifiedBy: $window.sessionStorage.userId,
                        lastModifiedDate: new Date(),
                        lastModifiedByRole: $window.sessionStorage.roleId
            }]
                    Restangular.one('patientrecords/' + $scope.reportsToDelete[$scope.deleteReportCount].id).customPUT($scope.itemreport[0]).then(function () {
                        $scope.deleteReportCount++;
                        if ($scope.deleteReportCount < $scope.reportsToDelete.length) {
                            $scope.deleteReportIncident(benId);
                        } else {
                            $scope.itemben = [{
                                partiallydeleted: false,
                                deleteflag: true,
                                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                lastmodifiedtime: new Date(),
                                lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                            Restangular.one('beneficiaries/' + benId).customPUT($scope.itemben[0]).then(function () {
                                //$route.reload();
                                $scope.modalInstanceLoad.close();
                                window.location = "/members";
                            });
                        }
                    });
                }

                $scope.UnDelete = function (id) {
                    $scope.auditlog.entityid = id;
                    var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
                    $scope.auditlog.description = 'Member Unarchived With Following Details: ' + 'Unarchived By UserId - ' + $window.sessionStorage.userId + ', ' + 'memberId - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

                    Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                        $scope.item = [{
                            partiallydeleted: false,
                            deleteflag: false,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                        Restangular.one('beneficiaries/' + id).customPUT($scope.item[0]).then(function () {
                            $route.reload();
                        });
                    });
                }


                /********************************************* INDEX *******************************************/

                /* $scope.sortingOrder = sortingOrder;
                 $scope.reverse = false;

                 $scope.sort_by = function (newSortingOrder) {
                     if ($scope.sortingOrder == newSortingOrder)
                         $scope.reverse = !$scope.reverse;

                     $scope.sortingOrder = newSortingOrder;
                 };*/

                $scope.sort = {
                active: '',
                descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

                /********************************** Language *************************************/

                /***********************************************************************/



            })
 .directive('migrantmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-md">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
