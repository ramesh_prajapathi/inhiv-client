'use strict';

angular.module('secondarySalesApp')
    .controller('ReferToCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }


        $scope.showForm = function () {
            var visible = $location.path() === '/patientflow/create' || $location.path() === '/patientflow/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/patientflow/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/patientflow/create' || $location.path() === '/patientflow/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/patientflow/create' || $location.path() === '/patientflow/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/patientflow-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /***new changes*****/
        $scope.showenglishLang = true;

        $scope.$watch('referto.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    $scope.referto.name = '';
                    $scope.referto.parentId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });
        /***new changes*****/

        if ($routeParams.id) {
            $scope.message = 'Patient Flow has been Updated!';
            Restangular.one('refertos', $routeParams.id).get().then(function (referto) {
                $scope.original = referto;
                $scope.referto = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Patient Flow has been Created!';
        }

        //$scope.Search = $scope.name;
    
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];
    
     $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /******************************** INDEX *******************************************/

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('refertos?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.refertos = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.refertos, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.patilanguages = sev;
            });

        } else {

            Restangular.all('refertos').getList().then(function (mt) {
                $scope.refertos = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.refertos, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

           
            
             Restangular.all('refertos?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.patilanguages = sev;
           
                      angular.forEach($scope.patilanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                          console.log('member', member);
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.patilanguages = sev;
                       });
                  }
              });
        }

        /***new changes*****/

        Restangular.all('refertos?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.patientdisply = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        /***new changes*****/
        /********************************************* SAVE *******************************************/
        $scope.referto = {
            "name": '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            "deleteflag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            //            document.getElementById('hnname').style.border = "";
            //            document.getElementById('knname').style.border = "";
            //            document.getElementById('taname').style.border = "";
            //            document.getElementById('tename').style.border = "";
            //            document.getElementById('mrname').style.border = "";

            //            if ($scope.referto.language == '' || $scope.referto.language == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Select Language';
            //                //  document.getElementById('language').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.referto.name == '' || $scope.referto.name == null) {
            //                $scope.validatestring = $scope.validatestring + ' Please Enter Refer To';
            //                document.getElementById('name').style.borderColor = "#FF0000";
            //
            //            }


            if ($scope.referto.language == '' || $scope.referto.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.referto.language == 1) {
                if ($scope.referto.name == '' || $scope.referto.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Patient Flow';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            } else if ($scope.referto.language != 1) {
                if ($scope.referto.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Patient Flow in English';

                } else if ($scope.referto.name == '' || $scope.referto.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Patient Flow';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }




            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                //                if ($scope.referto.parentId === '') {
                //                    delete $scope.referto['parentId'];
                //                }
                //                $scope.submitDisable = true;
                //                $scope.referto.parentFlag = $scope.showenglishLang;
                //                $scope.refertos.post($scope.referto).then(function () {
                //                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                    window.location = '/referto-list';
                //                }, function (error) {
                //                    if (error.data.error.constraint === 'medicine_lang_parenrid') {
                //                        alert('Value already exists for this language');
                //                    }
                //                });
                //                $scope.submitDisable = true;
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                $scope.refertos.post($scope.referto).then(function () {
                //                    window.location = '/patientflow-list';
                //                });
                if ($scope.referto.parentId === '') {
                    delete $scope.referto['parentId'];
                }
                $scope.submitDisable = true;
                $scope.referto.parentFlag = $scope.showenglishLang;
                $scope.refertos.post($scope.referto).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/patientflow-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'referto_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
             document.getElementById('name').style.border = "";
            //            document.getElementById('hnname').style.border = "";
            //            document.getElementById('knname').style.border = "";
            //            document.getElementById('taname').style.border = "";
            //            document.getElementById('tename').style.border = "";
            //            document.getElementById('mrname').style.border = "";

            //            if ($scope.referto.language == '' || $scope.referto.language == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Select Language';
            //                //  document.getElementById('language').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.referto.name == '' || $scope.referto.name == null) {
            //                $scope.validatestring = $scope.validatestring + ' Please Enter Refer To';
            //                document.getElementById('name').style.borderColor = "#FF0000";
            //
            //            }


            if ($scope.referto.language == '' || $scope.referto.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.referto.language == 1) {
                if ($scope.referto.name == '' || $scope.referto.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Patient Flow';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            } else if ($scope.referto.language != 1) {
                if ($scope.referto.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Patient Flow in English';

                } else if ($scope.referto.name == '' || $scope.referto.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Patient Flow';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {


                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                Restangular.one('refertos', $routeParams.id).customPUT($scope.referto).then(function () {
                //                    console.log('Step Saved');
                //                    window.location = '/patientflow-list';
                //
                //
                //                });
                if ($scope.referto.parentId === '') {
                    delete $scope.referto['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('refertos', $routeParams.id).customPUT($scope.referto).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('refertoe Saved');
                    setTimeout(function () {
                        window.location = '/patientflow-list';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'referto_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });



            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('refertos/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        
         /***************** Archive *****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('refertos/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
