'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/
        //$scope.hideAddBtn = true;
        $scope.disablePatientAdd = true;
        $scope.CreateClicked = false;
        $scope.disabledSave = false;
        $scope.role = {};
        $scope.role.enablePR = false;
    
      $scope.condition = {
            caseClosed: false,
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            createdDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedDate: new Date(),
            countryId: $window.sessionStorage.countryId,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            town: $window.sessionStorage.distributionAreaId,
            //relation: $scope.selfDis,
            facility: $window.sessionStorage.coorgId,
            deleteflag: false
            /*site: $window.sessionStorage.siteId.split(",")[0],*/

        };


        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
            //$scope.modalInstanceLoad.close();
            $scope.PRHeading = $scope.ConditionLanguage.addNewCondition;
            $scope.modaltitle1 = $scope.ConditionLanguage.areYouSureToWantToSave;
            $scope.message = $scope.ConditionLanguage.conditionSaved;
            $scope.entermemberName = $scope.ConditionLanguage.enterName;
            $scope.enteryourId = $scope.ConditionLanguage.enterId;
            $scope.selectingrelationship = $scope.ConditionLanguage.selectRelationship;
            $scope.selectingCondition = $scope.ConditionLanguage.selectCondition;
            $scope.pleaseselectareason = $scope.ConditionLanguage.pleaseselectreason;
            $scope.pleaseselectafollowup = $scope.ConditionLanguage.pleaseSelectFollowup;
            $scope.spousepartDis = $scope.ConditionLanguage.spouse;
            $scope.selfDis = $scope.ConditionLanguage.checkList;
            $scope.conditionexists = $scope.ConditionLanguage.conditionAlreadyExists;
            $scope.condition.relation = 'SELF';
      
        });
    
            

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }


        $scope.HideCreateButton = true;
        $scope.HideUpdateButton = true;
        $scope.editDisable = false;
        $scope.editDisable1 = true;
        $scope.confirmationModel = false;
        $scope.checkListModal = false;
        $scope.disableCaseClosed = true;
        $scope.disableAddBtn = false;
        $scope.ConditionLanguage = {};
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.hideDiagnosis = false;

        //$scope.selfDis = 'SELF';
      

        $scope.cancelCondition = function () {

            $window.sessionStorage.MemberId = '';
            $window.sessionStorage.fullName = '';
            $window.sessionStorage.relation = '';
            $window.sessionStorage.conditionId = '';
            $window.sessionStorage.conditionIdnew = '';
            $window.sessionStorage.previouspage = '';
            window.location = '/conditions-list';
        };

        $scope.auditlog = {};


        if ($window.sessionStorage.roleId) {
            $scope.hidePatient = false;
            $scope.condition.lastModifiedByRole = $window.sessionStorage.roleId;
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                $scope.condition.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;

            });

        }
        /*else {
            $scope.hidePatient = false;
            $scope.condition.lastModifiedByRole = 13;

            //  Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                $scope.condition.fieldworkername = fw.firstname;
                $scope.condition.fieldworker = fw.id;
                Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                    $scope.condition.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                });
            });
        }*/


        if ($window.sessionStorage.previous == '/patientrecord/create') {
            $scope.trailerArray = [];
            console.log("i m in if");
            //  $scope.condition.followup = $window.sessionStorage.followup;

            //  Restangular.one('conditionfollowups', $window.sessionStorage.followup).get().then(function (cntn) {
            //   $scope.followUpName = cntn.name;
            // var myDate = new Date();
            // myDate.setDate(myDate.getDate() + cntn.followUpDays);
            //$scope.condition.followupdate = myDate;
            // });

            // $scope.trailerArray = JSON.parse($window.sessionStorage.trailerArray);
            $scope.condition.followupdatepick = '';


        } else {
            $scope.trailerArray = [];
            $scope.condition.followup = '';
            // $scope.condition.followupdate = '';

            Restangular.all('conditionstatuses?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (csts) {

                $scope.conditionstatuses = csts;
                angular.forEach($scope.conditionstatuses, function (value, index) {
                    if ($window.sessionStorage.roleId == 13) {
                        if ($window.sessionStorage.language != 1) {


                            console.log('here')
                            if (value.parentId == 33) {
                                value.dataEnable = false
                            } else {
                                value.dataEnable = true
                            }
                        } else {
                            if (value.id == 33) {
                                value.dataEnable = false
                            } else {
                                value.dataEnable = true
                            }


                        }

                    }
                });

                Restangular.all('conditionsteps?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (cs) {
                    $scope.conditionsteps = cs;

                    angular.forEach($scope.conditionsteps, function (value, index) {
                        value.index = index;

                        if ($scope.UserLanguage == 1) {
                            value.id = value.id;
                        } else {
                            value.id = value.parentId;
                        }

                        $scope.trailerArray.push({
                            step: value.name,
                            status: '',
                            date: '',
                            id: value.id,
                            enabled: true,
                            hidelist: true,
                            action: '',
                            statuses: $scope.conditionstatuses
                        });

                        $scope.trailerArray[0].enabled = false;
                        $scope.trailerArray[0].status = 33;
                        $scope.trailerArray[0].date = new Date();
                        // $window.sessionStorage.trailerArray = JSON.stringify($scope.trailerArray);

                    });


                });
            });

        }


        Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (memberResp) {
            $scope.condition.membername = memberResp.fullname;
            $scope.memberName = memberResp.fullname;
            $scope.condition.memberId = memberResp.id;
            $scope.condition.tiId = memberResp.tiId;
            $scope.conditionictcId = memberResp.ictcid;
            $scope.conditionartId = memberResp.artId;
            $scope.condition.site = memberResp.site;
            $scope.site = memberResp.site;
            $scope.facilityID = memberResp.facility;
            $scope.conditionNEWtiId = memberResp.tiId;

            $scope.memberUpdateId = memberResp.id;

            $scope.memberDisabled = true;
            $scope.disabledId = true;
        });

        $scope.$watch('condition.relation', function (newValue, oldValue) {

            if (newValue == 'SELF') {
                $scope.disabledId = true;
            } else {
                $scope.disabledId = true;
            }

        });

        $scope.auditArray = [];

        Restangular.all('conditions?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (cn) {
            $scope.conditionsdsply = cn;
            $scope.condition.condition = $window.sessionStorage.conditionIdnew;

        });

        Restangular.all('reasonforcancellations?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rfc) {
            $scope.reasonforcancellations = rfc;

        });


        //$scope.trailerArray = [];



        $scope.updatepatientrec = function () {
            $window.sessionStorage.fullName = $scope.condition.memberId;
            $window.sessionStorage.relation = $scope.condition.relation;
            $window.sessionStorage.previouspage = '/condition/create';
            window.location = '/patientrecord/create';
            // $window.sessionStorage.trailerArray = JSON.stringify($scope.trailerArray);

        };


        $scope.$watch('condition.condition', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {

                $window.sessionStorage.conditionId = newValue;


                if ($window.sessionStorage.language == 1) {
                    Restangular.one('conditions', newValue).get().then(function (ctns) {
                        $scope.conditionName = ctns.name;
                    });

                } else {

                    Restangular.one('conditions?filter[where][parentId]=' + newValue).get().then(function (ctns) {
                        console.log('ctns', ctns);
                        $scope.conditionName = ctns[0].name;
                    });
                }
                // $scope.conditionName = ctns.name;


                Restangular.one('conditionheaders?filter[where][condition]=' + newValue + '&filter[where][memberId]=' + $scope.condition.memberId + '&filter[where][deleteflag]=false').get().then(function (cndnResp) {
                    if (cndnResp.length > 0) {
                        $scope.disablePatientAdd = true;
                        $scope.condition.condition = '';
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.conditionexists;
                    } else {
                        $scope.disablePatientAdd = false;
                    }
                });
                /****************new changes for based on condition it will clear ********/

                $scope.trailerArray = [];
                $scope.condition.followup = '';
                // $scope.condition.followupdate = '';

                Restangular.all('conditionstatuses?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (csts) {

                    $scope.conditionstatuses = csts;
                    angular.forEach($scope.conditionstatuses, function (value, index) {
                        if ($window.sessionStorage.roleId == 13) {
                            if ($window.sessionStorage.language != 1) {


                                console.log('here')
                                if (value.parentId == 33) {
                                    value.dataEnable = false
                                } else {
                                    value.dataEnable = true
                                }
                            } else {
                                if (value.id == 33) {
                                    value.dataEnable = false
                                } else {
                                    value.dataEnable = true
                                }


                            }


                        }
                    });

                    Restangular.all('conditionsteps?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (cs) {
                        $scope.conditionsteps = cs;

                        angular.forEach($scope.conditionsteps, function (value, index) {
                            value.index = index;

                            if ($scope.UserLanguage == 1) {
                                value.id = value.id;
                            } else {
                                value.id = value.parentId;
                            }

                            $scope.trailerArray.push({
                                step: value.name,
                                status: '',
                                date: '',
                                id: value.id,
                                enabled: true,
                                hidelist: true,
                                action: '',
                                statuses: $scope.conditionstatuses
                            });

                            $scope.trailerArray[0].enabled = false;
                            $scope.trailerArray[0].status = 33;
                            $scope.trailerArray[0].date = new Date();

                        });

                    });
                });
                /**************************/

            }
            $scope.ConditionId = +newValue;
        });




        Restangular.all('conditionfollowups?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (respFollow) {
            $scope.conditionfollowups = respFollow;
            $scope.condition.followup = $scope.condition.followup;
        });



        $scope.$watch('condition.followup', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                // $window.sessionStorage.followup = newValue;
                Restangular.one('conditionfollowups', newValue).get().then(function (cntn) {
                    $scope.followUpName = cntn.name;
                    var myDate = new Date();
                    myDate.setDate(myDate.getDate() + cntn.followUpDays);
                    $scope.condition.followupdate = myDate;
                });
            }
        });

        $scope.$watch('condition.reasonForCancel', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {

                Restangular.one('reasonforcancellations', newValue).get().then(function (resn) {
                    $scope.reasonForCancelName = resn.name;
                });
            }
        });




        $scope.hideReason = false;
        $scope.hideFollowUp = true;
        $scope.followupFlag = true;

        $scope.myArray = [];

        $scope.statusChange = function (id, index, status, statuses, step, oldvalue) {

            console.log('status', status);

            console.log('id', id);
            // console.log('index', index);
            var currindex = index;

            Restangular.one('conditionstatuses', status).get().then(function (csts) {
                $scope.actionable = csts.actionable;

                if ($scope.actionable == 'No') {
                    angular.forEach($scope.trailerArray, function (member, index) {
                        member.index = index;
                        // console.log('No', member);
                        member.action = 'No';
                        if (member.enabled == true && member.index > currindex) {
                            member.hidelist = false;


                        }
                    });

                } else if ($scope.actionable == 'Yes') {
                    angular.forEach($scope.trailerArray, function (member, index) {
                        member.index = index;
                        member.action = 'Yes';
                        //console.log('yes', member);
                        if (member.enabled == true) {
                            member.hidelist = true;
                            //member.action = 'Yes';

                        }
                    });
                }
            });


            $scope.myArray = [];

            $scope.indexValue = index;

            $scope.auditArray.push({
                stepId: id,
                oldStatusId: oldvalue,
                newStatusId: status
            });

            var indexVal = index + 1;
            var prevIndexVal = index - 1;

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];

            $scope.trailerArray[index].date = new Date();

            if (index != 0) {
                $scope.trailerArray[prevIndexVal].enabled = true;
            }

            if (status == 33) {
                Restangular.one('conditionstatuses', 33).get().then(function (resp) {
                    // console.log('resp', resp);
                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                        $scope.trailerArray[index].actionable = true;
                        $scope.hideFollowUp = true;
                        $scope.followupFlag = true;
                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.trailerArray[index].actionable = false;
                        $scope.hideFollowUp = false;
                        $scope.followupFlag = false;
                    }
                });

                $scope.hideReason = false;
                $scope.trailerArray[index].enabled = false;
                $scope.condition.reasonForCancel = '';


            } else if (status == 34) {

                Restangular.one('conditionstatuses', 34).get().then(function (resp) {
                    // console.log('resp', resp);
                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                        $scope.trailerArray[index].actionable = true;

                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.trailerArray[index].actionable = false;

                    }
                });
                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;
                $scope.condition.reasonForCancel = '';


            } else if (status == 35) {
                Restangular.one('conditionstatuses', 35).get().then(function (resp) {
                    // console.log('resp', resp);
                    if (resp.actionable == 'Yes') {
                        // $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                        $scope.trailerArray[index].actionable = true;

                    } else {
                        //  $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.trailerArray[index].actionable = false;
                    }
                });
                if (id == 36) {

                    $scope.trailerArray[indexVal].enabled = false;
                    $scope.trailerArray[indexVal].status = 33;
                } else {
                    $scope.trailerArray[indexVal].enabled = true;
                    $scope.trailerArray[indexVal].status = '';
                }

                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;
                $scope.condition.reasonForCancel = '';
                // $scope.trailerArray[indexVal].enabled = false;
                // $scope.trailerArray[indexVal].hidelist = true;



            } else if (status == 36) {
                Restangular.one('conditionstatuses', 36).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        // $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                        $scope.trailerArray[index].actionable = true;

                    } else {
                        // $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.trailerArray[index].actionable = false;

                    }
                });

                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;
                $scope.condition.reasonForCancel = '';

                // $scope.trailerArray[indexVal].enabled = false;
                // $scope.trailerArray[indexVal].hidelist = true;
                //$scope.hideFollowUp = true;
                //$scope.followupFlag = true;
                if (id == 36) {

                    $scope.trailerArray[indexVal].enabled = false;
                    $scope.condition.caseClosed = true;
                    $scope.trailerArray[indexVal].status = 33;
                } else {
                    $scope.trailerArray[indexVal].enabled = true;
                    $scope.trailerArray[indexVal].status = '';
                    // $scope.condition.caseClosed = false;
                }



            } else if (status == 37) {
                Restangular.one('conditionstatuses', 37).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                        $scope.trailerArray[index].actionable = true;
                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.trailerArray[index].actionable = false;
                    }
                });

                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;
                $scope.condition.reasonForCancel = '';




            } else if (status == 38) {
                Restangular.one('conditionstatuses', 38).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                        $scope.trailerArray[index].actionable = true;
                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.trailerArray[index].actionable = false;
                    }
                });


                $scope.hideReason = false;
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.condition.reasonForCancel = '';
                $scope.trailerArray[index].enabled = false;
                //$scope.trailerArray[indexVal].enabled = true;
                //$scope.trailerArray[indexVal].hidelist = false;




            } else if (status == 39) {
                Restangular.one('conditionstatuses', 39).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = false;
                        $scope.trailerArray[indexVal].hidelist = true;
                        $scope.trailerArray[index].actionable = true;
                        $scope.trailerArray[indexVal].status = 33;

                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.trailerArray[index].actionable = false;
                    }

                });

                if (id == 37) {

                    $scope.hideReason = true;
                    $scope.condition.caseClosed = true;
                } else {
                    $scope.hideReason = false;
                }

                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.trailerArray[index].enabled = false;
                //$scope.trailerArray[indexVal].enabled = false;
                //$scope.trailerArray[indexVal].hidelist = true;
                // $scope.hideFollowUp = true;
                // $scope.followupFlag = true;
            } else {
                $scope.hideReason = true;
                $scope.trailerArray[index].enabled = false;
                $scope.trailerArray[index].actionable = false;
                $scope.trailerArray[indexVal].hidelist = false;
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.condition.caseClosed = true;
            }

            //  $window.sessionStorage.trailerArray = JSON.stringify($scope.trailerArray);
        };



        $scope.validatestring = '';

        $scope.okConfirm = function () {

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];

            if ($scope.indexValue == $scope.trailerArray.lastIndexOf(last)) {

                $scope.condition.step = $scope.trailerArray[$scope.indexValue].id;
                $scope.condition.status = $scope.trailerArray[$scope.indexValue].status;

                if ($scope.ConditionId == 51 && $scope.condition.step == 34 && $scope.condition.status == 36) {
                    //$scope.condition.ictcId = '';
                    $scope.condition.ictcId = $scope.conditionictcId;
                    $scope.condition.artId = $scope.conditionartId;
                    $scope.condition.facility = $scope.facilityID;
                } else if ($scope.ConditionId == 51 && ($scope.condition.step == 34 || $scope.condition.step == 35 || $scope.condition.step == 36 || $scope.condition.step == 37)) {
                    $scope.condition.ictcId = $scope.conditionictcId;
                    $scope.condition.artId = $scope.conditionartId;
                    $scope.condition.facility = $scope.facilityID;
                } else {
                    $scope.condition.ictcId = $scope.conditionictcId;
                    $scope.condition.artId = '';
                    $scope.condition.facility = $scope.facilityID;
                }


                if ($window.sessionStorage.language == 1) {


                    Restangular.one('conditionstatuses', $scope.trailerArray[$scope.indexValue].status).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });

                    Restangular.one('conditionsteps', $scope.trailerArray[$scope.indexValue].id).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });

                } else {

                    Restangular.one('conditionstatuses?filter[where][parentId]=' + $scope.trailerArray[$scope.indexValue].status).get().then(function (sts) {

                        $scope.statusName = sts[0].name;
                    });

                    Restangular.one('conditionsteps?filter[where][parentId]=' + $scope.trailerArray[$scope.indexValue].id).get().then(function (stp) {
                        $scope.stepName = stp[0].name;
                    });


                }



            }

            for (var g = 0; g < $scope.trailerArray.length; g++) {
                if ($scope.trailerArray[g].enabled == false && $scope.trailerArray[g].status != '') {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;

                    if ($scope.ConditionId == 51 && $scope.condition.step == 34 && $scope.condition.status == 36) {
                        // $scope.condition.ictcId = '';
                        // $scope.condition.artId = $scope.conditionartId;
                        $scope.condition.ictcId = $scope.conditionictcId;
                        $scope.condition.artId = $scope.conditionartId;
                        $scope.condition.facility = $scope.facilityID;
                    } else if ($scope.ConditionId == 51 && ($scope.condition.step == 34 || $scope.condition.step == 35 || $scope.condition.step == 36 || $scope.condition.step == 37)) {
                        $scope.condition.ictcId = $scope.conditionictcId;
                        $scope.condition.artId = $scope.conditionartId;
                        $scope.condition.facility = $scope.facilityID;
                    } else {
                        $scope.condition.ictcId = $scope.conditionictcId;
                        $scope.condition.artId = '';
                        $scope.condition.facility = $scope.facilityID;
                    }


//                    Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
//                        $scope.statusName = sts.name;
//                    });
//
//                    Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
//                        $scope.stepName = stp.name;
//                    });
                    
                    if ($window.sessionStorage.language == 1) {


                    Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });

                    Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });

                } else {

                    Restangular.one('conditionstatuses?filter[where][parentId]=' + $scope.trailerArray[g].status).get().then(function (sts) {

                        $scope.statusName = sts[0].name;
                    });

                    Restangular.one('conditionsteps?filter[where][parentId]=' + $scope.trailerArray[g].id).get().then(function (stp) {
                        $scope.stepName = stp[0].name;
                    });


                }

                } else if ($scope.trailerArray[g].status == 35 || $scope.trailerArray[g].status == 40) {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;

                    if ($scope.ConditionId == 51 && $scope.condition.step == 34 && $scope.condition.status == 36) {
                        // $scope.condition.ictcId = '';
                        // $scope.condition.artId = $scope.conditionartId;
                        $scope.condition.ictcId = $scope.conditionictcId;
                        $scope.condition.artId = $scope.conditionartId;
                        $scope.condition.facility = $scope.facilityID;
                    } else if ($scope.ConditionId == 51 && ($scope.condition.step == 34 || $scope.condition.step == 35 || $scope.condition.step == 36 || $scope.condition.step == 37)) {
                        $scope.condition.ictcId = $scope.conditionictcId;
                        $scope.condition.artId = $scope.conditionartId;
                        $scope.condition.facility = $scope.facilityID;
                    } else {
                        $scope.condition.ictcId = $scope.conditionictcId;
                        $scope.condition.artId = '';
                        $scope.condition.facility = $scope.facilityID;
                    }

//                    Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
//                        $scope.statusName = sts.name;
//                    });
//
//
//
//                    Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
//                        $scope.stepName = stp.name;
//                    });
                    
                    if ($window.sessionStorage.language == 1) {


                    Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });

                    Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });

                } else {

                    Restangular.one('conditionstatuses?filter[where][parentId]=' + $scope.trailerArray[g].status).get().then(function (sts) {

                        $scope.statusName = sts[0].name;
                    });

                    Restangular.one('conditionsteps?filter[where][parentId]=' + $scope.trailerArray[g].id).get().then(function (stp) {
                        $scope.stepName = stp[0].name;
                    });


                }

                }
            }


            if ($scope.hideReason === false) {
                $scope.condition.reasonForCancel = null;
                $scope.reasonForCancelName = '';
            }

            $scope.followUpDate = $filter('date')($scope.condition.followupdate, 'dd-MMM-yyyy');
            console.log('$scope.hideReason', $scope.hideReason);
            console.log('$scope.hideFollowUp', $scope.hideFollowUp);

            if ($scope.condition.membername == '' || $scope.condition.membername == null) {
                $scope.validatestring = $scope.validatestring + $scope.entermemberName;
                //$scope.ConditionLanguage.pleaseSelectMember;

            } else if ($scope.condition.tiId == '' || $scope.condition.tiId == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteryourId;
                //$scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.condition.relation == '' || $scope.condition.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectingrelationship;
                //$scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.condition.condition == '' || $scope.condition.condition == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectingCondition;
                //$scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.hideReason === true) {
                if ($scope.condition.reasonForCancel == '' || $scope.condition.reasonForCancel == null) {
                    $scope.validatestring = $scope.validatestring + $scope.pleaseselectareason;
                    //$scope.ConditionLanguage.reasonForCancelling;
                }
            } else if ($scope.hideFollowUp === true) {
                if ($scope.condition.followup == '' || $scope.condition.followup == null) {
                    $scope.validatestring = $scope.validatestring + $scope.pleaseselectafollowup;
                    //$scope.ConditionLanguage.pleaseSelectFollowup;
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                //  console.log($scope.condition);


                Restangular.one('conditionheaders?filter[where][condition]=' + $scope.condition.condition + '&filter[where][memberId]=' + $scope.condition.memberId + '&filter[where][deleteflag]=false').get().then(function (cndnResp) {
                    if (cndnResp.length > 0) {
                        $scope.toggleValidation();
                        $scope.validatestring1 = $scope.conditionexists;
                    } else {
                        $scope.confirmationModel = true;
                    }
                });


            }
        };
        /******************************************/

        var tArray = 0;

        $scope.Save = function () {

            for (var h = 0; h < $scope.trailerArray.length; h++) {

                if ($scope.trailerArray[h].enabled == false && ($scope.trailerArray[h].status == 38 || $scope.trailerArray[h].status == 40)) {
                    //$scope.trailerArray[h].status == 39 ||
                    $scope.trailerArray[h].enabled = true;
                    tArray++;
                } else {
                    tArray++;
                }

                if (tArray == $scope.trailerArray.length) {
                    $scope.saveFunc();
                }
            }
        };


        $scope.memberupdate = {};
        $scope.memberupdate.completedflag = true;

        $scope.saveFunc = function () {



            $scope.confirmationModel = false;

            $scope.toggleLoading();
            //  $scope.condition.associatedHF = $scope.associatedHF;
            Restangular.all('conditionheaders').post($scope.condition).then(function (response) {
                $window.sessionStorage.headerId = response.id;
                Restangular.one('beneficiaries', $scope.memberUpdateId).customPUT($scope.memberupdate).then(function (updateResponse) {
                    // console.log(response);
                    $scope.saveTrailer(response.id, response.memberId, response.condition, response.ictcId, response.artId);
                });
            });
        };

        $scope.saveCount = 0;
        $scope.trailerpost = {};

        $scope.saveTrailer = function (headerId, memberId, condition, artId, ictcId) {
            if ($scope.saveCount < $scope.trailerArray.length) {
                $scope.trailerpost.memberId = memberId;
                $scope.trailerpost.condition = condition;
                $scope.trailerpost.conditionHeaderId = headerId;
                $scope.trailerpost.artId = artId;
                $scope.trailerpost.ictcId = ictcId;
                $scope.trailerpost.step = $scope.trailerArray[$scope.saveCount].id;
                $scope.trailerpost.status = $scope.trailerArray[$scope.saveCount].status;
                $scope.trailerpost.enabled = $scope.trailerArray[$scope.saveCount].enabled;
                $scope.trailerpost.actionable = $scope.trailerArray[$scope.saveCount].actionable;
                $scope.trailerpost.hidelist = $scope.trailerArray[$scope.saveCount].hidelist;
                $scope.trailerpost.createdDate = $scope.trailerArray[$scope.saveCount].date;
                $scope.trailerpost.countryId = $window.sessionStorage.countryId;
                $scope.trailerpost.state = $window.sessionStorage.stateId;
                $scope.trailerpost.district = $window.sessionStorage.districtId;
                $scope.trailerpost.town = $window.sessionStorage.distributionAreaId;
                $scope.trailerpost.site = $scope.site;
                $scope.trailerpost.createdDate = new Date();
                $scope.trailerpost.createdBy = $window.sessionStorage.userId;
                $scope.trailerpost.createdByRole = $window.sessionStorage.roleId;
                $scope.trailerpost.lastModifiedDate = new Date();
                $scope.trailerpost.lastModifiedBy = $window.sessionStorage.userId;
                $scope.trailerpost.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.trailerpost.associatedHF = $scope.condition.associatedHF;
                $scope.trailerpost.facility = $scope.facilityID;
                $scope.trailerpost.facilityId = $scope.condition.facilityId;
                $scope.trailerpost.deleteflag = false;

                if ($scope.trailerpost.enabled == true) {
                    $scope.trailerpost.enabled = false;
                } else {
                    $scope.trailerpost.enabled = true;
                }

                Restangular.all('conditiontrailers').post($scope.trailerpost).then(function (resp) {
                    // console.log(response);
                    $scope.saveCount++;
                    $scope.saveTrailer(headerId, memberId, condition);
                });
            } else {
                $scope.saveAuditTrail(headerId, memberId, condition);
            }
        };


        $scope.auidtCount = 0;

        $scope.saveAuditTrail = function (headerId, memberId, condition) {
            if ($scope.auidtCount < $scope.auditArray.length) {

                $scope.auditArray[$scope.auidtCount].entityid = headerId;
                $scope.auditArray[$scope.auidtCount].description = 'condition Create';
                $scope.auditArray[$scope.auidtCount].modifiedbyroleid = $window.sessionStorage.roleId;
                $scope.auditArray[$scope.auidtCount].modifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.auditArray[$scope.auidtCount].lastmodifiedtime = new Date();
                $scope.auditArray[$scope.auidtCount].state = $window.sessionStorage.zoneId;
                $scope.auditArray[$scope.auidtCount].district = $window.sessionStorage.salesAreaId;
                $scope.auditArray[$scope.auidtCount].facility = $window.sessionStorage.coorgId;
                $scope.auditArray[$scope.auidtCount].facilityId = $scope.auditlog.facilityId;

                Restangular.all('auditlogs').post($scope.auditArray[$scope.auidtCount]).then(function (auditresp) {
                    $scope.auidtCount++;
                    $scope.saveAuditTrail(headerId, memberId, condition);
                });
            } else {
                $scope.CreateClicked = true;
                Restangular.one('roles', $window.sessionStorage.roleId).get().then(function (respRole) {
                    $scope.role = respRole;
                    if (respRole.enablePR == true) {
                        $scope.role.enablePR = true;

                        $scope.modalInstanceLoad.close();
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('reloading...');
                        $scope.HideCreateButton = true;
                        $scope.CreateClicked = true;
                        $scope.stakeholderdataModal = false;
                    } else {

                        $scope.modalInstanceLoad.close();
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('reloading...');

                        setTimeout(function () {
                            $window.sessionStorage.MemberId = '';
                            $window.sessionStorage.fullName = '';
                            $window.sessionStorage.relation = '';
                            $window.sessionStorage.conditionId = '';
                            $window.sessionStorage.conditionIdnew = '';
                            window.location = "/conditions-list";
                        }, 350);
                    }

                });

            }
        };



        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.dt1 = new Date();

        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        //        $scope.clear = function () {
        //            $scope.dt = null;
        //        };

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();
        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.condition.followupopened = true;
        };

        $scope.conditionfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.condition.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///
    })
    .directive('shgmeetingmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-md">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
