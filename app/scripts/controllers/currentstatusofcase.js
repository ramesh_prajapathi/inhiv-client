'use strict';

angular.module('secondarySalesApp')
	.controller('currentstatusofcaseCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
		/*********/
		//$scope.modalTitle = 'Thank You';
		//$scope.message = 'currentstatusofcase has been created!';
		$scope.showForm = function () {
			var visible = $location.path() === '/currentstatus/create' || $location.path() === '/currentstatus/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/currentstatus/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/currentstatus/create' || $location.path() === '/currentstatus/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/currentstatus/create' || $location.path() === '/currentstatus/' + $routeParams.id;
			return visible;
		};
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}


		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		}


		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		if ($window.sessionStorage.prviousLocation != "partials/currentstatusofcase") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPagesize = 25;
			$window.sessionStorage.myRoute_currentPage = 1;
			$scope.currentpage = 1;
			$scope.pageSize = 25;
		}

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
		/*********************************** INDEX *******************************************/

		$scope.part = Restangular.all('currentstatusofcases?filter[where][deleteflag]=false').getList().then(function (part) {
			$scope.currentstatusofcases = part;
			$scope.currentstatusofcaseId = $window.sessionStorage.sales_currentstatusofcaseId;
			angular.forEach($scope.currentstatusofcases, function (member, index) {
				member.index = index + 1;

				$scope.TotalTodos = [];
				$scope.TotalTodos.push(member);
			});
		});

		/*-------------------------------------------------------------------------------------*/
		$scope.currentstatusofcase = {
			deleteflag: false,
			name: ''
		};

		$scope.statecodeDisable = false;
		$scope.membercountDisable = false;
		if ($routeParams.id) {
			$scope.message = 'Current status of the case has been Updated!';
			$scope.statecodeDisable = true;
			$scope.membercountDisable = true;
			Restangular.one('currentstatusofcases', $routeParams.id).get().then(function (currentstatusofcase) {
				$scope.original = currentstatusofcase;
				$scope.currentstatusofcase = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Current status of the case has been Updated!';
		}
		/************* SAVE *******************************************/

		$scope.submitDisable = false;
		$scope.validatestring = '';
		$scope.Savecurrentstatusofcase = function (clicked) {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.currentstatusofcase.name == '' || $scope.currentstatusofcase.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.hnname == '' || $scope.currentstatusofcase.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.knname == '' || $scope.currentstatusofcase.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.taname == '' || $scope.currentstatusofcase.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.tename == '' || $scope.currentstatusofcase.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.mrname == '' || $scope.currentstatusofcase.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.all('currentstatusofcases').post($scope.currentstatusofcase).then(function (Response) {
					window.location = '/currentstatusofcase';

				});
			}
		};

		/***************************** UPDATE *******************************************/
		$scope.Updatecurrentstatusofcase = function () {
			document.getElementById('name').style.border = "";
			document.getElementById('hnname').style.border = "";
			document.getElementById('knname').style.border = "";
			document.getElementById('taname').style.border = "";
			document.getElementById('tename').style.border = "";
			document.getElementById('mrname').style.border = "";
			if ($scope.currentstatusofcase.name == '' || $scope.currentstatusofcase.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case';
				document.getElementById('name').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.hnname == '' || $scope.currentstatusofcase.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in hindi';
				document.getElementById('hnname').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.knname == '' || $scope.currentstatusofcase.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in kannada';
				document.getElementById('knname').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.taname == '' || $scope.currentstatusofcase.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in tamil';
				document.getElementById('taname').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.tename == '' || $scope.currentstatusofcase.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in telugu';
				document.getElementById('tename').style.borderColor = "#FF0000";

			} else if ($scope.currentstatusofcase.mrname == '' || $scope.currentstatusofcase.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter Current status of the case in marathi';
				document.getElementById('mrname').style.borderColor = "#FF0000";

			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				Restangular.one('currentstatusofcases', $routeParams.id).customPUT($scope.currentstatusofcase).then(function () {
					window.location = '/currentstatusofcase';
				});
			}
		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};

		/*---------------------------Delete---------------------------------------------------*/

		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('currentstatusofcases/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}


	});
