'use strict';

angular.module('secondarySalesApp')
    .controller('FWCtrl', function ($scope, Restangular, $route, $window, $modal, $http, $filter) {
        /**************************/


//        if ($window.sessionStorage.roleId != 3) {
//            window.location = "/";
//        }
    
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined || $window.sessionStorage.sales_zoneId == null || $window.sessionStorage.sales_zoneId == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.sales_zoneId == null
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        if ($window.sessionStorage.prviousLocation != "partials/f-work-form") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
    
    
    //new changes
     Restangular.one('orwlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.orwlanguages = langResponse[0];
          //$scope.modalInstanceLoad.close();
            $scope.TIHeading = $scope.orwlanguages.trCreate;
            // $scope.modalTitle = $scope.RILanguage.thankYou;
            $scope.message = $scope.orwlanguages.thankYouCreated;
        
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        /*****************************************************************************/
        $scope.auditlog = {
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 51,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId
        };

        $scope.toggleLoading = function () {
            $scope.modalInstanceLoad = $modal.open({
                animation: true,
                templateUrl: 'template/LodingModal.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm',
                keyboard: false

            });
        };


        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.auditlog.facilityId = comember.id;
        });


        $scope.getassignedSites = function (siteids) {
            if (siteids != null || siteids != '' || siteids != 'undefined') {
                return Restangular.all('distribution-routes?filter={"where":{"id":{"inq":[' + siteids + ']}}}').getList().$object;
            }
        };


        /* $scope.FirstNameFilter = function () {
             $scope.backgroundCol1 = "blue";
             $scope.backgroundCol2 = "";
             $scope.partners = [];
             $scope.zn = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=lastname%20ASC').getList().then(function (zn) {
                 $scope.partners = zn;
                 $scope.modalInstanceLoad.close();
                 angular.forEach($scope.partners, function (member, index) {
                     member.index = index + 1;
                     
                     $scope.TotalData = [];
                     $scope.TotalData.push(member);

                 });

                 //});
             });
         }*/

        /*  $scope.UserNameFilter = function () {
                  $scope.backgroundCol1 = "";
                  $scope.backgroundCol2 = "blue";
                  $scope.partners = [];
                  $scope.zn = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (zn) {
                      $scope.partners = zn;
                      $scope.modalInstanceLoad.close();
                      angular.forEach($scope.partners, function (member, index) {
                          member.index = index + 1;
                          
                          $scope.TotalData = [];
                          $scope.TotalData.push(member);

                      });

                      //});
                  });
              }*/
        /************************************************************ INDEX *************************************/

        /*$scope.getassignedSites = function (siteids) {
			if (siteids != null || siteids != '' || siteids != 'undefined') {
				return Restangular.all('distribution-routes?filter={"where":{"id":{"inq":[' + siteids + ']}}}').getList().$object;
			}
		};*/
        /*$scope.dr = Restangular.all('distribution-routes?filter[where][partnerId]=' + $window.sessionStorage.coorgId + '&filter[where][deleteflag]=false').getList().then(function (dr) {
			$scope.distributionroutes = dr;*/
        $scope.partners = [];
     Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
        console.log('comember', comember);
        $scope.zn = Restangular.all('comembers?filter[where][deleteflag]=false' + '&filter[where][facilityId]=' + comember.id).getList().then(function (zn) {
            $scope.partners = zn;

            //$scope.modalInstanceLoad.close();
            angular.forEach($scope.partners, function (member, index) {
                member.index = index + 1;
                /*for (var n = 0; n < $scope.distributionroutes.length; n++) {
					if (member.sitesassigned == $scope.distributionroutes[n].id) {
						member.Sitename = $scope.distributionroutes[n];
						break;
					}
				}*/
                $scope.TotalData = [];
                $scope.TotalData.push(member);

            });

            //});
        });
     });

        /********************/
        /* $scope.sortingOrder = sortingOrder;
         $scope.reverse = false;

         $scope.sort_by = function (newSortingOrder) {
             if ($scope.sortingOrder == newSortingOrder)
                 $scope.reverse = !$scope.reverse;

             $scope.sortingOrder = newSortingOrder;
         };*/

       /* $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;
                console.log('m in if');

            } else {
                sort.active = column;
                sort.descending = false;
                console.log('m in else');

                
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'glyphicon-chevron-up' : 'glyphicon-chevron-down';
            }

            // return 'glyphicon-star';
        }*/

        /**************************/
        var regex = /^([a-z]*)(\d*)/i;
        $scope.boolAsc = false;
        $scope.boolAscOne = false;

        $scope.changeSorting = function (col) {
            if (col == 'firstname') {
                if ($scope.boolAsc) {
                    // console.log('i am in 1 if');
                    $scope.ascendingSorting(col)
                } else {
                    // console.log('i am in 1 else');
                    $scope.descendingSorting(col)
                }
            } else {
                if ($scope.boolAscOne) {
                    // console.log('i am in 2 if');
                    $scope.ascendingSorting(col)
                } else {
                    //  console.log('i am in 2 else');
                    $scope.descendingSorting(col)
                }
            }

        }

        /**********Icon***********/
        $scope.getIcon = function (column) {

            if ($scope.boolAsc) {
                return 'fa-sort-up';

            } else {
                return 'fa-sort-desc';
            }
        }
        $scope.getIconOne = function (colnew) {

            if ($scope.boolAscOne) {
                return 'fa-sort-up';

            } else {
                return 'fa-sort-desc';
            }
        }

        /*******************/
        $scope.ascendingSorting = function (column) {
            var x = column;

            function sortFn(a, b) {
                var _a = a[x].match(regex);
                var _b = b[x].match(regex);

                // if the alphabetic part of a is less than that of b => -1
                if (_a[1] < _b[1]) return -1;
                // if the alphabetic part of a is greater than that of b => 1
                if (_a[1] > _b[1]) return 1;

                // if the alphabetic parts are equal, check the number parts
                var _n = parseInt(_a[2]) - parseInt(_b[2]);
                if (_n == 0) // if the number parts are equal start a recursive test on the rest
                    return sortFn(a[x](_a[0].length), b[x](_b[0].length));
                // else, just sort using the numbers parts
                return _n;
            }
            // $scope.boolAsc = !$scope.boolAsc;
            // $scope.partners.sort(sortFn);
            if (x == 'firstname') {
                //console.log(' i m in boolAsc');
                $scope.boolAsc = !$scope.boolAsc;
                $scope.partners.sort(sortFn);
            } else {
                // console.log(' i m in boolAscOne');
                $scope.boolAscOne = !$scope.boolAscOne;
                $scope.partners.sort(sortFn);
            }
        };

        $scope.descendingSorting = function (column) {
            var y = column;


            function sortFn(a, b) {
                var _a = a[y].match(regex);
                var _b = b[y].match(regex);

                // if the alphabetic part of a is less than that of b => -1
                if (_a[1] < _b[1]) return 1;
                // if the alphabetic part of a is greater than that of b => 1
                if (_a[1] > _b[1]) return -1;

                // if the alphabetic parts are equal, check the number parts
                var _n = parseInt(_b[2]) - parseInt(_a[2]);
                if (_n == 0) // if the number parts are equal start a recursive test on the rest
                    return sortFn(a[y](_a[0].length), b[y](_b[0].length));
                // else, just sort using the numbers parts
                return _n;
            }
            if (y == 'firstname') {
                // console.log(' i m in boolAsc');
                $scope.boolAsc = !$scope.boolAsc;
                $scope.partners.sort(sortFn);
            } else {
                // console.log(' i m in boolAscOne');
                $scope.boolAscOne = !$scope.boolAscOne;
                $scope.partners.sort(sortFn);
            }
            // $scope.boolAsc = !$scope.boolAsc;
            // $scope.partners.sort(sortFn);
        };

        /************************** DELETE *******************************************/

        $scope.Delete = function (id) {
            Restangular.one('comembers/' + id).get().then(function (Responsefw) {
                console.log('Responsefw', Responsefw);
                $scope.getSiteassigned = Responsefw.sitesassigned;
                console.log('$scope.getSiteassigned', $scope.getSiteassigned);
                if ($scope.getSiteassigned != null) {
                    alert('This FW having site, You can not delete');
                } else {
                    $scope.auditlog.entityid = id;
                    var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
                    $scope.auditlog.description = 'Field Worker Deleted With Following Details: ' + 'Deleted By UserId - ' + $window.sessionStorage.userId + ', ' + 'Field Worker Id - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

                    Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {

                        $scope.item = [{
                            deleteflag: true,
                            lastmodifiedtime: new Date(),
                            modifiedby: $window.sessionStorage.UserEmployeeId
               }]
                        Restangular.one('comembers/' + id).customPUT($scope.item[0]).then(function () {
                            $route.reload();
                        });
                    });
                }
            });
        }

        $scope.getUser = function (employeeid) {
            return Restangular.one('users/findOne?filter[where][employeeid]=' + employeeid).get().$object;
        };



        /******************************** Language *********************************************/
        /***$scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.mandatoryfield = langResponse.mandatoryfield;
            $scope.date = langResponse.date;
            $scope.name = langResponse.name;
            $scope.username = langResponse.username;
            $scope.fullname = langResponse.fullname;
            $scope.printmobile = langResponse.mobile;
            $scope.action = langResponse.action;
            $scope.searchmember = langResponse.searchmember;
            $scope.organizedby = langResponse.organizedby;
            $scope.followuprequired = langResponse.followuprequired;
            $scope.membername = langResponse.membername;
            $scope.status = langResponse.status;
            $scope.followupdate = langResponse.followupdate;
            $scope.create = langResponse.create;
            $scope.update = langResponse.update;
            $scope.cancel = langResponse.cancel;
            $scope.site = langResponse.site;
            $scope.fieldworkercode = langResponse.fieldworkercode;
            $scope.fieldworkeremail = langResponse.fieldworkeremail;
            $scope.show = langResponse.show;
            $scope.entry = langResponse.entry;
            $scope.searchfor = langResponse.searchfor;
            $scope.addnew = langResponse.addnew;
            $scope.fieldworkerlist = langResponse.fieldworkerlist;
            $scope.fieldworkername = langResponse.fieldworkername;
            $scope.assignedsites = langResponse.assignedsites;

        }); ****/



    });
/************************************ Not In Use ***************************
		
//$scope.loading = true;
/*
$scope.part = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fw) {
		$scope.partners = fw;
		$scope.loading = false;
		$scope.modalInstanceLoad.close();
		angular.forEach($scope.partners, function (member, index) {
			member.index = index + 1;
		});
	});
	
	
	$scope.getassignedSites = function (siteids) {
	if (siteids != null || siteids != '' || siteids != 'undefined') {
			return Restangular.all('distribution-routes?filter={"where":{"id":{"inq":[' + siteids + ']}}}').getList().$object;
		}
	};
*/

/*************************** GET *********************************************
$scope.$watch('zoneId', function (newValue, oldValue) {
	$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue + '&filter[where][id]=' + $window.sessionStorage.salesAreaId).getList().$object;
});

$scope.$watch('salesAreaId', function (newValue, oldValue) {
	$scope.distributionAreas = Restangular.all('distribution-areas?filter[where][salesAreaId]=' + newValue).getList().$object;
});




$scope.zoneId = '';
$scope.zonalid = '';
$scope.salesAreaId = '';
$scope.salesid = '';
$scope.distributionAreaId = '';
$scope.distribtionareaid = '';
$scope.partnerId = '';
$scope.firstName = '';

$scope.$watch('zoneId', function (newValue, oldValue) {
	if (newValue === oldValue || newValue == '') {
		return;
	} else {
		$scope.salesAreas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;

		$scope.sal = Restangular.all('partners?filter[where][zoneId]=' + newValue + '&filter[where][groupId]=9').getList().then(function (sal) {
			$scope.partners = sal;
			angular.forEach($scope.partners, function (member, index) {
				member.index = index + 1;
			});
		});
		$scope.zonalid = +newValue;
	}
});

/****************************************** salesAreaId ************************************

$scope.$watch('salesAreaId', function (newValue, oldValue) {
	if (newValue === oldValue || newValue == '') {
		return;
	} else {

		$scope.filterpartners = Restangular.all('partners?filter[where][salesAreaId]=' + newValue + '&filter[where][groupId]=8').getList().$object;

		$scope.sal = Restangular.all('partners?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + newValue + '&filter[where][groupId]=9').getList().then(function (sal) {
			$scope.partners = sal;
			angular.forEach($scope.partners, function (member, index) {
				member.index = index + 1;
			});
		});

		$scope.salesid = +newValue;
	}
});


/****************************************** distributionAreaId ************************************

$scope.$watch('partnerId', function (newValue, oldValue) {
	if (newValue === oldValue || newValue == '') {
		return;
	} else {
		console.log('newValue', newValue);
		$scope.sal = Restangular.all('partners?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + $scope.salesid + '&filter[where][coorgId]=' + newValue + '&filter[where][groupId]=9').getList().then(function (sal) {
			$scope.partners = sal;
			angular.forEach($scope.partners, function (member, index) {
				member.index = index + 1;
			});
		});

		$scope.partid = +newValue;
	}
});



		$scope.employees = Restangular.all('employees').getList().$object;
		$scope.zones = Restangular.all('zones?filter[where][id]=' + $window.sessionStorage.zoneId).getList().$object;
		$scope.salesAreas = Restangular.all('sales-areas?filter[where][id]=' + $window.sessionStorage.salesAreaId).getList().$object;
		$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
		$scope.distributionSubAreas = Restangular.all('distribution-subareas').getList().$object;
		$scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
		$scope.searchPart = $scope.firstName;

/**************************************************************************************/
