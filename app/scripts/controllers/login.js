'use strict';
angular.module('secondarySalesApp').directive('loading', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="loading" style="color:black;"><img src="images/piclinks/loaderpink_blue.gif" width="20" height="20"  class="loadingwidth" /> <br/>LOADING...</div>',
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                if (val) $(element).show();
                else $(element).hide();
            });
        }
    }
}).directive('loading1', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="loading" style="color:#333;"><img src="images/piclinks/loaderpink_blue.gif" width="20" height="20" class="loadingwidth" /> </div>',
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                if (val) $(element).show();
                else $(element).hide();
            });
        }
    }
}).controller('LoginCtrl', function ($rootScope, $scope, $http, $window, $location, Restangular, $idle, $modal, AnalyticsRestangular, $filter) {
    //$scope.FilterDate = "2016-09-14T09:59:43.043Z";
    $scope.login = function () {
        $scope.loading = true;
        $scope.errormsg = 'hide';
        Restangular.one('users', 1);
        console.log('login');
        Restangular.all('users').login($scope.user).then(function (loginResult) {
           
            var session = $window.sessionStorage.loginResult = angular.toJson(loginResult);
            var sessionObj = angular.fromJson(session);
            $rootScope.accessToken = $window.sessionStorage.accessToken = sessionObj.id;
            $rootScope.currentUserId = $window.sessionStorage.userId = sessionObj.userId;
            $rootScope.currentUserTtl = $window.sessionStorage.userTtl = sessionObj.ttl;
        }).then(function () {
            //$location.path('/');
            /** AnalyticsRestangular.all('users').login($scope.user).then(function (loginResult) {
                // console.log('Analytics loginResult', loginResult.id);
                var analyticsAccessToken = loginResult.id;
                $rootScope.analyticsAccessToken = $window.sessionStorage.analyticsAccessToken = analyticsAccessToken;
                Restangular.one('users/' + $window.sessionStorage.userId + '?access_token=' + $window.sessionStorage.accessToken).get().then(function (customer) {
                    var name = customer.username;
                    var roleid = customer.roleId;
                    var countryId = customer.countryId;
                    var zoneId = customer.state;
                    var salesAreaId = customer.district;
                    var distributionAreaId = customer.town;
                    var coorgId = customer.tiId;
                    var groupId = customer.groupId;
                    var EmployeeId = customer.employeeid;
                    var language = customer.language;
                    var deleteFlag = customer.deleteflag;
                    if (EmployeeId == null) {
                        EmployeeId = 0
                    }
                    $rootScope.currentUserName = $window.sessionStorage.userName = name;
                    $rootScope.roleId = $window.sessionStorage.roleId = roleid;
                    $rootScope.countryId = $window.sessionStorage.countryId = countryId;
                    $rootScope.zoneId = $window.sessionStorage.zoneId = zoneId;
                    $rootScope.salesAreaId = $window.sessionStorage.salesAreaId = salesAreaId;
                    $rootScope.distributionAreaId = $window.sessionStorage.distributionAreaId = distributionAreaId;
                    $rootScope.coorgId = $window.sessionStorage.coorgId = coorgId;
                    $rootScope.groupId = $window.sessionStorage.groupId = groupId;
                    $rootScope.UserEmployeeId = $window.sessionStorage.UserEmployeeId = EmployeeId;
                    $rootScope.DeleteFlag = $window.sessionStorage.DeleteFlag = deleteFlag;
                    if (customer.language == null || customer.language === undefined || customer.language === '') {
                        $rootScope.language = $window.sessionStorage.language = 1;
                    } else {
                        $rootScope.language = $window.sessionStorage.language = language;
                    }
                }).then(function () {
                    //$location.path('/');
                    if ($window.sessionStorage.roleId == 1) {
                        $window.sessionStorage.countryId = 0;
                        $window.sessionStorage.zoneId = 0;
                        $window.sessionStorage.salesAreaId = 0;
                        $window.sessionStorage.distributionAreaId = 0;
                        $window.sessionStorage.coorgId = 0;
                    }
                     if ($window.sessionStorage.countryId == 'null') {
                        $window.sessionStorage.countryId = 0;
                    }
                    
                    if ($window.sessionStorage.zoneId == 'null') {
                        $window.sessionStorage.zoneId = 0;
                    }
                    if ($window.sessionStorage.salesAreaId == 'null') {
                        $window.sessionStorage.salesAreaId = 0;
                    }
                    
                    if ($window.sessionStorage.distributionAreaId == 'null') {
                        $window.sessionStorage.distributionAreaId = 0;
                    }
                    if ($window.sessionStorage.coorgId == 'null') {
                        $window.sessionStorage.coorgId = 0;
                    }
                    $scope.userlogindetail = {
                        "userid": $window.sessionStorage.userId,
                        "platform": "web",
                        "operationflag": "login",
                        "datetime": new Date(),
                        "role": $window.sessionStorage.roleId,
                        "countryId": $window.sessionStorage.countryId,
                        "state": $window.sessionStorage.zoneId,
                        "district": $window.sessionStorage.salesAreaId,
                        "town": $window.sessionStorage.distributionAreaId,
                        "tiId": $window.sessionStorage.coorgId,
                        "employeeid": $window.sessionStorage.UserEmployeeId
                    }
                    // console.log('$scope.userlogindetail',$scope.userlogindetail);
                    Restangular.all('userlogindetails?access_token=' + $window.sessionStorage.accessToken).post($scope.userlogindetail).then(function (userlogindetail) {
                        console.log('$window.sessionStorage.DeleteFlag', $window.sessionStorage.DeleteFlag);
                        if ($window.sessionStorage.roleId != 5) {
                            if ($window.sessionStorage.DeleteFlag == 'true') {
                                $scope.loading = false;
                               // console.log('i am here');
                                $scope.logout();
                            } //else if ($window.sessionStorage.roleId == 5 || $window.sessionStorage.roleId == 6) {
                            else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 5) {

                                Restangular.all('multilanguages?filter[where][id]=' + $window.sessionStorage.language + '&access_token=' + $window.sessionStorage.accessToken).getList().then(function (langResponse) {
                                    $scope.broadcastmessages = langResponse[0].broadcastmessages;
                                });
                                $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                                Restangular.all('events?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][deleteflag]=false&access_token=' + $window.sessionStorage.accessToken).getList().then(function (resEvent) {
                                    //console.log('resEvent', resEvent);
                                    $scope.events = resEvent;
                                    if (resEvent.length > 0) {
                                        $scope.Brodcast();
                                    } else {
                                        //  window.location = "/codashboard";
                                        window.location = "/";
                                    }
                                });
                                //window.location = "/codashboard";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            }
                            
                            else {
                                window.location = "/";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            }
                        } else {
                            //window.location = "/";
                            $scope.loading = false;
                            $scope.logout();
                        }
                    });
                });
            }, function (analyticsloginfail) {*/
                Restangular.one('users/' + $window.sessionStorage.userId + '?access_token=' + $window.sessionStorage.accessToken).get().then(function (customer) {
                     console.log('customer', customer);
                   var name = customer.username;
                    var roleid = customer.roleId;
                    var countryId = customer.countryId;
                    var zoneId = customer.state;
                    var salesAreaId = customer.district;
                    var distributionAreaId = customer.town;
                    var coorgId = customer.tiId;
                    var groupId = customer.groupId;
                    var EmployeeId = customer.employeeid;
                    var language = customer.language;
                    var deleteFlag = customer.deleteflag;
                    if (EmployeeId == null) {
                        EmployeeId = 0
                    }
                   $rootScope.currentUserName = $window.sessionStorage.userName = name;
                    $rootScope.roleId = $window.sessionStorage.roleId = roleid;
                    $rootScope.countryId = $window.sessionStorage.countryId = countryId;
                    $rootScope.zoneId = $window.sessionStorage.zoneId = zoneId;
                    $rootScope.salesAreaId = $window.sessionStorage.salesAreaId = salesAreaId;
                    $rootScope.distributionAreaId = $window.sessionStorage.distributionAreaId = distributionAreaId;
                    $rootScope.coorgId = $window.sessionStorage.coorgId = coorgId;
                    $rootScope.groupId = $window.sessionStorage.groupId = groupId;
                    $rootScope.UserEmployeeId = $window.sessionStorage.UserEmployeeId = EmployeeId;
                    $rootScope.DeleteFlag = $window.sessionStorage.DeleteFlag = deleteFlag;
                    if (customer.language == null || customer.language === undefined || customer.language === '') {
                        $rootScope.language = $window.sessionStorage.language = 1;
                    } else {
                        $rootScope.language = $window.sessionStorage.language = language;
                    }
                }).then(function () {
                    //$location.path('/');
                    if ($window.sessionStorage.roleId == 1) {
                        $window.sessionStorage.countryId = 0;
                        $window.sessionStorage.zoneId = 0;
                        $window.sessionStorage.salesAreaId = 0;
                        $window.sessionStorage.distributionAreaId = 0;
                        $window.sessionStorage.coorgId = 0;
                    }
                     if ($window.sessionStorage.countryId == 'null') {
                        $window.sessionStorage.countryId = 0;
                    }
                    
                    if ($window.sessionStorage.zoneId == 'null') {
                        $window.sessionStorage.zoneId = 0;
                    }
                    if ($window.sessionStorage.salesAreaId == 'null') {
                        $window.sessionStorage.salesAreaId = 0;
                    }
                    
                    if ($window.sessionStorage.distributionAreaId == 'null') {
                        $window.sessionStorage.distributionAreaId = 0;
                    }
                    if ($window.sessionStorage.coorgId == 'null') {
                        $window.sessionStorage.coorgId = 0;
                    }
                    $scope.userlogindetail = {
                         "userid": $window.sessionStorage.userId,
                        "platform": "web",
                        "operationflag": "login",
                        "datetime": new Date(),
                        "role": $window.sessionStorage.roleId,
                        "countryId": $window.sessionStorage.countryId,
                        "state": $window.sessionStorage.zoneId,
                        "district": $window.sessionStorage.salesAreaId,
                        "town": $window.sessionStorage.distributionAreaId,
                        "tiId": $window.sessionStorage.coorgId,
                        "employeeid": $window.sessionStorage.UserEmployeeId
                    }
                    // console.log('$scope.userlogindetail',$scope.userlogindetail);
                    Restangular.all('userlogindetails?access_token=' + $window.sessionStorage.accessToken).post($scope.userlogindetail).then(function (userlogindetail) {
                        console.log('$window.sessionStorage.DeleteFlag', $window.sessionStorage.DeleteFlag);
                        console.log('$window.sessionStorage.roleId', $window.sessionStorage.roleId);
                        if ($window.sessionStorage.roleId != 5) {
                            console.log('first if');
                            if ($window.sessionStorage.DeleteFlag == 'true') {
                                 console.log('2nd if');
                                $scope.loading = false;
                                $scope.logout();
                            } else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 5) {
                                 console.log('3nd if');

                                Restangular.all('multilanguages?filter[where][id]=' + $window.sessionStorage.language + '&access_token=' + $window.sessionStorage.accessToken).getList().then(function (langResponse) {
                                    $scope.broadcastmessages = langResponse[0].broadcastmessages;
                                });
                                $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                                Restangular.all('events?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][deleteflag]=false&access_token=' + $window.sessionStorage.accessToken).getList().then(function (resEvent) {
                                    //console.log('resEvent', resEvent);
                                    $scope.events = resEvent;
                                    if (resEvent.length > 0) {
                                        $scope.Brodcast();
                                    } else {
                                        //  window.location = "/codashboard";
                                        window.location = "/";
                                    }
                                });
                                //window.location = "/codashboard";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            }
                            /**else if ($window.sessionStorage.roleId == 3) {
                                window.location = "/spmdashboard";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            } else if ($window.sessionStorage.roleId == 4) {
                                window.location = "/rodashboard";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            } else if ($window.sessionStorage.roleId == 16) {
                                window.location = "/nodashboard";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            } else if ($window.sessionStorage.roleId == 17) {
                                window.location = "/fsmentordashboard";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            } **/
                            else {
                                window.location = "/";
                                console.log('Login Sucessfull');
                                $scope.loading = false;
                            }
                        } else {
                            window.location = "/";
                            console.log('i m in else');
                            $scope.loading = false;
                           // $scope.logout();
                        }
                    });
                });
           // });
            //$scope.loading = false;
        }, function (response) {
            console.log('response', response);
            //alert(response.statusText);
            //$scope.errormsg = 'Invalid Username or Password';
            $scope.errormsg = 'show';
            $scope.loading = false;
        });
    };
    $scope.logout = function () {
        //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
        Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
            $window.sessionStorage.userId = '';
            console.log('Logout');
        }).then(function (redirect) {
            window.location = "/login";
            alert('There is No Access for this User');
            $idle.unwatch();
        });
    };
    $scope.showModal1 = function () {
        $scope.HideSearch = true;
        $scope.HideError = true;
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/forgotpassword.html',
            scope: $scope
        });
    };
    $scope.Brodcast = function () {
        console.log('Brodcast');
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/Brodcast_Message.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg'
        });
    };
    $scope.getEvent = function () {}
    $scope.ok1 = function () {
        $scope.modalInstance1.close();
    };
    $scope.OKBDM = function () {
        $scope.modalInstance1.close();
        window.location = "/codashboard";
    }
    $scope.data = {};
    $scope.GetUserId = function () {
        $scope.HideError = true;
        $scope.HideUser = $scope.data.forgotusername;
        Restangular.all('users?filter[where][username]=' + $scope.data.forgotusername).getList().then(function (detail) {
            if (detail.length > 0) {
                $scope.HideSearch = false;
                $scope.data.usermobile = detail[0].mobile;
                $scope.data.useremail = detail[0].email;
                $scope.data.userfirstname = detail[0].firstname;
                $scope.userId = detail[0].id;
            } else {
                $scope.data.forgotusername = '';
                $scope.HideError = false;
            }
        });
    };
    $scope.ResetPassword = function () {
        var randompswd = Math.floor(Math.random() * (666666 - 111111 + 111111)) + 111111;
        $scope.newdata = {
            password: $scope.stringGen(6)
        };
        console.log($scope.data.usermobile);
        if ($scope.data.usermobile != null && $scope.data.usermobile != '') {
            Restangular.one('users/' + $scope.userId).customPUT($scope.newdata).then(function (response) {
                console.log('$scope.newdata', $scope.newdata);
                console.log('response', response);
                $http.post("http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=socialprotection&passwd=spsw@sti1&mobilenumber=91" + $scope.data.usermobile + "&message=Your new password is " + $scope.newdata.password + "&sid=919164022220&mtype=N&DR=Y").success(function (data, status) {
                    // console.log('status', status);
                });
                if ($scope.data.useremail != null) {
                    $scope.SendMail($scope.newdata.password, $scope.data.useremail, $scope.data.userfirstname);
                }
                $scope.ok1();
            });
        } else if ($scope.data.useremail != null) {
            $scope.SendMail($scope.newdata.password, $scope.data.useremail, $scope.data.userfirstname);
            $scope.ok1();
        } else {
            $scope.data.usermobile = 'Invalid Mobile Number';
        }
    };
    $scope.stringGen = function (len) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    $scope.SendMail = function (newpassword, email, name) {
        var mailJSON = {
            "key": "so5UoRRwxBY6vS_iIDz-GQ",
            "message": {
                "html": "<!DOCTYPE html><html><body><h2>Your New Password is " + newpassword + "</h2></body></html>",
                "text": "New Password",
                "subject": "New Password",
                "from_email": "hunterabhi246@gmail.com",
                "from_name": "Suren",
                "to": [
                    {
                        "email": email,
                        "name": name,
                        "type": "to"
            }
        ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null
            },
            "async": false,
            "ip_pool": "Main Pool"
        };
        var apiURL = "https://mandrillapp.com/api/1.0/messages/send.json";
        $http.post(apiURL, mailJSON).
        success(function (data, status, headers, config) {
            console.log('successful email send.');
            $scope.form = {};
            console.log('successful email send.');
            console.log('status: ' + status);
            console.log('data: ' + JSON.stringify(data));
            console.log('headers: ' + JSON.stringify(headers));
            console.log('config: ' + config);
        }).error(function (data, status, headers, config) {
            console.log('error sending email.');
            console.log('status: ' + status);
            console.log('data: ' + JSON.stringify(data));
        });
    }
}).directive('notification', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        controller: ['$scope', function ($scope) {
            $scope.notification = {
                status: 'hide',
                type: 'danger',
                message: 'Invalid Username or Password!!!'
            };
    }],
        link: function (scope, elem, attrs) {
            // watch for changes
            attrs.$observe('notification', function (value) {
                if (value === 'show') {
                    // shows alert
                    $(elem).slideDown();
                    // and after 3secs
                    $timeout(function () {
                        // hide it
                        $(elem).slideUp();
                        // and update the show property
                        scope.notification.status = 'hide';
                    }, 1000);
                }
            });
        }
    };
  }]);
