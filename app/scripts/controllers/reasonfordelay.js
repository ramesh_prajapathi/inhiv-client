'use strict';
angular.module('secondarySalesApp').controller('reasonfordelayCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    /*********/
    //$scope.modalTitle = 'Thank You';
    //$scope.message = 'reasonfordelay has been created!';
    $scope.showForm = function () {
        var visible = $location.path() === '/reasonfordelay/create' || $location.path() === '/reasonfordelay/' + $routeParams.id;
        return visible;
    };
    $scope.isCreateView = function () {
        if ($scope.showForm()) {
            var visible = $location.path() === '/reasonfordelay/create';
            return visible;
        }
    };
    $scope.hideCreateButton = function () {
        var visible = $location.path() === '/reasonfordelay/create' || $location.path() === '/reasonfordelay/' + $routeParams.id;
        return visible;
    };
    $scope.hideSearchFilter = function () {
        var visible = $location.path() === '/reasonfordelay/create' || $location.path() === '/reasonfordelay/' + $routeParams.id;
        return visible;
    };
    if ($window.sessionStorage.roleId != 1) {
        window.location = "/";
    }
    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 5;
    }
    else {
        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    if ($window.sessionStorage.prviousLocation != "partials/reasonfordelay") {
        $window.sessionStorage.myRoute = '';
        $window.sessionStorage.myRoute_currentPagesize = 5;
        $window.sessionStorage.myRoute_currentPage = 1;
        $scope.currentpage = 1;
        $scope.pageSize = 5;
    }
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /*********************************** INDEX *******************************************/
    $scope.part = Restangular.all('reasonfordelayed?filter[where][deleteflag]=false').getList().then(function (part) {
        $scope.reasonfordelayed = part;
        $scope.reasonfordelayId = $window.sessionStorage.sales_reasonfordelayId;
        angular.forEach($scope.reasonfordelayed, function (member, index) {
            member.index = index + 1;
            $scope.TotalTodos = [];
            $scope.TotalTodos.push(member);
        });
    });
    /*-------------------------------------------------------------------------------------*/
    $scope.reasonfordelay = {
        deleteflag: false
    };
    $scope.statecodeDisable = false;
    $scope.membercountDisable = false;
    if ($routeParams.id) {
        $scope.message = 'Reason for delay has been Updated!';
        $scope.statecodeDisable = true;
        $scope.membercountDisable = true;
        Restangular.one('reasonfordelayed', $routeParams.id).get().then(function (reasonfordelay) {
            $scope.original = reasonfordelay;
            $scope.reasonfordelay = Restangular.copy($scope.original);
        });
    }  else {
			$scope.message = 'Reason for delay has been Created!';
		}
    /************* SAVE *******************************************/
    $scope.submitDisable = false;
    $scope.validatestring = '';
    $scope.Savereasonfordelay = function () {
        if ($scope.reasonfordelay.name == '' || $scope.reasonfordelay.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay';
        }
        else if ($scope.reasonfordelay.hnname == '' || $scope.reasonfordelay.hnname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in hindi';
        }
        else if ($scope.reasonfordelay.knname == '' || $scope.reasonfordelay.knname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in kannada';
        }
        else if ($scope.reasonfordelay.taname == '' || $scope.reasonfordelay.taname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in tamil';
        }
        else if ($scope.reasonfordelay.tename == '' || $scope.reasonfordelay.tename == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in telugu';
        }
        else if ($scope.reasonfordelay.mrname == '' || $scope.reasonfordelay.mrname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in marathi';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            $scope.schemestagedataModal = !$scope.schemestagedataModal;
            $scope.submitDisable = true;
            //toaster.pop('success', "Reason For Delay has been created", null, null, 'trustedHtml');
            Restangular.all('reasonfordelayed').post($scope.reasonfordelay).then(function (Response) {
                window.location = '/reasonfordelay';
            });
        }
    };
    /***************************** UPDATE *******************************************/
    $scope.Updatereasonfordelay = function () {
        if ($scope.reasonfordelay.name == '' || $scope.reasonfordelay.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay';
        }
        else if ($scope.reasonfordelay.hnname == '' || $scope.reasonfordelay.hnname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in hindi';
        }
        else if ($scope.reasonfordelay.knname == '' || $scope.reasonfordelay.knname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in kannada';
        }
        else if ($scope.reasonfordelay.taname == '' || $scope.reasonfordelay.taname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in tamil';
        }
        else if ($scope.reasonfordelay.tename == '' || $scope.reasonfordelay.tename == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in telugu';
        }
        else if ($scope.reasonfordelay.mrname == '' || $scope.reasonfordelay.mrname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for delay in marathi';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
           $scope.schemestagedataModal = !$scope.schemestagedataModal;
            $scope.submitDisable = true;
            Restangular.one('reasonfordelayed', $routeParams.id).customPUT($scope.reasonfordelay).then(function () {
                window.location = '/reasonfordelay';
            });
        }
    };
    $scope.modalTitle = 'Thank You';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    /*---------------------------Delete---------------------------------------------------*/
    $scope.Delete = function (id) {
        //toaster.pop('success', "title", '<ul><li>Render html1</li></ul>', 5000, 'trustedHtml');
        $scope.item = [{
            deleteflag: true
            }]
        Restangular.one('reasonfordelayed/' + id).customPUT($scope.item[0]).then(function () {
            $route.reload();
        });
    }
});