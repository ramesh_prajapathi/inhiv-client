'use strict';

angular.module('secondarySalesApp')
    .controller('PatientRecordEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $timeout, $modal) {


        Restangular.one('prlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.PRLanguage = langResponse[0];
            //$scope.modalInstanceLoad.close();
            $scope.PRHeading = $scope.PRLanguage.prEdit;
            $scope.modeltitle1 = $scope.PRLanguage.areYouSureToWantToUpdate;
            $scope.message = $scope.PRLanguage.thankYouUpdated;
            $scope.plsentername = $scope.PRLanguage.entername;
            $scope.plsenterid = $scope.PRLanguage.enterid;
            $scope.selectcondition = $scope.PRLanguage.selcondition;
            $scope.enteryourrelation = $scope.PRLanguage.enterrelation;

        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.patient = {};

        $scope.HideSubmitButton = false;
        $scope.HideUpdateButton = false;
        $scope.diisableSavepopup = true;



        $scope.TestArray = [];

        $scope.role = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
        /********************************* Watch ****************************/


        $scope.disabledmeber = true;
        $scope.auditlog = {};


        /*if ($window.sessionStorage.roleId == 2) {
            $scope.patient.lastModifiedByRole = 2;
            $scope.hidePatient = true;
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                $scope.patient.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;

            });

        } else {
            $scope.patient.lastModifiedByRole = 6;
            $scope.hidePatient = false;
           // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                $scope.getsiteid = fw.id;
                $scope.patient.fieldworkername = fw.firstname;
                $scope.patient.fieldworker = fw.id;
                Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                    $scope.patient.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                });
            });
        }*/
        if ($window.sessionStorage.roleId) {
            $scope.patient.lastModifiedByRole = $window.sessionStorage.roleId;
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                $scope.patient.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;

            });

        }
        /*************************Cancle Button***********/

        $scope.cancelpatientrec = function () {

            $location.path('/conditions-list');

        };

        /****************/

        Restangular.all('conditions?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (resp) {
            $scope.conditionsdsply = resp;
            $scope.patient.condition = $scope.patient.condition;

        });

        Restangular.all('refertos?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (referto) {
            $scope.refertos = referto;
            $scope.patient.patientFlow = $scope.patient.patientFlow;

        });



        Restangular.all('seenbys?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (seenby) {
            $scope.seenbys = seenby;
            $scope.patient.purposeOfVisit = $scope.patient.purposeOfVisit;
        });


        Restangular.all('testnames?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Resp) {
            $scope.testnames = Resp;

        });

        $scope.$watch('patient.condition', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {

                Restangular.all('symptoms?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + newValue).getList().then(function (symptom) {
                    $scope.symptoms = symptom;
                    $scope.patient.symptom = $scope.patient.symptom;

                });

                Restangular.all('medicines?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + newValue).getList().then(function (med) {
                    $scope.medicines = med;
                    $scope.patient.drugGiven = $scope.patient.drugGiven;

                });

                Restangular.all('kitprescribs?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + newValue).getList().then(function (kitprescrib) {
                    $scope.kitprescribs = kitprescrib;
                    $scope.patient.kitPrescribed = $scope.patient.kitPrescribed;
                });
            }
        });



        if ($routeParams.id) {
            //             Restangular.one('conditionheaders', $routeParams.id).get().then(function (con) {
            //                console.log('con', con);
            //            Restangular.one('patientrecords?filter[where][memberId]=' + con.memberId + '&filter[condition]=' + con.condition).get().then(function (patient) {
            //                console.log('patient', patient);
            //                $scope.patientId = patient[0].id;
            //                console.log(' $scope.patientId', $scope.patientId);

            Restangular.one('patientrecords', $routeParams.id).get().then(function (patient) {
                console.log('tttt', patient);
                $scope.original = patient;
                $scope.patient = Restangular.copy($scope.original);


                $scope.patient.symptom = $scope.patient.symptom.split(',');
                $scope.patient.drugGiven = $scope.patient.drugGiven.split(',');
                $scope.patient.kitPrescribed = $scope.patient.kitPrescribed.split(',');
                // $scope.message = 'Patient record created has been updated';

                Restangular.all('testresults?filter[where][patientId]=' + patient.id + '&filter[where][deleteflag]=false').getList().then(function (testResponse) {
                    console.log('testResponse', testResponse);
                    if (testResponse.length > 0) {

                        angular.forEach(testResponse, function (member, index) {
                            member.testnames = $scope.testnames;
                            member.existingFlag = true;
                            // member.testnameId = member.testnameId.split(',');
                            $scope.TestArray.push(member);
                        });

                    } else {
                        $scope.TestArray = [{
                            result: '',
                            existingFlag: false,
                            deleteflag: false,
                            testnames: $scope.testnames,
                            visitdate: ''
                                        }];
                    }
                });
            });
            // });
            //  });
        }

        $scope.addTest = function (id, index) {
            $scope.TestArray.push({
                result: '',
                existingFlag: 'true',
                deleteflag: false,
                testnames: $scope.testnames,
                visitdate: ''
            });
        };

        $scope.removeTest = function (id, index) {

            $scope.TestArray[index].deleteflag = true;


        };

        $scope.auditArray = {};

        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /*********************************SAVE*********************************/
        $scope.validatestring = '';
        $scope.disableAssigned = false;
        $scope.UpdatePatientRecord = function () {
            //  $scope.toggleLoading();
            $scope.disableAssigned = true;
            document.getElementById('name').style.border = "";
            document.getElementById('tiId').style.border = "";

            if ($scope.patient.name == '' || $scope.patient.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.plsentername;
                //$scope.PRLanguage.enterContact;
            } else if ($scope.patient.tiId == '' || $scope.patient.tiId == null) {
                $scope.validatestring = $scope.validatestring + $scope.plsenterid;
                //$scope.PRLanguage.enterAge;

            } else if ($scope.patient.condition == '' || $scope.patient.condition == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectcondition;
                //$scope.PRLanguage.selectGender;

            } else if ($scope.patient.relation == '' || $scope.patient.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteryourrelation;
                //$scope.PRLanguage.selectMigrant;
            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                for (var i = 0; i < $scope.TestArray.length; i++) {
                   // console.log('$scope.TestArray', $scope.TestArray[i]);
                    if ($scope.TestArray[i].result == '' || $scope.TestArray[i].result == null || $scope.TestArray[i].result == undefined) {
                        $scope.hidetestresult = false;
                      //  console.log('i m in if')
                    } else {
                        $scope.hidetestresult = true;
                      //  console.log('i m in else')
                    }
                }
                $scope.confirmationModel = true;

            }

        };

        $scope.SavePatientFinal = function () {
            $scope.confirmationModel = false;
            $scope.toggleLoading();

            $scope.patient.lastModifiedDate = new Date();
            $scope.patient.lastModifiedBy = $window.sessionStorage.UserEmployeeId;
            $scope.patient.lastModifiedByRole = $window.sessionStorage.roleId;


            console.log('$scope.patient', $scope.patient);
            Restangular.one('patientrecords', $scope.patientId).customPUT($scope.patient).then(function (Response) {
                // console.log('Response', Response);
                $scope.patientId = Response.id;

                $scope.auditArray.entityid = Response.id;
                $scope.auditArray.description = 'patient Record Created';
                $scope.auditArray.modifiedbyroleid = $window.sessionStorage.roleId;
                $scope.auditArray.modifiedby = $window.sessionStorage.userId;
                $scope.auditArray.lastmodifiedtime = new Date();
                $scope.auditArray.conutryId = $window.sessionStorage.conutryId;
                $scope.auditArray.state = $window.sessionStorage.zoneId;
                $scope.auditArray.district = $window.sessionStorage.salesAreaId;
                $scope.auditArray.town = $window.sessionStorage.distributionAreaId;
                $scope.auditArray.facility = $window.sessionStorage.coorgId;
                $scope.auditArray.facilityId = $scope.auditlog.facilityId;

                Restangular.all('auditlogs').post($scope.auditArray).then(function (auditresp) {
                    $scope.testResultSave(Response.id);
                });
                /*if ($scope.TestArray.visitdate == null && $scope.TestArray.visitdate == ''){
                $scope.hidetestresult = true;
            }
            else {
                 $scope.hidetestresult = false;
            }*/
            });
        };




        /*************************************************************/

        $scope.TestCount = 0;
        $scope.testResultSave = function (patientId) {

            if ($scope.TestCount < $scope.TestArray.length) {

                if ($scope.TestArray[$scope.TestCount].existingFlag == true) {
                    $scope.patient.lastModifiedDate = new Date();
                    $scope.patient.lastModifiedBy = $window.sessionStorage.userId;
                    $scope.patient.lastModifiedByRole = $window.sessionStorage.roleId;

                    Restangular.one('testResults', $scope.TestArray[$scope.TestCount].id).customPUT($scope.TestArray[$scope.TestCount]).then(function (childResp) {
                        // console.log('childResp', childResp);
                        $scope.TestCount++;
                        $scope.testResultSave(patientId);
                    });

                } else {

                    if ($scope.TestArray[$scope.TestCount].testnameId == null || $scope.TestArray[$scope.TestCount].testnameId == '') {
                        $scope.TestCount++;
                        $scope.testResultSave(patientId);
                    } else {

                        $scope.TestArray[$scope.TestCount].patientId = patientId;
                        $scope.TestArray[$scope.TestCount].deleteflag = false;
                        $scope.TestArray[$scope.TestCount].countryId = $window.sessionStorage.countryId;
                        $scope.TestArray[$scope.TestCount].state = $window.sessionStorage.zoneId;
                        $scope.TestArray[$scope.TestCount].district = $window.sessionStorage.salesAreaId;
                        $scope.TestArray[$scope.TestCount].town = $window.sessionStorage.distributionAreaId;
                        //$scope.TestArray[$scope.TestCount].site = $window.sessionStorage.siteId.split(",")[0];
                        $scope.TestArray[$scope.TestCount].createdBy = $window.sessionStorage.userId;
                        $scope.TestArray[$scope.TestCount].createdDate = new Date();
                        $scope.TestArray[$scope.TestCount].createdByRole = $window.sessionStorage.roleId;
                        $scope.TestArray[$scope.TestCount].lastModifiedBy = $window.sessionStorage.userId;
                        $scope.TestArray[$scope.TestCount].lastModifiedDate = new Date();
                        $scope.TestArray[$scope.TestCount].lastModifiedByRole = $window.sessionStorage.roleId;

                        Restangular.all('testResults').post($scope.TestArray[$scope.TestCount]).then(function (childResp) {
                            //  console.log('childResp', childResp);
                            $scope.TestCount++;
                            $scope.testResultSave(patientId);
                        });
                    }
                }
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setTimeout(function () {
                    window.location = "/conditions-list";
                }, 350);
            }
        };




        /************************************************************/

        //Datepicker settings start
        $scope.patient.visitdate = new Date();
         /************************************************************/

        //Datepicker settings start
        //$scope.patient.visitdate = new Date();
     /**   $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];**/
        //Datepicker settings end******************/
    
    
    // $scope.patient.visitdate = new Date();
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };

        $scope.opentestdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickertestdate' + index).focus();
            });
            $scope.TestArray[index].testdateopened = true;
        };

        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];

    });
