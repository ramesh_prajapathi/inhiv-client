'use strict';

angular.module('secondarySalesApp')
    .controller('TypCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        //$scope.OtherLang = false;
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/typology/create' || $location.path() === '/typology/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/typology/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/typology/create' || $location.path() === '/typology/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/typology/create' || $location.path() === '/typology/' + $routeParams.id;
            return visible;
        };

        Restangular.all('genders?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rfch) {
            $scope.genders = rfch;
        });





        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/typology") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /**********************************/

        $scope.showenglishLang = true;

        $scope.$watch('typology.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    $scope.typology.name = '';
                    $scope.typology.parentId = '';
                    $scope.typology.code = '';
                    $scope.typology.genderId = '';
                    $scope.typology.orderno = ''
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;
                }

            }
        });
        $scope.$watch('typology.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('typologies', newValue).get().then(function (zn) {
                    $scope.typology.code = zn.code;
                    $scope.typology.genderId = zn.genderId;
                    $scope.typology.orderno = zn.orderno;

                });
            }
        });

        /**************************edit**********/
        if ($routeParams.id) {
            $scope.message = 'Typology has been Updated!';
            Restangular.one('typologies', $routeParams.id).get().then(function (typology) {
                $scope.original = typology;
                $scope.typology = Restangular.copy($scope.original);
                $scope.condiionoldvalue = $scope.typology.orderno;
            });
        } else {
            $scope.message = 'Typology has been Created!';
        }

        $scope.Search = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name','gendname'];
        $scope.Search = '';



        /*********************************** INDEX *******************************************/
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        Restangular.all('genders?filter[where][deleteflag]=false').getList().then(function (resp) {
            $scope.Dispgenders = resp;
            console.log('resp', resp);
        });

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('typologies?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.typologies = mt;
                Restangular.all('genders?filter[where][deleteflag]=false').getList().then(function (resp) {
                    $scope.genderdisply = resp;

                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                        $scope.languageDisp = lang;
                        angular.forEach($scope.typologies, function (member, index) {

                            if (member.deleteflag + '' === 'true') {
                                member.colour = "#A20303";
                            } else {
                                member.colour = "#000000";
                            }

                            for (var a = 0; a < $scope.genderdisply.length; a++) {
                                if (member.genderId == $scope.genderdisply[a].id) {
                                    member.gendname = $scope.genderdisply[a].name;
                                    break;
                                }
                            }

                            for (var b = 0; b < $scope.languageDisp.length; b++) {
                                if (member.language == $scope.languageDisp[b].id) {
                                    member.langName = $scope.languageDisp[b].name;
                                    break;
                                }
                            }


                            member.index = index + 1;
                        });
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.typlanguages = sev;
            });

        } else {

            Restangular.all('typologies').getList().then(function (mt) {
                $scope.typologies = mt;

                Restangular.all('genders?filter[where][deleteflag]=false').getList().then(function (resp) {
                    $scope.genderdisply = resp;

                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                        $scope.languageDisp = lang;

                        angular.forEach($scope.typologies, function (member, index) {

                            if (member.deleteflag + '' === 'true') {
                                member.colour = "#A20303";
                            } else {
                                member.colour = "#000000";
                            }

                              for (var a = 0; a < $scope.genderdisply.length; a++) {
                                if (member.genderId == $scope.genderdisply[a].id) {
                                    member.gendname = $scope.genderdisply[a].name;
                                    break;
                                }
                            }

                            for (var b = 0; b < $scope.languageDisp.length; b++) {
                                if (member.language == $scope.languageDisp[b].id) {
                                    member.langName = $scope.languageDisp[b].name;
                                    break;
                                }
                            }

                            member.index = index + 1;
                            //console.log('member', member);
                        });
                    });
                });
            });
            Restangular.all('typologies?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.typlanguages = sev;

                        angular.forEach($scope.typlanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.typlanguages = sev;
                    });
                }
            });


        }


        Restangular.all('typologies?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.typdisply = zn;
        });

        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.typdisply.filter(function (arr) {
                    return arr.orderno == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.typology.language != '') {
                    $scope.typology.orderno = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };







        /****************************** SAVE *******************************************/


        $scope.typology = {
            "name": '',
            "deleteflag": false
        };

        $scope.submitDisable = false;
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";
            document.getElementById('orderno').style.border = "";
            if ($scope.typology.language == '' || $scope.typology.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.typology.language == 1) {
                if ($scope.typology.name == '' || $scope.typology.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Typology name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.typology.code == '' || $scope.typology.code == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Typology Code';
                    document.getElementById('code').style.borderColor = "#FF0000";

                } else if ($scope.typology.code == '' || $scope.typology.code.length != 1) {
                    $scope.validatestring = $scope.validatestring + 'Please enter 1-Digit Typology Code';
                    document.getElementById('code').style.borderColor = "#FF0000";

                }
                
                else if ($scope.typology.genderId == '' || $scope.typology.genderId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Gender';
                    document.getElementById('gender').style.borderColor = "#FF0000";

                } else if ($scope.typology.orderno == '' || $scope.typology.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter OrderNo';
                    document.getElementById('orderno').style.borderColor = "#FF0000";

                }

            } else if ($scope.typology.language != 1) {
                if ($scope.typology.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Typology in English';

                } else if ($scope.typology.name == '' || $scope.typology.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Typology name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.typology.code == '' || $scope.typology.code == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Code';
                    document.getElementById('code').style.borderColor = "#FF0000";

                } else if ($scope.typology.genderId == '' || $scope.typology.genderId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Gender';
                    document.getElementById('gender').style.borderColor = "#FF0000";

                } else if ($scope.typology.orderno == '' || $scope.typology.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter OrderNo';
                    document.getElementById('orderno').style.borderColor = "#FF0000";

                }

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.typology.parentId === '') {
                    delete $scope.typology['parentId'];
                }
                $scope.submitDisable = true;
                $scope.typology.parentFlag = $scope.showenglishLang;
                $scope.typologies.post($scope.typology).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/typology';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'typology_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };
        /******************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.typology.name == '' || $scope.typology.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter typology name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.typology.parentId === '') {
                    delete $scope.typology['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('typologies', $routeParams.id).customPUT($scope.typology).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('Typology Name Saved');
                    setTimeout(function () {
                        window.location = '/typology';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'typology_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /************************************ DELETE *******************************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }




        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('typologies/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('typologies/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


    });
