'use strict';

angular.module('secondarySalesApp')
    .controller('PatientRecordCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $timeout, $modal) {

        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.patient = {
            createdDate: new Date(),
            deleteflag: false,
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            countryId: $window.sessionStorage.countryId,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            town: $window.sessionStorage.distributionAreaId,
            facility: $window.sessionStorage.coorgId,
            // siteId: $window.sessionStorage.siteId.split(",")[0]
        };


        $scope.role = {};
        $scope.role.enablePR = false;


        Restangular.one('prlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.PRLanguage = langResponse[0];
            //$scope.modalInstanceLoad.close();
            $scope.PRHeading = $scope.PRLanguage.prCreate;
            $scope.modeltitle1 = $scope.PRLanguage.areYouSureWantToSave;
            $scope.message = $scope.PRLanguage.thankYouCreated;
            $scope.plsentername = $scope.PRLanguage.entername;
            $scope.plsenterid = $scope.PRLanguage.enterid;
            $scope.selectcondition = $scope.PRLanguage.selcondition;
            $scope.enteryourrelation = $scope.PRLanguage.enterrelation;
            //$scope.patient.relation = 'SELF';
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.HideSubmitButton = true;
        $scope.HideUpdateButton = true;
        $scope.confirmationModel = false;
        $scope.cancelpatientrec = function () {
            // window.location = '/conditions-list';
            $window.sessionStorage.fullName = $window.sessionStorage.fullName;
            // window.location = '/patientrecord/create';
            if ($window.sessionStorage.previouspage == '/condition/create') {

                $timeout(function () {
                    //  $location.path($window.sessionStorage.previouspage);
                    // window.location = $window.sessionStorage.previouspage;
                    window.location = '/conditions-list';
                    $window.sessionStorage.previouspage = '';

                }, 380);

            } else if ($window.sessionStorage.previouspage == '/condition/edit/:id') {

                $timeout(function () {
                    $location.path('/condition/edit/' + $window.sessionStorage.previousRouteparamsId);
                    window.location = "/condition/edit/" + $window.sessionStorage.previousRouteparamsId;
                    $window.sessionStorage.previouspage = '';

                }, 380);

            }
        };

        $scope.disabledmeber = true;
        $scope.auditlog = {};


        if ($window.sessionStorage.roleId) {
            $scope.patient.lastModifiedByRole = $window.sessionStorage.roleId;
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                $scope.patient.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;

            });

        }
        /*else {
                $scope.patient.lastModifiedByRole = 3;

                //Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    $scope.getsiteid = fw.id;
                    $scope.patient.fieldworkername = fw.firstname;
                    $scope.patient.fieldworker = fw.id;
                    Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                        $scope.patient.facilityId = comember.id;
                        $scope.auditlog.facilityId = comember.id;
                    });
                });
            }*/

        /* if ($window.sessionStorage.previous == '/condition/create' || $window.sessionStorage.previous == '/condition/edit/:id') {
         */


        Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (memberResp) {

            $scope.patient.name = memberResp.fullname;
            $scope.memberName = memberResp.fullname;
            $scope.patient.memberId = memberResp.id;
            $scope.patient.tiId = memberResp.tiId;
            $scope.patient.site = memberResp.site;
            $scope.siteId = memberResp.site;
            $scope.patient.ictcId = memberResp.ictcid;
            $scope.patient.artId = memberResp.artId;
            $scope.patient.facility = memberResp.facility;
        });
        Restangular.one('conditionheaders', $window.sessionStorage.headerId).get().then(function (conResp) {

            $scope.patient.conditionheaderId = conResp.id;


        });
        $scope.patient.conditionheaderId = $window.sessionStorage.headerId;

        Restangular.one('conditions', $window.sessionStorage.conditionId).get().then(function (condition) {
            console.log('condition', condition);
            $scope.ConId = condition.id;
            $scope.patient.condition = condition.id;
            $window.sessionStorage.conditionIdnew = condition.id;
            $scope.patient.relation = $window.sessionStorage.relation;
        });


        Restangular.all('conditions?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (resp) {
            $scope.conditionsdsply = resp;
            $scope.patient.condition = $scope.patient.condition;
        });



        /********************************* Watch ****************************/
        $scope.refertos = Restangular.all('refertos?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;


        Restangular.all('seenbys?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (condResst) {
            console.log('condResst', condResst);
            $scope.seenbys = condResst;
            if ($scope.UserLanguage == 1) {
                $scope.patient.purposeOfVisit = condResst[2].id;
            } else if ($scope.UserLanguage != 1) {
                $scope.patient.purposeOfVisit = condResst[2].parentId;
            }

        });


        $scope.$watch('patient.condition', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.symptoms = Restangular.all('symptoms?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + newValue).getList().$object;

                $scope.medicines = Restangular.all('medicines?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + newValue).getList().$object;

                $scope.kitprescribs = Restangular.all('kitprescribs?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + newValue).getList().$object;

            }
        });

        $scope.conditionChnage = function (id) {
            console.log('id', id);

            $scope.medicines = Restangular.all('medicines?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + id).getList().$object;

            $scope.kitprescribs = Restangular.all('kitprescribs?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language + '&filter[where][conditionId]=' + id).getList().$object;

        };


        Restangular.all('testnames?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Resp) {
            $scope.testnames = Resp;
            $scope.TestArray = [{
                result: '',
                deleteflag: false,
                testnames: $scope.testnames,
                visitdate: ''
                     }];

        });

        $scope.TestArray = [{

            result: '',
            deleteflag: false,
            testnames: $scope.testnames,
            visitdate: ''
                     }];


        $scope.addTest = function (id, index) {
            $scope.TestArray.push({
                result: '',
                deleteflag: false,
                testnames: $scope.testnames,
                visitdate: ''
            });
        };

        $scope.removeTest = function (id, index) {
            if ($scope.TestArray.length == 1) {
                //  alert('You can not delete');
            } else {
                $scope.TestArray.splice(index, 1);
                // $scope.TestArray[index].deleteflag = true;
            }

        };
        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        $scope.diagnosisChange = function (testnameId, index) {
            $scope.TestArray[index].result = '';
        };

        /*********************************SAVE*********************************/
        $scope.validatestring = '';
        $scope.disableAssigned = false;
        $scope.SavePatientRecord = function () {
            //  $scope.toggleLoading();
            $scope.disableAssigned = true;
            document.getElementById('name').style.border = "";
            document.getElementById('tiId').style.border = "";

            if ($scope.patient.name == '' || $scope.patient.name == null) {
                $scope.validatestring = $scope.validatestring + $scope.plsentername;
                //$scope.PRLanguage.enterContact;
            } else if ($scope.patient.tiId == '' || $scope.patient.tiId == null) {
                $scope.validatestring = $scope.validatestring + $scope.plsenterid;
                //$scope.PRLanguage.enterAge;

            } else if ($scope.patient.condition == '' || $scope.patient.condition == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectcondition;
                //$scope.PRLanguage.selectGender;

            } else if ($scope.patient.relation == '' || $scope.patient.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteryourrelation;
                //$scope.PRLanguage.selectMigrant;
            }



            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {

                for (var i = 0; i < $scope.TestArray.length; i++) {
                  //  console.log('$scope.TestArray', $scope.TestArray[i]);
                    if ($scope.TestArray[i].result == '' || $scope.TestArray[i].result == null || $scope.TestArray[i].result == undefined) {
                        $scope.hidetestresult = false;
                       // console.log('i m in if')
                    } else {
                        $scope.hidetestresult = true;
                        //console.log('i m in else')
                    }
                }
                $scope.confirmationModel = true;

            }

        };


        $scope.auditArray = {};

        $scope.SavePatientFinal = function () {
            $scope.confirmationModel = false;
            $scope.toggleLoading();

            console.log('$scope.patient', $scope.patient);
            // $scope.patient.conditionheaderId = $window.sessionStorage.headerId;
            Restangular.all('patientrecords').post($scope.patient).then(function (Response) {
                //console.log('Response', Response);
                $scope.PatinetId = Response.id;


                $scope.auditArray.entityid = Response.id;
                $scope.auditArray.description = 'patient Record Created';
                $scope.auditArray.modifiedbyroleid = $window.sessionStorage.roleId;
                $scope.auditArray.modifiedby = $window.sessionStorage.userId;
                $scope.auditArray.lastmodifiedtime = new Date();
                $scope.auditArray.conutryId = $window.sessionStorage.conutryId;
                $scope.auditArray.state = $window.sessionStorage.zoneId;
                $scope.auditArray.district = $window.sessionStorage.salesAreaId;
                $scope.auditArray.town = $window.sessionStorage.distributionAreaId;
                $scope.auditArray.facility = $window.sessionStorage.coorgId;
                $scope.auditArray.facilityId = $scope.auditlog.facilityId;

                Restangular.all('auditlogs').post($scope.auditArray).then(function (auditresp) {
                    $scope.testResultSave(Response.id);
                });

            });
        };




        //  $scope.message = 'Patient record created ';
        $scope.TestCount = 0;
        $scope.testResultSave = function (patientId) {

            if ($scope.TestCount < $scope.TestArray.length) {
                if ($scope.TestArray[$scope.TestCount].testnameId == null || $scope.TestArray[$scope.TestCount].testnameId == '') {
                    $scope.TestCount++;
                    $scope.testResultSave(patientId);
                } else {

                    $scope.TestArray[$scope.TestCount].patientId = patientId;
                    $scope.TestArray[$scope.TestCount].deleteflag = false;
                    $scope.TestArray[$scope.TestCount].countryId = $window.sessionStorage.countryId;
                    $scope.TestArray[$scope.TestCount].state = $window.sessionStorage.zoneId;
                    $scope.TestArray[$scope.TestCount].district = $window.sessionStorage.salesAreaId;
                    $scope.TestArray[$scope.TestCount].town = $window.sessionStorage.distributionAreaId;
                    //$scope.TestArray[$scope.TestCount].site = cnd.;
                    $scope.TestArray[$scope.TestCount].createdBy = $window.sessionStorage.userId;
                    $scope.TestArray[$scope.TestCount].createdDate = new Date();
                    $scope.TestArray[$scope.TestCount].createdByRole = $window.sessionStorage.roleId;
                    $scope.TestArray[$scope.TestCount].lastModifiedBy = $window.sessionStorage.userId;
                    $scope.TestArray[$scope.TestCount].lastModifiedDate = new Date();
                    $scope.TestArray[$scope.TestCount].lastModifiedByRole = $window.sessionStorage.roleId;



                    Restangular.all('testResults').post($scope.TestArray[$scope.TestCount]).then(function (childResp) {

                        //  console.log('childResp', childResp);
                        $scope.TestCount++;
                        $scope.testResultSave(patientId);
                    });

                }
            } else {

                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');
                $window.sessionStorage.MemberId = '';
                $window.sessionStorage.fullName = '';
                $window.sessionStorage.relation = '';
                $window.sessionStorage.conditionId = '';
                $window.sessionStorage.conditionIdnew = '';
                $window.sessionStorage.headerId = '';

                //                  setTimeout(function () {
                //                    window.location = '/conditions-list';
                //                  }, 350);

                if ($window.sessionStorage.previouspage == '/condition/create') {
                    $scope.modalInstanceLoad.close();
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('reloading...');

                    $timeout(function () {
                        //$location.path($window.sessionStorage.previouspage);
                        // window.location = $window.sessionStorage.previouspage;  
                        window.location = '/conditions-list';
                        $window.sessionStorage.previouspage = '';

                    }, 380);

                } else if ($window.sessionStorage.previouspage == '/condition/edit/:id') {
                    $scope.modalInstanceLoad.close();
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('reloading...');
                    $timeout(function () {
                        $location.path('/condition/edit/' + $window.sessionStorage.previousRouteparamsId);
                        window.location = "/condition/edit/" + $window.sessionStorage.previousRouteparamsId;
                        $window.sessionStorage.previouspage = '';

                    }, 380);

                }
            }
        };









        /************************************************************/

        //Datepicker settings start
        //$scope.patient.visitdate = new Date();
     /**   $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];**/
        //Datepicker settings end******************/
    
    
    // $scope.patient.visitdate = new Date();
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };

        $scope.opentestdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickertestdate' + index).focus();
            });
            $scope.TestArray[index].testdateopened = true;
        };

        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];

    });
