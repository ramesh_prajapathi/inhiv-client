'use strict';

angular.module('secondarySalesApp')
    .controller('vocationCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        //$scope.OtherLang = false;
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/vocation/create' || $location.path() === '/vocation/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/vocation/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/vocation/create' || $location.path() === '/vocation/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/vocation/create' || $location.path() === '/vocation/' + $routeParams.id;
            return visible;
        };
        /*********/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/vocation") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }



        /*************************** INDEX *******************************************/
        //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';
    
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('vocationpartners?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.vocationpartners = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.vocationpartners, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.gendlanguages = sev;
            });

        } else {

            Restangular.all('vocationpartners').getList().then(function (mt) {
                $scope.vocationpartners = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.vocationpartners, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('vocationpartners?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.gendlanguages = sev;

                        angular.forEach($scope.gendlanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.gendlanguages = sev;
                    });
                }
            });
        }

        /***new changes*****/
        Restangular.all('vocationpartners?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.genddisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.genddisply.filter(function (arr) {
                    return arr.orderno == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.vocationpartner.language != '') {
                    $scope.vocationpartner.orderno = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };

        /**********************************/

        if ($routeParams.id) {
            // $scope.OtherLang = true;
            $scope.message = 'VOCATION OF THE PARTNER has been Updated!';
            Restangular.one('vocationpartners', $routeParams.id).get().then(function (vocationpartner) {
                $scope.original = vocationpartner;
                $scope.vocationpartner = Restangular.copy($scope.original);
                $scope.currvalue = $scope.vocationpartner.defaultValue;
                $scope.condiionoldvalue = $scope.vocationpartner.orderno;
            });

            Restangular.all('vocationpartners?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;
                //console.log('$scope.AllvitalRecord', $scope.AllvitalRecord);
            });

        } else {
            $scope.message = 'VOCATION OF THE PARTNER has been Created!';
            $scope.Updateflag = false;
            Restangular.all('vocationpartners?filter[where][defaultValue]=Yes' + '&filter[where][deleteflag]=false').getList().then(function (gender) {
                if (gender.length == 0) {
                    return;
                } else {
                    if (gender[0].defaultValue == 'Yes') {

                        $scope.vocationpartner.defaultValue = 'No';
                    }
                }
            });
        }
        $scope.Search = $scope.name;

        /***new changes*****/
        $scope.showenglishLang = true;

        $scope.$watch('vocationpartner.language', function (newValue, oldValue) {

            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (!$routeParams.id) {
                    $scope.vocationpartner.name = '';
                    $scope.vocationpartner.parentId = '';

                    //                    $scope.vocationpartner.defaultValue = '';
                    $scope.vocationpartner.orderno = ''
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;


                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;
                }

            }
        });
        /***new changes*****/
        Restangular.all('vocationpartners?filter[where][deleteflag]=false' + '&filter[where][defaultValue]=Yes' + '&filter[where][language]=' + 1).getList().then(function (screens) {
            console.log('screens', screens);
            $scope.updatingobj = screens;
        });


        /************ SAVE *******************************************/


        $scope.vocationpartner = {
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            "deleteflag": false
        };

        $scope.$watch('vocationpartner.parentId', function (newValue, oldValue) {

            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;

                Restangular.one('vocationpartners', newValue).get().then(function (zn) {
                    $scope.vocationpartner.orderno = zn.orderno;
                    $scope.vocationpartner.defaultValue = zn.defaultValue;

                });
            }
        });

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.creatingFlag = false;
        $scope.updatingFlag == false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('orderno').style.border = "";

            if ($scope.updatingobj.length > 0 && $scope.vocationpartner.defaultValue == 'Yes') {
                $scope.creatingFlag = true;
            }

            if ($scope.vocationpartner.language == '' || $scope.vocationpartner.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.vocationpartner.language == 1) {

                if ($scope.vocationpartner.name == '' || $scope.vocationpartner.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.vocationpartner.defaultValue == '' || $scope.vocationpartner.defaultValue == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter default';
                    document.getElementById('default').style.borderColor = "#FF0000";
                } else if ($scope.vocationpartner.orderno == '' || $scope.vocationpartner.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('orderno').style.borderColor = "#FF0000";

                }
            } else if ($scope.vocationpartner.language != 1) {

                if ($scope.vocationpartner.parentId == '' || $scope.vocationpartner.parentId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please select Vocation Partner In English';

                } else if ($scope.vocationpartner.name == '' || $scope.vocationpartner.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {


                if ($scope.vocationpartner.parentId === '') {
                    delete $scope.vocationpartner['parentId'];
                }
                $scope.submitDisable = true;
                $scope.vocationpartner.parentFlag = $scope.showenglishLang;
                Restangular.all('vocationpartners').post($scope.vocationpartner).then(function () {


                    if ($scope.creatingFlag == true && $scope.vocationpartner.language == 1) {

                        $scope.updateRow = {
                            defaultValue: 'No'
                        };

                        Restangular.one('vocationpartners', $scope.updatingobj[0].id).customPUT($scope.updateRow).then(function (resp) {
                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                            setTimeout(function () {
                                window.location = '/vocation';
                            }, 350);

                        });

                    } else {
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        setTimeout(function () {
                            window.location = '/vocation';
                        }, 350);
                    }
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'vocationpartner_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            };
        };



        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /********************** UPDATE *******************************************/

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.updatingobj.length > 0 && $scope.vocationpartner.defaultValue == 'Yes' && $scope.currvalue != $scope.vocationpartner.defaultValue) {
                $scope.updatingFlag = true;
            }

            if ($scope.vocationpartner.name == '' || $scope.vocationpartner.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.vocationpartner.parentId === '') {
                    delete $scope.vocationpartner['parentId'];
                }
                $scope.submitDisable = true;
                $scope.vocationpartner.parentFlag = $scope.showenglishLang;
                Restangular.one('vocationpartners', $routeParams.id).customPUT($scope.vocationpartner).then(function (respupdate) {

                    if ($scope.updatingFlag == true) {

                        $scope.updateRow = {
                            defaultValue: 'No'
                        };

                        Restangular.one('vocationpartners', $scope.updatingobj[0].id).customPUT($scope.updateRow).then(function (resp) {
                            $scope.MandatoryUpdate(respupdate);

                        });

                    } else {
                        $scope.MandatoryUpdate(respupdate);
                    }
                }, function (error) {
                    if (error.data.error.constraint === 'vocationpartner_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            }
        };

        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderno = myResponse.orderno;
                $scope.updateallRecord.defaultValue = myResponse.defaultValue;

                Restangular.one('vocationpartners', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('Test Name Saved');
                setTimeout(function () {
                    window.location = '/vocation';
                }, 350);
            }
        };


        /************************************ DELETE *******************************************/



        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }


        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('vocationpartners/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        /***************** Archive *****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('vocationpartners/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
