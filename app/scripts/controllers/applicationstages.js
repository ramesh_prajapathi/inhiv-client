'use strict';

angular.module('secondarySalesApp')
	.controller('applicationstageCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.showForm = function () {
			var visible = $location.path() === '/applicationstage/create' || $location.path() === '/applicationstage/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/applicationstage/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/applicationstage/create' || $location.path() === '/applicationstage/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/applicationstage/create' || $location.path() === '/applicationstage/' + $routeParams.id;
			return visible;
		};

		/*********************************** Pagination *******************************************/

		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 5;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};


		console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
		if ($window.sessionStorage.prviousLocation != "partials/applicationstages") {
			//$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 5;
			//$scope.currentpage = 1;
			//$scope.pageSize = 5;
		}
		/*********/

		//  $scope.applicationstages = Restangular.all('applicationstages').getList().$object;

		if ($routeParams.id) {
			$scope.message = 'Application Stage has been Updated!';
			Restangular.one('applicationstages', $routeParams.id).get().then(function (applicationstage) {
				$scope.original = applicationstage;
				$scope.applicationstage = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Application Stage has been Updated!';
		}
		$scope.searchZone = $scope.name;

		/************************** INDEX *******************************************/
		$scope.zn = Restangular.all('applicationstages?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.applicationstages = zn;
			angular.forEach($scope.applicationstages, function (member, index) {
				member.index = index + 1;
			});
		});

	$scope.applicationstage = {
		deleteflag: false,
		name: ''
	};
		/********************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.applicationstage.name == '' || $scope.applicationstage.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter application stage';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.applicationstages.post($scope.applicationstage).then(function () {
					window.location = '/applicationstage';
				});
			};
		};
		/*********************************** UPDATE *******************************************/
		$scope.validatestring = '';
		$scope.Update = function () {
			document.getElementById('name').style.border = "";
			if ($scope.applicationstage.name == '' || $scope.applicationstage.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter application stage';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;
				$scope.applicationstages.customPUT($scope.applicationstage).then(function () {
					console.log('Application Update');
					window.location = '/applicationstage';
				});
			}
		};
		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/************************************** DELETE *******************************************/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('applicationstages/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
