'use strict';

angular.module('secondarySalesApp')
    .controller('FcraStausCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/genders/create' || $location.path() === '/genders/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/genders/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/genders/create' || $location.path() === '/genders/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/genders/create' || $location.path() === '/genders/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/genders") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Gender has been Updated!';
            Restangular.one('genders', $routeParams.id).get().then(function (gender) {
                $scope.original = gender;
                $scope.gender = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Gender has been Created!';
        }
        $scope.Search = $scope.name;

        /******************************** INDEX *******************************************/
        $scope.zn = Restangular.all('genders?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.genders = zn;
            angular.forEach($scope.genders, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/
        $scope.gender = {
            "name": '',
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.gender.name == '' || $scope.gender.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.gender.hnname == '' || $scope.gender.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.gender.knname == '' || $scope.gender.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.gender.taname == '' || $scope.gender.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.gender.tename == '' || $scope.gender.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.gender.mrname == '' || $scope.gender.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.genders.post($scope.gender).then(function () {
                    window.location = '/genders';
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.gender.name == '' || $scope.gender.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.gender.hnname == '' || $scope.gender.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.gender.knname == '' || $scope.gender.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.gender.taname == '' || $scope.gender.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.gender.tename == '' || $scope.gender.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.gender.mrname == '' || $scope.gender.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Gender Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                Restangular.one('genders', $routeParams.id).customPUT($scope.gender).then(function () {
                    console.log('gender Saved');
                    window.location = '/genders';
                });
            }
        };
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('genders/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
