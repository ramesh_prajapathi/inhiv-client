'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionStepCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

        //$scope.OtherLang = false;


        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/conditionstep/create' || $location.path() === '/conditionstep/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/conditionstep/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/conditionstep/create' || $location.path() === '/conditionstep/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/conditionstep/create' || $location.path() === '/conditionstep/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/conditionstep-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }



        $scope.showenglishLang = true;
        //$scope.disableorder = false;


        $scope.$watch('conditionstep.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {

                    $scope.conditionstep.orderNo = '';
                    $scope.conditionstep.name = '';
                    $scope.conditionstep.parentId = '';
                }


                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;

                }


            }
        });



        $scope.$watch('conditionstep.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('conditionsteps', newValue).get().then(function (zn) {
                    $scope.conditionstep.orderNo = zn.orderNo;

                });
            }
        });

        /*******************************************************************/
        if ($routeParams.id) {
            // $scope.OtherLang = true;
            $scope.message = 'Step has been Updated!';
            Restangular.one('conditionsteps', $routeParams.id).get().then(function (conditionstep) {
                $scope.original = conditionstep;
                $scope.conditionstep = Restangular.copy($scope.original);
                $scope.condiionoldvalue = $scope.conditionstep.orderNo;
                console.log('$scope.condiionoldvalue', $scope.condiionoldvalue);
            });
            Restangular.all('conditionsteps?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;

            });

        } else {
            $scope.message = 'Step has been Created!';
        }

       // $scope.Search = $scope.name;
    
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /******************************** INDEX *******************************************/


        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('conditionsteps?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.conditionsteps = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditionsteps, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.condisteplanguages = sev;
            });

        } else {

            Restangular.all('conditionsteps').getList().then(function (mt) {
                $scope.conditionsteps = mt;
                   Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditionsteps, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('conditionsteps?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condisteplanguages = sev;

                        angular.forEach($scope.condisteplanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condisteplanguages = sev;
                    });
                }
            });


        }

        /***new changes*****/
        Restangular.all('conditionsteps?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.conditionstepdisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };


        var timeoutPromise;
        var delayInMs = 1500;

       
        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.conditionstepdisply.filter(function (arr) {
                    return arr.orderNo == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.conditionstep.language != '') {
                    $scope.conditionstep.orderNo = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };
    
    




        /********************************************* SAVE *******************************************/
        $scope.conditionstep = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";
            if ($scope.conditionstep.language == '' || $scope.conditionstep.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionstep.language == 1) {
                if ($scope.conditionstep.name == '' || $scope.conditionstep.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Step';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.conditionstep.orderNo == '' || $scope.conditionstep.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }

            } else if ($scope.conditionstep.language != 1) {
                if ($scope.conditionstep.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition Step in English';

                } else if ($scope.conditionstep.name == '' || $scope.conditionstep.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Step';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.conditionstep.orderNo == '' || $scope.conditionstep.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.conditionstep.parentId === '') {
                    delete $scope.conditionstep['parentId'];
                }
                $scope.submitDisable = true;
                $scope.conditionstep.parentFlag = $scope.showenglishLang;
                Restangular.all('conditionsteps').post($scope.conditionstep).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/conditionstep-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'conditionstep_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });


            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditionstep.language == '' || $scope.conditionstep.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionstep.name == '' || $scope.conditionstep.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Step';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditionstep.orderNo == '' || $scope.conditionstep.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.conditionstep.parentId === '') {
                    delete $scope.conditionstep['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('conditionsteps', $routeParams.id).customPUT($scope.conditionstep).then(function (updateResp) {
                   // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('conditionstep  Saved');
                    $scope.MandatoryUpdate(updateResp);

                }, function (error) {
                    if (error.data.error.constraint === 'conditionstep_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            }
        };


        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderNo = myResponse.orderNo;
                Restangular.one('conditionsteps', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                    window.location = '/conditionstep-list';
                }, 350);
            }
        };


        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('conditionsteps/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('conditionsteps/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


    });
