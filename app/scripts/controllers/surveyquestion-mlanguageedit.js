'use strict';

angular.module('secondarySalesApp')
    .controller('SurveyQuestionsEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/

        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/onetoonequestion/create' || $location.path() === '/onetoonequestion/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/onetoonequestion/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/onetoonequestion/create';
            return visible;
        };


       
        /*************************************************************************************************/


        $scope.showenglishLang = true;

        $scope.$watch('surveyquestion.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {


                    $scope.surveyquestion.question = '';
                    $scope.surveyquestion.parentId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableforother = true;
                    $scope.otherLangDisplay = true;
                      $scope.otherlanguage = false;
                    $scope.Englanguage = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableforother = false;
                    $scope.otherLangDisplay = false;
                     $scope.Englanguage = true;
                    $scope.otherlanguage = false;
                }

            }
        });
        Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=1' + '&filter[where][type]=Todo').getList().then(function (Resp) {
            $scope.todotypes = Resp;
        });

        Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=1' + '&filter[where][type]=Message').getList().then(function (Resp) {
            $scope.todomessages = Resp;
        });

       


        Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.questionDisplay = zn;
        });
        Restangular.all('typologies?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (respTypo) {
            $scope.typologies = respTypo;
        });
       
     Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.medilanguages = sev;
            });


        var timeoutPromise;
        var delayInMs = 1000;

        $scope.orderChange = function (serialno) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.questionDisplay.filter(function (arr) {
                    return arr.serialno == serialno
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.questionValue != question && $scope.surveyquestion.language != '') {
                    $scope.surveyquestion.serialno = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Question No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };


        $scope.YesPopup = true;
        $scope.NoPopup = true;
        $scope.YesIncrement = true;
        $scope.NoIncrement = true;
        $scope.YesProvideInfo = true;
        $scope.NoProvideInfo = true;

        /*********/

        $scope.surveyquestion = {
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false,
            skipdate: false,
            skiponTypology: false
        };

        $scope.submitsurveyquestns = Restangular.all('surveyquestions').getList().$object;



        $scope.NotSubQuestion = true;
        $scope.NotSkipQuestion = true;
        $scope.YesNotSkipQuestion = true;
        $scope.NoNotSkipQuestion = true;
        $scope.NotCountedCheckbox = true;
        $scope.NotRange = true;
        $scope.NotScheme = true;
        $scope.actionOn = true;
        $scope.hideoption = true;
        $scope.actionOnNO = true;
        $scope.actionOnYes = true;
        $scope.skipDate = true;
        $scope.skipTypology = true;
        $scope.skipQuestion = true;
        $scope.hideLimit = true;
        $scope.YesPopup = true;
        $scope.YesMessage = true;
        $scope.YesTodo = true;
        $scope.NoPopup = true;
        $scope.NoTodo = true;
      //  $scope.YesTodo = true;


        $scope.flag = false;
        $scope.$watch('surveyquestion.questiontype', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            }
            /*else if (newValue == 'sq') {
                $scope.NotSubQuestion = false;
                $scope.NotSkipQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'skp') {
                $scope.NotSkipQuestion = false;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } */
            else if (newValue == 'c') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = true;
                $scope.actionOnYes = true;
                $scope.actionOn = true;
                $scope.hideoption = false;
                $scope.hideLimit = true;
                $scope.YesPopup = true;
                $scope.YesMessage = true;
                $scope.YesTodo = true;
                $scope.NoPopup = true;
        $scope.NoTodo = true;
                //$scope.surveyquestion.skipdate = '';

            } else if (newValue == 'rb') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = true;
                $scope.actionOnYes = true;
                $scope.actionOn = true;
                $scope.hideoption = false;
                $scope.hideLimit = true;
                // $scope.surveyquestion.skipdate = '';

            } else if (newValue == 'nt') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = true;
                $scope.actionOnYes = true;
                $scope.actionOn = true;
                $scope.hideoption = true;
                $scope.hideLimit = false;
                $scope.YesPopup = true;
                $scope.YesMessage = true;
                $scope.YesTodo = true;
                $scope.NoPopup = true;
                $scope.NoTodo = true;
                // $scope.surveyquestion.skipdate = '';

            } else if (newValue == 'r') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = false;
                $scope.actionOnYes = false;
                $scope.flag = true;
                $scope.hideoption = false;
                $scope.hideLimit = true;
                /* $scope.YesPopup = true;
     $scope.YesMessage = true;
     $scope.YesTodo = true;*/
                //$scope.surveyquestion.skipdate = '';


            }
            /* else if (newValue == 'cc') {
                 $scope.NotSkipQuestion = true;
                 $scope.NotSubQuestion = true;
                 $scope.NotCountedCheckbox = false;
                 $scope.NotRange = true;
                 $scope.NotScheme = true;
             }  else if (newValue == 'rt') {
                 $scope.NotSkipQuestion = true;
                 $scope.NotSubQuestion = true;
                 $scope.NotCountedCheckbox = true;
                 $scope.NotRange = false;
                 $scope.NotScheme = true;
             } else if (newValue == 'scheme') {
                 $scope.NotSkipQuestion = true;
                 $scope.NotSubQuestion = true;
                 $scope.NotCountedCheckbox = true;
                 $scope.NotRange = true;
                 $scope.NotScheme = false;
             } else {
                 $scope.NotSubQuestion = true;
                 $scope.NotSkipQuestion = true;
                 $scope.NotCountedCheckbox = true;
                 $scope.NotRange = true;
                 $scope.NotScheme = true;
                 $scope.actionOn = false;
                 $scope.hideoption = false;
             }*/
        });

        $scope.$watch('surveyquestion.yesaction', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'popup') {
                $scope.YesPopup = false;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = false;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
                $scope.YesTodo = true;
                $scope.YesMessage = true;
            } else {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
                $scope.YesTodo = true;
                $scope.YesMessage = true;
            }
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);

        });
        $scope.$watch('surveyquestion.noaction', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'popup') {
                $scope.NoPopup = false;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = false;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
                $scope.YesTodo = true;
                $scope.YesMessage = true;
            } else if (newValue == 'increment') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonly') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonlyskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'providenextquestion') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = false;
            } else {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
                $scope.YesTodo = true;
                $scope.YesMessage = true;
            }
        });

      /*  $scope.$watch('surveyquestion.skipdate', function (newValue, oldValue) {
            console.log('skipdate', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            }  else if (newValue.length == 1) {
                if (newValue[0] == 'yes') {
                    $scope.skipDate = false;
                    $scope.skipTypology = true;
                } else if (newValue[0] == 'yes1') {
                    $scope.skipTypology = false;
                    $scope.skipDate = true;
                }

            } else if (newValue.length == 2) {
                $scope.skipDate = false;
                $scope.skipTypology = false;
            } else {
              
                $scope.skipDate = true;
                $scope.skipTypology = true;
            }
        }); */
    
      $scope.$watch('surveyquestion.skipdate', function (newValue, oldValue) {
            console.log('skipdate', newValue);
            if (newValue === oldValue || newValue == '' || newValue.length == 0) {
                $scope.skipDate = true;
                $scope.skipTypology = true;
                $scope.surveyquestion.skiponTypologyId = '';
                 $scope.surveyquestion.skipondate = '';
                 $scope.surveyquestion.skipdate = false;
                    $scope.surveyquestion.skiponTypology = false;
               // return;
                console.log('here');
            } 
         else if (newValue.length == 1) {
                if (newValue[0] == 'yes') {
                    $scope.skipDate = false;
                    $scope.skipTypology = true;
                    $scope.surveyquestion.skiponTypologyId = '';
                    
                } else if (newValue[0] == 'yes1') {
                    $scope.skipTypology = false;
                    $scope.skipDate = true;
                    $scope.surveyquestion.skipondate = '';
                }

            } else if (newValue.length == 2) {
                $scope.skipDate = false;
                $scope.skipTypology = false;
            } else {
                console.log('here');
                $scope.skipDate = true;
                $scope.skipTypology = true;
                $scope.surveyquestion.skiponTypologyId = '';
                 $scope.surveyquestion.skipondate = '';
            }
        });

        $scope.$watch('surveyquestion.skipquestion', function (newValue, oldValue) {
            console.log('skipquestion', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == true) {
                $scope.skipQuestion = false;
            } else {
                $scope.skipQuestion = true;
            }
        });

        $scope.YesTodo = true;
        $scope.YesMessage = true;
        $scope.YesDocument = true;
        $scope.NoTodo = true;
        $scope.NoDocument = true;
        $scope.$watch('surveyquestion.yespopup', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'todos') {
                $scope.YesTodo = false;
                $scope.YesMessage = true;
                $scope.YesDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;

            } else if (newValue == 'applicationflow') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;
            } else if (newValue == 'message') {
                $scope.YesMessage = false;
                $scope.YesTodo = true;
            } else {
                $scope.YesTodo = true;
                $scope.YesDocument = true;
                $scope.YesMessage = true;
            }
            // $scope.bool = $scope.original.yesdocumentflag;
            //console.log('$scope.bool', $scope.bool);
            // $scope.surveyquestion.yesdocumentflag = $scope.bool.toString();
            // console.log('$scope.surveyquestion.yesdocumentflag', $scope.surveyquestion.yesdocumentflag);
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);

        });

        $scope.$watch('surveyquestion.nopopup', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'todos') {
                $scope.NoTodo = false;
                $scope.NoDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else if (newValue == 'applicationflow') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else {
                $scope.NoTodo = true;
                $scope.NoDocument = true;
            }
            $scope.bool = $scope.original.nodocumentflag;
            $scope.surveyquestion.nodocumentflag = $scope.bool.toString();
            // console.log('$scope.surveyquestion.nodocumentflag', $scope.surveyquestion.nodocumentflag);
        });

        // $scope.yesdocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().$object;

        $scope.$watch('surveyquestion.yesdocumentflag', function (newValue, oldValue) {
            //console.log('oldValue', oldValue);
            console.log('newValue', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                if (newValue == 'true' || newValue == true) {
                    Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (response) {
                        $scope.yesdocumentsorschemes = response;
                        // $scope.surveyquestion = Restangular.copy($scope.original);
                        $scope.surveyquestion.yesdocumentid = $scope.original.yesdocumentid;
                    });;
                    // console.log('$scope.yesdocumentsorschemes', $scope.yesdocumentsorschemes);
                } else {
                    $scope.yesdocumentsorschemes = [{
                        id: 1,
                        name: 'One'
                    }, {
                        id: 2,
                        name: 'Two'
                    }, {
                        id: 3,
                        name: 'Three'
                    }, {
                        id: 4,
                        name: 'Four'
                        }, {
                        id: 5,
                        name: 'Five'
                    }];
                }
                //$scope.surveyquestion = Restangular.copy($scope.original);
                $scope.surveyquestion.yesdocumentid = $scope.original.yesdocumentid;
            }
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);
        });

        $scope.$watch('surveyquestion.nodocumentflag', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                if (newValue == true || newValue == 'true') {
                    $scope.nodocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().$object;
                } else {
                    $scope.nodocumentsorschemes = [{
                        id: 1,
                        name: 'One'
                        }, {
                        id: 2,
                        name: 'Two'
                        }, {
                        id: 3,
                        name: 'Three'
                        }, {
                        id: 4,
                        name: 'Four'
                        }, {
                        id: 5,
                        name: 'Five'
                        }];
                }
            }
        });


       


        $scope.sub_surveyquestions = [];
        $scope.$watch('taskbreakup', function (newValue, oldValue) {
            if (newValue > 20) {
                $scope.taskbreakup = '';
                alert('Task Breakup Cannot Exceed 20');
            }
            if (newValue <= 20) {
                $scope.sub_surveyquestions = [];
                for (var i = 0; i < newValue; i++) {

                    $scope.sub_surveyquestions.push({
                        "question": null,
                        "gujrathi": null,
                        "surveytypeid": null,
                        "surveysubcategoryid": null,
                        "questiontype": null,
                        "noofdropdown": null,
                        "answers": null,
                        "questionid": null,
                        "questionno": null
                    });

                }

            }
        });


        $scope.validatestring = '';
       

        $scope.updateSurveyQuestion = function () {

            document.getElementById('question').style.border = "";
            document.getElementById('noofoptions').style.border = "";
            document.getElementById('answeroptions').style.border = "";
            document.getElementById('questionno').style.border = "";
            document.getElementById('upper').style.border = "";



            if ($scope.surveyquestion.language == '' || $scope.surveyquestion.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a Language';
            } else if ($scope.surveyquestion.language == 1) {
                if ($scope.surveyquestion.serialno == '' || $scope.surveyquestion.serialno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Question Serial no';
                    document.getElementById('questionno').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == '' || $scope.surveyquestion.questiontype == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Question Type';
                } else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == 'nt') {
                    if ($scope.surveyquestion.range == '' || $scope.surveyquestion.range == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter Upper Limit';
                        document.getElementById('upper').style.borderColor = "#FF0000";
                    }
                } else if ($scope.surveyquestion.questiontype == 'c' || $scope.surveyquestion.questiontype == 'rb') {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    } else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    }
                } else if ($scope.surveyquestion.questiontype == 'r' && $scope.flag === true) {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    } else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                        $scope.validatestring = $scope.validatestring + 'Please select action on yes';
                    } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                        $scope.validatestring = $scope.validatestring + 'Please select action on No';
                    }
                }



            } else if ($scope.surveyquestion.language != 1) {
                if ($scope.surveyquestion.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select question in English';

                } else if ($scope.surveyquestion.serialno == '' || $scope.surveyquestion.serialno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Question Serial no';
                    document.getElementById('questionno').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == '' || $scope.surveyquestion.questiontype == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Question Type';
                } else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == 'nt') {
                    if ($scope.surveyquestion.range == '' || $scope.surveyquestion.range == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter Upper Limit';
                        document.getElementById('upper').style.borderColor = "#FF0000";
                    }
                } else if ($scope.surveyquestion.questiontype == 'c' || $scope.surveyquestion.questiontype == 'rb') {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    } else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    }
                } else if ($scope.surveyquestion.questiontype == 'r' && $scope.flag === true) {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    } else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                        $scope.validatestring = $scope.validatestring + 'Please select action on yes';
                    } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                        $scope.validatestring = $scope.validatestring + 'Please select action on No';
                    }
                }


            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.surveyquestion.parentId === '') {
                    delete $scope.surveyquestion['parentId'];
                }
                if ($scope.skipDate == false) {
                    $scope.surveyquestion.skipdate = true;
                }
                if ($scope.skipTypology == false) {
                    $scope.surveyquestion.skiponTypology = true;
                }
                $scope.submitDisable = true;
                $scope.surveyquestion.parentFlag = $scope.showenglishLang;

                var count = 0;

                $scope.submitsurveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
                    console.log('main question saved', surveyresponse);

                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                    setTimeout(function () {
                        window.location = '/onetoonequestion';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'surveyquestion_lang_parenrid') {
                        alert('Value already exists for this language');

                    }
                });

            };
            //};
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        if ($routeParams.id) {
            $scope.message = 'Survey question has been Updated!';
            Restangular.one('surveyquestions', $routeParams.id).get().then(function (surveyquestion) {
                $scope.original = surveyquestion;
                $scope.questionValue = surveyquestion.serialno;
                $scope.surveyquestionansweroptions = surveyquestion.answeroptions;

                $scope.surveyquestion = Restangular.copy($scope.original);
                console.log('$scope.surveyquestion', $scope.surveyquestion);


                if ($scope.surveyquestion.skipdate == true && $scope.surveyquestion.skiponTypology == true) {
                    console.log('both true')
                    $scope.surveyquestion.skipdate = ['yes', 'yes1'];
                } else if ($scope.surveyquestion.skipdate == false && $scope.surveyquestion.skiponTypology == false) {
                    console.log('both false')
                    $scope.surveyquestion.skipdate = [];
                } else if ($scope.surveyquestion.skipdate == true && $scope.surveyquestion.skiponTypology == false) {
                    console.log('skipdate true')
                    $scope.surveyquestion.skipdate = ['yes'];
                } else if ($scope.surveyquestion.skipdate == false && $scope.surveyquestion.skiponTypology == true) {
                    console.log('skiponTypology true')
                    $scope.surveyquestion.skipdate = ['yes1'];
                }



            });
        } 
    });
