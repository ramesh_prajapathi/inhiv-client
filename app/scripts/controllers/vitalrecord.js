'use strict';

angular.module('secondarySalesApp')
    .controller('VitalRecordCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/vitalrecord/create' || $location.path() === '/vitalrecord/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/vitalrecord/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/vitalrecord/create' || $location.path() === '/vitalrecord/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/vitalrecord/create' || $location.path() === '/vitalrecord/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/vitalrecord-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
    $scope.OtherLang = true;

        $scope.$watch('vitalrecord.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                     $scope.OtherLang = false;
                }
                
            }
        });
    
      $scope.$watch('vitalrecord.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('vitalrecords', newValue).get().then(function (sts) {
                    $scope.vitalrecord.orderNo = sts.orderNo;
                    $scope.vitalrecord.lowerBound = sts.lowerBound;
                    $scope.vitalrecord.upperBound = sts.upperBound;
                    $scope.vitalrecord.mandatoryFlag = sts.mandatoryFlag;
                    
                });
            }
        });
        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Vital Record has been Updated!';
            Restangular.one('vitalrecords', $routeParams.id).get().then(function (vitalrecord) {
                $scope.original = vitalrecord;
                $scope.vitalrecord = Restangular.copy($scope.original);
               
            });
            Restangular.all('vitalrecords?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;
                //console.log('$scope.AllvitalRecord', $scope.AllvitalRecord);
            });
            
        } else {
            $scope.message = 'Vital Record has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/
        Restangular.all('vitalrecords?filter[where][deleteFlag]=false').getList().then(function (vital) {
            $scope.vitalrecords = vital;
            angular.forEach($scope.vitalrecords, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    member.langname = lng.name;
                });
            });
        });

        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (sev) {
            $scope.vrlanguages = sev;
        });

        Restangular.all('vitalrecords?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.englishseverityofincidents = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };
        /********************************************* SAVE *******************************************/
        $scope.vitalrecord = {
            "name": '',
            "deleteFlag": false,
            "mandatoryFlag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.vitalrecord.language == '' || $scope.vitalrecord.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.name == '' || $scope.vitalrecord.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }else if ($scope.vitalrecord.orderNo == '' || $scope.vitalrecord.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Order No';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            else if ($scope.vitalrecord.lowerBound == '' || $scope.vitalrecord.lowerBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Lower Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            else if ($scope.vitalrecord.upperBound == '' || $scope.vitalrecord.upperBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Upper Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.vitalrecord.parentId === '') {
                    delete $scope.vitalrecord['parentId'];
                }
                $scope.submitDisable = true;
                $scope.vitalrecord.parentFlag = $scope.showenglishLang;
                $scope.vitalrecords.post($scope.vitalrecord).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    window.location = '/vitalrecord-list';
                }, function (error) {
                    if (error.data.error.constraint === 'vitalrecord_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

           if ($scope.vitalrecord.language == '' || $scope.vitalrecord.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.vitalrecord.name == '' || $scope.vitalrecord.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }else if ($scope.vitalrecord.orderNo == '' || $scope.vitalrecord.orderNo == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Order No';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            else if ($scope.vitalrecord.lowerBound == '' || $scope.vitalrecord.lowerBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Lower Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            else if ($scope.vitalrecord.upperBound == '' || $scope.vitalrecord.upperBound == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Upper Bound';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.vitalrecord.parentId === '') {
                    delete $scope.vitalrecord['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('vitalrecords', $routeParams.id).customPUT($scope.vitalrecord).then(function (updateResp) {
                    
                    console.log('Test Name Saved');
                    $scope.MandatoryUpdate(updateResp);
                   
                }, function (error) {
                    if (error.data.error.constraint === 'vitalrecord_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };
    
    /**************************Updating all palce flag value**************************/
    
      $scope.upgateFlagCount = 0;
    $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {
                
                $scope.updateallRecord.orderNo = myResponse.orderNo;
                $scope.updateallRecord.lowerBound = myResponse.lowerBound;
                $scope.updateallRecord.upperBound = myResponse.upperBound;
                $scope.updateallRecord.mandatoryFlag = myResponse.mandatoryFlag;
               Restangular.one('vitalrecords', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                     console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                   $scope.MandatoryUpdate(myResponse);

                });
            }
            else{
                
                 $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('Test Name Saved');
                    window.location = '/vitalrecord-list';
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

                var sort = $scope.sort;

                if (sort.active == column) {
                    return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
                }
            }
            /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('vitalrecords/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });