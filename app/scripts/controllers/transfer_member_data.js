'use strict';

angular.module('secondarySalesApp')
    .controller('TrasnferMemberDataCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter, $modal) {

        ///////////////ONE TO ONE ANSWERS/////////////////////////////
        if ($window.sessionStorage.language == 1) {
            $scope.printNotapplicable = 'N/A';
            $scope.printFormalSaving = 'Formal Saving';
            $scope.printInFormalSaving = 'Informal Saving';
        } else if ($window.sessionStorage.language == 2) {
            $scope.printNotapplicable = 'लागू नहीं';
            $scope.printFormalSaving = 'औपचारिक बचत';
            $scope.printInFormalSaving = 'अनौपचारिक बचत';
        } else if ($window.sessionStorage.language == 3) {
            $scope.printNotapplicable = 'ಅನ್ವಯಿಸುವುದಿಲ್ಲ';
            $scope.printFormalSaving = 'ಔಪಚಾರಿಕ ಉಳಿತಾಯ';
            $scope.printInFormalSaving = 'ಅನೌಪಚಾರಿಕ ಉಳಿತಾಯ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.printNotapplicable = 'பொருந்தாது';
            $scope.printFormalSaving = 'முறையான சேமிப்பு';
            $scope.printInFormalSaving = 'முறைசாரா சேமிப்பு';
        } else if ($window.sessionStorage.language == 5) {
            $scope.printNotapplicable = 'వర్తించదు';
            $scope.printFormalSaving = 'ఫార్మల్ సేవ్';
            $scope.printInFormalSaving = 'అనధికార పొదుపు';
        } else if ($window.sessionStorage.language == 6) {
            $scope.printNotapplicable = 'लागू नाही';
            $scope.printFormalSaving = 'औपचारिक बचत';
            $scope.printInFormalSaving = 'अनौपचारिक बचत';
        }
        Restangular.all('surveyquestions').getList().then(function (surveyquestions) {
            $scope.AllSurveyQuestions = surveyquestions;
            Restangular.all('surveyanswers?filter[where][pillarid]=5&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (HRes) {
                $scope.HealthAnswers = HRes;
                angular.forEach($scope.HealthAnswers, function (member, index) {
                    member.index = index + 1;
                    if (member.questionid == 2 || member.questionid == 3 || member.questionid == 4 || member.questionid == 64) {
                        member.colour = "#3498DB";
                    } else {
                        member.colour = "";
                    }
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=2&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (ssjRes) {
                $scope.SSJAnswers = ssjRes;
                // console.log('$scope.SSJAnswers',$scope.SSJAnswers);
                angular.forEach($scope.SSJAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=1&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (spRes) {
                $scope.SPAnswers = spRes;
                angular.forEach($scope.SPAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=3&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (fsRes) {
                $scope.FSAnswers = fsRes;
                //console.log('$scope.FSAnswers',$scope.FSAnswers)
                angular.forEach($scope.FSAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][pillarid]=4&filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (idsRes) {
                $scope.IDSAnswers = idsRes;
                angular.forEach($scope.IDSAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=5' + '&filter[where][questionid]=64').getList().then(function (idsRes) {
                $scope.RemainingAnswers = idsRes;
                angular.forEach($scope.RemainingAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });
        });
        $scope.HealthViewAnswer = function (ansId, quesId) {
            console.log('quesId', quesId);
            Restangular.one('surveyanswers', ansId).get().then(function (HealthAnsRes) {
                //console.log('HealthAnsRes', HealthAnsRes);
                $scope.AnsDate = HealthAnsRes.lastupdatedtime;
                Restangular.one('surveyquestions', HealthAnsRes.questionid).get().then(function (HealthQuesRes) {
                    // console.log('HealthQuesRes', HealthQuesRes);
                    var pillarid = HealthQuesRes.pillarid;
                    console.log('pillarid', pillarid);

                    if (pillarid == 5) {
                        //$scope.DisplayClickOption = languagedata;
                        if ($window.sessionStorage.language == 1) {
                            $scope.PillarName = 'HEALTH';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 2) {
                            $scope.PillarName = 'स्वास्थ्य';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 3) {
                            $scope.PillarName = 'ಆರೋಗ್ಯ';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 4) {
                            $scope.PillarName = 'ஆரோக்கியம்';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 5) {
                            $scope.PillarName = 'ఆరోగ్యం'
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 6) {
                            $scope.PillarName = 'आरोग्य'
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        }
                    };
                    if (quesId == 64) {
                        $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflaststitest;
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/DatePickerView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if (quesId == 2) {
                        //$scope.printcondomasked = $scope.DisplayBeneficiary.condomsasked.split(':')[1];
                        $scope.printcondomasked1 = $scope.DisplayBeneficiary.condomsasked.split(',');
                        $scope.printcondomasked2 = $scope.printcondomasked1[$scope.printcondomasked1.length - 1];
                        $scope.printcondomprovided1 = $scope.DisplayBeneficiary.condomsprovided.split(',');
                        $scope.printcondomprovided2 = $scope.printcondomprovided1[$scope.printcondomprovided1.length - 1];
                        $scope.printcondomasked = $scope.printcondomasked2.split(':')[3];
                        $scope.printcondomprovided = $scope.printcondomprovided2.split(':')[3];
                        $scope.printcondomdateasked = $scope.DisplayBeneficiary.dateprovided;
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/condomdetailsView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if (quesId == 3) {
                        $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflasthivtest;
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/DatePickerView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if (quesId == 4) {
                        $scope.pickerselectdate = $scope.DisplayBeneficiary.monthofdetection;
                        //console.log('');
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/DatePickerHIVView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if (quesId == 83) {
                        $scope.pickerselectdate = HealthAnsRes.lastupdatedtime;
                        //console.log('');
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/DatePickerHIVView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                });
            });
        };
        $scope.FSViewAnswer = function (ansId, quesId) {
            // console.log('quesId', quesId);
            Restangular.one('surveyanswers', ansId).get().then(function (HealthAnsRes) {
                //console.log('HealthAnsRes', HealthAnsRes.answer);
                //$scope.HealthAnsRes = HealthAnsRes;
                $scope.AnsDate = HealthAnsRes.lastupdatedtime;
                Restangular.one('surveyquestions', HealthAnsRes.questionid).get().then(function (HealthQuesRes) {
                    // console.log('HealthQuesRes', HealthQuesRes);
                    var pillarid = HealthQuesRes.pillarid;
                    if (pillarid == 3) {
                        if ($window.sessionStorage.language == 1) {
                            $scope.PillarName = 'FS';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 2) {
                            $scope.PillarName = 'एफ एस';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 3) {
                            $scope.PillarName = 'ಆರೋಗ್ಯ';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 4) {
                            $scope.PillarName = 'ஆரோக்கியம்';
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 5) {
                            $scope.PillarName = 'ఆరోగ్యం'
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        } else if ($window.sessionStorage.language == 6) {
                            $scope.PillarName = 'एफ एस'
                            $scope.QuestionName = HealthQuesRes.question;
                            $scope.DisplayClickOption = HealthAnsRes.answer;
                        }
                    }
                    // });
                    if (quesId == 17) {
                        if ($scope.DisplayBeneficiary.savings == true) {
                            $scope.Printsavings = 'YES';
                        } else {
                            $scope.Printsavings = 'NO';
                        }
                        if ($scope.DisplayBeneficiary.insurance == true) {
                            $scope.Printinsurance = 'YES';
                        } else {
                            $scope.Printinsurance = 'NO';
                        }
                        if ($scope.DisplayBeneficiary.pension == true) {
                            $scope.Printpension = 'YES';
                        } else {
                            $scope.Printpension = 'NO';
                        }
                        if ($scope.DisplayBeneficiary.credit == true) {
                            $scope.Printcredit = 'YES';
                        } else {
                            $scope.Printcredit = 'NO';
                        }
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/FinancialLiteracyView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if (quesId == 34) {
                        if ($scope.DisplayBeneficiary.finplansavings == true) {
                            $scope.Printsavings = 'YES';
                        } else {
                            $scope.Printsavings = 'NO';
                        }
                        if ($scope.DisplayBeneficiary.finplaninsurance == true) {
                            $scope.Printinsurance = 'YES';
                        } else {
                            $scope.Printinsurance = 'NO';
                        }
                        if ($scope.DisplayBeneficiary.finplanpension == true) {
                            $scope.Printpension = 'YES';
                        } else {
                            $scope.Printpension = 'NO';
                        }
                        if ($scope.DisplayBeneficiary.finplancredit == true) {
                            $scope.Printcredit = 'YES';
                        } else {
                            $scope.Printcredit = 'NO';
                        }
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/FinancialPlaningView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if (quesId == 19) {
                        $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflastsaving;
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/DatePickerView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    } else if (quesId == 20) {
                        $scope.pickerselectdate = $scope.DisplayBeneficiary.monthoflastsaving;
                        $scope.modalToDo = $modal.open({
                            animation: true,
                            templateUrl: 'template/DatePickerView.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                });
            });
        }

        $scope.IDSViewAnswer = function (ansId, quesId) {
            console.log('quesId', quesId);
            $scope.phonetypes = Restangular.all('phonetypes').getList().$object;
            Restangular.one('surveyanswers', ansId).get().then(function (HealthAnsRes) {
                $scope.AnsDate = HealthAnsRes.lastupdatedtime;
                Restangular.one('surveyquestions', HealthAnsRes.questionid).get().then(function (HealthQuesRes) {
                    Restangular.one('beneficiaries', HealthAnsRes.beneficiaryid).get().then(function (BenRes) {
                        $scope.phonetyp = BenRes.phonetype;
                        if ($scope.phonetyp == 1) {
                            $scope.phonetype.phonetp = 1;
                        } else if ($scope.phonetyp == 2) {
                            $scope.phonetype.phonetp = 2;
                        } else if ($scope.phonetyp == 3) {
                            $scope.phonetype.phonetp = 3;
                        }

                    });

                    if ($window.sessionStorage.language == 1) {
                        $scope.PillarName = 'IDS';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 2) {
                        $scope.PillarName = 'आईडीएस';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 3) {
                        $scope.PillarName = 'ಆರೋಗ್ಯ';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 4) {
                        $scope.PillarName = 'ஆரோக்கியம்';
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 5) {
                        $scope.PillarName = 'ఆరోగ్యం'
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    } else if ($window.sessionStorage.language == 6) {
                        $scope.PillarName = 'आईडीएस'
                        $scope.QuestionName = HealthQuesRes.question;
                        $scope.DisplayClickOption = HealthAnsRes.answer;
                    }
                    //}
                    $scope.modalINFO = $modal.open({
                        animation: true,
                        templateUrl: 'template/PhoneTypeView.html',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false
                    });
                });
            });

        }

        $scope.OkDpView = function () {
            $scope.modalToDo.close();
        }

        $scope.okPT = function () {
            $scope.modalINFO.close();
        };
        ///////////////ONE TO ONE ANSWERS/////////////////////////////


        ///////////////TODOS/////////////////////////////
        $scope.TotalTodos = [];
        $scope.currentPage = 1;
        $scope.pageSize = 20;
        $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false').getList().then(function (scheme) {
            $scope.printschemes = scheme;
            angular.forEach($scope.printschemes, function (member, index) {
                member.index = index;
                member.enabled = false;
            });
        });
        $scope.docm = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (docmt) {
            $scope.printdocuments = docmt;
            angular.forEach($scope.printdocuments, function (member, index) {
                member.index = index;
                member.enabled = false;
            });
        });

        $scope.todostat = Restangular.all('todostatuses').getList().then(function (todostat) {
            $scope.maintodostatuses = todostat;
        });

        $scope.todorprtstat = Restangular.all('currentstatusofcases').getList().then(function (todorprtstat) {
            $scope.mainreporttodostatuses = todorprtstat;
        });

        Restangular.all('beneficiaries?filter[where][id]=' + $routeParams.id).getList().then(function (partnrs) {
            $scope.partners = partnrs;
            $scope.emps = Restangular.all('employees').getList().then(function (emps) {
                $scope.TotalTodos = [];
                $scope.employees = emps;
                $scope.todotyp = Restangular.all('todotypes').getList().then(function (todotyp) {
                    $scope.maintodotypes = todotyp;
                    //$scope.zn = Restangular.all('todos?filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[order]=datetime%20DESC&filter[where][facility]=' + $window.sessionStorage.coorgId+'&filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId+'&filter={"where": {"status": {"inq": ["1", "2"]}}}').getList().then(function (zn) {
                    $scope.zn = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (myRes) {
                        $scope.todos = myRes;
                        $scope.TotalTodos = [];
                        //$scope.loading2 = false;
                        angular.forEach($scope.todos, function (member, index) {
                            member.index = index + 1;
                            member.backgroungclr = '#000000';
                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m];
                                    if ($scope.partners[m].deleteflag == true) {
                                        member.backgroungclr = '#D1160A';
                                    } else {
                                        member.backgroungclr = '#000000';
                                    }
                                    if ($scope.partners[m].transferred_flag == true) {
                                        member.transferred = true;
                                    } else {
                                        member.transferred = false;
                                    }
                                    break;
                                }
                            }
                            for (var n = 0; n < $scope.employees.length; n++) {
                                if (member.facility == $scope.employees[n].id) {
                                    member.COMember = $scope.employees[n];
                                    break;
                                }
                            }
                            for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                if (member.todotype == $scope.maintodotypes[o].id) {
                                    member.ActionType = $scope.maintodotypes[o];
                                    break;
                                }
                            }
                            for (var p = 0; p < $scope.printdocuments.length; p++) {
                                if (member.documentid == $scope.printdocuments[p].id) {
                                    member.DocumentName = $scope.printdocuments[p];
                                    break;
                                }
                            }
                            for (var p = 0; p < $scope.printschemes.length; p++) {
                                if (member.documentid == $scope.printschemes[p].id) {
                                    member.SchemeName = $scope.printschemes[p];
                                    break;
                                }
                            }

                            for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                                if (member.status == $scope.maintodostatuses[n].id) {
                                    member.TodoStatus = $scope.maintodostatuses[n];
                                    break;
                                }
                            }
                            member.datetime = member.datetime;
                            //member.purpose = $scope.getTodopurpose(member.purpose);
                            $scope.TotalTodos.push(member);
                            /*  if (index == 0) {
						      console.log('$scope.TotalTodos', $scope.TotalTodos);
						  }*/
                        });
                        $scope.rprt = Restangular.all('reportincidents?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (report) {
                            $scope.displayreportincidents = report;
                            //$scope.loading2 = false;
                            angular.forEach($scope.displayreportincidents, function (member, index) {
                                member.index = index + 1;
                                member.pillarid = 2;
                                member.todotype = 27;
                                member.datetime = member.followupdate;
                                member.backgroungclr = '#000000';
                                for (var m = 0; m < $scope.partners.length; m++) {
                                    if (member.beneficiaryid == $scope.partners[m].id) {
                                        member.Membername = $scope.partners[m];
                                        if ($scope.partners[m].deleteflag == true) {
                                            member.backgroungclr = '#D1160A';
                                        } else {
                                            member.backgroungclr = '#000000';
                                        }
                                        break;
                                    }
                                }
                                for (var n = 0; n < $scope.employees.length; n++) {
                                    if (member.facility == $scope.employees[n].id) {
                                        member.COMember = $scope.employees[n];
                                        break;
                                    }
                                }
                                for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                    if (member.todotype == $scope.maintodotypes[o].id) {
                                        member.ActionType = $scope.maintodotypes[o];
                                        break;
                                    }
                                }

                                for (var n = 0; n < $scope.mainreporttodostatuses.length; n++) {
                                    if (member.currentstatus == $scope.mainreporttodostatuses[n].name) {
                                        member.TodoStatus = $scope.mainreporttodostatuses[n];
                                        break;
                                    }
                                }
                                //member.COMember = $scope.getCoMember(member.facility);
                                //member.ActionType = $scope.getActionType(member.todotype);
                                member.datetime = member.datetime;
                                //member.purpose = $scope.getTodopurpose(member.purpose);
                                $scope.TotalTodos.push(member);
                            });
                        });
                    });
                });
            });
        });
        $scope.getSchemeDocumentStage = function (stid) {
            return Restangular.one('schemestages', stid).get().$object;
        };
        ///////////////TODOS/////////////////////////////
        ///////////////BENEFICIARY DATA/////////////////////////////
        $scope.DisplayBenef = Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
            $scope.DisplayBeneficiary = beneficiary;
            $scope.transferred_to = beneficiary.transferred_to;
            $scope.DisplayBeneficiary.dob = $scope.DisplayBeneficiary.dob.split('T')[0];
            //$scope.DisplayBeneficiary.condomsasked = $scope.DisplayBeneficiary.condomsasked.split(':')[1];
            //$scope.DisplayBeneficiary.condomsprovided = $scope.DisplayBeneficiary.condomsprovided.split(':')[1];
            if (beneficiary.consent == false) {
                $scope.openPillarStarted();
            }
            if (beneficiary.dynamicmember == true) {
                $scope.StringActiveMember = 'YES';
            } else {
                $scope.StringActiveMember = 'NO';
            }
            if (beneficiary.typology == true) {
                $scope.StringTypology = beneficiary.typology;
            } else {
                $scope.StringTypology = beneficiary.typology;
            }
            if (beneficiary.paidmember == true) {
                $scope.StringPaidMember = 'YES';
            } else {
                $scope.StringPaidMember = 'NO';
            }
            if (beneficiary.plhiv == true) {
                $scope.StringPlhiv = 'YES';
            } else {
                $scope.StringPlhiv = 'NO';
            }

            if (beneficiary.consent == true) {
                $scope.benconsent = 'YES';
            } else {
                $scope.benconsent = 'NO';
            }
            Restangular.one('employees', beneficiary.facility).get().then(function (comember) {
                $scope.Cohelpline = comember.mobile;
                $scope.FacilityName = comember.firstName;
            });
            Restangular.one('distribution-routes', beneficiary.site).get().then(function (site) {
                $scope.SiteName = site.name;
            });
        });
        ///////////////BENEFICIARY DATA/////////////////////////////

        /************************* Language *****************************************/
        $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.onetoneheader = langResponse.onetoneheader;
            $scope.previousanswers = langResponse.previousanswers;
            $scope.printlastmeetingdate = langResponse.lastmeetingdate;
            $scope.printfullname = langResponse.fullname;
            $scope.printavahanid = langResponse.avahanid;
            $scope.moredetails = langResponse.moredetails;
            $scope.lessdetails = langResponse.lessdetails;
            $scope.printactivemember = langResponse.activemember;
            $scope.printtypology = langResponse.typology;
            $scope.printdob = langResponse.dob;
            $scope.printnickname = langResponse.nickname;
            $scope.printtiid = langResponse.tiid;
            $scope.printtypology = langResponse.typology;
            $scope.printplhiv = langResponse.plhivprint;
            $scope.printpaidmember = langResponse.paidmember;
            $scope.printage = langResponse.age;
            $scope.monthofhivtest = langResponse.monthofhivtest;
            $scope.monthofstitest = langResponse.monthofstitest;
            $scope.currenthivstatus = langResponse.currenthivstatus;
            $scope.facilityhelplineno = langResponse.facilityhelplineno;
            $scope.todolist = langResponse.todolist;
            $scope.tododisplay = langResponse.tododisplay;
            $scope.date = langResponse.date;
            $scope.status = langResponse.status;
            $scope.prevquestionlist = langResponse.prevquestionlist;
            $scope.yes = langResponse.yes;
            $scope.no = langResponse.no;
            $scope.noofincidentmonth = langResponse.noofincidentmonth;
            $scope.helplineno = langResponse.helplineno;
            $scope.headerreportincident = langResponse.headerreportincident;
            $scope.documentscheme = langResponse.documentscheme;
            $scope.printreferenceno = langResponse.referenceno;
            $scope.dateoflastfs = langResponse.dateoflastfs;
            $scope.monthoflastsaving = langResponse.monthoflastsaving;
            $scope.noofsavingaccount = langResponse.noofsavingaccount;
            $scope.noofinsuranceproduct = langResponse.noofinsuranceproduct;
            $scope.noofpensionproduct = langResponse.noofpensionproduct;
            $scope.noofinvestmentproduct = langResponse.noofinvestmentproduct;
            $scope.noofinformalsaving = langResponse.noofinformalsaving;
            $scope.noofinformalcredit = langResponse.noofinformalcredit;
            $scope.noofformalcredit = langResponse.noofformalcredit;
            $scope.lastmembershipfeespaid = langResponse.lastmembershipfeespaid;
            $scope.dateanddetailnextevent = langResponse.dateanddetailnextevent;
            $scope.whetherbordpast = langResponse.whetherbordpast;
            $scope.proposed = langResponse.proposed;
            $scope.approved = langResponse.approved;
            $scope.noofsavingaccount = langResponse.noofsavingaccount;
            $scope.noofinsuranceproduct = langResponse.noofinsuranceproduct;
            $scope.noofpensionproduct = langResponse.noofpensionproduct;
            $scope.noofinvestmentproduct = langResponse.noofinvestmentproduct;
            $scope.noofinformalsaving = langResponse.noofinformalsaving;
            $scope.noofinformalcredit = langResponse.noofinformalcredit;
            $scope.pillar = langResponse.pillar;
            $scope.healthpillar = langResponse.healthpillar;
            $scope.ssjpillar = langResponse.ssjpillar;
            $scope.sppillar = langResponse.sppillar;
            $scope.fspillar = langResponse.fspillar;
            $scope.idspillar = langResponse.idspillar;
            $scope.alltaskpillar = langResponse.alltaskpillar;
            $scope.selecttodos = langResponse.selecttodos;
            $scope.selectstatus = langResponse.selectstatus;
            $scope.followupdate = langResponse.followupdate;
            $scope.monthtimeavailed = langResponse.monthtimeavailed;
            $scope.referencenumber = langResponse.referencenumber;
            $scope.todosprint = langResponse.todosprint;
            $scope.savebutton = langResponse.savebutton;
            $scope.update = langResponse.update;
            $scope.cancel = langResponse.cancel;
            $scope.create = langResponse.create;
            $scope.headerreportincident = langResponse.headerreportincident;
            $scope.whendidhappen = langResponse.whendidhappen;
            $scope.multiplepeopleeffect = langResponse.multiplepeopleeffect;
            $scope.membername = langResponse.membername;
            $scope.printmember = langResponse.member;
            $scope.divincidenttype = langResponse.divincidenttype;
            $scope.printphysical = langResponse.physical;
            $scope.sexual = langResponse.sexual;
            $scope.chieldrelated = langResponse.chieldrelated;
            $scope.emotional = langResponse.emotional;
            $scope.propertyrelated = langResponse.propertyrelated;
            $scope.severityofincident = langResponse.severityofincident;
            $scope.divperpetratordetail = langResponse.divperpetratordetail;
            $scope.police = langResponse.police;
            $scope.client = langResponse.client;
            $scope.serviceprovider = langResponse.serviceprovider;
            $scope.otherkp = langResponse.otherkp;
            $scope.neighbour = langResponse.neighbour;
            $scope.goons = langResponse.goons;
            $scope.partnersdisplay = langResponse.partners;
            $scope.PrintHusband = langResponse.husband;
            $scope.familiymember = langResponse.familiymember;
            $scope.othersexkp = langResponse.othersexkp;
            $scope.divreported = langResponse.divreported;
            $scope.reportedto = langResponse.reportedto;
            $scope.ngos = langResponse.ngos;
            $scope.friends = langResponse.friends;
            $scope.plv = langResponse.plv;
            $scope.coteam = langResponse.coteam;
            $scope.champion = langResponse.champion;
            $scope.otherkps = langResponse.otherkps;
            $scope.legalaidclinic = langResponse.legalaidclinic;
            $scope.divtimetorespond = langResponse.divtimetorespond;
            $scope.refferedcounselling = langResponse.refferedcounselling;
            $scope.refferedmedicalcare = langResponse.refferedmedicalcare;
            $scope.refferedcomanager = langResponse.refferedcomanager;
            $scope.refferedplv = langResponse.refferedplv;
            $scope.refferedaidclinic = langResponse.refferedaidclinic;
            $scope.refferedboardmember = langResponse.refferedboardmember;
            $scope.followupdate = langResponse.followupdate;
            $scope.divactiontaken = langResponse.divactiontaken;
            $scope.printdateofclosure = langResponse.dateofclosure;
            $scope.twohrs = langResponse.twohrs;
            $scope.twototwentyfourhrs = langResponse.twototwentyfourhrs;
            $scope.greatertwentyfourhrs = langResponse.greatertwentyfourhrs;
            $scope.currentstatuscase = langResponse.currentstatuscase;
            $scope.followuprequired = langResponse.followuprequired;
            $scope.name = langResponse.name;
            $scope.hotspot = langResponse.hotspot;
            $scope.phonenumber = langResponse.phonenumber;
            $scope.schemedocument = langResponse.schemedocument;
            $scope.applyforscheme = langResponse.applyforscheme;
            $scope.schemeordocument = langResponse.schemeordocument;
            $scope.scheme = langResponse.scheme;
            $scope.stage = langResponse.stage;
            $scope.responsereceive = langResponse.responsereceive;
            $scope.responserejection = langResponse.responserejection;
            $scope.responsedelay = langResponse.responsedelay;
            $scope.document = langResponse.document;
            $scope.monthyesrinfection = langResponse.monthyesrinfection;
            $scope.noofcondomask = langResponse.noofcondomask;
            $scope.noofcondomprovide = langResponse.noofcondomprovide;
            $scope.dateprovided = langResponse.dateprovided;
            $scope.saving = langResponse.saving;
            $scope.insurance = langResponse.insurance;
            $scope.pension = langResponse.pension;
            $scope.credit = langResponse.credit;
            $scope.remitance = langResponse.remitance;
            $scope.dateoflastsaving = langResponse.dateoflastsaving;
            $scope.state = langResponse.state;
            $scope.agegroup = langResponse.agegroup;
            $scope.gender = langResponse.gender;
            $scope.schemename = langResponse.schemename;
            $scope.schemelocalname = langResponse.schemelocalname;
            $scope.category = langResponse.category;
            $scope.select = langResponse.select;
            $scope.remitance = langResponse.remitance;
            $scope.clickyestorepeat = langResponse.clickyestorepeat;
            $scope.clicknotoview = langResponse.clicknotoview;
            $scope.searchfor = langResponse.searchfor;
            $scope.question = langResponse.question;
            $scope.answer = langResponse.answer;
            $scope.lastmodifytime = langResponse.lastmodifytime;
            $scope.monthavailed = langResponse.monthavailed;
            $scope.data = langResponse.data;
            $scope.ok = langResponse.ok;
            $scope.addbutton = langResponse.addbutton;
            $scope.questionprint = langResponse.question;
            $scope.selectprint = langResponse.select;
            $scope.monthofdetection = langResponse.monthofdetection;
            $scope.purposeofloan = langResponse.purposeofloan;
            $scope.amounttaken = langResponse.amounttaken;
            $scope.printnoofmonthrepay = langResponse.noofmonthrepay;
            $scope.printconsent = langResponse.consent;
            $scope.printconsentmsg = langResponse.consentmsg;
            $scope.optactiontaken = langResponse.optactiontaken;
            $scope.optreportedto = langResponse.optreportedto;
            $scope.optpreperator = langResponse.optpreperator;
            $scope.optincidenttype = langResponse.optincidenttype;
            $scope.atleastonemember = langResponse.atleastonemember;
            $scope.condomaskcantempty = langResponse.condomaskcantempty;
            $scope.condomprovidecantempty = langResponse.condomprovidecantempty;
            $scope.dateprovidedcantempty = langResponse.dateprovidedcantempty;
            $scope.gendernotmatch = langResponse.gendernotmatch;
            $scope.typologynotmatch = langResponse.typologynotmatch;
            $scope.agenotmatch = langResponse.agenotmatch;
            $scope.healthstnotmatch = langResponse.healthstnotmatch;
            $scope.socialstnotmatch = langResponse.socialstnotmatch;
            $scope.pnoactionreqmember = langResponse.noactionreqmember;
            $scope.noactiontaken = langResponse.noactiontaken;
            $scope.printaction = langResponse.action;
            $scope.actiontype = langResponse.actiontype;
            $scope.SiteLabel = langResponse.site;
            $scope.whendidhappen = langResponse.whendidhappen;
            $scope.mytodos = langResponse.mytodos;
            $scope.previousanswers = langResponse.previousanswers;
            $scope.memberdetails = langResponse.memberdetails;
        });

        $scope.redirect = function () {
            $location.path('/members/' + $scope.transferred_to);
        };
    });