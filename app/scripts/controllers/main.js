'use strict';
angular.module('secondarySalesApp').controller('MainCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $rootScope, $idle, $modal, $filter, $route, anchorSmoothScroll) {

        $rootScope.clickHome();


        // document.getElementById('disablebulk').style.background = "none";
        document.getElementById('disablebulk').style.cursor = "not-allowed";

        $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;

        //new changes to enable add new button based on role
        $scope.disableAddButton = false;
        //$scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
        if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
            $scope.disableAddButton = true;
            $scope.roles.enableCondition = false;
        }
        /* else if ($window.sessionStorage.roleId + "" === "13") {
             $scope.disableAddButton = false;
             $scope.roles.enableCondition = true;
         }
         else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 20) {
             $scope.disableAddButton = false;
             $scope.roles.enableCondition = true;
         }
         else if ($window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 18) {
             $scope.disableAddButton = false;
             $scope.roles.enableCondition = true;
         }*/
        //new changes ends

        $scope.emps = Restangular.all('employees').getList().then(function (emps) {
            $scope.employees = emps;
            $scope.todotyp = Restangular.all('todotypes').getList().then(function (todotyp) {

                $scope.maintodotypes = todotyp;
            });
        });


        if ($window.sessionStorage.roleId != 1) {
            // $scope.Main_modalInstanceLoad.close();
            $scope.main_toggleLoading = function () {
                $scope.Main_modalInstanceLoad = $modal.open({
                    animation: true,
                    templateUrl: 'template/MainLodingModal.html',
                    scope: $scope,
                    backdrop: 'static',
                    size: 'sm'
                });
            };
        }

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
        });

        Restangular.one('lhslanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langRespmain) {
            $scope.lhsmainLang = langRespmain[0];
        });

        //new cahnges for todo popup ends

        //todo changes for pop" what you want to add?
        $scope.confirmTodoModal = false;

        $scope.todoconfirm = {};

        $scope.confirmTodo = function () {
            $scope.disabledTodo = true;
            $scope.confirmTodoModal = true;
            /* if ($scope.roleId == 3 || $scope.roleId == 7 || $scope.roleId == 8) {
                 $scope.todoconfirm.value = '';
                 $scope.confirmTodoModal = true;
             }*/
        };

        $scope.disabledTodo = true;
        $scope.clickAddPopUp = function () {
            $scope.disabledTodo = false;
        };

        $scope.confirmTodoPopup = function () {
            if ($scope.todoconfirm.value == 'addinmember') {
                $scope.confirmTodoModal = false;
                window.location = "/members/create";
                $scope.todoconfirm.value = '';
            } else if ($scope.todoconfirm.value == 'startonetoone') {
                $scope.confirmTodoModal = false;
                $scope.openOnetoOnetodo();
                $scope.todoconfirm.value = '';
                //window.location = "/condition/create";
            } else if ($scope.todoconfirm.value == 'screentreat') {
                $scope.confirmTodoModal = false;
                $scope.openMemberPopupOpentodo();
                $scope.todoconfirm.value = '';
                // window.location = "/condition/create";
            } else if ($scope.todoconfirm.value == 'addintoDo') {
                $scope.confirmTodoModal = false;
                $scope.openMemberfortodo();
                //window.location = "/todo/create";
                $scope.todoconfirm.value = '';
            } else {
                return;
            }
        };
        $scope.CLOSEBUTTON = function () {
            $scope.todoconfirm.value = '';
            $scope.confirmTodoModal = false;
        };

        $scope.openOnetoOnetodo = function () {
            $rootScope.fullname = '';
            $scope.confirmTodoModal = true;
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/OnetooOne.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okOnetooOne = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/onetoone/" + fullname);
            $route.reload();
        };

        $scope.cancelOnetooOne = function () {
            $scope.disabledTodo = false;
            $scope.modalInstance1.close();
        };
        //todo changes for pop" what you want to add? ends

        $scope.openMemberPopupOpentodo = function () {
            $scope.confirmTodoModal = true;
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/memberpopup.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        /*$scope.disableAddButton = false;
            $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
            if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                $scope.disableAddButton = true;
                $scope.roles.enableCondition = false;
            }*/


        $scope.okMemberpopup = function (fullname) {
            //$scope.confirmTodoModal = false;
            $scope.confirmTodoModal = true;
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okMemberpopup', $rootScope.fullname);
            window.location = "/condition/create";

            $route.reload();
        };

        $scope.cancelMemberPopup = function () {
            $scope.disabledTodo = false;
            $scope.modalInstance1.close();
        };

        $scope.openMemberfortodo = function () {
            $scope.confirmTodoModal = true;
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/todopoplist.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        /*$scope.disableAddButton = false;
            $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
            if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                $scope.disableAddButton = true;
                $scope.roles.enableCondition = false;
            }*/


        $scope.okMemberpoptodo = function (fullname) {
            //$scope.confirmTodoModal = false;
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okMemberpopup', $rootScope.fullname);
            window.location = "/todo/create";

            $route.reload();
        };

        $scope.cancelMemberPopup = function () {
            $scope.disabledTodo = false;
            $scope.modalInstance1.close();
        };

        /**************************************** todo Member *******************************************/
        $scope.someFocusVariable = true;
        $scope.FocusMe = true;
        $scope.filterFields = ['fullname', 'tiId', 'phonenumber'];
        $scope.searchInput = '';


        /********************** Language ********************/
        $scope.UserLanguage = $window.sessionStorage.language;

        $scope.todo = {
            status: 1,
            stateid: $window.sessionStorage.zoneId,
            state: $window.sessionStorage.zoneId,
            districtid: $window.sessionStorage.salesAreaId,
            district: $window.sessionStorage.salesAreaId,
            coid: $window.sessionStorage.coorgId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date()
        };

        /*labeling from toDo page*/
        Restangular.one('todolanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (todoResp) {
            $scope.todofwlabel = todoResp[0];
            //console.log("todoResp",$scope.todofwlabel);
            console.log("todofwlabel", $scope.todofwlabel);
            $scope.todomytodo = $scope.todofwlabel.mytodo;
            $scope.todofwtodo = $scope.todofwlabel.fwtodo;
            $scope.todoreset = $scope.todofwlabel.reset;
            $scope.todofwstatus = $scope.todofwlabel.status;
            $scope.todotype = $scope.todofwlabel.type;
            $scope.todosearch = $scope.todofwlabel.search;
            $scope.todomembername = $scope.todofwlabel.membername;
            $scope.todomemberid = $scope.todofwlabel.memberid;
            $scope.todoti = $scope.todofwlabel.ti;
            $scope.todosite = $scope.todofwlabel.site;
            $scope.todoactiontype = $scope.todofwlabel.actiontype;
            $scope.todofollowup = $scope.todofwlabel.followup;
            $scope.todoaction = $scope.todofwlabel.action;
            $scope.dueoverdue = $scope.todofwlabel.due;
            $scope.todostatustodo = $scope.todofwlabel.status;
            $scope.conDis = $scope.todofwlabel.condition;
            $scope.todoDis = $scope.todofwlabel.toodo;
            //$scope.typeId = 'TODO';

            if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 20) {
                // console.log('here')
                $scope.typeId = 'todo';
            } else if ($window.sessionStorage.roleId == 13) {

                $scope.typeId = 'todo';
            } else if ($window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 18) {

                $scope.typeId = 'condition';
            } else if ($window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 19) {

                $scope.typeId = 'condition';
            }

        });

        Restangular.one('lhslanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langresp) {

            $scope.welcome = langresp[0].welcome;
        });


        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        $scope.todo.datetime = sevendays;
        $scope.fs = {};
        $scope.health = {};
        $scope.sp = {};

        $scope.follow = {};
        $scope.picker = {};
        $scope.cdids = {};
        $scope.schememaster = {};
        $scope.plhiv = {};
        $scope.condom = {};
        $scope.finliteracy = {};
        $scope.finplanning = {};
        $scope.increment = {};
        $scope.beneficiarycondom = {};
        $scope.RemainingPillars = 3;
        $scope.SSJPillar = 2;
        $scope.TotalTodos = [];
        $scope.FWorkTotalTodos = [];
        //////////////////////////////////////////////////////////////////////////////////////////
        //$scope.pillars = Restangular.one('pillars').getList().$object;
        $scope.submitsurveyanswers = Restangular.all('surveyanswers');
        $scope.submittodos = Restangular.all('todos');
        $scope.submitreportincidents = Restangular.all('reportincidents');
        $scope.surveyanswer = {};
        $scope.beneficiaryupdate = {};
        $scope.todostatuses = Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().$object;
        $scope.sourceofinfections = Restangular.all('sourceofinfections').getList().$object;
        $scope.alltodos = Restangular.all('todos?filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().$object;
        $scope.healthtodos = Restangular.all('todos?filter[where][pillarid]=5&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().$object;
        $scope.ssjtodos = Restangular.all('reportincidents?filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][currentstatus][nlike]=Closed%').getList().$object;
        $scope.sptodos = Restangular.all('todos?filter[where][pillarid]=1&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().$object;
        $scope.fstodos = Restangular.all('todos?filter[where][pillarid]=3&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().$object;
        $scope.idstodos = Restangular.all('todos?filter[where][pillarid]=4&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId).getList().$object;
        $scope.schemestages = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().$object;
        $scope.financialgoals = Restangular.all('financialgoals').getList().$object;
        //$scope.noofmonthrepaid = Restangular.all('noofmonthrepay').getList().$object;
        $scope.responcedreceived = Restangular.all('responcedreceived').getList().$object;
        $scope.reasonforrejections = Restangular.all('reasonforrejections').getList().$object;
        $scope.reasonfordelayed = Restangular.all('reasonfordelayed').getList().$object;
        $scope.reportincidentfollowups = Restangular.all('reportincidentfollowups').getList().$object;
        $scope.submitdocumentmasters = Restangular.all('schememasters');
        Restangular.all('servityofincidents?filter[where][deleteflag]=false').getList().then(function (responseseservity) {
            $scope.servityofincidents = responseseservity;
        });
        Restangular.all('currentstatusofcases?filter[where][deleteflag]=false').getList().then(function (response) {
            $scope.currentstatusofcases = response;
        });

        $scope.conditiondsplyNew = Restangular.all('conditions?filter[where][deleteflag]=false').getList().$object;
        $scope.stepsdsplyNew = Restangular.all('conditionsteps?filter[where][deleteflag]=false').getList().$object;
        $scope.statusesdsplyNew = Restangular.all('conditionstatuses?filter[where][deleteflag]=false').getList().$object;
        $scope.followupsdsply = Restangular.all('conditionfollowups?filter[where][deleteflag]=false').getList().$object;
        $scope.reasonsdsply = Restangular.all('reasonforcancellations?filter[where][deleteflag]=false').getList().$object;

        $scope.DisableEvent = true;
        $scope.DisableOptions = true;
        $scope.DisableFollowupRI = true;
        $scope.hideSP = true;
        $scope.hideSSJ = true;
        $scope.hideFS = true;
        $scope.hideIDS = true;
        $scope.hideRejection = true;
        $scope.hideRejectRejection = true;
        $scope.hideVulnerability_Index = true;
        $scope.hideMember_Not_Met = true;
        $scope.hideDue_Overdue = true;
        $scope.hideHealth = true;

        /******************************* Welcome User Name *************************/
        Restangular.one('users', $window.sessionStorage.userId).get().then(function (user) {
            if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 20 || $window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 18 || $window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 19) {
                Restangular.one('comembers', user.profileId).get().then(function (userResp) {
                    //console.log('$scope.user', userResp);
                    $scope.user = userResp;
                    $scope.username = userResp.name;
                });
            } else {
                Restangular.one('comembers', user.profileId).get().then(function (userResp) {
                    //console.log('$scope.user', userResp);
                    $scope.user = userResp;
                    $scope.username = userResp.lastname;
                });
            }
        });

        $scope.searchDep = $window.sessionStorage.MemberFilter;

        /*********************** new Changes for box value display ********************/
        $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.lastmonthDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate = new Date();
        currentdate.setDate(currentdate.getDate() - 30);
        $scope.lastmonthDate = $filter('date')(currentdate, 'yyyy-MM-dd');
        //console.log('$scope.lastmonthDate', $scope.lastmonthDate, $scope.todayDate);
        $scope.onetooneCount = 0;
        $scope.conditionCount = 0;
        $scope.openTodoCount = 0;
        $scope.membernotMetinOneMonth = 0;

        if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 20) {
            console.log('here')

            //$scope.typeId = 'todo';

            Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][lastmeetingdate][gte]=' + $scope.lastmonthDate + 'T00:00:00.000Z').getList().then(function (part1) {
                // '&filter[where][lastmeetingdate]!=null'
                $scope.membernotMetinOneMonth = part1.length;
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;

                });
            });

            $scope.countArray = [];
            var dd = [];
            Restangular.all('surveyanswers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (onetoone) {
                for (var i = 0; i < onetoone.length; i++) {

                    $scope.countArray.push(onetoone[i].beneficiaryid);
                }
                $scope.onttooneTOtalCount($scope.countArray);

            });
            $scope.onttooneTOtalCount = function (countArray) {
                // console.log('countArray', countArray);
                for (var i = 0; i < countArray.length; i++) {
                    if (dd.indexOf(countArray[i]) === -1) {
                        dd.push(countArray[i]);
                    }
                    $scope.onetooneCount = dd.length;
                    //console.log('dd', dd);
                }

            }


            $scope.filterCall1 = 'conditionheaders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][caseClosed]=false';
            Restangular.all($scope.filterCall1).getList().then(function (condResp) {
                $scope.conditionCount = condResp.length;
                console.log('$scope.conditionCount', $scope.conditionCount);
            });

            Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][status]=8').getList().then(function (myRes) {

                $scope.openTodoCount = myRes.length;
            });



        } else if ($window.sessionStorage.roleId == 13) {

            // $scope.typeId = 'todo';
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                //  Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"lastmeetingdate":{"gte":["2019-02-06T00:00:00.000Z"]}},{"deleteflag":false}]}}').getList().then(function (part1) {
                $scope.filterLast = $scope.lastmonthDate + 'T00:00:00.000Z';

                Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"lastmeetingdate":{"gte":["' + $scope.filterLast + '"]}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part1) {
                    // console.log('part1', part1);

                    // Restangular.all('beneficiaries?filter[where][site][inq]=' + fw.sitesassigned + '&filter[where][deleteflag]=false' + '&filter[where][lastmeetingdate][gte]=' + $scope.lastmonthDate + 'T00:00:00.000Z').getList().then(function (part1) {

                    $scope.membernotMetinOneMonth = part1.length;
                    $scope.partners = part1;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });
                });

                Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (condResp) {
                    $scope.conditionCount = condResp.length;
                    console.log('$scope.conditionCount', $scope.conditionCount);
                });

                Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][status]=8').getList().then(function (myRes) {

                    $scope.openTodoCount = myRes.length;
                });

                $scope.countArray = [];
                var dd = [];
                Restangular.all('surveyanswers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (onetoone) {
                    for (var i = 0; i < onetoone.length; i++) {

                        $scope.countArray.push(onetoone[i].beneficiaryid);
                    }
                    $scope.onttooneTOtalCount($scope.countArray);

                });
                $scope.onttooneTOtalCount = function (countArray) {
                    // console.log('countArray', countArray);
                    for (var i = 0; i < countArray.length; i++) {
                        if (dd.indexOf(countArray[i]) === -1) {
                            dd.push(countArray[i]);
                        }
                        $scope.onetooneCount = dd.length;
                        //console.log('dd', dd);
                    }

                }


            });

        } else if ($window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 18) {

            // $scope.typeId = 'condition';

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                Restangular.all('beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}},{"lastmeetingdate":{"gte":["2019-02-06T00:00:00.000Z"]}},{"deleteflag":false}]}}').getList().then(function (part1) {

                    // Restangular.all('beneficiaries?filter[where][ictcid][inq]=' + fw.ictcId + '&filter[where][deleteflag]=false' + '&filter[where][lastmeetingdate][gte]=' + $scope.lastmonthDate + 'T00:00:00.000Z').getList().then(function (part1) {

                    $scope.membernotMetinOneMonth = part1.length;

                    $scope.partners = part1;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });
                });

                Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (condResp) {
                    $scope.conditionCount = condResp.length;
                    console.log('$scope.conditionCount', $scope.conditionCount);
                    $scope.openTodoCount11 = condResp.length;


                    Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][status]=8').getList().then(function (myRes) {
                        //console.log('myRes', myRes, $scope.openTodoCount11);
                        if (myRes.length > 0) {
                            $scope.openTodoCount = myRes.length;
                        } else {
                            $scope.openTodoCount = $scope.openTodoCount11;
                        }
                    });

                });

                $scope.countArray = [];
                var dd = [];
                Restangular.all('surveyanswers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (onetoone) {
                    for (var i = 0; i < onetoone.length; i++) {

                        $scope.countArray.push(onetoone[i].beneficiaryid);
                    }
                    $scope.onttooneTOtalCount($scope.countArray);

                });
                $scope.onttooneTOtalCount = function (countArray) {
                    // console.log('countArray', countArray);
                    for (var i = 0; i < countArray.length; i++) {
                        if (dd.indexOf(countArray[i]) === -1) {
                            dd.push(countArray[i]);
                        }
                        $scope.onetooneCount = dd.length;
                        //console.log('dd', dd);
                    }

                }
            });

        } else if ($window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 19) {

            //$scope.typeId = 'condition';

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                Restangular.all('beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"lastmeetingdate":{"gte":["2019-02-06T00:00:00.000Z"]}},{"deleteflag":false}]}}').getList().then(function (part1) {

                    //Restangular.all('beneficiaries?filter[where][artId][inq]=' + fw.artId + '&filter[where][deleteflag]=false' + '&filter[where][lastmeetingdate][gte]=' + $scope.lastmonthDate + 'T00:00:00.000Z').getList().then(function (part1) {

                    $scope.membernotMetinOneMonth = part1.length;

                    $scope.partners = part1;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });
                });

                Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (condResp) {
                    $scope.conditionCount = condResp.length;
                    $scope.openTodoCount111 = condResp.length;


                    Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][status]=8').getList().then(function (myRes) {

                        if (myRes.length > 0) {
                            $scope.openTodoCount = myRes.length;
                        } else {
                            $scope.openTodoCount = $scope.openTodoCount111;
                        }
                    });
                });

                $scope.countArray = [];
                var dd = [];
                Restangular.all('surveyanswers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (onetoone) {
                    for (var i = 0; i < onetoone.length; i++) {

                        $scope.countArray.push(onetoone[i].beneficiaryid);
                    }
                    $scope.onttooneTOtalCount($scope.countArray);

                });
                $scope.onttooneTOtalCount = function (countArray) {
                    // console.log('countArray', countArray);
                    for (var i = 0; i < countArray.length; i++) {
                        if (dd.indexOf(countArray[i]) === -1) {
                            dd.push(countArray[i]);
                        }
                        $scope.onetooneCount = dd.length;
                        //console.log('dd', dd);
                    }

                }
            });

        }


        /*********************** new Changes for box value display ********************/


        /****************** INDEX *************/



        $scope.stakeholdertypes = Restangular.all('stakeholdertypes').getList().$object;
        $scope.events = Restangular.all('events?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][deleteflag]=false').getList().$object;
        $scope.purposeofmeetings = Restangular.all('purposeofmeetings').getList().$object;
        $scope.submittodo = Restangular.all('todos');
        $scope.maintodotypes = Restangular.all('todotypes').getList().$object;
        $scope.searchbulkupdate = '';
        $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');

        /************************ New Changes *********************/

        $scope.roleId = $window.sessionStorage.roleId;

        $scope.analyticsRedirect = function (path) {
            if ($scope.roleId == 2 || $scope.roleId == 20 || $scope.roleId == 11 || $scope.roleId == 18 || $scope.roleId == 13 || $scope.roleId == 12 || $scope.roleId == 19) {
                window.location = path;
            }
        };

        $scope.analyticsRedirect1 = function (path) {
            if ($scope.roleId == 2 || $scope.roleId == 20 || $scope.roleId == 11 || $scope.roleId == 18 || $scope.roleId == 13 || $scope.roleId == 12 || $scope.roleId == 19) {
                $scope.todoDown();
            }
        };



        $scope.todoDown = function () {
            anchorSmoothScroll.scrollTo('middle');
        };

        if ($window.sessionStorage.todoClick == 'clicked') {
            $scope.todoDown();
        }


        $scope.$on('totaltoDo', function (event, value) {
            // console.log(value);
            anchorSmoothScroll.scrollTo(value);
        });

        /********Type watch*******/

        //$scope.typeId = 'todo';

        $scope.$watch('typeId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {

                if ($window.sessionStorage.roleId != 1) {
                    $scope.main_toggleLoading();
                };
                var typecount = 0;
                $scope.TotalTodos = [];
                $scope.fwlitodost = [];
                $scope.statusDate.name = '';
                $scope.DisableStatus = true;

                $scope.currentPage = 1;

                if (newValue == 'todo') {
                    $scope.TotalTodos = [];
                    $scope.showTodo = true;
                    $scope.showCondition = false;

                    if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {

                        Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                            $scope.partners = part1;
                            angular.forEach($scope.partners, function (member, index) {
                                member.index = index + 1;

                            });
                        });

                        Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[order]=fullname%20ASC').getList().then(function (memberResp) {

                            $scope.memberDispaly = memberResp;


                            Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (myRes) {
                                //+ '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z'
                                $scope.todos = myRes;
                                // + '&filter[order]=datetime%20DESC'
                                // console.log("$scope.todos",$scope.todos);
                                //console.log('myRes', myRes);
                                if ($window.sessionStorage.roleId != 1) {
                                    $scope.Main_modalInstanceLoad.close();
                                };


                                angular.forEach($scope.todos, function (member, index) {
                                    // console.log('member', member.Membername);
                                    $scope.DisableStatus = false;
                                    member.index = index + 1;
                                    member.backgroungclr = '#000000';
                                    //member.todoType = 'Todo';
                                    if (member.questionid == null || member.questionid == '' || member.questionid == 0) {
                                        member.todoType = $scope.todoDis;

                                    } else {
                                        member.todoType = '1-1';

                                    }

                                    for (var m = 0; m < $scope.memberDispaly.length; m++) {

                                        if (member.beneficiaryid == $scope.memberDispaly[m].id) {
                                            member.Membername = $scope.memberDispaly[m];
                                            if ($scope.memberDispaly[m].deleteflag == true) {
                                                member.backgroungclr = '#D1160A';
                                            } else {
                                                member.backgroungclr = '#000000';
                                            }
                                            if ($scope.memberDispaly[m].transferred_flag == true) {
                                                member.transferred = true;
                                            } else {
                                                member.transferred = false;
                                            }
                                            break;
                                        }
                                    }
                                    for (var n = 0; n < $scope.employees.length; n++) {
                                        if (member.facility == $scope.employees[n].id) {
                                            member.COMember = $scope.employees[n];
                                            break;
                                        }
                                    }
//                                    for (var o = 0; o < $scope.maintodotypes.length; o++) {
//                                        if (member.todotype == $scope.maintodotypes[o].id) {
//                                            member.ActionType = $scope.maintodotypes[o];
//                                            break;
//                                        }
//                                    }

                                    for (var p = 0; p < $scope.printdocuments.length; p++) {
                                        if (member.documentid == $scope.printdocuments[p].id) {
                                            member.DocumentName = $scope.printdocuments[p];
                                            break;
                                        }
                                    }
                                    if ($window.sessionStorage.language == 1) {
                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                            if (member.status == $scope.todostatuses[z].id) {
                                                member.StatusName = $scope.todostatuses[z];
                                                break;
                                            }
                                        }

                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                            if (member.todotype == $scope.maintodotypes[o].id) {
                                                member.ActionType = $scope.maintodotypes[o];
                                                break;
                                            }
                                        }

                                    } else {
                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                            if (member.status == $scope.todostatuses[z].parentId) {
                                                member.StatusName = $scope.todostatuses[z];
                                                break;
                                            }
                                        }

                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                            if (member.todotype == $scope.maintodotypes[o].parentId) {
                                                member.ActionType = $scope.maintodotypes[o];
                                                break;
                                            }
                                        }

                                    }


                                    for (var p = 0; p < $scope.printschemes.length; p++) {
                                        if (member.documentid == $scope.printschemes[p].id) {
                                            member.SchemeName = $scope.printschemes[p];
                                            break;
                                        }
                                    }
                                    member.datetime = member.datetime;
                                    //member.purpose = $scope.getTodopurpose(member.purpose);
                                    $scope.TotalTodos.push(member);
                                    // console.log("$scope.TotalTodos", $scope.TotalTodos);
                                    // $scope.fwlitodost.push(member);
                                    // console.log("$scope.fwlitodost fw list1111"  ,$scope.fwlitodost);
                                });

                            });

                            Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=13' + '&filter[order]=datetime%20DESC' + '&filter[where][questionid][gt]=0' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (myRes) {
                                // + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z' 
                                // '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId +
                                $scope.todos = myRes;
                                // console.log('myRes11', myRes);

                                angular.forEach($scope.todos, function (member, index) {
                                    // console.log('member', member);
                                    $scope.DisableStatus = false;
                                    member.index = index + 1;
                                    member.backgroungclr = '#000000';
                                    // member.todoType = 'Todo';

                                    if (member.questionid == null || member.questionid == '' || member.questionid == 0) {
                                        member.todoType = $scope.todoDis;
                                    } else {
                                        member.todoType = '1-1';
                                    }
                                    for (var m = 0; m < $scope.memberDispaly.length; m++) {

                                        if (member.beneficiaryid == $scope.memberDispaly[m].id) {
                                            member.Membername = $scope.memberDispaly[m];
                                            if ($scope.memberDispaly[m].deleteflag == true) {
                                                member.backgroungclr = '#D1160A';
                                            } else {
                                                member.backgroungclr = '#000000';
                                            }
                                            if ($scope.memberDispaly[m].transferred_flag == true) {
                                                member.transferred = true;
                                            } else {
                                                member.transferred = false;
                                            }
                                            break;
                                        }
                                    }
                                    for (var n = 0; n < $scope.employees.length; n++) {
                                        if (member.facility == $scope.employees[n].id) {
                                            member.COMember = $scope.employees[n];
                                            break;
                                        }
                                    }
//                                    for (var o = 0; o < $scope.maintodotypes.length; o++) {
//                                        if (member.todotype == $scope.maintodotypes[o].id) {
//                                            member.ActionType = $scope.maintodotypes[o];
//                                            break;
//                                        }
//                                    }
                                    for (var p = 0; p < $scope.printdocuments.length; p++) {
                                        if (member.documentid == $scope.printdocuments[p].id) {
                                            member.DocumentName = $scope.printdocuments[p];
                                            break;
                                        }
                                    }

                                    //                                    for (var z = 0; z < $scope.todostatuses.length; z++) {
                                    //                                        if (member.status == $scope.todostatuses[z].id) {
                                    //                                            member.StatusName = $scope.todostatuses[z];
                                    //                                            break;
                                    //                                        }
                                    //                                    }

                                    if ($window.sessionStorage.language == 1) {
                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                            if (member.status == $scope.todostatuses[z].id) {
                                                member.StatusName = $scope.todostatuses[z];
                                                break;
                                            }
                                        }

                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                            if (member.todotype == $scope.maintodotypes[o].id) {
                                                member.ActionType = $scope.maintodotypes[o];
                                                break;
                                            }
                                        }

                                    } else {
                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                            if (member.status == $scope.todostatuses[z].parentId) {
                                                member.StatusName = $scope.todostatuses[z];
                                                break;
                                            }
                                        }

                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                            if (member.todotype == $scope.maintodotypes[o].parentId) {
                                                member.ActionType = $scope.maintodotypes[o];
                                                break;
                                            }
                                        }

                                    }
                                    for (var p = 0; p < $scope.printschemes.length; p++) {
                                        if (member.documentid == $scope.printschemes[p].id) {
                                            member.SchemeName = $scope.printschemes[p];
                                            break;
                                        }
                                    }
                                    member.datetime = member.datetime;
                                    //member.purpose = $scope.getTodopurpose(member.purpose);
                                    $scope.TotalTodos.push(member);
                                    //console.log("$scope.TotalTodos", $scope.TotalTodos); 
                                    //$scope.fwlitodost = [];
                                    // $scope.fwlitodost.push(member);
                                    // console.log("$scope.fwlitodost fw list222"  ,$scope.fwlitodost);
                                });

                            });


                        });
                    } else {
                        $scope.TotalTodos = [];

                        $scope.filterLast11 = $scope.todayDate + 'T00:00:00.000Z';

                        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                            $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                                $scope.partners = part2;
                                angular.forEach($scope.partners, function (member, index) {
                                    member.index = index + 1;
                                });



                                Restangular.all('todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=datetime%20DESC').getList().then(function (myRes) {

                                    //+ '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z'


                                    // '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId +  '&filter[order]=datetime%20DESC
                                    $scope.todos = myRes;
                                    //console.log('myRes', myRes);
                                    if ($window.sessionStorage.roleId != 1) {
                                        $scope.Main_modalInstanceLoad.close();
                                    };


                                    angular.forEach($scope.todos, function (member, index) {
                                        // console.log('member', member.id);
                                        $scope.DisableStatus = false;
                                        member.index = index + 1;
                                        member.backgroungclr = '#000000';
                                        // member.todoType = 'Todo';

                                        if (member.questionid == null || member.questionid == '' || member.questionid == 0) {
                                            member.todoType = $scope.todoDis;
                                        } else {
                                            member.todoType = '1-1';
                                        }
                                        for (var m = 0; m < $scope.partners.length; m++) {

                                            if (member.beneficiaryid == $scope.partners[m].id) {
                                                member.Membername = $scope.partners[m];
                                                if ($scope.partners[m].deleteflag == true) {
                                                    member.backgroungclr = '#D1160A';
                                                } else {
                                                    member.backgroungclr = '#000000';
                                                }
                                                if ($scope.partners[m].transferred_flag == true) {
                                                    member.transferred = true;
                                                } else {
                                                    member.transferred = false;
                                                }
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.employees.length; n++) {
                                            if (member.facility == $scope.employees[n].id) {
                                                member.COMember = $scope.employees[n];
                                                break;
                                            }
                                        }
//                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
//                                            if (member.todotype == $scope.maintodotypes[o].id) {
//                                                member.ActionType = $scope.maintodotypes[o];
//                                                break;
//                                            }
//                                        }
                                        for (var p = 0; p < $scope.printdocuments.length; p++) {
                                            if (member.documentid == $scope.printdocuments[p].id) {
                                                member.DocumentName = $scope.printdocuments[p];
                                                break;
                                            }
                                        }

                                        //                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                        //                                            if (member.status == $scope.todostatuses[z].id) {
                                        //                                                member.StatusName = $scope.todostatuses[z];
                                        //                                                break;
                                        //                                            }
                                        //                                        }

                                        if ($window.sessionStorage.language == 1) {
                                            for (var z = 0; z < $scope.todostatuses.length; z++) {
                                                if (member.status == $scope.todostatuses[z].id) {
                                                    member.StatusName = $scope.todostatuses[z];
                                                    break;
                                                }
                                            }

                                            for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                                if (member.todotype == $scope.maintodotypes[o].id) {
                                                    member.ActionType = $scope.maintodotypes[o];
                                                    break;
                                                }
                                            }

                                        } else {
                                            for (var z = 0; z < $scope.todostatuses.length; z++) {
                                                if (member.status == $scope.todostatuses[z].parentId) {
                                                    member.StatusName = $scope.todostatuses[z];
                                                    break;
                                                }
                                            }

                                            for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                                if (member.todotype == $scope.maintodotypes[o].parentId) {
                                                    member.ActionType = $scope.maintodotypes[o];
                                                    break;
                                                }
                                            }

                                        }
                                        for (var p = 0; p < $scope.printschemes.length; p++) {
                                            if (member.documentid == $scope.printschemes[p].id) {
                                                member.SchemeName = $scope.printschemes[p];
                                                break;
                                            }
                                        }
                                        member.datetime = member.datetime;
                                        //member.purpose = $scope.getTodopurpose(member.purpose);

                                        $scope.TotalTodos.push(member);

                                        // console.log("$scope.fwtodoslist else part",$scope.fwlitodost);
                                    });

                                });



                                //{"datetime":{"gte":["' + $scope.filterLast11 + '"]}},
                                Restangular.all('todos?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}},{"facility":{"inq":[' + $window.sessionStorage.coorgId + ']}},{"questionid":{"gt":["0"]}}]}}').getList().then(function (myRes111) {
                                    //console.log("myRes111",myRes111);
                                    //&filter[where][questionid][gt]=0



                                    $scope.todos = myRes111;

                                    angular.forEach($scope.todos, function (member, index) {
                                        //console.log('member', member.id);
                                        $scope.DisableStatus = false;
                                        member.index = index + 1;
                                        member.backgroungclr = '#000000';

                                        //    if(member.fieldworkerid == $window.sessionStorage.UserEmployeeId){
                                        //      console.log('memberId', member.id, index);
                                        //     //delete member;
                                        //      member.splice(index);
                                        //                       
                                        //    }
                                        // member.todoType = 'Todo';


                                        if (member.questionid == null || member.questionid == '' || member.questionid == 0) {
                                            member.todoType = $scope.todoDis;
                                        } else {
                                            member.todoType = '1-1';
                                        }
                                        for (var m = 0; m < $scope.partners.length; m++) {

                                            if (member.beneficiaryid == $scope.partners[m].id) {
                                                member.Membername = $scope.partners[m];
                                                if ($scope.partners[m].deleteflag == true) {
                                                    member.backgroungclr = '#D1160A';
                                                } else {
                                                    member.backgroungclr = '#000000';
                                                }
                                                if ($scope.partners[m].transferred_flag == true) {
                                                    member.transferred = true;
                                                } else {
                                                    member.transferred = false;
                                                }
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.employees.length; n++) {
                                            if (member.facility == $scope.employees[n].id) {
                                                member.COMember = $scope.employees[n];
                                                break;
                                            }
                                        }
//                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
//                                            if (member.todotype == $scope.maintodotypes[o].id) {
//                                                member.ActionType = $scope.maintodotypes[o];
//                                                break;
//                                            }
//                                        }
                                        for (var p = 0; p < $scope.printdocuments.length; p++) {
                                            if (member.documentid == $scope.printdocuments[p].id) {
                                                member.DocumentName = $scope.printdocuments[p];
                                                break;
                                            }
                                        }
                                        //                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                        //                                            if (member.status == $scope.todostatuses[z].id) {
                                        //                                                member.StatusName = $scope.todostatuses[z];
                                        //                                                break;
                                        //                                            }
                                        //                                        }

                                        if ($window.sessionStorage.language == 1) {
                                            for (var z = 0; z < $scope.todostatuses.length; z++) {
                                                if (member.status == $scope.todostatuses[z].id) {
                                                    member.StatusName = $scope.todostatuses[z];
                                                    break;
                                                }
                                            }

                                            for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                                if (member.todotype == $scope.maintodotypes[o].id) {
                                                    member.ActionType = $scope.maintodotypes[o];
                                                    break;
                                                }
                                            }

                                        } else {
                                            for (var z = 0; z < $scope.todostatuses.length; z++) {
                                                if (member.status == $scope.todostatuses[z].parentId) {
                                                    member.StatusName = $scope.todostatuses[z];
                                                    break;
                                                }
                                            }

                                            for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                                if (member.todotype == $scope.maintodotypes[o].parentId) {
                                                    member.ActionType = $scope.maintodotypes[o];
                                                    break;
                                                }
                                            }

                                        }
                                        for (var p = 0; p < $scope.printschemes.length; p++) {
                                            if (member.documentid == $scope.printschemes[p].id) {
                                                member.SchemeName = $scope.printschemes[p];
                                                break;
                                            }
                                        }
                                        member.datetime = member.datetime;
                                        //member.purpose = $scope.getTodopurpose(member.purpose);
                                        for (var i = 0; i < $scope.TotalTodos.length; i++) {
                                            if ($scope.TotalTodos[i].id == member.id) {

                                                //console.log('$scope.TotalTodos[i].id', $scope.TotalTodos[i].id)
                                                delete $scope.TotalTodos[i];
                                                break;
                                            } else {
                                                $scope.TotalTodos.push(member);
                                            }
                                        }


                                        //  console.log("$scope.TotalTodos", $scope.TotalTodos);

                                    });
                                    // console.log("$scope.todos 2nd call", $scope.todos);
                                });
                            });
                        });

                    }

                } else if (newValue == 'condition') {

                    $scope.showTodo = false;
                    $scope.showCondition = true;

                    $scope.TotalTodos = [];
                    if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {

                        // $scope.hideEdit = true;

                        $scope.filterCall1 = 'conditionheaders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId;

                        Restangular.all($scope.filterCall1).getList().then(function (cns) {
                            //console.log('cns', cns);
                            $scope.DisableStatus = false;
                            $scope.todos = cns;
                            if ($window.sessionStorage.roleId != 1) {
                                $scope.Main_modalInstanceLoad.close();
                            };

                            angular.forEach($scope.todos, function (value, index) {

                                value.index = index + 1;
                                // value.hideEdit = true;
                                if (value.caseClosed == true) {
                                    value.hideEdit = false;
                                } else {
                                    value.hideEdit = true;
                                }

                                if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                    $scope.hideEdit = true;
                                } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                    $scope.hideEdit = true;
                                }
                                /*else {
                                        $scope.hideEdit = true;
                                    }*/

                                /* if(value.step == 37 && (value.status == 39 ||value.status == 40)){
                                         value.hideEdit = false;
                                    }*/

                                value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                value.todoType = $scope.conDis;

                                var data1 = $scope.partners.filter(function (arr) {
                                    return arr.id == value.memberId
                                })[0];

                                if (data1 != undefined) {
                                    value.membername = data1.fullname;
                                    value.memberTiId = data1.tiId;

                                }


                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.id == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.parentId == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.id == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.id == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.id == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.parentId == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }
                                }

                                $scope.TotalTodos.push(value);

                            });
                        });

                    } else if ($window.sessionStorage.roleId + "" === "13") {
                        //$scope.hideEdit = false;


                        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {


                            Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (cns) {
                                // console.log('cns', cns);
                                $scope.DisableStatus = false;
                                $scope.todos = cns;
                                if ($window.sessionStorage.roleId != 1) {
                                    $scope.Main_modalInstanceLoad.close();
                                };

                                angular.forEach($scope.todos, function (value, index) {

                                    value.index = index + 1;
                                    value.hideEdit = false;

                                    value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');

                                    value.todoType = $scope.conDis;

                                    var data1 = $scope.partners.filter(function (arr) {
                                        return arr.id == value.memberId
                                    })[0];

                                    if (data1 != undefined) {
                                        value.membername = data1.fullname;
                                        value.memberTiId = data1.tiId;
                                    }



                                    if ($window.sessionStorage.language == 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.id == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                            return arr.parentId == value.condition
                                        })[0];

                                        if (data2 != undefined) {
                                            value.conditionname = data2.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.id == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.step
                                        })[0];

                                        if (data3 != undefined) {
                                            value.conditionstepname = data3.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.id == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                            return arr.parentId == value.status
                                        })[0];

                                        if (data4 != undefined) {
                                            value.conditionstatusname = data4.name;
                                        }
                                    }

                                    if ($window.sessionStorage.language == 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.id == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }

                                    } else if ($window.sessionStorage.language != 1) {

                                        var data5 = $scope.followupsdsply.filter(function (arr) {
                                            return arr.parentId == value.followup
                                        })[0];

                                        if (data5 != undefined) {
                                            value.conditionfollowupname = data5.name;
                                        }
                                    }
                                    $scope.TotalTodos.push(value);

                                });
                            });

                        });
                    } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {


                        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                            Restangular.all('beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (part2) {
                                $scope.partners = part2;
                                angular.forEach($scope.partners, function (member, index) {
                                    member.index = index + 1;

                                });


                                Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (cns) {
                                    // console.log('cns', cns);
                                    $scope.DisableStatus = false;
                                    $scope.todos = cns;
                                    if ($window.sessionStorage.roleId != 1) {
                                        $scope.Main_modalInstanceLoad.close();
                                    };

                                    angular.forEach($scope.todos, function (value, index) {

                                        value.index = index + 1;
                                        //  value.hideEdit = true;

                                        if (value.caseClosed == true) {
                                            value.hideEdit = false;
                                        } else if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                            value.hideEdit = true;
                                        } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                            value.hideEdit = true;
                                        } else {
                                            value.hideEdit = true;
                                        }


                                        /* if(value.step == 37 && (value.status == 39 ||value.status == 40)){
                                              value.hideEdit = false;
                                         }*/

                                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                        value.todoType = $scope.conDis;



                                        var data1 = $scope.partners.filter(function (arr) {
                                            return arr.id == value.memberId
                                        })[0];

                                        if (data1 != undefined) {
                                            value.membername = data1.fullname;
                                            value.memberTiId = data1.tiId;
                                        }


                                        if ($window.sessionStorage.language == 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.id == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.parentId == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.id == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.id == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.id == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.parentId == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }
                                        }

                                        $scope.TotalTodos.push(value);
                                    });
                                });
                            });

                        });
                    } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                        // $scope.hideEdit = true;


                        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                            Restangular.all('beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                                $scope.partners = part2;


                                Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (cns) {
                                    // console.log('cns', cns);
                                    $scope.DisableStatus = false;
                                    $scope.todos = cns;
                                    if ($window.sessionStorage.roleId != 1) {
                                        $scope.Main_modalInstanceLoad.close();
                                    };

                                    angular.forEach($scope.todos, function (value, index) {

                                        value.index = index + 1;

                                        if (value.caseClosed == true) {
                                            value.hideEdit = false;
                                        } else {
                                            value.hideEdit = true;
                                        }
                                        /*value.hideEdit = true;
                                
                                     if(value.step == 37 && (value.status == 39 ||value.status == 40)){
                                         value.hideEdit = false;
                                    }*/

                                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                        value.todoType = $scope.conDis;

                                        var data1 = $scope.partners.filter(function (arr) {
                                            return arr.id == value.memberId
                                        })[0];

                                        if (data1 != undefined) {
                                            value.membername = data1.fullname;
                                            value.memberTiId = data1.tiId;
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.id == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.parentId == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.id == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.id == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.id == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.parentId == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }
                                        }

                                        $scope.TotalTodos.push(value);
                                    });
                                });
                            });

                        });
                    }
                }

            }
            $scope.TodoValue = newValue;
        });




        /************************ New Changes *********************/




        $scope.$watch('statusDate.name', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {


                if ($scope.TodoValue == 'todo') {
                    console.log('todo');

                    if ($window.sessionStorage.roleId != 1) {
                        $scope.main_toggleLoading();
                    };
                    $scope.showTodo = true;
                    $scope.showCondition = false;

                    $scope.TotalTodos = [];

                    if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {
                        console.log('i m in fild worker filter status 1111')

                        Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[order]=fullname%20ASC').getList().then(function (memberResp) {

                            $scope.memberDispaly = memberResp;

                            Restangular.all('todos?filter[where][status]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[order]=datetime%20DESC').getList().then(function (myRes) {

                                //+ '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z'
                                //&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId
                                $scope.todos = myRes;
                                //console.log('myRes', myRes);
                                if ($window.sessionStorage.roleId != 1) {
                                    $scope.Main_modalInstanceLoad.close();
                                };


                                angular.forEach($scope.todos, function (member, index) {
                                    // console.log('member', member);
                                    member.index = index + 1;
                                    member.backgroungclr = '#000000';
                                    //member.todoType = 'Todo';
                                    if (member.questionid == null || member.questionid == '') {
                                        member.todoType = $scope.todoDis;
                                    } else {
                                        member.todoType = '1-1';
                                    }

                                    for (var m = 0; m < $scope.memberDispaly.length; m++) {
                                        if (member.beneficiaryid == $scope.memberDispaly[m].id) {
                                            member.Membername = $scope.memberDispaly[m];
                                            if ($scope.memberDispaly[m].deleteflag == true) {
                                                member.backgroungclr = '#D1160A';
                                            } else {
                                                member.backgroungclr = '#000000';
                                            }
                                            if ($scope.memberDispaly[m].transferred_flag == true) {
                                                member.transferred = true;
                                            } else {
                                                member.transferred = false;
                                            }
                                            break;
                                        }
                                    }
                                    for (var n = 0; n < $scope.employees.length; n++) {
                                        if (member.facility == $scope.employees[n].id) {
                                            member.COMember = $scope.employees[n];
                                            break;
                                        }
                                    }
                                    for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                        if (member.todotype == $scope.maintodotypes[o].id) {
                                            member.ActionType = $scope.maintodotypes[o];
                                            break;
                                        }
                                    }
                                    for (var p = 0; p < $scope.printdocuments.length; p++) {
                                        if (member.documentid == $scope.printdocuments[p].id) {
                                            member.DocumentName = $scope.printdocuments[p];
                                            break;
                                        }
                                    }

                                    for (var z = 0; z < $scope.todostatuses.length; z++) {
                                        if (member.status == $scope.todostatuses[z].id) {
                                            member.StatusName = $scope.todostatuses[z];
                                            break;
                                        }
                                    }
                                    for (var p = 0; p < $scope.printschemes.length; p++) {
                                        if (member.documentid == $scope.printschemes[p].id) {
                                            member.SchemeName = $scope.printschemes[p];
                                            break;
                                        }
                                    }
                                    member.datetime = member.datetime;
                                    //member.purpose = $scope.getTodopurpose(member.purpose);
                                    $scope.TotalTodos.push(member);

                                });
                                // console.log('$scope.TotalTodos', $scope.TotalTodos);
                            });

                            Restangular.all('todos?filter[where][status]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][roleId]=13' + '&filter[order]=datetime%20DESC' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][questionid][gte]=0').getList().then(function (myRes) {

                                //+ '&filter[where][datetime][gt]=' + $scope.todayDate + 'T00:00:00.000Z' + 
                                // '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId +
                                $scope.todos = myRes;
                                // console.log('myRes11', myRes);

                                angular.forEach($scope.todos, function (member, index) {
                                    // console.log('member', member);
                                    $scope.DisableStatus = false;
                                    member.index = index + 1;
                                    member.backgroungclr = '#000000';
                                    // member.todoType = 'Todo';

                                    if (member.questionid == null || member.questionid == '' || member.questionid == 0) {
                                        member.todoType = $scope.todoDis;
                                    } else {
                                        member.todoType = '1-1';
                                    }
                                    for (var m = 0; m < $scope.memberDispaly.length; m++) {

                                        if (member.beneficiaryid == $scope.memberDispaly[m].id) {
                                            member.Membername = $scope.memberDispaly[m];
                                            if ($scope.memberDispaly[m].deleteflag == true) {
                                                member.backgroungclr = '#D1160A';
                                            } else {
                                                member.backgroungclr = '#000000';
                                            }
                                            if ($scope.memberDispaly[m].transferred_flag == true) {
                                                member.transferred = true;
                                            } else {
                                                member.transferred = false;
                                            }
                                            break;
                                        }
                                    }
                                    for (var n = 0; n < $scope.employees.length; n++) {
                                        if (member.facility == $scope.employees[n].id) {
                                            member.COMember = $scope.employees[n];
                                            break;
                                        }
                                    }
                                    for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                        if (member.todotype == $scope.maintodotypes[o].id) {
                                            member.ActionType = $scope.maintodotypes[o];
                                            break;
                                        }
                                    }
                                    for (var p = 0; p < $scope.printdocuments.length; p++) {
                                        if (member.documentid == $scope.printdocuments[p].id) {
                                            member.DocumentName = $scope.printdocuments[p];
                                            break;
                                        }
                                    }

                                    for (var z = 0; z < $scope.todostatuses.length; z++) {
                                        if (member.status == $scope.todostatuses[z].id) {
                                            member.StatusName = $scope.todostatuses[z];
                                            break;
                                        }
                                    }
                                    for (var p = 0; p < $scope.printschemes.length; p++) {
                                        if (member.documentid == $scope.printschemes[p].id) {
                                            member.SchemeName = $scope.printschemes[p];
                                            break;
                                        }
                                    }
                                    member.datetime = member.datetime;
                                    //member.purpose = $scope.getTodopurpose(member.purpose);
                                    $scope.TotalTodos.push(member);
                                    //console.log("$scope.TotalTodos", $scope.TotalTodos); 
                                    //$scope.fwlitodost = [];
                                    // $scope.fwlitodost.push(member);
                                    // console.log("$scope.fwlitodost fw list222"  ,$scope.fwlitodost);
                                });

                            });


                        });
                    } else {

                        //console.log('i m in fild worker filter status')
                        $scope.TotalTodos = [];

                        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                            Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[order]=fullname%20ASC').getList().then(function (memberResp) {

                                $scope.memberDispaly = memberResp;

                                Restangular.all('todos?filter[where][status]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[order]=datetime%20DESC').getList().then(function (myRes) {

                                    //+ '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z'
                                    //&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId
                                    $scope.todos = myRes;
                                    //console.log('myRes', myRes);
                                    if ($window.sessionStorage.roleId != 1) {
                                        $scope.Main_modalInstanceLoad.close();
                                    };


                                    angular.forEach($scope.todos, function (member, index) {
                                        // console.log('member', member);
                                        member.index = index + 1;
                                        member.backgroungclr = '#000000';
                                        //member.todoType = 'Todo';
                                        if (member.questionid == null || member.questionid == '') {
                                            member.todoType = $scope.todoDis;
                                        } else {
                                            member.todoType = '1-1';
                                        }

                                        for (var m = 0; m < $scope.memberDispaly.length; m++) {
                                            if (member.beneficiaryid == $scope.memberDispaly[m].id) {
                                                member.Membername = $scope.memberDispaly[m];
                                                if ($scope.memberDispaly[m].deleteflag == true) {
                                                    member.backgroungclr = '#D1160A';
                                                } else {
                                                    member.backgroungclr = '#000000';
                                                }
                                                if ($scope.memberDispaly[m].transferred_flag == true) {
                                                    member.transferred = true;
                                                } else {
                                                    member.transferred = false;
                                                }
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.employees.length; n++) {
                                            if (member.facility == $scope.employees[n].id) {
                                                member.COMember = $scope.employees[n];
                                                break;
                                            }
                                        }
                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                            if (member.todotype == $scope.maintodotypes[o].id) {
                                                member.ActionType = $scope.maintodotypes[o];
                                                break;
                                            }
                                        }
                                        for (var p = 0; p < $scope.printdocuments.length; p++) {
                                            if (member.documentid == $scope.printdocuments[p].id) {
                                                member.DocumentName = $scope.printdocuments[p];
                                                break;
                                            }
                                        }

                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                            if (member.status == $scope.todostatuses[z].id) {
                                                member.StatusName = $scope.todostatuses[z];
                                                break;
                                            }
                                        }
                                        for (var p = 0; p < $scope.printschemes.length; p++) {
                                            if (member.documentid == $scope.printschemes[p].id) {
                                                member.SchemeName = $scope.printschemes[p];
                                                break;
                                            }
                                        }
                                        member.datetime = member.datetime;
                                        //member.purpose = $scope.getTodopurpose(member.purpose);
                                        $scope.TotalTodos.push(member);

                                    });
                                    // console.log('$scope.TotalTodos', $scope.TotalTodos);
                                });

                                // Restangular.all('todos?filter[where][status]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][roleId]=13' + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gt]=' + $scope.todayDate + 'T00:00:00.000Z' + '&filter[where][questionid][gte]=0'+ '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (myRes) {

                                Restangular.all('todos?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"status":{"inq":[' + newValue + ']}},{"deleteflag":{"inq":[false]}},{"datetime":{"gte":["' + $scope.filterLast11 + '"]}},{"facility":{"inq":[' + $window.sessionStorage.coorgId + ']}},{"questionid":{"gt":["0"]}}]}}').getList().then(function (myRes) {


                                    $scope.todos = myRes;
                                    // console.log('myRes11', myRes);

                                    angular.forEach($scope.todos, function (member, index) {
                                        // console.log('member', member);
                                        $scope.DisableStatus = false;
                                        member.index = index + 1;
                                        member.backgroungclr = '#000000';
                                        // member.todoType = 'Todo';

                                        if (member.questionid == null || member.questionid == '' || member.questionid == 0) {
                                            member.todoType = $scope.todoDis;
                                        } else {
                                            member.todoType = '1-1';
                                        }
                                        for (var m = 0; m < $scope.memberDispaly.length; m++) {

                                            if (member.beneficiaryid == $scope.memberDispaly[m].id) {
                                                member.Membername = $scope.memberDispaly[m];
                                                if ($scope.memberDispaly[m].deleteflag == true) {
                                                    member.backgroungclr = '#D1160A';
                                                } else {
                                                    member.backgroungclr = '#000000';
                                                }
                                                if ($scope.memberDispaly[m].transferred_flag == true) {
                                                    member.transferred = true;
                                                } else {
                                                    member.transferred = false;
                                                }
                                                break;
                                            }
                                        }
                                        for (var n = 0; n < $scope.employees.length; n++) {
                                            if (member.facility == $scope.employees[n].id) {
                                                member.COMember = $scope.employees[n];
                                                break;
                                            }
                                        }
                                        for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                            if (member.todotype == $scope.maintodotypes[o].id) {
                                                member.ActionType = $scope.maintodotypes[o];
                                                break;
                                            }
                                        }
                                        for (var p = 0; p < $scope.printdocuments.length; p++) {
                                            if (member.documentid == $scope.printdocuments[p].id) {
                                                member.DocumentName = $scope.printdocuments[p];
                                                break;
                                            }
                                        }

                                        for (var z = 0; z < $scope.todostatuses.length; z++) {
                                            if (member.status == $scope.todostatuses[z].id) {
                                                member.StatusName = $scope.todostatuses[z];
                                                break;
                                            }
                                        }
                                        for (var p = 0; p < $scope.printschemes.length; p++) {
                                            if (member.documentid == $scope.printschemes[p].id) {
                                                member.SchemeName = $scope.printschemes[p];
                                                break;
                                            }
                                        }
                                        member.datetime = member.datetime;

                                        for (var i = 0; i < $scope.TotalTodos.length; i++) {
                                            if ($scope.TotalTodos[i].id == member.id) {

                                                // console.log('$scope.TotalTodos[i].id', $scope.TotalTodos[i].id)
                                                delete $scope.TotalTodos[i];
                                                break;
                                            } else {
                                                $scope.TotalTodos.push(member);
                                            }
                                        }
                                        // $scope.TotalTodos.push(member);
                                        //console.log("$scope.TotalTodos", $scope.TotalTodos); 
                                        //$scope.fwlitodost = [];
                                        // $scope.fwlitodost.push(member);
                                        // console.log("$scope.fwlitodost fw list222"  ,$scope.fwlitodost);
                                    });

                                });
                            });


                        });

                    }
                } else if ($scope.TodoValue == 'condition') {
                    console.log('i m in condition ')
                    $scope.showTodo = false;
                    $scope.showCondition = true;

                    $scope.TotalTodos = [];
                    if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {

                        // $scope.hideEdit = true;
                        if (newValue == '8') {

                            $scope.filterCall1 = 'conditionheaders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][caseClosed]=false';

                        } else {
                            $scope.filterCall1 = 'conditionheaders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][caseClosed]=true';
                        }

                        Restangular.all($scope.filterCall1).getList().then(function (cns) {
                            //console.log('cns', cns);
                            $scope.DisableStatus = false;
                            $scope.todos = cns;
                            if ($window.sessionStorage.roleId != 1) {
                                $scope.Main_modalInstanceLoad.close();
                            };

                            angular.forEach($scope.todos, function (value, index) {

                                value.index = index + 1;
                                // value.hideEdit = true;

                                if (value.caseClosed == true) {
                                    value.hideEdit = false;
                                } else {
                                    value.hideEdit = true;
                                }

                                if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                    $scope.hideEdit = false;
                                } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                    $scope.hideEdit = false;
                                }

                                /*if(value.step == 37 && (value.status == 39 ||value.status == 40)){
                                        value.hideEdit = false;
                                   }*/

                                value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                value.todoType = $scope.conDis;

                                var data1 = $scope.partners.filter(function (arr) {
                                    return arr.id == value.memberId
                                })[0];

                                if (data1 != undefined) {
                                    value.membername = data1.fullname;
                                    value.memberTiId = data1.tiId;

                                }


                                if ($window.sessionStorage.language == 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.id == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                        return arr.parentId == value.condition
                                    })[0];

                                    if (data2 != undefined) {
                                        value.conditionname = data2.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.id == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.step
                                    })[0];

                                    if (data3 != undefined) {
                                        value.conditionstepname = data3.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.id == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                        return arr.parentId == value.status
                                    })[0];

                                    if (data4 != undefined) {
                                        value.conditionstatusname = data4.name;
                                    }
                                }

                                if ($window.sessionStorage.language == 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.id == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }

                                } else if ($window.sessionStorage.language != 1) {

                                    var data5 = $scope.followupsdsply.filter(function (arr) {
                                        return arr.parentId == value.followup
                                    })[0];

                                    if (data5 != undefined) {
                                        value.conditionfollowupname = data5.name;
                                    }
                                }

                                $scope.TotalTodos.push(value);
                            });
                        });

                    } else if ($window.sessionStorage.roleId + "" === "13") {
                        //$scope.hideEdit = false;
                        if (newValue == '8') {

                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {


                                Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (cns) {
                                    // console.log('cns', cns);

                                    $scope.todos = cns;

                                    $scope.DisableStatus = false;
                                    if ($window.sessionStorage.roleId != 1) {
                                        $scope.Main_modalInstanceLoad.close();
                                    };

                                    angular.forEach($scope.todos, function (value, index) {

                                        value.index = index + 1;
                                        value.hideEdit = false;

                                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');

                                        value.todoType = $scope.conDis;

                                        var data1 = $scope.partners.filter(function (arr) {
                                            return arr.id == value.memberId
                                        })[0];

                                        if (data1 != undefined) {
                                            value.membername = data1.fullname;
                                            value.memberTiId = data1.tiId;
                                        }



                                        if ($window.sessionStorage.language == 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.id == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.parentId == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.id == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.id == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.id == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.parentId == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }
                                        }
                                        $scope.TotalTodos.push(value);
                                    });
                                });

                            });
                        } else {


                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {


                                Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[true]}}]}}').getList().then(function (cns) {
                                    // console.log('cns', cns);

                                    $scope.todos = cns;

                                    $scope.DisableStatus = false;
                                    if ($window.sessionStorage.roleId != 1) {
                                        $scope.Main_modalInstanceLoad.close();
                                    };

                                    angular.forEach($scope.todos, function (value, index) {

                                        value.index = index + 1;
                                        value.hideEdit = false;

                                        value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');

                                        value.todoType = $scope.conDis;

                                        var data1 = $scope.partners.filter(function (arr) {
                                            return arr.id == value.memberId
                                        })[0];

                                        if (data1 != undefined) {
                                            value.membername = data1.fullname;
                                            value.memberTiId = data1.tiId;
                                        }



                                        if ($window.sessionStorage.language == 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.id == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                return arr.parentId == value.condition
                                            })[0];

                                            if (data2 != undefined) {
                                                value.conditionname = data2.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.id == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.step
                                            })[0];

                                            if (data3 != undefined) {
                                                value.conditionstepname = data3.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.id == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                return arr.parentId == value.status
                                            })[0];

                                            if (data4 != undefined) {
                                                value.conditionstatusname = data4.name;
                                            }
                                        }

                                        if ($window.sessionStorage.language == 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.id == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }

                                        } else if ($window.sessionStorage.language != 1) {

                                            var data5 = $scope.followupsdsply.filter(function (arr) {
                                                return arr.parentId == value.followup
                                            })[0];

                                            if (data5 != undefined) {
                                                value.conditionfollowupname = data5.name;
                                            }
                                        }
                                        $scope.TotalTodos.push(value);
                                    });
                                });

                            });
                        }
                    } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18") {

                        if (newValue == '8') {
                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                Restangular.all('beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (part2) {
                                    $scope.partners = part2;
                                    angular.forEach($scope.partners, function (member, index) {
                                        member.index = index + 1;

                                    });


                                    Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (cns) {
                                        // console.log('cns', cns);
                                        $scope.DisableStatus = false;
                                        $scope.todos = cns;
                                        if ($window.sessionStorage.roleId != 1) {
                                            $scope.Main_modalInstanceLoad.close();
                                        };

                                        angular.forEach($scope.todos, function (value, index) {

                                            value.index = index + 1;
                                            //  value.hideEdit = true;

                                            if (value.caseClosed == true) {
                                                value.hideEdit = false;
                                            } else if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                                value.hideEdit = false;
                                            } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                                value.hideEdit = false;
                                            } else {
                                                value.hideEdit = true;
                                            }


                                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                            value.todoType = $scope.conDis;



                                            var data1 = $scope.partners.filter(function (arr) {
                                                return arr.id == value.memberId
                                            })[0];

                                            if (data1 != undefined) {
                                                value.membername = data1.fullname;
                                                value.memberTiId = data1.tiId;
                                            }


                                            if ($window.sessionStorage.language == 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.id == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.id == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.id == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.id == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.parentId == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }
                                            }

                                            $scope.TotalTodos.push(value);
                                        });
                                    });
                                });

                            });
                        } else {
                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                Restangular.all('beneficiaries?filter={"where":{"and":[{"ictcid":{"inq":[' + fw.ictcId + ']}}]}}').getList().then(function (part2) {
                                    $scope.partners = part2;
                                    angular.forEach($scope.partners, function (member, index) {
                                        member.index = index + 1;

                                    });


                                    Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[true]}}]}}').getList().then(function (cns) {
                                        // console.log('cns', cns);
                                        $scope.DisableStatus = false;
                                        $scope.todos = cns;
                                        if ($window.sessionStorage.roleId != 1) {
                                            $scope.Main_modalInstanceLoad.close();
                                        };

                                        angular.forEach($scope.todos, function (value, index) {

                                            value.index = index + 1;
                                            //  value.hideEdit = true;
                                            if (value.caseClosed == true) {
                                                value.hideEdit = false;
                                            } else if (value.condition == 51 && value.step == 34 && value.status == 36) {
                                                value.hideEdit = false;
                                            } else if (value.condition == 51 && (value.step == 35 || value.step == 36 || value.step == 37)) {
                                                value.hideEdit = false;
                                            } else {
                                                value.hideEdit = true;
                                            }


                                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                            value.todoType = $scope.conDis;



                                            var data1 = $scope.partners.filter(function (arr) {
                                                return arr.id == value.memberId
                                            })[0];

                                            if (data1 != undefined) {
                                                value.membername = data1.fullname;
                                                value.memberTiId = data1.tiId;
                                            }


                                            if ($window.sessionStorage.language == 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.id == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.id == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.id == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.id == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.parentId == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }
                                            }

                                            $scope.TotalTodos.push(value);
                                        });
                                    });
                                });

                            });
                        }
                    } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
                        // $scope.hideEdit = true;
                        if (newValue == '8') {

                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                                Restangular.all('beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (part2) {
                                    $scope.partners = part2;
                                    angular.forEach($scope.partners, function (member, index) {
                                        member.index = index + 1;

                                    });


                                    Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (cns) {
                                        console.log('cns', cns);
                                        $scope.DisableStatus = false;
                                        $scope.todos = cns;
                                        if ($window.sessionStorage.roleId != 1) {
                                            $scope.Main_modalInstanceLoad.close();
                                        };

                                        angular.forEach($scope.todos, function (value, index) {

                                            value.index = index + 1;
                                            // value.hideEdit = true;
                                            if (value.caseClosed == true) {
                                                value.hideEdit = false;
                                            } else {
                                                value.hideEdit = true;
                                            }

                                            if (value.step == 37 && (value.status == 39 || value.status == 40)) {
                                                value.hideEdit = false;
                                            }

                                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                            value.todoType = $scope.conDis;

                                            var data1 = $scope.partners.filter(function (arr) {
                                                return arr.id == value.memberId
                                            })[0];

                                            if (data1 != undefined) {
                                                value.membername = data1.fullname;
                                                value.memberTiId = data1.tiId;
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.id == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.id == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.id == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.id == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.parentId == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }
                                            }

                                            $scope.TotalTodos.push(value);
                                        });
                                    });
                                });

                            });
                        } else {

                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {

                                Restangular.all('beneficiaries?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                                    $scope.partners = part2;
                                    angular.forEach($scope.partners, function (member, index) {
                                        member.index = index + 1;

                                    });


                                    Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"deleteflag":{"inq":[false]}},{"caseClosed":{"inq":[true]}}]}}').getList().then(function (cns) {
                                        // console.log('cns', cns);
                                        $scope.DisableStatus = false;
                                        $scope.todos = cns;
                                        if ($window.sessionStorage.roleId != 1) {
                                            $scope.Main_modalInstanceLoad.close();
                                        };

                                        angular.forEach($scope.todos, function (value, index) {

                                            value.index = index + 1;
                                            // value.hideEdit = true;

                                            if (value.caseClosed == true) {
                                                value.hideEdit = false;
                                            } else {
                                                value.hideEdit = true;
                                            }

                                            if (value.step == 37 && (value.status == 39 || value.status == 40)) {
                                                value.hideEdit = false;
                                            }

                                            value.followupdateDsply = $filter('date')(value.followupdate, 'dd-MMM-yyyy');
                                            value.todoType = $scope.conDis;

                                            var data1 = $scope.partners.filter(function (arr) {
                                                return arr.id == value.memberId
                                            })[0];

                                            if (data1 != undefined) {
                                                value.membername = data1.fullname;
                                                value.memberTiId = data1.tiId;
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.id == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data2 = $scope.conditiondsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.condition
                                                })[0];

                                                if (data2 != undefined) {
                                                    value.conditionname = data2.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.id == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data3 = $scope.stepsdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.step
                                                })[0];

                                                if (data3 != undefined) {
                                                    value.conditionstepname = data3.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.id == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data4 = $scope.statusesdsplyNew.filter(function (arr) {
                                                    return arr.parentId == value.status
                                                })[0];

                                                if (data4 != undefined) {
                                                    value.conditionstatusname = data4.name;
                                                }
                                            }

                                            if ($window.sessionStorage.language == 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.id == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }

                                            } else if ($window.sessionStorage.language != 1) {

                                                var data5 = $scope.followupsdsply.filter(function (arr) {
                                                    return arr.parentId == value.followup
                                                })[0];

                                                if (data5 != undefined) {
                                                    value.conditionfollowupname = data5.name;
                                                }
                                            }

                                            $scope.TotalTodos.push(value);
                                        });
                                    });
                                });

                            });
                        }
                    }
                }
            }
        });
        /********************/
        $scope.currentPage = 1;
        $scope.pageSize = 20;
        $scope.todos = [];
        $scope.FWorktodos = [];

        //$scope.loading2 = true;
        $scope.ClearFilters = function () {
            console.log('ClearFilters');
            //$scope.loading2 = true;
            //$scope.toggleLoading();
            $scope.main_toggleLoading();
            $scope.pillarId = "";
            $scope.statusDate = "";
            $scope.emps = Restangular.all('employees').getList().then(function (emps) {
                $scope.employees = emps;
                $scope.todotyp = Restangular.all('todotypes').getList().then(function (todotyp) {
                    $scope.maintodotypes = todotyp;

                    $scope.zn = Restangular.all('todos?filter[where][status][inq]=1&filter[where][status][inq]=2&filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (myRes) {
                        $scope.todos = myRes;
                        $scope.Main_modalInstanceLoad.close();
                        //$scope.loading2 = false;
                        angular.forEach($scope.todos, function (member, index) {
                            member.index = index + 1;
                            member.backgroungclr = '#000000';
                            for (var m = 0; m < $scope.partners.length; m++) {
                                if (member.beneficiaryid == $scope.partners[m].id) {
                                    member.Membername = $scope.partners[m];
                                    if ($scope.partners[m].deleteflag == true) {
                                        member.backgroungclr = '#D1160A';
                                    } else {
                                        member.backgroungclr = '#000000';
                                    }
                                    if ($scope.partners[m].transferred_flag == true) {
                                        member.transferred = true;
                                    } else {
                                        member.transferred = false;
                                    }
                                    break;
                                }
                            }
                            for (var n = 0; n < $scope.employees.length; n++) {
                                if (member.facility == $scope.employees[n].id) {
                                    member.COMember = $scope.employees[n];
                                    break;
                                }
                            }
                            for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                if (member.todotype == $scope.maintodotypes[o].id) {
                                    member.ActionType = $scope.maintodotypes[o];
                                    break;
                                }
                            }
                            for (var p = 0; p < $scope.printdocuments.length; p++) {
                                if (member.documentid == $scope.printdocuments[p].id) {
                                    member.DocumentName = $scope.printdocuments[p];
                                    break;
                                }
                            }
                            for (var p = 0; p < $scope.printschemes.length; p++) {
                                if (member.documentid == $scope.printschemes[p].id) {
                                    member.SchemeName = $scope.printschemes[p];
                                    break;
                                }
                            }
                            member.datetime = member.datetime;
                            //member.purpose = $scope.getTodopurpose(member.purpose);
                            $scope.TotalTodos.push(member);
                            /*  if (index == 0) {
						      console.log('$scope.TotalTodos', $scope.TotalTodos);
						  }*/
                        });

                    });
                });
            });
        };
        $scope.getSite = function (site) {
            return Restangular.one('distribution-routes', site).get().$object;
        };
        $scope.getCoMember = function (site) {
            return Restangular.one('employees', site).get().$object;
        };
        $scope.getMemberName = function (beneficiaryid) {
            return Restangular.one('beneficiaries', beneficiaryid).get().$object;
        };
        $scope.getActionType = function (followuprequired) {
            return Restangular.one('todotypes', followuprequired).get().$object;
        };
        $scope.showfollowupModal = false;
        $scope.toggleschemesModal = function (id) {
            Restangular.one('todos/' + id).get().then(function (event) {
                ////console.log('event', event);
                $scope.original = event;
                $scope.todo = Restangular.copy($scope.original);
                $scope.todo.datetime = event.datetime;
                $scope.todo.purpose = event.purpose.split(",");
            });
            $scope.SavetodoFollow = function () {
                $scope.submittodo.customPUT($scope.todo, $scope.todo.id).then(function (response) {
                    // //console.log('Save ToDo', response);
                    $route.reload();
                });
                $scope.showfollowupModal = !$scope.showfollowupModal;
            }
            $scope.showfollowupModal = !$scope.showfollowupModal;
        };
        $scope.CancelFollow = function () {
            $scope.showfollowupModal = !$scope.showfollowupModal;
        };
        /*********************** Date Picker Start *******************/
        $scope.followupdt = $filter('date')(new Date(), 'dd-MMMM-yyyy');
        var newdate = new Date();
        $scope.today = function () {
            $scope.dtmax = new Date();

        };
        $scope.today();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.toggleMin();
        $scope.follow = {};
        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            // $scope.opened = true;
            $scope.follow.followupopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end 
        $scope.statusDate = {};
        $scope.FWorkstatusDate = {};
        // $scope.pillars = Restangular.one('pillars?filter[order]=order%20ASC').getList().$object;

        Restangular.one('todostatuses?filter[where][deleteflag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (responseDue) {
            //console.log('responseDue', responseDue);
            $scope.todostatuseDisply = responseDue;
        });
        $scope.openOnetoOne = function () {
            console.log('openOnetoOne');
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/OnetoOneMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.okOnetoOne = function (fullname) {
            $scope.modalInstance1.close();
            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            //console.log('$rootScope.okOnetoOne', $rootScope.fullname);
            // window.location = "/onetoone/" + partnerid;
            $location.path("/onetoone/" + fullname);
        };
        $scope.cancelOnetoOne = function () {
            $scope.modalInstance1.close();
        };
        $scope.openReportIncident = function () {
            console.log('openReportIncident');
            $scope.modalInstanceReport = $modal.open({
                animation: true,
                templateUrl: 'template/ReportIncidentMain.html',
                scope: $scope,
                backdrop: 'static'
            });
            //$scope.modalInstanceLoad.close();
        };

        $scope.okApplyforScheme = function (fullname) {
            $scope.modalInstance1.close();
            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyforschemes");
            $route.reload();
        };
        $scope.cancelApplyforScheme = function () {
            $scope.modalInstance1.close();
        };
        $scope.openApplyforDocument = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/ApplyforDocument.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.okApplyforDocument = function (fullname) {
            $scope.modalInstance1.close();
            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyfordocuments");
            $route.reload();
        };
        $scope.cancelApplyforDocument = function () {
            $scope.modalInstance1.close();
        };
        $scope.inactivityTime = function () {
            var t;
            window.onload = resetTimer;
            window.onmousemove = resetTimer;
            window.onkeypress = resetTimer;

            function logout() {
                // alert('alert');
                $location.path("/login");
                //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
                Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
                    $window.sessionStorage.userId = '';
                    //console.log('Logout');
                }).then(function (redirect) {
                    $window.location.href = 'partials/login';
                    $window.location.reload();
                });
            }

            function resetTimer() {
                clearTimeout(t);
                t = setTimeout(logout, 60 * 60 * 1000)
                // 1000 milisec = 1 sec
            }
        };
        // $scope.inactivityTime();
        $idle.watch();
        //Edit OnetoOne////////////
        $scope.EditoneToOne = function (todotype, id, data, beneficiaryid) {
            $scope.schememaster = {};
            $scope.schememaster.stage = '';
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
            $scope.schememaster.noofmonthrepay = '';
            console.log('EditoneToOne', todotype, id, data, beneficiaryid);

            $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
            $scope.UpdateDisplayClickOption = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + data.questionid).get().$object;

            $scope.hideUpdateTodo = false;
            $scope.todotyes = Restangular.all('todotypes?filter[where][pillarid]=' + pillarid).getList().then(function (todotyes) {
                $scope.todotypes = todotyes;
                $scope.todo = data;
                $scope.MemName = Restangular.one('beneficiaries', data.beneficiaryid).get().then(function (memname) {
                    $scope.MemberName = memname;
                });
                $scope.openToDo();
            });





        };
        ///////////////////////////////Edit One to One Close/////////////////////////////////////////////
        $scope.openGroupMeeting = function () {
            $scope.modalGroupMeeting = $modal.open({
                animation: true,
                templateUrl: 'template/GroupMeetingMain.html',
                scope: $scope,
                backdrop: 'static',
                size: 'lg'
            });
        };
        $scope.okGroupMeeting = function () {
            $scope.modalGroupMeeting.close();
        };
        $scope.openFs = function () {
            $scope.modalFS = $modal.open({
                animation: true,
                templateUrl: 'template/fsMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };

        $scope.openHealth = function () {
            $scope.modalHealth = $modal.open({
                animation: true,
                templateUrl: 'template/healthMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.SaveHealth = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
            $scope.modalHealth.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okHealth = function () {
            $scope.modalHealth.close();
        };

        $scope.openToDo = function () {
            $scope.modalToDo = $modal.open({
                animation: true,
                templateUrl: 'template/ToDoMain.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            });
        };
        $scope.openToDoPhoneNameMain = function () {
            $scope.modalToDo = $modal.open({
                animation: true,
                templateUrl: 'template/ToDoPhoneNameMain.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            });
        };
        $scope.openToDoFollow = function () {
            $scope.modalToDo = $modal.open({
                animation: true,
                templateUrl: 'template/FollowUpMain.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            });
        };
        $scope.SaveToDo = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            $scope.todo.pillarid = pillarid;
            $scope.todo.beneficiaryid = beneficiaryid;
            $scope.todo.fieldworkerid = $window.sessionStorage.UserEmployeeId;
            $scope.todo.facility = $window.sessionStorage.coorgId;
            $scope.todo.district = $window.sessionStorage.salesAreaId;
            $scope.todo.state = $window.sessionStorage.zoneId;
            $scope.todo.questionid = questionid;
            $scope.submittodos.post($scope.todo).then(function (resp) {
                $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                $scope.todo = {
                    status: 1,
                    stateid: $window.sessionStorage.zoneId,
                    state: $window.sessionStorage.zoneId,
                    districtid: $window.sessionStorage.salesAreaId,
                    district: $window.sessionStorage.salesAreaId,
                    coid: $window.sessionStorage.coorgId,
                    facility: $window.sessionStorage.coorgId,
                    lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                    lastmodifiedtime: new Date()
                };
                var sevendays = new Date();
                sevendays.setDate(sevendays.getDate() + 7);
                $scope.todo.datetime = sevendays;
            }, function (error) {
                //console.log('error', error);
            });
            $scope.modalToDo.close();

            $scope.healthnextquestion();
        };
        $scope.okToDo = function () {
            $scope.modalToDo.close();
        };






        $scope.openDP = function () {
            $scope.modalDP = $modal.open({
                animation: true,
                templateUrl: 'template/DatePickerMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.beneficiarylastdate = {};
        $scope.SaveDP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            if (questionid == 3) {
                $scope.beneficiarylastdate.monthoflasthivtest = $scope.picker.selectdate;
            } else {
                $scope.beneficiarylastdate.monthoflaststitest = $scope.picker.selectdate;
            }
            if ($routeParams.id != undefined) {
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarylastdate).then(function (respo) {
                    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                });
            }
            $scope.modalDP.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okDP = function () {
            $scope.modalDP.close();
        };
        $scope.openIDP = function () {
            $scope.modalIDP = $modal.open({
                animation: true,
                templateUrl: 'template/IncrementDatePickerMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.beneficiaryincrement = {};
        $scope.SaveIDP = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            // //console.log('Button Clicked', beneficiaryid + '::' + pillarid + ',questionid::' + questionid + ',answer::' + answer + ',modifieddate::' + modifieddate + ',modifiedby::' + modifiedby + ',serialno::' + serialno + ',yesincrement::' + yesincrement + ',noincrement::' + noincrement);
            if ($routeParams.id != undefined) {
                Restangular.one('beneficiaries', $routeParams.id).get().then(function (kp) {
                    var d1 = Date.parse($scope.increment.selectdate);
                    var d2 = Date.parse($scope.DisplayBeneficiary.monthoflastsaving);
                    // //console.log('d1', d1);
                    // //console.log('d2', d2);
                    if (yesincrement == 'savings') {
                        $scope.beneficiaryincrement.noofsavingaccounts = +kp.noofsavingaccounts + 1;
                        if (d1 > d2) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                        }
                    } else if (yesincrement == 'investmentproducts') {
                        $scope.beneficiaryincrement.noofinvestmentproducts = +kp.noofinvestmentproducts + 1;
                        if (d1 > d2) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                        }
                    } else if (yesincrement == 'savingsources') {
                        $scope.beneficiaryincrement.noofinformalsavingsources = +kp.noofinformalsavingsources + 1;
                        if (d1 > d2) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                        }
                    } else if (yesincrement == 'insuranceproducts') {
                        $scope.beneficiaryincrement.noofinsuranceproducts = +kp.noofinsuranceproducts + 1;
                        if (d1 > d2) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                        }
                    } else if (yesincrement == 'reciept') {
                        $scope.beneficiaryincrement.paidmember = true;
                        if (d1 > d2) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else if ($scope.DisplayBeneficiary.monthoflastsaving == null) {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.increment.selectdate;
                        } else {
                            $scope.beneficiaryincrement.monthoflastsaving = $scope.DisplayBeneficiary.monthoflastsaving;
                        }
                    }
                    ////console.log('$scope.beneficiaryincrement', $scope.beneficiaryincrement);
                    if ($routeParams.id != undefined) {
                        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryincrement).then(function (respo) {
                            $scope.beneficiaryincrement = {};
                            $scope.increment = {};
                            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                        }, function (error) {
                            //console.log('error', error);
                        });
                    }
                });
            }
            $scope.modalIDP.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okIDP = function () {
            $scope.modalIDP.close();
        };
        $scope.openCDIDS = function () {
            $scope.modalCDIDS = $modal.open({
                animation: true,
                templateUrl: 'template/CDIDSMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.SaveCDIDS = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
            $scope.availeddate = $scope.cdids.availeddate;
            $scope.referenceno = $scope.cdids.referenceno;
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, $scope.availeddate, $scope.referenceno);
            $scope.modalCDIDS.close();
            if (pillarid == 1) {
                $scope.spquestioncount++;
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjquestioncount;
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsquestioncount;
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsquestioncount;
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthquestioncount;
                $scope.healthnextquestion();
            }
        };
        $scope.okCDIDS = function () {
            $scope.modalCDIDS.close();
        };
        $scope.openAW = function (size) {
            $scope.modalAW = $modal.open({
                animation: true,
                templateUrl: 'template/ApplicationWorkflowMain.html',
                scope: $scope,
                backdrop: 'static',
                size: size
            });
        };
        $scope.SaveAW = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
            if (answer == 'yes') {
                $scope.todo.documentid = yesdocumentid;
                $scope.todo.documentflag = yesdocumentflag;
                $scope.schememaster.documentflag = yesdocumentflag;
                $scope.schememaster.schemeId = yesdocumentid;
                if (yesdocumentflag == 'yes') {
                    $scope.todo.todotype = 29;
                    $scope.todo.pillarid = pillarid;
                    $scope.todo.questionid = questionid;
                    $scope.todo.beneficiaryid = beneficiaryid;
                    $scope.todo.datetime = $scope.schememaster.datetime;
                    $scope.todo.stage = $scope.schememaster.stage;
                    $scope.todo.facility = $window.sessionStorage.coorgId;
                    $scope.todo.district = $window.sessionStorage.salesAreaId;
                    $scope.todo.state = $window.sessionStorage.zoneId;
                    $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.todo.lastmodifiedtime = new Date();
                    $scope.todo.site = $scope.DisplayBeneficiary.site;
                    $scope.schememaster.memberId = beneficiaryid;
                    $scope.schememaster.facility = $window.sessionStorage.coorgId;
                    $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                    $scope.schememaster.state = $window.sessionStorage.zoneId;
                    $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.schememaster.lastmodifiedtime = new Date();
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        //console.log('$scope.documentmasters', $scope.schememaster);
                        $scope.todo.reportincidentid = resp.id;
                        $scope.submittodos.post($scope.todo).then(function () {
                            //console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                } else {
                    $scope.todo.todotype = 28;
                    Restangular.one('schemes?filter[state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + yesdocumentid).get().then(function (schemes) {
                        if (schemes.length > 0) {
                            $scope.todo.documentid = schemes[0].id;
                            $scope.schememaster.schemeId = schemes[0].id;
                            $scope.todo.pillarid = pillarid;
                            $scope.todo.questionid = questionid;
                            $scope.todo.beneficiaryid = beneficiaryid;
                            $scope.todo.datetime = $scope.schememaster.datetime;
                            $scope.todo.stage = $scope.schememaster.stage;
                            $scope.todo.facility = $window.sessionStorage.coorgId;
                            $scope.todo.district = $window.sessionStorage.salesAreaId;
                            $scope.todo.state = $window.sessionStorage.zoneId;
                            $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                            $scope.todo.lastmodifiedtime = new Date();
                            $scope.todo.site = $scope.DisplayBeneficiary.site;
                            $scope.schememaster.memberId = beneficiaryid;
                            $scope.schememaster.facility = $window.sessionStorage.coorgId;
                            $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                            $scope.schememaster.state = $window.sessionStorage.zoneId;
                            $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                            $scope.schememaster.lastmodifiedtime = new Date();
                            $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                                //console.log('$scope.documentmasters', $scope.schememaster);
                                $scope.todo.reportincidentid = resp.id;
                                $scope.submittodos.post($scope.todo).then(function () {
                                    //console.log('$scope.todo', $scope.todo);
                                    $scope.SchemeOrDocumentname = null;
                                    $scope.schememaster.stage = null;
                                });
                            });
                        }
                    });
                }
            } else {
                $scope.todo.documentid = nodocumentid;
                $scope.todo.documentflag = nodocumentflag;
                $scope.schememaster.documentflag = nodocumentflag;
                $scope.schememaster.schemeId = nodocumentid;
                if (yesdocumentflag == 'yes') {
                    $scope.todo.todotype = 29;
                    $scope.todo.pillarid = pillarid;
                    $scope.todo.questionid = questionid;
                    $scope.todo.beneficiaryid = beneficiaryid;
                    $scope.todo.datetime = $scope.schememaster.datetime;
                    $scope.todo.stage = $scope.schememaster.stage;
                    $scope.todo.facility = $window.sessionStorage.coorgId;
                    $scope.todo.district = $window.sessionStorage.salesAreaId;
                    $scope.todo.state = $window.sessionStorage.zoneId;
                    $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.todo.lastmodifiedtime = new Date();
                    $scope.todo.site = $scope.DisplayBeneficiary.site;
                    $scope.schememaster.memberId = beneficiaryid;
                    $scope.schememaster.facility = $window.sessionStorage.coorgId;
                    $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                    $scope.schememaster.state = $window.sessionStorage.zoneId;
                    $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.schememaster.lastmodifiedtime = new Date();
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        //console.log('$scope.documentmasters', $scope.schememaster);
                        $scope.todo.reportincidentid = resp.id;
                        $scope.submittodos.post($scope.todo).then(function () {
                            //console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                } else {
                    $scope.todo.todotype = 28;
                    Restangular.one('schemes?filter[state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=' + nodocumentid).get().then(function (schemes) {
                        if (schemes.length > 0) {
                            $scope.todo.documentid = schemes[0].id;
                            $scope.schememaster.schemeId = schemes[0].id;
                            $scope.todo.pillarid = pillarid;
                            $scope.todo.questionid = questionid;
                            $scope.todo.beneficiaryid = beneficiaryid;
                            $scope.todo.datetime = $scope.schememaster.datetime;
                            $scope.todo.stage = $scope.schememaster.stage;
                            $scope.todo.facility = $window.sessionStorage.coorgId;
                            $scope.todo.district = $window.sessionStorage.salesAreaId;
                            $scope.todo.state = $window.sessionStorage.zoneId;
                            $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                            $scope.todo.lastmodifiedtime = new Date();
                            $scope.todo.site = $scope.DisplayBeneficiary.site;
                            $scope.schememaster.memberId = beneficiaryid;
                            $scope.schememaster.facility = $window.sessionStorage.coorgId;
                            $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                            $scope.schememaster.state = $window.sessionStorage.zoneId;
                            $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                            $scope.schememaster.lastmodifiedtime = new Date();
                            $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                                //console.log('$scope.documentmasters', $scope.schememaster);
                                $scope.todo.reportincidentid = resp.id;
                                $scope.submittodos.post($scope.todo).then(function () {
                                    //console.log('$scope.todo', $scope.todo);
                                    $scope.SchemeOrDocumentname = null;
                                    $scope.schememaster.stage = null;
                                });
                            });
                        }
                    });
                }
            }
            $scope.modalAW.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okAW = function () {
            $scope.modalAW.close();
            // $route.reload();
        };
        $scope.openAWBoth = function (size) {
            $scope.schememaster.memberId = $scope.DisplayBeneficiary.id;
            $scope.modalAWBoth = $modal.open({
                animation: true,
                templateUrl: 'template/ApplicationWorkflowAllMain.html',
                scope: $scope,
                backdrop: 'static',
                size: size
            });
        };
        $scope.SaveAWBoth = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
            if (answer == 'yes') {
                $scope.todo.documentid = $scope.schememaster.schemeId;
                $scope.todo.documentflag = $scope.schememaster.documentflag;
                if (yesdocumentflag == 'yes') {
                    $scope.todo.todotype = 29;
                    $scope.todo.pillarid = pillarid;
                    $scope.todo.questionid = questionid;
                    $scope.todo.beneficiaryid = beneficiaryid;
                    $scope.todo.datetime = $scope.schememaster.datetime;
                    $scope.todo.stage = $scope.schememaster.stage;
                    $scope.todo.facility = $window.sessionStorage.coorgId;
                    $scope.todo.district = $window.sessionStorage.salesAreaId;
                    $scope.todo.state = $window.sessionStorage.zoneId;
                    $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.todo.lastmodifiedtime = new Date();
                    $scope.todo.site = $scope.DisplayBeneficiary.site;
                    $scope.schememaster.memberId = beneficiaryid;
                    $scope.schememaster.facility = $window.sessionStorage.coorgId;
                    $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                    $scope.schememaster.state = $window.sessionStorage.zoneId;
                    $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.schememaster.lastmodifiedtime = new Date();
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        //console.log('$scope.documentmasters', $scope.schememaster);
                        $scope.todo.reportincidentid = resp.id;
                        $scope.submittodos.post($scope.todo).then(function () {
                            //console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                } else {
                    $scope.todo.todotype = 28;
                    $scope.todo.pillarid = pillarid;
                    $scope.todo.questionid = questionid;
                    $scope.todo.beneficiaryid = beneficiaryid;
                    $scope.todo.datetime = $scope.schememaster.datetime;
                    $scope.todo.stage = $scope.schememaster.stage;
                    $scope.todo.facility = $window.sessionStorage.coorgId;
                    $scope.todo.district = $window.sessionStorage.salesAreaId;
                    $scope.todo.state = $window.sessionStorage.zoneId;
                    $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.todo.lastmodifiedtime = new Date();
                    $scope.todo.site = $scope.DisplayBeneficiary.site;
                    $scope.schememaster.memberId = beneficiaryid;
                    $scope.schememaster.facility = $window.sessionStorage.coorgId;
                    $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                    $scope.schememaster.state = $window.sessionStorage.zoneId;
                    $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.schememaster.lastmodifiedtime = new Date();
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        //console.log('$scope.documentmasters', $scope.schememaster);
                        $scope.todo.reportincidentid = resp.id;
                        $scope.submittodos.post($scope.todo).then(function () {
                            //console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                }
            } else {
                $scope.todo.documentid = $scope.schememaster.schemeId;
                $scope.todo.documentflag = $scope.schememaster.documentflag;
                if (yesdocumentflag == 'yes') {
                    $scope.todo.todotype = 29;
                    $scope.todo.pillarid = pillarid;
                    $scope.todo.questionid = questionid;
                    $scope.todo.beneficiaryid = beneficiaryid;
                    $scope.todo.datetime = $scope.schememaster.datetime;
                    $scope.todo.stage = $scope.schememaster.stage;
                    $scope.todo.facility = $window.sessionStorage.coorgId;
                    $scope.todo.district = $window.sessionStorage.salesAreaId;
                    $scope.todo.state = $window.sessionStorage.zoneId;
                    $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.todo.lastmodifiedtime = new Date();
                    $scope.todo.site = $scope.DisplayBeneficiary.site;
                    $scope.schememaster.memberId = beneficiaryid;
                    $scope.schememaster.facility = $window.sessionStorage.coorgId;
                    $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                    $scope.schememaster.state = $window.sessionStorage.zoneId;
                    $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.schememaster.lastmodifiedtime = new Date();
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        //console.log('$scope.documentmasters', $scope.schememaster);
                        $scope.todo.reportincidentid = resp.id;
                        $scope.submittodos.post($scope.todo).then(function () {
                            //console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                } else {
                    $scope.todo.todotype = 28;
                    $scope.todo.pillarid = pillarid;
                    $scope.todo.questionid = questionid;
                    $scope.todo.beneficiaryid = beneficiaryid;
                    $scope.todo.datetime = $scope.schememaster.datetime;
                    $scope.todo.stage = $scope.schememaster.stage;
                    $scope.todo.facility = $window.sessionStorage.coorgId;
                    $scope.todo.district = $window.sessionStorage.salesAreaId;
                    $scope.todo.state = $window.sessionStorage.zoneId;
                    $scope.todo.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.todo.lastmodifiedtime = new Date();
                    $scope.todo.site = $scope.DisplayBeneficiary.site;
                    $scope.schememaster.memberId = beneficiaryid;
                    $scope.schememaster.facility = $window.sessionStorage.coorgId;
                    $scope.schememaster.district = $window.sessionStorage.salesAreaId;
                    $scope.schememaster.state = $window.sessionStorage.zoneId;
                    $scope.schememaster.lastmodifiedby = $window.sessionStorage.UserEmployeeId;
                    $scope.schememaster.lastmodifiedtime = new Date();
                    $scope.submitdocumentmasters.post($scope.schememaster).then(function (resp) {
                        //console.log('$scope.documentmasters', $scope.schememaster);
                        $scope.todo.reportincidentid = resp.id;
                        $scope.submittodos.post($scope.todo).then(function () {
                            //console.log('$scope.todo', $scope.todo);
                            $scope.SchemeOrDocumentname = null;
                            $scope.schememaster.stage = null;
                        });
                    });
                }
            }
            $scope.modalAWBoth.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okAWBoth = function () {
            $scope.modalAWBoth.close();
            //$route.reload();
        };
        $scope.openPLHIV = function () {
            $scope.plhiv.source = $scope.DisplayBeneficiary.sourceofinfection;
            $scope.plhiv.month = $scope.DisplayBeneficiary.monthofdetection;
            $scope.modalPLHIV = $modal.open({
                animation: true,
                templateUrl: 'template/PLHIVMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.beneficiaryplhiv = {};
        $scope.SavePLHIV = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            $scope.beneficiaryplhiv.plhiv = true;
            $scope.beneficiaryplhiv.sourceofinfection = $scope.plhiv.source;
            $scope.beneficiaryplhiv.monthofdetection = $scope.plhiv.month;
            if ($routeParams.id != undefined) {
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryplhiv).then(function (respo) {
                    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                });
            }
            $scope.modalPLHIV.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okPLHIV = function () {
            $scope.modalPLHIV.close();
        };
        $scope.openCD = function () {
            if ($scope.DisplayBeneficiary.condomsasked != null) {
                var CondomsAsked = $scope.DisplayBeneficiary.condomsasked.split(',');
                var CondomsProvided = $scope.DisplayBeneficiary.condomsprovided.split(',');
                if (CondomsAsked.length > 0) {
                    var AskedNum = CondomsAsked[CondomsAsked.length - 1].split(':');
                    var ProvidedNum = CondomsProvided[CondomsProvided.length - 1].split(':');
                    $scope.condom.asked = AskedNum[AskedNum.length - 1];
                    $scope.condom.provided = ProvidedNum[ProvidedNum.length - 1];
                }
            }
            $scope.modalCD = $modal.open({
                animation: true,
                templateUrl: 'template/condomdetailsMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.SaveCD = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            $scope.askeddate = $filter('date')($scope.condom.dateasked, 'dd/MM/yyyy');
            if ($scope.DisplayBeneficiary.condomsasked != null) {
                $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
                $scope.beneficiarycondom.condomsasked = $scope.DisplayBeneficiary.condomsasked + ',' + $scope.askeddate + ':' + $scope.condom.asked;
                $scope.beneficiarycondom.condomsprovided = $scope.DisplayBeneficiary.condomsprovided + ',' + $scope.askeddate + ':' + $scope.condom.provided;
            } else {
                $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
                $scope.beneficiarycondom.condomsasked = $scope.askeddate + ':' + $scope.condom.asked;
                $scope.beneficiarycondom.condomsprovided = $scope.askeddate + ':' + $scope.condom.provided;
            }
            if ($routeParams.id != undefined) {
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarycondom).then(function (respo) {
                    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                });
            }
            $scope.modalCD.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okCD = function () {
            $scope.modalCD.close();
        };
        $scope.openFL = function () {
            $scope.finliteracy.savings = $scope.DisplayBeneficiary.savings;
            $scope.finliteracy.insurance = $scope.DisplayBeneficiary.insurance;
            $scope.finliteracy.pension = $scope.DisplayBeneficiary.pension;
            $scope.finliteracy.credit = $scope.DisplayBeneficiary.credit;
            $scope.finliteracy.remitance = $scope.DisplayBeneficiary.remitance;
            $scope.modalFL = $modal.open({
                animation: true,
                templateUrl: 'template/FinancialLiteracyMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.beneficiaryfl = {};
        $scope.SaveFL = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            $scope.beneficiaryfl.savings = $scope.finliteracy.savings;
            $scope.beneficiaryfl.insurance = $scope.finliteracy.insurance;
            $scope.beneficiaryfl.pension = $scope.finliteracy.pension;
            $scope.beneficiaryfl.credit = $scope.finliteracy.credit;
            $scope.beneficiaryfl.remitance = $scope.finliteracy.remitance;
            if ($routeParams.id != undefined) {
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryfl).then(function (respo) {
                    $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
                });
            }
            $scope.modalFL.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okFL = function () {
            $scope.modalFL.close();
        };
        $scope.openFP = function () {
            $scope.finplanning.savings = $scope.DisplayBeneficiary.finplansavings;
            $scope.finplanning.insurance = $scope.DisplayBeneficiary.finplaninsurance;
            $scope.finplanning.pension = $scope.DisplayBeneficiary.finplanpension;
            $scope.finplanning.credit = $scope.DisplayBeneficiary.finplancredit;
            $scope.finplanning.remitance = $scope.DisplayBeneficiary.finplanremitance;
            $scope.modalFP = $modal.open({
                animation: true,
                templateUrl: 'template/FinancialPlanningMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.openINFO = function () {
            $scope.modalINFO = $modal.open({
                animation: true,
                templateUrl: 'template/ProvideInfoMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.SaveINFO = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            $scope.SaveAnswers(beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement);
            $scope.modalINFO.close();
            if (pillarid == 1) {
                $scope.spnextquestion();
            } else if (pillarid == 2) {
                $scope.ssjnextquestion();
            } else if (pillarid == 3) {
                $scope.fsnextquestion();
            } else if (pillarid == 4) {
                $scope.idsnextquestion();
            } else if (pillarid == 5) {
                $scope.healthnextquestion();
            }
        };
        $scope.okINFO = function () {
            $scope.modalINFO.close();
        };
        $scope.openPillarCompleted = function () {
            $scope.modalPillarCompleted = $modal.open({
                animation: true,
                templateUrl: 'template/PillarCompletedMain.html',
                scope: $scope,
                backdrop: 'static'
            });
        };
        $scope.PillarCompleted = {
            completedpillar: 0,
            completedquestion: 0
        }
        $scope.SavePillarCompleted = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
            if ($routeParams.id != undefined) {
                Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.PillarCompleted).then(function (respo) {
                    $scope.modalPillarCompleted.close();
                    //$route.reload();
                    window.location = "/onetoone/" + $routeParams.id;
                });
            }
        };
        $scope.okPillarCompleted = function () {
            $scope.modalPillarCompleted.close();
            window.location = "/";
        };
        $scope.updateToDo = function () {
            /*$scope.memberupdate = {
            			lastmeetingdate: new Date()
            	}
            	Restangular.all('beneficiaries/' + $scope.todo.beneficiaryid).customPUT($scope.memberupdate).then(function (responsemember) {
            	});*/
            $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function (resp) {
                $scope.todo = {
                    status: 1
                };
                var sevendays = new Date();
                sevendays.setDate(sevendays.getDate() + 7);
                $scope.todo.datetime = sevendays;
                console.log('$scope.todo.datetime', $scope.todo.datetime);
                $scope.modalToDo.close();
                $scope.hideUpdateTodo = true;
            }, function (error) {
                //console.log('error', error);
            });
        };
        $scope.openOneAlert = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true,
                templateUrl: 'template/AlertModal.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
            });
        };
        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };


        $scope.updateAW = function () {

            $scope.paidamounts = $scope.installmentAmount * $scope.studcheck.length;
            //console.log('$scope.paidamounts', $scope.paidamounts);
            console.log('$scope.schememaster.paidamount', $scope.schememaster.paidamount);
            console.log('$scope.schememaster.amount', $scope.schememaster.amount);
            if ($scope.loanapplied == true && $scope.schememaster.stage == 7) {
                $scope.schememaster.paidamount = $scope.installmentAmount * $scope.studcheck.length;
            }
            if ($scope.schememaster.stage == "8") {
                $scope.schememaster.stage = "3";
                $scope.todo.status = 1;
            } else if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                $scope.todo.status = 3;
            } else if ($scope.schememaster.stage == 9) { //|| $scope.schememaster.stage == 7) {
                $scope.todo.status = 3;
            } else if ($scope.schememaster.paidamount == $scope.schememaster.amount) {
                $scope.todo.status = 3;
            } else if ($scope.schememaster.purposeofloan != 5) {
                $scope.todo.status = 3;
                console.log('$scope.todo.status2', $scope.todo.status);
            } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount < 10000) {
                $scope.todo.status = 3;
                console.log('$scope.todo.status1', $scope.todo.status);
            } else if ($scope.paidamounts == $scope.schememaster.amount) {
                $scope.todo.status = 3;
                console.log('$scope.todo.status3', $scope.todo.status);
            } else {
                $scope.todo.status = 1;
            }
            $scope.submitdocumentmasters.customPUT($scope.schememaster, $scope.schememaster.id).then(function (resp) {
                $scope.todo.reportincidentid = resp.id;
                $scope.todo.datetime = resp.datetime;
                $scope.todo.stage = resp.stage;
                console.log('schememaster', $scope.schememaster)
                $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function () {
                    console.log('$scope.todo', $scope.todo);
                    $scope.okAW();
                });
            });
        };
        $scope.surveyanswerrefer = {};
        $scope.UpdateCDIDS = function () {
            $scope.surveyanswerrefer.id = $scope.cdids.suransid;
            $scope.surveyanswerrefer.availeddate = $scope.cdids.availeddate;
            $scope.surveyanswerrefer.referenceno = $scope.cdids.referenceno;
            $scope.submitsurveyanswers.customPUT($scope.surveyanswerrefer, $scope.surveyanswerrefer.id).then(function () {
                $scope.okCDIDS();
                $scope.hideUpdateCDIDS = true;
                $scope.applieddocuments = Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=1&filter[where][answer]=yes&filter[where][availeddate][nlike]=null%').getList().$object;
            });
        };
        $scope.UpdateDocumentReference = function (surveyanswerid) {
            $scope.hideUpdateCDIDS = false;
            if (surveyanswerid != undefined && surveyanswerid != '' && surveyanswerid != null) {
                $scope.surveyanswer = Restangular.one('surveyanswers/findOne?filter[where][id]=' + surveyanswerid).get().then(function (surans) {
                    ////console.log('surans', surans);
                    $scope.cdids.availeddate = surans.availeddate;
                    $scope.cdids.referenceno = surans.referenceno;
                    $scope.cdids.suransid = surans.id;
                    $scope.openCDIDS();
                });
            } else {
                $scope.openCDIDS();
            }
        };
        $scope.member = true;
        $scope.memberone = true;

        $scope.report = true;
        /*******************Loan Changes **********/

        $scope.MonthRepay = function (monthrepay) {
            $scope.installment = $scope.schememaster.amount / monthrepay;
            console.log('$scope.installment', $scope.installment);
        }
        $scope.loanapplied = false;
        $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined) {
                return;
            } else {
                //$scope.schememaster.stage = '';
                Restangular.one('schemes', newValue).get().then(function (SchemeMaster) {
                    //  console.log('SchemeMaster', SchemeMaster);
                    var Array = [];
                    var Array2 = [];
                    Array = SchemeMaster.category;
                    Array2 = Array.split(',');
                    console.log('Array2', Array2.length);
                    for (var i = 0; i < Array2.length; i++) {
                        console.log('Array.length', Array.length);
                        console.log('Array', Array2[i]);
                        if (Array2[i] == 15) {
                            $scope.loanapplied = true;
                            console.log('$scope.loanapplied', $scope.loanapplied);
                        } else {
                            $scope.loanapplied = false;
                            //  console.log('Not Applied');
                        }
                    }
                });
            }
        });

        $scope.hide_Todo_Edit_Button = false;
        $scope.$watch('todo.status', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == undefined) {
                return;
            } else if (newValue == 4 || newValue == 3) {
                //console.log('newValue status', newValue);
                $scope.hide_Todo_Edit_Button = true;
            } else {
                $scope.hide_Todo_Edit_Button = false;
            }
        });

        $scope.TestamountTaken = function () {
            $scope.schememaster.noofmonthrepay = '';
        }
        $scope.datetimehide = false;
        $scope.reponserec = true;
        $scope.delaydis = true;
        $scope.collectedrequired = true;
        $scope.purposeofloanhide = true;

        /********************************************************************************************/
        $scope.sm = Restangular.all('schemes?filter[where][deleteflag]=false').getList().then(function (scheme) {
            $scope.printschemes = scheme;
            angular.forEach($scope.printschemes, function (member, index) {
                member.index = index;
                member.enabled = false;
            });
        });
        $scope.docm = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (docmt) {
            $scope.printdocuments = docmt;
            angular.forEach($scope.printdocuments, function (member, index) {
                member.index = index;
                member.enabled = false;
            });
        });
        $scope.getGender = function (genderId) {
            return Restangular.one('genders', genderId).get().$object;
        };
        $scope.schememaster = {
            attendees: []
        };
        /* $scope.rejectdis = true;
         $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
             if (newValue === oldValue) {
                 return;
             } else if (newValue === 'Rejected') {
                 $scope.rejectdis = false;
             } else {
                 $scope.rejectdis = true;
                 $scope.schememaster.rejection = null;
             }
         });*/
        $scope.schememaster.attendees = [];


        $scope.followupgroups = Restangular.all('followupgroups').getList().$object;
        $scope.submitgroupmeetings = Restangular.all('groupmeetings');
        $scope.fieldworkers4 = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fwrResponse) {
            $scope.fieldworkers = fwrResponse;
        });
        //$scope.fieldworkers = Restangular.all('fieldworkers').getList().$object;
        $scope.comembers = Restangular.all('comembers');
        $scope.applicastages = Restangular.all('applicationstages').getList().then(function (appstage) {
            $scope.applicationstages = appstage;
            angular.forEach($scope.applicationstages, function (member, index) {
                member.index = index + 1;
                member.enabled = false;
            });
        });
        $scope.getCO = function (partnerId) {
            return Restangular.one('comembers', partnerId).get().$object;
            //return Restangular.one('fieldworkers', partnerId).get().$object;
        };
        /*
        	$scope.getFW = function (partnerId) {
        		return Restangular.one('fieldworkers', partnerId).get().$object;
        	};*/
        $scope.getGroupMeetingTopics = function (partnerId) {
            return Restangular.all('groupmeetingtopics?filter={"where":{"id":{"inq":[' + partnerId + ']}}}').getList().$object;
        };
        $scope.getPilar = function (id) {
            return Restangular.all('pillars?filter={"where":{"id":{"inq":[' + id + ']}}}').getList().$object;
        };
        /***********************************************************************************************/
        $scope.documenthide = true;
        $scope.documenthide = true;
        $scope.$watch('groupmeeting.organised', function (newValue, oldValue) {
            ////console.log('groupmeeting.organisedby', newValue);
            if (newValue === oldValue) {
                return;
            } else {
                $scope.groupmeeting.organisedby = newValue;
                if (newValue == 'Others') {
                    $scope.documenthide = false;
                    $scope.groupmeeting.organisedby = null;
                } else {
                    $scope.documenthide = true;
                }
            }
        });
        /****************************************************************************************/
        $scope.groupcreate = false;
        var booleanValue = true;
        $scope.organisedbychange = true;
        $scope.Change = function () {
            if (booleanValue == false) {
                $scope.organisedbychange = true;
                $scope.groupcreate = true;
            } else {
                $scope.organisedbychange = false;
                $scope.groupcreate = true;
            }
        };
        $scope.groupmeeting = {
            attendees: [],
            follow: [],
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date()
        };
        $scope.$watch('groupmeeting.attendees', function (newValue, oldValue) {
            // //console.log('watch.attendees', newValue);
            if (newValue === oldValue) {
                return;
            } else {
                $scope.attendeestodo = newValue;
                //console.log('$scope.attendeestodo', $scope.attendeestodo);
                //console.log('$scope.attendeestodo', $scope.attendeestodo);
                if (newValue.length > oldValue.length) {
                    // something was added
                    var Array1 = newValue;
                    var Array2 = oldValue;
                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                            }
                        }
                    }
                    for (var i = 0; i < $scope.partners.length; i++) {
                        if ($scope.partners[i].id == Array1[0]) {
                            $scope.partners[i].enabled = true;
                        }
                    }
                    // do stuff
                } else if (newValue.length < oldValue.length) {
                    // something was removed
                    var Array1 = oldValue;
                    var Array2 = newValue;
                    for (var i = 0; i < Array2.length; i++) {
                        var arrlen = Array1.length;
                        for (var j = 0; j < arrlen; j++) {
                            if (Array2[i] == Array1[j]) {
                                Array1 = Array1.slice(0, j).concat(Array1.slice(j + 1, arrlen));
                            }
                        }
                    }
                    for (var i = 0; i < $scope.partners.length; i++) {
                        if ($scope.partners[i].id == Array1[0]) {
                            $scope.partners[i].enabled = false;
                        }
                    }
                    //$scope.partners[Array1[0]].enabled = false;
                }
            }
        });
        $scope.zipmasters = Restangular.all('pillars').getList().then(function (zipmaster) {
            $scope.zipmasters = zipmaster;
            angular.forEach($scope.zipmasters, function (member, index) {
                member.total = null;
                member.loader = null;
            });
        });
        $scope.checkbox = [];
        $scope.eventcheckbox = [];
        $scope.mobilenumbers = [];
        $scope.GetMobNumbers = function (index, zipcode, event) {
            ////console.log($scope.zipmasters[index]);
            if ($scope.zipmasters[index].enabled == true) {
                $scope.zipmasters[index].loader = 'images/loginloader.gif';
                //   //console.log($scope.checkbox[index], zipcode);
                $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
                    $scope.prospects = surveyquestions;
                    $scope.zipmasters[index].total = surveyquestions.length;
                    $scope.zipmasters[index].loader = null;
                    for (var i = 0; i < surveyquestions.length; i++) {
                        surveyquestions[i].enabled = false;
                        $scope.mobilenumbers.push(surveyquestions[i]);
                    };
                });
            } else {
                $scope.zipmasters[index].total = null;
                $scope.zipmasters[index].loader = 'images/loginloader.gif';
                $scope.surveyquestions = Restangular.all('groupmeetingtopics?filter[where][pillarid]=' + $scope.zipmasters[index].id).getList().then(function (surveyquestions) {
                    $scope.zipmasters[index].loader = null;
                    for (var i = 0; i < surveyquestions.length; i++) {
                        $scope.mobilenumbers.splice($scope.mobilenumbers.indexOf(surveyquestions[i].name), 1);
                    };
                });
            }
        }
        $scope.Updategroupmeeting = function () {
            $scope.groupmeeting.attendeeslist = null;
            $scope.groupmeeting.followup = null;
            $scope.groupmeeting.pillarid = null;
            $scope.groupmeeting.topicsdiscussed = null;
            //if (document.getElementsByClassName('messageCheckbox').length > 0) {
            //$scope.validatestring = $scope.validatestring + 'Plese Select a Pillar';
            var checkedValue = null;
            var inputElements1 = document.getElementsByClassName('messageCheckbox');
            for (var i = 0; inputElements1[i]; ++i) {
                if (inputElements1[i].checked) {
                    if (checkedValue == null) {
                        checkedValue = inputElements1[i].value;
                    } else {
                        checkedValue = checkedValue + ',' + inputElements1[i].value;
                    }
                }
            };
            $scope.groupmeeting.pillarid = checkedValue;
            var eventcheckedValue = null;
            var inputElements2 = document.getElementsByClassName('eventCheckbox');
            for (var i = 0; inputElements2[i]; ++i) {
                if (inputElements2[i].checked) {
                    if (eventcheckedValue == null) {
                        eventcheckedValue = inputElements2[i].value;
                    } else {
                        eventcheckedValue = eventcheckedValue + ',' + inputElements2[i].value;
                    }
                }
            }
            $scope.groupmeeting.topicsdiscussed = eventcheckedValue;
            var checkedValue1 = null;
            var inputElements3 = document.getElementsByClassName('attendcheckbox');
            for (var i = 0; inputElements3[i]; ++i) {
                if (inputElements3[i].checked) {
                    if (checkedValue1 == null) {
                        checkedValue1 = inputElements3[i].value;
                    } else {
                        checkedValue1 = checkedValue1 + ',' + inputElements3[i].value;
                    }
                }
            }
            $scope.groupmeeting.attendeeslist = checkedValue1;
            if ($scope.groupmeeting.follow != 'None') {
                //	$scope.groupmeeting.followup = null;
                //}	else {
                for (var i = 0; i < $scope.groupmeeting.follow.length; i++) {
                    if (i == 0) {
                        $scope.groupmeeting.followup = $scope.groupmeeting.follow[i];
                    } else {
                        $scope.groupmeeting.followup = $scope.groupmeeting.followup + ',' + $scope.groupmeeting.follow[i];
                    }
                }
            } else {
                $scope.groupmeeting.followup = null;
            }
            if ($scope.groupmeeting.name == '' || $scope.groupmeeting.name == null) {
                $scope.validatestring = $scope.validatestring + 'Plese Enter Group Meeting Name';
            } else if (checkedValue === null) {
                $scope.validatestring = $scope.validatestring + 'Plese Select Pillars';
            } else if (eventcheckedValue === null) {
                $scope.validatestring = $scope.validatestring + 'Plese Select Topics';
            } else if ($scope.groupmeeting.organised == '' || $scope.groupmeeting.organised == null) {
                $scope.validatestring = $scope.validatestring + 'Plese Select a Organised By';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitgroupmeetings.customPUT($scope.groupmeeting, $scope.groupmeeting.id).then(function () {
                    //console.log('groupmeeting Saved', $scope.groupmeeting);
                    $scope.okGroupMeeting();
                });
            };
        };
        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = new Date();
            //$scope.todo.datetime = $filter('date')(new Date(), 'y-MM-dd');
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            $scope.todo.datetime = sevendays;
        };
        // var nextmonth = new Date();
        // nextmonth.setMonth(sevendays.getMonth + 1);
        // $scope.cdids.maxdate = nextmonth;
        var oneday = new Date();
        oneday.setDate(oneday.getDate() + 1);
        $scope.reportincidentdtmin = new Date();
        $scope.today();
        $scope.condom.dt = new Date();
        $scope.picker.dt = new Date();
        $scope.picker.selectdate = new Date();
        $scope.condom.dateasked = new Date();
        $scope.plhiv.month = new Date();
        $scope.plhiv.maxdate = new Date();
        $scope.cdids.availeddate = new Date();
        $scope.cdidsmaxdate = new Date();
        $scope.increment.selectdate = new Date();
        $scope.condom.dateasked = new Date();
        $scope.todomin = new Date();
        $scope.schemedocmaxdt = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        // Disable weekend selection
        $scope.disabled = function (date, mode) {
            return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
        };
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.todoopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.todo.opened = true;
        };
        $scope.fsopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.fs.opened = true;
        };
        $scope.healthopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.health.opened = true;
        };
        $scope.spopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.sp.opened = true;
        };

        $scope.followopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.follow.opened = true;
        };
        $scope.pickeropen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.picker.opened = true;
        };
        $scope.cdidsopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.cdids.opened = true;
        };
        $scope.workflowopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.schememaster.opened = true;
        };
        $scope.plhivopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.plhiv.opened = true;
        };
        $scope.condomopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.condom.opened = true;
        };
        $scope.incrementopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.increment.opened = true;
        };
        $scope.folowupdateopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.schememaster.opened = true;
        };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end
        //////////////////////////////////////////////////////////Analytics Start////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////Analytics End////////////////////////////////////////////////////////
    })
    .service('anchorSmoothScroll', function () {

        this.scrollTo = function (eID) {

            // This scrolling function 
            // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

            var startY = currentYPosition();
            var stopY = elmYPosition(eID);
            var distance = stopY > startY ? stopY - startY : startY - stopY;
            if (distance < 100) {
                scrollTo(0, stopY);
                return;
            }
            var speed = Math.round(distance / 100);
            if (speed >= 20) speed = 20;
            var step = Math.round(distance / 25);
            var leapY = stopY > startY ? startY + step : startY - step;
            var timer = 0;
            if (stopY > startY) {
                for (var i = startY; i < stopY; i += step) {
                    setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                    leapY += step;
                    if (leapY > stopY) leapY = stopY;
                    timer++;
                }
                return;
            }
            for (var i = startY; i > stopY; i -= step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY -= step;
                if (leapY < stopY) leapY = stopY;
                timer++;
            }

            function currentYPosition() {
                // Firefox, Chrome, Opera, Safari
                if (self.pageYOffset) return self.pageYOffset;
                // Internet Explorer 6 - standards mode
                if (document.documentElement && document.documentElement.scrollTop)
                    return document.documentElement.scrollTop;
                // Internet Explorer 6, 7 and 8
                if (document.body.scrollTop) return document.body.scrollTop;
                return 0;
            }

            function elmYPosition(eID) {
                var elm = document.getElementById(eID);
                var y = elm.offsetTop;
                var node = elm;
                while (node.offsetParent && node.offsetParent != document.body) {
                    node = node.offsetParent;
                    y += node.offsetTop;
                }
                return y;
            }

        };

    });




//$scope.user = Restangular.one('users', 1).getList().$object;
/* var redirectTimeout;

var redirect = function() {
    $location.path("/login");
    
    //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
    Restangular.one('users/logout?access_token='+$window.sessionStorage.accessToken).post().then(function(logout){
        $window.sessionStorage.userId='';
		        //console.log('Logout');
		      });
}    

$timeout.cancel(redirectTimeout);

redirectTimeout = $timeout(function() {
    var timeoutTime = 30*60*1000 // 30 minutes
    redirectTimeout = $timeout(redirect, timeoutTime);
});*/
////////////////////////////////////////////auto=logout end////////////////////////////////////////////
