'use strict';
angular.module('secondarySalesApp').controller('cobudgets2Ctrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal) {
    
    
    
    $scope.BudgetEdit = function (budID, budMonth) {
        console.log('BudgetId', budID, budMonth);
        $scope.currentBudgetId = budID;
        Restangular.one('cobudgets', budID).get().then(function (aprl) {
            console.log('April', aprl);
            $scope.original = aprl;
            $scope.cobudget = Restangular.copy($scope.original);
        });
        if (budMonth == 4) {
            $scope.BudgetEditModal1();
        }
        else if (budMonth == 5) {
            $scope.BudgetEditModal2();
            //return;
        }
    }
    $scope.Update = function () {
        Restangular.one('cobudgets', $scope.currentBudgetId).customPUT($scope.cobudget).then(function (updateResponse) {
            console.log('updateResponse', updateResponse);
            $scope.modalAW.close();
            $route.reload();
        });
    }
    $scope.BudgetEditModal1 = function () {
        $scope.modalAW = $modal.open({
            animation: true
            , templateUrl: 'template/spmbudgetView.html'
            , scope: $scope
            , backdrop: 'static'
            , size: 'lg'
        });
    };
});