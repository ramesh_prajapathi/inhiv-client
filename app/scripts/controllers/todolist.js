'use strict';
angular.module('secondarySalesApp').controller('ToDoListCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $rootScope, $idle, $modal, $filter, $route) {

    $rootScope.clickFW();
    
      Restangular.one('todolanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (todoResp) {
        $scope.todolabel = todoResp[0];
      });
    
     $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
    $scope.maintodotypes = Restangular.all('todotypes?filter[where][deleteflag]=false').getList().$object;
     $scope.todostatuses = Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().$object;
    
    Restangular.one('todostatuses?filter[where][deleteflag]=false&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (responseDue) {
       // console.log('responseDue', responseDue);
        $scope.todostatuseDisply = responseDue;
    });
    $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');

  
/************************** List page ******************/
    if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {
         $scope.TotalTodos = [];
                    Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                        $scope.partners = part1;
                        //questionid
                        Restangular.all('todos?filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][faility]=' + $window.sessionStorage.coorgId  + '&filter[where][questionid][lte]=0' + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z').getList().then(function (myRes) {
                            $scope.todos = myRes;
                           // console.log('myRes', myRes);
                         $scope.modalInstanceLoad.close();


                            angular.forEach($scope.todos, function (member, index) {
                                member.index = index + 1;

                                for (var m = 0; m < $scope.partners.length; m++) {

                                    if (member.beneficiaryid == $scope.partners[m].id) {
                                        member.Membername = $scope.partners[m].fullname + ' /' + $scope.partners[m].tiId;
                                        break;
                                    }
                                }
                               
                                for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                    if (member.todotype == $scope.maintodotypes[o].id) {
                                        member.ActionType = $scope.maintodotypes[o].name;
                                        break;
                                    }
                                }
                               
                                for (var z = 0; z < $scope.todostatuses.length; z++) {
                                    if (member.status == $scope.todostatuses[z].id) {
                                        member.StatusName = $scope.todostatuses[z].name;
                                        break;
                                    }
                                }
                                
                                 /**for (var z = 0; z < $scope.todostatuses.length; z++) {
                                    if (member.status == $scope.todostatuses[z].id) {
                                        member.actionable = $scope.todostatuses[z].actionable;
                                        if( member.actionable == 'Yes'){
                                             member.hideEditicon = true;
                                        }else{
                                              member.hideEditicon = false;
                                        }
                                        break;
                                    }
                                }**/
                               
                                member.datetime = member.datetime;
                                //member.purpose = $scope.getTodopurpose(member.purpose);
                               // $scope.TotalTodos.push(member);

                            });
                            // console.log('$scope.TotalTodos', $scope.TotalTodos);
                        });
                    });

                } else {
                     $scope.TotalTodos = [];
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                        $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                            $scope.partners = part2;


                            Restangular.all('todos?filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][faility]=' + $window.sessionStorage.coorgId + '&filter[where][questionid][neq]=null' + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z').getList().then(function (myRes) {
                                // '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId +
                                $scope.todos = myRes;
                                //console.log('myRes', myRes);
                                $scope.modalInstanceLoad.close();

                                 angular.forEach($scope.todos, function (member, index) {
                                member.index = index + 1;

                                for (var m = 0; m < $scope.partners.length; m++) {

                                    if (member.beneficiaryid == $scope.partners[m].id) {
                                        member.Membername = $scope.partners[m].fullname + ' /' + $scope.partners[m].tiId;
                                        break;
                                    }
                                }
                               
                                for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                    if (member.todotype == $scope.maintodotypes[o].id) {
                                        member.ActionType = $scope.maintodotypes[o].name;
                                        break;
                                    }
                                }
                               
                                for (var z = 0; z < $scope.todostatuses.length; z++) {
                                    if (member.status == $scope.todostatuses[z].id) {
                                        member.StatusName = $scope.todostatuses[z].name;
                                        break;
                                    }
                                }
                               
                                member.datetime = member.datetime;
                              //  $scope.TotalTodos.push(member);

                            });
                                // console.log('$scope.TotalTodos', $scope.TotalTodos);
                            });


                        });
                    });
                }
    /**********************Watch function *************/
    $scope.$watch('statusId', function (newValue, oldValue) {

        if (newValue === oldValue || newValue == '' || newValue == undefined) {
            return;
        } else {


                $scope.TotalTodos = [];

                Restangular.all('todos?filter[where][status]=' + newValue + '&filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][faility]=' + $window.sessionStorage.coorgId + '&filter[where][questionid][neq]=null' + '&filter[order]=datetime%20DESC' + '&filter[where][datetime][gte]=' + $scope.todayDate + 'T00:00:00.000Z').getList().then(function (myRes) {
                    // + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId
                    $scope.todos = myRes;
                    //console.log('myRes', myRes);
                    

                    $scope.modalInstanceLoad.close();

                                 angular.forEach($scope.todos, function (member, index) {
                                member.index = index + 1;

                                for (var m = 0; m < $scope.partners.length; m++) {

                                    if (member.beneficiaryid == $scope.partners[m].id) {
                                        member.Membername = $scope.partners[m].fullname + ' /' + $scope.partners[m].tiId;
                                        break;
                                    }
                                }
                               
                                for (var o = 0; o < $scope.maintodotypes.length; o++) {
                                    if (member.todotype == $scope.maintodotypes[o].id) {
                                        member.ActionType = $scope.maintodotypes[o].name;
                                        break;
                                    }
                                }
                               
                                for (var z = 0; z < $scope.todostatuses.length; z++) {
                                    if (member.status == $scope.todostatuses[z].id) {
                                        member.StatusName = $scope.todostatuses[z].name;
                                        break;
                                    }
                                }
                               
                                member.datetime = member.datetime;
                              //  $scope.TotalTodos.push(member);

                            });

                    });
              
            }
    });
/************************** List page ******************/
   
    
     Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
        });
    
     
    
     $scope.openMemberPouupOpen = function () {
            $scope.modalInstance2 = $modal.open({
                animation: true,
                templateUrl: 'template/memberpopup.html',
                scope: $scope,
                backdrop: 'static'

            });
        };
    
     $scope.okMemberpopup = function (fullname) {

            $scope.modalInstance2.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okMemberpopup', $rootScope.fullname);
            $location.path("/todo/create");
            $route.reload();
        };

        $scope.cancelMemberPopup = function () {
            $scope.modalInstance2.close();
        };
    
    /**************************************** todo Member *******************************************/
        $scope.someFocusVariable = true;
        $scope.FocusMe = true;
        $scope.filterFields = ['fullname', 'tiId', 'phonenumber'];
        $scope.searchInput = '';

       if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {
             $scope.searchInput = '';
            Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {

                    member.index = index + 1;
                });
            });

        } else if ($window.sessionStorage.roleId + "" === "13") {
             $scope.searchInput = '';
             Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;

                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });
                });
             });
        }
    
    
     /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

});