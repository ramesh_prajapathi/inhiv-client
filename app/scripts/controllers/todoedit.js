'use strict';

angular.module('secondarySalesApp')
    .controller('ToDoEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    
      $scope.disableStatus = false;
    $scope.disabletodo = true;
    
      $scope.hideUpdate = false;
       
     Restangular.one('todolanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.TodoLanguage = langResponse[0];
         $scope.selecttodo = $scope.TodoLanguage.seltodo;
         $scope.selectfollowupdate = $scope.TodoLanguage.enterdate;
         $scope.selecttodostatus = $scope.TodoLanguage.selstatus;
        });
    
    
 $scope.todostatuses = Restangular.all('todostatuses?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;
    
 $scope.todotypes = Restangular.all('todotypes?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language  + '&filter[where][type]=Todo').getList().$object;
  
   
    
     if ($routeParams.id) {
        
            Restangular.one('todos', $routeParams.id).get().then(function (todosone) {
               
                 Restangular.one('todostatuses', todosone.status).get().then(function (StatusResp) { 
                     if(StatusResp.actionable == 'Yes'){
                          $scope.disableStatus = false;
                          $scope.hideUpdate = false;
                     }else{
                         $scope.disableStatus = true;
                          $scope.hideUpdate = true;
                     }
                     
                 });
                 Restangular.one('beneficiaries', todosone.beneficiaryid).get().then(function (member) {
                       
                     $scope.memberNameId = member.fullname + '-' + member.tiId;
                 $scope.original = todosone;
                    
                         
                $scope.todosone = Restangular.copy($scope.original);
            });
                 });
          
     }
                      
        /********* Cancel todo*******/
        $scope.cancelTodo = function (){
            
//            if($window.sessionStorage.previous == '/todolist'){
//                 window.location = "/todolist"; 
//            } else {
             window.location = "/"; 
       // }
       
        };                                        
    
        /***************/
        $scope.todosone = {
           
             lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date()
        };
        
               /********************************************** UPDATE ****************************/

        $scope.submitDisable = false;

       
        $scope.validatestring = '';
         $scope.UpdateTodo = function () {
            document.getElementById('name').style.border = "";

            if ($scope.todosone.status == '' || $scope.todosone.status == null) {
                    $scope.validatestring = $scope.validatestring + $scope.selecttodostatus;
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
             else if ($scope.todosone.datetime == '' || $scope.todosone.datetime == null) {
                    $scope.validatestring = $scope.validatestring + $scope.selectfollowupdate;
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
            
              $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.one('todos', $routeParams.id).customPUT($scope.todosone).then(function (updateResp) {
                    
                    console.log('todos  Saved');
                         window.location = "/";
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.message = 'Todo has been Updated!';
        $scope.showValidation = false;
        $scope.showUniqueValidation = false;
        $scope.toggleValidation = function () {
            $scope.showUniqueValidation = !$scope.showUniqueValidation;
        };

     
    
    //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.dt1 = new Date();

        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        //        $scope.clear = function () {
        //            $scope.dt = null;
        //        };

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();
        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.todosone.followupopened = true;
        };

        $scope.conditionfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.todosone.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///
    

    });
