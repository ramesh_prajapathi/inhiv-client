'use strict';

angular.module('secondarySalesApp')
    .controller('NavbarCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $idle, $modal, $route, $rootScope, $filter) {

       // $window.sessionStorage.MemberFilter = '';
        Restangular.one('lhslanguages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').get().then(function (langResponse) {
            $scope.lhslanguage = langResponse[0];
            $scope.mainLogout = $scope.lhslanguage.logout;
            $scope.printonetoneheader = langResponse[0].onetone;
            // console.log('langResponse',langResponse);


        });

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
        });
    
        /******* Role access ***********/
        $scope.hideMain = true;
        $scope.hideGeneral = true; // admin
        $scope.hideMember = true; // admin
        $scope.hideMemberMain = true; // user
        $scope.hideFWHide = true; //admin
        $scope.hideFeildWorker = true; // user
        $scope.hideFWicon = true; // user
        $scope.hideMemberMenu = true;


        if ($window.sessionStorage.userId) {
            Restangular.one('roles', $window.sessionStorage.roleId).get().then(function (roleresp) {
                $scope.roles = roleresp;

                /*if ( roleresp.id != 1) {

                   if ($scope.roles.orwModule == false ) {
                        $scope.hideFWicon = false;
                        console.log('m in if');
                    } else if ($scope.roles.orwModule == true) {
                        $scope.hideFWicon = true;
                        console.log('m in else');
                    }
                    
                  else if ($scope.roles.memberModule == false) {
                        $scope.hideMemberMenu = false;
                        console.log('m in if');
                    } else if ($scope.roles.memberModule == true) {
                        $scope.hideMemberMenu = true;
                        console.log('m in else');
                    }
                }*/

            });

        }

        if ($window.sessionStorage.roleId == 1) {
            $scope.tittlename = 'Language';
            $scope.tittlename1 = 'Users';
            $scope.hideMain = true;
            $scope.hideGeneral = true;
            $scope.hideMember = true;
            $scope.hideMemberMenu = true;
            $scope.hideMemberMain = false;
            $scope.hideFWHide = true;
            $scope.hideFeildWorker = false;
            $scope.hideFWicon = true;
            $scope.myColor = '-10.6em';
            $scope.hiderolebased = false;

        } else if ($window.sessionStorage.roleId == 4) {
            $scope.tittlename = 'Language';
            $scope.tittlename1 = 'Users';
            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMemberMenu = true;
            $scope.hideMember = true;
            $scope.hideMemberMain = false;
            $scope.hideFWHide = false;
            $scope.hideFeildWorker = false;
            $scope.hideFWicon = false;
            $scope.myColor = '-7em';
            $scope.hiderolebased = false;
            
        } else if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 20) {
            //console.log(' $scope.roles', $scope.roles);

            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = false;
            $scope.hideFWHide = false;
            $scope.tittlename = 'Member';
            $scope.tittlename1 = 'ORW';
            $scope.hideFWicon = true; // user
            $scope.hideMemberMenu = true;
            $scope.hiderolebased = true;
            
        } else if ($window.sessionStorage.roleId == 13) {
            //console.log(' $scope.roles', $scope.roles);

            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = false;
            $scope.hideFWHide = false;
            $scope.tittlename = 'Member';
            $scope.tittlename1 = 'ORW';
            $scope.hideFWicon = false; // user
            $scope.hideMemberMenu = true;
            $scope.hiderolebased = true;
            
        } else if ($window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 18 || $window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 19) {
            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = false;
            $scope.hideFWHide = false;
            $scope.tittlename = 'Member';
            $scope.tittlename1 = 'ORW';
            $scope.hideFWicon = false; // user
            $scope.hideMemberMenu = true;
            $scope.hiderolebased = true;
            
        } else if ($window.sessionStorage.roleId == 8 || $window.sessionStorage.roleId == 9 || $window.sessionStorage.roleId == 10) {
            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = false;
            $scope.hideFWHide = false;
            $scope.tittlename = 'Member';
            $scope.tittlename1 = 'ORW';
            $scope.hideFWicon = false; // user
            $scope.hideMemberMenu = true;
            $scope.hiderolebased = false;
            
        } else if ($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17) {
            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = false;
            $scope.hideFWHide = false;
            $scope.tittlename = 'Member';
            $scope.tittlename1 = 'ORW';
            $scope.hideFWicon = false; // user
            $scope.hideMemberMenu = true;
            $scope.hiderolebased = false;
        }



        /** if ($window.sessionStorage.roleId == 1) {
             $scope.hideMain = true;
             $scope.hideGeneral = true;
             $scope.hideMember = true;
             $scope.hideMemberMain = false;
             $scope.hideFWHide = true;
             $scope.hideFeildWorker = false;
             $scope.myColor = '-13em';

         } else if ($window.sessionStorage.roleId == 4) {
             $scope.hideMain = true;
             $scope.hideGeneral = false;
             $scope.hideMember = true;
             $scope.hideMemberMain = false;
             $scope.hideFWHide = false;
             $scope.hideFeildWorker = false;
             $scope.myColor = '-7em';

         } else if ($window.sessionStorage.roleId == 2) {
             $scope.hideMain = true;
             $scope.hideGeneral = false;
             $scope.hideMember = false;
             $scope.hideMemberMain = true;
             $scope.hideFWHide = false;
             $scope.hideFeildWorker = true;
             $scope.assignfieldworkertosite = true;
             $scope.comanagerprofile = true;


         } else if ($window.sessionStorage.roleId == 13) {
             $scope.hideMain = true;
             $scope.hideGeneral = false;
             $scope.hideMember = false;
             $scope.hideMemberMain = true;
             $scope.hideFWHide = false;
             $scope.hideFeildWorker = false;
             $scope.assignfieldworkertosite = false;
             $scope.comanagerprofile = false;
         } **/


        /****   for sidebar menu icon change *****/
        $scope.imageHome = "images/piclinks/home-blue.png";
        $scope.imageGeneral = "images/piclinks/facility-grey.png";
        $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
        if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4) {
            $scope.imageFW = "images/piclinks/language-grey.png";
            $scope.imageFacility = "images/piclinks/adminicon_grey.png";
        } else {
            $scope.imageFW = "images/piclinks/members-grey.png";
        $scope.imageFacility = "images/piclinks/facility-grey.png";
        }


        $scope.colorHome = "#FFFFFF";
        $scope.colorGeneral = "#FFFFFF";
        $scope.colorFW = "#FFFFFF";
        $scope.colorFacility = "#FFFFFF";
        $scope.colorAnalytic = "#FFFFFF";

        $scope.borderHome = '4px solid #004891';
        $scope.borderGeneral = '4px solid #FFFFFF';
        $scope.borderFW = '4px solid #FFFFFF';
        $scope.borderFacility = '4px solid #FFFFFF';
        $scope.borderAnalytic = '4px solid #FFFFFF';



         $rootScope.clickHome = function () {
            $scope.imageHome = "images/piclinks/home-blue.png";
             $scope.imageGeneral = "images/piclinks/facility-grey.png"; 
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
            if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4) {
            $scope.imageFW = "images/piclinks/language-grey.png";
            $scope.imageFacility = "images/piclinks/adminicon_grey.png";
        } else {
            $scope.imageFW = "images/piclinks/members-grey.png";
        $scope.imageFacility = "images/piclinks/facility-grey.png";
        }

          

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorAnalytic = "#FFFFFF";

            $scope.borderHome = '4px solid #004891';
            $scope.borderGeneral = '4px solid #FFFFFF';
            $scope.borderFW = '4px solid #FFFFFF';
            $scope.borderFacility = '4px solid #FFFFFF';
            $scope.borderAnalytic = '4px solid #FFFFFF';
            $location.path("/");

        };


        $rootScope.clickGeneral = function () {

            $scope.imageHome = "images/piclinks/home-gray.png";
             $scope.imageGeneral = "images/piclinks/facility-blue.png"; 
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
             if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4) {
            $scope.imageFW = "images/piclinks/language-grey.png";
            $scope.imageFacility = "images/piclinks/adminicon_grey.png";
        } else {
            $scope.imageFW = "images/piclinks/members-grey.png";
         $scope.imageFacility = "images/piclinks/facility-grey.png";
        }
            

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorAnalytic = "#FFFFFF";
            
             $scope.borderHome = '4px solid #FFFFFF';
            $scope.borderGeneral = '4px solid #004891';
            $scope.borderFW = '4px solid #FFFFFF';
            $scope.borderFacility = '4px solid #FFFFFF';
            $scope.borderAnalytic = '4px solid #FFFFFF';
        };

        $rootScope.clickFW = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
             if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4) {
            $scope.imageFW = "images/piclinks/language-blue.png";
            $scope.imageFacility = "images/piclinks/adminicon_grey.png";
        } else {
            
            $scope.imageFW = "images/piclinks/members-blue.png";
         $scope.imageFacility = "images/piclinks/facility-grey.png";
        }
            
            // $scope.imageFW = "images/piclinks/multilanguagedark.png";
            // $scope.imageFacility = "images/piclinks/admindark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorAnalytic = "#FFFFFF";
            
             $scope.borderHome = '4px solid #FFFFFF';
            $scope.borderGeneral = '4px solid #FFFFFF';
            $scope.borderFW = '4px solid #004891';
            $scope.borderFacility = '4px solid #FFFFFF';
            $scope.borderAnalytic = '4px solid #FFFFFF';

        };

        $rootScope.clickFacility = function () {
           $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
             $scope.imageDashboard = "images/piclinks/anlytics-grey.png";
             if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4) {
            $scope.imageFW = "images/piclinks/language-grey.png";
            $scope.imageFacility = "images/piclinks/adminicon_blue.png";
        } else {
            $scope.imageFW = "images/piclinks/members-grey.png";
        $scope.imageFacility = "images/piclinks/facility-blue.png";
        }
           

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorAnalytic = "#FFFFFF";
            
             $scope.borderHome = '4px solid #FFFFFF';
            $scope.borderGeneral = '4px solid #FFFFFF';
            $scope.borderFW = '4px solid #FFFFFF';
            $scope.borderFacility = '4px solid #004891';
            $scope.borderAnalytic = '4px solid #FFFFFF';

        };

        $rootScope.clickanalytic = function () {
             $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facilitydark.png";
            $scope.imageDashboard = "images/piclinks/anlytics-blue.png";
             if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4) {
            $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFacility = "images/piclinks/admindark.png";
        } else {
            $scope.imageFW = "images/piclinks/members-grey.png";
       $scope.imageFacility = "images/piclinks/facility-grey.png";
        }
           

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorAnalytic = "#FFFFFF";
            
              $scope.borderHome = '4px solid #FFFFFF';
            $scope.borderGeneral = '4px solid #FFFFFF';
            $scope.borderFW = '4px solid #FFFFFF';
            $scope.borderFacility = '4px solid #FFFFFF';
            $scope.borderAnalytic = '4px solid #004891';

        };


        /* $scope.clickSPMBudget = function () {
             $scope.imageHome = "images/piclinks/homedark.png";
             $scope.imageGeneral = "images/piclinks/facilitydark.png";
             $scope.imageGeneral1 = "images/piclinks/facilitydark.png";
             $scope.imageFW = "images/piclinks/admindark.png";
             $scope.imageFacility = "images/piclinks/dashboarddark.png";
             $scope.imageDashboard = "images/piclinks/dashboarddark.png";
             $scope.imageSPMBudget = "images/piclinks/budgetwhite.png";
             $scope.imageNOBudget = "images/piclinks/budgetdark.png";

             $scope.colorHome = "#FFFFFF";
             $scope.colorGeneral = "#FFFFFF";
             $scope.colorFW = "#FFFFFF";
             $scope.colorFacility = "#FFFFFF";
             $scope.colorDashboard = "#FFFFFF";
             $scope.colorSPMBudget = "#e72e5a";
             $scope.colorNOBudget = "#FFFFFF";
             $location.path("/spmbudgets");

         };

         $scope.clickNOBudget = function () {
             $scope.imageHome = "images/piclinks/homedark.png";
             $scope.imageGeneral = "images/piclinks/facilitydark.png";
             $scope.imageFW = "images/piclinks/admindark.png";
             $scope.imageFacility = "images/piclinks/dashboarddark.png";
             $scope.imageDashboard = "images/piclinks/dashboarddark.png";
             $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
             $scope.imageNOBudget = "images/piclinks/budgetwhite.png";

             $scope.colorHome = "#FFFFFF";
             $scope.colorGeneral = "#FFFFFF";
             $scope.colorFW = "#FFFFFF";
             $scope.colorFacility = "#FFFFFF";
             $scope.colorDashboard = "#FFFFFF";
             $scope.colorSPMBudget = "#FFFFFF";
             $scope.colorNOBudget = "#e72e5a";
             $location.path("/nobudgets");

         };

         $rootScope.clickDashboard = function () {
             $scope.imageHome = "images/piclinks/homedark.png";
             $scope.imageGeneral = "images/piclinks/facilitydark.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
             $scope.imageFacility = "images/piclinks/dashboarddark.png";
             $scope.imageDashboard = "images/piclinks/dashboardwhite.png";
             $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
             $scope.imageNOBudget = "images/piclinks/budgetdark.png";

             $scope.colorHome = "#FFFFFF";
             $scope.colorGeneral = "#FFFFFF";
             $scope.colorFW = "#FFFFFF";
             $scope.colorFacility = "#FFFFFF";
             $scope.colorDashboard = "#e72e5a";
             $scope.colorSPMBudget = "#FFFFFF";
             $scope.colorNOBudget = "#FFFFFF";

             if ($scope.roles.dashboardfacility) {
                 $location.path("/codashboard");
             } else if ($scope.roles.nobudget) {
                 $location.path("/nodashboard");
             } else if ($scope.roles.rodashboard) {
                 $location.path("/rodashboard");
             } else if ($scope.roles.sodashboard) {
                 $location.path("/spmdashboard");
             } else if ($scope.roles.fsmentordashboard) {
                 $location.path("/fsmentordashboard");
             }

         };*/

        /***** $scope.clickGeneral = function () {

             $scope.imageHome = "images/piclinks/homedark.png";
             $scope.imageGeneral = "images/piclinks/facilitydark.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
             $scope.imageFacility = "images/piclinks/admindark.png";

             $scope.colorHome = "#FFFFFF";
             $scope.colorGeneral = "#e72e5a";
             $scope.colorFW = "#FFFFFF";
             $scope.colorFacility = "#FFFFFF";

         };

         $scope.clickFW = function () {
             $scope.imageHome = "images/piclinks/homedark.png";
             $scope.imageGeneral = "images/piclinks/facilitywhite.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
             $scope.imageFacility = "images/piclinks/admindark.png";

             $scope.colorHome = "#FFFFFF";
             $scope.colorGeneral = "#FFFFFF";
             $scope.colorFW = "#e72e5a";
             $scope.colorFacility = "#FFFFFF";

         };**/

        /* $scope.clickFW11 = function () {
             $scope.imageHome = "images/piclinks/homedark.png";
             $scope.imageGeneral = "images/piclinks/facilitydark.png";
             $scope.imageGeneral1 = "images/piclinks/facilitydark.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
             $scope.imageFW1 = "images/piclinks/admindark.png";
             $scope.imageFacility = "images/piclinks/dashboarddark.png";
             $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
             $scope.imageDashboard = "images/piclinks/dashboarddark.png";
             $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
             $scope.imageNOBudget = "images/piclinks/budgetdark.png";

             $scope.colorHome = "#FFFFFF";
             $scope.colorGeneral = "#FFFFFF";
             $scope.colorFW = "#FFFFFF";
             $scope.colorFW1 = "#e72e5a";
             $scope.colorFacility = "#FFFFFF";
             $scope.colorFacility1 = "#FFFFFF";
             $scope.colorDashboard = "#FFFFFF";
             $scope.colorSPMBudget = "#FFFFFF";
             $scope.colorNOBudget = "#FFFFFF";
         };*/



        /*$scope.clickFacility1 = function () {
          
            $scope.imageHome = "images/piclinks/homedark.png";
            $scope.imageGeneral = "images/piclinks/facilitydark.png";
            $scope.imageGeneral1 = "images/piclinks/facilitydark.png";
            $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFW1 = "images/piclinks/admindark.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFW1 = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorFacility1 = "#e72e5a";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";
        };*/
        /**** Above code  for sidebar menu icon change *****/

        Restangular.one('users?filter[where][username]=' + $window.sessionStorage.userName).get().then(function (user) {
            $scope.user = user[0];
        });

        $scope.rolesconfiguration = true;
        $scope.rolesconfiguration1 = true;


         if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4 || $window.sessionStorage.roleId == 8 || $window.sessionStorage.roleId == 9 || $window.sessionStorage.roleId == 10 || $window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17) {
            $scope.rolesconfiguration = false;
        } else {
            $scope.rolesconfiguration1 = false;
        }
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.KnowledgeBaseName = "Knowledge Base";
        if ($scope.UserLanguage == 1) {
            $scope.KnowledgeBaseName = "Knowledge Base";
        } else if ($scope.UserLanguage == 2) {
            $scope.KnowledgeBaseName = "ज्ञानधार";
        } else if ($scope.UserLanguage == 3) {
            $scope.KnowledgeBaseName = "ಜ್ಞಾನದ ತಳಹದಿ";
        } else if ($scope.UserLanguage == 4) {
            $scope.KnowledgeBaseName = "அறிவு சார்ந்த";
        } else if ($scope.UserLanguage == 5) {
            $scope.KnowledgeBaseName = "నాలెడ్జ్ బేస్";
        } else if ($scope.UserLanguage == 6) {
            $scope.KnowledgeBaseName = "पायाभूत माहिती";
        }

        /**************************************** Member *******************************************/
        $scope.someFocusVariable = true;
        $scope.FocusMe = true;
        $scope.filterFields = ['fullname', 'tiId', 'phonenumber'];
        $scope.searchInput = '';

        if ($window.sessionStorage.roleId == 2 || $window.sessionStorage.roleId == 20) {
            $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;
                });
            });
        } else if ($window.sessionStorage.roleId == 13){

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                //console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });
                });
            });
        } else if ($window.sessionStorage.roleId == 14 || $window.sessionStorage.roleId == 15 || $window.sessionStorage.roleId == 9) {

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                //console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"state":{"inq":[' + fw.state + ']}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });

                });
            });
        } else if ($window.sessionStorage.roleId == 16 || $window.sessionStorage.roleId == 17 || $window.sessionStorage.roleId == 10) {

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                //console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"district":{"inq":[' + fw.district + ']}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });

                });
            });
        } else if ($window.sessionStorage.roleId == 8) {

            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                //console.log('fw', fw);
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"countryId":{"inq":[' + fw.countryId + ']}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });

                });
            });
        }

        /***********************************************************************/




        $scope.searchbulkupdate = '';



        $scope.openOnetoOne = function () {
            $window.sessionStorage.MemberFilter = '';
            $rootScope.fullname = '';
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/OnetooOne.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okOnetooOne = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/onetoone/" + fullname);
            $route.reload();
        };

        $scope.cancelOnetooOne = function () {
            $scope.modalInstance1.close();
        };

        $scope.loading = true;
        //$scope.LoadingMOdal = false;
        $scope.toggleLoading = function () {
            $scope.modalInstanceLoad = $modal.open({
                animation: true,
                templateUrl: 'template/LodingModal.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm'

            });
            //$scope.LoadingMOdal = !$scope.LoadingMOdal;
        };



        $scope.openMainReportIncident = function () {
            $scope.modalMainReport = $modal.open({
                animation: true,
                templateUrl: 'template/MainReportIncident.html',
                scope: $scope,
                backdrop: 'static'

            });

        };

        $scope.okMainReportIncident = function (fullname) {
            $scope.modalMainReport.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okReportIncident', $rootScope.fullname);
            $location.path("/reportincident");
            $route.reload();
        };

        $scope.cancelMainReportIncident = function () {
            $scope.modalMainReport.close();
        };
        /***********************************************/
        $scope.openApplyforScheme = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/ApplyforScheme.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okApplyforScheme = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyforscheme");
            $route.reload();
        };

        $scope.cancelApplyforScheme = function () {
            $scope.modalInstance1.close();
        };

        $scope.openApplyforDocument = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/ApplyforDocument.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okApplyforDocument = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyfordocument");
            $route.reload();
        };

        $scope.cancelApplyforDocument = function () {
            $scope.modalInstance1.close();
        };


        $scope.openfPlaning = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/fPlaning.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okfPlaning = function (fullname) {
            $scope.modalInstance1.close();
            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.fullname', $rootScope.fullname);
            $location.path("/financialPlanning");
            $route.reload();
        };

        $scope.cancelfPlaning = function () {
            $scope.modalInstance1.close();
        };
        /****************************************/

        $scope.menu = [{
                'title': 'Home',
                'link': '/'
    },
            {
                'title': 'States',
                'link': '/states'
    },
            {
                'title': 'Contact',
                'link': '#'
    }];

        $scope.fwprofileedit = true;
        $scope.coprofileedit = true;
        if ($window.sessionStorage.roleId == 5) {
            $scope.coprofileedit = false;
            $scope.UserId = $window.sessionStorage.UserEmployeeId;
        } else {
            //$scope.UserId = $window.sessionStorage.UserEmployeeId;
            $scope.fwprofileedit = false;
            $scope.UserId = $window.sessionStorage.UserEmployeeId;
        }

        //  console.log($window.sessionStorage.userId + 'sddsf');
        $scope.showNav = function () {
            //console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
            if ($window.sessionStorage.userId) {
                return true;
            }
        };
        $scope.isActive = function (route) {
            return route === $location.path();
        };

        $scope.getClass = function (path) {
            if ($location.path().substr(0, path.length) === path) {
                return 'active';
            } else {
                return '';
            }
        }

        $scope.logout = function () {
            //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
            Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
                $window.sessionStorage.userId = '';
                console.log('Logout');
            }).then(function (redirect) {

                $location.path("/login");
                $idle.unwatch();

            });
        };

/*overdue count display*/
    
     var value = 'middle';

        $scope.gotoElement = function () {
            if ($location.path() == '/') {
                $scope.$broadcast('totaltoDo', value);
                $window.sessionStorage.todoClick = '';
            } else if ($location.path() != '/') {
                $window.sessionStorage.todoClick = 'clicked';
                $scope.$broadcast('totaltoDo', value);
                $location.path('/');
            }
    
        };
    
    $scope.todoCount = 0;
    $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    //console.log("$scope.todayDate",$scope.todayDate);
    
         
    if ($window.sessionStorage.roleId + "" === "2" || $window.sessionStorage.roleId + "" === "20") {

            $scope.todoCall1 = 'todos?filter[where][deleteflag]=false' + '&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][status]=8' + '&filter[where][facility]=' + $window.sessionStorage.coorgId;
        
            $scope.todoCall2 = 'conditionheaders?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][caseClosed]=false';

        } else if($window.sessionStorage.roleId + "" === "13") {

            $scope.todoCall1 = 'todos?filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId  + '&filter[where][status]=8';
            
           
        } else if ($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18"){
                    
            $scope.todoCall1 ='todos?filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId ;
    
        } else if ($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19") {
        
            $scope.todoCall1 = 'todos?filter[where][deleteflag]=false&filter[where][roleId]=' + $window.sessionStorage.roleId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][facility]=' + $window.sessionStorage.coorgId ;
            
        }
         

     Restangular.all($scope.todoCall1).getList().then(function (resp) {

            $scope.todoArray =  $filter('orderBy')(resp, 'datetime');
            console.log("$scope.todoArray",$scope.todoArray.length);

            $scope.overDueCount = 0;

            $scope.todoArrayCount = 0;

            if ($scope.todoArray.length == 0) {
                //console.log("$scope.todoArray.length",$scope.todoArray.length);
                $scope.condOdFunc();
                
            } else {
               
                angular.forEach($scope.todoArray, function (data, index) {
                    data.index = index;

                    if (data.datetime == null || data.datetime == '') {
                        //console.log("im date null",$scope.todoArray.length);
                        $scope.todoArrayCount++;
                    } else {
                        var date1 = new Date();
                        var date2 = new Date(data.datetime);
                        var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
                        //console.log("diff days",diffDays);
                        
                        if (diffDays < 0) {
                            $scope.overDueCount++;
                            $scope.todoArrayCount++;
                            //console.log("$scope.overDueCount if <",$scope.overDueCount);
                           //$scope.todoCount = $scope.overDueCount;
                        } else {
                            $scope.todoArrayCount++;
                             //$scope.todoCount = $scope.overDueCount;
                            //console.log("$scope.overDueCount else",$scope.overDueCount);
                        }
                 }
                    
                    if ($scope.todoArrayCount == $scope.todoArray.length) {
                       $scope.condOdFunc();
                    }
                });
            }
        });

       $scope.condOdFunc = function () {
           //console.log("im from condition", $scope.todoCall2);
           if($window.sessionStorage.roleId + "" === "13"){
               Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                 //console.log(fw);
               
               Restangular.all('conditionheaders?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (resp3) {
                //Restangular.all($scope.todoCall2).getList().then(function (resp3) {
                   
                 
               
                $scope.condArray = $filter('orderBy')(resp3, 'followupdate');
               // console.log("$scope.condArray", $scope.condArray.length);
                var conArrayCount = 0;
                   // console.log("resp3", resp3);

                if ($scope.condArray.length == 0) {
                   // console.log("$scope.condArray.length == 0");
                     $scope.todoCount = $scope.overDueCount;
                    // console.log("$scope.todoArrayCount", $scope.todoCount);
                    return 
                } else {

                    angular.forEach($scope.condArray, function (data, index) {

                        data.index = index;
                        
                        if (data.followupdate == null || data.followupdate == '') {
                            conArrayCount++;
                           
                        } else {
                            var date1 = new Date();
                            var date2 = new Date(data.followupdate);
                            var diffDays1 = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
                            //console.log("else diffDays1", diffDays1);
                            if (diffDays1 < 0) {
                                $scope.overDueCount++;
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log("$scope.overDueCount if condition",$scope.overDueCount);
                                //console.log(" todoCount",  $scope.todoCount);
                            } else {
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log(" sec func $scope.todoCount",$scope.todoCount);
                            }
                        }
                        
                    });
                }
            });
               }); 
           }
            else if($window.sessionStorage.roleId + "" === "11" || $window.sessionStorage.roleId + "" === "18"){
               
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
               Restangular.all('conditionheaders?filter={"where":{"and":[{"ictcId":{"inq":[' + fw.ictcId + ']}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (resp3) {
                //Restangular.all($scope.todoCall2).getList().then(function (resp3) {
                //console.log("resp3", resp3);
                $scope.condArray = $filter('orderBy')(resp3, 'followupdate');
                //console.log("$scope.condArray", $scope.condArray.length);
                var conArrayCount = 0;

                if ($scope.condArray.length == 0) {
                     $scope.todoCount = $scope.overDueCount;
                    return 
                } else {

                    angular.forEach($scope.condArray, function (data, index) {

                        data.index = index;
                        
                        if (data.followupdate == null || data.followupdate == '') {
                            conArrayCount++;
                           
                        } else {
                            var date1 = new Date();
                            var date2 = new Date(data.followupdate);
                            var diffDays1 = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
                        //console.log("else diffDays1",  diffDays1);
                            if (diffDays1 < 0) {
                                $scope.overDueCount++;
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log("$scope.overDueCount if condition",$scope.overDueCount);
                                //console.log(" todoCount",  $scope.todoCount);
                            } else {
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log(" sec func $scope.todoCount",$scope.todoCount);
                            }
                        }
                        
                    });
                }
            });
                });
           }
           else if($window.sessionStorage.roleId + "" === "12" || $window.sessionStorage.roleId + "" === "19"){
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
               
               Restangular.all('conditionheaders?filter={"where":{"and":[{"artId":{"inq":[' + fw.artId + ']}},{"caseClosed":{"inq":[false]}}]}}').getList().then(function (resp3) {
                //Restangular.all($scope.todoCall2).getList().then(function (resp3) {
                //console.log("resp3", resp3);
                $scope.condArray = $filter('orderBy')(resp3, 'followupdate');
                //console.log("$scope.condArray", $scope.condArray.length);
                var conArrayCount = 0;

                if ($scope.condArray.length == 0) {
                     $scope.todoCount = $scope.overDueCount;
                    return 
                } else {

                    angular.forEach($scope.condArray, function (data, index) {

                        data.index = index;
                        
                        if (data.followupdate == null || data.followupdate == '') {
                            conArrayCount++;
                           
                        } else {
                            var date1 = new Date();
                            var date2 = new Date(data.followupdate);
                            var diffDays1 = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
                            //console.log("else diffDays1",  diffDays1);
                            if (diffDays1 < 0) {
                                $scope.overDueCount++;
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log("$scope.overDueCount if condition",$scope.overDueCount);
                                //console.log(" todoCount",  $scope.todoCount);
                            } else {
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log(" sec func $scope.todoCount",$scope.todoCount);
                            }
                        }
                        
                    });
                }
            });
                });
               
           }
           else {

            Restangular.all($scope.todoCall2).getList().then(function (resp3) {
                //console.log("resp3", resp3);
                $scope.condArray = $filter('orderBy')(resp3, 'followupdate');
                //console.log("$scope.condArray", $scope.condArray.length);
                var conArrayCount = 0;

                if ($scope.condArray.length == 0) {
                    return 
                } else {

                    angular.forEach($scope.condArray, function (data, index) {

                        data.index = index;
                        
                        if (data.followupdate == null || data.followupdate == '') {
                            conArrayCount++;
                           
                        } else {
                            var date1 = new Date();
                            var date2 = new Date(data.followupdate);
                            var diffDays1 = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
                            //console.log("else diffDays1",  diffDays1);
                            if (diffDays1 < 0) {
                                $scope.overDueCount++;
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log("$scope.overDueCount if condition",$scope.overDueCount);
                                //console.log(" todoCount",  $scope.todoCount);
                            } else {
                                conArrayCount++;
                                 $scope.todoCount = $scope.overDueCount;
                                //console.log(" sec func $scope.todoCount",$scope.todoCount);
                            }
                        }
                        
                    });
                }
            });
           }
        };
    
   
        /*************************** Language *******************************/
        /*$scope.languages = Restangular.all('languages').getList().$object;
        $scope.multiLang = Restangular.one('multilanguages?filter[where][id]=' + $window.sessionStorage.language).get().then(function (langResponse) {

            $rootScope.style = {
                "font-size": langResponse[0].fontsize,
                "font-weight": "normal"
            }

            $scope.printonetoneheader = langResponse[0].onetoneheader;
            $scope.applyforscheme = langResponse[0].applyforscheme;
            $scope.applyfordocument = langResponse[0].applyfordocument;
            $scope.bulkupdateheader = langResponse[0].bulkupdateheader;
            $scope.eventheader = langResponse[0].eventheader;
            $scope.headergroupmeeting = langResponse[0].headergroupmeeting;
            $scope.stakeholdermeeting = langResponse[0].stakeholdermeeting;
            $scope.printheaderreportincident = langResponse[0].headerreportincident;
            $scope.printmembername = langResponse[0].membername;
            $scope.nickname = langResponse[0].nickname;
            $scope.facility = langResponse[0].facility;
            $scope.fieldworkerth = langResponse[0].fieldworkerth;
            $scope.eventheader = langResponse[0].eventheader;
            $scope.comanagerhdfprofileheader = langResponse[0].comanagerhdfprofileheader;
            $scope.site = langResponse[0].site;
            $scope.home = langResponse[0].home;
            $scope.fieldwork = langResponse[0].fieldwork;
            $scope.members = langResponse[0].members;
            $scope.hotspot = langResponse[0].hotspot;
            $scope.trackapplication = langResponse[0].trackapplication;
            $scope.archivedmembers = langResponse[0].archivedmembers;
            $scope.assignfwtosite = langResponse[0].assignfwtosite;
            $scope.dashboard = langResponse[0].dashboard;
            $scope.facilityprofileheader = langResponse[0].facilityprofileheader;
            $scope.searchfor = langResponse[0].searchfor;
            $scope.savebutton = langResponse[0].savebutton;
            $scope.update = langResponse[0].update;
            $scope.cancel = langResponse[0].cancel;
            $scope.create = langResponse[0].create;
            $scope.printprofile = langResponse[0].profile;
            $scope.printsignout = langResponse[0].logout;
            $scope.financialplanning = langResponse[0].financialplanning;
            $scope.pcobudget = langResponse[0].cobudget;


            $scope.groupmeetingslabel = langResponse[0].groupmeetingslabel;
            $scope.stakeholdermeetingslabel = langResponse[0].stakeholdermeetingslabel;
            $scope.eventslabel = langResponse[0].eventslabel;

        });



*/
    });
