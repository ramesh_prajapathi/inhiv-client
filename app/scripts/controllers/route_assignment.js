'use strict';

angular.module('secondarySalesApp')
  .controller('RouteassignmentCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $http, $timeout,$filter,$route) {
      
      
        $scope.showForm = function(){
          var visible = $location.path() === '/route_assignment/create' || $location.path() === '/route_assignment';
          return visible;
        };
    
        $scope.isCreateView = function(){
          if($scope.showForm()){
            var visible = $location.path() === '/route_assignment';
            return visible;
          }
        };
       
        $scope.poheader = {
            sample: ''
          };
      
      
      
       $scope.tod = function() {
            $scope.date =  $filter('date')(new Date(), 'y-MM-dd');
           console.log('date format',$scope.date);
           
           
          };
     
      $scope.tod();
       $scope.routeassignment = {
            partnerId: ''
          };
        $scope.$watch('routeassignment.partnerId', function(newValue, oldValue){
          console.log('newValue: ' + newValue);
          console.log('oldValue: ' + oldValue);
            /////////////////////////////////////////////////////////
            $scope.item = [{
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          },{
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          },{
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          },{
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          },{
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          },{
            date: '',
            partnerId:'',
            distributionRouteId: '',
            tourtype: ''
          }]
      
            
            /////////////////////////////////////////////////////////
          $scope.routeassigns = Restangular.all('routeassignments?filter[where][partnerId]='+newValue).getList().$object;
            
          $scope.routes = Restangular.all('routelinkviews?filter[where][partnerid]='+newValue+'&filter[where][flag]=null').getList().$object;
            
            console.log('today', $scope.date);
            $scope.item[0].partnerId = newValue;
            $scope.item[1].partnerId = newValue;
            $scope.item[2].partnerId = newValue;
            $scope.item[3].partnerId = newValue;
            $scope.item[4].partnerId = newValue;
            $scope.item[5].partnerId = newValue;
        
          /*$http.get(baseUrl + '/partners/findOne?filter[where][id]='+newValue).success(function(partner) {
		        console.log('partners', partner);
		     // $scope.routeassignment.salesManager = partner.salesManagerId;
             // $scope.routeassignment.partnerManager = partner.partnerManagerCode;
             
            $scope.salesManager = Restangular.one('employees', partner.salesManagerId).get().$object;
            $scope.partnerManager = Restangular.one('partners', partner.partnerManagerCode).get().$object;
		      //console.log("partners", partner.firstName, "$scope.retailerdetail.name", $scope.retailerdetail.name);
		      });*/
            
             Restangular.one('employees', newValue).get().then(function(partner){
          $scope.salesManager = Restangular.one('employees', partner.salesManagerId).get().$object;
            $scope.partnerManager = Restangular.one('partners', partner.partnerManagerCode).get().$object;
        });
        
          Restangular.one('routeassignments', $routeParams.id).get().then(function(poheader) {
            $scope.original = poheader;
            $scope.poheader = Restangular.copy($scope.original);
            $scope.poheader.sample = Restangular.all('routeassignments?filter[where][partnerId]='+newValue+'&filter[where][date][gte]='+$scope.date).getList().$object;
               
             /* $http.get(baseUrl + '/routeassignments?filter[where][partnerId]='+newValue+'&filter[where][date][gte]='+$scope.date).success(function(routeassignment) {
		          for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.item[i].id = routeassignment[i].id;
                  $scope.item[i].partnerId = routeassignment[i].partnerId;
                  $scope.item[i].date = routeassignment[i].date;
                  $scope.item[i].distributionRouteId = routeassignment[i].distributionRouteId;
                  $scope.item[i].tourtype = routeassignment[i].tourtype;
              }
		        });*/
             
             // $scope.poheader.sample = Restangular.all('posamples').getList().$object;
            $scope.poheaders = Restangular.all('routeassignments').getList().$object;
          });
        });
        $scope.routelinks = Restangular.all('routelinks').getList().$object;
        //$scope.partners = Restangular.all('employees?filter={"where":{"groupId":{"inq":[1,4,5,6,7]}}}').getList().$object;
    
        $scope.partners = Restangular.all('employees').getList().$object;
    
        $scope.getPartner = function(partnerId){
          return Restangular.one('employees', partnerId).get().$object;
        };
    
        $scope.employee = Restangular.all('employees').getList().$object;
        $scope.getemployee = function(employeeId){
          return Restangular.one('employees', employeeId).get().$object;
        };
     
        $scope.partner = Restangular.all('partners').getList().$object;
        $scope.tourtypes = Restangular.all('tourtypes').getList().$object;
        $scope.getpartner = function(partnerId){
          return Restangular.one('partners', partnerId).get().$object;
        };
      
      $scope.$watch('item[0].date', function(newValue, oldValue){
          
          /*$http.get(baseUrl + '/routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).success(function(routeassignment) {
            $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[0].date='';
                 }
              }
          });*/
          console.log('Date',newValue);
          console.log('Date',JSON.stringify(newValue));
          
          
           Restangular.one('routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).get().then(function(routeassignment){
          $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[0].date='';
                 }
              }
        });
          
      });
      
      $scope.$watch('item[1].date', function(newValue, oldValue){
          
         /* $http.get(baseUrl + '/routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).success(function(routeassignment) {
            $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[1].date='';
                 }
              }
          });*/
          
           Restangular.one('routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).get().then(function(routeassignment){
          $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[1].date='';
                 }
              }
        });
          
      });
      
       $scope.$watch('item[2].date', function(newValue, oldValue){
          
          /*$http.get(baseUrl + '/routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).success(function(routeassignment) {
            $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[2].date='';
                 }
              }
          });*/
           
            Restangular.one('routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).get().then(function(routeassignment){
          $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[2].date='';
                 }
              }
        });
          
      });
      
      $scope.$watch('item[3].date', function(newValue, oldValue){
          
          /*$http.get(baseUrl + '/routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).success(function(routeassignment) {
            $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[3].date='';
                 }
              }
          });*/
          
           Restangular.one('routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).get().then(function(routeassignment){
          $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[3].date='';
                 }
              }
        });
          
      });
      
      $scope.$watch('item[4].date', function(newValue, oldValue){
          
          /*$http.get(baseUrl + '/routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).success(function(routeassignment) {
            $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[4].date='';
                 }
              }
          });*/
          
           Restangular.one('routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).get().then(function(routeassignment){
          $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[4].date='';
                 }
              }
        });
          
      });
      
      $scope.$watch('item[5].date', function(newValue, oldValue){
          
         /* $http.get(baseUrl + '/routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).success(function(routeassignment) {
            $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[5].date='';
                 }
              }
          });*/
          
           Restangular.one('routeassignments?filter[where][partnerId]='+$scope.routeassignment.partnerId).get().then(function(routeassignment){
          $scope.routedate =  $filter('date')(newValue, 'yyyy-MM-dd');
              for(var i =0,len ;i<routeassignment.length;i++){
                  $scope.ite = routeassignment[i].date;
                  $scope.assignroutedate =  $filter('date')($scope.ite, 'yyyy-MM-dd');
                 if($scope.assignroutedate==$scope.routedate){
                     alert('Already Exists');
                     $scope.item[5].date='';
                 }
              }
        });
          
      });
     
     
     /* Restangular.one('routeassignments', 'routeassignment.partnerId').get().then(function(routeassignment) {
            $scope.routeassignment.salesManager = Restangular.one('employees', partner.salesManagerId).getList().$object;
            $scope.routeassignment.partnerManager = Restangular.one('partners', partner.partnerManagerCode).getList().$object;
           
        });*/
      
      
      
      //Datepicker settings start
        $scope.today = function() {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
          };
        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function() {
            $scope.showWeeks = !$scope.showWeeks;
          };

        $scope.clear = function() {
            $scope.dt = null;
          };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return (mode === 'day' && (date.getDay() === 0));
          };

        $scope.toggleMin = function() {
            $scope.minDate = ($scope.minDate) ? null : new Date();
          };
        $scope.toggleMin();

        $scope.open = function($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function() {
                $('#datepicker' + index).focus();
              });
            $scope.opened = true;
          };
      
      $scope.open2 = function($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function() {
                $('#datepicker' + index).focus();
              });
            $scope.opened2 = true;
          };
      
      $scope.open3 = function($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function() {
                $('#datepicker' + index).focus();
              });
            $scope.opened3 = true;
          };
      
      $scope.open4 = function($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function() {
                $('#datepicker' + index).focus();
              });
            $scope.opened4 = true;
          };
      
      $scope.open5 = function($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function() {
                $('#datepicker' + index).focus();
              });
            $scope.opened5 = true;
          };
      
      $scope.open6 = function($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function() {
                $('#datepicker' + index).focus();
              });
            $scope.opened6 = true;
          };

        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
          };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end
      
     
     
        $scope.distributionRoute = Restangular.all('distribution-routes').getList().$object;
      
        $scope.getdistributionRoute = function(distributionRouteId){
          return Restangular.one('distribution-routes', distributionRouteId).get().$object;
        };
    
    
        Restangular.one('partners', $routeParams.id).get().then(function(partner){
          $scope.salesManager = Restangular.one('employees', partner.salesManagerId).getList().$object;
          $scope.partnerManager = Restangular.one('partners', partner.partnerManagerCode).getList().$object;
			    $scope.original = partner;
			    $scope.partner = Restangular.copy($scope.original);
        });
      /*********/
        $scope.addRouteLinkItem = function() {
            $scope.opened = '';
            $scope.poheader.sample.push({
              'partnerId': $scope.routeassignment.partnerId
            });
          };
        

       /* $scope.removeRouteLinkItem = function(index) {
            var item = $scope.poheader.sample[index];
            $scope.poheader.sample.splice(index, 1);
            console.log('rem obj', JSON.stringify(item));
            if (item && item.id !== undefined) {
              console.log('existing');
              Restangular.one('routeassignments', item.id).get().then(function(purchaseorder) {
                    purchaseorder.remove();
                  });
            } else {
              console.log('does not exist');
            }
          };*/

        $scope.addRouteLinks = function(value) {
            console.log('length', $scope.item.length);
            for(var i=0;i<$scope.item.length;i++){
                if($scope.item[i].date != ''&& $scope.item[i].partnerId!='' && $scope.item[i].tourtype!=''){
                    /*if ($scope.item[i].id!='') {
                  Restangular.one('routeassignments/'+$scope.item[i].id).customPUT($scope.item[i]);
                        console.log('routeassignments updated', $scope.item[i]);
                } else {*/
                    //console.log('$scope.item[i]',$scope.item[i]);
                   $scope.routeassignments.post($scope.item[i]).then(function(){
                console.log('routeassignments saved', $scope.item[i]);
                    });
                //}
                   
            }
            }
           
           /* angular.forEach($scope.poheader.sample, function(value, key) {
                console.log(key + ': ' + JSON.stringify(value));
                console.log('$scope.poheader.sample[' + key + ']: ', $scope.poheader.sample[key]);
                if (value.id) {
                  Restangular.all('routeassignments').one(value.id).customPUT($scope.poheader.sample[key]);
                } else {
                  $scope.routeassignments.post($scope.poheader.sample[key]);
                }
                $route.reload();
              });*/
            $route.reload();
          };
      
      

        $scope.routeassignments = Restangular.all('routeassignments').getList().$object;
      
        $scope.getrouteassignment = function(partnerId){
          return Restangular.one('routeassignments', partnerId).get().$object;
        };
    
        if($routeParams.id){
          Restangular.one('routeassignments',$routeParams.id).get().then(function(routeassignment){
				    $scope.original = routeassignment;
				    $scope.routeassignment = Restangular.copy($scope.original);
          });
        }
        $scope.date1 = function()
{
          $scope.greeting =$scope.username;
          var d = new Date();
          var weekday=new Array(7);
          weekday[0]='Sunday';
          weekday[1]='Monday';
          weekday[2]='Tuesday';
          weekday[3]='Wednesday';
          weekday[4]='Thursday';
          weekday[5]='Friday';
          weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
        };
	
	
        $scope.username = 'World';

        $scope.sayHello = function() {
          $scope.greeting = ' ' + $scope.username + '';
	        var d = new Date($scope.greeting);
	        var weekday=new Array(7);
          weekday[0]='Sunday';
	        weekday[1]='Monday';
	        weekday[2]='Tuesday';
	        weekday[3]='Wednesday';
	        weekday[4]='Thursday';
	        weekday[5]='Friday';
	        weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
	        $scope.day = (weekday[d.getDay()]);
        };
	
	      $scope.username1 = 'World';
        $scope.sayHello1 = function() {
          $scope.greeting1 = ' ' + $scope.username1 + '';
	        var d = new Date($scope.greeting1);
	        var weekday=new Array(7);
	        weekday[0]='Sunday';
	        weekday[1]='Monday';
	        weekday[2]='Tuesday';
          weekday[3]='Wednesday';
          weekday[4]='Thursday';
          weekday[5]='Friday';
          weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
	        $scope.day1 = (weekday[d.getDay()]);
        };
	
	
	
	      $scope.username2 = 'World';
	      $scope.sayHello2 = function() {
          $scope.greeting2 = ' ' + $scope.username2 + '';
	        var d = new Date($scope.greeting2);
	        var weekday=new Array(7);
	        weekday[0]='Sunday';
	        weekday[1]='Monday';
	        weekday[2]='Tuesday';
	        weekday[3]='Wednesday';
	        weekday[4]='Thursday';
	        weekday[5]='Friday';
	        weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
	        $scope.day2 = (weekday[d.getDay()]);
        };
	
	
	      $scope.username3 = 'World';
	      $scope.sayHello3 = function() {
          $scope.greeting3 = ' ' + $scope.username3 + '';
	        var d = new Date($scope.greeting3);
	        var weekday=new Array(7);
	        weekday[0]='Sunday';
	        weekday[1]='Monday';
	        weekday[2]='Tuesday';
	        weekday[3]='Wednesday';
	        weekday[4]='Thursday';
	        weekday[5]='Friday';
	        weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
	        $scope.day3 = (weekday[d.getDay()]);
        };
	
	
	      $scope.username4 = 'World';
	      $scope.sayHello4 = function() {
          $scope.greeting4 = ' ' + $scope.username4 + '';
	        var d = new Date($scope.greeting4);
	        var weekday=new Array(7);
	        weekday[0]='Sunday';
	        weekday[1]='Monday';
	        weekday[2]='Tuesday';
	        weekday[3]='Wednesday';
	        weekday[4]='Thursday';
	        weekday[5]='Friday';
	        weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
	        $scope.day4 = (weekday[d.getDay()]);
        };
	
	
	      $scope.username5 = 'World';
	      $scope.sayHello5 = function() {
          $scope.greeting5 = ' ' + $scope.username5 + '';
	        var d = new Date($scope.greeting5);
	        var weekday=new Array(7);
	        weekday[0]='Sunday';
	        weekday[1]='Monday';
	        weekday[2]='Tuesday';
	        weekday[3]='Wednesday';
	        weekday[4]='Thursday';
	        weekday[5]='Friday';
	        weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
	        $scope.day5 = (weekday[d.getDay()]);
        };
	
	
	      $scope.username6 = 'World';
	      $scope.sayHello6 = function() {
          $scope.greeting6 = ' ' + $scope.username6 + '';
	        var d = new Date($scope.greeting6);
	        var weekday=new Array(7);
	        weekday[0]='Sunday';
	        weekday[1]='Monday';
	        weekday[2]='Tuesday';
	        weekday[3]='Wednesday';
	        weekday[4]='Thursday';
	        weekday[5]='Friday';
	        weekday[6]='Saturday';


//alert(weekday[d.getDay()]);
	        $scope.day6 = (weekday[d.getDay()]);
        };
	

      });

