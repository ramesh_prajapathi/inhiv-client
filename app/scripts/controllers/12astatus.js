'use strict';

angular.module('secondarySalesApp')
    .controller('12AStatusCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/12astatus/create' || $location.path() === '/12astatus/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/12astatus/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/genders/create' || $location.path() === '/12astatus/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/12astatus/create' || $location.path() === '/12astatus/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/12astatus") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Status has been Updated!';
            Restangular.one('employeestatuses', $routeParams.id).get().then(function (employeestatus) {
                $scope.original = employeestatus;
                $scope.employeestatus = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Status has been Created!';
        }
        $scope.Search = $scope.name;

        /******************************** INDEX *******************************************/
        $scope.zn = Restangular.all('employeestatuses?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.employeestatuses = zn;
            angular.forEach($scope.employeestatuses, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/
        $scope.employeestatus = {
            "name": '',
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.employeestatus.name == '' || $scope.employeestatus.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.hnname == '' || $scope.employeestatus.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.knname == '' || $scope.employeestatus.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.taname == '' || $scope.employeestatus.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.tename == '' || $scope.employeestatus.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.mrname == '' || $scope.employeestatus.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.employeestatuses.post($scope.employeestatus).then(function () {
                    window.location = '/12astatus';
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.employeestatus.name == '' || $scope.employeestatus.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.hnname == '' || $scope.employeestatus.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.knname == '' || $scope.employeestatus.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.taname == '' || $scope.employeestatus.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.tename == '' || $scope.employeestatus.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.employeestatus.mrname == '' || $scope.employeestatus.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                Restangular.one('employeestatuses', $routeParams.id).customPUT($scope.employeestatus).then(function () {
                    console.log('Status Saved');
                    window.location = '/12astatus';
                });
            }
        };
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('employeestatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
