'use strict';
angular.module('secondarySalesApp').controller('BnCreateCtrl', function ($scope, Restangular, $filter, $timeout, $window, $route, $modal) {
        

        $scope.UserLanguage = $window.sessionStorage.language;
    

        Restangular.one('memberlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.memberLang = langResponse[0];
            //$scope.modalInstanceLoad.close();
            $scope.memberHeading = $scope.memberLang.memberregistration;
            $scope.modalTitle1 = $scope.memberLang.areYouSureToWantToSave;
            $scope.message = $scope.memberLang.memberSaved;
            $scope.enteringchildage = $scope.memberLang.enterchildage;
            $scope.enteryourfullname = $scope.memberLang.peyfullname;
            $scope.enteryournickname = $scope.memberLang.enternickname;
            $scope.selectsite = $scope.memberLang.selsite;
            $scope.selecttypology = $scope.memberLang.seltypology;
            $scope.selectage = $scope.memberLang.selage;
            $scope.selectgender = $scope.memberLang.selgender;
            $scope.Selectdob = $scope.memberLang.Seldob;
            $scope.enteringhotspot = $scope.memberLang.enterhotspot;
            $scope.selectmaritalstatus = $scope.memberLang.selmaritalstatus;
            $scope.selectsubtypology = $scope.memberLang.selsubtypology;
            $scope.enterphonenum = $scope.memberLang.enterphonenumber;
            $scope.selectnativestate = $scope.memberLang.selnativestate;
            $scope.selectnativedistrict = $scope.memberLang.selnativedistrict;
            $scope.selectregularpartner = $scope.memberLang.selregularpartner;
            $scope.selectvocation = $scope.memberLang.selvocation;
            $scope.enternoofsexyear = $scope.memberLang.entersexyear;
            $scope.selchildrendob = $scope.memberLang.selchilddob;

        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }

        $scope.hidePrevData = true;

        $scope.validationStateId = $window.sessionStorage.zoneId;
        $scope.isCreateView = true;
        $scope.isCreateEdit = true;
        $scope.isCreateSave = true;
        $scope.hideremove = false;
        $scope.DisableSubmit = false;
        $scope.ShowText = false;
        $scope.ShowStart = true;
        $scope.disableapprove = true;
        $scope.hotspotprint = true;
        $scope.createClicked = false;
        $scope.detailsHide = false;
        $scope.createmodifyDisable = true;
        $scope.detailsHide2 = false;
        $scope.showMoreDetails2 = false;
        $scope.hideavahanid = true;
        $scope.actpaid = false;
        $scope.familydeletebuttonhide = true;
        $scope.paralegalproposed_yes = true;
        $scope.boardmemberhide = true;
        $scope.phonetype_disabled = false;
        $scope.disableVocation = true;
        $scope.disablefullname = false;

        $scope.alertFlag = false;
        /*******full Name validation*********/
        $scope.Validname = function ($event, value) {
            var arr = $scope.beneficiary.fullname.split(/\s+/);
            console.log('arr.length', arr.length);
            if (arr.length == 0 || arr.length == 1) {
                setTimeout(function () {
                    // alert($scope.HHLanguage.invalidName);
                    $scope.alertFlag = true;
                    //  alert('Invalid Full Name');
                }, 1000);
                document.getElementById('name').style.borderColor = "#FF0000";

            } else {
                $scope.alertFlag = false;
                document.getElementById('name').style.borderColor = "";
                // $scope.duplicateHHFind(value);
            }
        };

        $scope.beneficiary = {
            facility: $window.sessionStorage.coorgId,
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            createddatetime: new Date(),
            district: $window.sessionStorage.salesAreaId,
            countryId: $window.sessionStorage.countryId,
            state: $window.sessionStorage.zoneId,
            town: $window.sessionStorage.distributionAreaId,
            completedflag: false,
            deleteflag: false
        };

        $scope.auditlog = {};

        /**************** Disable feild in mobility Status ****************/
        $scope.disableNoofdaysWD = true;
        $scope.disablerReasonOfMoveWD = true;
        $scope.disableNoofdaysWS = true;
        $scope.disablerReasonOfMoveWS = true;
        $scope.disableNoofdaysOS = true;
        $scope.disablerReasonOfMoveOS = true;

        $scope.ValidNumberOne = function (value) {
            // var arr = $scope.beneficiary.noofTimeDistrict;
            console.log('value', value);

            if (value > 0) {
                $scope.disableNoofdaysWD = false;
                $scope.disablerReasonOfMoveWD = false;

            } else {
                $scope.disableNoofdaysWD = true;
                $scope.disablerReasonOfMoveWD = true;
                $scope.beneficiary.noofDayDistrict = '';
                $scope.beneficiary.reasonofMovingDistrict = '';
                
            }
        };

        $scope.ValidNumberTwo = function (value) {

            if (value > 0) {
                $scope.disableNoofdaysWS = false;
                $scope.disablerReasonOfMoveWS = false;

            } else {
                $scope.disableNoofdaysWS = true;
                $scope.disablerReasonOfMoveWS = true;
                 $scope.beneficiary.noofDayState = '';
                $scope.beneficiary.reasonofMovingState = '';

            }
        };

        $scope.ValidNumberThree = function (value) {

            if (value > 0) {
                $scope.disableNoofdaysOS = false;
                $scope.disablerReasonOfMoveOS = false;

            } else {
                $scope.disableNoofdaysOS = true;
                $scope.disablerReasonOfMoveOS = true;
                 $scope.beneficiary.noofDayOutState = '';
                $scope.beneficiary.reasonofMovingOutState = '';
            }
        };


        Restangular.all('maritalstatuses?filter[order]=orderno%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
            $scope.maritalstatuses = mt;

            var data14 = $scope.maritalstatuses.filter(function (arr) {
                return arr.defaultValue == 'Yes'
            })[0];

            console.log('data14', data14);

            if ($scope.UserLanguage == 1) {
                // $scope.beneficiary.maritalstatus = mt[0].id;
                if (data14 != undefined) {
                    $scope.beneficiary.maritalstatus = data14.id;
                }
            } else {
                // $scope.beneficiary.maritalstatus = mt[0].parentId;
                if (data14 != undefined) {
                    $scope.beneficiary.maritalstatus = data14.parentId;
                }
            }
        });




        $scope.studyingin = Restangular.all('educations?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;



        $scope.reasonofmoves = Restangular.all('nativestates?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (ZoneResp) {
            $scope.zones = ZoneResp;
        });

        $scope.salesareas = Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (SalesResp) {
            $scope.salesareas = SalesResp;
        });
        $scope.towns = Restangular.all('cities?filter[where][district]=' + $window.sessionStorage.salesAreaId).getList().$object;

        Restangular.all('regularpartners?filter[order]=orderno%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rp) {
            $scope.regularpartners = rp;

            /* var data141 = $scope.regularpartners.filter(function (arr) {
                 return arr.defaultValue == 'Yes'
             })[0];

             console.log('data141', data141);

             if ($scope.UserLanguage == 1) {
                 // $scope.beneficiary.maritalstatus = mt[0].id;
                 if (data141 != undefined) {
                     $scope.beneficiary.regularpartnerId = data141.id;
                 }
             } else {
                 // $scope.beneficiary.maritalstatus = mt[0].parentId;
                 if (data141 != undefined) {
                     $scope.beneficiary.regularpartnerId = data141.parentId;
                 }
             }*/
        });



        Restangular.all('vocationpartners?filter[order]=orderno%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (vp) {
            $scope.vocationpartners = vp;

            /*  var data1411 = $scope.vocationpartners.filter(function (arr) {
                  return arr.defaultValue == 'Yes'
              })[0];

              console.log('data1411', data1411);

              if ($scope.UserLanguage == 1) {
                  // $scope.beneficiary.maritalstatus = mt[0].id;
                  if (data1411 != undefined) {
                      $scope.beneficiary.vocationPartnerId = data1411.id;
                  }
              } else {
                  // $scope.beneficiary.maritalstatus = mt[0].parentId;
                  if (data1411 != undefined) {
                      $scope.beneficiary.vocationPartnerId = data1411.parentId;
                  }
              }*/
        });



        Restangular.all('hotspots?filter[where][countryId]=' + $window.sessionStorage.countryId + '&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (resp) {
            $scope.hotspots = resp;
            console.log('$scope.hotspots', $scope.hotspots);
        });

        Restangular.all('genders?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (gen) {
            $scope.genderdisplays = gen;

            if ($scope.UserLanguage == 1) {
                $scope.familymembergender = gen[0].id;
            } else {
                $scope.familymembergender = gen[0].parentId;
            }

        });

        Restangular.all('typologies?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (respTypo) {
            $scope.typologies = respTypo;


            if ($scope.UserLanguage == 1) {
                $scope.beneficiary.typology = respTypo[0].id;
            } else {
                $scope.beneficiary.typology = respTypo[0].parentId;
            }
        });


        $scope.beneficiary.typology = '';
        $scope.$watch('beneficiary.typology', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                $scope.DisableGender = true;
               

                Restangular.all('subtypologies?filter[where][typologyId]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Subresp) {
                    console.log('Subresp', Subresp);
                    $scope.subtypologies = Subresp;

                    if ($scope.UserLanguage == 1) {
                        $scope.beneficiary.subTypologyId = Subresp[0].id;
                    } else {
                        $scope.beneficiary.subTypologyId = Subresp[0].parentId;
                    }
                    // $scope.beneficiary.subTypologyId = $scope.beneficiary.subTypologyId;

                });

                Restangular.one('typologies', newValue).get().then(function (respTypo) {
                    $scope.genderId = respTypo.genderId;
                    $scope.typology11 = respTypo.code.charAt(0);

                    //                    $scope.beneficiary.tiId = $scope.SCode + '' + $scope.DCode + '' + $scope.TCode + '' + $scope.TiCode + '' + $scope.typology11 + '' + $scope.seqSize + $scope.meberCount;

                    $scope.beneiTiId = $scope.SCode + '' + $scope.DCode + '' + $scope.TCode + '' + $scope.TiCode + '' + $scope.typology11;

                    $scope.beneficiary.tiId = $scope.SCode + '' + $scope.DCode + '' + $scope.TCode + '' + $scope.TiCode + '' + $scope.typology11 + '' + $scope.seqSize + $scope.meberCount;

                    Restangular.all('genders?filter[where][id]=' + respTypo.genderId + '&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (genderResp) {
                        console.log('genderResp', genderResp);
                        $scope.genders = genderResp;


                        if ($scope.UserLanguage == 1) {
                            $scope.beneficiary.gender = genderResp[0].id;
                        } else {
                            $scope.beneficiary.gender = genderResp[0].parentId;
                        }
                        //$scope.beneficiary.gender = $scope.beneficiary.gender;

                    });
                });

            }
        });

        $scope.beneficiary.regularpartnerId = '';
        $scope.$watch('beneficiary.regularpartnerId', function (newValue, oldValue) {
            if (newValue == 42) {
                $scope.disableVocation = false;
            } else {
                $scope.beneficiary.vocationPartnerId = '';
                $scope.disableVocation = true;
            }
        });




        $scope.auditlog = {
            description: 'Member Create',
            modifiedbyroleid: $window.sessionStorage.roleId,
            modifiedby: $window.sessionStorage.UserEmployeeId,
            lastmodifiedtime: new Date(),
            entityroleid: 55,
            state: $window.sessionStorage.zoneId,
            district: $window.sessionStorage.salesAreaId,
            facility: $window.sessionStorage.coorgId
        };
        $scope.MemCount = {};
        $scope.reportincident = {};
        $scope.beneficiary.dynamicmember = false;
        $scope.beneficiary.plhiv = false;
        $scope.beneficiary.paidmember = false;
        $scope.beneficiary.physicallydisabled = false;
        $scope.beneficiary.mentallydisabled = false;
        $scope.beneficiary.championapproved = false;
        $scope.beneficiary.championproposed = false;
        $scope.beneficiary.paralegalproposed = false;
        $scope.beneficiary.paralegaltrained = false;
        $scope.beneficiary.boardmember = false;
        $scope.beneficiary.paralegalhasplv = false;


        $scope.DisableGender = false;



        $scope.fwsitehide = true;
        $scope.cosite = true;

        $scope.stateupdate = {};

        Restangular.one('zones', $window.sessionStorage.zoneId).get().then(function (zone) {
            $scope.userstateId = zone.id;
            $scope.zoneName = zone.name;
            $scope.beneficiary.state = zone.id;
            //$scope.meberCount = zone.membercount;
            $scope.beneficiary.nativeStateId = zone.id;
            $scope.beneficiary.nativeStateId = $scope.beneficiary.nativeStateId;

            var zoneCode = zone.code;
            if (zoneCode.length == 2) {
                $scope.SCode = zoneCode;
                // console.log(' i m If ', $scope.SCode);
            } else {
                var newCode = zoneCode.charAt(0) + zoneCode.charAt(1);
                $scope.SCode = newCode;
                // console.log(' I m else', $scope.SCode);
            }



            Restangular.one('sales-areas', $window.sessionStorage.salesAreaId).get().then(function (salesResp) {
                $scope.beneficiary.nativeDistrictId = salesResp.id;
                $scope.beneficiary.nativeDistrictId = $scope.beneficiary.nativeDistrictId;

                var districtCode = salesResp.districtCode;
                if (districtCode.length == 2) {
                    $scope.DCode = districtCode;
                    // console.log(' i m If ', $scope.DCode);
                } else {
                    var newCode1 = districtCode.charAt(0) + districtCode.charAt(1);
                    $scope.DCode = newCode1;
                    //console.log(' I m else', $scope.DCode);
                }

            });

            Restangular.one('cities', $window.sessionStorage.distributionAreaId).get().then(function (cityResp) {

                var TCode = cityResp.townCode;
                if (TCode.length <= 3) {
                    $scope.TCode = TCode;
                    // console.log(' i m If ', $scope.TCode);
                } else {
                    var newCode12 = TCode.charAt(0) + TCode.charAt(1) + TCode.charAt(2);
                    $scope.TCode = newCode12;
                    // console.log(' I m else', $scope.TCode);
                }

            });

            if ($window.sessionStorage.roleId == 2) {
                $scope.beneficiary.lastmodifiedbyrole = 2;
                $scope.disableapprove = false;
                $scope.fwsitehide = true;
                $scope.cosite = false;
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                    console.log('comember', comember);
                    $scope.beneficiary.facilityId = comember.id;
                    $scope.beneficiary.artId = comember.artId;
                    $scope.beneficiary.ictcid = comember.ictcId;
                    $scope.auditlog.facilityId = comember.id;
                    Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                        console.log('employee', employee);

                        /*******New Chnages**********/
                        $scope.empUpdateId = employee.id;
                        $scope.meberCount = employee.membercount;

                        var num = employee.membercount;
                        var str = num.toString();
                        var len = str.length;

                        if (len == 1) {
                            $scope.seqSize = '000';
                        } else if (len == 2) {
                            $scope.seqSize = '00';
                        } else if (len == 3) {
                            $scope.seqSize = '0';
                        } else if (len == 4) {
                            $scope.seqSize = '';
                            // console.log('$scope.seqSize', $scope.seqSize);
                        } else {
                            $scope.seqSize = '';
                            // console.log('$scope.seqSize', $scope.seqSize);
                        }
                        $scope.stateupdate.membercount = employee.membercount + 1;

                        /*******New Chnages**********/


                        var typologyid = employee.typologyId.split(",")[0];

                        Restangular.one('typologies', $scope.beneficiary.typology).get().then(function (typo) {
                            $scope.typology11 = typo.code.charAt(0);
                            $scope.facilityName = employee.firstName;


                            var TiCode = employee.salesCode;
                            if (TiCode.length <= 3) {
                                $scope.TiCode = TiCode;
                                // console.log(' i m If ', $scope.TCode);
                            } else {
                                var TICodeNew = TiCode.charAt(0) + TiCode.charAt(1) + TiCode.charAt(2);
                                $scope.TiCode = TICodeNew;
                                // console.log(' I m else', $scope.TiCode);
                            }
                            $scope.sites = Restangular.all('distribution-routes?filter[where][tiId]=' + employee.id + '&filter[where][deleteflag]=false').getList().$object;
                            // console.log('$scope.sites', $scope.sites);



                            $scope.beneiTiId = $scope.SCode + '' + $scope.DCode + '' + $scope.TCode + '' + $scope.TiCode + '' + $scope.typology11;

                            $scope.beneficiary.tiId = $scope.SCode + '' + $scope.DCode + '' + $scope.TCode + '' + $scope.TiCode + '' + $scope.typology11 + '' + $scope.seqSize + $scope.meberCount;
                        });
                    });

                    $scope.modalInstanceLoad.close();

                    Restangular.all('subtypologies?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Subresp) {
                        $scope.subtypologies = Subresp;
                        $scope.beneficiary.subTypologyId = $scope.beneficiary.subTypologyId;

                        Restangular.all('genders?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (genderResp) {
                            $scope.genders = genderResp;
                            $scope.beneficiary.gender = $scope.beneficiary.gender;

                        });

                    });

                    $scope.$watch('beneficiary.site', function (newValue, oldValue) {
                        if (newValue === oldValue || newValue == '' || newValue == null) {
                            return;
                        } else {
                            Restangular.all('routelinks?filter[where][distributionRouteId]=' + newValue).getList().then(function (routeResponse) {

                                //console.log('routeResponse', routeResponse);
                                Restangular.one('comembers', routeResponse[0].partnerId).get().then(function (fwget) {

                                    // console.log('fwget', fwget);

                                    $scope.beneficiary.fieldworkername = fwget.lastname;
                                    $scope.beneficiary.fieldworker = fwget.id;
                                });
                            });
                        }
                    });

                });



            } else {
                console.log('feildworker');
                $scope.beneficiary.lastmodifiedbyrole = 13;
                $scope.fwsitehide = false;
                $scope.cosite = true;
                $scope.disableapprove = true;
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    $scope.getsiteid = fw.id;
                    $scope.beneficiary.fieldworkername = fw.lastname;
                    $scope.beneficiary.fieldworker = fw.id;
                    Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                        $scope.beneficiary.facilityId = comember.id;
                        $scope.auditlog.facilityId = comember.id;
                        $scope.beneficiary.artId = comember.artId;
                        $scope.beneficiary.ictcid = comember.ictcId;
                        Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {

                            /*******New Chnages**********/
                            $scope.meberCount = employee.membercount;
                            $scope.empUpdateId = employee.id;
                            var num = employee.membercount;
                            var str = num.toString();
                            var len = str.length;

                            if (len == 1) {
                                $scope.seqSize = '000';
                            } else if (len == 2) {
                                $scope.seqSize = '00';
                            } else if (len == 3) {
                                $scope.seqSize = '0';
                            } else if (len == 4) {
                                $scope.seqSize = '';
                                // console.log('$scope.seqSize', $scope.seqSize);
                            } else {
                                $scope.seqSize = '';
                                // console.log('$scope.seqSize', $scope.seqSize);
                            }
                            $scope.stateupdate.membercount = employee.membercount + 1;

                            /*******New Chnages**********/


                            $scope.facilityName = employee.firstName;

                            var typologyid = employee.typologyId.split(",")[0];

                            Restangular.one('typologies', $scope.beneficiary.typology).get().then(function (typo) {
                                $scope.typology11 = typo.code.charAt(0);



                                var TiCode = employee.salesCode;
                                if (TiCode.length <= 3) {
                                    $scope.TiCode = TiCode;
                                    console.log(' i m If ', $scope.TCode);
                                } else {
                                    var TICodeNew = TiCode.charAt(0) + TiCode.charAt(1) + TiCode.charAt(2);
                                    $scope.TiCode = TICodeNew;
                                    console.log(' I m else', $scope.TiCode);
                                }

                                $scope.beneiTiId = $scope.SCode + '' + $scope.DCode + '' + $scope.TCode + '' + $scope.TiCode + '' + $scope.typology11;

                                $scope.beneficiary.tiId = $scope.SCode + '' + $scope.DCode + '' + $scope.TCode + '' + $scope.TiCode + '' + $scope.typology11 + '' + $scope.seqSize + $scope.meberCount;
                                //beneiTiId

                                Restangular.all('routelinks?filter[where][partnerId]=' + $scope.getsiteid).getList().then(function (routeResponse) {
                                    $scope.routelinksget = routeResponse;
                                    console.log('routeResponse', routeResponse);
                                    angular.forEach($scope.routelinksget, function (member, index) {
                                        member.index = index + 1;
                                        member.routeName = Restangular.one('distribution-routes', member.distributionRouteId).get().$object;
                                    });
                                });
                            });
                        });

                        Restangular.all('subtypologies?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Subresp) {
                            $scope.subtypologies = Subresp;
                            $scope.beneficiary.subTypologyId = $scope.beneficiary.subTypologyId;

                            Restangular.all('genders?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (genderResp) {
                                $scope.genders = genderResp;
                                $scope.beneficiary.gender = $scope.beneficiary.gender;

                            });

                        });
                        $scope.modalInstanceLoad.close();
                    });
                });


            }
        });




        $scope.UpdateZone = {
            membercount: 0
        };
        $scope.beneficiarydataModal = false;
        $scope.submitcount = 0;
        $scope.validatestring = '';
        $scope.no = {};
        $scope.newhotspot = {};
        $scope.newhotspot.facility = $window.sessionStorage.coorgId;
        $scope.newhotspot.state = $window.sessionStorage.zoneId;
        $scope.newhotspot.district = $window.sessionStorage.salesAreaId;
        $scope.newhotspot.countryId = $window.sessionStorage.countryId;
        $scope.newhotspot.deleteflag = false;

        $scope.SaveProfile = function () {

            if ($scope.beneficiary.hotspot != $scope.no.Noresponse) {
                $scope.beneficiary.hotspot = $scope.no.Noresponse;
                $scope.newhotspot.name = $scope.no.Noresponse;
                Restangular.all('hotspots').post($scope.newhotspot).then(function (hotspot) {});
            }

            if ($scope.beneficiary.fullname == '' || $scope.beneficiary.fullname == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteryourfullname;
            } else if ($scope.beneficiary.fullname.indexOf(' ') == -1) {
                $scope.validatestring = $scope.validatestring + $scope.enteryourfullname;
                //$scope.beneficiary.fullname = null;
            } else if ($scope.alertFlag == true) {
                $scope.validatestring = $scope.validatestring + 'Invalid Full Name';
                //$scope.beneficiary.fullname = null;
            } else if ($scope.beneficiary.nickname == '' || $scope.beneficiary.nickname == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteryournickname;
                //$scope.beneficiary.fullname = null;
            } else if ($scope.beneficiary.site == '' || $scope.beneficiary.site == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectsite;
            } else if ($scope.beneficiary.hotspot == '' || $scope.beneficiary.hotspot == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteringhotspot;
            }
            //            else if ($scope.beneficiary.facility == '' || $scope.beneficiary.facility == null) {
            //                $scope.validatestring = $scope.validatestring + $scope.selfacility;
            //            } 
            else if ($scope.beneficiary.typology == '' || $scope.beneficiary.typology == null) {
                $scope.validatestring = $scope.validatestring + $scope.selecttypology;
            } else if ($scope.beneficiary.gender == '' || $scope.beneficiary.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectgender;
            } else if ($scope.beneficiary.subTypologyId == '' || $scope.beneficiary.subTypologyId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectsubtypology;
            } else if ($scope.beneficiary.maritalstatus == '' || $scope.beneficiary.maritalstatus == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectmaritalstatus;
            } else if ($scope.beneficiary.age == '' || $scope.beneficiary.age == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectage;
            } else if ($scope.beneficiary.dob == '' || $scope.beneficiary.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.Selectdob;
            } else if ($scope.beneficiary.phonenumber == '' || $scope.beneficiary.phonenumber == null) {
                $scope.validatestring = $scope.validatestring + $scope.enterphonenum;
            }  else if ($scope.beneficiary.phonenumber.length !=10) {
                $scope.validatestring = $scope.validatestring + $scope.enterphonenum;
            } 
            else if ($scope.beneficiary.nativeStateId == '' || $scope.beneficiary.nativeStateId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectnativestate;
            } else if ($scope.beneficiary.nativeDistrictId == '' || $scope.beneficiary.nativeDistrictId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectnativedistrict;
            } else if ($scope.beneficiary.regularpartnerId == '' || $scope.beneficiary.regularpartnerId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectregularpartner;
            }
            //            else if ($scope.beneficiary.vocationPartnerId == '' || $scope.beneficiary.vocationPartnerId == null) {
            //                $scope.validatestring = $scope.validatestring + $scope.selectvocation;
            //            } 
            else if ($scope.beneficiary.noofSexYear == '' || $scope.beneficiary.noofSexYear == null) {
                $scope.validatestring = $scope.validatestring + $scope.enternoofsexyear;
            }

            /**else if ($scope.beneficiary.typology == 2 || $scope.beneficiary.gender == 2) {
                $scope.SaveFamilymember();
            }**/
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {
                $scope.SaveFamilymember();

            }
        };


        $scope.callModal = function () {
            $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
        }

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };
        $scope.openOneAlert = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true,
                templateUrl: 'template/AlertModal.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
            });
        };
        $scope.okAlert = function () {
            $scope.modalOneAlert.close();
        };
        $scope.dobcount = 0;
        $scope.$watch('beneficiary.dob', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                $scope.dobcount++;
                $scope.today = new Date();
                $scope.birthyear = newValue;
                var ynew = $scope.today.getFullYear();
                var mnew = $scope.today.getMonth();
                var dnew = $scope.today.getDate();
                var yold = $scope.birthyear.getFullYear();
                var mold = $scope.birthyear.getMonth();
                var dold = $scope.birthyear.getDate();
                var diff = ynew - yold;
                if (mold > mnew) diff--;
                else {
                    if (mold == mnew) {
                        if (dold > dnew) diff--;
                    }
                }
                $scope.beneficiary.age = diff;
            }
        });
        /********************************* FamilyMember Change **************/
        $scope.goforsave = false;
        $scope.goforsavevalidate = false;
        $scope.SaveFamilymember = function () {
            console.log('I m SaveFamilymember');
            // if ($scope.FamilyGenderChanges == true) {
            /** for (var h = 0; h < $scope.familymembers.length; h++) {

                 if ($scope.familymembers[h].gender < !1 || $scope.familymembers[h].gender == '' || $scope.familymembers[h].gender == undefined) {
                     $scope.validatestring = $scope.enterchildgender;
                 } else if ($scope.familymembers[h].age < !1 || $scope.familymembers[h].age == '' || $scope.familymembers[h].age == undefined) {
                     //alert('Enter Children Age');
                     $scope.validatestring = $scope.enteringchildage; //$scope.enterchildage;
                     //$scope.goforsave = false;
                 } else if ($scope.familymembers[h].dob == '' || $scope.familymembers[h].dob == undefined || $scope.familymembers[h].dob == null) {
                     //alert('Select Children DOB');
                     $scope.validatestring = $scope.selchildrendob; 
                     $scope.goforsave = false;
                 } else {
                     $scope.goforsave = true;
                 }
             }**/

            //}
            $scope.goforsave = true;
            if ($scope.goforsave == true) {
                // $scope.createClicked = true;

                // console.log('$scope.currentmemberId', $scope.currentmemberId);

                //  console.log('$scope.beneficiary', $scope.beneficiary);

                Restangular.one('distribution-routes', $scope.beneficiary.site).get().then(function (siteget) {
                    $scope.siteName = siteget.name;
                });
//                Restangular.one('fieldworkers', $scope.beneficiary.fieldworker).get().then(function (fwget) {
//                    $scope.printfieldworkerName = fwget.firstname; //+ ' ' + fwget.lastname;
//                });
                Restangular.one('comembers', $scope.beneficiary.fieldworker).get().then(function (fwget) {
                    $scope.printfieldworkerName = fwget.firstname; //+ ' ' + fwget.lastname;
                });

                Restangular.one('genders', $scope.beneficiary.gender).get().then(function (genderget) {
                    $scope.genderName = genderget;
                });
                Restangular.one('maritalstatuses', $scope.beneficiary.maritalstatus).get().then(function (maritalstatusget) {
                    $scope.maritalstatusName = maritalstatusget;
                });
                Restangular.one('typologies', $scope.beneficiary.typology).get().then(function (typ) {
                    $scope.typologyName = typ;
                });
                Restangular.one('subtypologies', $scope.beneficiary.subTypologyId).get().then(function (subtyp) {
                    $scope.subtypologyName = subtyp;
                });

                Restangular.one('sales-areas', $scope.beneficiary.nativeDistrictId).get().then(function (nDist) {
                    $scope.nativeDistrict = nDist.name;
                });

                Restangular.one('zones', $scope.beneficiary.nativeStateId).get().then(function (nState) {
                    $scope.nativeState = nState.name;
                });


                Restangular.one('regularpartners', $scope.beneficiary.regularpartnerId).get().then(function (regPart) {
                    $scope.regularpatrnerDisaply = regPart.name;
                    $scope.beneficiarydataModal = true;
                });

                /*  Restangular.one('vocationpartners', $scope.beneficiary.vocationPartnerId).get().then(function (vocPart) {
                      $scope.vocationpatrnerDisaply = vocPart.name;
                     
                  });*/

                $scope.TiIdDisplay = $scope.beneficiary.tiId;

            }
        };

        $scope.OKBUTTON = function () {

            Restangular.all('beneficiaries').post($scope.beneficiary).then(function (saveResponse) {
                console.log('saveResponse2', saveResponse);
                $scope.CurrentMemberId = saveResponse.id;

                Restangular.one('employees', $scope.empUpdateId).customPUT($scope.stateupdate).then(function (res) {
                    $scope.testResultSave($scope.CurrentMemberId);

                });
            });
            //console.log(' $scope.familymembers.length',  $scope.familymembers.length);

        };



        $scope.TestCount = 0;
        $scope.testResultSave = function (CurrentMemberId) {

            if ($scope.TestCount < $scope.familymembers.length) {
                if ($scope.familymembers[$scope.TestCount].gender == '') {
                    $scope.TestCount++;
                    $scope.testResultSave(CurrentMemberId);

                } else {

                    $scope.familymembers[$scope.TestCount].beneficiaryid = CurrentMemberId;
                    /**************new Added******/
                    Restangular.all('familymembers').post($scope.familymembers[$scope.TestCount]).then(function (childResp) {
                        console.log('family', childResp);
                        $scope.TestCount++;
                        $scope.testResultSave(CurrentMemberId);
                    });

                }

            } else {
                $scope.saveAuditTrail(CurrentMemberId);
            }
        };

        $scope.auidtCount = 0;
        $scope.auditArray = [];


        $scope.saveAuditTrail = function (CurrentMemberId) {
            if ($scope.auidtCount < $scope.auditArray.length) {

                $scope.auditArray[$scope.auidtCount].entityid = CurrentMemberId;
                $scope.auditArray[$scope.auidtCount].description = 'Member Create';
                $scope.auditArray[$scope.auidtCount].modifiedbyroleid = $window.sessionStorage.roleId;
                $scope.auditArray[$scope.auidtCount].modifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.auditArray[$scope.auidtCount].lastmodifiedtime = new Date();
                $scope.auditArray[$scope.auidtCount].state = $window.sessionStorage.zoneId;
                $scope.auditArray[$scope.auidtCount].district = $window.sessionStorage.salesAreaId;
                $scope.auditArray[$scope.auidtCount].facility = $window.sessionStorage.coorgId;
                $scope.auditArray[$scope.auidtCount].facilityId = $scope.auditlog.facilityId;

                Restangular.all('auditlogs').post($scope.auditArray[$scope.auidtCount]).then(function (auditresp) {
                    $scope.auidtCount++;
                    $scope.saveAuditTrail(CurrentMemberId);
                });
            } else {
                $scope.modalInstanceLoad.close();

                console.log('reloading...');
                $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                $timeout(function () {
                    window.location = '/members';
                }, 400);
            }
        };


        /***************************/




        $scope.CLOSEBUTTON = function () {
            $scope.beneficiarydataModal = false;
        };

        $scope.FamilyGenderChanges = false;

        $scope.CheckGender = function (agee, scope, index, famId) {
            console.log('agee', agee);
            console.log('index', index);
            $scope.FamilyGenderChanges = true;
            $scope.familymembers[index].age = '';
             $scope.familymembers[index].dob = '';
            if (scope.familymember.gender >= 1 && scope.familymember.Age != undefined && scope.familymember.Age != '') {
                return;
            } else if (agee <= 0 || agee == '') {
                //$scope.AlertMessage = 'Enter Children Age';
                //$scope.openOneAlert();
            }
        }
        $scope.FamilyAgeChanges = false;
        $scope.ChilredAge = function (scope, index) {
            $scope.FamilyAgeChanges = true;
            if (scope.familymember.age >= 0 && scope.familymember.age <= 99) {
                if ($scope.dobcount == 0) {
                    var todaydate = new Date();
                    var newdate = new Date(todaydate);
                    newdate.setFullYear(newdate.getFullYear() - scope.familymember.age);
                    var nd = new Date(newdate);
                    $scope.familymembers[index].dob = nd;
                } else {
                    $scope.dobcount = 0;
                }
            } else {
                //alert('Invalid Age');
                $scope.AlertMessage = 'Invalid Age'; //'Select Date Of Birth';
                $scope.openOneAlert();
                $scope.familymember[index].age = null;
                $scope.familymember[index].dob = null;
            }
        };
        $scope.FamilyDOBChanges = false;
        $scope.ChilredDOB = function (scope, index) {
            //console.log('scope', scope);
            $scope.FamilyDOBChanges = true;
            $scope.dobcount++;
            $scope.today = new Date();
            $scope.birthyear = scope.familymember.dob;
            var ynew = $scope.today.getFullYear();
            var mnew = $scope.today.getMonth();
            var dnew = $scope.today.getDate();
            var yold = $scope.birthyear.getFullYear();
            var mold = $scope.birthyear.getMonth();
            var dold = $scope.birthyear.getDate();
            var diff = ynew - yold;
            if (mold > mnew) diff--;
            else {
                if (mold == mnew) {
                    if (dold > dnew) diff--;
                }
            }
            $scope.familymembers[index].age = diff;
        }
        $scope.FamilyStudyChanges = false;
        $scope.CheckStudyIn = function (idstd) {
            $scope.FamilyStudyChanges = true;
        }

        $scope.familymembers = [];
        $scope.familymembers = [{
            age: '',
            gender: $scope.familymembergender,
            Stuyding: null,
            dob: null,
            beneficiaryid: 0,
            deleteflag: false
  }];

        $scope.addFamilyMember = function (id, index) {

            $scope.familymembers.push({
                age: '',
                gender: '',
                Stuyding: null,
                dob: null,
                beneficiaryid: 0,
                deleteflag: false
            });
            $scope.totalfamilymembercount = $scope.familymembers.length;
        };
        $scope.totalfamilymembercount = $scope.familymembers.length;

        $scope.removeFamilyMember = function (index) {
            var item = $scope.familymembers[index];
            if (index == 0) {
                //alert('Can not Delete..!');
                $scope.AlertMessage = $scope.cannot + ' ' + $scope.deletebutton; //'Select Date Of Birth';
                $scope.openOneAlert();
            } else {
                $scope.familymembers.splice(index, 1);
            }
        };
        /**********************************************/
        $scope.$watch('beneficiary.age', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue >= 0 && newValue <= 99) {
                    if ($scope.dobcount == 0) {
                        var todaydate = new Date();
                        var newdate = new Date(todaydate);
                        newdate.setFullYear(newdate.getFullYear() - newValue);
                        var nd = new Date(newdate);
                        $scope.beneficiary.dob = nd;
                    } else {
                        $scope.dobcount = 0;
                    }
                } else {
                    //alert('Invalid Age');
                    $scope.AlertMessage = $scope.invalid + ' ' + $scope.printage;
                    $scope.beneficiary.dob = null;
                    $scope.beneficiary.age = null;
                }
            }
        });
        $scope.HideMonthofDetection = true;
        $scope.$watch('beneficiary.plhiv', function (newValue, oldValue) {
            //console.log('newValue', newValue);
            if (newValue === oldValue) {
                return;
            } else if (newValue === true) {
                $scope.HideMonthofDetection = false;
            } else {
                $scope.HideMonthofDetection = true;
            }
        });
        //Datepicker settings start
        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };
        $scope.today();
        $scope.presenttoday = new Date();
        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };
        $scope.clear = function () {
            $scope.dt = null;
        };
        $scope.dtmax = new Date();
        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };
        $scope.toggleMin();
        $scope.picker = {};
        $scope.mod = {};
        $scope.start = {};
        $scope.incident = {};
        $scope.hlth = {};
        $scope.datestartedart = {};
        $scope.lasttest = {};
        $scope.open = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            item.opened = true;
        };
        $scope.open1 = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker1' + index).focus();
            });
            item.opened = true;
        };
        $scope.opendob = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdob' + index).focus();
            });
            $scope.picker.dobopened = true;
        };
        $scope.openfamilydob = function ($event, item, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#familydobopen' + index).focus();
            });
            item.familydobopened = true;
        };
        $scope.openmod = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickermod' + index).focus();
            });
            $scope.mod.openedmod = true;
        };
        $scope.openstart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerstart' + index).focus();
            });
            $scope.start.openedstart = true;
        };
        $scope.incidentopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerincident' + index).focus();
            });
            $scope.incident.incidentopened = true;
        };
        $scope.healthopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerhealth' + index).focus();
            });
            $scope.hlth.healthopened = true;
        };
        $scope.opendatestartedatart = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerdatestartedatart' + index).focus();
            });
            $scope.datestartedart.openeddatestartedatart = true;
        };
        $scope.lasttestdateopen = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.lasttest.lasttestdateopened = true;
        };
        $scope.termstartdate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termstartdate.dobopened = true;
        };
        $scope.termenddate = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepickerlasttestdate' + index).focus();
            });
            $scope.termenddate.dobopened = true;
        };
        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };
        $scope.monthOptions = {
            formatYear: 'yyyy',
            startingDay: 1,
            minMode: 'month'
        };
        $scope.mode = 'month';
        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.monthformat = $scope.monthformats[0];
        //Datepicker settings end
    })
    /********************************************************/
    .directive('modal1', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="modal-header">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="modal-body" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    })
    /******ravi*******/
    .directive('benefmodal1', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
/********beneficiaries*********/
