'use strict';
angular.module('secondarySalesApp').controller('RoutesViewListCtrl', function ($scope, Restangular, $route, $window) {
    /****************/
//    if ($window.sessionStorage.roleId != 5) {
//        window.location = "/";
//    }
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    if ($window.sessionStorage.prviousLocation != "partials/routesviewlist") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    
    
    //new changes
     Restangular.one('tilanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.TILanguage = langResponse[0];
          //$scope.modalInstanceLoad.close();
            $scope.TIHeading = $scope.TILanguage.trCreate;
            // $scope.modalTitle = $scope.RILanguage.thankYou;
            $scope.message = $scope.TILanguage.thankYouCreated;
        
        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }
    
    
    
    /* $scope.FacilityFilter = function () {
         $scope.backgroundCol1 = "";
         $scope.backgroundCol2 = "blue";
         $scope.dr = Restangular.all('employees?filter[where][stateId]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false').getList().then(function (dr) {
             $scope.employees = dr;
             $scope.sa = Restangular.all('sales-areas?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][deleteflag]=false').getList().then(function (sa) {
                 $scope.salesAreas = sa;
                 $scope.za = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (za) {
                     $scope.Zones = za;
                     $scope.zn = Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][partnerId]=' + $window.sessionStorage.coorgId + '&filter[order]=partnerId%20ASC').getList().then(function (zn) {
                         $scope.routes = zn;
                         //console.log('$scope.routes',$scope.routes);
                         $scope.modalInstanceLoad.close();
                         angular.forEach($scope.routes, function (member, index) {
                             member.index = index + 1;
                             for (var m = 0; m < $scope.employees.length; m++) {
                                 if (member.partnerId == $scope.employees[m].id) {
                                     member.FacilityName = $scope.employees[m];
                                     break;
                                 }
                             }
                             for (var n = 0; n < $scope.salesAreas.length; n++) {
                                 if (member.salesAreaId == $scope.salesAreas[n].id) {
                                     member.DistrictName = $scope.salesAreas[n];
                                     break;
                                 }
                             }
                             for (var o = 0; o < $scope.Zones.length; o++) {
                                 if (member.zoneId == $scope.Zones[o].id) {
                                     member.StateName = $scope.Zones[o];
                                     break;
                                 }
                             }
                             $scope.TotalData = [];
                             $scope.TotalData.push(member);
                         });
                     });
                 });
             });
         });
     }
     $scope.SiteFilter = function () {
             $scope.backgroundCol1 = "blue";
             $scope.backgroundCol2 = "";
             $scope.dr = Restangular.all('employees?filter[where][stateId]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false').getList().then(function (dr) {
                 $scope.employees = dr;
                 $scope.sa = Restangular.all('sales-areas?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][deleteflag]=false').getList().then(function (sa) {
                     $scope.salesAreas = sa;
                     $scope.za = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (za) {
                         $scope.Zones = za;
                         $scope.zn = Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][partnerId]=' + $window.sessionStorage.coorgId + '&filter[order]=name%20ASC').getList().then(function (zn) {
                             $scope.routes = zn;
                             //console.log('$scope.routes',$scope.routes);
                             $scope.modalInstanceLoad.close();
                             angular.forEach($scope.routes, function (member, index) {
                                 member.index = index + 1;
                                 for (var m = 0; m < $scope.employees.length; m++) {
                                     if (member.partnerId == $scope.employees[m].id) {
                                         member.FacilityName = $scope.employees[m];
                                         break;
                                     }
                                 }
                                 for (var n = 0; n < $scope.salesAreas.length; n++) {
                                     if (member.salesAreaId == $scope.salesAreas[n].id) {
                                         member.DistrictName = $scope.salesAreas[n];
                                         break;
                                     }
                                 }
                                 for (var o = 0; o < $scope.Zones.length; o++) {
                                     if (member.zoneId == $scope.Zones[o].id) {
                                         member.StateName = $scope.Zones[o];
                                         break;
                                     }
                                 }
                                 $scope.TotalData = [];
                                 $scope.TotalData.push(member);
                             });
                         });
                     });
                 });
             });
         }*/
    /********************************** INDEX **********************************/
    $scope.FetchMember = function (benId) {
        return Restangular.all('beneficiaries?filter[where][site]=' + benId + '&filter[where][deleteflag]=false').getList().$object;
    }
    Restangular.all('employees').getList().then(function (dr) {
        // ?filter[where][deleteflag]=false+ '&filter[where][countryId]=' + $window.sessionStorage.countryId +'&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][town]=' + $window.sessionStorage.distributionAreaId
        $scope.employees = dr;
        Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (sa) {
            $scope.salesAreas = sa;
            Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (za) {
                $scope.Zones = za;
                Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (country) {
                $scope.Country = country;
                    
                     Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (city) {
                $scope.City = city;
                    
                 Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][countryId]=' + $window.sessionStorage.countryId +'&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][town]=' + $window.sessionStorage.distributionAreaId + '&filter[where][tiId]=' + $window.sessionStorage.coorgId + '&filter[order]=name%20ASC').getList().then(function (zn) {
                    $scope.routes = zn;
                    //console.log('$scope.routes',$scope.routes);
                    $scope.modalInstanceLoad.close();
                    angular.forEach($scope.routes, function (member, index) {
                        member.index = index + 1;
                        member.totalamounts = $scope.FetchMember(member.id);
                         console.log("member.totalamounts",member.totalamounts.length);
                        
                        
                        for (var m = 0; m < $scope.employees.length; m++) {
                            if (member.tiId == $scope.employees[m].id) {
                                member.FacilityName = $scope.employees[m].firstName;
                                break;
                            }
                        }
                        for (var n = 0; n < $scope.salesAreas.length; n++) {
                            if (member.district == $scope.salesAreas[n].id) {
                                member.DistrictName = $scope.salesAreas[n].name;
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.Zones.length; o++) {
                            if (member.state == $scope.Zones[o].id) {
                                member.StateName = $scope.Zones[o].name;
                                break;
                            }
                        }
                        
                         for (var o = 0; o < $scope.Country.length; o++) {
                            if (member.countryId == $scope.Country[o].id) {
                                member.CountryName = $scope.Country[o].name;
                                break;
                            }
                        }
                        for (var o = 0; o < $scope.City.length; o++) {
                            if (member.town == $scope.City[o].id) {
                                member.TownName = $scope.City[o].name;
                                break;
                            }
                        }
                        //console.log(member);
                        $scope.TotalData = [];
                        $scope.TotalData.push(member);
                        
                       
                    });
                     
                });
            });
        });
            });
    });
    });
    /********************/
    /* $scope.sortingOrder = sortingOrder;
     $scope.reverse = false;

     $scope.sort_by = function (newSortingOrder) {
         if ($scope.sortingOrder == newSortingOrder)
             $scope.reverse = !$scope.reverse;

         $scope.sortingOrder = newSortingOrder;
     };*/

  /*  $scope.sort = {
        active: '',
        descending: undefined
    }

    $scope.changeSorting = function (column) {

        var sort = $scope.sort;

        if (sort.active == column) {
            sort.descending = !sort.descending;

        } else {
            sort.active = column;
            sort.descending = false;
        }
    };

    $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'glyphicon-chevron-up' : 'glyphicon-chevron-down';
            }
        }*/
      /******************************************/
var regex = /^([a-z]*)(\d*)/i;
    //$scope.boolAsc = false;
   // $scope.boolAscOne = false;
    
    //newly added
    $scope.sort = {
                active: '',
                descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }


    /*$scope.changeSorting = function (col) {
        if (col == 'name') {
            if ($scope.boolAsc) {
                // console.log('i am in 1 if');
                $scope.ascendingSorting(col)
            } else {
                // console.log('i am in 1 else');
                $scope.descendingSorting(col)
            }
        } else {
            if ($scope.boolAscOne) {
                // console.log('i am in 2 if');
                $scope.ascendingSorting(col)
            } else {
                // console.log('i am in 2 else');
                $scope.descendingSorting(col)
            }
        }

    }

    $scope.getIcon = function (column) {

        if ($scope.boolAsc) {
            return 'fa-sort-up';

        } else {
            return 'fa-sort-desc';
        }
    }
    $scope.getIconOne = function (colnew) {

        if ($scope.boolAscOne) {
            return 'fa-sort-up';

        } else {
            return 'fa-sort-desc';
        }
    }*/

    /******************/
   /* $scope.ascendingSorting = function (column) {
        var x = column;

        function sortFn(a, b) {
            var _a = a[x].match(regex);
            var _b = b[x].match(regex);

            // if the alphabetic part of a is less than that of b => -1
            if (_a[1] < _b[1]) return -1;
            // if the alphabetic part of a is greater than that of b => 1
            if (_a[1] > _b[1]) return 1;

            // if the alphabetic parts are equal, check the number parts
            var _n = parseInt(_a[2]) - parseInt(_b[2]);
            if (_n == 0) // if the number parts are equal start a recursive test on the rest
                return sortFn(a[x](_a[0].length), b[x](_b[0].length));
            // else, just sort using the numbers parts
            return _n;
        }*/

        /*  $scope.boolAsc = !$scope.boolAsc;
          $scope.boolAscone = !$scope.boolAscone;
          $scope.routes.sort(sortFn);*/
    
       /* if (x == 'name') {
            // console.log(' i m in boolAsc');
            $scope.boolAsc = !$scope.boolAsc;
            $scope.routes.sort(sortFn);
        } else {
            // console.log(' i m in boolAscOne');
            $scope.boolAscOne = !$scope.boolAscOne;
            $scope.routes.sort(sortFn);
        }
    };*/

   /* $scope.descendingSorting = function (column) {
        var y = column;


        function sortFn(a, b) {
            var _a = a[y].match(regex);
            var _b = b[y].match(regex);

            // if the alphabetic part of a is less than that of b => -1
            if (_a[1] < _b[1]) return 1;
            // if the alphabetic part of a is greater than that of b => 1
            if (_a[1] > _b[1]) return -1;

            // if the alphabetic parts are equal, check the number parts
            var _n = parseInt(_b[2]) - parseInt(_a[2]);
            if (_n == 0) // if the number parts are equal start a recursive test on the rest
                return sortFn(a[y](_a[0].length), b[y](_b[0].length));
            // else, just sort using the numbers parts
            return _n;
        }*/

        /*$scope.boolAsc = !$scope.boolAsc;
        $scope.boolAscone = !$scope.boolAscone;
        $scope.routes.sort(sortFn);*/
       /* if (y == 'name') {
            //  console.log(' i m in boolAsc');
            $scope.boolAsc = !$scope.boolAsc;
            $scope.routes.sort(sortFn);
        } else {
            // console.log(' i m in boolAscOne');
            $scope.boolAscOne = !$scope.boolAscOne;
            $scope.routes.sort(sortFn);
        }
    };*/

    /********************** Language ********************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.headersitelist = langResponse.headersitelist;
        $scope.entry = langResponse.entry;
        $scope.searchfor = langResponse.searchfor;
        $scope.sitename = langResponse.site;
        $scope.facility = langResponse.facility;
        $scope.district = langResponse.district;
         //$scope.country = langResponse.country;
         $scope.country = 'Country';
         $scope.town = langResponse.town;
        $scope.state = langResponse.state;
        $scope.show = langResponse.show;
        $scope.members = langResponse.members;
    });
});
/************************************* Not In Use **************************************************
		$scope.con = Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (con) {
		$scope.routes = con;
		$scope.modalInstanceLoad.close();
		angular.forEach($scope.routes, function (member, index) {
			member.index = index + 1;
		});
	});

		$scope.getZone = function (zoneId) {
		return Restangular.one('zones', zoneId).get().$object;
	};

	$scope.getSalesArea = function (salesAreaId) {
		return Restangular.one('sales-areas', salesAreaId).get().$object;
	};

	$scope.getPartner = function (partnerId) {
		return Restangular.one('employees', partnerId).get().$object;
	};

	$scope.getDistributionSubarea = function (distributionSubareaId) {
		return Restangular.one('distribution-subareas', distributionSubareaId).get().$object;
	};

	$scope.zones = Restangular.all('zones').getList().$object;


	$scope.searchDA = $scope.name;
	
	
	$scope.zoneId = '';
	$scope.salesareaId = '';
	$scope.salesid = '';
	$scope.partnerId;
	$scope.partid = '';
	$scope.zonalid = '';
	$scope.distributionSubareaId = '';
	$scope.firstName = '';

	$scope.$watch('zoneId', function (newValue, oldValue) {
		if (newValue === oldValue || newValue == '') {
			return;
		} else {
			$scope.salesareas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;

			$scope.con = Restangular.all('distribution-routes?filter[where][zoneId]=' + newValue).getList().then(function (con) {
				$scope.routes = con;
				//console.log('CON2',con);
				angular.forEach($scope.routes, function (member, index) {
					member.index = index + 1;
				});
			});

			$scope.zonalid = +newValue;
		}

	});


	
	$scope.$watch('salesAreaId', function (newValue, oldValue) {
		if (newValue === oldValue || newValue == '') {
			return;
		} else {
			$scope.partners = Restangular.all('partners?filter[where][salesAreaId]=' + newValue + '&filter[where][groupId]=8').getList().$object;

			$scope.Rt = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + newValue).getList().then(function (Rt) {
				$scope.routes = Rt;
				angular.forEach($scope.routes, function (member, index) {
					member.index = index + 1;
				});
			});
			$scope.salesid = +newValue;
		}

	});

	$scope.$watch('firstName', function (newValue, oldValue) {
		if (newValue === oldValue || newValue == '') {
			return;
		} else {
			//$scope.partners = Restangular.all('partners?filter[where][partnerId]='+newValue).getList().$object;

			$scope.Rt = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + $scope.salesid + '&filter[where][partnerId]=' + newValue).getList().then(function (Rt) {
				$scope.routes = Rt;
				angular.forEach($scope.routes, function (member, index) {
					member.index = index + 1;
				});
			});

			$scope.partid = +newValue;
		}
	});

	
	
$("#Search").keyup(function () {
		var data = this.value.toUpperCase().split(" ");
		var js = $("#bnbody").find("tr");
		if (this.value == "") {
			js.show();
			return;
		}
		js.hide();
		js.filter(function (i, v) {
			var $t = $(this);
			for (var d = 0; d < data.length; ++d) {
				if ($t.text().toUpperCase().indexOf(data[d]) > -1) {
					return true;
				}
			}
			return false;
		}).show();
	});
	
	*****************************************************************************/
