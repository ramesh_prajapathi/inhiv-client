'use strict';

angular.module('secondarySalesApp')
    .controller('SubTypCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/subtypology/create' || $location.path() === '/subtypology/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/subtypology/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/subtypology/create' || $location.path() === '/subtypology/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/subtypology/create' || $location.path() === '/subtypology/' + $routeParams.id;
            return visible;
        };
        //  $scope.OtherLang = false;



        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/subtypology") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }
        /*************************watch functions*************************************/

        $scope.showenglishLang = true;

        $scope.$watch('subtypology.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    $scope.subtypology.name = '';
                    $scope.subtypology.parentId = '';
                    $scope.subtypology.typologyId = '';
                    $scope.subtypology.orderno = ''
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;
                }

            }
        });
        $scope.$watch('subtypology.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('subtypologies', newValue).get().then(function (zn) {
                    $scope.subtypology.typologyId = zn.typologyId;

                    $scope.subtypology.orderno = zn.orderno;

                });
            }
        });



        /**********************************/
        Restangular.all('typologies?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rfch) {
            $scope.typologies = rfch;
        });
        /********************Edit***************************/


        if ($routeParams.id) {
            $scope.message = 'SubTypology Name has been Updated!';
            Restangular.one('subtypologies', $routeParams.id).get().then(function (subtypology) {
                $scope.original = subtypology;
                $scope.subtypology = Restangular.copy($scope.original);
                $scope.condiionoldvalue = $scope.subtypology.orderno;
            });
        } else {
            $scope.message = 'SubTypology Name has been Created!';
        }

       // $scope.Search = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name','typoname'];
        $scope.Search = '';

        /*********************************** INDEX *******************************************/
        //?filter[where][deleteflag]=false
        //        $scope.zn = Restangular.all('subtypologies?filter[where][deleteflag]=false').getList().then(function (zn) {
        //            $scope.subtypologies = zn;
        //            angular.forEach($scope.subtypologies, function (member, index) {
        //                member.index = index + 1;
        //            });
        //        });
    
    $scope.Disptypologies = Restangular.all('typologies?filter[where][deleteflag]=false').getList().$object;
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
        if ($window.sessionStorage.roleId == 4) {
            $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
            Restangular.all('subtypologies?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.subtypologies = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.subtypologies, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                    
                    for (var a = 0; a < $scope.Disptypologies.length; a++) {
                                if (member.typologyId == $scope.Disptypologies[a].id) {
                                    member.typoname = $scope.Disptypologies[a].name;
                                    break;
                                }
                            }
                    
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.subtyplanguages = sev;
            });

        } else {

            Restangular.all('subtypologies').getList().then(function (mt) {
                $scope.subtypologies = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.subtypologies, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                    
                      for (var a = 0; a < $scope.Disptypologies.length; a++) {
                                if (member.typologyId == $scope.Disptypologies[a].id) {
                                    member.typoname = $scope.Disptypologies[a].name;
                                    break;
                                }
                            }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('subtypologies?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.subtyplanguages = sev;

                        angular.forEach($scope.subtyplanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.subtyplanguages = sev;
                    });
                }
            });


        }
    $scope.$watch('subtypology.typologyId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.all('subtypologies?filter[where][deleteflag]=false&filter[where][language]=1' + '&filter[where][typologyId]=' + newValue).getList().then(function (zn) {
            $scope.subtypdisply = zn;
        });
            }
        });

        



        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.subtypdisply.filter(function (arr) {
                    return arr.orderno == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.subtypology.language != '') {
                    $scope.subtypology.orderno = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };



        /****************************************************************************************/


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };





        /****************************** SAVE *******************************************/

        $scope.subtypology = {
            "name": '',
            "deleteflag": false
        };

        $scope.submitDisable = false;
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('typ').style.border = "";
            document.getElementById('orderno').style.border = "";
            if ($scope.subtypology.language == '' || $scope.subtypology.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.subtypology.language == 1) {
                if ($scope.subtypology.name == '' || $scope.subtypology.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter SubTypology name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.subtypology.typologyId == '' || $scope.subtypology.typologyId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Typology';
                    document.getElementById('typ').style.borderColor = "#FF0000";

                } else if ($scope.subtypology.orderno == '' || $scope.subtypology.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Order';
                    document.getElementById('orderno').style.borderColor = "#FF0000";

                }
            } else if ($scope.subtypology.language != 1) {
                if ($scope.subtypology.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select SubTypology in English';

                } else if ($scope.subtypology.name == '' || $scope.subtypology.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter SubTypology name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.subtypology.typologyId == '' || $scope.subtypology.typologyId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Typology';
                    document.getElementById('typ').style.borderColor = "#FF0000";

                } else if ($scope.subtypology.orderno == '' || $scope.subtypology.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Order';
                    document.getElementById('orderno').style.borderColor = "#FF0000";

                }
            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                $scope.submitDisable = true;
                //                $scope.subtypologies.post($scope.subtypology).then(function () {
                //                    console.log('SubTypology Saved');
                //                    window.location = '/subtypology';
                //                });
                if ($scope.subtypology.parentId === '') {
                    delete $scope.subtypology['parentId'];
                }
                $scope.submitDisable = true;
                $scope.subtypology.parentFlag = $scope.showenglishLang;
                $scope.subtypologies.post($scope.subtypology).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/subtypology';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'subtypology_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };
        /******************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.subtypology.name == '' || $scope.subtypology.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter SubTypology name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                $scope.submitDisable = true;
                //                $scope.subtypologies.customPUT($scope.subtypology).then(function () {
                //                    console.log('subtypology Saved');
                //                    window.location = '/subtypology';
                //                });
                if ($scope.subtypology.parentId === '') {
                    delete $scope.subtypology['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('subtypologies', $routeParams.id).customPUT($scope.subtypology).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('Subtypology Name Saved');
                    setTimeout(function () {
                        window.location = '/subtypology';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'subtypology_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            }
        };


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /************************************ DELETE *******************************************/



        $scope.gettypology = function (typologyId) {
            return Restangular.one('typologies', typologyId).get().$object;
        };


        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }




        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('subtypologies/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('subtypologies/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


    });
