'use strict';
angular.module('secondarySalesApp').controller('BnEditCtrl', function ($scope, Restangular, $routeParams, $filter, $timeout, $window, AnalyticsRestangular, $modal, $route, $location) {
    $scope.hidePrevData = true;


    Restangular.one('memberlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.memberLang = langResponse[0];
        //$scope.modalInstanceLoad.close();
        $scope.memberHeading = $scope.memberLang.memberregistrationedit;
        $scope.modalTitle1 = $scope.memberLang.areYouSureToWantToUpdate;
         $scope.message = $scope.memberLang.memberUpdated;
            $scope.enteringchildage = $scope.memberLang.enterchildage;
            $scope.enteryourfullname = $scope.memberLang.peyfullname;
            $scope.enteryournickname = $scope.memberLang.enternickname;
            $scope.selectsite = $scope.memberLang.selsite;
            $scope.selecttypology = $scope.memberLang.seltypology;
            $scope.selectage = $scope.memberLang.selage;
            $scope.selectgender = $scope.memberLang.selgender;
            $scope.Selectdob = $scope.memberLang.Seldob;
            $scope.enteringhotspot = $scope.memberLang.enterhotspot;
            $scope.selectmaritalstatus = $scope.memberLang.selmaritalstatus;
            $scope.selectsubtypology =  $scope.memberLang.selsubtypology;
            $scope.enterphonenum = $scope.memberLang.enterphonenumber;
            $scope.selectnativestate = $scope.memberLang.selnativestate;
            $scope.selectnativedistrict = $scope.memberLang.selnativedistrict;
            $scope.selectregularpartner = $scope.memberLang.selregularpartner;
            $scope.selectvocation = $scope.memberLang.selvocation;
            $scope.enternoofsexyear = $scope.memberLang.entersexyear;
            $scope.selchildrendob = $scope.memberLang.selchilddob;

    });

    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle = 'Thank You';
    } else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle = 'धन्यवाद';
    } else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle = 'ಧನ್ಯವಾದ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle = 'நன்றி';
    }

    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.validationStateId = $window.sessionStorage.zoneId;
    $scope.isCreateView = true;
    $scope.isCreateEdit = false;
    $scope.isCreateSave = false;
    $scope.hotspotprint = false;
    $scope.disableapprove = true;
    $scope.detailsHide = true;
    $scope.createmodifyDisable = false;
    $scope.hideavahanid = false;
    $scope.actpaid = true;
    $scope.UpdateClicked = false;
    $scope.familydeletebuttonhide = false;
    $scope.paralegalproposed_yes = false;
    $scope.phonetype_disabled = true;
    $scope.disableVocation = true;

    /**************** Disable feild in mobility Status ****************/
    $scope.disableNoofdaysWD = true;
    $scope.disablerReasonOfMoveWD = true;
    $scope.disableNoofdaysWS = true;
    $scope.disablerReasonOfMoveWS = true;
    $scope.disableNoofdaysOS = true;
    $scope.disablerReasonOfMoveOS = true;

       $scope.ValidNumberOne = function (value) {
            // var arr = $scope.beneficiary.noofTimeDistrict;
            console.log('value', value);

            if (value > 0) {
                $scope.disableNoofdaysWD = false;
                $scope.disablerReasonOfMoveWD = false;

            } else {
                $scope.disableNoofdaysWD = true;
                $scope.disablerReasonOfMoveWD = true;
                $scope.beneficiary.noofDayDistrict = '';
                $scope.beneficiary.reasonofMovingDistrict = '';
                
            }
        };

        $scope.ValidNumberTwo = function (value) {

            if (value > 0) {
                $scope.disableNoofdaysWS = false;
                $scope.disablerReasonOfMoveWS = false;

            } else {
                $scope.disableNoofdaysWS = true;
                $scope.disablerReasonOfMoveWS = true;
                 $scope.beneficiary.noofDayState = '';
                $scope.beneficiary.reasonofMovingState = '';

            }
        };

        $scope.ValidNumberThree = function (value) {

            if (value > 0) {
                $scope.disableNoofdaysOS = false;
                $scope.disablerReasonOfMoveOS = false;

            } else {
                $scope.disableNoofdaysOS = true;
                $scope.disablerReasonOfMoveOS = true;
                 $scope.beneficiary.noofDayOutState = '';
                $scope.beneficiary.reasonofMovingOutState = '';
            }
        };




    $scope.maritalstatuses = Restangular.all('maritalstatuses?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;


    $scope.studyingin = Restangular.all('educations?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;



    $scope.reasonofmoves = Restangular.all('nativestates?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (ZoneResp) {
        $scope.zones = ZoneResp;
    });

    $scope.salesareas = Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (SalesResp) {
        $scope.salesareas = SalesResp;
    });
    $scope.towns = Restangular.all('cities?filter[where][district]=' + $window.sessionStorage.salesAreaId).getList().$object;

    $scope.regularpartners = Restangular.all('regularpartners?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    $scope.vocationpartners = Restangular.all('vocationpartners?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    $scope.genderdisplays = Restangular.all('genders?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    Restangular.all('hotspots?filter[where][countryId]=' + $window.sessionStorage.countryId + '&filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (resp) {
        $scope.hotspots = resp;
        console.log('$scope.hotspots', $scope.hotspots);
    });

    $scope.typologies = Restangular.all('typologies?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;

    Restangular.all('subtypologies?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Subresp) {
        //console.log('Subresp', Subresp);
        $scope.subtypologies = Subresp;
    });
    Restangular.all('genders?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (genderResp) {
        $scope.genders = genderResp;

    });



    $scope.detailsHide2 = true;
    $scope.showMoreDetails2 = true;
    //console.log(' new Date()', new Date());
    $scope.beneficiary = {
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId, //lastmodifiedtime: new Date(),
        district: $window.sessionStorage.salesAreaId,
        deleteflag: false,
        nooftermsheld: 1
    };
    $scope.reportincident = {};
    $scope.beneficiary.dynamicmember = true;
    $scope.beneficiary.plhiv = false;
    $scope.beneficiary.paidmember = false;
    $scope.beneficiary.physicallydisabled = false;
    $scope.beneficiary.mentallydisabled = false;
    $scope.beneficiary.championapproved = false;
    $scope.beneficiary.championproposed = false;
    $scope.beneficiary.paralegalhasplv = false;
    $scope.disablefullname = true;

    $scope.auditlog = {};

    $scope.$watch('beneficiary.site', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            Restangular.all('routelinks?filter[where][distributionRouteId]=' + newValue).getList().then(function (routeResponse) {
                $scope.routeResponse = routeResponse;
               // Restangular.one('fieldworkers', routeResponse[0].partnerId).get().then(function (fwget) {
                Restangular.one('comembers', routeResponse[0].partnerId).get().then(function (fwget) {
                
                    $scope.beneficiary.fieldworkername = fwget.lastname;
                    $scope.beneficiary.fieldworker = fwget.id;
                });
            });
        }
    });

    $scope.DisableGender = false;

    $scope.$watch('beneficiary.typology', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == null) {
            return;
        } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
            return;
        } else {

            $scope.DisableGender = true;

            Restangular.all('subtypologies?filter[where][typologyId]=' + newValue + '&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (Subresp) {
                //console.log('Subresp', Subresp);
                $scope.subtypologies = Subresp;
                $scope.beneficiary.subTypologyId = Subresp[0].id;
                // $scope.beneficiary.subTypologyId = $scope.beneficiary.subTypologyId;

                Restangular.one('typologies', newValue).get().then(function (respTypo) {
                    $scope.genderId = respTypo.genderId;

                    Restangular.all('genders?filter[where][id]=' + $scope.genderId + '&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (genderResp) {
                        $scope.genders = genderResp;
                        $scope.beneficiary.gender = genderResp[0].id;

                    });
                });
            });

        }
    });


    $scope.$watch('beneficiary.regularpartnerId', function (newValue, oldValue) {
        if (newValue == 42) {
            $scope.disableVocation = false;
        } else {
              $scope.beneficiary.vocationPartnerId = '';
            $scope.disableVocation = true;
        }
    });



    $scope.fwsitehide = true;
    $scope.cosite = true;
    $scope.familymembers = [];

    /******************************************* beneficiaries', $routeParams.id *******************/
    $scope.familymembers = [];
    Restangular.one('beneficiaries', $routeParams.id).get().then(function (partner) {

        $scope.partner = partner;
        $scope.original = partner;
        $scope.oldHotspot = partner.hotspot;
        $scope.beneiTiId = partner.tiId;

        Restangular.one('hotspots?filter[where][name]=' + partner.hotspot).get().then(function (hot) {
            $scope.original1 = hot;
            $scope.Printhotspot = hot;
            $scope.getHotspot = hot[0].name;
        });
        Restangular.one('familymembers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][deleteflag]=false').get().then(function (familymem) {
            console.log('familymem', familymem);

            if (familymem.length > 0) {

                angular.forEach(familymem, function (member, index) {
                    console.log('member', member);
                    member.existingFlag = true;
                    $scope.familymembers.push(member);

                });
                console.log('$scope.familymembers', $scope.familymembers);

            } else {
                $scope.familymembers = [{
                    age: '',
                    gender: '',
                    Stuyding: null,
                    dob: null,
                    existingFlag: false,
                    beneficiaryid: 0,
                    deleteflag: false
                }];
            }
            $scope.modalInstanceLoad.close();

        });
        /*********************************  SITE ****************************************/
        if ($window.sessionStorage.roleId == 2) {
            $scope.beneficiary.lastmodifiedbyrole = 2;
            $scope.disableapprove = false;
            $scope.fwsitehide = true;
            $scope.cosite = false;
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
                $scope.beneficiary.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;
                $scope.beneficiary.artId = comember.artId;
                        $scope.beneficiary.ictcid = comember.ictcId;
                Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                    console.log('employee', employee);

                    $scope.sites = Restangular.all('distribution-routes?filter[where][tiId]=' + employee.id + '&filter[where][deleteflag]=false').getList().$object;
                    setTimeout(function () {
                        $scope.beneficiary.site = $scope.original.site;
                    }, 2000, false);

                });
            });

        } else {
            $scope.beneficiary.lastmodifiedbyrole = 6;
            $scope.fwsitehide = false;
            $scope.cosite = true;
            $scope.disableapprove = true;
           // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                $scope.getsiteid = fw.id;
                $scope.beneficiary.fieldworkername = fw.lastname;
                $scope.beneficiary.fieldworker = fw.id;
                Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                    $scope.beneficiary.facilityId = comember.id;
                    $scope.auditlog.facilityId = comember.id;
                    $scope.beneficiary.artId = comember.artId;
                        $scope.beneficiary.ictcid = comember.ictcId;
                    Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (employee) {
                        $scope.facilityName = employee.firstName;



                        Restangular.all('routelinks?filter[where][partnerId]=' + $scope.getsiteid).getList().then(function (routeResponse) {
                            $scope.routelinksget = routeResponse;
                            angular.forEach($scope.routelinksget, function (member, index) {
                                member.index = index + 1;
                                member.routeName = Restangular.one('distribution-routes', member.distributionRouteId).get().$object;
                            });
                        });

                        setTimeout(function () {
                            $scope.beneficiary.site = $scope.original.site;
                        }, 2000, false);
                    });
                });

            });

        }
        $scope.beneficiary = Restangular.copy($scope.original);
    });
    /****************** Update *******************************************/
    $scope.beneficiarydataModal = false;
    $scope.submitcount = 0;
    $scope.validatestring = '';
    $scope.no = {};
    $scope.newhotspot = {};
    $scope.newhotspot.facility = $window.sessionStorage.coorgId;
    $scope.newhotspot.state = $window.sessionStorage.zoneId;
    $scope.newhotspot.district = $window.sessionStorage.salesAreaId;

    $scope.UpdateProfile = function () {

        if ($scope.beneficiary.hotspot != $scope.no.Noresponse && $scope.no.Noresponse != null) {
            $scope.beneficiary.hotspot = $scope.no.Noresponse;
            $scope.newhotspot.name = $scope.no.Noresponse;
            Restangular.all('hotspots').post($scope.newhotspot).then(function (hotspot) {});
        }
        if ($scope.beneficiary.fullname == '' || $scope.beneficiary.fullname == null) {
            $scope.validatestring = $scope.validatestring + $scope.enteryourfullname;
        } else if ($scope.beneficiary.fullname.indexOf(' ') == -1) {
            $scope.validatestring = $scope.validatestring + $scope.enteryourfullname;
            //$scope.beneficiary.fullname = null;
        } else if ($scope.alertFlag == true) {
            $scope.validatestring = $scope.validatestring + 'Invalid Full Name';
            //$scope.beneficiary.fullname = null;
        } else if ($scope.beneficiary.nickname == '' || $scope.beneficiary.nickname == null) {
            $scope.validatestring = $scope.validatestring + $scope.enteryournickname;
            //$scope.beneficiary.fullname = null;
        }  else if ($scope.beneficiary.site == '' || $scope.beneficiary.site == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectsite;
            } else if ($scope.beneficiary.hotspot == '' || $scope.beneficiary.hotspot == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteringhotspot;
            }
            //            else if ($scope.beneficiary.facility == '' || $scope.beneficiary.facility == null) {
            //                $scope.validatestring = $scope.validatestring + $scope.selfacility;
            //            } 
            else if ($scope.beneficiary.typology == '' || $scope.beneficiary.typology == null) {
                $scope.validatestring = $scope.validatestring + $scope.selecttypology;
            } else if ($scope.beneficiary.gender == '' || $scope.beneficiary.gender == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectgender;
            } else if ($scope.beneficiary.subTypologyId == '' || $scope.beneficiary.subTypologyId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectsubtypology;
            } else if ($scope.beneficiary.maritalstatus == '' || $scope.beneficiary.maritalstatus == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectmaritalstatus;
            } else if ($scope.beneficiary.age == '' || $scope.beneficiary.age == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectage;
            } else if ($scope.beneficiary.dob == '' || $scope.beneficiary.dob == null) {
                $scope.validatestring = $scope.validatestring + $scope.Selectdob;
            } else if ($scope.beneficiary.phonenumber == '' || $scope.beneficiary.phonenumber == null) {
                $scope.validatestring = $scope.validatestring + $scope.enterphonenum;
            }  else if ($scope.beneficiary.phonenumber.length !=10) {
                $scope.validatestring = $scope.validatestring + $scope.enterphonenum;
            }  else if ($scope.beneficiary.nativeStateId == '' || $scope.beneficiary.nativeStateId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectnativestate;
            } else if ($scope.beneficiary.nativeDistrictId == '' || $scope.beneficiary.nativeDistrictId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectnativedistrict;
            } else if ($scope.beneficiary.regularpartnerId == '' || $scope.beneficiary.regularpartnerId == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectregularpartner;
            } 
        /*else if ($scope.beneficiary.vocationPartnerId == '' || $scope.beneficiary.vocationPartnerId == null) {
            $scope.validatestring = $scope.validatestring + $scope.selectvocation;
        } */
        else if ($scope.beneficiary.noofSexYear == '' || $scope.beneficiary.noofSexYear == null) {
            $scope.validatestring = $scope.validatestring + $scope.enternoofsexyear;
        }


        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //$scope.beneficiarydataModal = !$scope.beneficiarydataModal;
        } else {

            $scope.TiIdDisplay = $scope.beneficiary.tiId;

            Restangular.one('distribution-routes', $scope.beneficiary.site).get().then(function (siteget) {
                $scope.siteName = siteget.name;
            });
           // Restangular.one('fieldworkers', $scope.beneficiary.fieldworker).get().then(function (fwget) {
              ///  $scope.printfieldworkerName = fwget.firstname; //+ ' ' + fwget.lastname;
           // });
              Restangular.one('comembers', $scope.beneficiary.fieldworker).get().then(function (fwget) {
                $scope.printfieldworkerName = fwget.firstname; //+ ' ' + fwget.lastname;
            });

            Restangular.one('genders', $scope.beneficiary.gender).get().then(function (genderget) {
                $scope.genderName = genderget;
            });
            Restangular.one('maritalstatuses', $scope.beneficiary.maritalstatus).get().then(function (maritalstatusget) {
                $scope.maritalstatusName = maritalstatusget;
            });
            Restangular.one('typologies', $scope.beneficiary.typology).get().then(function (typ) {
                $scope.typologyName = typ;
            });
            Restangular.one('subtypologies', $scope.beneficiary.subTypologyId).get().then(function (subtyp) {
                $scope.subtypologyName = subtyp;
            });

            Restangular.one('sales-areas', $scope.beneficiary.nativeDistrictId).get().then(function (nDist) {
                $scope.nativeDistrict = nDist.name;
            });

            Restangular.one('zones', $scope.beneficiary.nativeStateId).get().then(function (nState) {
                $scope.nativeState = nState.name;
            });


            Restangular.one('regularpartners', $scope.beneficiary.regularpartnerId).get().then(function (regPart) {
                $scope.regularpatrnerDisaply = regPart.name;
                $scope.beneficiarydataModal = true;
            });

            /* Restangular.one('vocationpartners', $scope.beneficiary.vocationPartnerId).get().then(function (vocPart) {
                 $scope.vocationpatrnerDisaply = vocPart.name;
                 $scope.beneficiarydataModal = true;
             });*/
        }
    };





    $scope.OKBUTTON = function () {
        $scope.beneficiary.lastmodifiedtime = new Date();
        Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiary).then(function (updateResponse) {
            console.log('$scope.checkAddFamily', $scope.checkAddFamily);
            console.log('$scope.checkAddFamily', $scope.familymemberlength);
            $scope.CurrentMemberId = updateResponse.id;

            $scope.auditlog.entityid = updateResponse.id;
            $scope.auditlog.description = 'Member Updated';
            $scope.auditlog.modifiedbyroleid = $window.sessionStorage.roleId;
            $scope.auditlog.modifiedby = $window.sessionStorage.UserEmployeeId;
            $scope.auditlog.lastmodifiedtime = new Date();
            $scope.auditlog.state = $window.sessionStorage.zoneId;
            $scope.auditlog.district = $window.sessionStorage.salesAreaId;
            $scope.auditlog.facility = $window.sessionStorage.coorgId;
            $scope.auditlog.facilityId = $scope.auditlog.facilityId;

            Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                $scope.testResultSave($scope.CurrentMemberId);
            });
        });


    };


    /***************************newChnages**************/

    $scope.TestCount = 0;
    $scope.testResultSave = function (CurrentMemberId) {

        if ($scope.TestCount < $scope.familymembers.length) {

            if ($scope.familymembers[$scope.TestCount].existingFlag == true) {
                $scope.familymembers[$scope.TestCount].beneficiaryid = CurrentMemberId;

                Restangular.one('familymembers', $scope.familymembers[$scope.TestCount].id).customPUT($scope.familymembers[$scope.TestCount]).then(function (childResp) {
                    // console.log('childResp', childResp);
                    $scope.TestCount++;
                    $scope.testResultSave(CurrentMemberId);
                });

            } else {
                if ($scope.familymembers[$scope.TestCount].gender == null || $scope.familymembers[$scope.TestCount].gender == '') {
                    $scope.TestCount++;
                    $scope.testResultSave(CurrentMemberId);
                } else {

                    $scope.familymembers[$scope.TestCount].beneficiaryid = CurrentMemberId;
                    Restangular.all('familymembers').post($scope.familymembers[$scope.TestCount]).then(function (childResp) {
                        //  console.log('childResp', childResp);
                        $scope.TestCount++;
                        $scope.testResultSave(CurrentMemberId);
                    });
                }
            }

        } else {

            //$scope.modalInstanceLoad.close();
            // $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
            $scope.beneficiarydataModal = false;
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

            console.log('reloading...');

            setInterval(function () {
                window.location = '/members';
            }, 400);
        }

    };


    /***************************newChnages**************/


    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    $scope.openOneAlert = function () {
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-danger'
        });
    };
    $scope.okAlert = function () {
        $scope.modalOneAlert.close();
    };
    /***ravi***/
    $scope.redirecttransfer = function () {
        $location.path('/transfer_member_data/' + $scope.transferredFrom);
    };

    /********************************* FamilyMember Change **************/
    $scope.FamilyGenderChanges = false;
    $scope.CheckGender = function (scope, index, agee, famId) {
        $scope.FamilyGenderChanges = true;
         $scope.familymembers[index].age = '';
         $scope.familymembers[index].dob = '';

        if (scope.familymember.gender >= 1 && scope.familymember.Age != undefined && scope.familymember.Age != '') {
            return;
        } else if (agee <= 0 || agee == '') {

        }
    }
    $scope.FamilyAgeChanges = false;
    $scope.ChilredAge = function (scope, index) {
        $scope.FamilyAgeChanges = true;
        if (scope.familymember.age >= 0 && scope.familymember.age <= 99) {
            if ($scope.dobcount == 0) {
                var todaydate = new Date();
                var newdate = new Date(todaydate);
                newdate.setFullYear(newdate.getFullYear() - scope.familymember.age);
                var nd = new Date(newdate);
                $scope.familymembers[index].dob = nd;
            } else {
                $scope.dobcount = 0;
            }
        } else {
            //alert('Invalid Age');
            $scope.AlertMessage = 'Invalid Age'; //'Select Date Of Birth';
            $scope.openOneAlert();
            $scope.familymember[index].age = null;
            $scope.familymember[index].dob = null;
        }
    };
    $scope.FamilyDOBChanges = false;
    $scope.ChilredDOB = function (scope, index) {
        console.log('scope', scope);
        $scope.FamilyDOBChanges = true;
        $scope.dobcount++;
        $scope.today = new Date();
        $scope.birthyear = scope.familymember.dob;
        var ynew = $scope.today.getFullYear();
        var mnew = $scope.today.getMonth();
        var dnew = $scope.today.getDate();
        var yold = $scope.birthyear.getFullYear();
        var mold = $scope.birthyear.getMonth();
        var dold = $scope.birthyear.getDate();
        var diff = ynew - yold;
        if (mold > mnew) diff--;
        else {
            if (mold == mnew) {
                if (dold > dnew) diff--;
            }
        }
        $scope.familymembers[index].age = diff;
    }
    $scope.FamilyStudyChanges = false;
    $scope.CheckStudyIn = function (idstd) {
        $scope.FamilyStudyChanges = true;
    }
    /** $scope.goforsave = false;
     $scope.goforupdate = false;
     $scope.SaveFamilymember = function (benId) {
         console.log('I m SaveFamilymember');
         for (var h = 0; h < $scope.familymembers.length; h++) {
             //console.log('$scope.familymembers[h]',$scope.familymembers[h]);
             if (($scope.familymembers[h].age < !1 || $scope.familymembers[h].age == '' || $scope.familymembers[h].age == undefined) && $scope.familymembers[h].id != null) {
                 //$scope.validatestring = 'Enter Children Age';
                 $scope.AlertMessage = $scope.enterchildage;
                 $scope.openOneAlert();
                 //alert('Enter Children Age');
                 $scope.goforsave = false;
                 //console.log('$scope.familymembers[h].gender', $scope.familymembers[h].gender);
             } else if (($scope.familymembers[h].dob == '' || $scope.familymembers[h].dob == undefined || $scope.familymembers[h].dob == null) && $scope.familymembers[h].id != null) {
                 //alert('Select Children DOB');
                 $scope.AlertMessage = $scope.selchilddob;
                 $scope.openOneAlert();
                 $scope.goforsave = false;
             } else if (($scope.familymembers[h].gender < !1 || $scope.familymembers[h].gender == '' || $scope.familymembers[h].gender == undefined) && $scope.familymembers[h].id != null) {
                 //$scope.validatestring = 'Enter Children Gender';
                 //alert('Enter Children Gender');
                 $scope.AlertMessage = $scope.enterchildgender;
                 $scope.openOneAlert();
                 $scope.goforsave = false;
             } else {
                 $scope.goforsave = true;
             }
         }
         if ($scope.goforsave == true) {
             $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
             console.log('Ready to Save');
             $scope.familymembers.beneficiaryid = $routeParams.id;
             for (var i = 0; i < $scope.totalfamilymembercount; i++) {
                 //console.log(' $scope.totalfamilymembercount', $scope.familymember[i].age);
                 $scope.familymembers[i].beneficiaryid = $routeParams.id;
             }
             Restangular.all('familymembers').post($scope.familymembers).then(function (familyRes) {
                 console.log('SaveFamilymember', familyRes);
                 //$scope.beneficiarydataModal = !$scope.beneficiarydataModal;
             });
         }
     }
     $scope.UpdateFamilymember = function () {
         console.log('I m UpdateFamilymember');
         if ($scope.FamilyGenderChanges == true) {
             for (var h = 0; h < $scope.familymembers.length; h++) {
                 if ($scope.familymembers[h].age < !1 || $scope.familymembers[h].age == '' || $scope.familymembers[h].age == undefined) {
                     //alert('Enter Childern Age');
                     $scope.AlertMessage = $scope.enterchildage;
                     $scope.openOneAlert();
                     $scope.goforupdate = false;
                 } else if ($scope.familymembers[h].dob == '' || $scope.familymembers[h].dob == undefined || $scope.familymembers[h].dob == null) {
                     $scope.AlertMessage = $scope.selchilddob;
                     $scope.openOneAlert();
                     $scope.goforupdate = false;
                 } else if ($scope.familymembers[h].gender < !1 || $scope.familymembers[h].gender == '' || $scope.familymembers[h].gender == undefined) {
                     $scope.AlertMessage = $scope.enterchildgender;
                     $scope.openOneAlert();
                     $scope.goforupdate = false;
                 } else {
                     $scope.goforupdate = true;
                 }
             }
         } else if ($scope.FamilyGenderChanges == false) {
             $scope.UpdateClicked = true;
             $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
         }
         if ($scope.goforupdate == true) {
             $scope.beneficiarydataModal = !$scope.beneficiarydataModal;
             //console.log('Ready to Update', $scope.currentFamilyMember.length);
             for (var iii = 0; iii < $scope.currentFamilyMember.length; iii++) {
                 Restangular.one('familymembers', $scope.currentFamilyMember[iii].id).customPUT($scope.familymembers[iii]).then(function (response) {
                     console.log('update family', response);
                 });
             }
         }
     };**/
    $scope.DeleteFamilyMember = function (famid, index, memid) {
        $scope.item = [{
            deleteflag: true
    }]
        Restangular.one('familymembers/' + famid).customPUT($scope.item[0]).then(function (deletefamily) {
            //$scope.familymemberlength--;
            $route.reload();
        });
        $scope.familymembers.splice(index, 1);

    }


    $scope.checkAddFamily = false;
    $scope.addFamilyMember = function (id, index) {
        $scope.checkAddFamily = true;
        $scope.familymembers.push({
            age: '',
            gender: '',
            Stuyding: null,
            dob: null,
            existingFlag: true,
            beneficiaryid: 0,
            deleteflag: false
        });
        $scope.totalfamilymembercount = $scope.familymembers.length;
    }
    $scope.totalfamilymembercount = $scope.familymembers.length;;
    $scope.removeFamilyMember = function (index) {
        var item = $scope.familymembers[index];
        if (index == 0) {
            //alert('Can not Delete..!');
            $scope.AlertMessage = $scope.cannot + ' ' + $scope.deletebutton; //'Select Date Of Birth';
            $scope.openOneAlert();
        } else {
            $scope.familymembers.splice(index, 1);
            //$scope.familymembers[index].deleteflag = true;
        }
    };



    //Datepicker settings start
    $scope.today = function () {
        $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
    };
    $scope.today();
    $scope.presenttoday = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmax = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.mod = {};
    $scope.start = {};
    $scope.incident = {};
    $scope.hlth = {};
    $scope.datestartedart = {};
    $scope.lasttest = {};
    $scope.open = function ($event, item, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        item.opened = true;
    };
    $scope.open1 = function ($event, item, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker1' + index).focus();
        });
        item.opened = true;
    };
    $scope.opendob = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerdob' + index).focus();
        });
        $scope.picker.dobopened = true;
    };
    $scope.openfamilydob = function ($event, item, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerdob' + index).focus();
        });
        item.familydobopened = true;
    };
    $scope.openmod = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickermod' + index).focus();
        });
        $scope.mod.openedmod = true;
    };
    $scope.openstart = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerstart' + index).focus();
        });
        $scope.start.openedstart = true;
    };
    $scope.incidentopen = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerincident' + index).focus();
        });
        $scope.incident.incidentopened = true;
    };
    $scope.healthopen = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerhealth' + index).focus();
        });
        $scope.hlth.healthopened = true;
    };
    $scope.opendatestartedatart = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerdatestartedatart' + index).focus();
        });
        $scope.datestartedart.openeddatestartedatart = true;
    };
    $scope.lasttestdateopen = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerlasttestdate' + index).focus();
        });
        $scope.lasttest.lasttestdateopened = true;
    };
    $scope.termstartdate = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerlasttestdate' + index).focus();
        });
        $scope.termstartdate.dobopened = true;
    };
    $scope.termenddate = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerlasttestdate' + index).focus();
        });
        $scope.termenddate.dobopened = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.monthOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
        minMode: 'month'
    };
    $scope.mode = 'month';
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.monthformat = $scope.monthformats[0];
    //Datepicker settings end
    $scope.$watch('beneficiary.age', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == null) {
            return;
        } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
            return;
        } else {
            if (newValue >= 0 && newValue <= 99) {
                if ($scope.dobcount == 0) {
                    var todaydate = new Date();
                    var newdate = new Date(todaydate);
                    newdate.setFullYear(newdate.getFullYear() - newValue);
                    var nd = new Date(newdate);
                    $scope.beneficiary.dob = nd;
                } else {
                    $scope.dobcount = 0;
                }
            } else {
                //alert('Invalid Age');
                $scope.AlertMessage = 'Invalid' + ' ' + $scope.printage; //'Select Date Of Birth';
                $scope.openOneAlert();
                $scope.beneficiary.dob = null;
                $scope.beneficiary.age = null;
            }
        }
    });
    $scope.dobcount = 0;
    $scope.$watch('beneficiary.dob', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '' || newValue == null) {
            return;
        } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
            return;
        } else {
            $scope.dobcount++;
            $scope.today = new Date();
            $scope.birthyear = newValue;
            var ynew = $scope.today.getFullYear();
            var mnew = $scope.today.getMonth();
            var dnew = $scope.today.getDate();
            var yold = $scope.birthyear.getFullYear();
            var mold = $scope.birthyear.getMonth();
            var dold = $scope.birthyear.getDate();
            var diff = ynew - yold;
            if (mold > mnew) diff--;
            else {
                if (mold == mnew) {
                    if (dold > dnew) diff--;
                }
            }
            $scope.beneficiary.age = diff;
        }
    });
    /***********************************************************************/

    $scope.beneficiarydetails = {
        "rationcard": false,
        "accidentalinsurrance": false,
        "aadharcard": false,
        "voterid": false,
        "pancard": false,
        "savingaccount": false,
        "rd": false,
        "fd": false,
        "lifeinsurance": false,
        "healthinsurance": false,
        "loan": false,
        "plhiv": false,
        "onart": false,
        "vulnerablemember": false,
        "financialliteracy": false,
        "financialplanning": false
    };
    AnalyticsRestangular.one('beneficiary_f/findOne?filter[where][beneficiary_id]=' + $routeParams.id).get().then(function (partner) {
        $scope.AnalyticsBeneficiary = partner;
        //console.log('partner', partner);
        Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (answeredquestions) {
            // console.log('answeredquestions', answeredquestions);
            ///plhiv
            //$scope.beneficiarydetails.plhiv = $scope.beneficiary.plhiv;
            for (var i = 0; i < answeredquestions.length; i++) {
                ///RationCard
                if (answeredquestions[i].questionid == 42) {
                    //console.log('$scope.beneficiarydetails.rationcard',answeredquestions[i].answer.toLowerCase());
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.rationcard = true;
                    }
                }
                ///Accidental Insurrance
                if (answeredquestions[i].questionid == 27) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.accidentalinsurrance = true;
                        //console.log('Accidental Insurrance', answeredquestions[i].answer.toLowerCase());
                    } else {
                        $scope.beneficiarydetails.accidentalinsurrance = false;
                    }
                }
                ///aadharcard
                if (answeredquestions[i].questionid == 40) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.aadharcard = true;
                    } else {
                        $scope.beneficiarydetails.aadharcard = false;
                    }
                }
                ///voterid
                if (answeredquestions[i].questionid == 44) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.voterid = true;
                    } else {
                        $scope.beneficiarydetails.voterid = false;
                    }
                }
                /* ///pancard
                 if (answeredquestions[i].questionid == 44) {
                     if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                         $scope.beneficiarydetails.pancard = true;
                     } else {
                         $scope.beneficiarydetails.pancard = false;
                     }
                 } else {
                     $scope.beneficiarydetails.pancard = false;
                 }*/
                ///savingaccount
                if (answeredquestions[i].questionid == 72) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.savingaccount = true;
                    } else {
                        $scope.beneficiarydetails.savingaccount = false;
                    }
                }
                /* ///rd
                 if (answeredquestions[i].questionid == 44) {
                     if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                         $scope.beneficiarydetails.rd = true;
                     } else {
                         $scope.beneficiarydetails.rd = false;
                     }
                 } else {
                     $scope.beneficiarydetails.rd = false;
                 }*/
                /* ///fd
                 if (answeredquestions[i].questionid == 44) {
                     if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                         $scope.beneficiarydetails.fd = true;
                     } else {
                         $scope.beneficiarydetails.fd = false;
                     }
                 } else {
                     $scope.beneficiarydetails.fd = false;
                 }*/
                ///lifeinsurance
                if (answeredquestions[i].questionid == 23) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.lifeinsurance = true;
                    } else {
                        $scope.beneficiarydetails.lifeinsurance = false;
                    }
                }
                ///healthinsurance
                if (answeredquestions[i].questionid == 25) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.healthinsurance = true;
                    } else {
                        $scope.beneficiarydetails.healthinsurance = false;
                    }
                }
                ///loan
                if (answeredquestions[i].questionid == 31) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.loan = true;
                    } else {
                        $scope.beneficiarydetails.loan = false;
                    }
                }
                ///onart
                if (answeredquestions[i].questionid == 6) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.onart = true;
                    } else {
                        $scope.beneficiarydetails.onart = false;
                    }
                }
                ///vulnerablemember
                if ($scope.AnalyticsBeneficiary.vulnerability_count > 0) {
                    $scope.beneficiarydetails.vulnerablemember = true;
                } else {
                    $scope.beneficiarydetails.vulnerablemember = false;
                }
                ///financialliteracy
                if (answeredquestions[i].questionid == 17) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.financialliteracy = true;
                    } else {
                        $scope.beneficiarydetails.financialliteracy = false;
                    }
                }
                ///financialplanning
                if (answeredquestions[i].questionid == 34) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.financialplanning = true;
                    } else {
                        $scope.beneficiarydetails.financialplanning = false;
                    }
                }
            }
        });
    }, function (error) {
        Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (answeredquestions) {
            // console.log('answeredquestions', answeredquestions);
            ///plhiv
            //$scope.beneficiarydetails.plhiv = $scope.beneficiary.plhiv;
            for (var i = 0; i < answeredquestions.length; i++) {
                ///RationCard
                if (answeredquestions[i].questionid == 42) {
                    // console.log('$scope.beneficiarydetails.rationcard',answeredquestions[i].answer.toLowerCase());
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.rationcard = true;
                    }
                }
                ///Accidental Insurrance
                if (answeredquestions[i].questionid == 27) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.accidentalinsurrance = true;
                        //console.log('Accidental Insurrance', answeredquestions[i].answer.toLowerCase());
                    } else {
                        $scope.beneficiarydetails.accidentalinsurrance = false;
                    }
                }
                ///aadharcard
                if (answeredquestions[i].questionid == 40) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.aadharcard = true;
                    } else {
                        $scope.beneficiarydetails.aadharcard = false;
                    }
                }
                ///voterid
                if (answeredquestions[i].questionid == 44) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.voterid = true;
                    } else {
                        $scope.beneficiarydetails.voterid = false;
                    }
                }
                /* ///pancard
                 if (answeredquestions[i].questionid == 44) {
                     if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                         $scope.beneficiarydetails.pancard = true;
                     } else {
                         $scope.beneficiarydetails.pancard = false;
                     }
                 } else {
                     $scope.beneficiarydetails.pancard = false;
                 }*/
                ///savingaccount
                if (answeredquestions[i].questionid == 72) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.savingaccount = true;
                    } else {
                        $scope.beneficiarydetails.savingaccount = false;
                    }
                }
                /* ///rd
                 if (answeredquestions[i].questionid == 44) {
                     if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                         $scope.beneficiarydetails.rd = true;
                     } else {
                         $scope.beneficiarydetails.rd = false;
                     }
                 } else {
                     $scope.beneficiarydetails.rd = false;
                 }*/
                /* ///fd
                 if (answeredquestions[i].questionid == 44) {
                     if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                         $scope.beneficiarydetails.fd = true;
                     } else {
                         $scope.beneficiarydetails.fd = false;
                     }
                 } else {
                     $scope.beneficiarydetails.fd = false;
                 }*/
                ///lifeinsurance
                if (answeredquestions[i].questionid == 23) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.lifeinsurance = true;
                    } else {
                        $scope.beneficiarydetails.lifeinsurance = false;
                    }
                }
                ///healthinsurance
                if (answeredquestions[i].questionid == 26) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.healthinsurance = true;
                    } else {
                        $scope.beneficiarydetails.healthinsurance = false;
                    }
                }
                ///loan
                if (answeredquestions[i].questionid == 31) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.loan = true;
                    } else {
                        $scope.beneficiarydetails.loan = false;
                    }
                }
                ///onart
                if (answeredquestions[i].questionid == 6) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.onart = true;
                    } else {
                        $scope.beneficiarydetails.onart = false;
                    }
                }
                ///vulnerablemember
                /* if ($scope.AnalyticsBeneficiary.vulnerability_count > 0) {
                     $scope.beneficiarydetails.vulnerablemember = true;
                 } else {
                     $scope.beneficiarydetails.vulnerablemember = false;
                 }*/
                ///financialliteracy
                if (answeredquestions[i].questionid == 17) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.financialliteracy = true;
                    } else {
                        $scope.beneficiarydetails.financialliteracy = false;
                    }
                }
                ///financialplanning
                if (answeredquestions[i].questionid == 34) {
                    if (answeredquestions[i].answer.toLowerCase() == 'yes') {
                        $scope.beneficiarydetails.financialplanning = true;
                    } else {
                        $scope.beneficiarydetails.financialplanning = false;
                    }
                }
            }
        });
    });
});
