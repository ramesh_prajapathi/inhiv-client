'use strict';

angular.module('secondarySalesApp')
    .controller('TransferCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        $scope.transfer = {};
        $scope.validatestring = "";
        $scope.showValidation = false;
        $scope.disableInitiateTransfer = true;
        $scope.showFacility = false;
        $scope.employees = Restangular.all('employees?filter[where][deleteflag]=false').getList().$object;
        $scope.getMemberDetails = function () {
            $scope.disableInitiateTransfer = true;
            if ($scope.transfer.avahanid == null || $scope.transfer.avahanid == "" || $scope.transfer.avahanid == undefined) {
                $scope.validatestring = "Enter AvahanId";
                $scope.showValidation = !$scope.showValidation;
            } else {
                Restangular.all("beneficiaries?filter[where][avahanid]=" + $scope.transfer.avahanid).getList().then(function (members) {
                    if (members.length > 0) {
                        $scope.disableInitiateTransfer = false;
                        $("#demo").collapse('show');
                        $scope.beneficiary = members[0];

                        Restangular.one('fieldworkers', $scope.beneficiary.fieldworker).get().then(function (fwget) {
                            $scope.beneficiary.fieldworkername = fwget.firstname;
                        });

                        Restangular.one('employees', $scope.beneficiary.facility).get().then(function (employee) {
                            $scope.beneficiary.facilityName = employee.firstName;
                        });

                        Restangular.one('distribution-routes', $scope.beneficiary.site).get().then(function (site) {
                            $scope.beneficiary.siteName = site.name;
                        });

                        Restangular.one('genders', $scope.beneficiary.gender).get().then(function (gender) {
                            $scope.beneficiary.genderName = gender.name;
                        });

                    } else {
                        $scope.validatestring = "No Record found with this AvahanId";
                        $scope.showValidation = !$scope.showValidation;
                        $scope.disableInitiateTransfer = true;
                    }
                });
            }
        };

        $scope.inviteTransfer = function () {
            $scope.showFacility = true;
        };

        $scope.confirmTransfer = function () {
            $scope.beneficiary.transferred_from = $scope.beneficiary.id;
            delete $scope.beneficiary.id;
            delete $scope.beneficiary.avahanid;
            $scope.beneficiary.facility = $scope.transfer.facility;
            $scope.beneficiary.site = $scope.transfer.site;
            $scope.beneficiary.fieldworker = $scope.transfer.fieldworker;
            $scope.beneficiary.completedpillar = 0;
            $scope.beneficiary.completedquestion = 0;
            $scope.beneficiary.monthoflasthivtest = null;
            $scope.beneficiary.monthoflaststitest = null;
            $scope.beneficiary.sourceofinfection = 0;
            $scope.beneficiary.savings = false;
            $scope.beneficiary.insurance = false;
            $scope.beneficiary.pension = false;
            $scope.beneficiary.credit = false;
            $scope.beneficiary.remitance = false;
            $scope.beneficiary.monthoflastsaving = null;
            $scope.beneficiary.startofsexwork = null;
            $scope.beneficiary.monthofdetection = null;
            $scope.beneficiary.typeoftransmission = null;
            $scope.beneficiary.noofsavingaccounts = 0;
            $scope.beneficiary.noofinsuranceproducts = 0;
            $scope.beneficiary.noofpensionproducts = 0;
            $scope.beneficiary.noofinvestmentproducts = 0;
            $scope.beneficiary.noofinformalsavingsources = 0;
            $scope.beneficiary.noofinformalcreditsources = 0;
            $scope.beneficiary.noofformalcreditsources = 0;
            $scope.beneficiary.finplansavings = false;
            $scope.beneficiary.finplaninsurance = false;
            $scope.beneficiary.finplanpension = false;
            $scope.beneficiary.finplancredit = false;
            $scope.beneficiary.finplanremitance = false;
            $scope.beneficiary.dateofreciept = null;
            $scope.beneficiary.lastmeetingdate = null;
            $scope.beneficiary.condomsasked = null;
            $scope.beneficiary.condomsprovided = null;
            $scope.beneficiary.championproposed = false;
            $scope.beneficiary.dateprovided = null;
            $scope.beneficiary.consent = false;
            $scope.beneficiary.hotspot = $scope.hotspots[0].name;
            $scope.beneficiary.town = $scope.towns[0].id;
            $scope.beneficiary.state = $scope.transfer.state;
            $scope.beneficiary.district = $scope.transfer.district;
            Restangular.all('beneficiaries').post($scope.beneficiary).then(function (saveResponse) {
                $scope.updateBeneficiary = {
                    "id": saveResponse.transferred_from,
                    "transferred_to": saveResponse.id,
                    "transferred_flag": true,
                    "deleteflag": true
                };
                Restangular.one('beneficiaries', saveResponse.transferred_from).customPUT($scope.updateBeneficiary).then(function (updateResponse) {
                    $location.path('/transfer_form/'+saveResponse.id);
                });
            });
        };

        $scope.$watch('transfer.facility', function (newValue, oldValue) {
            if (newValue === oldValue) {
                return;
            } else {
                Restangular.one('employees', newValue).get().then(function (facility) {
                    $scope.transfer.state = facility.stateId;
                    $scope.transfer.district = facility.district;
                    $scope.sites = Restangular.all('distribution-routes?filter[where][partnerId]=' + newValue).getList().$object;
                    $scope.towns = Restangular.all('cities?filter[where][district]=' + facility.district).getList().$object;
                    $scope.hotspots = Restangular.all('hotspots?filter[where][state]=' + facility.stateId + '&filter[where][district]=' + facility.district + '&filter[where][facility]=' + facility.id).getList().$object;
                });
            }
        });

        $scope.$watch('transfer.site', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == null) {
                return;
            } else Restangular.all('routelinks?filter[where][distributionRouteId]=' + newValue).getList().then(function (routeResponse) {
                Restangular.one('fieldworkers', routeResponse[0].partnerId).get().then(function (fwget) {
                    $scope.transfer.fieldworker = fwget.id;
                });
            });
        });
    });