'use strict';

angular.module('secondarySalesApp')
    .controller('IctcCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

        $scope.isCreateView = true;
        $scope.modalTitle = 'Thank You';
        $scope.message = 'ICTC has been Created!';
        //        $scope.fscodedisable = false;
        $scope.disableLang = false;


        $scope.ictc = {
            createdBy: $window.sessionStorage.userId,
            createdDate: new Date(),
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedDate: new Date(),
            lastModifiedByRole: $window.sessionStorage.roleId,
            usercreated: false,
            deleteflag: false
        };


        $scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;


        $scope.showenglishLang = true;

        $scope.$watch('ictc.languageId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {

                $scope.ictc.name = '';
                $scope.ictc.parentId = '';
                $scope.ictc.code = '';
                $scope.ictc.latitude = '';
                $scope.ictc.latitude = '';
                $scope.ictc.longitude = '';
                $scope.ictc.address = '';
                $scope.ictc.countryId = '';
                $scope.ictc.state = '';
                $scope.ictc.district = '';
                $scope.ictc.town = '';

                if (newValue + "" != "1") {
                    $scope.disableorder = true;
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.disableorder = false;
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });
        $scope.ictc.parentId = '';
        $scope.$watch('ictc.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.disableorder = true;

                Restangular.one('ictccenters', newValue).get().then(function (zn) {
                    console.log("zn", zn)
                    $scope.ictc.code = zn.code;
                    $scope.ictc.latitude = zn.latitude;
                    $scope.ictc.latitude = zn.latitude;
                    $scope.ictc.longitude = zn.longitude;
                    $scope.ictc.address = zn.address;
                    $scope.ictc.countryId = zn.countryId;
                    Restangular.all('zones?filter[where][id]=' + zn.state).getList().then(function (respState) {
                        $scope.zones = respState;
                        $scope.ictc.state = zn.state;
                    });
                    Restangular.all('sales-areas?filter[where][id]=' + zn.district).getList().then(function (respDist) {
                        $scope.salesareas = respDist;
                        $scope.ictc.district = zn.district;

                    });
                    Restangular.all('cities?filter[where][id]=' + zn.town).getList().then(function (respTown) {
                        $scope.cities = respTown;
                        $scope.ictc.town = zn.town;
                    });

                });
            }
        });





        $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;


        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('ictccenters?filter[where][languageId]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.ictccenters = mt;
                angular.forEach($scope.ictccenters, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }

                    /*var data14 = $scope.Displaylanguages.filter(function (arr) {
                        return arr.id == member.languageId
                    })[0];

                    if (data14 != undefined) {
                        member.langName = data14.name;
                    }*/
                    member.index = index + 1;
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.medilanguages = sev;
            });

        } else {

            Restangular.all('ictccenters').getList().then(function (mt) {
                $scope.ictccenters = mt;
                angular.forEach($scope.ictccenters, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }

                    /* var data14 = $scope.Displaylanguages.filter(function (arr) {
                         return arr.id == member.languageId
                     })[0];

                     if (data14 != undefined) {
                         member.langName = data14.name;
                     }*/
                    member.index = index + 1;
                });
            });


            Restangular.all('ictccenters?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;

                        angular.forEach($scope.medilanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;
                    });
                }
            });
        }

        /***new changes*****/

        Restangular.all('ictccenters?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.disaplyictccenetrs = zn;
        });

        var timeoutPromise;
        var delayInMs = 1000;

        $scope.orderChange = function (serialno) {
            console.log('clicked');

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.disaplyictccenetrs.filter(function (arr) {
                    return arr.code == serialno
                })[0];
                console.log('data', data);

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.ictc.languageId != '') {
                    $scope.ictc.code = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'ICTC Code Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };


        $("#code").keydown(function (e) {
            var k = e.keyCode || e.which;
            var ok = k >= 65 && k <= 90 || // A-Z
                k >= 96 && k <= 105 || // a-z
                k >= 35 && k <= 40 || // arrows
                k == 9 || //tab
                k == 46 || //del
                k == 8 || // backspaces
                (!e.shiftKey && k >= 48 && k <= 57); // only 0-9 (ignore SHIFT options)

            if (!ok || (e.ctrlKey && e.altKey)) {
                e.preventDefault();
            }
        });


        /********************* WATCH ****************************/


        $scope.$watch('ictc.countryId', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {

                $scope.ictc.state = '';
                $scope.ictc.district = '';
                $scope.ictc.town = '';

                Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (znResp) {
                    $scope.zones = znResp;
                    $scope.ictc.state = $scope.ictc.state;
                });
            }
            $scope.countryid = +newValue;
        });

        $scope.$watch('ictc.state', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.ictc.district = '';
                $scope.ictc.town = '';

                Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (stResp) {
                    $scope.salesareas = stResp;
                    $scope.ictc.district = $scope.ictc.district;
                });
            }
            $scope.stateid = +newValue;
        });

        $scope.$watch('ictc.district', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.ictc.town = '';

                Restangular.all('cities?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyResp) {
                    $scope.cities = ctyResp;
                    $scope.ictc.town = $scope.ictc.town;
                });
            }
            $scope.districtid = +newValue;
        });

        $scope.CheckDuplicate = function (name) {
            // console.log('click')
            var CheckName = name.toUpperCase();

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.ictccenters.filter(function (arr) {
                    return arr.name == CheckName
                })[0];
                // console.log(data , CheckName);
                if (data != undefined) {
                    $scope.ictcname = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'ICTC Name Already Exist';

                }
            }, delayInMs);
        };

        /********************* WATCH ****************************/

        /************* SAVE *******************************************/
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";
            document.getElementById('latitude').style.border = "";
            document.getElementById('longitude').style.border = "";

            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
            /*if ($scope.ictc.languageId == '' || $scope.ictc.languageId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            }*/
            //else if ($scope.ictc.languageId == 1) {
            if ($scope.ictc.countryId == '' || $scope.ictc.countryId == null) {
                $scope.ictc.countryId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            } else if ($scope.ictc.state == '' || $scope.ictc.state == null) {
                $scope.ictc.state == '';
                $scope.validatestring = $scope.validatestring + 'Please Select State';
            } else if ($scope.ictc.district == '' || $scope.ictc.district == null) {
                $scope.ictc.district == '';
                $scope.validatestring = $scope.validatestring + 'Please Select District';
            } else if ($scope.ictc.town == '' || $scope.ictc.town == null) {
                $scope.ictc.town == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Town';
            } else if ($scope.ictcname == '' || $scope.ictcname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Ictc Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } else if (!regEmail.test($scope.ictcname)) {
                $scope.ictcname = '';
                $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                document.getElementById('name').style.border = "1px solid #ff0000";
            } else if ($scope.ictc.code == '' || $scope.ictc.code == null) {
                document.getElementById('code').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter ictc Code';

            } else if ($scope.ictc.latitude == '' || $scope.ictc.latitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter latitude';
                document.getElementById('latitude').style.borderColor = "#FF0000";

            } else if ($scope.ictc.longitude == '' || $scope.ictc.longitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter longitude';
                document.getElementById('longitude').style.borderColor = "#FF0000";

            }


            //} 
            /*else if ($scope.ictc.languageId != 1) {

                if ($scope.ictc.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select ART Name in English';

                } else if ($scope.ictc.name == '' || $scope.ictc.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Ictc Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }
            }*/

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {

                var xyz = $scope.ictcname;
                $scope.ictc.name = xyz.toUpperCase();

                /*$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                //toaster.pop('success', "State has been created", null, null, 'trustedHtml');
                Restangular.all('ictccenters').post($scope.ictc).then(function (znResponse) {
                    console.log('znResponse', znResponse);
                    window.location = '/ictccenter';*/


                $scope.submitDisable = true;

                $scope.ictccenters.post($scope.ictc).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/ictccenter';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;

                });


            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /************************** Map *************************/

        $scope.mapdataModal = false;

        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.ictc.latitude = latLng.lat();
                $scope.ictc.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
                // $scope.ictc.address = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };

        $scope.CLOSEBUTTON1 = function () {
            console.log('cancle');
            $scope.ictc.latitude = '';
            $scope.ictc.longitude = '';
            $scope.mapdataModal = false;
        }

    })
    .directive('mapmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-lg">' + '<div class="modal-content">' + '<div class="">' +
                // '<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
