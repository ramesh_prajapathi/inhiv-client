'use strict';
angular.module('secondarySalesApp').controller('OneToOneCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $modal, $filter, $compile) {
    /*********/
    //console.log('Loading');
    $window.sessionStorage.MemberFilter = '';
   if($window.sessionStorage.previous == '/onetoone/:id'){
       $window.sessionStorage.previous = '';
          window.location = '/onetoone/' + $routeParams.id;
    }
    Restangular.one('beneficiaries?filter[where][id]=' + $routeParams.id).get().then(function (benResp) {
        if (benResp.length > 0) {
            $scope.selectedBeneficiary = benResp[0];
        } else {
            $scope.selectedBeneficiary = {
                stress_data: false
            };
        }
    });

    $scope.PresentCompletedPillar = 0;
    $scope.PresentCompletedQuestion = 0;

    $scope.DisableSave = true;
    $scope.hideButton = true;
    $scope.showAns = false;
    /*********************************** Pagination *******************************************/
    if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
        $window.sessionStorage.myRoute = null;
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    if ($window.sessionStorage.prviousLocation != "partials/zones") {
        $window.sessionStorage.myRoute_currentPage = 1;
        $window.sessionStorage.myRoute_currentPagesize = 25;
    }
    $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
    $scope.PageChanged = function (newPage, oldPage) {
        $scope.currentpage = newPage;
        $window.sessionStorage.myRoute_currentPage = newPage;
    };
    $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
    $scope.pageFunction = function (mypage) {
        $scope.pageSize = mypage;
        $window.sessionStorage.myRoute_currentPagesize = mypage;
    };
    /*********************************** INDEX *******************************************/
    
      

    $scope.UserLanguage = $window.sessionStorage.language;
    if ($routeParams.id != undefined) {
        $scope.DisplayBenef = Restangular.one('beneficiaries', $routeParams.id).get().then(function (beneficiary) {
            $scope.DisplayBeneficiary = beneficiary;
            $scope.DisplayBeneficiary.dob = $scope.DisplayBeneficiary.dob.split('T')[0];
           /* if (beneficiary.dynamicmember == true) {
                $scope.StringActiveMember = 'YES';
            } else {
                $scope.StringActiveMember = 'NO';
            }
            if (beneficiary.paidmember == true) {
                $scope.StringPaidMember = 'YES';
            } else {
                $scope.StringPaidMember = 'NO';
            }
            if (beneficiary.plhiv == true) {
                $scope.StringPlhiv = 'YES';
            } else {
                $scope.StringPlhiv = 'NO';
            }

            if (beneficiary.consent == true) {
                $scope.benconsent = 'YES';
            } else {
                $scope.benconsent = 'NO';
            }*/
            Restangular.one('employees', beneficiary.facility).get().then(function (comember) {
                $scope.Cohelpline = comember.mobile;
            });
            $scope.remainingschemes = Restangular.all('schemes?filter[where][state]=' + $scope.DisplayBeneficiary.state + '&filter[where][topscheme]=null').getList().$object;
            $scope.documenttypes = Restangular.all('documenttypes').getList().$object; 
            
             Restangular.one('typologies', beneficiary.typology).get().then(function(resp){
                  $scope.StringTypology = resp.name;
             });
            
        });
    }
    $scope.DisableHealthPrev = false;
    $scope.showMoreDetails = true;
    $scope.showMoreDetails = true;
    $scope.displayHealthPrev = false;

    $scope.MoreDetails = function () {
        $scope.showMoreDetails = !$scope.showMoreDetails;
    };

    $scope.todo = {
        status: 8,
        stateid: $window.sessionStorage.zoneId,
        state: $window.sessionStorage.zoneId,
        countryId: $window.sessionStorage.countryId,
        town: $window.sessionStorage.distributionAreaId,
        districtid: $window.sessionStorage.salesAreaId,
        district: $window.sessionStorage.salesAreaId,
        coid: $window.sessionStorage.coorgId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        roleId: $window.sessionStorage.roleId
    };
    $scope.TotalTodos = [];
    var sevendays = new Date();
    sevendays.setDate(sevendays.getDate() + 7);
    $scope.todo.datetime = sevendays;
    $scope.fs = {};
    $scope.health = {};
    $scope.sp = {};
    $scope.phonetype = {};


    $scope.follow = {};
    $scope.picker = {};
    $scope.cdids = {};
    $scope.schememaster = {};
    $scope.plhiv = {};
    $scope.condom = {};
    $scope.finliteracy = {};
    $scope.finplanning = {};
    $scope.increment = {};
    $scope.beneficiarycondom = {};
    //////////////////////////////////////////////////////////////////////////////////////////
    $scope.todotyp = Restangular.all('todotypes').getList().then(function (todotyp) {
        $scope.maintodotypes = todotyp;
    });
    $scope.todostat = Restangular.all('todostatuses').getList().then(function (todostat) {
        $scope.maintodostatuses = todostat;
    });

    /****************************** Warning Message ************************/


    //////////////////////////////////All Tasks End///////////////////////
    $scope.TodayDate = $filter('date')(new Date(), 'yyyy-MM-dd') + 'T00:00:00.000Z';
    $scope.healthquestions = [];
    $scope.healthquestioncount = 0;

    $scope.healthLoaded = false;

/*************new popup for final answer*******/    
     $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };
    
     $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/';

        };

    
    /*************** Warning ****************/

    $scope.healthques = Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[order]=serialno%20ASC' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (surveyques) {
        //console.log('partner',partner);
        for (var i = 0; i < surveyques.length; i++) {
            $scope.healthdata = {};

            if ($window.sessionStorage.language == 1) {
               // console.log('i m in if');
                $scope.healthdata['question'] = surveyques[i].question;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].yesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].noprovideinfo;
                $scope.healthdata['id'] = surveyques[i].id;
            } else if ($window.sessionStorage.language != 1) {
                //console.log('i m in else');
                $scope.healthdata['question'] = surveyques[i].question;
                $scope.healthdata['answers'] = surveyques[i].answeroptions;
                $scope.healthdata['yesprovideinfo'] = surveyques[i].yesprovideinfo;
                $scope.healthdata['noprovideinfo'] = surveyques[i].noprovideinfo;
                $scope.healthdata['id'] = surveyques[i].parentId;
            }
            /****************************Suman Changes End***************************************/
            $scope.healthdata['questiontype'] = surveyques[i].questiontype;
            $scope.healthdata['noofdropdown'] = surveyques[i].noofoptions;
            $scope.healthdata['yesaction'] = surveyques[i].yesaction;
            $scope.healthdata['noaction'] = surveyques[i].noaction;
            $scope.healthdata['yespopup'] = surveyques[i].yespopup;
            $scope.healthdata['nopopup'] = surveyques[i].nopopup;
            $scope.healthdata['yesskipto'] = surveyques[i].yesskipto;
            $scope.healthdata['noskipto'] = surveyques[i].noskipto;
            $scope.healthdata['schemeslno'] = surveyques[i].schemeslno;
            $scope.healthdata['serialno'] = surveyques[i].serialno;
            $scope.healthdata['yesincrement'] = surveyques[i].yesincrement;
            $scope.healthdata['noincrement'] = surveyques[i].noincrement;
            $scope.healthdata['yestodotype'] = surveyques[i].yestodotype;
            $scope.healthdata['notodotype'] = surveyques[i].notodotype;
            $scope.healthdata['yesdocumentflag'] = surveyques[i].yesdocumentflag;
            $scope.healthdata['nodocumentflag'] = surveyques[i].nodocumentflag;
            $scope.healthdata['yesdocumentid'] = surveyques[i].yesdocumentid;
            $scope.healthdata['nodocumentid'] = surveyques[i].nodocumentid;
            /******************************New Changes *******************************/
            $scope.healthdata['teansweroptions'] = surveyques[i].teansweroptions;
            $scope.healthdata['hnansweroptions'] = surveyques[i].hnansweroptions;
            $scope.healthdata['kaansweroptions'] = surveyques[i].kaansweroptions;
            $scope.healthdata['taansweroptions'] = surveyques[i].taansweroptions;
            $scope.healthdata['maansweroptions'] = surveyques[i].maansweroptions;
            $scope.healthdata['skipquestion'] = surveyques[i].skipquestion;
            $scope.healthdata['skipon'] = surveyques[i].skipon;
            $scope.healthdata['skipondate'] = surveyques[i].skipondate;
            $scope.healthdata['skipfield'] = surveyques[i].skipfield;
            $scope.healthdata['skiponfield'] = surveyques[i].skiponfield;
            $scope.healthdata['skipdate'] = surveyques[i].skipdate;
              $scope.healthdata['skiponTypology'] = surveyques[i].skiponTypology;
              $scope.healthdata['skiponTypologyId'] = surveyques[i].skiponTypologyId;
            $scope.healthdata['skiponfieldvalue'] = surveyques[i].skiponfieldvalue;
              $scope.healthdata['range'] = surveyques[i].range;
            $scope.healthquestions.push($scope.healthdata);

            if (i == surveyques.length - 1) {
                console.log('i == length', i , surveyques.length - 1);
                 $scope.questioncount = surveyques.length;
                 $scope.finalcount = $scope.healthquestioncount;

                $scope.healthsurveyquestion = $scope.healthquestions[$scope.healthquestioncount];
              //  console.log('$scope.healthsurveyquestion', $scope.healthsurveyquestion);
                $scope.healthLoaded = true;
                
                if ($scope.healthsurveyquestion.skiponTypology == true ) {
                   // console.log('skipTypology');
                    
                    if ($scope.healthsurveyquestion.skiponTypologyId != $scope.DisplayBeneficiary.typology) {
                        $scope.healthnextquestion();
                    }
                }

                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).getList().then(function (answered) {
                   // console.log(' first health answered', answered);
                    if(answered.length == 0) {
                         $scope.showAns = false;
                      //  console.log('not answered');
                        $scope.modalInstanceLoad.close();
                    }
                   else if (answered.length > 0) {
                         $scope.showAns = true;
                        /******************************New Changes *******************************/
                       if(answered.length == 1) {
                          // $scope.answerd = answered[0].answer;
                       
                       $scope.surveyanswerDisplay = answered[0].answer;
                        if ($scope.healthsurveyquestion.skipdate == true) {
                            console.log('skipdate');  
                            if (answered[0].answer === 'yes' || answered[0].answer != null) {
                                var today_date = new Date();
                                var old_date = new Date(answered[0].lastupdatedtime);
                                var xyz = today_date - old_date;
                                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                                var total_round_days = Math.trunc(total_days);
                                //console.log('xyz', xyz);
                                //console.log('total_round_days', total_round_days);
                                if (total_round_days < $scope.healthsurveyquestion.skipondate) {
                                    $scope.healthnextquestion();
                                }
                                else{
                                  
                                     $scope.modalInstanceLoad.close();
                                }
                            }
                        } else {
                           
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = enganswers[ansindex];

                            } else if ($window.sessionStorage.language != 1) {
                                var hindianswers = [];
                                if ($scope.healthquestions[$scope.healthquestioncount].parentFlag == false) {
                                    hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                }
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                                $scope.healthsurveyanswer = hindianswers[ansindex];
                            }
                        }
                       } else if(answered.length > 1) {
                            $scope.surveyanswerDisplay = answered[answered.length-1].answer;
                           // $scope.answerd =  answered[answered.length-1].answer;
                               if ($scope.healthsurveyquestion.skipdate == true) {
                            console.log('skipdate');  
                            if (answered[answered.length-1].answer === 'yes' || answered[answered.length-1].answer != null) {
                                var today_date = new Date();
                                var old_date = new Date(answered[answered.length-1].lastupdatedtime);
                                var xyz = today_date - old_date;
                                var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                                var total_round_days = Math.trunc(total_days);
                                //console.log('xyz', xyz);
                                //console.log('total_round_days', total_round_days);
                                if (total_round_days < $scope.healthsurveyquestion.skipondate) {
                                    $scope.healthnextquestion();
                                }
                                else{
                                  
                                     $scope.modalInstanceLoad.close();
                                }
                            }
                        } 
                           else {
                           
                            if ($window.sessionStorage.language == 1) {
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[answered.length-1].answer.toUpperCase());
                                $scope.healthsurveyanswer = enganswers[ansindex];

                            } else if ($window.sessionStorage.language != 1) {
                                var hindianswers = [];
                                if ($scope.healthquestions[$scope.healthquestioncount].parentFlag == false) {
                                    hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                } else {
                                    hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                }
                                var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                                var ansindex = enganswers.indexOf(answered[answered.length-1].answer.toUpperCase());
                                $scope.healthsurveyanswer = hindianswers[ansindex];
                            }
                        }
                       }
                      /* else if ($scope.healthsurveyquestion.skipquestion == true && $scope.healthsurveyquestion.skipon == answered[0].answer) {
                            $scope.healthnextquestion();
                        }*/
                       
                    }
                });


                $scope.DisableSubmit = false;
                $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, '', "healthanswers", "healthradio", $scope.healthquestions[$scope.healthquestioncount].yesaction, $scope.healthquestions[$scope.healthquestioncount].noaction, $scope.healthquestions[$scope.healthquestioncount].yespopup, $scope.healthquestions[$scope.healthquestioncount].nopopup, $scope.healthquestions[$scope.healthquestioncount].yesskipto, $scope.healthquestions[$scope.healthquestioncount].noskipto, $scope.healthquestions[$scope.healthquestioncount].schemeslno, $scope.healthquestions[$scope.healthquestioncount].serialno, $scope.healthquestions[$scope.healthquestioncount].yesincrement, $scope.healthquestions[$scope.healthquestioncount].noincrement, $scope.healthquestions[$scope.healthquestioncount].yesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].noprovideinfo, $scope.healthquestions[$scope.healthquestioncount].question, $scope.healthquestions[$scope.healthquestioncount].yestodotype, $scope.healthquestions[$scope.healthquestioncount].notodotype, $scope.healthquestions[$scope.healthquestioncount].yesdocumentflag, $scope.healthquestions[$scope.healthquestioncount].nodocumentflag, $scope.healthquestions[$scope.healthquestioncount].yesdocumentid, $scope.healthquestions[$scope.healthquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.healthquestions[$scope.healthquestioncount].hnyesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnnoprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnquestion,
                $scope.healthquestions[$scope.healthquestioncount].range
                    /****************************Suman Changes End****************************************/
                );
                
            }
        }
    });
    /************************************************** healthnextquestion *************************/
    $scope.healthnextquestion = function () {
        //console.log('$scope.healthnextquestion', $scope.healthquestioncount, $scope.healthquestions.length)
        $scope.DisableHealthPrev = false;
        $scope.healthquestioncount++;
         // console.log('health nextques1', $scope.healthsurveyquestion);
        console.log('health nextques1', $scope.healthquestions.length , $scope.healthquestioncount);
       $scope.finalcount = $scope.healthquestioncount;
       $scope.questioncount = $scope.healthquestions.length;
        
       if ($scope.healthquestioncount < $scope.healthquestions.length) {
            $scope.healthsurveyquestion = $scope.healthquestions[$scope.healthquestioncount];
           //  console.log('health nextques1', $scope.healthsurveyquestion);
             if ($scope.healthsurveyquestion.skiponTypology == true ) {
                   //console.log('skipTypology');
                    if ($scope.healthsurveyquestion.skiponTypologyId != $scope.DisplayBeneficiary.typology) {
                        $scope.healthnextquestion();
                    }
                }

            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).getList().then(function (answered) {
                //console.log('next question',answered);
                if(answered.length == 0){
                   // console.log('next question not answerd');
                   $scope.modalInstanceLoad.close();
                    $scope.showAns = false;
                }
               else if (answered.length > 0) {
                     $scope.showAns = true;
                     /******************************New Changes *******************************/
                       if(answered.length == 1) {
                       $scope.surveyanswerDisplay = answered[0].answer;
                         //   $scope.answerd =  answered[0].answer;
                    if ($scope.prevquestionClick == true) {
                        $scope.healthquestioncount--;
                        $scope.prevquestionClick = false
                    } else if ($scope.healthsurveyquestion.skipdate == true) {
                        
                        if (answered[0].answer === 'yes'  || answered[0].answer != null) {
                            var today_date = new Date();
                            var old_date = new Date(answered[0].lastupdatedtime);
                            var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                            var total_round_days = Math.trunc(total_days);
                            if (total_round_days < $scope.healthsurveyquestion.skipondate) {
                                $scope.healthnextquestion();
                            }
                             else{
                                     $scope.modalInstanceLoad.close();
                                }
                        }
                    } 
                    /*else if ($scope.healthsurveyquestion.skipquestion == true) {
                        $scope.healthnextquestion();
                    } */
                   
                    else {
                      
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = enganswers[ansindex];
                         //   console.log('$scope.healthsurveyanswer11Prev', $scope.healthsurveyanswer)
                        } else if ($window.sessionStorage.language != 1) {

                            var hindianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].parentFlag == false) {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = hindianswers[ansindex];

                        }
                    }
                    }
                    else if(answered.length > 1) {
                       //  $scope.answerd =  answered[answered.length-1].answer;
                       $scope.surveyanswerDisplay = answered[answered.length-1].answer;
                    if ($scope.prevquestionClick == true) {
                        $scope.healthquestioncount--;
                        $scope.prevquestionClick = false
                    } else if ($scope.healthsurveyquestion.skipdate == true) {
                        
                        if (answered[answered.length-1].answer === 'yes'  || answered[answered.length-1].answer != null) {
                            var today_date = new Date();
                            var old_date = new Date(answered[answered.length-1].lastupdatedtime);
                            var total_days = (today_date - old_date) / (1000 * 60 * 60 * 24);
                            var total_round_days = Math.trunc(total_days);
                            if (total_round_days < $scope.healthsurveyquestion.skipondate) {
                                $scope.healthnextquestion();
                            }
                             else{
                                     $scope.modalInstanceLoad.close();
                                }
                        }
                    } 
                    /*else if ($scope.healthsurveyquestion.skipquestion == true) {
                        $scope.healthnextquestion();
                    } */
                   
                    else {
                      
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[answered.length-1].answer.toUpperCase());
                            $scope.healthsurveyanswer = enganswers[ansindex];
                         //   console.log('$scope.healthsurveyanswer11Prev', $scope.healthsurveyanswer)
                        } else if ($window.sessionStorage.language != 1) {

                            var hindianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].parentFlag == false) {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[answered.length-1].answer.toUpperCase());
                            $scope.healthsurveyanswer = hindianswers[ansindex];

                        }
                    }
                    }
                }
            });
           // $scope.modalInstanceLoad.close();

            var mydiv = document.getElementById("healthanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, '', "healthanswers", "healthradio", $scope.healthquestions[$scope.healthquestioncount].yesaction, $scope.healthquestions[$scope.healthquestioncount].noaction, $scope.healthquestions[$scope.healthquestioncount].yespopup, $scope.healthquestions[$scope.healthquestioncount].nopopup, $scope.healthquestions[$scope.healthquestioncount].yesskipto, $scope.healthquestions[$scope.healthquestioncount].noskipto, $scope.healthquestions[$scope.healthquestioncount].schemeslno, $scope.healthquestions[$scope.healthquestioncount].serialno, $scope.healthquestions[$scope.healthquestioncount].yesincrement, $scope.healthquestions[$scope.healthquestioncount].noincrement, $scope.healthquestions[$scope.healthquestioncount].yesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].noprovideinfo, $scope.healthquestions[$scope.healthquestioncount].question, $scope.healthquestions[$scope.healthquestioncount].yestodotype, $scope.healthquestions[$scope.healthquestioncount].notodotype, $scope.healthquestions[$scope.healthquestioncount].yesdocumentflag, $scope.healthquestions[$scope.healthquestioncount].nodocumentflag, $scope.healthquestions[$scope.healthquestioncount].yesdocumentid, $scope.healthquestions[$scope.healthquestioncount].nodocumentid, $scope.healthquestions[$scope.healthquestioncount].hnyesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnnoprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnquestion,
                $scope.healthquestions[$scope.healthquestioncount].range);
        } else {
            $scope.healthquestioncount = $scope.healthquestions.length - 1;
           // console.log( $scope.healthquestioncount)
            $scope.healthsurveyquestion = $scope.healthquestions[$scope.healthquestioncount];
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + $scope.healthquestions[$scope.healthquestioncount].id).getList().then(function (answered) {
               // console.log('third', answered)
                if(answered.length == 0){
                     $scope.showAns = false;
                   // console.log('next question not answerd');
                   $scope.modalInstanceLoad.close();
                }
               else if (answered.length > 0) {
                     $scope.showAns = true;
                       $scope.surveyanswerDisplay = answered[0].answer;
                    if ($scope.healthsurveyquestion.skipquestion == true && $scope.healthsurveyquestion.skipon == answered[0].answer) {
                        $scope.healthnextquestion();
                    } else {
                       
                        /******************************New Changes *******************************/
                        if ($window.sessionStorage.language == 1) {
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = enganswers[ansindex];
                        } else if ($window.sessionStorage.language == 2) {

                            var hindianswers = [];
                            if ($scope.healthquestions[$scope.healthquestioncount].parentFlag == false) {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            } else {
                                hindianswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            }
                            var enganswers = $scope.healthquestions[$scope.healthquestioncount].answers.split(',');
                            var ansindex = enganswers.indexOf(answered[0].answer.toUpperCase());
                            $scope.healthsurveyanswer = hindianswers[ansindex];
                            //console.log('$scope.healthsurveyanswer3', $scope.healthsurveyanswer);
                        }
                    }
                }
            });
            var mydiv = document.getElementById("healthanswers");
            mydiv.innerHTML = '';
            $scope.DisableSubmit = false;
            $scope.ChangeClient($scope.healthquestions[$scope.healthquestioncount].id, '', "healthanswers", "healthradio", $scope.healthquestions[$scope.healthquestioncount].yesaction, $scope.healthquestions[$scope.healthquestioncount].noaction, $scope.healthquestions[$scope.healthquestioncount].yespopup, $scope.healthquestions[$scope.healthquestioncount].nopopup, $scope.healthquestions[$scope.healthquestioncount].yesskipto, $scope.healthquestions[$scope.healthquestioncount].noskipto, $scope.healthquestions[$scope.healthquestioncount].schemeslno, $scope.healthquestions[$scope.healthquestioncount].serialno, $scope.healthquestions[$scope.healthquestioncount].yesincrement, $scope.healthquestions[$scope.healthquestioncount].noincrement, $scope.healthquestions[$scope.healthquestioncount].yesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].noprovideinfo, $scope.healthquestions[$scope.healthquestioncount].question, $scope.healthquestions[$scope.healthquestioncount].yestodotype, $scope.healthquestions[$scope.healthquestioncount].notodotype, $scope.healthquestions[$scope.healthquestioncount].yesdocumentflag, $scope.healthquestions[$scope.healthquestioncount].nodocumentflag, $scope.healthquestions[$scope.healthquestioncount].yesdocumentid, $scope.healthquestions[$scope.healthquestioncount].nodocumentid, /****************************Suman Changes Start****************************************/ $scope.healthquestions[$scope.healthquestioncount].hnyesprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnnoprovideinfo, $scope.healthquestions[$scope.healthquestioncount].hnquestion,
            $scope.healthquestions[$scope.healthquestioncount].range
                /****************************Suman Changes End****************************************/
            );
//$scope.modalInstanceLoad.close();
        }
        //$scope.modalInstanceLoad.close();
    };
    /****************************************************** healthprevquestion ***************/
    $scope.prevquestionClick = false;
   


    ///////////////////////IDS END/////////////////////////////
    $scope.submitsurveyanswers = Restangular.all('surveyanswers');
    $scope.submitallsurveyanswers = Restangular.all('allsurveyanswers');
    $scope.submittodos = Restangular.all('todos');
    $scope.submitreportincidents = Restangular.all('reportincidents');
    $scope.surveyanswer = {};
    $scope.beneficiaryupdate = {};

    $scope.submitdocumentmasters = Restangular.all('schememasters');
    if ($window.sessionStorage.fullName != undefined) {
        $scope.mem = Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (member) {
            //console.log('$window.sessionStorage.fullName', $window.sessionStorage.fullName);
            $scope.memberfullname = member;
            $scope.membername = member.fullname;
        });
    }

    $scope.getSurveyanswers = function (beneficiaryid, questionid) {
        return Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + questionid).getList().$object;
    };

    $scope.getTodoType = function (roleId) {
        return Restangular.one('todotypes', roleId).get().$object;
    };
    $scope.getStatus = function (roleId) {
        return Restangular.one('todostatuses', roleId).get().$object;
    };
    $scope.getCurrentStatusCase = function (roleId) {
        return Restangular.one('currentstatusofcases?filter[where][name]=' + roleId).get().$object;
    };
    $scope.getPillar = function (roleId) {
        return Restangular.one('pillars', roleId).get().$object;
    };
    $scope.getSurQues = function (quesId) {
        return Restangular.one('surveyquestions', quesId).get().$object;
    };
    $scope.getDocumentName = function (quesId) {
        return Restangular.one('documenttypes', quesId).get().$object;
    };
    $scope.getSchemeName = function (quesId) {
        return Restangular.one('schemes', quesId).get().$object;
    };
    $scope.getSchemeDocumentStage = function (stid) {
        return Restangular.one('schemestages', stid).get().$object;
    };


    /****************************************************** Save Answer *********************************/
    $scope.SaveAnswers = function (beneficiaryid, questionid, answer, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, availeddate, referenceno) {

        //console.log('save', beneficiaryid, questionid, answer, serialno);

        $scope.surveyanswer = {};
        if (answer == 'yes') {
            $scope.surveyanswer.documentflag = yesdocumentflag;
            $scope.surveyanswer.documentid = yesdocumentid;
            //console.log('$scope.surveyanswer.documentid1', $scope.surveyanswer.documentid);
            $scope.surveyanswer.schemeid = yesdocumentid;
        } else {
            /// $scope.surveyanswer.documentflag = nodocumentflag;
            // $scope.surveyanswer.documentid = nodocumentid;
            // $scope.surveyanswer.schemeid = nodocumentid;
            //console.log('$scope.surveyanswer.documentid2', $scope.surveyanswer.documentid);
        }
        $scope.surveyanswer.beneficiaryid = beneficiaryid;
        $scope.surveyanswer.questionid = questionid;
        $scope.surveyanswer.answer = answer;
        $scope.surveyanswer.deleteflag = false;
        $scope.surveyanswer.lastupdatedtime = new Date();
        $scope.surveyanswer.lastupdatedby = $window.sessionStorage.UserEmployeeId;
        // $scope.surveyanswer.availeddate = availeddate;
        // $scope.surveyanswer.referenceno = referenceno;
        $scope.surveyanswer.countryId = $window.sessionStorage.countryId;
        $scope.surveyanswer.town = $window.sessionStorage.distributionAreaId;
        $scope.surveyanswer.state = $window.sessionStorage.zoneId;
        $scope.surveyanswer.district = $window.sessionStorage.salesAreaId;
        $scope.surveyanswer.facility = $window.sessionStorage.coorgId;
        $scope.surveyanswer.lastmodifiedbyrole = $window.sessionStorage.roleId;
        $scope.beneficiaryupdate.completedflag = 'yes';
        $scope.beneficiaryupdate.completedquestion = serialno;
        $scope.beneficiaryupdate.lastmeetingdate = new Date();
        //$scope.surveyanswer.stress_data = $scope.selectedBeneficiary.stress_data;
        //console.log(' $scope.surveyanswer', $scope.surveyanswer);

        $scope.submitsurveyanswers.post($scope.surveyanswer).then(function (resp) {
            $scope.submitallsurveyanswers.post($scope.surveyanswer).then(function (resp) {
                //console.log('submitallsurveyanswers',resp);
            });
          //  console.log('answer Saved1', resp);
            // $scope.healthsurveyanswer = resp.answer;
            //  console.log('$scope.healthsurveyanswer Saved1', $scope.healthsurveyanswer)
            $scope.cdids = {};
            Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryupdate).then(function (respo) {
                console.log('answer Saved', respo);
                console.log('all',  $scope.finalcount, $scope.questioncount );
                
                $scope.DisplayBeneficiary = respo;
                if ( $scope.finalcount == $scope.questioncount){
                  // window.location = '/'; 
                    console.log('all',$scope.healthquestions.length  ,$scope.healthquestioncount);
                    $scope.toggleCheck();
                      $scope.validatestring1 = 'All Question Answered Sucessfully';
            } else{
                $scope.healthnextquestion();
            }
                
            });
            
        }, function (error) {
            if (error.data.error.constraint == "pillar_question_beneficiary") {
                // console.log('error', error);
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + questionid).getList().then(function (answered) {
                    if (answered.length > 0) {
                        $scope.surveyanswer.id = answered[0].id;
                        $scope.submitsurveyanswers.customPUT($scope.surveyanswer, answered[0].id).then(function (resp) {
                            // console.log('answer updated', resp);
                            $scope.cdids = {};
                            Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryupdate).then(function (respo) {
                                // console.log('member updated', respo);
                                $scope.DisplayBeneficiary = respo;

                            });
                        });
                    }
                });
            }
        });
    };
    //////////////////////////////////////////////////////////////////////////////////////////
    $scope.openPrevAns = function () {
        if ($window.sessionStorage.language == 1) {
            $scope.printNotapplicable = 'N/A';
            $scope.printFormalSaving = 'Formal Saving';
            $scope.printInFormalSaving = 'Informal Saving';
        } else if ($window.sessionStorage.language == 2) {
            $scope.printNotapplicable = 'लागू नहीं';
            $scope.printFormalSaving = 'औपचारिक बचत';
            $scope.printInFormalSaving = 'अनौपचारिक बचत';
        } else if ($window.sessionStorage.language == 3) {
            $scope.printNotapplicable = 'ಅನ್ವಯಿಸುವುದಿಲ್ಲ';
            $scope.printFormalSaving = 'ಔಪಚಾರಿಕ ಉಳಿತಾಯ';
            $scope.printInFormalSaving = 'ಅನೌಪಚಾರಿಕ ಉಳಿತಾಯ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.printNotapplicable = 'பொருந்தாது';
            $scope.printFormalSaving = 'முறையான சேமிப்பு';
            $scope.printInFormalSaving = 'முறைசாரா சேமிப்பு';
        } else if ($window.sessionStorage.language == 5) {
            $scope.printNotapplicable = 'వర్తించదు';
            $scope.printFormalSaving = 'ఫార్మల్ సేవ్';
            $scope.printInFormalSaving = 'అనధికార పొదుపు';
        } else if ($window.sessionStorage.language == 6) {
            $scope.printNotapplicable = 'लागू नाही';
            $scope.printFormalSaving = 'औपचारिक बचत';
            $scope.printInFormalSaving = 'अनौपचारिक बचत';
        }
        $scope.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        console.log(' $scope.todayDate',  $scope.todayDate);
        //T12:16:29.829Z
        Restangular.all('surveyquestions').getList().then(function (surveyquestions) {
            $scope.AllSurveyQuestions = surveyquestions;
            Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (HRes) {
                // + '&filter[where][lastupdatedtime][gte]=' + $scope.todayDate + 'T00:00:00.000Z'
                $scope.HealthAnswers = HRes;
                angular.forEach($scope.HealthAnswers, function (member, index) {
                    member.index = index + 1;
                   
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });


           /* Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][pillarid]=5' + '&filter[where][questionid]=64').getList().then(function (idsRes) {
                $scope.RemainingAnswers = idsRes;
                angular.forEach($scope.RemainingAnswers, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.AllSurveyQuestions.length; m++) {
                        if (member.questionid == $scope.AllSurveyQuestions[m].id) {
                            member.Question = $scope.AllSurveyQuestions[m];
                            member.QuestionSlNo = +($scope.AllSurveyQuestions[m].serialno);
                            break;
                        }
                    }
                });
            });*/
            $scope.modalPrevAns = $modal.open({
                animation: true,
                templateUrl: 'template/Previousanswers.html',
                scope: $scope,
                backdrop: 'static',
                size: 'lg'
            });
        });
    };
    $scope.HealthViewAnswer = function (ansId, quesId) {
       // console.log('quesId', quesId);
        Restangular.one('surveyanswers', ansId).get().then(function (HealthAnsRes) {
            //console.log('HealthAnsRes', HealthAnsRes);
            $scope.AnsDate = HealthAnsRes.lastupdatedtime;
            Restangular.one('surveyquestions', HealthAnsRes.questionid).get().then(function (HealthQuesRes) {
                // console.log('HealthQuesRes', HealthQuesRes);

            });
        });
    };



    $scope.OkDpView = function () {
        $scope.modalToDo.close();
    }
    $scope.okPrevAns = function () {
        $scope.modalPrevAns.close();
    };

    $scope.openHealth = function () {
        $scope.modalHealth = $modal.open({
            animation: true,
            templateUrl: 'template/health.html',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.openToDo = function () {
        $scope.toggleLoading();
        Restangular.all('todostatuses?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').getList().then(function (todostats) {
            $scope.todostatuses = todostats;
            $scope.modalInstanceLoad.close();
            $scope.modalToDo = $modal.open({
                animation: true,
                templateUrl: 'template/ToDo.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false
            });
        });
    };

    $scope.okToDo = function () {
        $scope.modalToDo.close();
    };

    $scope.SaveHealth = function (beneficiaryid, questionid, answer, serialno, yesincrement, noincrement) {
       // console.log('saveHealth', beneficiaryid, questionid, answer, serialno, yesincrement, noincrement);

        $scope.SaveAnswers(beneficiaryid, questionid, answer, serialno, yesincrement, noincrement);
        //$scope.PresentCompletedPillar = pillarid;
        $scope.PresentCompletedQuestion = serialno;

        // $scope.healthnextquestion();

    };
    $scope.okHealth = function () {
        $scope.modalHealth.close();
    };

    $scope.openOneAlert = function () {
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-danger'
        });
    };
    $scope.okAlert = function () {
        $scope.modalOneAlert.close();
    };
    $scope.openToDoPhoneName = function () {
        $scope.modalToDo = $modal.open({
            animation: true,
            templateUrl: 'template/ToDoPhoneName.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false
        });
    };
  
    $scope.alcohal = {};
    

    $scope.ButtonClicked = function (data, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, questionid, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, languagedata) {
        console.log('ButtonClicked', data, questionid)
        $scope.DisplayClickOption = data;
        $scope.hideUpdateTodo = true;
        if (data == 'Yes') {
            $scope.todotyes = Restangular.all('todotypes?filter[where][id]=' + yestodotype + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (todotyes) {
                
                // console.log('todotyes', todotyes);
                
                if(todotyes.length > 0){
                    console.log('todotyes', todotyes);
                $scope.todotypes = todotyes;
                $scope.todo.todotype = yestodotype;
                $scope.Infomessage = todotyes[0].name;
                $scope.messagetype = todotyes[0].messagetype;
                }
              //console.log('m here', $scope.messagetype);

            if (yesaction == 'nextquestion') {

                // $scope.SaveAnswers($routeParams.id, questionid, 'yes', new Date(), $window.sessionStorage.UserEmployeeId, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                $scope.PresentCompletedPillar = pillarid;
                $scope.PresentCompletedQuestion = serialno;
            } else if (yesaction == 'popup') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupYesDocumentflag = yesdocumentflag;
                $scope.PopupNoDocumentFlag = nodocumentflag;
                $scope.PopupYesDocumentid = yesdocumentid;
                $scope.PopupNoDocumentid = nodocumentid;

                if (yespopup == 'message') {
                    
                    if($scope.messagetype == 'M'){
                       // $scope.Infodisplay = data;
                         $scope.openCondomUsed();
                    }
                   else if($scope.messagetype == 'NT' || $scope.messagetype == 'TF'){
                        console.log('m here');
                       $scope.openAlcohalUsed();
                    }
                    else if($scope.messagetype == 'HD'){
                        console.log('m here');
                       $scope.openCD();
                    }
                } else if (yespopup == 'todos') {
                    $scope.openToDo();
                }

            } else if (yesaction == 'skp') {
                var skpcnt = +yesskipto - +serialno - 1;
                $scope.PresentCompletedQuestion = serialno;

            } else if (yesaction == 'providenextquestion') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = yesprovideinfo;
                $scope.openINFO();
            } else if (yesaction == 'freeflow') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupPillarid = pillarid;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'yes';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = yesprovideinfo;
                $scope.openPhoneName();
            }
            });
            //console.log('Button Clicked', data + '::' + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',questionid::' + questionid);
        }
        if (data == 'No') {
            
             $scope.todotyes = Restangular.all('todotypes?filter[where][id]=' + yestodotype + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (todotyes) {
                
                 console.log('todotyes', todotyes);
                
                if(todotyes.length > 0){
                    console.log('todotyes', todotyes);
                $scope.todotypes = todotyes;
                $scope.todo.todotype = yestodotype;
                $scope.Infomessage = todotyes[0].name;
                $scope.messagetype = todotyes[0].messagetype;
                }

            if (noaction == 'nextquestion') {

               // $scope.healthnextquestion();

               $scope.SaveAnswers($routeParams.id, questionid, 'no', serialno, $window.sessionStorage.UserEmployeeId, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
                $scope.PresentCompletedQuestion = serialno;
            } else if (noaction == 'popup') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupYesDocumentflag = yesdocumentflag;
                $scope.PopupNoDocumentFlag = nodocumentflag;
                $scope.PopupYesDocumentid = yesdocumentid;
                $scope.PopupNoDocumentid = nodocumentid;
                
                  if (yespopup == 'message') {
                    
                    if($scope.messagetype == 'M'){
                       // $scope.Infodisplay = data;
                         $scope.openCondomUsed();
                    }
                   else if($scope.messagetype == 'NT' || $scope.messagetype == 'TF'){
                        console.log('m here');
                       $scope.openAlcohalUsed();
                    }
                    else if($scope.messagetype == 'HD'){
                        console.log('m here');
                       $scope.openCD();
                    }
                } else if (yespopup == 'todos') {
                    $scope.openToDo();
                }

            } else if (noaction == 'skp') {
                $scope.PresentCompletedPillar = pillarid;
                $scope.PresentCompletedQuestion = serialno;
                var skpcnt = +noskipto - +serialno - 1;
                $scope.healthquestioncount = +$scope.healthquestioncount + (+skpcnt);
                $scope.healthnextquestion();
                $scope.SaveAnswers($routeParams.id, questionid, 'no', new Date(), $window.sessionStorage.UserEmployeeId, (+noskipto) - 1, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);

            } else if (noaction == 'providenextquestion') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = noprovideinfo;
                $scope.openINFO();
            } else if (noaction == 'freeflow') {
                $scope.PopupBenificiaryid = $routeParams.id;
                $scope.PopupQuestionId = questionid;
                $scope.PopupAnswer = 'no';
                $scope.PopupLastUpdateDate = new Date();
                $scope.PopupLastUpdatedBy = $window.sessionStorage.UserEmployeeId;
                $scope.PopupSerialNo = serialno;
                $scope.PopupYesIncrement = yesincrement;
                $scope.PopupNoIncrement = noincrement;
                $scope.PopupInfo = yesprovideinfo;
                $scope.openPhoneName();
            }
             });
            // console.log('Button Clicked', data + '::' + pillarid + ',yesaction::' + yesaction + ',noaction::' + noaction + ',yespopup::' + yespopup + ',nopopup::' + nopopup + ',yesskipto::' + yesskipto + ',noskipto::' + noskipto + ',schemeslno::' + schemeslno+ ',questionid::' + questionid);
        }
    };


    $scope.openCondomUsed = function () {
        $scope.modalCon = $modal.open({
            animation: true,
            templateUrl: 'template/condomused.html',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.OkCondomView = function () {
        $scope.modalCon.close();
    }
    
    $scope.openAlcohalUsed = function () {
        $scope.modalAlcohal = $modal.open({
            animation: true,
            templateUrl: 'template/alcohol.html',
            scope: $scope,
            backdrop: 'static'
        });
    };

    $scope.cancelAlcohal = function () {
        $scope.modalAlcohal.close();
    }
    /******************create post **********************/
    $scope.SaveToDo = function (beneficiaryid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {

        $scope.todo.beneficiaryid = beneficiaryid;
        $scope.todo.fieldworkerid = $window.sessionStorage.UserEmployeeId;
        $scope.todo.facility = $window.sessionStorage.coorgId;
        $scope.todo.district = $window.sessionStorage.salesAreaId;
        $scope.todo.state = $window.sessionStorage.zoneId;
        $scope.todo.questionid = questionid;
        $scope.todo.roleId = $window.sessionStorage.roleId;
        $scope.todo.countryId = $window.sessionStorage.countryId;
        $scope.todo.town = $window.sessionStorage.distributionAreaId;
        $scope.todo.deleteflag = false;

        // $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
        $scope.submittodos.post($scope.todo).then(function (resp) {
            console.log('submittodos1', resp);
             Restangular.one('beneficiaries', resp.beneficiaryid).get().then(function (fw) {
                 $window.sessionStorage.MemberFilter = fw.fullname;
            $scope.SaveAnswers(beneficiaryid, questionid, answer, serialno, modifieddate, modifiedby, yesincrement, noincrement);
             });
           // $scope.PresentCompletedPillar = pillarid;
            $scope.PresentCompletedQuestion = serialno;

            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            $scope.todo.datetime = sevendays;
        }, function (error) {
            console.log('error', error);
        });
        $scope.modalToDo.close();

    };

    /********************** UPdate Todo *******************/
    $scope.updateToDo = function () {
        $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function (resp) {
            console.log('submittodos9', resp);

            $scope.modalToDo.close();
            $scope.hideUpdateTodo = true;


        }, function (error) {
            console.log('error', error);
        });
    };

 $scope.beneficiaryalcohal = {};
    /**************alchoal Saaved ****************/
    $scope.alcohalSave = function (beneficiaryid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement){
        if ($scope.alcohal.noofday == '' || $scope.alcohal.noofday == null || $scope.alcohal.noofday == undefined) {
            $scope.AlertMessage = 'please enter no of days Alcohal consumed';
            $scope.openOneAlert();
        } 
         else {
           $scope.beneficiaryalcohal.noofDayAlcohalDrink = $scope.alcohal.noofday;
             console.log($scope.beneficiaryalcohal.noofDayAlcohalDrink)
           Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiaryalcohal).then(function (respo) {
               $scope.SaveAnswers(beneficiaryid, questionid, answer, serialno, modifieddate, modifiedby, yesincrement, noincrement);
        $scope.PresentCompletedQuestion = serialno;
            });
            $scope.modalAlcohal.close();
         }
    };
    /**************alchoal Saaved ****************/
     $scope.beneficiarypopup = {};
    
    /***********************save Message PopUp****************/
     $scope.SaveMessage = function (beneficiaryid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement){
        
         console.log('messagepopup');
          // Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarypopup).then(function (respo) {
               $scope.SaveAnswers(beneficiaryid, questionid, answer, serialno, modifieddate, modifiedby, yesincrement, noincrement);
            $scope.PresentCompletedQuestion = serialno;
           // });
             $scope.modalCon.close();
        
    };
    
    /************************Condom Details *******************/
    
    $scope.openCD = function () {
        if ($scope.DisplayBeneficiary.condomsasked != null) {
            var CondomsAsked = $scope.DisplayBeneficiary.condomsasked.split(',');
            //var CondomsProvided = $scope.DisplayBeneficiary.condomsprovided.split(',');
            if (CondomsAsked.length > 0) {
                var AskedNum = CondomsAsked[CondomsAsked.length - 1].split(':');
               // var ProvidedNum = CondomsProvided[CondomsProvided.length - 1].split(':');
                $scope.condom.asked = AskedNum[AskedNum.length - 1];
                //$scope.condom.provided = ProvidedNum[ProvidedNum.length - 1];
            }
        }
        $scope.modalCD = $modal.open({
            animation: true,
            templateUrl: 'template/condomdetails.html',
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.SaveCD = function (beneficiaryid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement) {
        //console.log('$scope.condom.asked', $scope.condom.asked);
        //console.log('$scope.condom.provided', $scope.condom.provided);
        if ($scope.condom.asked == '' || $scope.condom.asked == null || $scope.condom.asked == undefined) {
            $scope.AlertMessage = 'Condoms Asked cannot be Empty';
            $scope.openOneAlert();
        } 
        /*else if ($scope.condom.provided == '' || $scope.condom.provided == null || $scope.condom.provided == undefined) {
            $scope.AlertMessage = 'Condoms Provided cannot be Empty';
            $scope.openOneAlert();
        } else if ($scope.condom.dateasked == '' || $scope.condom.dateasked == null || $scope.condom.dateasked == undefined) {
            $scope.AlertMessage = 'Date Provided cannot be Empty';
            $scope.openOneAlert();
        }*/
        else {
            //$scope.askeddate = $filter('date')($scope.condom.dateasked, 'dd/MM/yyyy');
            //$scope.askeddate = $filter('date')(new Date(), 'yyyy-MM-dd') + 'T00:00:00.000Z'; //$filter('date')($scope.condom.dateasked, 'yyyy/MM/dd');
            //['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate']);
            //console.log('new Date()', new Date());
            if ($scope.DisplayBeneficiary.condomsasked != null) {
               // $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
                $scope.beneficiarycondom.condomsasked = $scope.DisplayBeneficiary.condomsasked + ',' + $scope.askeddate + ':' + $scope.condom.asked;
              //  $scope.beneficiarycondom.condomsprovided = $scope.DisplayBeneficiary.condomsprovided + ',' + $scope.askeddate + ':' + $scope.condom.provided;
            } else {
               // $scope.beneficiarycondom.dateprovided = $scope.condom.dateasked;
                $scope.beneficiarycondom.condomsasked = $scope.askeddate + ':' + $scope.condom.asked;
               // $scope.beneficiarycondom.condomsprovided = $scope.askeddate + ':' + $scope.condom.provided;
            }
            Restangular.one('beneficiaries', $routeParams.id).customPUT($scope.beneficiarycondom).then(function (respo) {
                $scope.SaveAnswers(beneficiaryid, questionid, answer, serialno, modifieddate, modifiedby, yesincrement, noincrement);
        $scope.PresentCompletedQuestion = serialno;
            });
             $scope.modalCD.close();
           
        }
    };
    $scope.okCD = function () {
        $scope.modalCD.close();
    };
    /************************Condom Details *******************/
    


    ///////////////////////////////////Health End////////////////////////////////////////////////
    $scope.ChangeClient = function (id, answered, Elementid, radioid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
        $scope.route = id;
       // console.log('id', id);
        $rootScope.surveyquestions = Restangular.one('surveyquestions/' + id).get().then(function (surveyquestions) {
           // console.log('surveyquestions', surveyquestions);
            $rootScope.routeid = surveyquestions.surveytypeid;
            $scope.surveyquestion = surveyquestions;
            $scope.QuestionName = surveyquestions.question;


            $scope.CreateForm(surveyquestions.id, surveyquestions.noofoptions, surveyquestions.answeroptions, surveyquestions.questiontype, surveyquestions.serialno, surveyquestions.range, $scope.answerd, answered, Elementid, radioid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid);
        });
    }
    $scope.CreateForm = function (id, noofdropdowns, answers, questiontype, slno, range, answerd, answered, ElementId, radioid, yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid) {
    console.log('answerd',  answerd, range);

        var mydiv = document.getElementById(ElementId);
        var myForm = document.createElement("form");
        while (mydiv.firstChild) {
            mydiv.removeChild(mydiv.firstChild);
        }
        myForm.method = "post";
        if (questiontype == 't') {

            var myTextElement;
            try {
                myTextElement = document.createElement('<input type="text" name="camera_status"  />On');
            } catch (err) {
                myTextElement = document.createElement('input');
            }
            myTextElement.type = "text";
            myTextElement.name = "camera_status";
            myTextElement.label = "Text";
            myForm.appendChild(myTextElement);
            mydiv.appendChild(myForm);
        }

        if (questiontype == 'nt') {
               $scope.hideButton = true;

            var myTextElement1;
            try {
               // console.log('in try');
                myTextElement1 = document.createElement('<input type="text" name="camera_status"  placeholder="Enter Value"  value="" />On');
               myTextElement1.addEventListener("click", function (event) {
                    // console.log('event', event);
                    $scope.inputClick1(id, noofdropdowns, answers, slno, range, yesincrement, noincrement, event);
                   // event.preventDefault();

                });
            } catch (err) {
                //console.log('in catch');
                myTextElement1 = document.createElement('input');
                myTextElement1.addEventListener("click", function (event) {
                    // console.log('event', event);
                    $scope.inputClick1(id, noofdropdowns, answers, slno, range, yesincrement, noincrement, event);
                    //event.preventDefault();

                });
            }
            myTextElement1.type = "text";
            myTextElement1.min = "1";
            myTextElement1.step = "1";
            myTextElement1.name = "camera_status";
            myTextElement1.label = "Text";
            myTextElement1.id = "Txt";
            myTextElement1.placeholder = "Enter Value";
            myTextElement1.value = "";
            myTextElement1.setAttribute("ng-model", "test");
            myTextElement1.setAttribute("ng-change", "inputClick()");
            
            $compile(myTextElement1)($scope)
            myForm.appendChild(myTextElement1);
            mydiv.appendChild(myForm);
        }
        if (questiontype == 'td') {

            var array = answers.split(',');
            for (var i = 0; i < noofdropdowns; i++) {
                var myTextDisplayElement;
                try {
                    myTextDisplayElement = document.createElement('div');
                    myTextDisplayElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <p style='color: white;'>" + '-' + array[i] + "</p></div";
                } catch (err) {
                    myTextDisplayElement = document.createElement('div');
                    myTextDisplayElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <p style='color: white;'>" + '-' + array[i] + "</p></div";
                }

                myForm.appendChild(myTextDisplayElement);
            };
            mydiv.appendChild(myForm);
        }

        if (questiontype == 'r') {
            //console.log('id', id);
            $scope.hideButton = false;
            //camera_status
            var array = answers.split(',');
            //console.log('array', array);
            var displayarray = [];
            if ($window.sessionStorage.language == 1) {
                displayarray = answers.split(',');
            } else if ($window.sessionStorage.language != 1) {
                if (parentFlag == false) {
                    displayarray = answers.split(',');
                } else {
                    displayarray = answers.split(',');
                }
            }
            for (var i = 0; i < noofdropdowns; i++) {
                var myRadioElement;
                try {
                    myRadioElement = document.createElement('<input type="radio" name="camera_status" />On');
                } catch (err) {
                    if (answered == array[i]) {
                        myRadioElement = document.createElement('div');

                        var radioYes = document.createElement("input");
                        radioYes.setAttribute("type", "button");
                        radioYes.setAttribute("id", radioid + i);
                        radioYes.setAttribute("name", "myInputs[]");
                        radioYes.setAttribute("value", array[i]);
                        radioYes.setAttribute("checked", "true");
                        if (array[i].toLowerCase() == 'yes') {
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('Yes', yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
                                //event.preventDefault();
                               
                            });
                        }
                        if (array[i].toLowerCase() == 'no') {
                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('No', yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
                                //event.preventDefault();
                            });
                        }
                        var lblYes = document.createElement("label");
                        var textYes = document.createTextNode(displayarray[i]);
                        lblYes.appendChild(textYes);
                        mydiv.appendChild(radioYes);
                        mydiv.appendChild(lblYes);
                    } else {
                       // console.log('i m in else part');
                        myRadioElement = document.createElement('div');
                        var radioYes = document.createElement("input");
                        radioYes.setAttribute("type", "button");
                        radioYes.setAttribute("id", radioid + i);
                        radioYes.setAttribute("name", "myInputs[]");
                        radioYes.setAttribute("value", displayarray[i]);
                        radioYes.setAttribute("class", "btn btn-success");
                        radioYes.style.width = "60px";
                        radioYes.style.height = "25px";
                        radioYes.style["font-size"] = "12px";
                        radioYes.style["align"] = "center";
                        radioYes.style["line-height"] = "0.7em";
                        
                        if (array[i].toLowerCase() == 'yes') {
                             // console.log('yes', array[i].toLowerCase());

                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('Yes', yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[0]);
                                event.preventDefault();
                            });
                        }
                        if (array[i].toLowerCase() == 'no') {
                           //  console.log('NO', array[i].toLowerCase());
                            
                            radioYes.style.marginLeft = "5px";
                            radioYes.setAttribute("class", "btn btn-red");

                            radioYes.addEventListener("click", function (event) {
                                $scope.ButtonClicked('No', yesaction, noaction, yespopup, nopopup, yesskipto, noskipto, schemeslno, id, serialno, yesincrement, noincrement, yesprovideinfo, noprovideinfo, displayquestion, yestodotype, notodotype, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, displayarray[1]);
                                event.preventDefault();
                            });
                        }
                        var lblYes = document.createElement("label");
                        var textYes = document.createTextNode(displayarray[i]);
                        lblYes.appendChild(textYes);
                        mydiv.appendChild(radioYes);

                    }
                }

            };

        }

        if (questiontype == 'c') {
           // console.log(' i m in checkbox');
            $scope.hideButton = true;

            var array = answers.split(',');
            var answeredarray = answered.split(',');
            //console.log('answeredarray', answeredarray);
            for (var i = 0; i < noofdropdowns; i++) {
                // console.log('array', array);
                var myCheckElement;
                try {
                     console.log('else');
                    if (answeredarray.indexOf(array[i]) == -1) {
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' class='slectOne' name='myInputs[]' value='" + i + "' >" + '  ' + array[i] + "</div";


                        myCheckElement.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.sel(id, noofdropdowns, answers, slno, yesincrement, noincrement, event);
                            //event.preventDefault();

                        });
                    } else {
                       // console.log('first esle');
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' name='myInputs[] value='" + i + "' checked >" + '  ' + array[i] + "</div";

                        myCheckElement.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.sel(id, noofdropdowns, answers, slno, yesincrement, noincrement, event);
                            //event.preventDefault();

                        });

                    }


                } catch (err) {
                   // console.log('catch');
                    if (answeredarray.indexOf(array[i]) == -1) {
                        // console.log('if');
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' name='myInputs[] value='" + i + "' >" + '  ' + array[i] + "</div";

                        myCheckElement.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.sel(id, noofdropdowns, answers, slno, yesincrement, noincrement, event);
                            event.preventDefault();

                        });


                    } else {
                       //  console.log('else');
                        myCheckElement = document.createElement('div');
                        myCheckElement.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='checkbox' name='myInputs[] value='" + i + "' checked>" + '  ' + array[i] + "</div";

                        myCheckElement.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.sel(id, noofdropdowns, answers, slno, yesincrement, noincrement, event, myInputs);
                            event.preventDefault();

                        });

                    }

                }

                myForm.appendChild(myCheckElement);
                //$compile(myForm)($scope);
            }
            mydiv.appendChild(myForm);
        }
        /******new Changes********/
        if (questiontype == 'rb') {
           // console.log(' i m in checkbox');
            $scope.hideButton = true;

            var array = answers.split(',');
            var answeredarray = answered.split(',');
           // console.log('answeredarray', answeredarray);
            for (var i = 0; i < noofdropdowns; i++) {
                // console.log('array', array);
                var myCheckElement1;
                try {
                     //console.log('else');
                    if (answeredarray.indexOf(array[i]) == -1) {
                        myCheckElement1 = document.createElement('div');
                        myCheckElement1.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='radio' class='slectOne' name='myInputs[]' value='" + i + "' >" + '  ' + array[i] + "</div";


                        myCheckElement1.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.selReadio(id, noofdropdowns, answers, slno, yesincrement, noincrement, event);
                            //event.preventDefault();

                        });
                    } else {
                       // console.log('first esle');
                        myCheckElement1 = document.createElement('div');
                        myCheckElement1.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='radio' name='myInputs[] value='" + i + "' checked >" + '  ' + array[i] + "</div";

                        myCheckElement1.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.selReadio(id, noofdropdowns, answers, slno, yesincrement, noincrement, event);
                            //event.preventDefault();

                        });

                    }


                } catch (err) {
                   // console.log('catch');
                    if (answeredarray.indexOf(array[i]) == -1) {
                         console.log('if');
                        myCheckElement1 = document.createElement('div');
                        myCheckElement1.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='radio' name='myInputs[] value='" + i + "' >" + '  ' + array[i] + "</div";

                        myCheckElement1.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.selReadio(id, noofdropdowns, answers, slno, yesincrement, noincrement, event);
                            event.preventDefault();

                        });


                    } else {
                         console.log('else');
                        myCheckElement1 = document.createElement('div');
                        myCheckElement1.innerHTML = "<div  style='color: #333; display:block; width:250px;min-height:30px;word-wrap: break-word;'> <input type='radio' name='myInputs[] value='" + i + "' checked>" + '  ' + array[i] + "</div";

                        myCheckElement1.addEventListener("click", function (event) {
                            // console.log('event', event);
                            $scope.selReadio(id, noofdropdowns, answers, slno, yesincrement, noincrement, event, myInputs);
                            event.preventDefault();

                        });

                    }

                }

                myForm.appendChild(myCheckElement1);
                //$compile(myForm)($scope);
            }
            mydiv.appendChild(myForm);
        }
    };


    /********checkbox question Save ************/

    $scope.sel = function (id, noofdropdowns, answers, slno, yesincrement, noincrement, event, myInputs) {
        
    $scope.ansArray = $('input[type=checkbox]:checked').map(function(_, el) {
        return $(el).val();
    }).get();
    
        
//        if(event.toElement.checked == false){
//            $scope.DisableSave = true;
//        }else{
//            $scope.DisableSave = false;
//        }
   // console.log($scope.ansArray);
       
        $scope.Ansarray = answers.split(',');
        $scope.finalArray = [];
        for (var i = 0; i < $scope.Ansarray.length; i++) {
            $scope.finalArray.push({
                "index": i,
                "option": $scope.Ansarray[i]
            });
        }
        $scope.answerd = [];
        angular.forEach($scope.finalArray, function (member, index) {
           // console.log('member', member);
            member.index = index;
             var data = $scope.ansArray.filter(function (arr) {
                    return arr == member.index
                })[0];
            if (data != undefined) {
                $scope.answerd.push(member.option);
                //console.log( $scope.answerd);
            }
            //console.log(' $scope.answerd', $scope.answerd);
        });
        if($scope.answerd == '' || $scope.answerd == null){
           $scope.DisableSave = true;
           }
        else{
            $scope.DisableSave = false;
        }
        $scope.obj = {
            beneficiaryid: $routeParams.id,
            questionid: id,
            answer: $scope.answerd,
            serialno: slno,
            yesincrement: yesincrement,
            noincrement: noincrement
        };



    }
    
    /********NEw chnages Radio*****/
    
    
    $scope.selReadio = function (id, noofdropdowns, answers, slno, yesincrement, noincrement, event, myInputs) {
        
    $scope.ansArray = $('input[type=radio]:checked').map(function(_, el) {
        return $(el).val();
    }).get();
    
        
//        if(event.toElement.checked == false){
//            $scope.DisableSave = true;
//        }else{
//            $scope.DisableSave = false;
//        }
   // console.log($scope.ansArray);
       
        $scope.Ansarray = answers.split(',');
        $scope.finalArray = [];
        for (var i = 0; i < $scope.Ansarray.length; i++) {
            $scope.finalArray.push({
                "index": i,
                "option": $scope.Ansarray[i]
            });
        }
        $scope.answerd = [];
        angular.forEach($scope.finalArray, function (member, index) {
           // console.log('member', member);
            member.index = index;
             var data = $scope.ansArray.filter(function (arr) {
                    return arr == member.index
                })[0];
            if (data != undefined) {
                $scope.answerd.push(member.option);
               // console.log( $scope.answerd);
            }
           // console.log(' $scope.answerd', $scope.answerd);
        });
        if($scope.answerd == '' || $scope.answerd == null){
           $scope.DisableSave = true;
           }
        else{
            $scope.DisableSave = false;
        }
        $scope.obj = {
            beneficiaryid: $routeParams.id,
            questionid: id,
            answer: $scope.answerd,
            serialno: slno,
            yesincrement: yesincrement,
            noincrement: noincrement
        };



    }
    /********NEw chnages Radio*****/
    
    $scope.savedQuestion = function () {
        $scope.DisableSave = true;
        $scope.test = '';
        $scope.SaveHealth($scope.obj.beneficiaryid, $scope.obj.questionid, $scope.obj.answer, $scope.obj.serialno, $scope.obj.yesincrement, $scope.obj.noincrement);
    };

  $scope.inputClick1 = function (id, noofdropdowns, answers, slno, range, yesincrement, noincrement){
      
  
    $scope.inputClick = function () {
       
       // console.log(' questionid', id, range);
      $scope.answerd = [];
       /**** accept only numbers****/
       $("#Txt").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       // display error message
      //  $("#errmsg").html("Digits Only").show().fadeOut("slow");
             return false;
    }
            
   });
        
       
       $("#Txt").each(function () {
            if (this.value != '')
             $scope.answerd.push(this.value);
          /* setTimeout(function(){ 
               
                if(this.value <= range){
                     $scope.answerd.push(this.value);
                }
               else{
                   this.value = '';
                   alert(' Number Not fall in  Range');
               }
               
           }, 500);*/
               
        });
        
        
            $scope.obj = {
                beneficiaryid: $routeParams.id,
                questionid: id,
                answer: $scope.answerd,
                serialno: slno,
                yesincrement: yesincrement,
                noincrement: noincrement
            };
       
          if ($scope.answerd == null || $scope.answerd == '' || $scope.answerd == undefined){
           // console.log('null');
            $scope.DisableSave = true;
        } else{
            // console.log('notnull');
                $scope.DisableSave = false;
            }

    }
  }
   
  

    /************Check box select one*******/

    $scope.today = function () {
        $scope.dt = new Date();
        //$scope.todo.datetime = $filter('date')(new Date(), 'y-MM-dd');
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);

        $scope.todo.datetime = sevendays;
    };
    // var nextmonth = new Date();
    // nextmonth.setMonth(sevendays.getMonth + 1);
    // $scope.cdids.maxdate = nextmonth;
    var oneday = new Date();
    oneday.setDate(oneday.getDate() + 1);
    // $scope.reportincident.dtmin = oneday;
    $scope.today();
    $scope.condom.dt = new Date();
    $scope.picker.dt = new Date();
    $scope.picker.selectdate = new Date();
    $scope.condom.dateasked = new Date();
    $scope.plhiv.month = new Date();
    $scope.plhiv.maxdate = new Date();
    $scope.cdids.availeddate = new Date();
    $scope.cdidsmaxdate = new Date();
    $scope.schememaster.datetime = new Date();
    $scope.increment.selectdate = new Date();
    // $scope.reportincident.dtmax = new Date();
    $scope.condom.dateasked = new Date();
    $scope.todomin = new Date();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        //$scope.dt = null;
    };
    // Disable weekend selection
    $scope.disabled = function (date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.todoopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.todo.opened = true;
    };
    $scope.fsopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.fs.opened = true;
    };
    $scope.healthopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.health.opened = true;
    };
    $scope.spopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.sp.opened = true;
    };
    $scope.reportopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        // $scope.reportincident.opened = true;
    };
    $scope.followopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.follow.opened = true;
    };
    $scope.pickeropen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.picker.opened = true;
    };
    $scope.cdidsopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.cdids.opened = true;
    };
    $scope.workflowopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.schememaster.opened = true;
    };
    $scope.plhivopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.plhiv.opened = true;
    };
    $scope.condomopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.condom.opened = true;
    };
    $scope.incrementopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.increment.opened = true;
    };
    $scope.folowupdateopen = function ($event, index) {
        //$event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepicker' + index).focus();
        });
        $scope.schememaster.opened = true;
    };

    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.monthOptions = {
        formatYear: 'yyyy',
        startingDay: 1,
        minMode: 'month'
    };
    $scope.mode = 'month';
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.monthformats = ['MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.monthformat = $scope.monthformats[0];
    //Datepicker settings end
    /******************************************* Suman's Changes ApplicationWorkflow *************/
    if ($window.sessionStorage.fullName != undefined) {
        $scope.mem = Restangular.one('beneficiaries', $window.sessionStorage.fullName).get().then(function (member) {
            $scope.memberfullname = member;
            $scope.benfid = $window.sessionStorage.fullName;
        });
    }
    $scope.member = true;
    $scope.memberone = true;

    $scope.followupdatehide = false;
    $scope.followuphide = false;
    $scope.dateofclosure = true;


    $scope.noofmonthrepay_Hide = true;

    $scope.loanapplied = false;

    $scope.mygenderstatus = false;
    $scope.myoccupationstatus = false;
    $scope.mysocialstatus = false;
    $scope.myagestatus = false;
    $scope.myhealthstatus = false;
    $scope.loanapplied = false;




    $scope.datetimehide = false;
    $scope.reponserec = true;
    $scope.delaydis = true;
    $scope.collectedrequired = true;
    $scope.rejectdis = true;
    $scope.purposeofloanhide = true;
    /**************************************************************/
    $scope.rejectdis = true;

    /********************************************************************************************/

    $scope.docm = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (docmt) {
        $scope.printdocuments = docmt;
        angular.forEach($scope.printdocuments, function (member, index) {
            member.index = index;
            member.enabled = false;
        });
    });
    $scope.getGender = function (genderId) {
        return Restangular.one('genders', genderId).get().$object;
    };


    /******************************************/
    $scope.hideUpdateCDIDS = true;
    $scope.hideUpdateTodo = true;
    $scope.hideUpdateRI = true;
    $scope.hideUpdateAW = true;
    //Edit OnetoOne////////////
    $scope.EditoneToOne = function (todotype, id, data, pillarid) {
        console.log('EditoneToOne', id);
        $scope.UpdateQuestionId = data.questionid;
        if (data.questionid != null) {
            $scope.UpdateQuestion = Restangular.one('surveyquestions', data.questionid).get().$object;
            $scope.UpdateDisplayClickOption12 = Restangular.one('surveyanswers?filter[where][beneficiaryid]=' + $routeParams.id + '&filter[where][questionid]=' + data.questionid).get().then(function (Sans) {
                //console.log('Sans', Sans[0].answer);
                if ($window.sessionStorage.language == 1) {
                    $scope.PillarName = 'SP';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = 'Yes';
                    } else {
                        $scope.UpdateDisplayClickOption = 'No';
                    }
                } else if ($window.sessionStorage.language != 1) {
                    $scope.PillarName = '?? ??';
                    if (Sans[0].answer == 'yes') {
                        $scope.UpdateDisplayClickOption = '???';
                    } else {
                        $scope.UpdateDisplayClickOption = '????';
                    }
                }
            });
        } else {
            $scope.UpdateQuestion = 'Scheme/Document';
            $scope.UpdateDisplayClickOption = '';
        }
    }
    $scope.updateToDo = function () {
        $scope.submittodos.customPUT($scope.todo, $scope.todo.id).then(function (resp) {
            console.log('submittodos9', resp);
            $scope.todo = {};
            $scope.todo = {
                status: 1,
                stateid: $window.sessionStorage.zoneId,
                state: $window.sessionStorage.zoneId,
                districtid: $window.sessionStorage.salesAreaId,
                district: $window.sessionStorage.salesAreaId,
                coid: $window.sessionStorage.coorgId,
                facility: $window.sessionStorage.coorgId,
                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                lastmodifiedtime: new Date(),
                roleId: $window.sessionStorage.roleId
            };
            var sevendays = new Date();
            sevendays.setDate(sevendays.getDate() + 7);
            $scope.todo.datetime = sevendays;
            $scope.modalToDo.close();
            $scope.hideUpdateTodo = true;

            $scope.hlthtodos = Restangular.all('todos?filter[where][beneficiaryid]=' + $routeParams.id).getList().then(function (hlthtodos) {
                $scope.healthtodos = hlthtodos;
                angular.forEach($scope.healthtodos, function (member, index) {
                    member.index = index + 1;
                    for (var m = 0; m < $scope.maintodotypes.length; m++) {
                        if (member.todotype == $scope.maintodotypes[m].id) {
                            member.TodoType = $scope.maintodotypes[m];
                            break;
                        }
                    }
                    for (var n = 0; n < $scope.maintodostatuses.length; n++) {
                        if (member.status == $scope.maintodostatuses[n].id) {
                            //if ($scope.UserLanguage == 1) {
                            member.TodoStatus = $scope.maintodostatuses[n];

                            break;
                        }
                    }
                });
            });


        }, function (error) {
            console.log('error', error);
        });
    };

    /************************* Language *****************************************/
    // $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
    $scope.multiLang = Restangular.one('multilanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
      //  console.log('langResponse', langResponse);
        $scope.onetoneheader = langResponse[0].onetoneheader;
        $scope.previousanswers = langResponse[0].previousanswers;
        $scope.printlastmeetingdate = langResponse[0].lastmeetingdate;
        $scope.printfullname = langResponse[0].fullname;
        $scope.printavahanid = langResponse[0].avahanid;
        $scope.moredetails = langResponse[0].moredetails;
        $scope.lessdetails = langResponse[0].lessdetails;
        $scope.printactivemember = langResponse[0].activemember;
        $scope.printtypology = langResponse[0].typology;
        $scope.printdob = langResponse[0].dob;
        $scope.printnickname = langResponse[0].nickname;
        $scope.printtiid = langResponse[0].tiid;
        $scope.printtypology = langResponse[0].typology;
        $scope.printplhiv = langResponse[0].plhivprint;
        $scope.printpaidmember = langResponse[0].paidmember;
        $scope.printage = langResponse[0].age;
        $scope.monthofhivtest = langResponse[0].monthofhivtest;
        $scope.monthofstitest = langResponse[0].monthofstitest;
        $scope.currenthivstatus = langResponse[0].currenthivstatus;
        $scope.facilityhelplineno = langResponse[0].facilityhelplineno;
        $scope.todolist = langResponse[0].todolist;
        $scope.tododisplay = langResponse[0].tododisplay;
        $scope.date = langResponse[0].date;
        $scope.status = langResponse[0].status;
        $scope.prevquestionlist = langResponse[0].prevquestionlist;
        $scope.yes = langResponse[0].yes;
        $scope.no = langResponse[0].no;
        $scope.noofincidentmonth = langResponse[0].noofincidentmonth;
        $scope.helplineno = langResponse[0].helplineno;
        $scope.headerreportincident = langResponse[0].headerreportincident;
        $scope.documentscheme = langResponse[0].documentscheme;
        $scope.printreferenceno = langResponse[0].referenceno;
        $scope.dateoflastfs = langResponse[0].dateoflastfs;
        $scope.monthoflastsaving = langResponse[0].monthoflastsaving;
        $scope.noofsavingaccount = langResponse[0].noofsavingaccount;
        $scope.noofinsuranceproduct = langResponse[0].noofinsuranceproduct;
        $scope.noofpensionproduct = langResponse[0].noofpensionproduct;
        $scope.noofinvestmentproduct = langResponse[0].noofinvestmentproduct;
        $scope.noofinformalsaving = langResponse[0].noofinformalsaving;
        $scope.noofinformalcredit = langResponse[0].noofinformalcredit;
        $scope.noofformalcredit = langResponse[0].noofformalcredit;
        $scope.lastmembershipfeespaid = langResponse[0].lastmembershipfeespaid;
        $scope.dateanddetailnextevent = langResponse[0].dateanddetailnextevent;
        $scope.whetherbordpast = langResponse[0].whetherbordpast;
        $scope.proposed = langResponse[0].proposed;
        $scope.approved = langResponse[0].approved;
        $scope.noofsavingaccount = langResponse[0].noofsavingaccount;
        $scope.noofinsuranceproduct = langResponse[0].noofinsuranceproduct;
        $scope.noofpensionproduct = langResponse[0].noofpensionproduct;
        $scope.noofinvestmentproduct = langResponse[0].noofinvestmentproduct;
        $scope.noofinformalsaving = langResponse[0].noofinformalsaving;
        $scope.noofinformalcredit = langResponse[0].noofinformalcredit;
        $scope.pillar = langResponse[0].pillar;
        $scope.healthpillar = langResponse[0].healthpillar;
        $scope.ssjpillar = langResponse[0].ssjpillar;
        $scope.sppillar = langResponse[0].sppillar;
        $scope.fspillar = langResponse[0].fspillar;
        $scope.idspillar = langResponse[0].idspillar;
        $scope.alltaskpillar = langResponse[0].alltaskpillar;
        $scope.selecttodos = langResponse[0].selecttodos;
        $scope.selectstatus = langResponse[0].selectstatus;
        $scope.followupdate = langResponse[0].followupdate;
        $scope.monthtimeavailed = langResponse[0].monthtimeavailed;
        $scope.referencenumber = langResponse[0].referencenumber;
        $scope.todosprint = langResponse[0].todosprint;
        $scope.savebutton = langResponse[0].savebutton;
        $scope.update = langResponse[0].update;
        $scope.cancel = langResponse[0].cancel;
        $scope.create = langResponse[0].create;
        $scope.headerreportincident = langResponse[0].headerreportincident;
        $scope.whendidhappen = langResponse[0].whendidhappen;
        $scope.multiplepeopleeffect = langResponse[0].multiplepeopleeffect;
        $scope.membername = langResponse[0].membername;
        $scope.printmember = langResponse[0].member;
        $scope.divincidenttype = langResponse[0].divincidenttype;
        $scope.printphysical = langResponse[0].physical;
        $scope.sexual = langResponse[0].sexual;
        $scope.chieldrelated = langResponse[0].chieldrelated;
        $scope.emotional = langResponse[0].emotional;
        $scope.propertyrelated = langResponse[0].propertyrelated;
        $scope.severityofincident = langResponse[0].severityofincident;
        $scope.divperpetratordetail = langResponse[0].divperpetratordetail;
        $scope.police = langResponse[0].police;
        $scope.client = langResponse[0].client;
        $scope.serviceprovider = langResponse[0].serviceprovider;
        $scope.otherkp = langResponse[0].otherkp;
        $scope.neighbour = langResponse[0].neighbour;
        $scope.goons = langResponse[0].goons;
        $scope.partners = langResponse[0].partners;
        $scope.PrintHusband = langResponse[0].husband;
        $scope.familiymember = langResponse[0].familiymember;
        $scope.othersexkp = langResponse[0].othersexkp;
        $scope.divreported = langResponse[0].divreported;
        $scope.reportedto = langResponse[0].reportedto;
        $scope.ngos = langResponse[0].ngos;
        $scope.friends = langResponse[0].friends;
        $scope.plv = langResponse[0].plv;
        $scope.coteam = langResponse[0].coteam;
        $scope.champion = langResponse[0].champion;
        $scope.otherkps = langResponse[0].otherkps;
        $scope.legalaidclinic = langResponse[0].legalaidclinic;
        $scope.divtimetorespond = langResponse[0].divtimetorespond;
        $scope.refferedcounselling = langResponse[0].refferedcounselling;
        $scope.refferedmedicalcare = langResponse[0].refferedmedicalcare;
        $scope.refferedcomanager = langResponse[0].refferedcomanager;
        $scope.refferedplv = langResponse[0].refferedplv;
        $scope.refferedaidclinic = langResponse[0].refferedaidclinic;
        $scope.refferedboardmember = langResponse[0].refferedboardmember;
        $scope.followupdate = langResponse[0].followupdate;
        $scope.divactiontaken = langResponse[0].divactiontaken;
        $scope.printdateofclosure = langResponse[0].dateofclosure;
        $scope.twohrs = langResponse[0].twohrs;
        $scope.twototwentyfourhrs = langResponse[0].twototwentyfourhrs;
        $scope.greatertwentyfourhrs = langResponse[0].greatertwentyfourhrs;
        $scope.currentstatuscase = langResponse[0].currentstatuscase;
        $scope.followuprequired = langResponse[0].followuprequired;
        $scope.name = langResponse[0].name;
        $scope.hotspot = langResponse[0].hotspot;
        $scope.phonenumber = langResponse[0].phonenumber;
        $scope.schemedocument = langResponse[0].schemedocument;
        $scope.applyforscheme = langResponse[0].applyforscheme;
        $scope.schemeordocument = langResponse[0].schemeordocument;
        $scope.scheme = langResponse[0].scheme;
        $scope.stage = langResponse[0].stage;
        $scope.responsereceive = langResponse[0].responsereceive;
        $scope.responserejection = langResponse[0].responserejection;
        $scope.responsedelay = langResponse[0].responsedelay;
        $scope.document = langResponse[0].document;
        $scope.monthyesrinfection = langResponse[0].monthyesrinfection;
        $scope.noofcondomask = langResponse[0].noofcondomask;
        $scope.noofcondomprovide = langResponse[0].noofcondomprovide;
        $scope.dateprovided = langResponse[0].dateprovided;
        $scope.saving = langResponse[0].saving;
        $scope.insurance = langResponse[0].insurance;
        $scope.pension = langResponse[0].pension;
        $scope.credit = langResponse[0].credit;
        $scope.remitance = langResponse[0].remitance;
        $scope.dateoflastsaving = langResponse[0].dateoflastsaving;
        $scope.state = langResponse[0].state;
        $scope.agegroup = langResponse[0].agegroup;
        $scope.gender = langResponse[0].gender;
        $scope.schemename = langResponse[0].schemename;
        $scope.schemelocalname = langResponse[0].schemelocalname;
        $scope.category = langResponse[0].category;
        $scope.select = langResponse[0].select;
        $scope.remitance = langResponse[0].remitance;
        $scope.clickyestorepeat = langResponse[0].clickyestorepeat;
        $scope.clicknotoview = langResponse[0].clicknotoview;
        $scope.searchfor = langResponse[0].searchfor;
        $scope.question = langResponse[0].question;
        $scope.answer = langResponse[0].answer;
        $scope.lastmodifytime = langResponse[0].lastmodifytime;
        $scope.monthavailed = langResponse[0].monthavailed;
        $scope.data = langResponse[0].data;
        $scope.ok = langResponse[0].ok;
        $scope.addbutton = langResponse[0].addbutton;
        $scope.questionprint = langResponse[0].question;
        $scope.selectprint = langResponse[0].select;
        $scope.monthofdetection = langResponse[0].monthofdetection;
        $scope.purposeofloan = langResponse[0].purposeofloan;
        $scope.amounttaken = langResponse[0].amounttaken;
        $scope.printnoofmonthrepay = langResponse[0].noofmonthrepay;
        $scope.printconsent = langResponse[0].consent;
        $scope.printconsentmsg = langResponse[0].consentmsg;
        $scope.optactiontaken = langResponse[0].optactiontaken;
        $scope.optreportedto = langResponse[0].optreportedto;
        $scope.optpreperator = langResponse[0].optpreperator;
        $scope.optincidenttype = langResponse[0].optincidenttype;
        $scope.atleastonemember = langResponse[0].atleastonemember;
        $scope.condomaskcantempty = langResponse[0].condomaskcantempty;
        $scope.condomprovidecantempty = langResponse[0].condomprovidecantempty;
        $scope.dateprovidedcantempty = langResponse[0].dateprovidedcantempty;
        $scope.gendernotmatch = langResponse[0].gendernotmatch;
        $scope.typologynotmatch = langResponse[0].typologynotmatch;
        $scope.agenotmatch = langResponse[0].agenotmatch;
        $scope.healthstnotmatch = langResponse[0].healthstnotmatch;
        $scope.socialstnotmatch = langResponse[0].socialstnotmatch;
        $scope.pnoactionreqmember = langResponse[0].noactionreqmember;
        $scope.noactiontaken = langResponse[0].noactiontaken;
        $scope.printaction = langResponse[0].action;
    });
});
