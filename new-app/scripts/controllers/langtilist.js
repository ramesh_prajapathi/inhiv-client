'use strict';

angular.module('secondarySalesApp')
    .controller('LangTictrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {


        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }


        $scope.showForm = function () {
            var visible = $location.path() === '/LangTI/create' || $location.path() === '/LangTI/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/LangTI/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/LangTI/create' || $location.path() === '/LangTI/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/LangTI/create' || $location.path() === '/LangTI/edit/' + $routeParams.id;
            return visible;
        };

        /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
            //console.log('$scope.pillarid', $scope.pillarid);
        }

        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
        if ($window.sessionStorage.prviousLocation != "partials/langtilist") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        // new changes for seacrh box

        $scope.someFocusVariable = true;
        $scope.filterFields = ['index', 'languageName', 'country', 'facility', 'address', 'helplineno'];
        $scope.Search = '';

        /*Restangular.all('languages?filter[where][deleteflag]=false').getList()..then(function (reslang) {
          $scope.Dislanguages = reslang;
     });*/

        //to show first option in language dropdown
        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('tilanguages?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (response) {
                $scope.tilanguages = response;

                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (reslang) {
                    $scope.Dislanguages = reslang;

                    angular.forEach($scope.tilanguages, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.Dislanguages.length; a++) {
                            if (member.language == $scope.Dislanguages[a].id) {
                                member.languageName = $scope.Dislanguages[a].name;
                                break;
                            }
                        }
                        // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
                    });
                });
            });
            $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().$object;


        } else {

            Restangular.all('tilanguages?filter[where][deleteflag]=false').getList().then(function (response) {
                $scope.tilanguages = response;

                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (reslang) {
                    $scope.Dislanguages = reslang;


                    angular.forEach($scope.tilanguages, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.Dislanguages.length; a++) {
                            if (member.language == $scope.Dislanguages[a].id) {
                                member.languageName = $scope.Dislanguages[a].name;
                                break;
                            }
                        }
                        // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
                    });

                });
            });
            $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        }





        /***************************************save & update ************************************/

        // $scope.languages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        $scope.$watch('langtilist.language', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id) {
                return;
            } else {

                Restangular.one('tilanguages?filter[where][language]=' + 1 + '&filter[where][deleteflag]=false').get().then(function (memblanguage) {
                    console.log('memblanguage', memblanguage);
                    $scope.original = memblanguage[0];
                    //$scope.memblanguage.language = newValue;
                    delete $scope.original['language'];
                    delete $scope.original['id'];
                    $scope.original.language = newValue;
                    $scope.langtilist = Restangular.copy($scope.original);
                });

                Restangular.all('tilanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    console.log('response', response);
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';

                    }
                });

            }
        });

        $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            setTimeout(function () {
                window.location = '/LangTIList';
            }, 250);
        };


        $scope.langtilist = {
            deleteflag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId
        };

        $scope.validatestring = '';
        $scope.Save = function () {
            /* old one
            Restangular.all('conditionLanguages').post($scope.condition).then(function (conResponse) {
                console.log('conResponse', conResponse);
                window.location = '/LangTIList';
            });*/

            /*****neww**/
            if ($scope.langtilist.language == '' || $scope.langtilist.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                Restangular.all('tilanguages').post($scope.langtilist).then(function (tiResponse) {
                    console.log('tiResponse', tiResponse);
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    window.location = '/LangTIList';

                });
            }



        };
        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };

        $scope.Update = function () {
            Restangular.one('tilanguages', $routeParams.id).customPUT($scope.langtilist).then(function (tiResponse) {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('tiResponse', tiResponse);
                setTimeout(function () {
                    window.location = '/LangTIList';
                }, 350);
            });
        };

        if ($routeParams.id) {
            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            $scope.message = 'TI Langauge has been Updated!';
            Restangular.one('tilanguages', $routeParams.id).get().then(function (langtilist) {
//                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (znes) {
//                    $scope.languages = znes;
//                    $scope.langtilist.language = langtilist.language;
                    $scope.original = langtilist;
                    $scope.langtilist = Restangular.copy($scope.original);
                //});
            });
        } else {
            $scope.message = 'TI Langauge has been Created!';
        }

        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /************************************************/


    });
