'use strict';

angular.module('secondarySalesApp')
    .controller('kitprescribedCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }


        $scope.showForm = function () {
            var visible = $location.path() === '/kitprescribed/create' || $location.path() === '/kitprescribed/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/kitprescribed/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/kitprescribed/create' || $location.path() === '/kitprescribed/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/kitprescribed/create' || $location.path() === '/kitprescribed/edit/' + $routeParams.id;
            return visible;
        };

$scope.disabledCOndition = false;
        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/kitprescribed-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /***new changes*****/
        $scope.showenglishLang = true;

        $scope.$watch('kitprescrib.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {

                    $scope.kitprescrib.conditionId = '';
                    $scope.kitprescrib.name = '';
                    $scope.kitprescrib.parentId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });
    
     $scope.$watch('kitprescrib.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {

                Restangular.one('kitprescribs', newValue).get().then(function (resp) {
                    console.log('resp', resp);
                    $scope.kitprescrib.conditionId = resp.conditionId;
                });
            }
        });
        /***new changes*****/




        if ($routeParams.id) {
            $scope.disabledCOndition = true;
            
            $scope.message = 'Kit Prescribed has been Updated!';
            Restangular.one('kitprescribs', $routeParams.id).get().then(function (kitprescrib) {
                $scope.original = kitprescrib;
                $scope.kitprescrib = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Kit Prescribed has been Created!';
        }

        //$scope.Search = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        //  $scope.MeetingTodos = [];
     $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
 $scope.conditions = Restangular.all('conditions?filter[where][deleteflag]=false&filter[where][language]=1').getList().$object;
        /******************************** INDEX *******************************************/
        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('kitprescribs?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.kitprescribs = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.kitprescribs, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });
            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.kitlanguages = sev;
            });

        } else {

            Restangular.all('kitprescribs').getList().then(function (mt) {
                $scope.kitprescribs = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.kitprescribs, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            
            
            Restangular.all('kitprescribs?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.kitlanguages = sev;
           
                      angular.forEach($scope.kitlanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                          
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.kitlanguages = sev;
                       });
                  }
              });

        }

        Restangular.all('kitprescribs?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.kitdisply = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        /***new changes*****/
        /********************************************* SAVE *******************************************/
        $scope.kitprescrib = {
            "name": '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            "deleteflag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";


            if ($scope.kitprescrib.language == '' || $scope.kitprescrib.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.kitprescrib.language == 1) {
                if ($scope.kitprescrib.conditionId == '' || $scope.kitprescrib.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Select Condition';
                }
                   else if ($scope.kitprescrib.name == '' || $scope.kitprescrib.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Kit Prescribed';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }

            } else if ($scope.kitprescrib.language != 1) {
                if ($scope.kitprescrib.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Kit Prescribed in English';

                } 
                else if ($scope.kitprescrib.conditionId == '' || $scope.kitprescrib.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                }
                else if ($scope.kitprescrib.name == '' || $scope.kitprescrib.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Kit Prescribed';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.kitprescrib.parentId === '') {
                    delete $scope.kitprescrib['parentId'];
                }
                $scope.submitDisable = true;
                $scope.kitprescrib.parentFlag = $scope.showenglishLang;
                $scope.kitprescribs.post($scope.kitprescrib).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/kitprescribed-list';
                    }, 350);

                }, function (error) {
                     $scope.submitDisable = false;
                    if (error.data.error.constraint === 'kitprescrib_lang_parenrid') {
                        alert('Value already exists for this language');
                        
                    }
                });

            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            if ($scope.kitprescrib.language == '' || $scope.kitprescrib.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";    

            } 
             else if ($scope.kitprescrib.language == 1) {
                if ($scope.kitprescrib.conditionId == '' || $scope.kitprescrib.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                } else if ($scope.kitprescrib.name == '' || $scope.kitprescrib.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Kit Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            }
            else if ($scope.kitprescrib.language != 1) {

                if ($scope.kitprescrib.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select kit Name in English';

                } else if ($scope.kitprescrib.conditionId == '' || $scope.kitprescrib.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                } else if ($scope.kitprescrib.name == '' || $scope.kitprescrib.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Kit Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }
            }
            
            /*else if ($scope.kitprescrib.name == '' || $scope.kitprescrib.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter kitprescrib';
                document.getElementById('name').style.borderColor = "#FF0000";

            }*/
            /** else if ($scope.kitprescrib.hnname == '' || $scope.kitprescrib.hnname == null) {
                 $scope.validatestring = $scope.validatestring + 'Please Enter kitprescrib in Hindi';
                 document.getElementById('hnname').style.borderColor = "#FF0000";

             } else if ($scope.kitprescrib.knname == '' || $scope.kitprescrib.knname == null) {
                 $scope.validatestring = $scope.validatestring + 'Please Enter kitprescrib in Kannada';
                 document.getElementById('knname').style.borderColor = "#FF0000";

             } else if ($scope.kitprescrib.taname == '' || $scope.kitprescrib.taname == null) {
                 $scope.validatestring = $scope.validatestring + 'Please Enter kitprescrib in Tamil';
                 document.getElementById('taname').style.borderColor = "#FF0000";

             } else if ($scope.kitprescrib.tename == '' || $scope.kitprescrib.tename == null) {
                 $scope.validatestring = $scope.validatestring + 'Please Enter kitprescrib in Telugu';
                 document.getElementById('tename').style.borderColor = "#FF0000";

             } else if ($scope.kitprescrib.mrname == '' || $scope.kitprescrib.mrname == null) {
                 $scope.validatestring = $scope.validatestring + 'Please Enter kitprescrib in Marathi';
                 document.getElementById('mrname').style.borderColor = "#FF0000";

             }*/ ///
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                //             $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                Restangular.one('kitprescribs', $routeParams.id).customPUT($scope.kitprescrib).then(function () {
                //                    console.log('Step Saved');
                //                    window.location = '/kitprescribed-list';
                //
                //
                //                });
                if ($scope.kitprescrib.parentId === '') {
                    delete $scope.kitprescrib['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('kitprescribs', $routeParams.id).customPUT($scope.kitprescrib).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('kitprescrib Name Saved');
                    setTimeout(function () {
                        window.location = '/kitprescribed-list';
                    }, 350);
                }, function (error) {
                     $scope.submitDisable = false;
                    if (error.data.error.constraint === 'kitprescrib_lang_parenrid') {
                        alert('Value already exists for this language');
                         
                    }
                });



            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('kitprescribs/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        
         /***************** Archive *****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('kitprescribs/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
