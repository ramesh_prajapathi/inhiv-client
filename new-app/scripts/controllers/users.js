'use strict';

angular.module('secondarySalesApp')
    .controller('UsersCtrl', function ($scope, Restangular, $http, $window, $route, $filter) {

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

       /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            // $scope.countryId = $window.sessionStorage.myRoute;
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        if ($window.sessionStorage.prviousLocation != "partials/users" || $window.sessionStorage.prviousLocation != "partials/users-form") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }
        /***********************************************************************************/
            
             $scope.someFocusVariable = true;
            $scope.filterFields = ['index','username','email','roleName'];
            $scope.searchUser = '';

            Restangular.all('users?filter[where][deleteflag]=false').getList().then(function (response) {
                 Restangular.all('roles?filter[where][deleteflag]=false').getList().then(function (rolesres) {
            $scope.RoleDisplay = rolesres;
                $scope.users = response;
                     console.log('$scope.users', $scope.users);
                angular.forEach($scope.users, function (member, index) {
                    
                     for (var b = 0; b < $scope.RoleDisplay.length; b++) {
                            if (member.roleId == $scope.RoleDisplay[b].id) {
                                member.roleName = $scope.RoleDisplay[b].name;
                                break;
                            }
                        }
                    
                    member.index = index + 1;
                    if (member.deleteflag === 'true') {
                        member.backgroundColor = "#ff0000"
                    } else {
                        member.backgroundColor = "#000000"
                    }
                });
                 });
            });

        $scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
            $scope.zones = znes;
           // $scope.countryId = $window.sessionStorage.user_zoneId;
        });


        $scope.getRole = function (stakeholdertype) {
            return Restangular.one('roles', stakeholdertype).get().$object;
        };
    
    


			/**************************************CALLING FUNCTION**********************/
        $scope.countryId = '';
     $scope.failityId = '';
        //$scope.stateId = '';
        //$scope.statesid = '';
        //$scope.countiesid = '';

        $scope.$watch('countryId', function (newValue, oldValue) {
            console.log(newValue);
            console.log(oldValue);
           if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.failityId = '';
               // $window.sessionStorage.user_zoneId = newValue;
                Restangular.all('employees?filter[where][state]=' + newValue).getList().then(function (responceSt) {
                    $scope.facilities = responceSt;
                });


                $scope.cities = Restangular.all('users?filter[where][state]=' + newValue).getList().then(function (ctyRes) {
                    $scope.users = ctyRes;
                    angular.forEach($scope.users, function (member, index) {
                        
                        for (var b = 0; b < $scope.RoleDisplay.length; b++) {
                            if (member.roleId == $scope.RoleDisplay[b].id) {
                                member.roleName = $scope.RoleDisplay[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                        if (member.deleteflag === 'true') {
                            member.backgroundColor = "#ff0000"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                    });
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('failityId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
               Restangular.all('users?filter[where][tiId]=' + newValue + '&filter[where][state]=' + $scope.countiesid).getList().then(function (ctyRes) {
                    $scope.users = ctyRes;
                    angular.forEach($scope.users, function (member, index) {
                        
                        for (var b = 0; b < $scope.RoleDisplay.length; b++) {
                            if (member.roleId == $scope.RoleDisplay[b].id) {
                                member.roleName = $scope.RoleDisplay[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                        if (member.deleteflag === 'true') {
                            member.backgroundColor = "#ff0000"
                        } else {
                            member.backgroundColor = "#000000"
                        }
                    });
                });
            }
        });

        $scope.updateUsrFlag = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('users/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.unArchive = function (id) {
            $scope.itemunarchive = [{
                deleteflag: false
            }]
            Restangular.one('users/' + id).customPUT($scope.itemunarchive[0]).then(function () {
                $route.reload();
            });
        }

    });