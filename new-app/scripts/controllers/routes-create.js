'use strict';

angular.module('secondarySalesApp')
	.controller('RoutesCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}

		$scope.isCreateView = true;
		$scope.heading = 'Site Create';
		$scope.fscodedisable = false;
		//$scope.routes = Restangular.all('distribution-routes').getList().$object;
		/*********************************** INDEX *******************************************/
		$scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;
		$scope.distributionroutes = Restangular.all('distribution-routes?filter[where][deleteflag]=false').getList().$object;

        $scope.$watch('route.countryId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
                $scope.route.state = '';
                $scope.route.district = '';
                $scope.route.town = '';
                $scope.route.tiId = '';
                
				$scope.zones = Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
				$scope.countryID = newValue;
			}
		});
    

		$scope.$watch('route.state', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
                 $scope.route.district = '';
                $scope.route.town = '';
                $scope.route.tiId = '';
                
				$scope.salesareas = Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countryID + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
				$scope.zonalid = newValue;
			}
		});


		$scope.$watch('route.district', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
                
                $scope.route.town = '';
                $scope.route.tiId = '';
                
                $scope.cities = Restangular.all('cities?filter[where][countryId]=' + $scope.countryID + '&filter[where][state]=' + $scope.zonalid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
				$scope.districtid = newValue;
			}
		});
    
    $scope.$watch('route.town', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
                 $scope.route.tiId = '';
                
				$scope.displaytis = Restangular.all('employees?filter[where][countryId]=' + $scope.countryID + '&filter[where][state]=' + $scope.zonalid +'&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
                
                console.log('$scope.displaytis', $scope.displaytis);
			}
		});

	$scope.route = {
			lastmodifiedtime: new Date(),
			lastmodifiedby: $window.sessionStorage.UserEmployeeId,
			deleteflag: false
		}

		$scope.modalTitle = 'Thank You';
		$scope.message = 'Site has been created';
		$scope.showValidation = false;
	
		$scope.toggleValidation = function () {
			console.log('toggleValidation');
			$scope.showValidation = !$scope.showValidation;
		};
    
    var timeoutPromise;
        var delayInMs = 1200;

        $scope.CheckDuplicate = function (name) {
           // console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.distributionroutes.filter(function (arr) {
                    return arr.name == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.routename = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Site Name Already Exist';
                 }
            }, delayInMs);
        };

		/************************************ SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Save = function () {
             var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
			document.getElementById('name').style.border = "";
			
			if ($scope.routename == '' || $scope.routename == null) {
				$scope.validatestring = $scope.validatestring + 'Please Enter Site Name';
				document.getElementById('name').style.borderColor = "#FF0000";
				
			}  else if (!regEmail.test($scope.routename)) {
                $scope.routename = '';
                 $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                document.getElementById('name').style.border = "1px solid #ff0000";
            }
            else if ($scope.route.countryId == '' || $scope.route.countryId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select Country';
				
			} else if ($scope.route.state === '' || $scope.route.state === null) {
				$scope.validatestring = $scope.validatestring + 'Please Select State';
				
			} 
            else if ($scope.route.district === '' || $scope.route.district === null) {
				$scope.validatestring = $scope.validatestring + 'Please Select District';
			}
            else if ($scope.route.town === '' || $scope.route.town === null) {
				$scope.validatestring = $scope.validatestring + 'Please Select Town';
			}
            
            else if ($scope.route.tiId == '' || $scope.route.tiId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select TI';
				//document.getElementById('state').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
                 var xyz = $scope.routename;
                
                $scope.route.name = xyz.toUpperCase();
                
                
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				Restangular.all('distribution-routes').post($scope.route).then(function (znResponse) {	
					console.log('$scope.route', $scope.znResponse);
					window.location = '/sites';
				});
			}
		};

	});
