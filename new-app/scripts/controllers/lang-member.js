'use strict';

angular.module('secondarySalesApp')
    .controller('LangMemberctrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {


        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }


        $scope.showForm = function () {
            var visible = $location.path() === '/LangMember/create' || $location.path() === '/LangMember/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/LangMember/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/LangMember/create' || $location.path() === '/LangMember/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/LangMember/create' || $location.path() === '/LangMember/edit/' + $routeParams.id;
            return visible;
        };


        /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
            //console.log('$scope.pillarid', $scope.pillarid);
        }

        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
        if ($window.sessionStorage.prviousLocation != "partials/lang-member") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }

        // new changes for seacrh box

        $scope.someFocusVariable = true;
        $scope.filterFields = ['index', 'languageName', 'fullname', 'country', 'state', 'district'];
        $scope.Search = '';




        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('memberlanguages?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (response) {
                $scope.memberlanguages = response;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Dislanguages = lang;
                    angular.forEach($scope.memberlanguages, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.Dislanguages.length; a++) {
                            if (member.language == $scope.Dislanguages[a].id) {
                                member.languageName = $scope.Dislanguages[a].name;
                                break;
                            }
                        }
                        // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
                    });
                });
            });
            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (resp) {
                $scope.languages = resp;
            });


        } else {

            Restangular.all('memberlanguages?filter[where][deleteflag]=false').getList().then(function (response) {
                $scope.memberlanguages = response;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Dislanguages = lang;
                    angular.forEach($scope.memberlanguages, function (member, index) {
                        member.index = index + 1;

                        for (var a = 0; a < $scope.Dislanguages.length; a++) {
                            if (member.language == $scope.Dislanguages[a].id) {
                                member.languageName = $scope.Dislanguages[a].name;
                                break;
                            }
                        }
                        // console.log(' $scope.lhsLanguages', $scope.lhsLanguages);
                    });
                });
            });
            Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (resp) {
                $scope.languages = resp;
            });

        }




        /***************************************save & update ************************************/


        $scope.$watch('memblanguage.language', function (newValue, oldValue) {
            // console.log('oldValue', oldValue);
            // console.log('newValue', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id) {
                return;
            } else {

                Restangular.one('memberlanguages?filter[where][language]=' + 1 + '&filter[where][deleteflag]=false').get().then(function (memblanguage) {
                    console.log('memblanguage', memblanguage);
                    $scope.original = memblanguage[0];
                    //$scope.memblanguage.language = newValue;
                    delete $scope.original['language'];
                    delete $scope.original['id'];
                    $scope.original.language = newValue;
                    $scope.memblanguage = Restangular.copy($scope.original);
                });


                Restangular.all('memberlanguages?filter[where][language]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (response) {
                    console.log('response', response);
                    if (response.length == 0) {
                        $scope.HideCreateButton = true;
                    } else {
                        $scope.toggleCheck();
                        $scope.validatestring1 = 'This language Value already exist go and Update';

                    }
                });

            }

        });

        $scope.OKBUTTON = function () {
            $scope.toggleCheck();
            window.location = '/LangMemberList';

        };

        $scope.memblanguage = {
            deleteflag: false,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId
        };


        $scope.validatestring = '';
        $scope.Save = function () {
            if ($scope.memblanguage.language == '' || $scope.memblanguage.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';


            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                Restangular.all('memberlanguages').post($scope.memblanguage).then(function (conResponse) {
                    console.log('conResponse', conResponse);
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/LangMemberList';
                    }, 350);
                });
            }

        };
        $scope.modalTitle = 'Thank You';
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        $scope.toggleCheck = function () {
            $scope.showValidationNew = !$scope.showValidationNew;
        };


        $scope.Update = function () {
            Restangular.one('memberlanguages', $routeParams.id).customPUT($scope.memblanguage).then(function (conResponse) {
                console.log('conResponse', conResponse);
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                    window.location = '/LangMemberList';
                }, 350);
            });
        };

        if ($routeParams.id) {
            $scope.HideCreateButton = false;
            $scope.langdisable = true;
            $scope.message = 'Member Langauge has been Updated!';
            Restangular.one('memberlanguages', $routeParams.id).get().then(function (memblanguage) {

                // Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (znes) {
                //  $scope.languages = znes;
                //  $scope.memblanguage.language = memblanguage.language;

                $scope.original = memblanguage;

                $scope.memblanguage = Restangular.copy($scope.original);
                //});
            });
        } else {
            $scope.message = 'Member Langauge has been Created!';
        }



        /**************************Sorting **********************************/

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /************************************************/




    })
    .directive('checkmodal', function () {
        return {
            template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-danger modal-sm">' + '<div class="modal-content">' + '<div class="modal-header">' + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="modal-body" ng-transclude></div>' + '</div>' + '</div>' + '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title1 = attrs.title1;
                scope.$watch(attrs.visible, function (value) {
                    // console.log('value', value);
                    if (value == true) {
                        //console.log('elementif', element[0]);
                        $(element).modal('show');
                        // document.getElementsByClassName("modal-dialog").modal='show';
                    } else {
                        // console.log('elementelse', element[0]);
                        $(element).modal('hide');
                        //document.getElementsByClassName("modal-dialog").modal='hide';
                    }
                });
                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });
                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
