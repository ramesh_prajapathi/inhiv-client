'use strict';

angular.module('secondarySalesApp')
	.controller('TiManagerCreateCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
    
    $scope.isCreateView = true;
		$scope.fscodedisable = false;
		$scope.fsnamedisable = false;
		$scope.hfdflagDisabled = false;
		$scope.heading = 'TI Manager Create';
    
    
    
    $scope.partner = {
			hdfflag: true,
			deleteflag: false,
			usercreated: false,
			//state: '',
			latitude: '',
			longitude: '',
			address: '',
			//district: '',
			//state: $window.sessionStorage.zoneId,
			//district: $window.sessionStorage.salesAreaId,
			lastmodifiedtime: new Date(),
			//facility: $window.sessionStorage.coorgId,
			lastmodifiedby: $window.sessionStorage.UserEmployeeId
		};

		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}
    
    $scope.displaytis = Restangular.all('employees?filter[where][deleteflag]=false').getList().$object;
     $scope.roleDisplays = Restangular.all('roles?filter[where][deleteflag]=false' + '&filter[where][rolefalg]=A').getList().$object;
    
		$scope.$watch('partner.tiId', function (newValue, oldValue) {
			console.log('newValue', newValue);
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				Restangular.one('employees', newValue).get().then(function (response) {
					$scope.getcountryId = response.countryId;
                    $scope.getStateId = response.state;
					$scope.gettDistricId = response.district;
					$scope.gettTownId = response.town;
                   $scope.gettartId = response.artId;
					$scope.gettictcId = response.ictcId;
                    //$scope.partner.artName = response.artName;
                   // $scope.partner.ictcName = response.ictcName;
                   // $scope.partner.artId = response.artId;
                   // $scope.partner.ictcId = response.ictcId;
                    
                    Restangular.one('countries?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.getcountryId).get().then(function (countryResp) {
						$scope.countryname = countryResp[0].name;
						$scope.partner.countryId = countryResp[0].id;
						console.log('country', $scope.partner.countryId);
					});
                    
                     Restangular.one('zones?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.getStateId).get().then(function (responseZone) {
						$scope.statename = responseZone[0].name;
						$scope.partner.state = responseZone[0].id;
						console.log('state', $scope.partner.state);
					});
                    
					Restangular.one('sales-areas?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.gettDistricId).get().then(function (responsesalesAreas) {
						$scope.districtname = responsesalesAreas[0].name;
						$scope.partner.district = responsesalesAreas[0].id;
						console.log('district', $scope.partner.district);
					});
                    
                    Restangular.one('cities?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.gettTownId).get().then(function (responseTown) {
						$scope.townname = responseTown[0].name;
						$scope.partner.town = responseTown[0].id;
						console.log('town', $scope.partner.town);
					});
                    
                     Restangular.one('artcenters?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.gettartId).get().then(function (responseARt) {
						$scope.artName = responseARt[0].name;
						$scope.partner.artId = responseARt[0].id;
                         $scope.partner.artName = responseARt[0].name;
						//console.log('town', $scope.partner.town);
					});
                    
                    Restangular.one('ictccenters?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.gettictcId).get().then(function (responseICTc) {
						$scope.ictcName = responseICTc[0].name;
                        $scope.partner.ictcName = responseICTc[0].name;
						$scope.partner.ictcId = responseICTc[0].id;
						//console.log('town', $scope.partner.town);
					});
				});

				$scope.zonalid = newValue;

			}
		});

		$scope.modalTitle = 'Thank You';
		$scope.message = 'TI Manager has been Created!';
		/******************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.submitDisable = false;
		$scope.Save = function () {
			document.getElementById('name').style.border = "";
			if ($scope.partner.tiId == '' || $scope.partner.tiId == null) {
				$scope.validatestring = $scope.validatestring + 'Please Select TI Code';
			} else if ($scope.partner.name == '' || $scope.partner.name == null) {
				$scope.validatestring = $scope.validatestring + 'Plese Enter Name';
				document.getElementById('name').style.borderColor = "#FF0000";
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
				$scope.submitDisable = true;

				console.log('$scope.partner', $scope.partner);
				Restangular.all('comembers').post($scope.partner).then(function () {
					console.log('$scope.partner', $scope.partner);
					window.location = '/profile';
				});
			}
		};

		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
    
    /************************** Map *************************/

        $scope.mapdataModal = false;

        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.partner.latitude = latLng.lat();
                $scope.partner.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };

    
});