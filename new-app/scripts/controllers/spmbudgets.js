'use strict';
angular.module('secondarySalesApp').controller('spmbudgetsCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $modal) {

    console.log('$window.sessionStorage.zoneId', $window.sessionStorage.zoneId);
    console.log('$window.sessionStorage.salesAreaId', $window.sessionStorage.salesAreaId);
    console.log('$window.sessionStorage.UserEmployeeId', $window.sessionStorage.UserEmployeeId);
    Restangular.all('employees?filter[where][stateId]=' + $window.sessionStorage.zoneId + '&filter[where][deleteflag]=false').getList().then(function (coRes) {
        $scope.comembers = coRes;
    });
    $scope.curentY = $filter('date')(new Date(), 'yyyy');
    $scope.curentYear = parseInt($scope.curentY);
    $scope.nextYear = $scope.curentYear + 1;
    $scope.cominedYear = $scope.curentYear + '-' + $scope.nextYear;
    $scope.saveDisable = true;
    //console.log('$scope.cominedYear', $scope.cominedYear);
    Restangular.all('spmbudgetyears?filter[order]=name%20ASC').getList().then(function (bud) {
        $scope.spmbudgetyears = bud;
        var alength = bud.length - 1;
        $scope.spmbudgetyears2 = bud[alength];
        $scope.current_Year_Name = $scope.spmbudgetyears2.name.split("-");
        $scope.yearone = parseInt($scope.current_Year_Name[1]);
        $scope.yeartwo = $scope.yearone + 1;
        $scope.nextyear = $scope.yearone + '-' + $scope.yeartwo;
        //console.log('$scope.nextyear', $scope.nextyear);
        $scope.spmbudgetyear.name = $scope.nextyear;
    });
    $scope.spmbudgetyear = {
        deleteflag: false,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        lastmodifiedbyrole: $window.sessionStorage.roleId
    };
    $scope.spmbudget = {
        deleteflag: false,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        lastmodifiedbyrole: $window.sessionStorage.roleId,
        facility: $window.sessionStorage.coorgId,
    };
    $scope.Add_Year = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/AddYear.html',
            scope: $scope,
            backdrop: 'static',
            size: 'md'
        });
    }
    $scope.budgetDisable = false;
    $scope.Budget = function () {
        $scope.budgetDisable = false;
        Restangular.all('spmbudgetyears').post($scope.spmbudgetyear).then(function (spmYear) {
            //console.log('spmYear', spmYear)
            $scope.modalInstance1.close();
            location.reload();
        });
        //$route.reload();
    }
    $scope.OkValidation = function () {
        $scope.modalInstance1.close();
    };
    $scope.toggleValidation = function () {
        $scope.modalInstance1 = $modal.open({
            animation: true,
            templateUrl: 'template/validation.html',
            scope: $scope,
            backdrop: 'static',
            size: 'sm'
        });
    };
    $scope.YearFunction = function (selyr) {
        $scope.your_Selected_Year = selyr;
    }

    $scope.$watch('spmbudget.monitoringevaluation', function (newValue, oldValue) {
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            $scope.spmbudget.totalamount = parseInt($scope.spmbudget.humanresources) + parseInt($scope.spmbudget.travelandcommunication) + parseInt($scope.spmbudget.genericactivity) + parseInt($scope.spmbudget.specificactivity) + parseInt($scope.spmbudget.msgtgactivity) + parseInt($scope.spmbudget.administrativeexpense) + parseInt($scope.spmbudget.monitoringevaluation);
        }
    });

    $scope.$watch('spmbudget.facility', function (newValue, oldValue) {
        $scope.saveDisable = true;
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            console.log('newValue', newValue);
           
            if (newValue != null && newValue != 'null') {
               
                Restangular.one('employees', newValue).get().then(function (emp) {
                    //console.log('emp', emp)
                    $scope.spmbudget.state = emp.stateId;
                    $scope.spmbudget.district = emp.district;
                });

                Restangular.all('comembers?filter[where][facility]=' + newValue + '&filter[where][deleteflag]=false&filter[where][hdfflag]=false').getList().then(function (comembers) {
                    //console.log("comembers", comembers);
                    if (comembers.length > 0) {
                        $scope.spmbudget.comember = comembers[0].id;

                        $scope.saveDisable = false;
                    } else {
                        alert('There is no CO for this facility');
                    }
                });
            }
        }
    });


    $scope.stakeholderdataModal = false;
    $scope.validatestring = '';
    /* $scope.Test = function () {
         //Your_Selected_Year
         console.log('Your_Selected_Co', $scope.Your_Selected_Co);
         Restangular.all('spmbudgets').getList().then(function (check) {
             for (var i = 0; i < check.length; i++) {
                 if (check[i].facility == $scope.Your_Selected_Co);
                 alert('This CO Already Having Budgets')
             }
         });
     }*/
    $scope.Save = function () {

        $scope.spmbudget.balancetilldate = $scope.spmbudget.totalamount;

        document.getElementById('humanresources').style.borderColor = "";
        document.getElementById('travelandcommunication').style.borderColor = "";
        document.getElementById('genericactivity').style.borderColor = "";
        document.getElementById('specificactivity').style.borderColor = "";
        document.getElementById('travelandcommunication').style.borderColor = "";
        document.getElementById('administrativeexpense').style.borderColor = "";
        document.getElementById('monitoringevaluation').style.borderColor = "";

        if ($scope.spmbudget.year == '' || $scope.spmbudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Budget Year';
            //document.getElementById('humanresources').style.borderColor = "#FF0000";
        } else if ($scope.spmbudget.comember == '' || $scope.spmbudget.comember == null) {
            $scope.validatestring = $scope.validatestring + 'Select CO';
            //document.getElementById('humanresources').style.borderColor = "#FF0000";
        }
        /*else if ($scope.spmbudget.facility != '' || $scope.spmbudget.facility != null) {
            Restangular.all('spmbudgets').getList().then(function (check) {
                for (var i = 0; i < check.length; i++) {
                    if (check[i].facility == $scope.spmbudget.facility);
                    alert('This CO Already Having Budgets')
                    }
            });
        }*/
        else if ($scope.spmbudget.humanresources == '' || $scope.spmbudget.humanresources == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount';
            document.getElementById('humanresources').style.borderColor = "#FF0000";
            //$scope.Test();
        } else if ($scope.spmbudget.travelandcommunication == '' || $scope.spmbudget.travelandcommunication == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount';
            document.getElementById('travelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.spmbudget.genericactivity == '' || $scope.spmbudget.genericactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount';
            document.getElementById('genericactivity').style.borderColor = "#FF0000";
        } else if ($scope.spmbudget.specificactivity == '' || $scope.spmbudget.specificactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount';
            document.getElementById('specificactivity').style.borderColor = "#FF0000";
        } else if ($scope.spmbudget.msgtgactivity == '' || $scope.spmbudget.msgtgactivity == null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSG/TG Activity Amount';
            document.getElementById('travelandcommunication').style.borderColor = "#FF0000";
        } else if ($scope.spmbudget.administrativeexpense == '' || $scope.spmbudget.administrativeexpense == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount';
            document.getElementById('administrativeexpense').style.borderColor = "#FF0000";
        } else if ($scope.spmbudget.monitoringevaluation == '' || $scope.spmbudget.monitoringevaluation == null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount';
            document.getElementById('monitoringevaluation').style.borderColor = "#FF0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            //
            $scope.submitDisable = true;

            //  $scope.spmbudget.totalamount = parseInt($scope.spmbudget.humanresources) + parseInt($scope.spmbudget.travelandcommunication) + parseInt($scope.spmbudget.genericactivity) + parseInt($scope.spmbudget.specificactivity) + parseInt($scope.spmbudget.msgtgactivity) + parseInt($scope.spmbudget.administrativeexpense) + parseInt($scope.spmbudget.monitoringevaluation);

            Restangular.all('spmbudgets').post($scope.spmbudget).then(function (postResponse) {
                console.log('$scope.spmbudget', $scope.spmbudget);
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                window.location = '/spmbudgets';
            }, function (error) {
                // $scope.UpdateClicked = false;
                console.log('fw error', error);
                // $scope.toggleValidation();
                //  $scope.validatestring1 = "This CO budget is already created for this year refresh the page and try again";
                if (error.data.error.constraint == 'unique_year_comember') {
                    $scope.toggleValidation();
                    $scope.validatestring1 = "This CO budget is already created for this year refresh the page and try again";
                }
            });
        }
    };

    $scope.Update = function () {

        document.getElementById('humanresources').style.borderColor = "";
        document.getElementById('travelandcommunication').style.borderColor = "";
        document.getElementById('genericactivity').style.borderColor = "";
        document.getElementById('specificactivity').style.borderColor = "";
        document.getElementById('travelandcommunication').style.borderColor = "";
        document.getElementById('administrativeexpense').style.borderColor = "";
        document.getElementById('monitoringevaluation').style.borderColor = "";

        if ($scope.spmbudget.year == '' || $scope.spmbudget.year == null) {
            $scope.validatestring = $scope.validatestring + 'Select Budget Year';
            //document.getElementById('humanresources').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.comember == '' || $scope.spmbudget.comember == null) {
            $scope.validatestring = $scope.validatestring + 'Select CO';
            //document.getElementById('humanresources').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.humanresources === '' || $scope.spmbudget.humanresources === null) {
            $scope.validatestring = $scope.validatestring + 'Enter Human Resource Amount';
            document.getElementById('humanresources').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.travelandcommunication === '' || $scope.spmbudget.travelandcommunication === null) {
            $scope.validatestring = $scope.validatestring + 'Enter Travel and Communication Amount';
            document.getElementById('travelandcommunication').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.genericactivity === '' || $scope.spmbudget.genericactivity === null) {
            $scope.validatestring = $scope.validatestring + 'Enter Generic Activity Amount';
            document.getElementById('genericactivity').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.specificactivity === '' || $scope.spmbudget.specificactivity === null) {
            $scope.validatestring = $scope.validatestring + 'Enter Specific Activity Amount';
            document.getElementById('specificactivity').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.msgtgactivity === '' || $scope.spmbudget.msgtgactivity === null) {
            $scope.validatestring = $scope.validatestring + 'Enter MSG/TG Activity Amount';
            document.getElementById('travelandcommunication').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.administrativeexpense === '' || $scope.spmbudget.administrativeexpense === null) {
            $scope.validatestring = $scope.validatestring + 'Enter Administrative Expense Amount';
            document.getElementById('administrativeexpense').style.borderColor = "#FF0000";

        } else if ($scope.spmbudget.monitoringevaluation === '' || $scope.spmbudget.monitoringevaluation === null) {
            $scope.validatestring = $scope.validatestring + 'Enter Monitoring Evaluation Amount';
            document.getElementById('monitoringevaluation').style.borderColor = "#FF0000";
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {

            $scope.submitDisable = true;
            $scope.spmbudget.totalamount = parseInt($scope.spmbudget.humanresources) + parseInt($scope.spmbudget.travelandcommunication) + parseInt($scope.spmbudget.genericactivity) + parseInt($scope.spmbudget.specificactivity) + parseInt($scope.spmbudget.msgtgactivity) + parseInt($scope.spmbudget.administrativeexpense) + parseInt($scope.spmbudget.monitoringevaluation);
            Restangular.one('spmbudgets', $routeParams.id).customPUT($scope.spmbudget).then(function () {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('UPDATE', $scope.spmbudget);
                window.location = '/spmbudgets';
            }, function (error) {
                if (error.data.error.constraint == 'unique_year_comember') {
                    $scope.toggleValidation();
                    $scope.validatestring1 = "This CO budget is already created for this year refresh the page and try again";
                }
            });
        }
    }
    $scope.SPM_Budget_Year = '';
    $scope.SPM_Budget_CO = '';
    $scope.$watch('SPM_Budget_Year', function (newValue, oldValue) {
        //console.log('SPM_Budget_Year', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            $scope.SPM_Budget_CO = '';
            Restangular.all('spmbudgets?filter[where][year]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (respon) {
                $scope.spmbudgets = respon;
                angular.forEach($scope.spmbudgets, function (member, index) {
                    member.index = index + 1;
                });
            });
            $scope.yearid = +newValue;
        }
    });
    $scope.$watch('SPM_Budget_CO', function (newValue, oldValue) {
        // console.log('SPM_Budget_CO', newValue);
        if (newValue === oldValue || newValue == '') {
            return;
        } else {
            Restangular.all('comembers?filter[where][facility]=' + newValue + '&filter[where][deleteflag]=false&filter[where][hdfflag]=false').getList().then(function (comembers) {
                if (comembers.length > 0) {
                    Restangular.all('spmbudgets?filter[where][comember]=' + comembers[0].id + '&filter[where][year]=' + $scope.yearid + '&filter[where][deleteflag]=false').getList().then(function (respon) {
                        $scope.spmbudgets = respon;
                        angular.forEach($scope.spmbudgets, function (member, index) {
                            member.index = index + 1;
                        });
                    });
                    $scope.Your_Selected_Co = comembers[0].id;
                }
            });

        }
    });
    $scope.Comemer = function (coid) {
        return Restangular.one('employees', coid).get().$object;
    }
    $scope.isCreateView = false;
    $scope.modalTitle = 'Thank You';


    if ($routeParams.id) {
        $scope.isCreateView = true;
        $scope.message = 'Budget has been Updated!';
        $scope.CO_Budget_Dispaly = false;
        //$scope.statecodeDisable = true;
        //$scope.membercountDisable = true;
        Restangular.one('spmbudgets', $routeParams.id).get().then(function (spmbud) {
            $scope.original = spmbud;
            $scope.spmbudget = Restangular.copy($scope.original);
            // $scope.spmbudget_Year = $scope.original.year;
            //$scope.spmbudget_facility = $scope.original.facility;
            // console.log('$scope.spmbudget_Year' + ':' + $scope.spmbudget_Year + '$scope.spmbudget_facility' + ':' + $scope.spmbudget_facility);
            Restangular.all('cobudgets?filter[where][year]=' + spmbud.year + '&filter[where][facility]=' + spmbud.facility).getList().then(function (cobudget) {
                console.log('cobudget', cobudget)
                for (var i = 0; i < cobudget.length; i++) {
                    $scope.CO_Budget_HumanResources = $scope.CO_Budget_HumanResources + cobudget[i].humanresources;
                    $scope.CO_Budget_TravelandCommunication = $scope.CO_Budget_TravelandCommunication + cobudget[i].travelandcommunication;
                    $scope.CO_Budget_GenericActivity = $scope.CO_Budget_GenericActivity + cobudget[i].genericactivity;
                    $scope.CO_Budget_SpecificActivity = $scope.CO_Budget_SpecificActivity + cobudget[i].specificactivity;
                    $scope.CO_Budget_MsgtActivity = $scope.CO_Budget_MsgtActivity + cobudget[i].msgtgactivity;
                    $scope.CO_Budget_AdministrativeExpence = $scope.CO_Budget_AdministrativeExpence + cobudget[i].administrativeexpense;
                    $scope.CO_Budget_MoniteringEvaluation = $scope.CO_Budget_MoniteringEvaluation + cobudget[i].monitoringevaluation;
                    $scope.CO_Budget_Total = $scope.CO_Budget_Total + cobudget[i].totalamount;
                    console.log('$scope.CO_Budget_HumanResources', $scope.CO_Budget_HumanResources);
                }
            });
        });



    } else {
        $scope.message = 'Budget has been created!';
        $scope.CO_Budget_Dispaly = true;
    }
    $scope.CO_Budget_HumanResources = 0;
    $scope.CO_Budget_TravelandCommunication = 0
    $scope.CO_Budget_GenericActivity = 0;
    $scope.CO_Budget_SpecificActivity = 0;
    $scope.CO_Budget_MsgtActivity = 0;
    $scope.CO_Budget_AdministrativeExpence = 0;
    $scope.CO_Budget_MoniteringEvaluation = 0;
    $scope.CO_Budget_Total = 0;
    /*Restangular.one('spmbudgets', $routeParams.id).get().then(function (spmbud) {
        $scope.original = spmbud;
        $scope.spmbudget_Year = $scope.original.year;
        $scope.spmbudget_facility = $scope.original.facility;
        console.log('$scope.spmbudget_Year' + ':' + $scope.spmbudget_Year + '$scope.spmbudget_facility' + ':' + $scope.spmbudget_facility);
        
    });*/

    $scope.Delete = function (id) {
        $scope.message = 'Budget has been Deleted!';
        $scope.item = [{
            deleteflag: true
            }]
        Restangular.one('spmbudgets/' + id).customPUT($scope.item[0]).then(function () {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $route.reload();
        });
    }
});
