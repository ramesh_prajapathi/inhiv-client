'use strict';

angular.module('secondarySalesApp')
    .controller('StatesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/states/create' || $location.path() === '/states/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/states/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/states/create' || $location.path() === '/states/' + $routeParams.id;
            return visible;
        };

        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/states/create' || $location.path() === '/states/' + $routeParams.id;
            return visible;
        };

        /*********/

        $scope.countries = Restangular.all('countries').getList().$object;

        $scope.getCountry = function (countryId) {
            return Restangular.one('countries', countryId).get().$object;
        };
        /************************************************************************* INDEX **********************************/

        //$scope.states = Restangular.all('states').getList().$object;
        $scope.searchCon = $scope.name;

        $scope.con = Restangular.all('states').getList().then(function (con) {
            $scope.states = con;
            angular.forEach($scope.states, function (member, index) {
                member.index = index + 1;
            });
        });


        $scope.state = {};

        /********************************************************************************* SAVE ********************************/
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";

            if ($scope.state.name == '' || $scope.state.name == null) {
                $scope.state.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your state name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            } else if ($scope.state.countryId == '' || $scope.state.countryId == null) {
                $scope.state.countryId = null;
                $scope.validatestring = $scope.validatestring + 'Plese select your country name';
                document.getElementById('countryId').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.states.post($scope.state).then(function () {
                    console.log('$scope.state', $scope.state);
                    window.location = '/states';
                });
            }
        };
        /**************************************************************************** UPDATE ********************************/

        if ($routeParams.id) {
            Restangular.one('states', $routeParams.id).get().then(function (state) {
                $scope.original = state;
                $scope.state = Restangular.copy($scope.original);
            });
        }



        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.state.name == '' || $scope.state.name == null) {
                $scope.state.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your state name';
                document.getElementById('name').style.border = "1px solid #ff0000";

            } else if ($scope.state.countryId == '' || $scope.state.countryId == null) {
                $scope.state.countryId = null;
                $scope.validatestring = $scope.validatestring + 'Plese select your country name';
                document.getElementById('countryId').style.border = "1px solid #ff0000";

            }
            if ($scope.validatestring != '') {
                alert($scope.validatestring);
                $scope.validatestring = '';
            } else {
                $scope.states.customPUT($scope.state).then(function () {
                    console.log('$scope.state', $scope.state);
                    window.location = '/states';
                });
            }
        };

        /**************************************************************************** DELETE ********************************/
        $scope.Delete = function (id) {
            if (confirm("Are you sure want to delete..!") == true) {
                Restangular.one('states/' + id).remove($scope.state).then(function () {
                    $route.reload();
                });

            } else {

            }

        }




        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }



        /********************************************************************* CALL ********************************/
        $scope.countryId = '';
        $scope.zonalid = '';

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue) {

                return;
            } else {
                $scope.states = Restangular.all('states?filter[where][countryId]=' + newValue).getList().then(function (state) {

                    $scope.states = state;
                    $scope.zonalid = +newValue;



                    angular.forEach($scope.states, function (member, index) {
                        member.index = index + 1;

                    })
                });
            }
        });



    });
