'use strict';

angular.module('secondarySalesApp')
	.controller('reasonforrejectionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter ) {
		/*********/
		//$scope.modalTitle = 'Thank You';
		//$scope.message = 'reasonforrejection has been created!';
		$scope.showForm = function () {
			var visible = $location.path() === '/reasonforrejection/create' || $location.path() === '/reasonforrejection/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/reasonforrejection/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/reasonforrejection/create' || $location.path() === '/reasonforrejection/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/reasonforrejection/create' || $location.path() === '/reasonforrejection/' + $routeParams.id;
			return visible;
		};
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}


		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 5;
		} else {
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		}


		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		if ($window.sessionStorage.prviousLocation != "partials/reasonforrejection") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPagesize = 5;
			$window.sessionStorage.myRoute_currentPage = 1;
			$scope.currentpage = 1;
			$scope.pageSize = 5;
		}

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
		/*********************************** INDEX *******************************************/

		$scope.part = Restangular.all('reasonforrejections?filter[where][deleteflag]=false').getList().then(function (part) {
			$scope.reasonforrejections = part;
			$scope.reasonforrejectionId = $window.sessionStorage.sales_reasonforrejectionId;
			angular.forEach($scope.reasonforrejections, function (member, index) {
				member.index = index + 1;

				$scope.TotalTodos = [];
				$scope.TotalTodos.push(member);
			});
		});

		/*-------------------------------------------------------------------------------------*/
		$scope.reasonforrejection = {
			deleteflag: false
		};

		$scope.statecodeDisable = false;
		$scope.membercountDisable = false;
		if ($routeParams.id) {
            $scope.message = 'Reason for Rejection has been Updated';
			$scope.statecodeDisable = true;
			$scope.membercountDisable = true;
			Restangular.one('reasonforrejections', $routeParams.id).get().then(function (reasonforrejection) {
				$scope.original = reasonforrejection;
				$scope.reasonforrejection = Restangular.copy($scope.original);
			});
		} else {
            $scope.message = 'Reason for Rejection has been Created';
        }
		/************* SAVE *******************************************

		$scope.submitDisable = false;
		$scope.Savereasonforrejection = function (clicked) {
			document.getElementById('name').style.border = "";
			if ($scope.reasonforrejection.name == '' || $scope.reasonforrejection.name == null) {
				toaster.pop('error', "", 'Enter Reason For Rejection', null, 'trustedHtml');
				document.getElementById('name').style.borderColor = "#FF0000";
			} else {
				$scope.submitDisable = true;
				toaster.pop('success', "Reason For Rejection has been created", null, null, 'trustedHtml');
				Restangular.all('reasonforrejections').post($scope.reasonforrejection).then(function (Response) {
					window.location = '/reasonforrejection';
					
				});
			}
		};

		/***************************** UPDATE *******************************************
		$scope.Updatereasonforrejection = function () {
			document.getElementById('name').style.border = "";
			if ($scope.reasonforrejection.name == '' || $scope.reasonforrejection.name == null) {
				toaster.pop('error', "", 'Enter Reason For Rejection', null, 'trustedHtml');
				document.getElementById('name').style.borderColor = "#FF0000";
			} else {
				$scope.submitDisable = true;
				toaster.pop('success', "Reason For Rejection has been updated", null, null, 'trustedHtml');
				Restangular.one('reasonforrejections', $routeParams.id).customPUT($scope.reasonforrejection).then(function () {
					$location.path('/reasonforrejection');
				});
			}
		};
    */
     $scope.validatestring = '';
    $scope.Savereasonforrejection = function () {
        if ($scope.reasonforrejection.name == '' || $scope.reasonforrejection.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection';
        }
        else if ($scope.reasonforrejection.hnname == '' || $scope.reasonforrejection.hnname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in hindi';
        }
        else if ($scope.reasonforrejection.knname == '' || $scope.reasonforrejection.knname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in kannada';
        }
        else if ($scope.reasonforrejection.taname == '' || $scope.reasonforrejection.taname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in tamil';
        }
        else if ($scope.reasonforrejection.tename == '' || $scope.reasonforrejection.tename == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in telugu';
        }
        else if ($scope.reasonforrejection.mrname == '' || $scope.reasonforrejection.mrname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in marathi';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            $scope.schemestagedataModal = !$scope.schemestagedataModal;
            $scope.submitDisable = true;
            //toaster.pop('success', "Reason For rejection has been created", null, null, 'trustedHtml');
            Restangular.all('reasonforrejections').post($scope.reasonforrejection).then(function (Response) {
                window.location = '/reasonforrejection';
            });
        }
    };
    /***************************** UPDATE *******************************************/
    $scope.Updatereasonforrejection = function () {
        if ($scope.reasonforrejection.name == '' || $scope.reasonforrejection.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection';
        }
        else if ($scope.reasonforrejection.hnname == '' || $scope.reasonforrejection.hnname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in hindi';
        }
        else if ($scope.reasonforrejection.knname == '' || $scope.reasonforrejection.knname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in kannada';
        }
        else if ($scope.reasonforrejection.taname == '' || $scope.reasonforrejection.taname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in tamil';
        }
        else if ($scope.reasonforrejection.tename == '' || $scope.reasonforrejection.tename == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in telugu';
        }
        else if ($scope.reasonforrejection.mrname == '' || $scope.reasonforrejection.mrname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter reason for rejection in marathi';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
           $scope.schemestagedataModal = !$scope.schemestagedataModal;
            $scope.submitDisable = true;
            Restangular.one('reasonforrejections', $routeParams.id).customPUT($scope.reasonforrejection).then(function () {
                window.location = '/reasonforrejection';
            });
        }
    };
    $scope.modalTitle = 'Thank You';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
	/*---------------------------Delete---------------------------------------------------*/
	
		$scope.Delete = function (id) {
			//toaster.pop('success', "title", '<ul><li>Render html1</li></ul>', 5000, 'trustedHtml');
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('reasonforrejections/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}
		

	});
