'use strict';
angular.module('secondarySalesApp').controller('BudgetYearCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $modal) {
    /*********/
    if ($window.sessionStorage.roleId != 1) {
        window.location = "/";
    }
    $scope.showForm = function () {
        var visible = $location.path() === '/budgetyear/create' || $location.path() === '/budgetyear/' + $routeParams.id;
        return visible;
    };
    /* $scope.isCreateView = function () {
         if ($scope.showForm()) {
             var visible = $location.path() === '/budgetyear/create';
             return visible;
         }
     };
     $scope.hideCreateButton = function () {
         var visible = $location.path() === '/budgetyear/create' || $location.path() === '/budgetyear/' + $routeParams.id;
         return visible;
     };
     $scope.hideSearchFilter = function () {
         var visible = $location.path() === '/budgetyear/create' || $location.path() === '/budgetyear/' + $routeParams.id;
         return visible;
     };
     /*********************************** Pagination *******************************************
     if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
         $window.sessionStorage.myRoute = null;
         $window.sessionStorage.myRoute_currentPage = 1;
         $window.sessionStorage.myRoute_currentPagesize = 25;
     }
     else {
         $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
         $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
     }
     if ($window.sessionStorage.prviousLocation != "partials/budgetyear") {
         $window.sessionStorage.myRoute_currentPage = 1;
         $window.sessionStorage.myRoute_currentPagesize = 25;
     }
     $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
     $scope.PageChanged = function (newPage, oldPage) {
         $scope.currentpage = newPage;
         $window.sessionStorage.myRoute_currentPage = newPage;
     };
     $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
     $scope.pageFunction = function (mypage) {
         $scope.pageSize = mypage;
         $window.sessionStorage.myRoute_currentPagesize = mypage;
     };
     /*********/
    // $scope.submitstakeholdertypes = Restangular.all('financialgoals').getList().$object;
    //    if ($routeParams.id) {
    //        $scope.message = 'budgetyear type has been Updated!';
    //        Restangular.one('budgetyears', $routeParams.id).get().then(function (stakeholdertype) {
    //            $scope.original = stakeholdertype;
    //            $scope.budgetyear = Restangular.copy($scope.original);
    //        });
    //    }
    //    else {
    //        $scope.message = 'budgetyear type has been Created!';
    //    }
    //    $scope.searchstakeholdertype = $scope.name;
    $scope.message = 'Budget Year has been Updated!';
    /***************************** INDEX *******************************************/
    $scope.zn = Restangular.all('spmbudgetyears?filter[where][deleteflag]=false').getList().then(function (zn) {
        $scope.budgetyears = zn[0];
        $scope.original = zn[0];
        $scope.budgetyear = Restangular.copy($scope.original);
        console.log('$scope.budgetyear', $scope.budgetyear);
    });
    $scope.budgetyear = {
        name: '',
        deleteflag: false
    };
    /************************************ SAVE *******************************************
    $scope.validatestring = '';
    $scope.Savestakeholdertype = function () {
        document.getElementById('name').style.border = "";
      
        if ($scope.budgetyear.name == '' || $scope.budgetyear.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter budgetyear name';
            document.getElementById('name').style.borderColor = "#FF0000";
        }
      
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            $scope.submitDisable = true;
            $scope.budgetyears.post($scope.budgetyear).then(function () {
                console.log('budgetyear Saved');
                window.location = '/budgetyear';
            });
        }
    };
    /********************************* UPDATE *******************************************/
    $scope.Update = function () {
        var arr2 = [];
        var str = $scope.budgetyear.name;
        var arr2 = str.split('-');
        var diff = parseFloat(arr2[1]) - parseInt(arr2[0]);

        if (arr2.length > 2) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.budgetyear = Restangular.copy($scope.original);
            return;
        }
        if ((isNaN(arr2[0])) || (isNaN(arr2[1]))) {
            $scope.AlertMessage = 'Missing or wrong format';
            $scope.openOneAlert();
            $scope.budgetyear = Restangular.copy($scope.original);
            return;
        }
        //console.log('diff',diff);
        if (parseFloat(arr2[0]) > parseInt(arr2[1])) {
            $scope.AlertMessage = 'Start year should be lesser than the year number';
            $scope.openOneAlert();
            //console.log('Condition3');
            $scope.budgetyear = Restangular.copy($scope.original);
            return;
        }

        if (diff > 1) {
            $scope.AlertMessage = 'Differnce between Start year and End year should be one year';
            $scope.openOneAlert();
            //console.log('Condition3');
            $scope.budgetyear = Restangular.copy($scope.original);
            return;
        }

        if (arr2[0].length == 0 || arr2[1].length == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.budgetyear = Restangular.copy($scope.original);
            return;
        }
        if (arr2[0] == 0 || arr2[1] == 0) {
            $scope.AlertMessage = 'Wrong format';
            $scope.openOneAlert();
            $scope.budgetyear = Restangular.copy($scope.original);
            return;
        }
        Restangular.one('spmbudgetyears', $scope.budgetyear.id).customPUT($scope.budgetyear).then(function () {
            $scope.AlertMessage = 'Budget year has been Updated!';
            $scope.openOneAlert2();
            setTimeout(function(){
                window.location = '/';
            },1000);
        });

    }

    $scope.openOneAlert = function () {
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-danger'
        });
        
    };
    $scope.openOneAlert2 = function () {
        $scope.modalOneAlert = $modal.open({
            animation: true,
            templateUrl: 'template/AlertModal2.html',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            windowClass: 'modal-sucess'
        });
        
    };
    $scope.okAlert = function () {
        $scope.modalOneAlert.close();
    };

});
