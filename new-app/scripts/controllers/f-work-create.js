'use strict';
angular.module('secondarySalesApp').controller('FWCreateCtrl', function ($scope, Restangular, $window, $filter, $modal, AnalyticsRestangular) {



    $scope.isCreateView = true;
    $scope.heading = 'Field Worker Create';
    $scope.HideUsername = false;
    $scope.UpdateClicked = false;
    $scope.hideSitesAssign = true;

    //new changes
    Restangular.one('orwlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.orwlanguages = langResponse[0];

        //$scope.modalInstanceLoad.close();
        $scope.TIHeading = $scope.orwlanguages.fieldworkercreate;
        // $scope.modalTitle = $scope.RILanguage.thankYou;
        $scope.message = $scope.orwlanguages.hasbeencreated;

    });
    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
        $scope.languages = lang;
        $scope.fieldworker.language = 1;
          $scope.user.language =  $scope.fieldworker.language;
    });

    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle = 'Thank You';
    } else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle = 'धन्यवाद';
    } else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle = 'ಧನ್ಯವಾದ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle = 'நன்றி';
    }

    $scope.getLanguage = function (languageId) {
        return Restangular.one('languages', languageId).get().$object;
    };

    $scope.toggleLoading = function () {
        $scope.modalInstanceLoad = $modal.open({
            animation: true,
            templateUrl: 'template/LodingModal.html',
            scope: $scope,
            backdrop: 'static',
            size: 'sm'
        });
    };

    $scope.fieldworker = {
        address: '',
        lastmodifiedtime: new Date(),
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        facility: $window.sessionStorage.coorgId,
        deleteflag: false,
        profileFlag: 'FW'
    };
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.userId,
        lastmodifiedtime: new Date(),
        entityroleid: 51,
        countryId: $window.sessionStorage.countryId,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        town: $window.sessionStorage.distributionAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.userId,
        description: 'ORW Created'
    };
    $scope.user = {
        flag: true,
        lastmodifiedtime: new Date(),
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        deleteflag: false,
        status: 'active',
        countryId: $window.sessionStorage.countryId,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        town: $window.sessionStorage.distributionAreaId,
        tiId: $window.sessionStorage.coorgId
    };

    Restangular.one('zones', $window.sessionStorage.zoneId).get().then(function (zone) {
        $scope.zoneName = zone.name;
        $scope.fieldworker.state = zone.id;
    });
    
     Restangular.all('roles?filter[where][deleteflag]=false' + '&filter[where][rolefalg]=NA').getList().then(function (role) {
         
         $scope.roleId = role[0].id;
         $scope.fieldworker.roleId = role[0].id;
           console.log($scope.roleId);
     });
    Restangular.one('sales-areas', $window.sessionStorage.salesAreaId).get().then(function (salesarea) {
        $scope.salesareaName = salesarea.name;
        $scope.fieldworker.district = salesarea.id;
        $scope.fieldworker.profileFlag = 'FW';
    });
    //$scope.getSalescode = emp.salesCode;
    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
        console.log('comember', comember);
        if(comember.fwcount == '' || comember.fwcount == null){
            $scope.comemberlength = 0;
        }
        else{
            $scope.comemberlength = comember.fwcount;
        }
        
        $scope.coorgName = comember.name;
        $scope.fieldworker.facilityId = comember.id;
        $scope.fieldworker.countryId = comember.countryId;
        $scope.fieldworker.town = comember.town;
        $scope.auditlog.facilityId = comember.id;
       // Restangular.one('fieldworkers?filter[where][facility]=' + $window.sessionStorage.coorgId).get().then(function (fldwrkrs) {
            Restangular.one('employees', $window.sessionStorage.coorgId).get().then(function (facility) {

                $scope.fieldworker.firstname = 'fw' + facility.salesCode.toLowerCase() + ($scope.comemberlength + 1);
                $scope.fieldworker.fwcode = ($scope.comemberlength + 1);

                $scope.modalInstanceLoad.close();
            });
       // });
    });
    /************************************ SAVE *******************************************/
    $scope.validatestring = '';
    $scope.UpdateComember = {
        fwcount: 0
    };
    $scope.createDisabled = false;
    $scope.dataModal = false;
    $scope.Save = function () {

        var regEmail = /\S+@\S+\.\S+/;

        document.getElementById('firstname').style.border = "";
        document.getElementById('lastname').style.border = "";
        document.getElementById('mobile').style.border = "";
        document.getElementById('email').style.border = "";

        if ($scope.fieldworker.firstname == '' || $scope.fieldworker.firstname == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterfullname;
            document.getElementById('firstname').style.border = "1px solid #ff0000";
        } else if ($scope.fieldworker.lastname == '' || $scope.fieldworker.lastname == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterfullname;
            document.getElementById('lastname').style.border = "1px solid #ff0000";
        } else if ($scope.fieldworker.email == '' || $scope.fieldworker.email == null) {
            $scope.validatestring = $scope.validatestring + $scope.enteremail;
        } else if (!regEmail.test($scope.fieldworker.email)) {
            $scope.fieldworker.email = '';
            $scope.validatestring = $scope.validatestring + 'Please Enter Your valid Email Id';
            document.getElementById('email').style.border = "1px solid #ff0000";
        } else if ($scope.fieldworker.mobile == '' || $scope.fieldworker.mobile == null) {
            $scope.validatestring = $scope.validatestring + $scope.entermobile;
        } else if ($scope.fieldworker.mobile.length != 10) {
            $scope.validatestring = $scope.validatestring + 'Please Enter valid Mobile Number';
            document.getElementById('mobile').style.borderColor = "#FF0000";
        } else if ($scope.user.password == '' || $scope.user.password == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterpassword;
        } else if ($scope.fieldworker.language == '' || $scope.fieldworker.language == null) {
            $scope.validatestring = $scope.validatestring + $scope.sellanguage;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.UpdateClicked = true;
            $scope.createDisabled = true;
            Restangular.one('languages', $scope.fieldworker.language).get().then(function (lang) {
                $scope.languageName = lang.name;
                $scope.dataModal = !$scope.dataModal;
            });


        }
    };
    $scope.disaveSaved = false;
    $scope.OK = function () {
        $scope.disaveSaved = true;
        $scope.fieldworker.roleId = $scope.roleId;
        Restangular.all('comembers').post($scope.fieldworker).then(function (submitfw) {
            $scope.auditlog.entityid = submitfw.id;
            $scope.user.username = submitfw.firstname;
            $scope.user.mobile = submitfw.mobile;
            $scope.user.employeeid = submitfw.id;
            $scope.user.email = submitfw.email;
            $scope.user.roleId = submitfw.roleId;
            $scope.user.profileId = submitfw.id;

            Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                console.log('responseaudit', responseaudit);
                Restangular.all('users').post($scope.user).then(function (responseuser) {
                    console.log('responseuser', responseuser);
                    $scope.user.id = responseuser.id;
                    $scope.user.roleid = responseuser.roleId;
                    $scope.user.coorg_id = responseuser.coorgId;
                    $scope.user.employee_id = responseuser.employeeid;
                    $scope.user.zone_id = responseuser.state;
                    $scope.user.salesarea_id = responseuser.salesAreaId;
                    // AnalyticsRestangular.all('users').post($scope.user).then(function (analyticsuser) {
                    $scope.UpdateComember.fwcount = +($scope.fieldworker.fwcode) + 1;
                    Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).customPUT($scope.UpdateComember).then(function (comember) {
                        //window.location = '/fieldworkers';

                        $scope.dataModal = !$scope.dataModal;

                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        console.log('Medicine Name Saved');
                        setTimeout(function () {
                            window.location = '/fieldworkers';
                        }, 350);

                    });
                    // });

                }, function (error) {
                    console.log('user error', error);
                });
            });
        }, function (error) {
            $scope.UpdateClicked = false;
            console.log('fw error', error);
            if (error.data.error.constraint == 'unique_firstname') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.usernameexits; //"Username already exits please refresh the page and try again";
            } else if (error.data.error.constraint == 'unique_email') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.emailexit; //"email id already exits";
            }
        });




    };
    $scope.cancelSave = function () {
        $scope.createDisabled = false;
        $scope.disaveSaved = false;
        $scope.dataModal = !$scope.dataModal;
    };
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };

    /********************************************* Map ******************************/
    /************************** Map *************************/

    $scope.mapdataModal = false;

    $scope.LocateMe = function () {
        $scope.mapdataModal = true;

        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 4,

            center: new google.maps.LatLng(12.9538477, 77.3507369),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        });
        var marker, i;

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(12.9538477, 77.3507369),
            map: map,
            html: ''
        });

        $scope.toggleMapModal();
    };

    $scope.toggleMapModal = function () {
        $scope.mapcount = 0;

        ///////////////////////////////////////////////////////MAP//////////////////////////

        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {
            //  console.log(latLng);
            $scope.fieldworker.latitude = latLng.lat();
            $scope.fieldworker.longitude = latLng.lng();

            //  console.log('$scope.updatepromotion', $scope.updatepromotion);

            document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
        }

        function updateMarkerAddress(str) {
            document.getElementById('mapaddress').innerHTML = str;
        }
        var map;

        function initialize() {

            $scope.latitude = 12.9538477;
            $scope.longitude = 77.3507369;
            navigator.geolocation.getCurrentPosition(function (location) {
                //                    console.log(location.coords.latitude);
                //                    console.log(location.coords.longitude);
                //                    console.log(location.coords.accuracy);
                $scope.latitude = location.coords.latitude;
                $scope.longitude = location.coords.longitude;
                //                });

                // console.log('$scope.address', $scope.address);

                var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                map = new google.maps.Map(document.getElementById('mapCanvas'), {
                    zoom: 4,
                    center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                });
                var marker = new google.maps.Marker({
                    position: latLng,
                    title: 'Point A',
                    map: map,
                    draggable: true
                });

                // Update current position info.
                updateMarkerPosition(latLng);
                geocodePosition(latLng);

                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function () {
                    updateMarkerAddress('Dragging...');
                });

                google.maps.event.addListener(marker, 'drag', function () {
                    updateMarkerStatus('Dragging...');
                    updateMarkerPosition(marker.getPosition());
                });

                google.maps.event.addListener(marker, 'dragend', function () {
                    updateMarkerStatus('Drag ended');
                    geocodePosition(marker.getPosition());
                });
            });


        }

        // Onload handler to fire off the app.
        //google.maps.event.addDomListener(window, 'load', initialize);
        initialize();

        window.setTimeout(function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
            map.setZoom(10);
        }, 1000);


        $scope.SaveMap = function () {
            $scope.showMapModal = !$scope.showMapModal;
            //  console.log($scope.reportincident);
        };

        //console.log('fdfd');
        $scope.showMapModal = !$scope.showMapModal;
    };

    /****************************** Language *********************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        console.log('langResponse', langResponse);

        $scope.fieldworkercreate = langResponse.fieldworkercreate;
        $scope.divbasicinfo = langResponse.divbasicinfo;
        $scope.divuserinfo = langResponse.divuserinfo;
        $scope.printfirstname = langResponse.firstname;
        $scope.printlastname = langResponse.lastname;
        $scope.printfwcode = langResponse.fwcode;
        $scope.printemail = langResponse.email;
        $scope.state = langResponse.state;
        $scope.district = langResponse.district;
        $scope.comanagerhdf = langResponse.comanagerhdf;
        $scope.fullname = langResponse.fullname;
        $scope.printmobile = langResponse.mobile;
        $scope.printaddress = langResponse.address;
        $scope.latlong = langResponse.latlong;
        $scope.username = langResponse.username;
        $scope.password = langResponse.password;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.ok = langResponse.ok;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        //$scope.modalTitle = langResponse.createdetails;

        $scope.enterfullname = langResponse.enterfullname;
        $scope.entermobile = langResponse.entermobile;
        $scope.enteremail = langResponse.enteremail;
        $scope.enterpassword = langResponse.enterpassword;
        $scope.sellanguage = langResponse.sellanguage;
        $scope.usernameexits = langResponse.usernameexits;
        $scope.emailexit = langResponse.emailexit;

    });
    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle12 = 'Created With the Following Details';
        $scope.LanguagePrint = 'Language';
    } else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle12 = 'निम्न विवरण के साथ बनाया';
        $scope.LanguagePrint = 'भाषा';
    } else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle12 = 'ಕೆಳಗಿನ ವಿವರಗಳಿಂದ  ರಚಿಸಲಾಗಿದೆा';
        $scope.LanguagePrint = 'ಭಾಷೆ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle12 = 'பின்வரும் விவரங்கள் உடன் உருவாக்கப்பட்டதுा';
        $scope.LanguagePrint = 'மொழி';
    } else if ($window.sessionStorage.language == 5) {
        $scope.modalTitle12 = 'క్రింది వివరాలను తో రూపొందించబడింది';
        $scope.LanguagePrint = 'భాష';
    } else if ($window.sessionStorage.language == 6) {
        $scope.modalTitle12 = 'ह्या माहिती प्रमाणे करनायत आलेळे आहे';
        $scope.LanguagePrint = 'भाषा';
    }
});
