'use strict';

angular.module('secondarySalesApp')
    .controller('CitiesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route, $filter, $timeout) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

    $scope.someFocusVariable = true;
    
        $scope.showForm = function () {
            var visible = $location.path() === '/town/create' || $location.path() === '/town/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/town/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/town/create' || $location.path() === '/town/' + $routeParams.id;
            return visible;
        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/town/create' || $location.path() === '/town/' + $routeParams.id;
            return visible;
        };
        /************************************************************************************/
        if ($window.sessionStorage.facility_zoneId == null || $window.sessionStorage.facility_zoneId == undefined || $window.sessionStorage.facility_stateId == null || $window.sessionStorage.facility_stateId == undefined) {
            $window.sessionStorage.facility_zoneId = null;
            $window.sessionStorage.facility_stateId = null;
            $window.sessionStorage.facility_currentPage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.facility_zoneId;
            $scope.stateId = $window.sessionStorage.facility_stateId;
            $scope.currentpage = $window.sessionStorage.facility_currentPage;
            $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        }


        if ($window.sessionStorage.prviousLocation != "partials/cities") {
            $window.sessionStorage.facility_zoneId = '';
            $window.sessionStorage.facility_stateId = '';
            $window.sessionStorage.facility_currentPage = 1;
            $scope.currentpage = 1;
            $window.sessionStorage.facility_currentPageSize = 25;
            $scope.pageSize = 25;
        }

        $scope.pageSize = $window.sessionStorage.facility_currentPageSize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.facility_currentPageSize = mypage;
        };

        $scope.currentpage = $window.sessionStorage.facility_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.facility_currentPage = newPage;
        };

        /*************************Index***********************/
    
    /*$scope.filterFields = ['name','townCode','dstrictName','stateName','CountryName'];
        $scope.searchCity = '';*/
        $scope.DisplayCities = [];
    
    //for search by fist letter with first column
    /*Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (ctyRes2) {
            $scope.DisplayCities = ctyRes2;
       //console.log("$scope.DisplayCities", $scope.DisplayCities);
           $scope.TotalDisplayCities = $scope.DisplayCities.sort();
      
      //console.log("$scope.TotalDisplayCities", $scope.TotalDisplayCities);
    });*/
    
    $scope.search = '';
       $scope.startsWith = function (actual, expected) {
           // console.log("inside stratswith");
        var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
                }

        $scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;

        $scope.displayzones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
        $scope.displaysalesareas = Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().$object;

        Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
            $scope.DisplayCities = ctyRes1;

            Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                $scope.countrieDis = con;
                Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zn) {
                    $scope.zoneDisplay = zn;
                    Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (salarea) {
                        $scope.salesareaDispaly = salarea;
                        angular.forEach($scope.DisplayCities, function (member, index) {

                            for (var a = 0; a < $scope.countrieDis.length; a++) {
                                if (member.countryId == $scope.countrieDis[a].id) {
                                    member.CountryName = $scope.countrieDis[a].name;
                                    break;
                                }
                            }

                            for (var b = 0; b < $scope.zoneDisplay.length; b++) {
                                if (member.state == $scope.zoneDisplay[b].id) {
                                    member.stateName = $scope.zoneDisplay[b].name;
                                    break;
                                }
                            }

                            for (var c = 0; c < $scope.salesareaDispaly.length; c++) {
                                if (member.district == $scope.salesareaDispaly[c].id) {
                                    member.dstrictName = $scope.salesareaDispaly[c].name;
                                    break;
                                }
                            }

                            member.index = index + 1;

                        });
                    });
                });
            });
        });
        /************************************/
 var timeoutPromise;
        var delayInMs = 1200;

        $scope.orderChange = function (code) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {
                console.log(code);

                 var data =  $scope.DisplayCities.filter(function (arr) {
                    return arr.townCode == code
                })[0];
                 if (data != undefined) {
                    $scope.city.townCode = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Town code Already Exist';
                 }
            }, delayInMs);
        };
    
    $scope.CheckDuplicate = function (name) {
           // console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.DisplayCities.filter(function (arr) {
                    return arr.name == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.cityname = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Town Name Already Exist';
                 }
            }, delayInMs);
        };
        /************************************************/
        $("#code").keydown(function (e) {
            var k = e.keyCode || e.which;
            var ok = k >= 65 && k <= 90 || // A-Z
                k >= 96 && k <= 105 || // a-z
                k >= 35 && k <= 40 || // arrows
                k == 9 || //tab
                k == 46 || //del
                k == 8 || // backspaces
                (!e.shiftKey && k >= 48 && k <= 57); // only 0-9 (ignore SHIFT options)

            if (!ok || (e.ctrlKey && e.altKey)) {
                e.preventDefault();
            }
        });

        $scope.countryId = '';
        $scope.stateId = '';
        $scope.zoneId = '';
        $scope.statesid = '';
        $scope.countiesid = '';


        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.stateId = '';
                $scope.zoneId = '';
                $window.sessionStorage.facility_zoneId = newValue;
                Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceSt) {
                    $scope.displayzones = responceSt;
                    // $scope.stateId = $window.sessionStorage.facility_stateId;
                });

                $scope.cities1 = Restangular.all('cities?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
                    $scope.DisplayCities = ctyRes1;
                    Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.countrieDis = con;
                        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zn) {
                            $scope.zoneDisplay = zn;
                            Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (salarea) {
                                $scope.salesareaDispaly = salarea;
                                angular.forEach($scope.DisplayCities, function (member, index) {

                                    for (var a = 0; a < $scope.countrieDis.length; a++) {
                                        if (member.countryId == $scope.countrieDis[a].id) {
                                            member.CountryName = $scope.countrieDis[a].name;
                                            break;
                                        }
                                    }

                                    for (var b = 0; b < $scope.zoneDisplay.length; b++) {
                                        if (member.state == $scope.zoneDisplay[b].id) {
                                            member.stateName = $scope.zoneDisplay[b].name;
                                            break;
                                        }
                                    }


                                    for (var c = 0; c < $scope.salesareaDispaly.length; c++) {
                                        if (member.district == $scope.salesareaDispaly[c].id) {
                                            member.dstrictName = $scope.salesareaDispaly[c].name;
                                            break;
                                        }
                                    }
                                    member.index = index + 1;
                                });
                            });
                        });
                    });
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('zoneId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_zoneId = newValue;
                Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceSt) {
                    $scope.displaysalesareas = responceSt;

                });

                $scope.cities2 = Restangular.all('cities?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes) {
                    $scope.DisplayCities = ctyRes;
                    Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.countrieDis = con;
                        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zn) {
                            $scope.zoneDisplay = zn;
                            Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (salarea) {
                                $scope.salesareaDispaly = salarea;
                                angular.forEach($scope.DisplayCities, function (member, index) {

                                    for (var a = 0; a < $scope.countrieDis.length; a++) {
                                        if (member.countryId == $scope.countrieDis[a].id) {
                                            member.CountryName = $scope.countrieDis[a].name;
                                            break;
                                        }
                                    }

                                    for (var b = 0; b < $scope.zoneDisplay.length; b++) {
                                        if (member.state == $scope.zoneDisplay[b].id) {
                                            member.stateName = $scope.zoneDisplay[b].name;
                                            break;
                                        }
                                    }


                                    for (var c = 0; c < $scope.salesareaDispaly.length; c++) {
                                        if (member.district == $scope.salesareaDispaly[c].id) {
                                            member.dstrictName = $scope.salesareaDispaly[c].name;
                                            break;
                                        }
                                    }
                                    member.index = index + 1;
                                });
                            });
                        });
                    });
                });

                $scope.statesid = +newValue;
            }
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.facility_zoneId = newValue;


                $scope.cities2 = Restangular.all('cities?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.statesid + +'&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes) {
                    $scope.DisplayCities = ctyRes;
                    Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.countrieDis = con;
                        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zn) {
                            $scope.zoneDisplay = zn;
                            Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (salarea) {
                                $scope.salesareaDispaly = salarea;
                                angular.forEach($scope.DisplayCities, function (member, index) {

                                    for (var a = 0; a < $scope.countrieDis.length; a++) {
                                        if (member.countryId == $scope.countrieDis[a].id) {
                                            member.CountryName = $scope.countrieDis[a].name;
                                            break;
                                        }
                                    }

                                    for (var b = 0; b < $scope.zoneDisplay.length; b++) {
                                        if (member.state == $scope.zoneDisplay[b].id) {
                                            member.stateName = $scope.zoneDisplay[b].name;
                                            break;
                                        }
                                    }


                                    for (var c = 0; c < $scope.salesareaDispaly.length; c++) {
                                        if (member.district == $scope.salesareaDispaly[c].id) {
                                            member.dstrictName = $scope.salesareaDispaly[c].name;
                                            break;
                                        }
                                    }
                                    member.index = index + 1;
                                });
                            });
                        });
                    });
                });
            }
        });
    
    /****************auto capital*************/
        $.fn.capitalize = function () {
            $.each(this, function () {
                this.value = this.value.replace(/\b[a-z]/gi, function ($0) {
                    return $0.toUpperCase();
                });
                this.value = this.value.replace(/@([a-z])([^.]*\.[a-z])/gi, function ($0, $1) {
                    console.info(arguments);
                    return '@' + $0.toUpperCase() + $1.toLowerCase();
                });

            });
        }

        //usage
        $("input").keyup(function () {
            $(this).capitalize();
        }).capitalize();


        /**********************************************************************************/
        $scope.statedisable = false;
        $scope.districdisable = false;
        //$scope.searchCity = $scope.name;




        /*************************************** DELETE *******************************/

        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('cities/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }


        /********************************************** WATCH ***************************************/


        $scope.$watch('city.countryId', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.city.state = '';
                $scope.city.district = '';
                
                $scope.zones = Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
        });

        $scope.$watch('city.state', function (newValue, oldValue) {
            //  console.log('newValue', newValue);
            //   console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.city.district = '';
                
                $scope.salesareas = Restangular.all('sales-areas?filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
        });


        /****************************************** CREATE *********************************/

        $scope.city = {
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            
            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
           // var regEmail = /^(?=.[a-z])(?=.*[a-z]).*$/;

            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";
            if ($scope.cityname == '' || $scope.cityname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } 
             else if (!regEmail.test($scope.cityname)) {
                $scope.cityname = '';
                 $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                document.getElementById('name').style.border = "1px solid #ff0000";
            }
            
            else if ($scope.city.townCode == '' || $scope.city.townCode == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Town Code';
                document.getElementById('code').style.borderColor = "#FF0000";
            } 
            else if ($scope.city.townCode == '' || $scope.city.townCode.length != 3) {
                $scope.validatestring = $scope.validatestring + 'Please Enter 3-Digit Town Code';
                document.getElementById('code').style.borderColor = "#FF0000";
            } 
            
            else if ($scope.city.countryId == '' || $scope.city.countryId == null) {
                $scope.city.countryId == '';
                $scope.validatestring = $scope.validatestring + 'Please select Country';
            } else if ($scope.city.state == '' || $scope.city.state == null) {
                $scope.city.state == '';
                $scope.validatestring = $scope.validatestring + 'Please Select State';
            } else if ($scope.city.district == '' || $scope.city.district == null) {
                $scope.city.district == '';
                $scope.validatestring = $scope.validatestring + 'Please District District';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                 var xyz = $scope.cityname;
                $scope.city.name = xyz.toUpperCase();
                
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.all('cities').post($scope.city).then(function () {
                    window.location = '/town';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };




        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /******************************************************** UPDATE ************************/
        $scope.modalTitle = 'Thank You';
        if ($routeParams.id) {
            $scope.statedisable = true;
            $scope.districdisable = true;
            $scope.message = 'Town has been Updated!';
            Restangular.one('cities', $routeParams.id).get().then(function (city) {
                $scope.original = city;

                Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (Resp) {
                    $scope.zones = Resp;
                    $scope.state = $scope.state;
                    Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (Respsales) {
                        $scope.salesareas = Respsales;

                        $scope.district = $scope.district;
                        $scope.city = Restangular.copy($scope.original);
                        $scope.cityname = $scope.city.name;
                    });
                });
            });
        } else {
            $scope.message = 'Town has been Created!';
        }


        /*
        		$scope.validatestring = '';
        		$scope.Update = function () {
        			delete $scope.city.district;
        			delete $scope.city.facility;
        			delete $scope.city.site;
        			delete $scope.city.state;
        			$scope.submitcities.customPUT($scope.city).then(function (updateRes) {
        				console.log('$scope.city', updateRes);
        				window.location = '/cities';
        			}, function (error) {
        				console.log('error', error);
        			});
        		};*/
        $scope.Update = function () {
            
             var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
            document.getElementById('name').style.border = "";
            
           if ($scope.cityname == '' || $scope.cityname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } 
             else if (!regEmail.test($scope.cityname)) {
                $scope.cityname = '';
                 $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                document.getElementById('name').style.border = "1px solid #ff0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                 var xyz = $scope.cityname;
                $scope.city.name = xyz.toUpperCase();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.submitDisable = true;
                delete $scope.city.district;
                delete $scope.city.facility;
                delete $scope.city.site;
                delete $scope.city.state;
                Restangular.one('cities', $routeParams.id).customPUT($scope.city).then(function () {
                    //$location.path('/cities');
                    window.location = '/town';
                });
            }
        };



    })
.filter('startsWith', function() {
        return function(array, search) {
            var matches = [];
            for(var i = 0; i < array.length; i++) {
                if (array[i].indexOf(search) === 0 &&
                    search.length < array[i].length) {
                    matches.push(array[i]);
                }
            }
            return matches;
        };
    });

    