'use strict';

angular.module('secondarySalesApp')
    .controller('ReasonForCancellingCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/reasonforcancelling/create' || $location.path() === '/reasonforcancelling/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/reasonforcancelling/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/reasonforcancelling/create' || $location.path() === '/reasonforcancelling/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/reasonforcancelling/create' || $location.path() === '/reasonforcancelling/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
       

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/reasonforcancelling-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }


        $scope.showenglishLang = true;
        //$scope.OtherLang = true;

        $scope.$watch('reason.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {

                    $scope.reason.orderno = '';
                    $scope.reason.name = '';
                    $scope.reason.parentId = '';
                    $scope.reason.statusId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;

                }


            }
        });



        $scope.$watch('reason.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('reasonforcancellations', newValue).get().then(function (zn) {
                    $scope.reason.orderno = zn.orderno;
                    $scope.reason.statusId = zn.statusId;
                });
            }
        });

        /**************************************************************************************/
        if ($routeParams.id) {
            $scope.message = 'Reason has been Updated!';
            Restangular.one('reasonforcancellations', $routeParams.id).get().then(function (reason) {
                console.log(reason);
                $scope.original = reason;
                $scope.reason = Restangular.copy($scope.original);
                $scope.condiionoldvalue = $scope.reason.orderno;
            });
            Restangular.all('reasonforcancellations?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;

            });
        } else {
            $scope.message = 'reason has been Created!';
        }

        //$scope.Search = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;


        /******************************** INDEX *******************************************/

        Restangular.all('conditionstatuses?filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rfch) {
            $scope.conditionstatuses = rfch;
        });

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('reasonforcancellations?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.reasonforcancellations = mt;
                  Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.reasonforcancellations, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.reasonlanguages = sev;
            });

        } else {

            Restangular.all('reasonforcancellations').getList().then(function (mt) {
                $scope.reasonforcancellations = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.reasonforcancellations, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('reasonforcancellations?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.reasonlanguages = sev;

                        angular.forEach($scope.reasonlanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.reasonlanguages = sev;
                    });
                }
            });


        }


        Restangular.all('reasonforcancellations?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.reasonfollowdisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.reasonfollowdisply.filter(function (arr) {
                    return arr.orderno == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.reason.language != '') {
                    $scope.reason.orderno = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };




        /********************************************* SAVE *******************************************/
        $scope.reason = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.border = "";
            document.getElementById('status').style.border = "";

            if ($scope.reason.language == '' || $scope.reason.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.reason.language == 1) {
                if ($scope.reason.name == '' || $scope.reason.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Reason for Cancelling';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.reason.orderno == '' || $scope.reason.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.reason.statusId == '' || $scope.reason.statusId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Status';
                    document.getElementById('status').style.borderColor = "#FF0000";

                }
            } else if ($scope.reason.language != 1) {
                if ($scope.reason.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Reason for Cancelling in English';

                } else if ($scope.reason.name == '' || $scope.reason.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Reason for Cancelling';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.reason.orderno == '' || $scope.reason.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.reason.statusId == '' || $scope.reason.statusId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Status';
                    document.getElementById('status').style.borderColor = "#FF0000";

                }
            }



            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.reason.parentId === '') {
                    delete $scope.reason['parentId'];
                }
                $scope.submitDisable = true;
                $scope.reason.parentFlag = $scope.showenglishLang;
                $scope.reasonforcancellations.post($scope.reason).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/reasonforcancelling-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'reason_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                })


            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            if ($scope.reason.language == '' || $scope.reason.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.reason.name == '' || $scope.reason.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Reason for Cancelling';
                document.getElementById('name').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.reason.parentId === '') {
                    delete $scope.reason['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('reasonforcancellations', $routeParams.id).customPUT($scope.reason).then(function (updateResp) {
                   // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('reasonforcancellation  Saved');
                    $scope.MandatoryUpdate(updateResp);
                }, function (error) {
                    if (error.data.error.constraint === 'reason_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }

        };



        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderno = myResponse.orderno;
                $scope.updateallRecord.statusId = myResponse.statusId;

                Restangular.one('reasonforcancellations', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                    window.location = '/reasonforcancelling-list';
                }, 350);
            }
        };







        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('reasonforcancellations/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('reasonforcancellations/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


    });
