'use strict';

angular.module('secondarySalesApp')
    .controller('GroupsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $window, $route) {
        /*********/

        $scope.showForm = function () {
            var visible = $location.path() === '/groups/create' || $location.path() === '/groups/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/groups/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() ==='/groups/create' || $location.path() === '/groups/' + $routeParams.id;
            return visible;
        };

    
     $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/groups/create' || $location.path() === '/groups/' + $routeParams.id;
            return visible;
        };

        /*********/

     $scope.searchGroup = $scope.name;

      $scope.$watch('group.name', function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                } else {
                 var RegExpression = /^[\a-zA-Z\s]*$/;

                    if (RegExpression.test(newValue)) {

                    } else {
                       $scope.group.name = oldValue;
                    }
                    
                }
            });
      
/*************************************************************** INDEX *******************************************/ 
       
     $scope.grp = Restangular.all('groups').getList().then(function (grp) {
        $scope.groups = grp;
        angular.forEach($scope.groups, function (member, index) {
             member.index = index + 1;
        });
    });
    
    $scope.group = {};
    /*************************************************************** SAVE *******************************************/ 
   
        $scope.validatestring = '';
        $scope.Save = function () {
                document.getElementById('name').style.border = "";
               
            
          if ($scope.group.name == '' || $scope.group.name == null) {
                $scope.group.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your group name..';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.groups.post($scope.group).then(function () {
                        console.log('$scope.group', $scope.group);
                        window.location = '/groups';
                    });
                }


        };
  /*************************************************************** UPDATE *******************************************/ 
     
    $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
               
            
          if ($scope.group.name == '' || $scope.group.name == null) {
                $scope.group.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your group name';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.groups.customPUT($scope.group).then(function () {
                        console.log('$scope.group', $scope.group);
                        window.location = '/groups';
                    });
                }


        };
    
   /*************************************************************** DELETE *******************************************/ 
     $scope.updateGroupFlag = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
           Restangular.one('groups/' + id).remove($scope.group).then(function () {
                $route.reload();
            });

        } else {

        }

    }

   

        if ($routeParams.id) {
            Restangular.one('groups', $routeParams.id).get().then(function (group) {
                $scope.original = group;
                $scope.group = Restangular.copy($scope.original);
            });
        }

    });