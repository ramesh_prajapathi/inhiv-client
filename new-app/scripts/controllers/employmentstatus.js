'use strict';

angular.module('secondarySalesApp')
  .controller('EmpstCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams,$timeout,baseUrl, $route, $window) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/employmentstatuses/create' || $location.path() === '/employmentstatuses/' + $routeParams.id;
      return visible;
    };
    
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/employmentstatuses/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/employmentstatuses/create'|| $location.path() === '/employmentstatuses/' + $routeParams.id;
        return visible;
      };

    
    $scope.hideSearchFilter = function (){
        var visible = $location.path() === '/employmentstatuses/create'|| $location.path() === '/employmentstatuses/' + $routeParams.id;
        return visible;
      };

    
    /*********/

  //  $scope.employmentstatuses = Restangular.all('employmentstatuses').getList().$object;
    
    if($routeParams.id){
      Restangular.one('employmentstatuses', $routeParams.id).get().then(function(employmentstatus){
				$scope.original = employmentstatus;
				$scope.employmentstatus = Restangular.copy($scope.original);
      });
    }
    $scope.Search = $scope.name;
    
/************************************************************************** INDEX *******************************************/
       $scope.zn = Restangular.all('employmentstatuses').getList().then(function (zn) {
        $scope.employmentstatuses = zn;
        angular.forEach($scope.employmentstatuses, function (member, index) {
            member.index = index + 1;
        });
    });
    
/*************************************************************************** SAVE *******************************************/
     $scope.validatestring = '';
        $scope.Save = function () {       
                    $scope.employmentstatuses.post($scope.employmentstatus).then(function () {
                        console.log('employmentstatuses Saved');
                        window.location = '/employmentstatuses';
                    });
        };
/*************************************************************************** UPDATE *******************************************/ 
     $scope.validatestring = '';
        $scope.Update = function () {
                document.getElementById('name').style.border = "";
          if ($scope.employmentstatus.name == '' || $scope.employmentstatus.name == null) {
                $scope.employmentstatus.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your employment status Type';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.employmentstatuses.customPUT($scope.employmentstatus).then(function () {
                        console.log('employmentstatus Saved');
                        window.location = '/employmentstatuses';
                    });
                }


        };
/*************************************************************************** DELETE *******************************************/ 
   $scope.Delete = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('employmentstatuses/' + id).remove($scope.employmentstatus).then(function () {
                $route.reload();
            });

        } else {

        }

    }
    
  });


