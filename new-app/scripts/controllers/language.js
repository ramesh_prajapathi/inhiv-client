'use strict';

angular.module('secondarySalesApp')
    .controller('languagectrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        //$scope.OtherLang = false;


        /*********/
         if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }
     
        

        $scope.showForm = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/language/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/language/create' || $location.path() === '/language/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/language-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }



        if ($routeParams.id) {
            // $scope.OtherLang = true;
            $scope.message = 'Language has been Updated!';
            Restangular.one('languages', $routeParams.id).get().then(function (lang) {
                $scope.original = lang;
                $scope.language = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Language has been Created!';
        }

        //$scope.Search = $scope.name;
        // new changes for seacrh box
    
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index', 'name'];
        $scope.Search = '';
    
        $scope.languages = Restangular.all('languages').getList().$object;
    
    if ($window.sessionStorage.roleId == 4) {
          $scope.hideAddButton = false;
        Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (cs) {
            //?filter[where][deleteflag]=false
            $scope.languagedisplys = cs;
            angular.forEach($scope.languagedisplys, function (member, index) {
                member.index = index + 1;
            });
        });
        
        
        } else {
            $scope.hideAddButton = true;
            Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (cs) {
            //?filter[where][deleteflag]=false
            $scope.languagedisplys = cs;
            angular.forEach($scope.languagedisplys, function (member, index) {
                member.index = index + 1;
            });
        });
        }

        



        /********************************************* SAVE *******************************************/
        $scope.language = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";


            if ($scope.language.name == '' || $scope.language.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Language';
                document.getElementById('name').style.borderColor = "#FF0000";

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.languages.post($scope.language).then(function () {
                    window.location = '/language-list';
                });


            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";


            if ($scope.language.name == '' || $scope.language.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Language';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {


                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                Restangular.one('languages', $routeParams.id).customPUT($scope.language).then(function () {
                    console.log('Language Saved');
                    window.location = '/language-list';


                });

            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('languages/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });

