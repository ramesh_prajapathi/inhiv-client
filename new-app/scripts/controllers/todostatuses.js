'use strict';

angular.module('secondarySalesApp')
    .controller('ToDoStatusesCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }
        $scope.showForm = function () {
            var visible = $location.path() === '/todostatus/create' || $location.path() === '/todostatus/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/todostatus/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/todostatus/create' || $location.path() === '/todostatus/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/todostatus/create' || $location.path() === '/todostatus/' + $routeParams.id;
            return visible;
        };

        /*********************************** Pagination *******************************************/

        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }


        if ($window.sessionStorage.prviousLocation != "partials/todostatuses") {
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        $scope.showenglishLang = true;

        $scope.$watch('todostatus.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {

                    $scope.todostatus.orderNo = '';
                    $scope.todostatus.name = '';
                    $scope.todostatus.parentId = '';
                    $scope.todostatus.followUpDays = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;

                }

            }
        });
        $scope.$watch('todostatus.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('todostatuses', newValue).get().then(function (zn) {
                    $scope.todostatus.orderNo = zn.orderNo;
                    $scope.todostatus.actionable = zn.actionable;
                    $scope.todostatus.type = zn.type;
                });
            }
        });





        /*********/
        //  $scope.todostatuses = Restangular.all('todostatuses').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Todo Status has been Updated!';
            Restangular.one('todostatuses', $routeParams.id).get().then(function (todostatus) {
                $scope.original = todostatus;
                $scope.todostatus = Restangular.copy($scope.original);
                 $scope.condiionoldvalue = $scope.todostatus.orderNo;
            });
            Restangular.all('todostatuses?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;

            });
        } else {
            $scope.message = 'Todo Status has been Created!';
        }
    
       /* $scope.searchToDoStatus = $scope.name;
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;*/
    
   
        //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name','actionable'];
        $scope.searchToDoType = '';
        
    
        /************************** INDEX *******************************************/

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('todostatuses?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                //filter[where][deleteflag]=false' + '&
                $scope.todostatuses = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.todostatuses, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.todostatuslanguages = sev;
            });

        } else {

            Restangular.all('todostatuses').getList().then(function (mt) {
                //?filter[where][deleteflag]=false
                $scope.todostatuses = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.todostatuses, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });
           
            
               Restangular.all('todostatuses?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.todostatuslanguages = sev;
           
                      angular.forEach($scope.todostatuslanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                          console.log('member', member);
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.todostatuslanguages = sev;
                       });
                  }
              });
        }


        Restangular.all('todostatuses?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.todostatusdisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };
    
     var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.todostatuses.filter(function (arr) {
                    return arr.orderNo == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.todostatus.language != '') {
                    $scope.todostatus.orderNo = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };



        /*********************************************************************************************************/
        $scope.todostatus = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };
        /******************************************** SAVE *******************************************/
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.SaveToDoStatus = function () {
            document.getElementById('name').style.border = "";


            if ($scope.todostatus.language == '' || $scope.todostatus.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.todostatus.language == 1) {

                if ($scope.todostatus.name == '' || $scope.todostatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Value';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.todostatus.orderNo == '' || $scope.todostatus.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.todostatus.actionable == '' || $scope.todostatus.actionable == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter actionable';
                    document.getElementById('act').style.borderColor = "#FF0000";

                }
            } else if ($scope.todostatus.language != 1) {
                if ($scope.todostatus.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select ToDo Status in English';

                } else if ($scope.todostatus.name == '' || $scope.todostatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Value';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.todostatus.orderNo == '' || $scope.todostatus.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.todostatus.actionable == '' || $scope.todostatus.actionable == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter actionable';
                    document.getElementById('act').style.borderColor = "#FF0000";

                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                $scope.submitDisable = true;
                //                $scope.todostatuses.post($scope.todostatus).then(function () {
                //                    console.log('ToDoStatus Saved');
                //                    window.location = '/todostatus';
                //                });


                if ($scope.todostatus.parentId === '') {
                    delete $scope.todostatus['parentId'];
                }
                $scope.submitDisable = true;
                $scope.todostatus.parentFlag = $scope.showenglishLang;
                $scope.todostatuses.post($scope.todostatus).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/todostatus';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'todostatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                })
            };
        };
        /*********************************** UPDATE *******************************************/
        $scope.UpdateToDoStatus = function () {
            document.getElementById('name').style.border = "";

            if ($scope.todostatus.name == '' || $scope.todostatus.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
            
                if ($scope.todostatus.parentId === '') {
                    delete $scope.todostatus['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('todostatuses', $routeParams.id).customPUT($scope.todostatus).then(function (updateResp) {
                   // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('todostatus  Saved');
                    $scope.MandatoryUpdate(updateResp);

                }, function (error) {
                    if (error.data.error.constraint === 'todostatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };




        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderNo = myResponse.orderNo;

                $scope.updateallRecord.actionable = myResponse.actionable;
                $scope.updateallRecord.type = myResponse.type;
                Restangular.one('todostatuses', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                    window.location = '/todostatus';
                }, 350);
            }
        };


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }



        /*************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('todostatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.getPillar = function (pillarId) {
            return Restangular.one('pillars', pillarId).get().$object;
        };
        /**************** Archive ****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('todostatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };


    });
