'use strict';

angular.module('secondarySalesApp')

    .controller('BnCtrl', function ($scope, Restangular, $route, $window, $filter, $modal) {

$window.sessionStorage.MemberFilter = '';

            Restangular.one('memberlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
                $scope.memberLang = langResponse[0];
                $scope.headingName = $scope.memberLang.houseHoldCreate;
               // $scope.modalTitle = $scope.memberLang.thankYou;
                 $scope.printcancel = $scope.memberLang.cancel;
            $scope.okbutton = $scope.memberLang.ok;
                $scope.message = $scope.memberLang.thankYouCreated;
            });
            /*********************************** Pagination *******************************************/

            if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
                $window.sessionStorage.myRoute = null;
                $window.sessionStorage.myRoute_currentPage = 1;
                $window.sessionStorage.myRoute_currentPagesize = 25;
            }

            if ($window.sessionStorage.prviousLocation != "partials/beneficiaries" || $window.sessionStorage.prviousLocation != "partials/beneficiaries-form") {
                $window.sessionStorage.myRoute_currentPage = 1;
                $window.sessionStorage.myRoute_currentPagesize = 25;
            }

            $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
            $scope.PageChanged = function (newPage, oldPage) {
                $scope.currentpage = newPage;
                $window.sessionStorage.myRoute_currentPage = newPage;
            };

            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.pageFunction = function (mypage) {
                $scope.pageSize = mypage;
                $window.sessionStorage.myRoute_currentPagesize = mypage;
            };
            /*************************************************************************************************/
            $scope.auditlog = {
                modifiedbyroleid: $window.sessionStorage.roleId,
                modifiedby: $window.sessionStorage.UserEmployeeId,
                lastmodifiedtime: new Date(),
                entityroleid: 49,
                state: $window.sessionStorage.zoneId,
                district: $window.sessionStorage.salesAreaId,
                facility: $window.sessionStorage.coorgId,
                lastmodifiedby: $window.sessionStorage.UserEmployeeId,

            };

            $scope.hidePrevData = true;
            $scope.towns = Restangular.all('cities?filter[where][deleteflag]=false').getList().$object;

            $scope.typologies = Restangular.all('typologies?filter[where][deleteflag]=false').getList().$object;

            $scope.sites = Restangular.all('distribution-routes?filter[where][deleteflag]=false').getList().$object;

            if ($window.sessionStorage.roleId == 2) {
                $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {

                    console.log('part1', part1);
                    $scope.beneficiaries = part1;
                    $scope.modalInstanceLoad.close();
                    angular.forEach($scope.beneficiaries, function (member, index) {
                        member.index = index + 1;

                        var data14 = $scope.towns.filter(function (arr) {
                            return arr.id == member.town
                        })[0];

                        if (data14 != undefined) {
                            member.townName = data14.name;
                        }
                        var data15 = $scope.typologies.filter(function (arr) {
                            return arr.id == member.typology
                        })[0];

                        if (data15 != undefined) {
                            member.typologyName = data15.name;
                        }

                        var data16 = $scope.sites.filter(function (arr) {
                            return arr.id == member.site
                        })[0];

                        if (data16 != undefined) {
                            member.siteName = data16.name;
                        }
                        
                         if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                        $scope.TotalData = [];
                        $scope.TotalData.push(member);

                    });
                });

            } else {
                console.log('fieldworkers');
               // Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                    console.log('fw', fw);
                    $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (part2) {
                          console.log('part2', part2);
                        $scope.beneficiaries = part2;
                        $scope.modalInstanceLoad.close();
                        angular.forEach($scope.beneficiaries, function (member, index) {
                            member.index = index + 1;

                            if (member.partiallydeleted == true) {
                                member.backgroundColor = "#C7196E"
                            } else if (member.deleteflag == true) {
                                member.backgroundColor = "#ff0000"
                            } else {
                                member.backgroundColor = "#000000"
                            }

                            var data14 = $scope.towns.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data14 != undefined) {
                                member.townName = data14.name;
                            }
                            var data15 = $scope.typologies.filter(function (arr) {
                                return arr.id == member.typology
                            })[0];

                            if (data15 != undefined) {
                                member.typologyName = data15.name;
                            }

                            var data16 = $scope.sites.filter(function (arr) {
                                return arr.id == member.site
                            })[0];

                            if (data16 != undefined) {
                                member.siteName = data16.name;
                            }

                            $scope.TotalData = [];
                            $scope.TotalData.push(member);
                        });
                    });
                });
            }

            /********************/
            $scope.status = 'active';

            $scope.$watch('status', function (newValue, oldValue) {
                    if (newValue === oldValue || newValue === '' || newValue === null) {
                        return;
                    } else {
                        if ($window.sessionStorage.roleId + "" === "3") {
                            $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"facility":{"inq":[' + $window.sessionStorage.coorgId + ']}},{"deleteflag":{"inq":[false]}}]}}';
                        } else {

                            Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                                $scope.filterCall = 'beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"transferred_flag":{"inq":[false]}}]}}';
                            });
                        }
                            Restangular.all($scope.filterCall).getList().then(function (household) {
                                $scope.beneficiaries = household;
                                angular.forEach($scope.beneficiaries, function (member, index) {

                                    member.index = index + 1;

                                    var data14 = $scope.towns.filter(function (arr) {
                                        return arr.id == member.town
                                    })[0];

                                    if (data14 != undefined) {
                                        member.townName = data14.name;
                                    }
                                    var data15 = $scope.typologies.filter(function (arr) {
                                        return arr.id == member.typology
                                    })[0];

                                    if (data15 != undefined) {
                                        member.typologyName = data15.name;
                                    }

                                    var data16 = $scope.sites.filter(function (arr) {
                                        return arr.id == member.site
                                    })[0];

                                    if (data16 != undefined) {
                                        member.siteName = data16.name;
                                    }

                                    $scope.TotalData = [];
                                    $scope.TotalData.push(member);

                                });
                            });

                        }
                    });





                /***************************FILTER *********************/
                /* $scope.SiteFilter = function (fl) {
                      // console.log('LevelFilter', fl);
                      //$scope.mycolor = '#4169e1';
                      $scope.backgroundCol1 = "blue";
                      $scope.backgroundCol2 = "";
                      if ($window.sessionStorage.roleId == 5) {

                          $scope.fws = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fws) {
                              $scope.fieldwork = fws;
                              $scope.dsr = Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (dsRes) {
                                  $scope.routes = dsRes;
                                  $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=site').getList().then(function (part1) {
                                      $scope.beneficiaries = part1;
                                      $scope.modalInstanceLoad.close();
                                      angular.forEach($scope.beneficiaries, function (member, index) {
                                          member.index = index + 1;

                                          for (var i = 0; i < $scope.routes.length; i++) {
                                              if (member.site == $scope.routes[i].id) {
                                                  member.SiteName = $scope.routes[i];
                                                  break;
                                              }
                                          }

                                          for (var j = 0; j < $scope.fieldwork.length; j++) {
                                              if (member.fieldworker == $scope.fieldwork[j].id) {
                                                  member.FWName = $scope.fieldwork[j];
                                                  break;
                                              }
                                          }

                                          if (member.partiallydeleted == true) {
                                              member.backgroundColor = "#C7196E"
                                          } else if (member.deleteflag == true) {
                                              member.backgroundColor = "#ff0000"
                                          } else {
                                              member.backgroundColor = "#000000"
                                          }
                                          $scope.TotalData = [];
                                          $scope.TotalData.push(member);

                                      });
                                  });
                              })
                          });
                      } else if ($window.sessionStorage.roleId == 15) {
                          $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
                              $scope.beneficiaries = part1;
                              $scope.modalInstanceLoad.close();
                              angular.forEach($scope.beneficiaries, function (member, index) {
                                  member.index = index + 1;

                                  if (member.partiallydeleted == true) {
                                      member.backgroundColor = "#C7196E"
                                  } else if (member.deleteflag == true) {
                                      member.backgroundColor = "#ff0000"
                                  } else {
                                      member.backgroundColor = "#000000"
                                  }

                                  $scope.TotalData = [];
                                  $scope.TotalData.push(member);
                              });
                          });
                      } else {
                          Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                              $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (part2) {
                                  $scope.beneficiaries = part2;
                                  $scope.modalInstanceLoad.close();
                                  angular.forEach($scope.beneficiaries, function (member, index) {
                                      member.index = index + 1;

                                      if (member.partiallydeleted == true) {
                                          member.backgroundColor = "#C7196E"
                                      } else if (member.deleteflag == true) {
                                          member.backgroundColor = "#ff0000"
                                      } else {
                                          member.backgroundColor = "#000000"
                                      }

                                      $scope.TotalData = [];
                                      $scope.TotalData.push(member);
                                  });
                              });
                          });
                      }

                  };

                  $scope.HotspotFilter = function (fl) {
                      // console.log('LevelFilter', fl);
                      $scope.backgroundCol1 = "";
                      $scope.backgroundCol2 = "blue";
                      if ($window.sessionStorage.roleId == 5) {

                          $scope.fws = Restangular.all('fieldworkers?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (fws) {
                              $scope.fieldwork = fws;
                              $scope.dsr = Restangular.all('distribution-routes?filter[where][deleteflag]=false' + '&filter[where][partnerId]=' + $window.sessionStorage.coorgId).getList().then(function (dsRes) {
                                  $scope.routes = dsRes;
                                  $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=hotspot%20ASC').getList().then(function (part1) {
                                      $scope.beneficiaries = part1;
                                      $scope.modalInstanceLoad.close();
                                      angular.forEach($scope.beneficiaries, function (member, index) {
                                          member.index = index + 1;

                                          for (var i = 0; i < $scope.routes.length; i++) {
                                              if (member.site == $scope.routes[i].id) {
                                                  member.SiteName = $scope.routes[i];
                                                  break;
                                              }
                                          }

                                          for (var j = 0; j < $scope.fieldwork.length; j++) {
                                              if (member.fieldworker == $scope.fieldwork[j].id) {
                                                  member.FWName = $scope.fieldwork[j];
                                                  break;
                                              }
                                          }

                                          if (member.partiallydeleted == true) {
                                              member.backgroundColor = "#C7196E"
                                          } else if (member.deleteflag == true) {
                                              member.backgroundColor = "#ff0000"
                                          } else {
                                              member.backgroundColor = "#000000"
                                          }
                                          $scope.TotalData = [];
                                          $scope.TotalData.push(member);

                                      });
                                  });
                              })
                          });
                      } else if ($window.sessionStorage.roleId == 15) {
                          $scope.part = Restangular.all('beneficiaries?filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
                              $scope.beneficiaries = part1;
                              $scope.modalInstanceLoad.close();
                              angular.forEach($scope.beneficiaries, function (member, index) {
                                  member.index = index + 1;

                                  if (member.partiallydeleted == true) {
                                      member.backgroundColor = "#C7196E"
                                  } else if (member.deleteflag == true) {
                                      member.backgroundColor = "#ff0000"
                                  } else {
                                      member.backgroundColor = "#000000"
                                  }

                                  $scope.TotalData = [];
                                  $scope.TotalData.push(member);
                              });
                          });
                      } else {
                          Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                              $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}}]}}').getList().then(function (part2) {
                                  $scope.beneficiaries = part2;
                                  $scope.modalInstanceLoad.close();
                                  angular.forEach($scope.beneficiaries, function (member, index) {
                                      member.index = index + 1;

                                      if (member.partiallydeleted == true) {
                                          member.backgroundColor = "#C7196E"
                                      } else if (member.deleteflag == true) {
                                          member.backgroundColor = "#ff0000"
                                      } else {
                                          member.backgroundColor = "#000000"
                                      }

                                      $scope.TotalData = [];
                                      $scope.TotalData.push(member);
                                  });
                              });
                          });
                      }

                  };*/
                /********************************************* INDEX *******************************************
	if ($window.sessionStorage.roleId == 5) {
		$scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
			$scope.beneficiaries = part1;
			$scope.modalInstanceLoad.close();
			angular.forEach($scope.beneficiaries, function (member, index) {
				member.index = index + 1;

				$scope.TotalData = [];
				$scope.TotalData.push(member);

			});
		});
	} else if ($window.sessionStorage.roleId == 15) {
		$scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().then(function (part1) {
			$scope.beneficiaries = part1;
			$scope.modalInstanceLoad.close();
			angular.forEach($scope.beneficiaries, function (member, index) {
				member.index = index + 1;

				$scope.TotalData = [];
				$scope.TotalData.push(member);
			});
		});
	} else {
		Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
			$scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
				$scope.beneficiaries = part2;
				$scope.modalInstanceLoad.close();
				angular.forEach($scope.beneficiaries, function (member, index) {
					member.index = index + 1;

					$scope.TotalData = [];
					$scope.TotalData.push(member);
				});
			});
		});
	}

	/************************** DELETE *******************************************
	$scope.Delete = function (id) {
		//$scope.OK();
		//$scope.documentdataModal = !$scope.documentdataModal;
		$scope.toggleLoading();
		$scope.deleteCount = 0;
		$scope.deleteReportCount = 0;
		$scope.deleteGroupMeetingCount = 0;
		$scope.auditlog.entityid = id;
		var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
		$scope.auditlog.description = 'Member Aarchived With Following Details: ' + 'Archived By UserId - ' + $window.sessionStorage.userId + ', ' + 'memberId - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

		Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
			$scope.item = [{
				partiallydeleted: true,
				lastmodifiedby: $window.sessionStorage.UserEmployeeId,
				lastmodifiedtime: new Date(),
				lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
			Restangular.one('beneficiaries/' + id).customPUT($scope.item[0]).then(function () {
				//$route.reload();
				Restangular.all('todos?filter[where][beneficiaryid]=' + id + '&filter[where][deleteflag]=false').getList().then(function (tds) {
					if (tds.length > 0) {
						$scope.todosToDelete = tds;
						$scope.deleteTodo(id);
					} else {
						Restangular.all('reportincidents?filter[where][beneficiaryid]=' + id + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
							if (rprtincdnts.length > 0) {
								$scope.reportsToDelete = rprtincdnts;
								$scope.deleteReportIncident(id);
							} else {
								$scope.itemben = [{
									partiallydeleted: false,
									deleteflag: true,
									lastmodifiedby: $window.sessionStorage.UserEmployeeId,
									lastmodifiedtime: new Date(),
									lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
								Restangular.one('beneficiaries/' + id).customPUT($scope.itemben[0]).then(function () {
									$scope.modalInstanceLoad.close();
									//$route.reload();
									window.location = "/members";
								});
							}
						});
					}
				});
			});
		});
	}
*/
                $scope.beneficiary = {
                    "reasonforarchive": false
                }
                //console.log('$scope.beneficiary.reasonforarchive', $scope.reasonforarchive)
                $scope.modalTitle = 'Reason For Archive Member'
                $scope.memberdataModal = false; $scope.documentdataModal = false;



                $scope.Delete11 = function (id) {
                    console.log('i m delete1');
                    $scope.archivMemId = id;
                    $scope.documentdataModal = !$scope.documentdataModal;
                }

                $scope.Delete1 = function (id) {
                    $scope.archivMemId = id;
                    $scope.modalInstance1 = $modal.open({
                        animation: true,
                        templateUrl: 'template/option.html',
                        scope: $scope,
                        backdrop: 'static'

                    });
                };

                $scope.Cancelmodal = function () {
                    $scope.modalInstance1.close();
                }; $scope.Delete2 = function (id) {
                    console.log('i m delete2');
                    //$scope.toggleLoading();
                    $scope.toggleLoading();
                    $scope.deleteCount = 0;
                    $scope.deleteReportCount = 0;
                    $scope.deleteGroupMeetingCount = 0;
                    $scope.auditlog.entityid = id;
                    var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
                    $scope.auditlog.description = 'Member Aarchived With Following Details: ' + 'Archived By UserId - ' + $window.sessionStorage.userId + ', ' + 'memberId - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

                    Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                        $scope.item = [{
                            partiallydeleted: true,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                        Restangular.one('beneficiaries/' + id).customPUT($scope.item[0]).then(function () {
                            //$route.reload();
                            Restangular.all('conditionheaders?filter[where][memberId]=' + id + '&filter[where][deleteflag]=false').getList().then(function (tds) {
                                if (tds.length > 0) {
                                    $scope.todosToDelete = tds;
                                    $scope.deleteTodo(id);
                                } else {
                                    Restangular.all('patientrecords?filter[where][memberId]=' + id + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
                                        if (rprtincdnts.length > 0) {
                                            $scope.reportsToDelete = rprtincdnts;
                                            $scope.deleteReportIncident(id);
                                        } else {
                                            $scope.itemben = [{
                                                partiallydeleted: false,
                                                deleteflag: true,
                                                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                                lastmodifiedtime: new Date(),
                                                lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                                            Restangular.one('beneficiaries/' + id).customPUT($scope.itemben[0]).then(function () {
                                                $scope.modalInstanceLoad.close();
                                                //$route.reload();
                                                window.location = "/members";
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    });
                }

                $scope.showValidation = false; $scope.toggleValidation = function () {
                    $scope.showValidation = !$scope.showValidation;
                }; $scope.okAlert = function () {
                    $scope.modalOneAlert.close();
                };

                //$scope.reasonforarchive = '';
                console.log('$scope.documentdataModal', $scope.documentdataModal); $scope.okdone = false; $scope.validatestring = ''; 
                $scope.OK = function () {
                    console.log('I m Ok');
                    //console.log('$scope.documentdataModal',$scope.documentdataModal);
                    if ($scope.beneficiary.reasonforarchive == false) {
                        /*	$scope.validatestring = $scope.validatestring + 'Please Enter State Count';
                        }
                        if ($scope.validatestring != '') {
                        	$scope.toggleValidation();
                        	$scope.validatestring1 = $scope.validatestring;
                        	$scope.validatestring = '';
                        	
                        	*/
                        //alert('Choose an Option');

                        $scope.modalOneAlert = $modal.open({
                            animation: true,
                            templateUrl: 'template/AlertModal.html',
                            scope: $scope,
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sm',
                            windowClass: 'modal-danger'

                        });

                    } else {
                        $scope.modalInstance1.close();
                        //$scope.documentdataModal = !$scope.documentdataModal;
                        $scope.item2 = [{
                            reasonforarchive: $scope.beneficiary.reasonforarchive
            }]
                        Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.item2[0]).then(function (reasonforarchive) {
                            console.log('reasonforarchive', reasonforarchive);
                        });
                        $scope.okdone = true;
                        console.log('$scope.reasonforarchive', $scope.beneficiary.reasonforarchive);
                        console.log('$scope.archivMemId', $scope.archivMemId);
                        if ($scope.okdone == true) {
                            $scope.Delete2($scope.archivMemId);
                        }
                    }
                }
                $scope.OK1 = function () {
                    console.log('$scope.reasonforarchive', $scope.beneficiary.reasonforarchive);
                    console.log('$scope.archivMemId', $scope.archivMemId);
                    var id = $scope.archivMemId;
                    $scope.item = [{
                        partiallydeleted: true,
                        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                        lastmodifiedtime: new Date(),
                        lastmodifiedbyrole: $window.sessionStorage.roleId,
                        reasonforarchive: $scope.beneficiary.reasonforarchive
            }]
                    Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.item[0]).then(function () {
                        //$route.reload();
                        Restangular.all('conditionheaders?filter[where][memberId]=' + $scope.archivMemId + '&filter[where][deleteflag]=false').getList().then(function (tds) {
                            if (tds.length > 0) {
                                $scope.todosToDelete = tds;
                                $scope.deleteTodo($scope.archivMemId);
                            } else {
                                Restangular.all('patientrecords?filter[where][memberId]=' + $scope.archivMemId + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
                                    if (rprtincdnts.length > 0) {
                                        $scope.reportsToDelete = rprtincdnts;
                                        $scope.deleteReportIncident($scope.archivMemId);
                                    } else {
                                        $scope.itemben = [{
                                            partiallydeleted: false,
                                            deleteflag: true,
                                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                            lastmodifiedtime: new Date(),
                                            lastmodifiedbyrole: $window.sessionStorage.roleId,
                                            reasonforarchive: $scope.beneficiary.reasonforarchive
            }]
                                        Restangular.one('beneficiaries/' + $scope.archivMemId).customPUT($scope.itemben[0]).then(function () {
                                            $scope.modalInstanceLoad.close();
                                            //$route.reload();
                                            window.location = "/members";
                                        });
                                    }
                                });
                            }
                        });
                    });


                }

                $scope.deleteTodo = function (benId) {
                    $scope.itemtodo = [{
                        deleteflag: true,
                        lastModifiedBy: $window.sessionStorage.userId,
                        lastModifiedDate: new Date(),
                        lastModifiedByRole: $window.sessionStorage.roleId
            }]
                    Restangular.one('conditionheaders/' + $scope.todosToDelete[$scope.deleteCount].id).customPUT($scope.itemtodo[0]).then(function () {
                        $scope.deleteCount++;
                        if ($scope.deleteCount < $scope.todosToDelete.length) {
                            $scope.deleteTodo(benId);
                        } else {
                            Restangular.all('reportincidents?filter[where][beneficiaryid]=' + benId + '&filter[where][deleteflag]=false').getList().then(function (rprtincdnts) {
                                if (rprtincdnts.length > 0) {
                                    $scope.reportsToDelete = rprtincdnts;
                                    $scope.deleteReportIncident(benId);
                                } else {
                                    $scope.itemben = [{
                                        partiallydeleted: false,
                                        deleteflag: true,
                                        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                        lastmodifiedtime: new Date(),
                                        lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                                    Restangular.one('beneficiaries/' + benId).customPUT($scope.itemben[0]).then(function () {
                                        $scope.modalInstanceLoad.close();
                                        //$route.reload();
                                        window.location = "/members";
                                    });
                                }
                            });
                        }
                    });
                }

                $scope.deleteReportIncident = function (benId) {
                    $scope.itemreport = [{
                        deleteflag: true,
                        lastModifiedBy: $window.sessionStorage.userId,
                        lastModifiedDate: new Date(),
                        lastModifiedByRole: $window.sessionStorage.roleId
            }]
                    Restangular.one('patientrecords/' + $scope.reportsToDelete[$scope.deleteReportCount].id).customPUT($scope.itemreport[0]).then(function () {
                        $scope.deleteReportCount++;
                        if ($scope.deleteReportCount < $scope.reportsToDelete.length) {
                            $scope.deleteReportIncident(benId);
                        } else {
                            $scope.itemben = [{
                                partiallydeleted: false,
                                deleteflag: true,
                                lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                                lastmodifiedtime: new Date(),
                                lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                            Restangular.one('beneficiaries/' + benId).customPUT($scope.itemben[0]).then(function () {
                                //$route.reload();
                                $scope.modalInstanceLoad.close();
                                window.location = "/members";
                            });
                        }
                    });
                }

                $scope.UnDelete = function (id) {
                    $scope.auditlog.entityid = id;
                    var respdate = $filter('date')(new Date(), 'dd-MMMM-yyyy');
                    $scope.auditlog.description = 'Member Unarchived With Following Details: ' + 'Unarchived By UserId - ' + $window.sessionStorage.userId + ', ' + 'memberId - ' + id + ', ' + 'RoleId - ' + $window.sessionStorage.roleId + ', ' + 'Date - ' + respdate;

                    Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                        $scope.item = [{
                            partiallydeleted: false,
                            deleteflag: false,
                            lastmodifiedby: $window.sessionStorage.UserEmployeeId,
                            lastmodifiedtime: new Date(),
                            lastmodifiedbyrole: $window.sessionStorage.roleId
            }]
                        Restangular.one('beneficiaries/' + id).customPUT($scope.item[0]).then(function () {
                            $route.reload();
                        });
                    });
                }


                /********************************************* INDEX *******************************************/

                /* $scope.sortingOrder = sortingOrder;
                 $scope.reverse = false;

                 $scope.sort_by = function (newSortingOrder) {
                     if ($scope.sortingOrder == newSortingOrder)
                         $scope.reverse = !$scope.reverse;

                     $scope.sortingOrder = newSortingOrder;
                 };*/

                $scope.sort = {
                active: '',
                descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

                /********************************** Language *************************************/

                /***********************************************************************/



            });
