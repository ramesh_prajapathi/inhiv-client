'use strict';

angular.module('secondarySalesApp')
    .controller('SalesAreasCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {

        /***********************************************SHOW AND HIDE BUTTON**************************************/

        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm1 = false;
        $scope.showForm = function () {
            var visible = $location.path() === '/district/create' || $location.path() === '/district/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/district/create';
                return visible;
                //	$scope.statedisable = false;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/district/create' || $location.path() === '/district/' + $routeParams.id;
            return visible;

        };
        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/district/create' || $location.path() === '/district/' + $routeParams.id;
            return visible;
        };
    
        $scope.someFocusVariable = true;
       // $scope.searchSalesArea = $scope.name;
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.sales_zoneId == null
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.zoneId = $window.sessionStorage.myRoute;
        }

        if ($window.sessionStorage.prviousLocation != "partials/sales-areas") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };
        /*....................................................................................*/
    
   
  Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zon) {
                    $scope.zonesdisplay = zon;
  });
     Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                $scope.countries = con;
     });
        /*****************************index**************************/
    
     $scope.filterFields = ['name','districtCode','stateName', 'CountryName'];
        $scope.searchSalesArea = '';

        Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (sal) {
            $scope.DisplaysalesAreas = sal;

            Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                $scope.countrieDis = con;
                Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zon) {
                    $scope.zonesDisplay = zon;
                    angular.forEach($scope.DisplaysalesAreas, function (member, index) {

                        for (var a = 0; a < $scope.countrieDis.length; a++) {
                            if (member.countryId == $scope.countrieDis[a].id) {
                                member.CountryName = $scope.countrieDis[a].name;
                                break;
                            }
                        }

                        for (var b = 0; b < $scope.zonesDisplay.length; b++) {
                            if (member.state == $scope.zonesDisplay[b].id) {
                                member.stateName = $scope.zonesDisplay[b].name;
                                break;
                            }
                        }

                        member.index = index + 1;



                    });
                });
            });
        });

    
     var timeoutPromise;
        var delayInMs = 1200;

        $scope.orderChange = function (code) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {
                console.log(code);

                 var data =  $scope.DisplaysalesAreas.filter(function (arr) {
                    return arr.districtCode == code
                })[0];
                 if (data != undefined) {
                     console.log('data',data);
                    $scope.salesArea.districtCode = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'District code Already Exist';
                 }
            }, delayInMs);
        };
    
    
     $scope.CheckDuplicate = function (name) {
           // console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.DisplaysalesAreas.filter(function (arr) {
                    return arr.name == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.salesAreaname = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'District Name Already Exist';
                 }
            }, delayInMs);
        };
    
    

        $scope.countryId = '';
        $scope.zoneId = '';

        $scope.$watch('countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (zone) {
                    $scope.zonesdisplay = zone;
                });

                $scope.zoneId = '';
                $window.sessionStorage.myRoute = newValue;
                $window.sessionStorage.currentRoute = newValue;
                //$scope.zoneId = $window.sessionStorage.myRoute;
                //console.log('Inside Value:', $window.sessionStorage.myRoute);
                $scope.sal = Restangular.all('sales-areas?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
                    $scope.DisplaysalesAreas = sal;
                    Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.countries = con;
                        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zon) {
                            $scope.zonesDisplay = zon;
                            angular.forEach($scope.DisplaysalesAreas, function (member, index) {

                                for (var a = 0; a < $scope.countries.length; a++) {
                                    if (member.countryId == $scope.countries[a].id) {
                                        member.CountryName = $scope.countries[a].name;
                                        break;
                                    }
                                }

                                for (var b = 0; b < $scope.zonesDisplay.length; b++) {
                                    if (member.state == $scope.zonesDisplay[b].id) {
                                        member.stateName = $scope.zonesDisplay[b].name;
                                        break;
                                    }
                                }

                                member.index = index + 1;


                            });
                        });
                    });
                });
            }
            $scope.countryId = +newValue;
        });


        $scope.$watch('zoneId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $window.sessionStorage.myRoute = newValue;
                $window.sessionStorage.currentRoute = newValue;
                //$scope.zoneId = $window.sessionStorage.myRoute;
                //console.log('Inside Value:', $window.sessionStorage.myRoute);
                $scope.sal = Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countryId + '&filter[where][zone]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (sal) {
                    $scope.DisplaysalesAreas = sal;
                    Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.countries = con;
                        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (zon) {
                            $scope.zonesDisplay = zon;
                            angular.forEach($scope.DisplaysalesAreas, function (member, index) {
                                for (var a = 0; a < $scope.countries.length; a++) {
                                    if (member.countryId == $scope.countries[a].id) {
                                        member.CountryName = $scope.countries[a].name;
                                        break;
                                    }
                                }

                                for (var b = 0; b < $scope.zonesDisplay.length; b++) {
                                    if (member.state == $scope.zonesDisplay[b].id) {
                                        member.stateName = $scope.zonesDisplay[b].name;
                                        break;
                                    }
                                }
                                member.index = index + 1;

                            });
                        });
                    });
                });
            }

        });


        $scope.$watch('salesArea.countryId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.zones = Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
        });


        /******************************************************************************************/

        $("#code").keydown(function (e) {
            var k = e.keyCode || e.which;
            var ok = k >= 65 && k <= 90 || // A-Z
                k >= 96 && k <= 105 || // a-z
                k >= 35 && k <= 40 || // arrows
                k == 9 || //tab
                k == 46 || //del
                k == 8 || // backspaces
                (!e.shiftKey && k >= 48 && k <= 57); // only 0-9 (ignore SHIFT options)

            if (!ok || (e.ctrlKey && e.altKey)) {
                e.preventDefault();
            }
        });

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }









        $scope.DisplaysalesAreas = [];
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('sales-areas/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.salesArea = {
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.roleId,
            deleteflag: false
        }


        $scope.submitDisable = false;
        $scope.validatestring = '';
        $scope.Save = function () {
            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
            
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";
            if ($scope.salesAreaname == '' || $scope.salesAreaname == null) {
                document.getElementById('name').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter District Name';
            } else if ($scope.salesArea.districtCode == '' || $scope.salesArea.districtCode == null) {
                document.getElementById('code').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter District Code';
            } 
            else if ($scope.salesArea.districtCode == '' || $scope.salesArea.districtCode.length != 2) {
                document.getElementById('code').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter 2-Digit District Code';
            }
            else if ($scope.salesArea.countryId == '' || $scope.salesArea.countryId == null) {
                $scope.salesArea.countryId = '';
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            } else if ($scope.salesArea.state == '' || $scope.salesArea.state == null) {
                $scope.salesArea.state = '';
                $scope.validatestring = $scope.validatestring + 'Please Select State ';
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                 var xyz = $scope.salesAreaname;
                $scope.salesArea.name = xyz.toUpperCase();
                
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.all('sales-areas').post($scope.salesArea).then(function () {
                    //$location.path('/sales-areas');
                    window.location = '/district';
                });
            }
        };

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };


        ///////////////////////////////////////////////////////////UPDATE DELETE///////////////////////////////    

        //$scope.zones = Restangular.all('zones').getList().$object;
        $scope.modalTitle = 'Thank You';
        if ($routeParams.id) {
            $scope.showForm1 = true;
            $scope.message = 'District has been Updated!';
            Restangular.one('sales-areas', $routeParams.id).get().then(function (salesArea) {

                $scope.znes = Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (znes) {
                    $scope.zones = znes;
                    $scope.salesArea.state = salesArea.state;

                    $scope.original = salesArea;
                    $scope.salesArea = Restangular.copy($scope.original);
                    $scope.salesAreaname = $scope.salesArea.name;
                });
            });
        } else {
            $scope.message = 'District has been Created!';
        }

        $scope.validatestring = '';
        $scope.Update = function () {
            
            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
            
            document.getElementById('name').style.border = "";
            if ($scope.salesAreaname == '' || $scope.salesAreaname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter District Area';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            else if (!regEmail.test($scope.salesAreaname)) {
                $scope.salesAreaname = '';
                 $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                document.getElementById('name').style.border = "1px solid #ff0000";
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {
                 var xyz = $scope.salesAreaname;
                $scope.salesArea.name = xyz.toUpperCase();
                
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                Restangular.one('sales-areas', $routeParams.id).customPUT($scope.salesArea).then(function () {
                    console.log('Sales-Area Saved', $scope.salesArea);
                    window.location = '/district';
                    //$location.path('/sales-areas');
                });
            }
        };


        /////////////////////////////////////////CALL CITY ACCORDING TO ZONE//////////////////


    })
.filter('search', function () {
        return function (list, query, fields) {

            if (!query) {
                return list;
            }

            query = query.toLowerCase().split(' ');

            if (!angular.isArray(fields)) {
                fields = [fields.toString()];
            }

            return list.filter(function (item) {
                return query.every(function (needle) {
                    return fields.some(function (field) {
                        var content = item[field] != null ? item[field] : '';

                        if (!angular.isString(content)) {
                            content = '' + content;
                        }

                        return content.toLowerCase().indexOf(needle) > -1;
                    });
                });
            });
        };
    });
