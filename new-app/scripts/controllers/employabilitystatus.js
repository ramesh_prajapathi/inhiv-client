'use strict';

angular.module('secondarySalesApp')
    .controller('EmployeeStatusCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        //$scope.OtherLang = false;
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/employabilitystatus/create' || $location.path() === '/employabilitystatus/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/employabilitystatus/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/employabilitystatus/create' || $location.path() === '/employabilitystatus/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/employabilitystatus/create' || $location.path() === '/employabilitystatus/' + $routeParams.id;
            return visible;
        };
        /*********/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/employabilitystatus") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        /************************ INDEX *******************************************/
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('employabilitystatuses?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.employabilitystatuses = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.employabilitystatuses, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.gendlanguages = sev;
            });

        } else {

            Restangular.all('employabilitystatuses').getList().then(function (mt) {
                $scope.employabilitystatuses = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.employabilitystatuses, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });
            Restangular.all('employabilitystatuses?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.gendlanguages = sev;

                        angular.forEach($scope.gendlanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.gendlanguages = sev;
                    });
                }
            });
        }

        /***new changes*****/
        Restangular.all('employabilitystatuses?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.genddisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.genddisply.filter(function (arr) {
                    return arr.orderno == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.employabilitystatus.language != '') {
                    $scope.employabilitystatus.orderno = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };




        /**********************************/
        $scope.disabledefault = false;
        if ($routeParams.id) {
            // $scope.OtherLang = true;
            $scope.message = 'Employability Status has been Updated!';
            Restangular.one('employabilitystatuses', $routeParams.id).get().then(function (employabilitystatus) {
                $scope.original = employabilitystatus;
                $scope.employabilitystatus = Restangular.copy($scope.original);
                $scope.currvalue = $scope.employabilitystatus.defaultValue;
                $scope.condiionoldvalue = $scope.employabilitystatus.orderno;
            });

            Restangular.all('employabilitystatuses?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;
                //console.log('$scope.AllvitalRecord', $scope.AllvitalRecord);
            });


        } else {
            $scope.message = 'Employability Status has been Created!';
            $scope.Updateflag = false;
            Restangular.all('employabilitystatuses?filter[where][defaultValue]=Yes' + '&filter[where][deleteflag]=false').getList().then(function (gender) {
                console.log('gender', gender);
                if (gender.length == 0) {
                    return;
                } else {
                    if (gender[0].defaultValue == 'Yes') {

                        $scope.employabilitystatus.defaultValue = 'No';
                    }
                }
            });
        }
        $scope.Search = $scope.name;

        $scope.showenglishLang = true;

        $scope.$watch('employabilitystatus.language', function (newValue, oldValue) {

            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    $scope.employabilitystatus.name = '';
                    $scope.employabilitystatus.parentId = '';

                    //                    $scope.employabilitystatus.defaultValue = '';
                    $scope.employabilitystatus.orderno = ''
                }

                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;


                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;
                }

            }
        });


        /***new changes*****/
        Restangular.all('employabilitystatuses?filter[where][deleteflag]=false' + '&filter[where][defaultValue]=Yes' + '&filter[where][language]=' + 1).getList().then(function (screens) {
            console.log('screens', screens);
            $scope.updatingobj = screens;
        });


        /************ SAVE *******************************************/


        $scope.employabilitystatus = {
            "name": '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            "deleteflag": false
        };


        $scope.$watch('employabilitystatus.parentId', function (newValue, oldValue) {

            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('employabilitystatuses', newValue).get().then(function (zn) {
                    $scope.employabilitystatus.orderno = zn.orderno;
                    $scope.employabilitystatus.defaultValue = zn.defaultValue;

                });
            }
        });

        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.creatingFlag = false;
        $scope.updatingFlag == false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('orderno').style.border = "";

            if ($scope.updatingobj.length > 0 && $scope.employabilitystatus.defaultValue == 'Yes') {
                $scope.creatingFlag = true;
            }

            if ($scope.employabilitystatus.language == '' || $scope.employabilitystatus.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.employabilitystatus.language == 1) {

                if ($scope.employabilitystatus.name == '' || $scope.employabilitystatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.employabilitystatus.defaultValue == '' || $scope.employabilitystatus.defaultValue == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter default';
                    document.getElementById('default').style.borderColor = "#FF0000";
                } else if ($scope.employabilitystatus.orderno == '' || $scope.employabilitystatus.orderno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('orderno').style.borderColor = "#FF0000";

                }
            } else if ($scope.employabilitystatus.language != 1) {

                if ($scope.employabilitystatus.parentId == '' || $scope.employabilitystatus.parentId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please select Name  In English';

                } else if ($scope.employabilitystatus.name == '' || $scope.employabilitystatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";

                }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {


                if ($scope.employabilitystatus.parentId === '') {
                    delete $scope.employabilitystatus['parentId'];
                }
                $scope.submitDisable = true;
                $scope.employabilitystatus.parentFlag = $scope.showenglishLang;
                Restangular.all('employabilitystatuses').post($scope.employabilitystatus).then(function () {


                    if ($scope.creatingFlag == true && $scope.employabilitystatus.language == 1) {

                        $scope.updateRow = {
                            defaultValue: 'No'
                        };

                        Restangular.one('employabilitystatuses', $scope.updatingobj[0].id).customPUT($scope.updateRow).then(function (resp) {
                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                            setTimeout(function () {
                                window.location = '/employabilitystatus';
                            }, 350);

                        });

                    } else {
                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        setTimeout(function () {
                            window.location = '/employabilitystatus';
                        }, 350);
                    }
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'employabilitystatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            };
        };

        /************************************** UPDATE *******************************************/

        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            if ($scope.updatingobj.length > 0 && $scope.employabilitystatus.defaultValue == 'Yes' && $scope.currvalue != $scope.employabilitystatus.defaultValue) {
                $scope.updatingFlag = true;
            }

            if ($scope.employabilitystatus.name == '' || $scope.employabilitystatus.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.employabilitystatus.parentId === '') {
                    delete $scope.employabilitystatus['parentId'];
                }
                $scope.submitDisable = true;
                $scope.employabilitystatus.parentFlag = $scope.showenglishLang;
                Restangular.one('employabilitystatuses', $routeParams.id).customPUT($scope.employabilitystatus).then(function (respupdate) {

                    if ($scope.updatingFlag == true) {

                        $scope.updateRow = {
                            defaultValue: 'No'
                        };

                        Restangular.one('employabilitystatuses', $scope.updatingobj[0].id).customPUT($scope.updateRow).then(function (resp) {
                            $scope.MandatoryUpdate(respupdate);

                        });

                    } else {
                        $scope.MandatoryUpdate(respupdate);
                    }
                }, function (error) {
                    if (error.data.error.constraint === 'employabilitystatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            }
        };

        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderno = myResponse.orderno;
                $scope.updateallRecord.defaultValue = myResponse.defaultValue;

                Restangular.one('employabilitystatuses', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('Test Name Saved');
                setTimeout(function () {
                    window.location = '/employabilitystatus';
                }, 350);
            }
        };


        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /************************************ DELETE *******************************************/



        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }






        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('employabilitystatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('employabilitystatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
