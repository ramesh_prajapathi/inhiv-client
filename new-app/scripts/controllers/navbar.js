'use strict';

angular.module('secondarySalesApp')
    .controller('NavbarCtrl', function ($scope, $http, Restangular, $window, $timeout, $location, baseUrl, $idle, $modal, $route, $rootScope) {

    $window.sessionStorage.MemberFilter = '';
     Restangular.one('lhslanguages?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').get().then(function (langResponse) {
            $scope.lhslanguage = langResponse[0];
         $scope.mainLogout = $scope.lhslanguage.logout;
         $scope.printonetoneheader = langResponse[0].onetone;
        // console.log('langResponse',langResponse);
         
         
        });
    
     Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
        });
    
    
    

        if ($window.sessionStorage.userId) {
            $scope.roles = Restangular.one('roles', $window.sessionStorage.roleId).get().$object;
        }

        $scope.hideMain = true;
        $scope.hideGeneral = true;
        $scope.hideMember = true;
        $scope.hideMemberMain = true;
        $scope.hideFWHide = true;
        $scope.hideFeildWorker = true;
     

        if ($window.sessionStorage.roleId == 1) {
            $scope.hideMain = true;
            $scope.hideGeneral = true;
            $scope.hideMember = true;
            $scope.hideMemberMain = false;
            $scope.hideFWHide = true;
            $scope.hideFeildWorker = false;
            $scope.myColor = '-13em';

        } else if ($window.sessionStorage.roleId == 4){
            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = true;
            $scope.hideMemberMain = false;
            $scope.hideFWHide = false;
            $scope.hideFeildWorker = false;
             $scope.myColor = '-7em';
            
        }
     else if ($window.sessionStorage.roleId == 2) {
            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = false;
            $scope.hideMemberMain = true;
            $scope.hideFWHide = false;
            $scope.hideFeildWorker = true;
            $scope.assignfieldworkertosite = true;
            $scope.comanagerprofile = true;
         

        }
    else if ($window.sessionStorage.roleId == 13) {
            $scope.hideMain = true;
            $scope.hideGeneral = false;
            $scope.hideMember = false;
            $scope.hideMemberMain = true;
            $scope.hideFWHide = false;
            $scope.hideFeildWorker = false;
            $scope.assignfieldworkertosite = false;
            $scope.comanagerprofile = false;
         

        }


        /****   for sidebar menu icon change *****/
        $scope.imageHome = "images/piclinks/home-gray.png";
        $scope.imageGeneral = "images/piclinks/facility-grey.png";
        $scope.imageGeneral1 = "images/piclinks/facility-grey.png";
         $scope.imageFW = "images/piclinks/multilanguagedark.png";
        $scope.imageFW1 = "images/piclinks/members-grey.png";
        $scope.imageFacility = "images/piclinks/dashboarddark.png";
        $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
        $scope.imageDashboard = "images/piclinks/dashboarddark.png";
        $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
        $scope.imageNOBudget = "images/piclinks/budgetdark.png";

        $scope.colorHome = "#FFFFFF";
        $scope.colorGeneral = "#FFFFFF";
        $scope.colorFW = "#FFFFFF";
        $scope.colorFacility = "#FFFFFF";
        $scope.colorFW1 = "#FFFFFF";
        $scope.colorFacility1 = "#FFFFFF";
        $scope.colorDashboard = "#FFFFFF";
        $scope.colorSPMBudget = "#FFFFFF";
        $scope.colorNOBudget = "#FFFFFF";

        $scope.borderHome = '5px solid #FFFFFF';
            $scope.borderGeneral = '5px solid #FFFFFF';
            $scope.borderFW = '5px solid #FFFFFF';
            $scope.borderFW1 = '5px solid #FFFFFF';
            $scope.borderFacility = '5px solid #FFFFFF';
            $scope.borderFacility1 = '5px solid #FFFFFF';

        $scope.clickHome = function () {
            $scope.imageHome = "images/piclinks/home-blue.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageGeneral1 = "images/piclinks/facility-grey.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFW1 = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFW1 = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorFacility1 = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";

            $scope.borderHome = '5px solid #004891';
            $scope.borderGeneral = '5px solid #FFFFFF';
            $scope.borderFW = '5px solid #FFFFFF';
            $scope.borderFW1 = '5px solid #FFFFFF';
            $scope.borderFacility = '5px solid #FFFFFF';
            $scope.borderFacility1 = '5px solid #FFFFFF';
            $location.path("/");

        };

        $scope.clickSPMBudget = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageGeneral1 = "images/piclinks/facility-grey.png";
            $scope.imageFW = "images/piclinks/admindark.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetwhite.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";
            $location.path("/spmbudgets");

        };

        $scope.clickNOBudget = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageFW = "images/piclinks/admindark.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetwhite.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";
            $location.path("/nobudgets");

        };

        $rootScope.clickDashboard = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboardwhite.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";

            if ($scope.roles.dashboardfacility) {
                $location.path("/codashboard");
            } else if ($scope.roles.nobudget) {
                $location.path("/nodashboard");
            } else if ($scope.roles.rodashboard) {
                $location.path("/rodashboard");
            } else if ($scope.roles.sodashboard) {
                $location.path("/spmdashboard");
            } else if ($scope.roles.fsmentordashboard) {
                $location.path("/fsmentordashboard");
            }

        };

        $scope.clickGeneral = function () {
            //            $scope.imageHome = "images/piclinks/homedark.png";
            //            $scope.imageGeneral = "images/piclinks/facilitywhite.png";
            //            $scope.imageFW = "images/piclinks/admindark.png";
            //            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-blue.png";
            $scope.imageGeneral1 = "images/piclinks/facility-blue.png";
            $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFW1 = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/dashboardwhite.png";
            $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFW1 = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorFacility1 = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";

            $scope.borderHome = '5px solid #FFFFFF';
            $scope.borderGeneral = '5px solid #004891';
            $scope.borderFW = '5px solid #FFFFFF';
            $scope.borderFW1 = '5px solid #FFFFFF';
            $scope.borderFacility = '5px solid #FFFFFF';
            $scope.borderFacility1 = '5px solid #FFFFFF';
        };

        $scope.clickFW = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageGeneral1 = "images/piclinks/facility-grey.png";
            //            $scope.imageFW = "images/piclinks/adminwhite.png";
          $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFW1 = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFW1 = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";

            $scope.borderHome = '5px solid #FFFFFF';
            $scope.borderGeneral = '5px solid #FFFFFF';
            $scope.borderFW = '5px solid #004891';
            $scope.borderFW1 = '5px solid #FFFFFF';
            $scope.borderFacility = '5px solid #FFFFFF';
            $scope.borderFacility1 = '5px solid #FFFFFF';
        };

        $scope.clickFW11 = function () {
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageGeneral1 = "images/piclinks/facility-grey.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFW1 = "images/piclinks/members-blue.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFW1 = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorFacility1 = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";

            $scope.borderHome = '5px solid #FFFFFF';
            $scope.borderGeneral = '5px solid #FFFFFF';
            $scope.borderFW = '5px solid #FFFFFF';
            $scope.borderFW1 = '5px solid #004891';
            $scope.borderFacility = '5px solid #FFFFFF';
            $scope.borderFacility1 = '5px solid #FFFFFF';
        };

        $scope.clickFacility = function () {
            //            $scope.imageHome = "images/piclinks/homedark.png";
            //            $scope.imageGeneral = "images/piclinks/facilitydark.png";
            //            $scope.imageFW = "images/piclinks/admindark.png";
            //            $scope.imageFacility = "images/piclinks/dashboardwhite.png";
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-grey.png";
            $scope.imageGeneral1 = "images/piclinks/facility-grey.png";
             $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFW1 = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFW1 = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorFacility1 = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";

            $scope.borderHome = '5px solid #FFFFFF';
            $scope.borderGeneral = '5px solid #FFFFFF';
            $scope.borderFW = '5px solid #FFFFFF';
            $scope.borderFW1 = '5px solid #FFFFFF';
            $scope.borderFacility = '5px solid #004891';
            $scope.borderFacility1 = '5px solid #FFFFFF';
        };

        $scope.clickFacility1 = function () {
            //            $scope.imageHome = "images/piclinks/homedark.png";
            //            $scope.imageGeneral = "images/piclinks/facilitydark.png";
            //            $scope.imageFW = "images/piclinks/admindark.png";
            //            $scope.imageFacility = "images/piclinks/dashboardwhite.png";
            $scope.imageHome = "images/piclinks/home-gray.png";
            $scope.imageGeneral = "images/piclinks/facility-blue.png";
            $scope.imageGeneral1 = "images/piclinks/facility-blue.png";
              $scope.imageFW = "images/piclinks/multilanguagedark.png";
            $scope.imageFW1 = "images/piclinks/members-grey.png";
            $scope.imageFacility = "images/piclinks/dashboarddark.png";
            $scope.imageFacility1 = "images/piclinks/dashboarddark.png";
            $scope.imageDashboard = "images/piclinks/dashboarddark.png";
            $scope.imageSPMBudget = "images/piclinks/budgetdark.png";
            $scope.imageNOBudget = "images/piclinks/budgetdark.png";

            $scope.colorHome = "#FFFFFF";
            $scope.colorGeneral = "#FFFFFF";
            $scope.colorFW = "#FFFFFF";
            $scope.colorFW1 = "#FFFFFF";
            $scope.colorFacility = "#FFFFFF";
            $scope.colorFacility1 = "#FFFFFF";
            $scope.colorDashboard = "#FFFFFF";
            $scope.colorSPMBudget = "#FFFFFF";
            $scope.colorNOBudget = "#FFFFFF";

            //$scope.borderHome = '5px solid #FFFFFF';
            $scope.borderGeneral = '5px solid #FFFFFF';
            $scope.borderFW = '5px solid #FFFFFF';
            $scope.borderFW1 = '5px solid #FFFFFF';
            $scope.borderFacility = '5px solid #FFFFFF';
            $scope.borderFacility1 = '5px solid #004891';
        };
        /**** Above code  for sidebar menu icon change *****/

        Restangular.one('users?filter[where][username]=' + $window.sessionStorage.userName).get().then(function (user) {
            $scope.user = user[0];
        });

        $scope.rolesconfiguration = true;
        $scope.rolesconfiguration1 = true;
        //        if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 10 || $window.sessionStorage.roleId == 11 || $window.sessionStorage.roleId == 12 || $window.sessionStorage.roleId == 13 || $window.sessionStorage.roleId == 14) {
        //            $scope.rolesconfiguration = false;
        //        } else {
        //            $scope.rolesconfiguration1 = false;
        //        }
    
    if ($window.sessionStorage.roleId == 1 || $window.sessionStorage.roleId == 4) {
            $scope.rolesconfiguration = false;
        } else {
            $scope.rolesconfiguration1 = false;
        }
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.KnowledgeBaseName = "Knowledge Base";
        if ($scope.UserLanguage == 1) {
            $scope.KnowledgeBaseName = "Knowledge Base";
        } else if ($scope.UserLanguage == 2) {
            $scope.KnowledgeBaseName = "ज्ञानधार";
        } else if ($scope.UserLanguage == 3) {
            $scope.KnowledgeBaseName = "ಜ್ಞಾನದ ತಳಹದಿ";
        } else if ($scope.UserLanguage == 4) {
            $scope.KnowledgeBaseName = "அறிவு சார்ந்த";
        } else if ($scope.UserLanguage == 5) {
            $scope.KnowledgeBaseName = "నాలెడ్జ్ బేస్";
        } else if ($scope.UserLanguage == 6) {
            $scope.KnowledgeBaseName = "पायाभूत माहिती";
        }

        /*
		       $scope.menu = [{
		         'title': 'Home',
		         'route': '/?(navbar)?',
		         'link': '/'
		       },
		       {
		         'title': 'hh',
		         'route': '/hh',
		         'link': '/hh'
		       }];
		       */
        /*************************************************************************************
			if ($window.sessionStorage.roleId == 5) {
				Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
					$scope.comemberid = comember.id;
					$scope.partners1 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][facilityId]=' + $scope.comemberid + '&filter={"where":{"lastmodifiedbyrole":{"inq":[5,6]}}}').getList().then(function (resPartner1) {
						$scope.partners = resPartner1;
					});
				});

			} else {
				$scope.partners2 = Restangular.all('beneficiaries?filter[where][state]=' + $window.sessionStorage.zoneId + '&filter[where][district]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=false' + '&filter[where][fieldworker]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][lastmodifiedbyrole]=6').getList().then(function (resPartner2) {
					$scope.partners = resPartner2;
				});
			}
		/**************************************** Member *******************************************/

        if ($window.sessionStorage.roleId == 2) {
            $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[order]=fullname%20ASC').getList().then(function (part1) {
                $scope.partners = part1;
                angular.forEach($scope.partners, function (member, index) {
                    member.index = index + 1;
                });
            });
        } else {
            Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
                $scope.part = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().then(function (part2) {
                    $scope.partners = part2;
                    angular.forEach($scope.partners, function (member, index) {
                        member.index = index + 1;

                    });
                });
            });
        }

        /***********************************************************************/




        $scope.searchbulkupdate = '';



        $scope.openOnetoOne = function () {
             $rootScope.fullname = '';
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/OnetooOne.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okOnetooOne = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/onetoone/" + fullname);
            $route.reload();
        };

        $scope.cancelOnetooOne = function () {
            $scope.modalInstance1.close();
        };

        $scope.loading = true;
        //$scope.LoadingMOdal = false;
        $scope.toggleLoading = function () {
            $scope.modalInstanceLoad = $modal.open({
                animation: true,
                templateUrl: 'template/LodingModal.html',
                scope: $scope,
                backdrop: 'static',
                size: 'sm'

            });
            //$scope.LoadingMOdal = !$scope.LoadingMOdal;
        };



        $scope.openMainReportIncident = function () {
            $scope.modalMainReport = $modal.open({
                animation: true,
                templateUrl: 'template/MainReportIncident.html',
                scope: $scope,
                backdrop: 'static'

            });

        };

        $scope.okMainReportIncident = function (fullname) {
            $scope.modalMainReport.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okReportIncident', $rootScope.fullname);
            $location.path("/reportincident");
            $route.reload();
        };

        $scope.cancelMainReportIncident = function () {
            $scope.modalMainReport.close();
        };
        /***********************************************/
        $scope.openApplyforScheme = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/ApplyforScheme.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okApplyforScheme = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyforscheme");
            $route.reload();
        };

        $scope.cancelApplyforScheme = function () {
            $scope.modalInstance1.close();
        };

        $scope.openApplyforDocument = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/ApplyforDocument.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okApplyforDocument = function (fullname) {
            $scope.modalInstance1.close();

            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.okOnetooOne', $rootScope.fullname);
            $location.path("/applyfordocument");
            $route.reload();
        };

        $scope.cancelApplyforDocument = function () {
            $scope.modalInstance1.close();
        };


        $scope.openfPlaning = function () {
            $scope.modalInstance1 = $modal.open({
                animation: true,
                templateUrl: 'template/fPlaning.html',
                scope: $scope,
                backdrop: 'static'

            });
        };

        $scope.okfPlaning = function (fullname) {
            $scope.modalInstance1.close();
            var fullname = fullname;
            $rootScope.fullname = $window.sessionStorage.fullName = fullname;
            console.log('$rootScope.fullname', $rootScope.fullname);
            $location.path("/financialPlanning");
            $route.reload();
        };

        $scope.cancelfPlaning = function () {
            $scope.modalInstance1.close();
        };
        /****************************************/

        $scope.menu = [{
                'title': 'Home',
                'link': '/'
    },
            {
                'title': 'States',
                'link': '/states'
    },
            {
                'title': 'Contact',
                'link': '#'
    }];

        $scope.fwprofileedit = true;
        $scope.coprofileedit = true;
        if ($window.sessionStorage.roleId == 5) {
            $scope.coprofileedit = false;
            $scope.UserId = $window.sessionStorage.UserEmployeeId;
        } else {
            //$scope.UserId = $window.sessionStorage.UserEmployeeId;
            $scope.fwprofileedit = false;
            $scope.UserId = $window.sessionStorage.UserEmployeeId;
        }

      //  console.log($window.sessionStorage.userId + 'sddsf');
        $scope.showNav = function () {
            //console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
            if ($window.sessionStorage.userId) {
                return true;
            }
        };
        $scope.isActive = function (route) {
            return route === $location.path();
        };

        $scope.getClass = function (path) {
            if ($location.path().substr(0, path.length) === path) {
                return 'active';
            } else {
                return '';
            }
        }

        $scope.logout = function () {
            //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
            Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
                $window.sessionStorage.userId = '';
                console.log('Logout');
            }).then(function (redirect) {

                $location.path("/login");
                $idle.unwatch();

            });
        };


        /*************************** Language *******************************/
        /*$scope.languages = Restangular.all('languages').getList().$object;
        $scope.multiLang = Restangular.one('multilanguages?filter[where][id]=' + $window.sessionStorage.language).get().then(function (langResponse) {

            $rootScope.style = {
                "font-size": langResponse[0].fontsize,
                "font-weight": "normal"
            }

            $scope.printonetoneheader = langResponse[0].onetoneheader;
            $scope.applyforscheme = langResponse[0].applyforscheme;
            $scope.applyfordocument = langResponse[0].applyfordocument;
            $scope.bulkupdateheader = langResponse[0].bulkupdateheader;
            $scope.eventheader = langResponse[0].eventheader;
            $scope.headergroupmeeting = langResponse[0].headergroupmeeting;
            $scope.stakeholdermeeting = langResponse[0].stakeholdermeeting;
            $scope.printheaderreportincident = langResponse[0].headerreportincident;
            $scope.printmembername = langResponse[0].membername;
            $scope.nickname = langResponse[0].nickname;
            $scope.facility = langResponse[0].facility;
            $scope.fieldworkerth = langResponse[0].fieldworkerth;
            $scope.eventheader = langResponse[0].eventheader;
            $scope.comanagerhdfprofileheader = langResponse[0].comanagerhdfprofileheader;
            $scope.site = langResponse[0].site;
            $scope.home = langResponse[0].home;
            $scope.fieldwork = langResponse[0].fieldwork;
            $scope.members = langResponse[0].members;
            $scope.hotspot = langResponse[0].hotspot;
            $scope.trackapplication = langResponse[0].trackapplication;
            $scope.archivedmembers = langResponse[0].archivedmembers;
            $scope.assignfwtosite = langResponse[0].assignfwtosite;
            $scope.dashboard = langResponse[0].dashboard;
            $scope.facilityprofileheader = langResponse[0].facilityprofileheader;
            $scope.searchfor = langResponse[0].searchfor;
            $scope.savebutton = langResponse[0].savebutton;
            $scope.update = langResponse[0].update;
            $scope.cancel = langResponse[0].cancel;
            $scope.create = langResponse[0].create;
            $scope.printprofile = langResponse[0].profile;
            $scope.printsignout = langResponse[0].logout;
            $scope.financialplanning = langResponse[0].financialplanning;
            $scope.pcobudget = langResponse[0].cobudget;


            $scope.groupmeetingslabel = langResponse[0].groupmeetingslabel;
            $scope.stakeholdermeetingslabel = langResponse[0].stakeholdermeetingslabel;
            $scope.eventslabel = langResponse[0].eventslabel;

        });

*/
    });
