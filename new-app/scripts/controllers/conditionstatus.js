'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionStatusCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        //$scope.OtherLang = false;
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/conditionstatus/create' || $location.path() === '/conditionstatus/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/conditionstatus/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/conditionstatus/create' || $location.path() === '/conditionstatus/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/conditionstatus/create' || $location.path() === '/conditionstatus/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/conditionstatus-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.showenabled = true;

        $scope.$watch('conditionstatus.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {

                    $scope.conditionstatus.orderNo = '';
                    $scope.conditionstatus.name = '';
                    $scope.conditionstatus.parentId = '';
                    $scope.conditionstatus.actionable = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;

                }


            }
        });


        $scope.$watch('conditionstatus.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('conditionstatuses', newValue).get().then(function (zn) {
                    $scope.conditionstatus.orderNo = zn.orderNo;
                    $scope.conditionstatus.actionable = zn.actionable;
                });
            }
        });

        $scope.$watch('conditionstatus.actionable', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue == 'Yes') {
                    $scope.showenabled = false;
                } else {
                    $scope.showenabled = true;
                }

            };
        });

        /*********************************************************************************/

        if ($routeParams.id) {
            //$scope.OtherLang = true;
            $scope.message = 'Status has been Updated!';
            Restangular.one('conditionstatuses', $routeParams.id).get().then(function (conditionstatus) {
                $scope.original = conditionstatus;
                $scope.conditionstatus = Restangular.copy($scope.original);
                $scope.condiionoldvalue = $scope.conditionstatus.orderNo;
                // $scope.conditionstatus.enabledFor = $scope.conditionstatus.enabledFor.split(",");
            });
            Restangular.all('conditionstatuses?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;

            });
        } else {
            $scope.message = 'Status has been Created!';
        }

        //$scope.Search = $scope.name;
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /******************************** INDEX *******************************************/
        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('conditionstatuses?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.conditionstatuses = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditionstatuses, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.condistatuslanguages = sev;
            });

        } else {

            Restangular.all('conditionstatuses').getList().then(function (mt) {
                $scope.conditionstatuses = mt;
                   Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditionstatuses, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });

            Restangular.all('conditionstatuses?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condistatuslanguages = sev;

                        angular.forEach($scope.condistatuslanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            //console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condistatuslanguages = sev;
                    });
                }
            });


        }


        Restangular.all('conditionstatuses?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.conditionstatusdisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.conditionstatusdisply.filter(function (arr) {
                    return arr.orderNo == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.conditionstatus.language != '') {
                    $scope.conditionstatus.orderNo = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };





        /********************************************* SAVE *******************************************/
        $scope.conditionstatus = {
            name: '',
            actionable: '',
            enabledFor: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditionstatus.language == '' || $scope.conditionstatus.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionstatus.language == 1) {
                if ($scope.conditionstatus.name == '' || $scope.conditionstatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Status';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.conditionstatus.orderNo == '' || $scope.conditionstatus.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.conditionstatus.actionable == '' || $scope.conditionstatus.actionable == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Actionable';
                    document.getElementById('order').style.borderColor = "#FF0000";
                }
            } else if ($scope.conditionstatus.language != 1) {
                if ($scope.conditionstatus.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition Status in English';

                } else if ($scope.conditionstatus.name == '' || $scope.conditionstatus.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Status';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.conditionstatus.orderNo == '' || $scope.conditionstatus.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                } else if ($scope.conditionstatus.actionable == '' || $scope.conditionstatus.actionable == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Actionable';
                    document.getElementById('order').style.borderColor = "#FF0000";
                }
            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.conditionstatus.parentId === '') {
                    delete $scope.conditionstatus['parentId'];
                }
                $scope.submitDisable = true;
                $scope.conditionstatus.parentFlag = $scope.showenglishLang;
                $scope.conditionstatuses.post($scope.conditionstatus).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/conditionstatus-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'conditionstatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                })


            };
        };

        $scope.modalTitle = 'Thank You';

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";
            if ($scope.conditionstatus.language == '' || $scope.conditionstatus.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditionstatus.name == '' || $scope.conditionstatus.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Status';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditionstatus.orderNo == '' || $scope.conditionstatus.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            } else if ($scope.conditionstatus.actionable == '' || $scope.conditionstatus.actionable == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Actionable';
                document.getElementById('order').style.borderColor = "#FF0000";
            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.conditionstatus.parentId === '') {
                    delete $scope.conditionstatus['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('conditionstatuses', $routeParams.id).customPUT($scope.conditionstatus).then(function (updateResp) {
                   // $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('conditionstatus  Saved');
                    $scope.MandatoryUpdate(updateResp);

                }, function (error) {
                    if (error.data.error.constraint === 'conditionstatus_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });


            }
        };



        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderNo = myResponse.orderNo;
                $scope.updateallRecord.actionable = myResponse.actionable;
                Restangular.one('conditionstatuses', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                    window.location = '/conditionstatus-list';
                }, 350);
            }
        };


        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('conditionstatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('conditionstatuses/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
