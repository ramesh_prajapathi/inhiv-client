'use strict';

angular.module('secondarySalesApp')
    .controller('SurveyQuestionsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/

        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/onetoonequestion/create' || $location.path() === '/onetoonequestion/' + $routeParams.id;
            return visible;
        };
        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/onetoonequestion/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/onetoonequestion/create';
            return visible;
        };


        /***************************** Pagination ***********************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            $scope.pillarid = $window.sessionStorage.myRoute;
            //console.log('$scope.pillarid', $scope.pillarid);
        }

        $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        // console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        //if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View") {
        if ($window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage-View" && $window.sessionStorage.prviousLocation != "partials/surveyquestion-mlanguage") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $window.sessionStorage.myRoute_currentPage = 1;
            //$scope.currentpage = 1;
            //$scope.pageSize = 5;
        }

        /*************************************************************************************************/


        $scope.showenglishLang = true;

        $scope.$watch('surveyquestion.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {


                    $scope.surveyquestion.question = '';
                    $scope.surveyquestion.parentId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableforother = true;
                    $scope.otherLangDisplay = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableforother = false;
                    $scope.otherLangDisplay = false;
                }

            }
        });
        Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=1' + '&filter[where][type]=Todo').getList().then(function (Resp) {
            $scope.todotypes = Resp;
        });

        Restangular.all('todotypes?filter[where][deleteflag]=false&filter[where][language]=1' + '&filter[where][type]=Message').getList().then(function (Resp) {
            $scope.todomessages = Resp;
        });

        $scope.someFocusVariable = true;
        $scope.filterFields = ['index', 'langName', 'question'];
        $scope.searchUser = '';


        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.medilanguages = sev;
            });

            $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

            Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[order]=serialno ASC' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (SalRes) {
                $scope.surveyquestions = SalRes;
                angular.forEach($scope.surveyquestions, function (member, index) {


                    for (var b = 0; b < $scope.Displanguages.length; b++) {
                        if (member.language == $scope.Displanguages[b].id) {
                            member.langName = $scope.Displanguages[b].name;
                            break;
                        }
                    }
                    
                    if(member.questiontype == 'rb'){
                        member.questiontypeName = 'Radio Button';
                        
                    } else if (member.questiontype == 'r'){
                         member.questiontypeName = 'Yes No Question';
                        
                    } else if (member.questiontype == 'nt'){
                         member.questiontypeName = 'Numeric Text Field';
                        
                    } else if (member.questiontype == 'c'){
                         member.questiontypeName = 'Check Box';
                        
                    }

                    member.index = index + 1;
                    // console.log('member', member);

                });
            });



        } else {
            $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
            Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[order]=serialno ASC').getList().then(function (SalRes) {
                $scope.surveyquestions = SalRes;
                angular.forEach($scope.surveyquestions, function (member, index) {

                    for (var b = 0; b < $scope.Displanguages.length; b++) {
                        if (member.language == $scope.Displanguages[b].id) {
                            member.langName = $scope.Displanguages[b].name;
                            break;
                        }
                    }
                    
                    if(member.questiontype == 'rb'){
                        member.questiontypeName = 'Radio Button';
                        
                    } else if (member.questiontype == 'r'){
                         member.questiontypeName = 'Yes No Question';
                        
                    } else if (member.questiontype == 'nt'){
                         member.questiontypeName = 'Numeric Text Field';
                        
                    } else if (member.questiontype == 'c'){
                         member.questiontypeName = 'Check Box';
                        
                    }
                    member.index = index + 1;
                    // console.log('member', member.langName);

                });
            });
            Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.medilanguages = sev;
            });

        }


        Restangular.all('surveyquestions?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.questionDisplay = zn;
        });
        Restangular.all('typologies?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (respTypo) {
            $scope.typologies = respTypo;
        });
        $scope.$watch('surveyquestion.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                Restangular.one('surveyquestions', newValue).get().then(function (zn) {
                    $scope.surveyquestion.serialno = zn.serialno;
                    $scope.surveyquestion.range = zn.range;
                    $scope.surveyquestion.noofoptions = zn.noofoptions;
                    $scope.surveyquestion.questiontype = zn.questiontype;
                    $scope.surveyquestion.yesaction = zn.yesaction;
                    $scope.surveyquestion.noaction = zn.noaction;
                    $scope.surveyquestionansweroptions = zn.answeroptions;
                    $scope.surveyquestion.yespopup = zn.yespopup;
                    $scope.surveyquestion.yestodotype = zn.yestodotype;
                    //$scope.surveyquestion.skipdate = 'yes';
                    //$scope.surveyquestion.skipondate = zn.skipondate;

                    if (zn.skipdate == true && zn.skiponTypology == true) {
                        $scope.surveyquestion.skipdate = ['yes', 'yes1'];
                        $scope.surveyquestion.skipondate = zn.skipondate;
                        $scope.surveyquestion.skiponTypologyId = zn.skiponTypologyId;
                    } else if (zn.skipdate == false && zn.skiponTypology == false) {
                        $scope.surveyquestion.skipdate = [];
                    } else if (zn.skipdate == true && zn.skiponTypology == false) {
                        $scope.surveyquestion.skipdate = ['yes'];
                        $scope.surveyquestion.skipondate = zn.skipondate;
                    } else if (zn.skipdate == false && zn.skiponTypology == true) {
                        $scope.surveyquestion.skipdate = ['yes1'];
                        $scope.surveyquestion.skiponTypologyId = zn.skiponTypologyId;
                    } else if (zn.skipquestion == true) {
                        $scope.surveyquestion.skipquestion = zn.skipquestion;
                        $scope.surveyquestion.skipto = zn.skipto;
                    }

                });

            }
        });


        var timeoutPromise;
        var delayInMs = 1000;

        $scope.orderChange = function (serialno) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.questionDisplay.filter(function (arr) {
                    return arr.serialno == serialno
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.questionValue != question && $scope.surveyquestion.language != '') {
                    $scope.surveyquestion.serialno = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Question No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };


        $scope.YesPopup = true;
        $scope.NoPopup = true;
        $scope.YesIncrement = true;
        $scope.NoIncrement = true;
        $scope.YesProvideInfo = true;
        $scope.NoProvideInfo = true;

        /*********/

        $scope.surveyquestion = {
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false,
            skipdate: false,
            skiponTypology: false
        };

        $scope.submitsurveyquestns = Restangular.all('surveyquestions').getList().$object;



        $scope.NotSubQuestion = true;
        $scope.NotSkipQuestion = true;
        $scope.YesNotSkipQuestion = true;
        $scope.NoNotSkipQuestion = true;
        $scope.NotCountedCheckbox = true;
        $scope.NotRange = true;
        $scope.NotScheme = true;
        $scope.actionOn = true;
        $scope.hideoption = true;
        $scope.actionOnNO = true;
        $scope.actionOnYes = true;
        $scope.skipDate = true;
        $scope.skipTypology = true;
        $scope.skipQuestion = true;
        $scope.hideLimit = true;


        $scope.flag = false;
        $scope.$watch('surveyquestion.questiontype', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            }
            /*else if (newValue == 'sq') {
                $scope.NotSubQuestion = false;
                $scope.NotSkipQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } else if (newValue == 'skp') {
                $scope.NotSkipQuestion = false;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
            } */
            else if (newValue == 'c') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = true;
                $scope.actionOnYes = true;
                $scope.actionOn = true;
                $scope.hideoption = false;
                $scope.hideLimit = true;
                //$scope.surveyquestion.skipdate = '';

            } else if (newValue == 'rb') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = true;
                $scope.actionOnYes = true;
                $scope.actionOn = true;
                $scope.hideoption = false;
                $scope.hideLimit = true;
               // $scope.surveyquestion.skipdate = '';

            } else if (newValue == 'nt') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = true;
                $scope.actionOnYes = true;
                $scope.actionOn = true;
                $scope.hideoption = true;
                $scope.hideLimit = false;
               // $scope.surveyquestion.skipdate = '';

            } else if (newValue == 'r') {
                $scope.NotSkipQuestion = true;
                $scope.NotSubQuestion = true;
                $scope.NotCountedCheckbox = true;
                $scope.NotRange = true;
                $scope.NotScheme = true;
                $scope.actionOnNO = false;
                $scope.actionOnYes = false;
                $scope.flag = true;
                $scope.hideoption = false;
                $scope.hideLimit = true;
                //$scope.surveyquestion.skipdate = '';


            }
            /* else if (newValue == 'cc') {
                 $scope.NotSkipQuestion = true;
                 $scope.NotSubQuestion = true;
                 $scope.NotCountedCheckbox = false;
                 $scope.NotRange = true;
                 $scope.NotScheme = true;
             }  else if (newValue == 'rt') {
                 $scope.NotSkipQuestion = true;
                 $scope.NotSubQuestion = true;
                 $scope.NotCountedCheckbox = true;
                 $scope.NotRange = false;
                 $scope.NotScheme = true;
             } else if (newValue == 'scheme') {
                 $scope.NotSkipQuestion = true;
                 $scope.NotSubQuestion = true;
                 $scope.NotCountedCheckbox = true;
                 $scope.NotRange = true;
                 $scope.NotScheme = false;
             } else {
                 $scope.NotSubQuestion = true;
                 $scope.NotSkipQuestion = true;
                 $scope.NotCountedCheckbox = true;
                 $scope.NotRange = true;
                 $scope.NotScheme = true;
                 $scope.actionOn = false;
                 $scope.hideoption = false;
             }*/
        });

        $scope.$watch('surveyquestion.yesaction', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'popup') {
                $scope.YesPopup = false;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = false;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
                 $scope.YesTodo = true;
                $scope.YesMessage = true;
            }  else {
                $scope.YesPopup = true;
                $scope.YesNotSkipQuestion = true;
                $scope.YesIncrement = true;
                $scope.YesProvideInfo = true;
                 $scope.YesTodo = true;
                $scope.YesMessage = true;
            }
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);

        });
        $scope.$watch('surveyquestion.noaction', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'popup') {
                $scope.NoPopup = false;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'skp') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = false;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
                  $scope.YesTodo = true;
                $scope.YesMessage = true;
            } else if (newValue == 'increment') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonly') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'incrementonlyskip') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = false;
                $scope.NoProvideInfo = true;
            } else if (newValue == 'providenextquestion') {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = false;
            } else {
                $scope.NoPopup = true;
                $scope.NoNotSkipQuestion = true;
                $scope.NoIncrement = true;
                $scope.NoProvideInfo = true;
                 $scope.YesTodo = true;
                $scope.YesMessage = true;
            }
        });

        $scope.$watch('surveyquestion.skipdate', function (newValue, oldValue) {
            console.log('skipdate', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue.length == 1) {
                if (newValue[0] == 'yes') {
                    $scope.skipDate = false;
                    $scope.skipTypology = true;
                } else if (newValue[0] == 'yes1') {
                    $scope.skipTypology = false;
                    $scope.skipDate = true;
                }

            } else if (newValue.length == 2) {
                $scope.skipDate = false;
                $scope.skipTypology = false;
            } else {
                $scope.skipDate = true;
                $scope.skipTypology = true;
            }
        });

        $scope.$watch('surveyquestion.skipquestion', function (newValue, oldValue) {
            console.log('skipquestion', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == true) {
                $scope.skipQuestion = false;
            } else {
                $scope.skipQuestion = true;
            }
        });

        $scope.YesTodo = true;
        $scope.YesMessage = true;
        $scope.YesDocument = true;
        $scope.NoTodo = true;
        $scope.NoDocument = true;
        $scope.$watch('surveyquestion.yespopup', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'todos') {
                $scope.YesTodo = false;
                $scope.YesMessage = true;
                $scope.YesDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;

            } else if (newValue == 'applicationflow') {
                $scope.YesDocument = false;
                $scope.YesTodo = true;
            } else if (newValue == 'message') {
                $scope.YesMessage = false;
                $scope.YesTodo = true;
            } else {
                $scope.YesTodo = true;
                $scope.YesDocument = true;
                $scope.YesMessage = true;
            }
            // $scope.bool = $scope.original.yesdocumentflag;
            //console.log('$scope.bool', $scope.bool);
            // $scope.surveyquestion.yesdocumentflag = $scope.bool.toString();
            // console.log('$scope.surveyquestion.yesdocumentflag', $scope.surveyquestion.yesdocumentflag);
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);

        });

        $scope.$watch('surveyquestion.nopopup', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if (newValue == 'todos') {
                $scope.NoTodo = false;
                $scope.NoDocument = true;
            } else if (newValue == 'dateforid') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else if (newValue == 'applicationflow') {
                $scope.NoDocument = false;
                $scope.NoTodo = true;
            } else {
                $scope.NoTodo = true;
                $scope.NoDocument = true;
            }
            $scope.bool = $scope.original.nodocumentflag;
            $scope.surveyquestion.nodocumentflag = $scope.bool.toString();
            // console.log('$scope.surveyquestion.nodocumentflag', $scope.surveyquestion.nodocumentflag);
        });

        // $scope.yesdocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().$object;

        $scope.$watch('surveyquestion.yesdocumentflag', function (newValue, oldValue) {
            //console.log('oldValue', oldValue);
            console.log('newValue', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                if (newValue == 'true' || newValue == true) {
                    Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().then(function (response) {
                        $scope.yesdocumentsorschemes = response;
                        // $scope.surveyquestion = Restangular.copy($scope.original);
                        $scope.surveyquestion.yesdocumentid = $scope.original.yesdocumentid;
                    });;
                    // console.log('$scope.yesdocumentsorschemes', $scope.yesdocumentsorschemes);
                } else {
                    $scope.yesdocumentsorschemes = [{
                        id: 1,
                        name: 'One'
                    }, {
                        id: 2,
                        name: 'Two'
                    }, {
                        id: 3,
                        name: 'Three'
                    }, {
                        id: 4,
                        name: 'Four'
                        }, {
                        id: 5,
                        name: 'Five'
                    }];
                }
                //$scope.surveyquestion = Restangular.copy($scope.original);
                $scope.surveyquestion.yesdocumentid = $scope.original.yesdocumentid;
            }
            console.log('$scope.surveyquestion.yesdocumentid', $scope.surveyquestion.yesdocumentid);
        });

        $scope.$watch('surveyquestion.nodocumentflag', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                if (newValue == true || newValue == 'true') {
                    $scope.nodocumentsorschemes = Restangular.all('documenttypes?filter[where][deleteflag]=false').getList().$object;
                } else {
                    $scope.nodocumentsorschemes = [{
                        id: 1,
                        name: 'One'
                        }, {
                        id: 2,
                        name: 'Two'
                        }, {
                        id: 3,
                        name: 'Three'
                        }, {
                        id: 4,
                        name: 'Four'
                        }, {
                        id: 5,
                        name: 'Five'
                        }];
                }
            }
        });


        $scope.getSurveyType = function (surveytypeid) {
            return Restangular.one('surveytypes', surveytypeid).get().$object;
        };

        $scope.getPilllar = function (pillarid) {
            return Restangular.one('pillars', pillarid).get().$object;
        };
        /*********/

        //$scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;

        /* $scope.surveytypeid = '';
         $scope.surveysubcategoryid = '';
         $scope.subcategoryid = '';
         $scope.surveyid = '';
         $scope.surveyquestions = {};*/



        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }


        $scope.sub_surveyquestions = [];
        $scope.$watch('taskbreakup', function (newValue, oldValue) {
            if (newValue > 20) {
                $scope.taskbreakup = '';
                alert('Task Breakup Cannot Exceed 20');
            }
            if (newValue <= 20) {
                $scope.sub_surveyquestions = [];
                for (var i = 0; i < newValue; i++) {

                    $scope.sub_surveyquestions.push({
                        "question": null,
                        "gujrathi": null,
                        "surveytypeid": null,
                        "surveysubcategoryid": null,
                        "questiontype": null,
                        "noofdropdown": null,
                        "answers": null,
                        "questionid": null,
                        "questionno": null
                    });

                }

            }
        });


        $scope.validatestring = '';
        $scope.addRouteLinks = function () {


            document.getElementById('question').style.border = "";
            document.getElementById('noofoptions').style.border = "";
            document.getElementById('answeroptions').style.border = "";
            document.getElementById('questionno').style.border = "";
            document.getElementById('upper').style.border = "";



            if ($scope.surveyquestion.language == '' || $scope.surveyquestion.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a Language';
            } else if ($scope.surveyquestion.language == 1) {
                if ($scope.surveyquestion.serialno == '' || $scope.surveyquestion.serialno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Question Serial no';
                    document.getElementById('questionno').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == '' || $scope.surveyquestion.questiontype == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Question Type';
                } 
                else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } 
                 else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } 
                else if ($scope.surveyquestion.questiontype == 'nt') {
                  if ($scope.surveyquestion.range == '' || $scope.surveyquestion.range == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Upper Limit';
                    document.getElementById('upper').style.borderColor = "#FF0000";
                } 
                }
                else if ($scope.surveyquestion.questiontype == 'c' || $scope.surveyquestion.questiontype == 'rb') {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                }
                else if ($scope.surveyquestion.questiontype == 'r' && $scope.flag === true) {
                     if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    }
                    else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                    
                       else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on yes';
                        } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on No';
                        }
                    }



            } else if ($scope.surveyquestion.language != 1) {
                if ($scope.surveyquestion.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select question in English';

                } else if ($scope.surveyquestion.serialno == '' || $scope.surveyquestion.serialno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Question Serial no';
                    document.getElementById('questionno').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == '' || $scope.surveyquestion.questiontype == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Question Type';
                } 
                else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } 
               else if ($scope.surveyquestion.questiontype == 'nt') {
                  if ($scope.surveyquestion.range == '' || $scope.surveyquestion.range == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Upper Limit';
                    document.getElementById('upper').style.borderColor = "#FF0000";
                } 
                }
                else if ($scope.surveyquestion.questiontype == 'c' || $scope.surveyquestion.questiontype == 'rb') {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                }
                 else if ($scope.surveyquestion.questiontype == 'r' && $scope.flag === true) {
                     if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    }
                    else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                    
                       else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on yes';
                        } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on No';
                        }
                    }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.surveyquestion.parentId === '') {
                    delete $scope.surveyquestion['parentId'];
                }
                $scope.submitDisable = true;
                if ($scope.skipDate == false) {
                    $scope.surveyquestion.skipdate = true;
                }
                if ($scope.skipTypology == false) {
                    $scope.surveyquestion.skiponTypology = true;
                }
                //$scope.surveyquestion.skipdate = true;
                $scope.surveyquestion.parentFlag = $scope.showenglishLang;
                console.log('saved');

                var count = 0;
                if ($scope.sub_surveyquestions.length == 0) {
                    $scope.submitsurveyquestns.post($scope.surveyquestion).then(function (surveyresponse) {
                        console.log('main question saved', surveyresponse);

                        $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                        setTimeout(function () {
                            window.location = '/onetoonequestion';
                        }, 350);
                    }, function (error) {
                        $scope.submitDisable = false;
                        if (error.data.error.constraint === 'surveyquestion_lang_parenrid') {
                            alert('Value already exists for this language');

                        }
                    });
                } else {
                    $scope.submitsurveyquestns.post($scope.surveyquestion).then(function (surveyresponse) {
                        console.log('main question saved', surveyresponse);
                        for (var i = 0; i < $scope.sub_surveyquestions.length; i++) {
                            $scope.sub_surveyquestions[i].surveytypeid = surveyresponse.surveytypeid;
                            $scope.sub_surveyquestions[i].surveysubcategoryid = surveyresponse.surveysubcategoryid;
                            $scope.sub_surveyquestions[i].questionid = surveyresponse.id;
                            $scope.submitsurveyquestns.post($scope.sub_surveyquestions[i]).then(function (response) {
                                console.log('sub question saved', response);
                                count++;
                                if (count == $scope.sub_surveyquestions.length) {
                                    //$route.reload();
                                    window.location = "/onetoonequestion";
                                }
                            });
                        }
                    });
                }
            };
        };


        $scope.updateSurveyQuestion = function () {
           
            document.getElementById('question').style.border = "";
            document.getElementById('noofoptions').style.border = "";
            document.getElementById('answeroptions').style.border = "";
            document.getElementById('questionno').style.border = "";
            document.getElementById('upper').style.border = "";



            if ($scope.surveyquestion.language == '' || $scope.surveyquestion.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please select a Language';
            } else if ($scope.surveyquestion.language == 1) {
                if ($scope.surveyquestion.serialno == '' || $scope.surveyquestion.serialno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Question Serial no';
                    document.getElementById('questionno').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == '' || $scope.surveyquestion.questiontype == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Question Type';
                } 
                else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } 
                 else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } 
                else if ($scope.surveyquestion.questiontype == 'nt') {
                  if ($scope.surveyquestion.range == '' || $scope.surveyquestion.range == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Upper Limit';
                    document.getElementById('upper').style.borderColor = "#FF0000";
                } 
                }
                else if ($scope.surveyquestion.questiontype == 'c' || $scope.surveyquestion.questiontype == 'rb') {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                }
                 else if ($scope.surveyquestion.questiontype == 'r' && $scope.flag === true) {
                     if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    }
                    else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                    
                       else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on yes';
                        } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on No';
                        }
                    }



            } else if ($scope.surveyquestion.language != 1) {
                if ($scope.surveyquestion.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select question in English';

                } else if ($scope.surveyquestion.serialno == '' || $scope.surveyquestion.serialno == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Question Serial no';
                    document.getElementById('questionno').style.borderColor = "#FF0000";
                } else if ($scope.surveyquestion.questiontype == '' || $scope.surveyquestion.questiontype == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Question Type';
                } 
                else if ($scope.surveyquestion.question == '' || $scope.surveyquestion.question == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter question';
                    document.getElementById('question').style.borderColor = "#FF0000";
                } 
               else if ($scope.surveyquestion.questiontype == 'nt') {
                  if ($scope.surveyquestion.range == '' || $scope.surveyquestion.range == null) {
                    $scope.validatestring = $scope.validatestring + 'Please enter Upper Limit';
                    document.getElementById('upper').style.borderColor = "#FF0000";
                } 
                }
                else if ($scope.surveyquestion.questiontype == 'c' || $scope.surveyquestion.questiontype == 'rb') {
                    if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    } else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                }
                else if ($scope.surveyquestion.questiontype == 'r' && $scope.flag === true) {
                     if ($scope.surveyquestion.noofoptions == '' || $scope.surveyquestion.noofoptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter number of options';
                        document.getElementById('noofoptions').style.borderColor = "#FF0000";
                    }
                    else if ($scope.surveyquestion.answeroptions == '' || $scope.surveyquestion.answeroptions == null) {
                        $scope.validatestring = $scope.validatestring + 'Please enter answer options';
                    }
                    else if ($scope.surveyquestion.answeroptions.indexOf(',') == -1) {
                        $scope.validatestring = $scope.validatestring + 'Answer Options must consist of two words eg:Yes,No';
                    } 
                    
                       else if ($scope.surveyquestion.yesaction == '' || $scope.surveyquestion.yesaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on yes';
                        } else if ($scope.surveyquestion.noaction == '' || $scope.surveyquestion.noaction == null) {
                            $scope.validatestring = $scope.validatestring + 'Please select action on No';
                        }
                    }


            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                if ($scope.surveyquestion.parentId === '') {
                    delete $scope.surveyquestion['parentId'];
                }
                if ($scope.skipDate == false) {
                    $scope.surveyquestion.skipdate = true;
                }
                if ($scope.skipTypology == false) {
                    $scope.surveyquestion.skiponTypology = true;
                }
                $scope.submitDisable = true;
                $scope.surveyquestion.parentFlag = $scope.showenglishLang;

                var count = 0;

                $scope.submitsurveyquestns.customPUT($scope.surveyquestion).then(function (surveyresponse) {
                    console.log('main question saved', surveyresponse);

                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                    setTimeout(function () {
                        window.location = '/onetoonequestion';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'surveyquestion_lang_parenrid') {
                        alert('Value already exists for this language');

                    }
                });

            };
            //};
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        if ($routeParams.id) {
            $scope.message = 'Survey question has been Updated!';
            Restangular.one('surveyquestions', $routeParams.id).get().then(function (surveyquestion) {
                $scope.original = surveyquestion;
                $scope.questionValue = surveyquestion.serialno;
                $scope.surveyquestionansweroptions = surveyquestion.answeroptions;

                $scope.surveyquestion = Restangular.copy($scope.original);
                console.log('$scope.surveyquestion', $scope.surveyquestion);


                if ($scope.surveyquestion.skipdate == true && $scope.surveyquestion.skiponTypology == true) {
                    console.log('both true')
                    $scope.surveyquestion.skipdate = ['yes', 'yes1'];
                } else if ($scope.surveyquestion.skipdate == false && $scope.surveyquestion.skiponTypology == false) {
                    console.log('both false')
                    $scope.surveyquestion.skipdate = [];
                } else if ($scope.surveyquestion.skipdate == true && $scope.surveyquestion.skiponTypology == false) {
                    console.log('skipdate true')
                    $scope.surveyquestion.skipdate = ['yes'];
                } else if ($scope.surveyquestion.skipdate == false && $scope.surveyquestion.skiponTypology == true) {
                    console.log('skiponTypology true')
                    $scope.surveyquestion.skipdate = ['yes1'];
                }



            });
        } else {
            $scope.message = 'Survey question has been Created!';
        }
    });
