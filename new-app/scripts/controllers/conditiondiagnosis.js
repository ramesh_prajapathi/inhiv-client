'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionDiagnosisCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {

        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/conditiondiagnosis/create' || $location.path() === '/conditiondiagnosis/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/conditiondiagnosis/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/conditiondiagnosis/create' || $location.path() === '/conditiondiagnosis/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/conditiondiagnosis/create' || $location.path() === '/conditiondiagnosis/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/conditiondiagnosis-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        $scope.showenglishLang = true;
        $scope.OtherLang = true;

        $scope.$watch('conditiondiagnos.language', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;

                    Restangular.all('conditions?filter[where][deleteFlag]=false').getList().then(function (cns) {
                        $scope.conditions = cns;
                    });

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;

                    Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + newValue).getList().then(function (cns) {
                        $scope.conditions = cns;
                    });
                }
            }
        });

        $scope.$watch('conditiondiagnos.parentId', function (newValue, oldValue) {
            if (newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('conditiondiagnosis', newValue).get().then(function (sts) {
                    $scope.conditiondiagnos.orderNo = sts.orderNo;
                    $scope.conditiondiagnos.condition = sts.condition;
                    $scope.conditiondiagnos.metric = sts.metric;
                    // console.log($scope.conditiondiagnos);
                });
            }
        });

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Diagnosis has been Updated!';
            Restangular.one('conditiondiagnosis', $routeParams.id).get().then(function (conditiondiagnos) {
                $scope.original = conditiondiagnos;
                $scope.conditiondiagnos = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Diagnosis has been Created!';
        }

        $scope.Search = $scope.name;

        $scope.MeetingTodos = [];

        /******************************** INDEX *******************************************/
        Restangular.all('languages?filter[where][deleteFlag]=false').getList().then(function (zn) {
            $scope.cdlanguages = zn;
        });

        Restangular.all('conditiondiagnosis?filter[where][deleteFlag]=false').getList().then(function (cfs) {
            $scope.conditiondiagnosis = cfs;
            angular.forEach($scope.conditiondiagnosis, function (member, index) {
                member.index = index + 1;

                Restangular.one('languages', member.language).get().then(function (lng) {
                    Restangular.one('conditions', member.condition).get().then(function (cnts) {
                        member.langname = lng.name;
                        member.conditionName = cnts.name;
                    });
                });
            });
        });

        Restangular.all('conditiondiagnosis?filter[where][deleteFlag]=false&filter[where][language]=1').getList().then(function (cn) {
            $scope.englishconditiondiagnosis = cn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };
        /********************************************* SAVE *******************************************/
        $scope.conditiondiagnos = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteFlag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('metric').style.borderColor = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditiondiagnos.language == '' || $scope.conditiondiagnos.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.condition == '' || $scope.conditiondiagnos.condition == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.conditiondiagnos.name == '' || $scope.conditiondiagnos.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Diagnosis';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.metric == '' || $scope.conditiondiagnos.metric == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Metric';
                document.getElementById('metric').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.orderNo == '' || $scope.conditiondiagnos.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.conditiondiagnos.parentId === '') {
                    delete $scope.conditiondiagnos['parentId'];
                }
                $scope.submitDisable = true;
                $scope.conditiondiagnos.parentFlag = $scope.showenglishLang;
                $scope.conditiondiagnosis.post($scope.conditiondiagnos).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    window.location = '/conditiondiagnosis-list';
                }, function (error) {
                    if (error.data.error.constraint === 'conditiondiagnos_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('metric').style.borderColor = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.conditiondiagnos.language == '' || $scope.conditiondiagnos.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.condition == '' || $scope.conditiondiagnos.condition == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Condition';

            } else if ($scope.conditiondiagnos.name == '' || $scope.conditiondiagnos.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Step';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.metric == '' || $scope.conditiondiagnos.metric == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Metric';
                document.getElementById('metric').style.borderColor = "#FF0000";

            } else if ($scope.conditiondiagnos.orderNo == '' || $scope.conditiondiagnos.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                if ($scope.conditiondiagnos.parentId === '') {
                    delete $scope.conditiondiagnos['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('conditiondiagnosis', $routeParams.id).customPUT($scope.conditiondiagnos).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    // console.log('Step Saved');
                    window.location = '/conditiondiagnosis-list';
                }, function (error) {
                    if (error.data.error.constraint === 'conditiondiagnos_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

                var sort = $scope.sort;

                if (sort.active == column) {
                    return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
                }
            }
            /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteFlag: true
            }]
            Restangular.one('conditiondiagnosis/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });