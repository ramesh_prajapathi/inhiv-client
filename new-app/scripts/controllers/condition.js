'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {


        //$scope.OtherLang = false;
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/conditionlist/create' || $location.path() === '/conditionlist/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/conditionlist/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/conditionlist/create' || $location.path() === '/conditionlist/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/conditionlist/create' || $location.path() === '/conditionlist/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);

        if ($window.sessionStorage.prviousLocation != "partials/condition-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /***new changes*****/
        $scope.showenglishLang = true;
        //$scope.disableorder = false;


        $scope.$watch('condition.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {

                if (!$routeParams.id) {

                    $scope.condition.orderNo = '';
                    $scope.condition.name = '';
                    $scope.condition.parentId = '';
                }

                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                    $scope.disableorder = true;

                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                    $scope.disableorder = false;

                }


            }
        });

        $scope.$watch('condition.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                $scope.disableorder = true;
                Restangular.one('conditions', newValue).get().then(function (zn) {
                    $scope.condition.orderNo = zn.orderNo;
                    // $scope.condition.orderNo = '';
                });
            }
        });
        /********************************************************************/

        if ($routeParams.id) {
            //$scope.OtherLang = true;
            $scope.message = 'Condition has been Updated!';
            Restangular.one('conditions', $routeParams.id).get().then(function (condition) {
                $scope.original = condition;
                $scope.condition = Restangular.copy($scope.original);
                $scope.condiionoldvalue = $scope.condition.orderNo;
            });

            Restangular.all('conditions?filter[where][parentId]=' + $routeParams.id).getList().then(function (vitalResp) {
                $scope.AllvitalRecord = vitalResp;

            });
        } else {
            $scope.message = 'Condition has been Created!';
        }

        $scope.Search = $scope.name;
    
     //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];
        $scope.Displanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /******************************** INDEX *******************************************/
        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('conditions?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.conditions = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditions, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });
            
            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.condilanguages = sev;
            });

        } else {

            Restangular.all('conditions').getList().then(function (mt) {
                $scope.conditions = mt;
                 Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                      $scope.Displang = lang;
                angular.forEach($scope.conditions, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }
                      for (var a = 0; a < $scope.Displang.length; a++) {
                    if (member.language == $scope.Displang[a].id) {
                        member.langName = $scope.Displang[a].name;
                        break;
                    }
                }
                    member.index = index + 1;
                });
            });
            });


            Restangular.all('conditions?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condilanguages = sev;

                        angular.forEach($scope.condilanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.condilanguages = sev;
                    });
                }
            });
        }


        Restangular.all('conditions?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.conditiondisply = zn;
        });


        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        var timeoutPromise;
        var delayInMs = 1500;

        $scope.orderChange = function (order) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                var data = $scope.conditiondisply.filter(function (arr) {
                    return arr.orderNo == order
                })[0];

                if (data != undefined && $window.sessionStorage.language == 1 && $scope.condiionoldvalue != order && $scope.condition.language != '') {
                    $scope.condition.orderNo = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'Order No Already Exist';
                    // console.log('data', data);
                }
            }, delayInMs);
        };


        /********************************************* SAVE *******************************************/
        $scope.condition = {
            name: '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            deleteflag: false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.border = "";

            if ($scope.condition.language == '' || $scope.condition.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.condition.language == 1) {
                if ($scope.condition.name == '' || $scope.condition.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Condition';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.condition.orderNo == '' || $scope.condition.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }
            } else if ($scope.condition.language != 1) {
                if ($scope.condition.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition in English';

                } else if ($scope.condition.name == '' || $scope.condition.name == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Condition';
                    document.getElementById('name').style.borderColor = "#FF0000";

                } else if ($scope.condition.orderNo == '' || $scope.condition.orderNo == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                    document.getElementById('order').style.borderColor = "#FF0000";

                }
            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {


                if ($scope.condition.parentId === '') {
                    delete $scope.condition['parentId'];
                }
                $scope.submitDisable = true;
                $scope.condition.parentFlag = $scope.showenglishLang;
                $scope.conditions.post($scope.condition).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/condition-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'condition_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('order').style.borderColor = "";

            if ($scope.condition.language == '' || $scope.condition.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.condition.name == '' || $scope.condition.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Condition';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.condition.orderNo == '' || $scope.condition.orderNo == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Order';
                document.getElementById('order').style.borderColor = "#FF0000";

            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {


                if ($scope.condition.parentId === '') {
                    delete $scope.condition['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('conditions', $routeParams.id).customPUT($scope.condition).then(function (updateResp) {

                    console.log('condition Name Saved');

                    $scope.MandatoryUpdate(updateResp);

                }, function (error) {
                    if (error.data.error.constraint === 'condition_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            };
        };


        /**************************Updating all palce flag value**************************/

        $scope.upgateFlagCount = 0;
        $scope.updateallRecord = {};

        $scope.MandatoryUpdate = function (myResponse) {
            if ($scope.upgateFlagCount < $scope.AllvitalRecord.length) {

                $scope.updateallRecord.orderNo = myResponse.orderNo;
                Restangular.one('conditions', $scope.AllvitalRecord[$scope.upgateFlagCount].id).customPUT($scope.updateallRecord).then(function (childResp) {
                    console.log('childResp', childResp);
                    $scope.upgateFlagCount++;
                    $scope.MandatoryUpdate(myResponse);

                });
            } else {

                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                setTimeout(function () {
                    window.location = '/condition-list';
                }, 350);
            }
        };


        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('conditions/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('conditions/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };



    });
