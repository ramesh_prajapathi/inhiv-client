'use strict';

angular.module('secondarySalesApp')
    .controller('UsersCreateCtrl', function ($scope, Restangular, $http, $window, $route, $filter, AnalyticsRestangular, $timeout) {
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }
        $scope.heading = 'User Create';
        $scope.Saved = false;
        $scope.Updated = true;
        $scope.roleDisable = false;
        $scope.userDisable = false;
        $scope.userDisable1 = false;

        $scope.usernameDisable = false;
        $scope.disableButton = false;

        /*****hide and show Form ******/
        $scope.editShow = false;
        $scope.createShow = true;
        /*****hide and show Form ******/

        $scope.partner = {
            hdfflag: true,
            deleteflag: false,
            usercreated: false,
            latitude: '',
            longitude: '',
            address: '',
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.userId
        };


        $scope.user = {
            flag: true,
            email: null,
            zoneId: null,
            salesAreaId: null,
            coorgId: null,
            lastmodifiedtime: new Date(),
            lastmodifiedby: $window.sessionStorage.userId,
            deleteflag: false,
            status: 'active'
        };

        $scope.lan = Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lRas) {
            $scope.languages = lRas;
            $scope.user.language = 1;
        });

        $scope.roleDisplays = Restangular.all('roles?filter[where][deleteflag]=false' + '&filter[where][rolefalg]=A').getList().$object;

        /*  $scope.admins = Restangular.all('employees?filter[where][groupId]=1&filter[where][deleteflag]=false').getList().$object;


          Restangular.all('employees?filter[where][deleteflag]=false' + '&filter[where][usercreated]=false').getList().then(function (respuser) {
              $scope.userDisplays = respuser;
          });*/


        $scope.fieldworkers = Restangular.all('fieldworkers?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;


        $scope.comembers = Restangular.all('employees?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;

        $scope.ictccenterdisplay = Restangular.all('ictccenters?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;

        $scope.artcenterdisplay = Restangular.all('artcenters?filter[where][deleteflag]=false&filter[where][usercreated]=false').getList().$object;

        Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (responseCountry) {
            $scope.countries = responseCountry;
            $scope.user.countryId = $scope.user.countryId;
        });

        //        Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (responseZone) {
        //            $scope.zones = responseZone;
        //            $scope.user.state = $scope.user.state;
        //        });

        //        Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (responsesalesAreas) {
        //            $scope.salesareas = responsesalesAreas;
        //            $scope.user.district = $scope.user.district;
        //        });

        Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (responseTown) {
            $scope.cities = responseTown;
            $scope.user.town = $scope.user.town;
        });

        Restangular.all('ictccenters?filter[where][deleteflag]=false').getList().then(function (ictc) {
            $scope.ictccenters = ictc;
            $scope.user.ictcId = $scope.user.ictcId;
        });

        Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (art) {
            $scope.artcenters = art;
            $scope.user.artId = $scope.user.artId;
        });

        $scope.userDisplay = Restangular.all('users?filter[where][deleteflag]=false').getList().$object;
        /***************check username********/
        var timeoutPromise;
        var delayInMs = 1200;

        $scope.checkUserName = function (name) {

            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {


                var data = $scope.userDisplay.filter(function (arr) {
                    return arr.username == name
                })[0];
                if (data != undefined) {
                    $scope.user.username = '';
                    $scope.toggleValidation();
                    $scope.validatestring2 = 'Other user Created with these username';
                }
            }, delayInMs);
        };



        /************************************************** watch*******************************/
        $scope.showUniqueValidation = false;
        $scope.createduser = {
            "usercreated": true
        }



        $scope.$watch('user.countryId', function (newValue, oldValue) {
            // console.log('newValue', newValue);
            // console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                // $scope.user.state = '';
                // $scope.user.district = '';


                Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responseZone) {
                    $scope.zones = responseZone;
                    $scope.user.state = $scope.user.state;
                });
            }
        });

        $scope.$watch('user.state', function (newValue, oldValue) {
            //  console.log('newValue', newValue);
            //   console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                // $scope.city.district = '';


                Restangular.all('sales-areas?filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responsesalesAreas) {
                    $scope.salesareas = responsesalesAreas;
                    $scope.user.district = $scope.user.district;
                });
            }
        });


        /******************************************** WATCH *************************/

        $scope.$watch('user.usrName', function (newValue, oldValue) {


            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.newUser = JSON.parse(newValue);

                console.log('newuser', $scope.newUser);
                /*  if ($scope.user.roleId == 1 || $scope.user.roleId == 4) {
                      $scope.user.username = $scope.newUser.salesCode.toLowerCase();
                      // $scope.user.employeeid = $scope.newUser.id;
                     
                  }*/


                if ($scope.user.roleId == 2) {

                    $scope.useremployeeid = $scope.newUser.id;
                    $scope.user.countryId = $scope.newUser.countryId;
                    $scope.user.state = $scope.newUser.state;
                    $scope.user.district = $scope.newUser.district;
                    $scope.user.town = $scope.newUser.town;

                    Restangular.one('employees?filter[where][deleteflag]=false' + '&filter[where][id]=' + $scope.newUser.id).get().then(function (responseFacility) {
                        $scope.user.tiId = responseFacility[0].id;
                        $scope.user.coorgId = responseFacility[0].id;
                        $scope.user.ictcId = responseFacility[0].ictcId;
                        $scope.user.artId = responseFacility[0].artId;
                    });

                } else if ($scope.user.roleId == 11) {
                    $scope.user.tiId = '';
                    $scope.user.ictcId = $scope.newUser.id;
                     $scope.user.coorgId = $scope.newUser.id;
                    $scope.useremployeeid = $scope.newUser.id;
                    $scope.user.countryId = $scope.newUser.countryId;
                    $scope.user.state = $scope.newUser.state;
                    $scope.user.district = $scope.newUser.district;
                    $scope.user.town = $scope.newUser.town;



                } else if ($scope.user.roleId == 12) {
                    $scope.user.tiId = '';
                    $scope.useremployeeid = $scope.newUser.id;
                    $scope.user.coorgId = $scope.newUser.id;
                    $scope.user.artId = $scope.newUser.id;
                    $scope.user.countryId = $scope.newUser.countryId;
                    $scope.user.state = $scope.newUser.state;
                    $scope.user.district = $scope.newUser.district;
                    $scope.user.town = $scope.newUser.town;



                }
            }
        });

        $scope.hideCountry = true;
        $scope.hideState = true;
        $scope.hideDistrict = true;
        $scope.hideTown = true;
        $scope.hideTI = true;
        $scope.hideICTC = true;
        $scope.hideART = true;
        $scope.hideartdata = true;
        $scope.hideictcdata = true;

        $scope.$watch('user.roleId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '' || newValue == undefined) {
                return;
            } else {
                $scope.user.usrName = '';
                $scope.user.countryId = '';
                $scope.user.state = '';
                $scope.user.district = '';
                $scope.user.town = '';
                $scope.user.ictcId = '';
                $scope.user.artId = '';
                $scope.user.username = null;
                $scope.user.employeeid = null;
                if (newValue == 1) {

                    $scope.hideCountry = true;
                    $scope.hideState = true;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;

                } else if (newValue == 2) {
                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = false;
                    $scope.hideTI = false;
                    $scope.hideartdata = false;
                    $scope.hideictcdata = false;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.userDisable = true;
                    $scope.ItSelect ='Please select TI';
                } else if (newValue == 4) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 8) {

                    $scope.hideCountry = false;
                    $scope.hideState = true;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 9) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = true;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 10) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = true;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = false;

                } else if (newValue == 11) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = false;
                    $scope.hideTI = true;
                    $scope.hideICTC = false;
                    $scope.hideART = true;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = true;
                    $scope.ItSelect ='Please select ICTC';

                } else if (newValue == 12) {

                    $scope.hideCountry = false;
                    $scope.hideState = false;
                    $scope.hideDistrict = false;
                    $scope.hideTown = false;
                    $scope.hideTI = true;
                    $scope.hideICTC = true;
                    $scope.hideART = false;
                    $scope.hideartdata = true;
                    $scope.hideictcdata = true;
                    $scope.userDisable = true;
                    $scope.ItSelect ='Please select ART';

                }

            }
        });

        $(function () {
            var txt = $("input#userName");
            var func = function () {
                txt.val(txt.val().replace(/\s/g, ''));
            }
            txt.keyup(func).blur(func);
        });

        $('#userName').keypress(function (evt) {
            if (/^[a-zA-Z\d\-_.,\s]+$/.test(String.fromCharCode(evt.charCode)) == false) {
                evt.returnValue = false;
                return false;
            }
        });

        /**********************Save ********************************/
        $scope.validatestring = '';
        $scope.Save = function () {

            var regEmail = /\S+@\S+\.\S+/;

            document.getElementById('name').style.border = "";
            document.getElementById('mobile').style.border = "";
            document.getElementById('email').style.border = "";
            document.getElementById('userName').style.border = "";
            if ($scope.user.roleId == '' || $scope.user.roleId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Your Role';
            } else if ($scope.user.roleId == 1) {

                if ($scope.user.firstname == '' || $scope.user.firstname == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile.length != 10) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter valid Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.email == '' || $scope.user.email == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
                    document.getElementById('email').style.borderColor = "#FF0000";
                } else if (!regEmail.test($scope.user.email)) {
                    $scope.user.email = '';
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your valid Email Id';
                    document.getElementById('email').style.border = "1px solid #ff0000";
                } else if ($scope.user.username == '' || $scope.user.username == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter username';
                    document.getElementById('userName').style.borderColor = "#FF0000";

                } else if ($scope.user.password == '' || $scope.user.password == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
                    document.getElementById('password').style.borderColor = "#FF0000";
                } else if ($scope.user.language == '' || $scope.user.language == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Your Language';
                }
            } else if ($scope.user.roleId == 4 || $scope.user.roleId == 9) {

                if ($scope.user.firstname == '' || $scope.user.firstname == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile.length != 10) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter valid Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.email == '' || $scope.user.email == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
                    document.getElementById('email').style.borderColor = "#FF0000";
                } else if (!regEmail.test($scope.user.email)) {
                    $scope.user.email = '';
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your valid Email Id';
                    document.getElementById('email').style.border = "1px solid #ff0000";
                } else if ($scope.user.countryId == '' || $scope.user.countryId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Country';
                } else if ($scope.user.state == '' || $scope.user.state == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select State';
                } else if ($scope.user.username == '' || $scope.user.username == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter username';
                    document.getElementById('userName').style.borderColor = "#FF0000";

                } else if ($scope.user.password == '' || $scope.user.password == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
                    document.getElementById('password').style.borderColor = "#FF0000";
                } else if ($scope.user.language == '' || $scope.user.language == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Your Language';
                }
            } else if ($scope.user.roleId == 8) {

                if ($scope.user.firstname == '' || $scope.user.firstname == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile.length != 10) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter valid Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.email == '' || $scope.user.email == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
                    document.getElementById('email').style.borderColor = "#FF0000";
                } else if (!regEmail.test($scope.user.email)) {
                    $scope.user.email = '';
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your valid Email Id';
                    document.getElementById('email').style.border = "1px solid #ff0000";
                } else if ($scope.user.countryId == '' || $scope.user.countryId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Country';
                } else if ($scope.user.username == '' || $scope.user.username == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter username';
                    document.getElementById('userName').style.borderColor = "#FF0000";

                } else if ($scope.user.password == '' || $scope.user.password == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
                    document.getElementById('password').style.borderColor = "#FF0000";
                } else if ($scope.user.language == '' || $scope.user.language == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Your Language';
                }
            } else if ($scope.user.roleId == 10) {

                if ($scope.user.firstname == '' || $scope.user.firstname == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile.length != 10) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter valid Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.email == '' || $scope.user.email == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
                    document.getElementById('email').style.borderColor = "#FF0000";
                } else if (!regEmail.test($scope.user.email)) {
                    $scope.user.email = '';
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your valid Email Id';
                    document.getElementById('email').style.border = "1px solid #ff0000";
                } else if ($scope.user.countryId == '' || $scope.user.countryId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Country';
                } else if ($scope.user.state == '' || $scope.user.state == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select State';
                } else if ($scope.user.district == '' || $scope.user.district == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select District';
                } else if ($scope.user.username == '' || $scope.user.username == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter username';
                    document.getElementById('userName').style.borderColor = "#FF0000";

                } else if ($scope.user.password == '' || $scope.user.password == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
                    document.getElementById('password').style.borderColor = "#FF0000";
                } else if ($scope.user.language == '' || $scope.user.language == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Your Language';
                }
            } else if ($scope.user.roleId == 2 || $scope.user.roleId == 11 || $scope.user.roleId == 12) {

                if ($scope.user.usrName == '' || $scope.user.usrName == null) {
                    $scope.validatestring = $scope.validatestring + $scope.ItSelect; //'Please Select Your User';
                } else if ($scope.user.firstname == '' || $scope.user.firstname == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile == '' || $scope.user.mobile == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.mobile.length != 10) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter valid Mobile Number';
                    document.getElementById('mobile').style.borderColor = "#FF0000";
                } else if ($scope.user.email == '' || $scope.user.email == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Email Id';
                    document.getElementById('email').style.borderColor = "#FF0000";
                } else if (!regEmail.test($scope.user.email)) {
                    $scope.user.email = '';
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your valid Email Id';
                    document.getElementById('email').style.border = "1px solid #ff0000";
                } else if ($scope.user.username == '' || $scope.user.username == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter username';
                    document.getElementById('userName').style.borderColor = "#FF0000";

                } else if ($scope.user.password == '' || $scope.user.password == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Enter Your Password';
                    document.getElementById('password').style.borderColor = "#FF0000";
                } else if ($scope.user.language == '' || $scope.user.language == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Your Language';
                }
            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring2 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                
                  $scope.disableButton = true;
                /**********new changes****************/
                $scope.partner.roleId = $scope.user.roleId;
                $scope.partner.name = $scope.user.firstname;
                $scope.partner.helplineone = $scope.user.mobile;
                $scope.partner.email = $scope.user.email;
                $scope.partner.countryId = $scope.user.countryId;
                $scope.partner.state = $scope.user.state;
                $scope.partner.district = $scope.user.district;
                $scope.partner.town = $scope.user.town;
                $scope.partner.tiId = $scope.user.tiId;
                $scope.partner.ictcId = $scope.user.ictcId;
                $scope.partner.artId = $scope.user.artId;
                $scope.partner.profileFlag = 'A';

                Restangular.all('comembers').post($scope.partner).then(function (submitpartner) {

                    $scope.user.profileId = submitpartner.id;
                    $scope.user.employeeid = submitpartner.id;


                    $scope.users.post($scope.user).then(function (PostResponce) {
                            console.log('$scope.user', PostResponce);


                            $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                            $scope.submitDisable = true;
                            if ($scope.user.roleId == 2) {
                                Restangular.one('employees/' + $scope.useremployeeid).customPUT($scope.createduser).then(function () {
                                    window.location = '/users';
                                });
                            }

                           else if ($scope.user.roleId == 11) {
                                Restangular.one('ictccenters/' + $scope.useremployeeid).customPUT($scope.createduser).then(function () {
                                    window.location = '/users';
                                });
                            }

                           else if ($scope.user.roleId == 12) {
                                Restangular.one('artcenters/' + $scope.useremployeeid).customPUT($scope.createduser).then(function () {
                                    window.location = '/users';
                                });
                            } else {
                                window.location = '/users';
                            }

                        },
                        function (response) {
                            //alert(error.data.error.message);
                            $scope.showValidation = !$scope.showValidation;
                             $scope.disableButton = false;
                            $scope.validatestring1 = 'Email ID Already Exit';
                            
                        });

                });
            };
        };


        $scope.modalTitle = 'Thank You';
        $scope.message = 'User has been Created!';
        $scope.showValidation = false;
        /*$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};*/

        $scope.toggleValidation = function () {
            $scope.showUniqueValidation = !$scope.showUniqueValidation;
        };



        $scope.cofw = {
            data: 'co'
        };



        $scope.users = Restangular.all('users').getList().$object;
        $scope.groups = Restangular.all('groups').getList().$object;
        $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        $scope.distributionSubareas = Restangular.all('distribution-subareas').getList().$object;





    });
