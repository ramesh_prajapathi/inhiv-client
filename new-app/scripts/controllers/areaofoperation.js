'use strict';

angular.module('secondarySalesApp')
    .controller('AreaofOpeartionCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/areaofoperation/create' || $location.path() === '/areaofoperation/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/areaofoperation/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/areaofoperation/create' || $location.path() === '/areaofoperation/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/areaofoperation/create' || $location.path() === '/areaofoperation/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/areaofoperation") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        //  $scope.genders = Restangular.all('genders').getList().$object;

        if ($routeParams.id) {
            $scope.message = 'Area of Opeartion has been Updated!';
            Restangular.one('areaofoperations', $routeParams.id).get().then(function (areaofoperation) {
                $scope.original = areaofoperation;
                $scope.areaofoperation = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Area of Opeartion has been Created!';
        }
        $scope.Search = $scope.name;
        //$scope.submitareaofopeartions = Restangular.all('areaofopeartions');
        /******************************** INDEX *******************************************/
        $scope.zn = Restangular.all('areaofoperations?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.areaofoperations = zn;
            // console.log(' $scope.areaofoperations', $scope.areaofoperations);
            angular.forEach($scope.areaofoperations, function (member, index) {
                member.index = index + 1;
            });
        });

        /********************************************* SAVE *******************************************/
        $scope.areaofoperation = {
            "name": '',
            "deleteflag": false
        };
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.areaofoperation.name == '' || $scope.areaofoperation.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.hnname == '' || $scope.areaofoperation.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.knname == '' || $scope.areaofoperation.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.taname == '' || $scope.areaofoperation.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.tename == '' || $scope.areaofoperation.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.mrname == '' || $scope.areaofoperation.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.areaofoperations.post($scope.areaofoperation).then(function () {
                    window.location = '/areaofoperation';
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.areaofopeartion.name == '' || $scope.areaofopeartion.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.hnname == '' || $scope.areaofoperation.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.knname == '' || $scope.areaofoperation.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.taname == '' || $scope.areaofoperation.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.tename == '' || $scope.areaofoperation.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.areaofoperation.mrname == '' || $scope.areaofoperation.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter Area of Opeartion Name in Marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.submitDisable = true;
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                Restangular.one('areaofoperations', $routeParams.id).customPUT($scope.areaofoperation).then(function () {
                    console.log('Area of Opeartion Saved');
                    window.location = '/areaofoperation';
                });
            }
        };
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('areaofoperations/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
