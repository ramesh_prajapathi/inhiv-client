'use strict';
angular.module('secondarySalesApp').controller('RoutesCtrl', function ($scope, Restangular, $route, $window) {
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.someFocusVariable = true;
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.countryId = $window.sessionStorage.myRoute;
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        if ($window.sessionStorage.prviousLocation != "partials/routes" || $window.sessionStorage.prviousLocation != "partials/routes-form") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }
        //$scope.zones = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;

        //$scope.searchDA = $scope.name;


        $scope.search = '';
        $scope.startsWith = function (actual, expected) {
            // console.log("inside stratswith");
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        }

        $scope.showValidation = false;
        $scope.modalTitle = 'Alert';
        $scope.message = 'There are some assignments on this site,delete them and then proceed.';
        $scope.toggleValidation = function () {
            console.log('toggleValidation');
            $scope.showValidation = !$scope.showValidation;
        };


        $scope.znes = Restangular.all('countries?filter[where][deleteflag]=false').getList().then(function (znes) {
            $scope.countries = znes;
        });
        $scope.zoneDisplay = Restangular.all('zones?filter[where][deleteflag]=false').getList().$object;
        $scope.salesareaDispaly = Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().$object;
        $scope.cityDispaly = Restangular.all('cities?filter[where][deleteflag]=false').getList().$object;
        $scope.tisDispaly = Restangular.all('employees?filter[where][deleteflag]=false').getList().$object;

        Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {
            $scope.con = Restangular.all('distribution-routes?filter[where][deleteflag]=false').getList().then(function (con) {
                $scope.displayroutes = con;
                angular.forEach($scope.displayroutes, function (member, index) {

                    member.index = index + 1;
                    member.colour = "#29DF66";

                    if (member.deleteflag === true) {
                        member.colour = "#D53118";
                    }

                    var data14 = $scope.countries.filter(function (arr) {
                        return arr.id == member.countryId
                    })[0];

                    if (data14 != undefined) {
                        member.countryName = data14.name;
                    }
                    var data15 = $scope.zoneDisplay.filter(function (arr) {
                        return arr.id == member.state
                    })[0];

                    if (data15 != undefined) {
                        member.stateName = data15.name;
                    }

                    var data16 = $scope.salesareaDispaly.filter(function (arr) {
                        return arr.id == member.district
                    })[0];

                    if (data16 != undefined) {
                        member.districtName = data16.name;
                    }
                    var data17 = $scope.cityDispaly.filter(function (arr) {
                        return arr.id == member.town
                    })[0];

                    if (data17 != undefined) {
                        member.cityName = data17.name;
                    }

                    var data18 = $scope.tisDispaly.filter(function (arr) {
                        return arr.id == member.tiId
                    })[0];

                    if (data18 != undefined) {
                        member.tiName = data18.firstName;
                    }

                    for (var i = 0; i < routelinks.length; i++) {
                        if (routelinks[i].distributionRouteId == member.id) {
                            member.colour = "";
                            //member.totalamounts = $scope.FetchMember(member.id);
                            break;
                        } else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                            member.colour = "#F33226";
                            break;
                        }
                    }
                    member.index = index + 1;
                    //console.log('member', member);
                });
            });
        });
        /************************************* filter **************************************/

        $scope.CountryID = '';
        $scope.stateId = '';
        $scope.districtId = '';
        $scope.townId = '';
        $scope.TiId = '';

        $scope.$watch('CountryID', function (newValue, oldValue) {
            console.log('newValue', newValue);
            console.log('oldValue', oldValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                $scope.stateId = '';
        $scope.districtId = '';
        $scope.townId = '';
        $scope.TiId = '';


                Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceSt) {
                    $scope.dispalyZones = responceSt;

                });

                Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {
                    Restangular.all('distribution-routes?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.displayroutes = con;
                        angular.forEach($scope.displayroutes, function (member, index) {

                            member.index = index + 1;

                            member.colour = "#29DF66";
                            for (var i = 0; i < routelinks.length; i++) {
                                if (routelinks[i].distributionRouteId == member.id) {
                                    member.colour = "";
                                    //member.totalamounts = $scope.FetchMember(member.id);
                                    break;
                                } else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                    member.colour = "#F33226";
                                    break;
                                }
                            }

                            var data14 = $scope.countries.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data14 != undefined) {
                                member.countryName = data14.name;
                            }
                            var data15 = $scope.zoneDisplay.filter(function (arr) {
                                return arr.id == member.state
                            })[0];

                            if (data15 != undefined) {
                                member.stateName = data15.name;
                            }

                            var data16 = $scope.salesareaDispaly.filter(function (arr) {
                                return arr.id == member.district
                            })[0];

                            if (data16 != undefined) {
                                member.districtName = data16.name;
                            }
                            var data17 = $scope.cityDispaly.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data17 != undefined) {
                                member.cityName = data17.name;
                            }

                            var data18 = $scope.tisDispaly.filter(function (arr) {
                                return arr.id == member.tiId
                            })[0];

                            if (data18 != undefined) {
                                member.tiName = data18.firstName;
                            }
                           // console.log('member', member);
                        });
                    });
                });
                $scope.countiesid = +newValue;
            }
        });

        $scope.$watch('stateId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                 $scope.districtId = '';
        $scope.townId = '';
        $scope.TiId = '';

                Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceState) {
                    $scope.displaySalesareas = responceState;

                });
                Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {

                    Restangular.all('distribution-routes?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.displayroutes = con;
                        angular.forEach($scope.displayroutes, function (member, index) {

                            member.index = index + 1;

                            member.colour = "#29DF66";
                            for (var i = 0; i < routelinks.length; i++) {
                                if (routelinks[i].distributionRouteId == member.id) {
                                    member.colour = "";
                                    //member.totalamounts = $scope.FetchMember(member.id);
                                    break;
                                } else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                    member.colour = "#F33226";
                                    break;
                                }
                            }

                            var data14 = $scope.countries.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data14 != undefined) {
                                member.countryName = data14.name;
                            }
                            var data15 = $scope.zoneDisplay.filter(function (arr) {
                                return arr.id == member.state
                            })[0];

                            if (data15 != undefined) {
                                member.stateName = data15.name;
                            }

                            var data16 = $scope.salesareaDispaly.filter(function (arr) {
                                return arr.id == member.district
                            })[0];

                            if (data16 != undefined) {
                                member.districtName = data16.name;
                            }
                            var data17 = $scope.cityDispaly.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data17 != undefined) {
                                member.cityName = data17.name;
                            }

                            var data18 = $scope.tisDispaly.filter(function (arr) {
                                return arr.id == member.tiId
                            })[0];

                            if (data18 != undefined) {
                                member.tiName = data18.firstName;
                            }
                          //  console.log('member', member);
                        });
                    });
                });

                $scope.stateid = +newValue;
            }
        });

        $scope.$watch('districtId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {
                 $scope.townId = '';
        $scope.TiId = '';

                Restangular.all('cities?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceCity) {
                    $scope.displayCities = responceCity;

                });

                Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {

                    Restangular.all('distribution-routes?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (con) {
                        $scope.displayroutes = con;
                        angular.forEach($scope.displayroutes, function (member, index) {

                            member.index = index + 1;

                            member.colour = "#29DF66";
                            for (var i = 0; i < routelinks.length; i++) {
                                if (routelinks[i].distributionRouteId == member.id) {
                                    member.colour = "";
                                    //member.totalamounts = $scope.FetchMember(member.id);
                                    break;
                                } else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                    member.colour = "#F33226";
                                    break;
                                }
                            }

                            var data14 = $scope.countries.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data14 != undefined) {
                                member.countryName = data14.name;
                            }
                            var data15 = $scope.zoneDisplay.filter(function (arr) {
                                return arr.id == member.state
                            })[0];

                            if (data15 != undefined) {
                                member.stateName = data15.name;
                            }

                            var data16 = $scope.salesareaDispaly.filter(function (arr) {
                                return arr.id == member.district
                            })[0];

                            if (data16 != undefined) {
                                member.districtName = data16.name;
                            }
                            var data17 = $scope.cityDispaly.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data17 != undefined) {
                                member.cityName = data17.name;
                            }

                            var data18 = $scope.tisDispaly.filter(function (arr) {
                                return arr.id == member.tiId
                            })[0];

                            if (data18 != undefined) {
                                member.tiName = data18.firstName;
                            }
                            //console.log('member', member);
                        });
                    });
                });

                $scope.districtid = +newValue;
            }
        });
        $scope.$watch('townId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                  $scope.TiId = '';
                Restangular.all('employees?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (responceti) {
                    $scope.empDispaly = responceti;

                });

                Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {

                    Restangular.all('distribution-routes?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
                        $scope.displayroutes = ctyRes1;
                        angular.forEach($scope.displayroutes, function (member, index) {

                            member.index = index + 1;

                            member.colour = "#29DF66";
                            for (var i = 0; i < routelinks.length; i++) {
                                if (routelinks[i].distributionRouteId == member.id) {
                                    member.colour = "";
                                    //member.totalamounts = $scope.FetchMember(member.id);
                                    break;
                                } else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                    member.colour = "#F33226";
                                    break;
                                }
                            }

                            var data14 = $scope.countries.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data14 != undefined) {
                                member.countryName = data14.name;
                            }
                            var data15 = $scope.zoneDisplay.filter(function (arr) {
                                return arr.id == member.state
                            })[0];

                            if (data15 != undefined) {
                                member.stateName = data15.name;
                            }

                            var data16 = $scope.salesareaDispaly.filter(function (arr) {
                                return arr.id == member.district
                            })[0];

                            if (data16 != undefined) {
                                member.districtName = data16.name;
                            }
                            var data17 = $scope.cityDispaly.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data17 != undefined) {
                                member.cityName = data17.name;
                            }

                            var data18 = $scope.tisDispaly.filter(function (arr) {
                                return arr.id == member.tiId
                            })[0];

                            if (data18 != undefined) {
                                member.tiName = data18.firstName;
                            }
                            //console.log('member', member);
                        });
                    });
                });

                $scope.townid = +newValue;

            }
        });

        $scope.$watch('TiId', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else {

                Restangular.all('routelinks?filter[where][deleteflag]=false').getList().then(function (routelinks) {

                    Restangular.all('distribution-routes?filter[where][countryId]=' + $scope.countiesid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + $scope.districtid + '&filter[where][town]=' + $scope.townid + '&filter[where][tiId]=' + newValue + '&filter[where][deleteflag]=false').getList().then(function (ctyRes1) {
                        $scope.displayroutes = ctyRes1;
                        angular.forEach($scope.displayroutes, function (member, index) {

                            member.index = index + 1;

                            member.colour = "#29DF66";
                            for (var i = 0; i < routelinks.length; i++) {
                                if (routelinks[i].distributionRouteId == member.id) {
                                    member.colour = "";
                                    //member.totalamounts = $scope.FetchMember(member.id);
                                    break;
                                } else if (routelinks[i].distributionRouteId != member.id && member.deleteflag == true) {
                                    member.colour = "#F33226";
                                    break;
                                }
                            }

                            var data14 = $scope.countries.filter(function (arr) {
                                return arr.id == member.countryId
                            })[0];

                            if (data14 != undefined) {
                                member.countryName = data14.name;
                            }
                            var data15 = $scope.zoneDisplay.filter(function (arr) {
                                return arr.id == member.state
                            })[0];

                            if (data15 != undefined) {
                                member.stateName = data15.name;
                            }

                            var data16 = $scope.salesareaDispaly.filter(function (arr) {
                                return arr.id == member.district
                            })[0];

                            if (data16 != undefined) {
                                member.districtName = data16.name;
                            }
                            var data17 = $scope.cityDispaly.filter(function (arr) {
                                return arr.id == member.town
                            })[0];

                            if (data17 != undefined) {
                                member.cityName = data17.name;
                            }

                            var data18 = $scope.tisDispaly.filter(function (arr) {
                                return arr.id == member.tiId
                            })[0];

                            if (data18 != undefined) {
                                member.tiName = data18.name;
                            }
                            //console.log('member', member);
                        });
                    });
                });

            }
        });

        /************************************* DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.all('routelinks?filter[where][distributionRouteId]=' + id).getList().then(function (rtelnks) {
                if (rtelnks.length == 0) {
                    Restangular.one('distribution-routes/' + id).customPUT($scope.item[0]).then(function () {
                        $route.reload();
                    });
                } else {
                    $scope.toggleValidation();
                    $scope.validatestring1 = "There are some assignments on this site,delete them and then proceed.";
                }
            });
        }
        $scope.UnDelete = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('distribution-routes/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        /*************************** Watch **************************************************/
        /*$scope.Member = function (siteId) {
        	Restangular.all('beneficiaries?filter[where][site]=' + siteId + '&filter[where][limit]=10').then(function (response) {
        		console.log('MemberLength', response.length);
        	});
        };
        $scope.Member = Restangular.all('beneficiaries??filter[where][site]=' + siteId + '&filter[where][limit]=10' + '&filter[where][deleteflag]=false').getList().then(function (me) {
        	$scope.members = me;
        	//   $scope.countryId = $window.sessionStorage.site_zoneId;
        });*/
        $scope.Member = function (siteId) {
            console.log('siteId', siteId);
            /* Restangular.one('beneficiaries??filter[where][site]=siteId').get().then(function(Res){
            	 console.log('Res',Res);
             });*/
            /*Restangular.all('beneficiaries?filter[where][site]=siteId' + '&filter[where][limit]=10' + '&filter[where][deleteflag]=false').getList().then(function(Res){
            	 console.log('Res',Res);
             });*/
        };
        /*$scope.MemberCount = function (benId) {
        	console.log('conId', benId);
        	Restangular.one('beneficiaries?filter[where][site]=' + benId + '&filter[where][deleteflag]=false' + '&filter[limit]=100').get().then(function (todores) {
        		$scope.alltodo = todores;
        		console.log('todores', todores[0]);
        	});
        };*/
        /*$scope.FetchMember = function (benId) {
        	//console.log('I m calling by', benId5);
        	$scope.deleteCount51 = 0;
        	Restangular.all('todos?filter[where][facility]=' + benId5 + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (todores) {
        		$scope.alltodo = todores;
        		if (todores.length > 0) {
        			$scope.mainTodosToDelete = todores;
        			$scope.deleteMainTodos(benId5);
        			//$scope.Fetchtodo(benId5);
        		} else {
        			$scope.FetchreportIncidents(benId5);

        		}
        	});
        }*/
        $scope.FetchMember = function (siteId) {
            //console.log('siteId', siteId);
            Restangular.all('beneficiaries?filter[where][site]=59' + '&filter[where][deleteflag]=false' + '&filter[limit]=1000').getList().then(function (memberres) {
                $scope.allmember = memberres;
                console.log('allmember', memberres.length);
            });
        };

        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }

        /***************************************************/
        /*$scope.FetchMember = function (benId) {
                return Restangular.all('beneficiaries?filter[where][site]=' + benId + '&filter[where][deleteflag]=false').getList().$object;
              }*/
        /***************************************************/
        //--------------------------------------------------------------------------    

    })
    .filter('startsWith', function () {
        return function (array, search) {
            var matches = [];
            for (var i = 0; i < array.length; i++) {
                if (array[i].indexOf(search) === 0 &&
                    search.length < array[i].length) {
                    matches.push(array[i]);
                }
            }
            return matches;
        };
    });
