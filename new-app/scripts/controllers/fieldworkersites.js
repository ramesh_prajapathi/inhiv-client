'use strict';

angular.module('secondarySalesApp')
	.controller('FWSitesCtrl', function ($scope, Restangular, $route, $window) {

		//$scope.partners = Restangular.all('partners?filter[where][groupId]=8').getList().$object;

				
		console.log('$window.sessionStorage.State', $window.sessionStorage.zoneId);
		console.log('$window.sessionStorage.Distric', $window.sessionStorage.salesAreaId);
		console.log('$window.sessionStorage.Facility', $window.sessionStorage.coorgId);
		console.log('$window.sessionStorage.LastModifyBy', $window.sessionStorage.UserEmployeeId);
		console.log('$window.sessionStorage.LastmofyTime', $window.sessionStorage.zoneId);
	
	
		$scope.getZone = function (zoneId) {
			return Restangular.one('zones', zoneId).get().$object;
		};

		$scope.getSalesArea = function (salesAreaId) {
			return Restangular.one('sales-areas', salesAreaId).get().$object;
		};

		$scope.getPartner = function (partnerId) {
			return Restangular.one('comembers', partnerId).get().$object;
		};

		$scope.getDistributionSubarea = function (distributionSubareaId) {
			return Restangular.one('distribution-subareas', distributionSubareaId).get().$object;
		};

		$scope.zones = Restangular.all('zones').getList().$object;


		$scope.searchDA = $scope.name;
		/************************************************************************* INDEX **********************************/

		$scope.searchCon = $scope.name;
		console.log('$window.sessionStorage.coorgId', $window.sessionStorage.coorgId);


		$scope.part = Restangular.all('distribution-routes?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][deleteflag]=null').getList().then(function (part) {
			$scope.routes = part;
			angular.forEach($scope.routes, function (member, index) {
				member.index = index + 1;
			});
		});
/*
		$scope.part = Restangular.all('distribution-routes?filter[where][zoneId]=' + $window.sessionStorage.zoneId + '&filter[where][salesAreaId]=' + $window.sessionStorage.salesAreaId + '&filter[where][lastmodifiedby]=' + $window.sessionStorage.UserEmployeeId + '&filter[where][deleteflag]=null').getList().then(function (part) {
			$scope.routes = part;
			angular.forEach($scope.routes, function (member, index) {
				member.index = index + 1;
			});
		});
*/

		/************************************* DELETE *******************************************/
		$scope.Delete = function (id) {
				if (confirm("Are You Sure Want to Archive Member..!") == true) {
					$scope.item = [{
						deleteflag: 'yes'
            }]
					Restangular.one('distribution-routes/' + id).customPUT($scope.item[0]).then(function () {
						$route.reload();
					});

				} else {

				}

			}
			/*************************** Watch **************************************************/
		$scope.zoneId = '';
		$scope.salesareaId = '';
		$scope.salesid = '';
		$scope.partnerId;
		$scope.partid = '';
		$scope.zonalid = '';
		$scope.distributionSubareaId = '';
		$scope.firstName = '';

		$scope.$watch('zoneId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.salesareas = Restangular.all('sales-areas?filter[where][zoneId]=' + newValue).getList().$object;

				// $scope.routes = Restangular.all('distribution-routes?filter[where][zoneId]='+newValue).getList().$object;

				$scope.con = Restangular.all('distribution-routes?filter[where][zoneId]=' + newValue).getList().then(function (con) {
					$scope.routes = con;
					//console.log('CON2',con);
					angular.forEach($scope.routes, function (member, index) {
						member.index = index + 1;
					});
				});

				$scope.zonalid = +newValue;
			}

		});


		//-------------------------------------------------------------------------------------------------------------------------    


		$scope.$watch('salesAreaId', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				$scope.partners = Restangular.all('comembers').getList().$object;

				$scope.Rt = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + newValue).getList().then(function (Rt) {
					$scope.routes = Rt;
					angular.forEach($scope.routes, function (member, index) {
						member.index = index + 1;
					});
				});
				$scope.salesid = +newValue;
			}

		});

		$scope.$watch('firstName', function (newValue, oldValue) {
			if (newValue === oldValue || newValue == '') {
				return;
			} else {
				//$scope.partners = Restangular.all('partners?filter[where][partnerId]='+newValue).getList().$object;

				$scope.Rt = Restangular.all('distribution-routes?filter[where][zoneId]=' + $scope.zonalid + '&filter[where][salesAreaId]=' + $scope.salesid + '&filter[where][partnerId]=' + newValue).getList().then(function (Rt) {
					$scope.routes = Rt;
					angular.forEach($scope.routes, function (member, index) {
						member.index = index + 1;
					});
				});

				$scope.partid = +newValue;
			}
		});



	});
