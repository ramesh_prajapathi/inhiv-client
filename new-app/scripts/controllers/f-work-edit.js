'use strict';
angular.module('secondarySalesApp').controller('FWEditCtrl', function ($scope, Restangular, $routeParams, $window, $modal, AnalyticsRestangular, $filter) {
    //    if ($window.sessionStorage.roleId != 2 && $window.sessionStorage.roleId != 6) {
    //        window.location = "/";
    //    }
    $scope.isCreateView = false;
    $scope.heading = 'Field Worker Edit';
    $scope.HideUsername = true;
    $scope.tabtwohide = true;
    $scope.hideSitesAssign = false;
    $scope.createDisabled = false;
    $scope.UpdateClicked = false;
    $scope.fieldworker = {
        address: '',
        lastmodifiedtime: new Date(),
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        facility: $window.sessionStorage.coorgId,
        deleteflag: false
    };
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.userId,
        lastmodifiedtime: new Date(),
        entityroleid: 51,
        countryId: $window.sessionStorage.countryId,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        town: $window.sessionStorage.distributionAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.userId,
        description: 'ORW Updated'
    };
    $scope.user = {
        flag: true,
        lastmodifiedtime: new Date(),
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        deleteflag: false,
        status: 'active',
        countryId: $window.sessionStorage.countryId,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        town: $window.sessionStorage.distributionAreaId,
        tiId: $window.sessionStorage.coorgId
    };

    //new changes
    Restangular.one('orwlanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.orwlanguages = langResponse[0];
        //$scope.modalInstanceLoad.close();
        $scope.TIHeading = $scope.orwlanguages.fieldworkeredit;
        // $scope.modalTitle = $scope.RILanguage.thankYou;
        $scope.message = $scope.orwlanguages.hasbeenupdated;

    });

    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
        $scope.languages = lang;
         $scope.fieldworker.language =  $scope.fieldworker.language;
         $scope.user.language =  $scope.fieldworker.language;
    });

    if ($window.sessionStorage.language == 1) {
        $scope.modalTitle = 'Thank You';
    } else if ($window.sessionStorage.language == 2) {
        $scope.modalTitle = 'धन्यवाद';
    } else if ($window.sessionStorage.language == 3) {
        $scope.modalTitle = 'ಧನ್ಯವಾದ';
    } else if ($window.sessionStorage.language == 4) {
        $scope.modalTitle = 'நன்றி';
    }



    Restangular.one('comembers', $routeParams.id).get().then(function (partner) {
        $scope.original = partner;
        $scope.fieldworker = Restangular.copy($scope.original);
        Restangular.one('zones', partner.state).get().then(function (zone) {
            $scope.zoneName = zone.name;
        });
        Restangular.one('sales-areas', partner.district).get().then(function (salesarea) {
            $scope.salesareaName = salesarea.name;
        });
        Restangular.one('users', partner.id).get().then(function (userResp) {
           $scope.fieldworker.language = userResp.language;
        });
        Restangular.one('comembers', partner.facilityId).get().then(function (partner) {
            $scope.coorgName = partner.name;
            $scope.fieldworker.facilityId = partner.id;
            $scope.auditlog.facilityId = partner.id;
            $scope.fieldworker.countryId = partner.countryId;
            $scope.fieldworker.town = partner.town;
        });
        Restangular.all('distribution-routes?filter={"where":{"id":{"inq":[' + partner.sitesassigned + ']}}}').getList().then(function (assignedsites) {
            for (var i = 0; i < assignedsites.length; i++) {
                if (i == 0) {
                    $scope.AssignedSites = assignedsites[i].id + "-" + assignedsites[i].name;
                } else {
                    $scope.AssignedSites = $scope.AssignedSites + "," + assignedsites[i].id + "-" + assignedsites[i].name;
                }
            }
        });
        $scope.modalInstanceLoad.close();
    });
    /*********************************************** Update *******************************************/

    $scope.validatestring = '';
    $scope.UpdateComember = {
        fwcount: 0
    };
    $scope.dataModal = false;
    Restangular.all('users?filter[where][employeeid]=' + $routeParams.id).getList().then(function (submituser) {
        console.log('submituser', submituser);
        $scope.getUserId = submituser[0].id;
        $scope.fieldworker.language = submituser[0].language;
    });
    $scope.Update = function () {
        if ($scope.fieldworker.firstname == '' || $scope.fieldworker.firstname == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterfullname;
        } else if ($scope.fieldworker.lastname == '' || $scope.fieldworker.lastname == null) {
            $scope.validatestring = $scope.validatestring + $scope.enterfullname;
        } else if ($scope.fieldworker.mobile == '' || $scope.fieldworker.mobile == null) {
            $scope.validatestring = $scope.validatestring + $scope.entermobile;
        } else if ($scope.fieldworker.email == '' || $scope.fieldworker.email == null) {
            $scope.validatestring = $scope.validatestring + $scope.enteremail;
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
            //$scope.dataModal = !$scope.dataModal;
        } else {
            $scope.UpdateClicked = false;
            $scope.createDisabled = true;
            Restangular.one('languages', $scope.fieldworker.language).get().then(function (lang) {
                $scope.languageName = lang.name;
                $scope.dataModal = !$scope.dataModal;
            });
        }
    };
 $scope.disaveSaved = false;
    $scope.OK = function () {
        
        $scope.disaveSaved = true;

        Restangular.one('comembers', $routeParams.id).customPUT($scope.fieldworker).then(function (submitfw) {
            $scope.auditlog.entityid = submitfw.id;
            $scope.user.username = submitfw.firstname;
            $scope.user.mobile = submitfw.mobile;
            $scope.user.employeeid = submitfw.id;
            $scope.user.email = submitfw.email;
            $scope.user.profileId = submitfw.id;
            $scope.user.roleId = submitfw.roleId;
            // var respdate = $filter('date')(submitfw.lastmodifiedtime, 'dd-MMMM-yyyy');
            // $scope.auditlog.description = 'Field Worker Updated With Following Details: ' + 'User Name - ' + submitfw.firstname + ' , ' + 'Full Name - ' + submitfw.lastname + ' , ' + 'FW Code - ' + submitfw.fwcode + ' , ' + 'Email - ' + submitfw.email + ' , ' + 'Mobile - ' + submitfw.mobile + 'Address - ' + submitfw.address + ' , ' + 'latitude - ' + submitfw.latitude + ' , ' + 'Language - ' + submitfw.language + ' , ' + 'Last Modied - ' + respdate;
            Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                $scope.UpdateComember.fwcount = +($scope.fieldworker.fwcode) + 1;
                Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).customPUT($scope.UpdateComember).then(function (comember) {});
                Restangular.one('users/' + $scope.getUserId).customPUT($scope.user).then(function (submituser) {
                    //console.log('submituser', submituser);
                    // AnalyticsRestangular.one('users/' + $scope.getUserId).customPUT($scope.user).then(function (analyticssubResponse) {

                    //});

                    $scope.dataModal = !$scope.dataModal;

                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('Medicine Name Saved');
                    setTimeout(function () {
                        window.location = '/fieldworkers';
                    }, 350);
                });
            });

        }, function (error) {
            $scope.UpdateClicked = false;
            //console.log('fw error', error);
            if (error.data.error.constraint == 'unique_firstname') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.usernameexits; //"Username already exits please refresh the page and try again";
            } else if (error.data.error.constraint == 'unique_email') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.emailexit; //"email id already exits";
            }
        });



    };


    $scope.cancelSave = function () {
        $scope.createDisabled = false;
         $scope.disaveSaved = false;
        $scope.dataModal = !$scope.dataModal;
    };
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    ///////////////////////////////////////////////////////MAP//////////////////////////
    $scope.showMapModal = false;
    $scope.toggleMapModal = function () {
        $scope.mapcount = 0;
        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {
            $scope.fieldworker.latitude = latLng.lat() + ',' + latLng.lng();
            $scope.fieldworker.longitude = latLng.lng();
            document.getElementById('info').innerHTML = [
    latLng.lat()
    , latLng.lng()
  ].join(', ');
        }

        function updateMarkerAddress(str) {
            document.getElementById('mapaddress').innerHTML = str;
        }
        var map;

        function initialize() {
            $scope.address = $scope.fieldworker.address;
            // console.log('$scope.address', $scope.address);
            $scope.latitude = 21.0000;
            $scope.longitude = 78.0000;
            if ($scope.address.length > 0) {
                var addressgeocoder = new google.maps.Geocoder();
                addressgeocoder.geocode({
                    'address': $scope.address
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $scope.latitude = parseInt(results[0].geometry.location.lat());
                        $scope.longitude = parseInt(results[0].geometry.location.lng());
                        //console.log($scope.latitude, $scope.longitude);
                        var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                        map = new google.maps.Map(document.getElementById('mapCanvas'), {
                            zoom: 4,
                            center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        var marker = new google.maps.Marker({
                            position: latLng,
                            title: 'Point A',
                            map: map,
                            draggable: true
                        });
                        // Update current position info.
                        updateMarkerPosition(latLng);
                        geocodePosition(latLng);
                        // Add dragging event listeners.
                        google.maps.event.addListener(marker, 'dragstart', function () {
                            updateMarkerAddress('Dragging...');
                        });
                        google.maps.event.addListener(marker, 'drag', function () {
                            updateMarkerStatus('Dragging...');
                            updateMarkerPosition(marker.getPosition());
                        });
                        google.maps.event.addListener(marker, 'dragend', function () {
                            updateMarkerStatus('Drag ended');
                            geocodePosition(marker.getPosition());
                        });
                    }
                });
            } else {
                $scope.latitude = 21.0000;
                $scope.longitude = 78.0000;
                var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                map = new google.maps.Map(document.getElementById('mapCanvas'), {
                    zoom: 4,
                    center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var marker = new google.maps.Marker({
                    position: latLng,
                    title: 'Point A',
                    map: map,
                    draggable: true
                });
                // Update current position info.
                updateMarkerPosition(latLng);
                geocodePosition(latLng);
                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'dragstart', function () {
                    updateMarkerAddress('Dragging...');
                });
                google.maps.event.addListener(marker, 'drag', function () {
                    updateMarkerStatus('Dragging...');
                    updateMarkerPosition(marker.getPosition());
                });
                google.maps.event.addListener(marker, 'dragend', function () {
                    updateMarkerStatus('Drag ended');
                    geocodePosition(marker.getPosition());
                });
            }
        }
        // Onload handler to fire off the app.
        //google.maps.event.addDomListener(window, 'load', initialize);
        initialize();
        window.setTimeout(function () {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
            map.setZoom(4);
        }, 1000);
        $scope.SaveMap = function () {
            $scope.showMapModal = !$scope.showMapModal;
        };
        $scope.showMapModal = !$scope.showMapModal;
    };
    $scope.CancelMap = function () {
        if ($scope.mapcount == 0) {
            $scope.showMapModal = !$scope.showMapModal;
            $scope.mapcount++;
        }
    };
    /****************************** Language *********************************/
    /* $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.fieldworkercreate = langResponse.fieldworkercreate;
        $scope.divbasicinfo = langResponse.divbasicinfo;
        $scope.divuserinfo = langResponse.divuserinfo;
        $scope.printfirstname = langResponse.firstname;
        $scope.printlastname = langResponse.lastname;
        $scope.printfwcode = langResponse.fwcode;
        $scope.printemail = langResponse.email;
        $scope.state = langResponse.state;
        $scope.district = langResponse.district;
        $scope.facility = langResponse.facility;
        $scope.comanagerhdf = langResponse.comanagerhdf;
        $scope.fullname = langResponse.fullname;
        $scope.printmobile = langResponse.mobile;
        $scope.printaddress = langResponse.address;
        $scope.latlong = langResponse.latlong;
        $scope.username = langResponse.username;
        $scope.password = langResponse.password;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.ok = langResponse.ok;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
		
		$scope.enterfullname = langResponse.enterfullname;
		$scope.entermobile = langResponse.entermobile;
		$scope.enteremail = langResponse.enteremail;
		$scope.enterpassword = langResponse.enterpassword;
		$scope.sellanguage = langResponse.sellanguage;
		$scope.usernameexits = langResponse.usernameexits;
		$scope.emailexit = langResponse.emailexit;
		$scope.modalTitle12 = langResponse.createdetails;
    });
    if ($window.sessionStorage.language == 1) {
        //$scope.modalTitle12 = 'Created With the Following Details';
        $scope.LanguagePrint = 'Language';
    }
    else if ($window.sessionStorage.language == 2) {
        //$scope.modalTitle12 = 'निम्न विवरण के साथ बनाया';
        $scope.LanguagePrint = 'भाषा';
    }
    else if ($window.sessionStorage.language == 3) {
        //$scope.modalTitle12 = 'ಕೆಳಗಿನ ವಿವರಗಳಿಂದ  ರಚಿಸಲಾಗಿದೆा';
        $scope.LanguagePrint = 'ಭಾಷೆ';
    }
    else if ($window.sessionStorage.language == 4) {
        //$scope.modalTitle12 = 'பின்வரும் விவரங்கள் உடன் உருவாக்கப்பட்டதுा';
        $scope.LanguagePrint = 'மொழி';
    }
    else if ($window.sessionStorage.language == 5) {
        //$scope.modalTitle12 = 'క్రింది వివరాలను తో రూపొందించబడింది';
        $scope.LanguagePrint = 'భాష';
    }
    else if ($window.sessionStorage.language == 6) {
        //$scope.modalTitle12 = 'ह्या माहिती सुधारणा करण्यात आलेले आहे';
        $scope.LanguagePrint = 'भाषा';
    }*/
});
/*************************************** Not In Use ***************************
        $scope.employees = Restangular.all('employees').getList().$object;
        $scope.partners = Restangular.all('fieldworkers').getList().$object;
        $scope.submitpartners = Restangular.all('fieldworkers').getList().$object;
        $scope.retailers = Restangular.all('retailers').getList().$object;
        $scope.distributionRoutes = Restangular.all('distribution-routes').getList().$object;
        //  $scope.groups = Restangular.all('groups').getList().$object;
        $scope.cities = Restangular.all('cities').getList().$object;
        $scope.states = Restangular.all('states').getList().$object;
        $scope.ims = Restangular.all('ims').getList().$object;
        $scope.retailercategories = Restangular.all('retailercategories').getList().$object;

        $scope.zone = Restangular.all('zones').getList().$object;
        $scope.salesareas = Restangular.all('sales-areas').getList().$object;
        $scope.distributionAreas = Restangular.all('distribution-areas').getList().$object;
        $scope.distributionSubAreas = Restangular.all('distribution-subareas').getList().$object;
        $scope.submitauditlogs = Restangular.all('auditlogs').getList().$object;
        $scope.submitusers = Restangular.all('users').getList().$object;

        

        $scope.partner = {};
        $scope.groups = Restangular.one('groups', 9).get().then(function (group) {
            $scope.groupname = group.name;
            $scope.partner.groupId = group.id;
        });
		
		 /*
					$scope.user = {
						flag: true,
						lastmodifiedtime: new Date(),
						lastmodifiedby: $window.sessionStorage.UserEmployeeId,
						deleteflag: false,
						roleId: 6,
						status: 'active',
						salesAreaId: $window.sessionStorage.salesAreaId,
						coorgId: $window.sessionStorage.coorgId,
						zoneId: $window.sessionStorage.zoneId,
						username: submitfw.firstname,
						mobile: submitfw.mobile,
						employeeid: submitfw.id,
						email: submitfw.email
					};*/
/*

		        $scope.openResetPswd = function () {
		            $scope.modalPswd = $modal.open({
		                animation: true,
		                templateUrl: 'template/resetpassword.html',
		                scope: $scope,
		                backdrop: 'static',
		                size: 'lg'

		            });
		        };

		        $scope.SaveResetPswd = function () {
		            Restangular.all('users').login($scope.user).then(function (loginResult) {
		                console.log('loginResult', loginResult);
		            }).then(function () {
		                if ($scope.user.newpassword === $scope.user.confirmnewpassword) {
		                    Restangular.all('users?filter[where][username]=' + $scope.user.username).getList().then(function (detail) {
		                        if (detail.length > 0) {
		                            $scope.userId = detail[0].id;

		                            $scope.newdata = {
		                                password: $scope.user.newpassword
		                            };

		                            Restangular.one('users/' + $scope.userId).customPUT($scope.newdata).then(function (response) {
		                                console.log('response', response);

		                                $scope.modalPswd.close();
		                                $scope.logout();
		                            });
		                        }
		                    });
		                } else {
		                    $scope.errormsg = 'Passwords Doesnt Match';
		                }
		            }, function (response) {
		                console.log('response', response);

		                $scope.errormsg = 'Invalid Password';

		            });
		        };

		        $scope.okResetPswd = function () {
		            $scope.modalPswd.close();
		        };

				*/
/**********************************************************************/
