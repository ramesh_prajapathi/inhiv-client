'use strict';

angular.module('secondarySalesApp')
	.controller('responsereceiveCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
		/*********/
		//$scope.modalTitle = 'Thank You';
		//$scope.message = 'responsereceive has been created!';
		$scope.showForm = function () {
			var visible = $location.path() === '/responsereceive/create' || $location.path() === '/responsereceive/' + $routeParams.id;
			return visible;
		};
		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/responsereceive/create';
				return visible;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/responsereceive/create' || $location.path() === '/responsereceive/' + $routeParams.id;
			return visible;
		};
		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/responsereceive/create' || $location.path() === '/responsereceive/' + $routeParams.id;
			return visible;
		};
		if ($window.sessionStorage.roleId != 1) {
			window.location = "/";
		}


		/*********************************** Pagination *******************************************/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 5;
		} else {
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		}


		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		if ($window.sessionStorage.prviousLocation != "partials/responcedreceived") {
			$window.sessionStorage.myRoute = '';
			$window.sessionStorage.myRoute_currentPagesize = 5;
			$window.sessionStorage.myRoute_currentPage = 1;
			$scope.currentpage = 1;
			$scope.pageSize = 5;
		}

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};
		/*********************************** INDEX *******************************************/

		$scope.part = Restangular.all('responcedreceived?filter[where][deleteflag]=false').getList().then(function (part) {
			$scope.responcedreceived = part;
			$scope.responsereceiveId = $window.sessionStorage.sales_responsereceiveId;
			angular.forEach($scope.responcedreceived, function (member, index) {
				member.index = index + 1;

				$scope.TotalTodos = [];
				$scope.TotalTodos.push(member);
			});
		});

		/*-------------------------------------------------------------------------------------*/
		$scope.responsereceive = {
			deleteflag: false
		};

		$scope.statecodeDisable = false;
		$scope.membercountDisable = false;
		if ($routeParams.id) {
            $scope.message = 'Response Receive has been Updated';
			$scope.statecodeDisable = true;
			$scope.membercountDisable = true;
			Restangular.one('responcedreceived', $routeParams.id).get().then(function (responsereceive) {
				$scope.original = responsereceive;
				$scope.responsereceive = Restangular.copy($scope.original);
			});
		} else {
            $scope.message = 'Response Receive has been Created';
        }
		/************* SAVE *******************************************

		$scope.submitDisable = false;
		$scope.Saveresponsereceive = function (clicked) {
			document.getElementById('name').style.border = "";
			if ($scope.responsereceive.name == '' || $scope.responsereceive.name == null) {
				toaster.pop('error', "", 'Enter Response Receive Name', null, 'trustedHtml');
				document.getElementById('name').style.borderColor = "#FF0000";
			} else {
				$scope.submitDisable = true;
				toaster.pop('success', "Response Receive has been created", null, null, 'trustedHtml');
				Restangular.all('responcedreceived').post($scope.responsereceive).then(function (Response) {
					window.location = '/responsereceive';
					
				});
			}
		};

		/***************************** UPDATE *******************************************
		$scope.Updateresponsereceive = function () {
			document.getElementById('name').style.border = "";
			if ($scope.responsereceive.name == '' || $scope.responsereceive.name == null) {
				toaster.pop('error', "", 'Enter Response Receive Name', null, 'trustedHtml');
				document.getElementById('name').style.borderColor = "#FF0000";
			} else {
				$scope.submitDisable = true;
				toaster.pop('success', "Response Receive has been updated", null, null, 'trustedHtml');
				Restangular.one('responcedreceived', $routeParams.id).customPUT($scope.responsereceive).then(function () {
					$location.path('/responsereceive');
				});
			}
		};
    **************************/
    
     $scope.validatestring = '';
    $scope.Saveresponsereceive = function () {
        if ($scope.responsereceive.name == '' || $scope.responsereceive.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive';
        }
        else if ($scope.responsereceive.hnname == '' || $scope.responsereceive.hnname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in hindi';
        }
        else if ($scope.responsereceive.knname == '' || $scope.responsereceive.knname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in kannada';
        }
        else if ($scope.responsereceive.taname == '' || $scope.responsereceive.taname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in tamil';
        }
        else if ($scope.responsereceive.tename == '' || $scope.responsereceive.tename == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in telugu';
        }
        else if ($scope.responsereceive.mrname == '' || $scope.responsereceive.mrname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in marathi';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
            $scope.schemestagedataModal = !$scope.schemestagedataModal;
            $scope.submitDisable = true;
            //toaster.pop('success', "response receive has been created", null, null, 'trustedHtml');
            Restangular.all('responcedreceived').post($scope.responsereceive).then(function (Response) {
                window.location = '/responsereceive';
            });
        }
    };
    /***************************** UPDATE *******************************************/
    $scope.Updateresponsereceive = function () {
        if ($scope.responsereceive.name == '' || $scope.responsereceive.name == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive';
        }
        else if ($scope.responsereceive.hnname == '' || $scope.responsereceive.hnname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in hindi';
        }
        else if ($scope.responsereceive.knname == '' || $scope.responsereceive.knname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in kannada';
        }
        else if ($scope.responsereceive.taname == '' || $scope.responsereceive.taname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in tamil';
        }
        else if ($scope.responsereceive.tename == '' || $scope.responsereceive.tename == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in telugu';
        }
        else if ($scope.responsereceive.mrname == '' || $scope.responsereceive.mrname == null) {
            $scope.validatestring = $scope.validatestring + 'Please enter response receive in marathi';
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        }
        else {
           $scope.schemestagedataModal = !$scope.schemestagedataModal;
            $scope.submitDisable = true;
            Restangular.one('responcedreceived', $routeParams.id).customPUT($scope.responsereceive).then(function () {
                window.location = '/responsereceive';
            });
        }
    };
    $scope.modalTitle = 'Thank You';
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
	/*---------------------------Delete---------------------------------------------------*/
	
		$scope.Delete = function (id) {
			//toaster.pop('success', "title", '<ul><li>Render html1</li></ul>', 5000, 'trustedHtml');
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('responcedreceived/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}
		

	});
