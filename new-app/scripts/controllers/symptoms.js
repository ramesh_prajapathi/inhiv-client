'use strict';

angular.module('secondarySalesApp')
    .controller('SymptomsCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }



        $scope.showForm = function () {
            var visible = $location.path() === '/symptom/create' || $location.path() === '/symptom/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/symptom/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/symptom/create' || $location.path() === '/symptom/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/symptom/create' || $location.path() === '/symptom/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
    $scope.disabledCOndition = false;
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/symptoms-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }
        /***new changes*****/
        $scope.showenglishLang = true;

        $scope.$watch('symptom.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    $scope.symptom.name = '';
                    $scope.symptom.parentId = '';
                     $scope.symptom.conditionId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });
    
    $scope.$watch('symptom.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {

                Restangular.one('symptoms', newValue).get().then(function (resp) {
                    console.log('resp', resp);
                    $scope.symptom.conditionId = resp.conditionId;
                });
            }
        });
        /***new changes*****/

        if ($routeParams.id) {
             $scope.disabledCOndition = true;
            $scope.message = 'Symptom has been Updated!';
            Restangular.one('symptoms', $routeParams.id).get().then(function (symptom) {
                $scope.original = symptom;
                $scope.symptom = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Symptom has been Created!';
        }

        //$scope.Search = $scope.name;

        $scope.MeetingTodos = [];
    
     $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;
    
     $scope.conditions = Restangular.all('conditions?filter[where][deleteflag]=false&filter[where][language]=1').getList().$object;

        /******************************** INDEX *******************************************/
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('symptoms?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.symptoms = mt;
               Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.symptoms, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.symlanguages = sev;
            });

        } else {

            Restangular.all('symptoms').getList().then(function (mt) {
                $scope.symptoms = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.symptoms, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });
          
            
            Restangular.all('symptoms?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.symlanguages = sev;
           
                      angular.forEach($scope.symlanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                         
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.symlanguages = sev;
                       });
                  }
              });
        }

        /***new changes*****/

        Restangular.all('symptoms?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.symdisply = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        /***new changes*****/
        //        Restangular.all('symptoms?filter[where][deleteflag]=false').getList().then(function (sy) {
        //            $scope.symptoms = sy;
        //            angular.forEach($scope.symptoms, function (member, index) {
        //                member.index = index + 1;
        //
        //                //                Restangular.one('languages', member.language).get().then(function (lng) {
        //                //                    member.langname = lng.name;
        //                //                });
        //            });
        //        });
        //
        //        /***new changes*****/
        //        Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
        //            $scope.symlanguages = sev;
        //        });
        //
        //        Restangular.all('symptoms?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
        //            $scope.symdisply = zn;
        //        });
        //
        //        $scope.getLanguage = function (languageId) {
        //            return Restangular.one('languages', languageId).get().$object;
        //        };
        //
        //        /***new changes*****/
        /********************************************* SAVE *******************************************/
        $scope.symptom = {
            "name": '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            "deleteflag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            //            document.getElementById('hnname').style.border = "";
            //            document.getElementById('knname').style.border = "";
            //            document.getElementById('taname').style.border = "";
            //            document.getElementById('tename').style.border = "";
            //            document.getElementById('mrname').style.border = "";
            //            if ($scope.symptom.language == '' || $scope.symptom.language == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Select Language';
            //                //  document.getElementById('language').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.symptom.name == '' || $scope.symptom.name == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Enter Symptom';
            //                document.getElementById('name').style.borderColor = "#FF0000";
            //
            //            }

            if ($scope.symptom.language == '' || $scope.symptom.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.symptom.language == 1) {
                
                 if ($scope.symptom.conditionId == '' || $scope.symptom.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                }
               else if ($scope.symptom.name == '' || $scope.symptom.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Symptom';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            } else if ($scope.symptom.language != 1) {
                if ($scope.symptom.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Symptom in English';

                } else  if ($scope.symptom.conditionId == '' || $scope.symptom.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                }
                else if ($scope.symptom.name == '' || $scope.symptom.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Symptom';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                //                $scope.submitDisable = true;
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                $scope.symptoms.post($scope.symptom).then(function () {
                //                    window.location = '/symptoms-list';
                //                });
                if ($scope.symptom.parentId === '') {
                    delete $scope.symptom['parentId'];
                }
                $scope.submitDisable = true;
                $scope.symptom.parentFlag = $scope.showenglishLang;
                $scope.symptoms.post($scope.symptom).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/symptoms-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'symptom_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            //            document.getElementById('hnname').style.border = "";
            //            document.getElementById('knname').style.border = "";
            //            document.getElementById('taname').style.border = "";
            //            document.getElementById('tename').style.border = "";
            //            document.getElementById('mrname').style.border = "";
            if ($scope.symptom.language == '' || $scope.symptom.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.symptom.language == 1) {
                
                 if ($scope.symptom.conditionId == '' || $scope.symptom.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                }
               else if ($scope.symptom.name == '' || $scope.symptom.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Symptom';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            } else if ($scope.symptom.language != 1) {
                if ($scope.symptom.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Symptom in English';

                } else  if ($scope.symptom.conditionId == '' || $scope.symptom.conditionId == null) {
                    $scope.validatestring = $scope.validatestring + 'Please Select Condition';
                }
                else if ($scope.symptom.name == '' || $scope.symptom.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Symptom';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {

                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                Restangular.one('symptoms', $routeParams.id).customPUT($scope.symptom).then(function () {
                //                    console.log('Step Saved');
                //                    window.location = '/symptoms-list';
                //
                //
                //                });

                if ($scope.symptom.parentId === '') {
                    delete $scope.symptom['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('symptoms', $routeParams.id).customPUT($scope.symptom).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('symptom Name Saved');
                    setTimeout(function () {
                        window.location = '/symptoms-list';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'symptom_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('symptoms/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        
        /***************** Archive *****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('symptoms/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
