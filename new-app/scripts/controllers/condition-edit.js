'use strict';

angular.module('secondarySalesApp')
    .controller('ConditionEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $route, $window, $filter, $timeout) {
        /*********/

        //$scope.disablePatientAdd = true;
        // $window.sessionStorage.previous = '/condition/create';
        $window.sessionStorage.previousRouteparamsId = $routeParams.id;
        $window.sessionStorage.previouspage = '/condition/edit/:id';

        Restangular.one('conditionLanguages?filter[where][language]=' + $window.sessionStorage.language).get().then(function (langResponse) {
            $scope.ConditionLanguage = langResponse[0];
            //$scope.modalInstanceLoad.close();
            $scope.PRHeading = $scope.ConditionLanguage.editCondition;
            $scope.modaltitle1 = $scope.ConditionLanguage.areYouSureToWantToUpdate;
            $scope.message = $scope.ConditionLanguage.conditionUpdated;
            $scope.entermemberName = $scope.ConditionLanguage.enterName;
            $scope.enteryourId = $scope.ConditionLanguage.enterId;
            $scope.selectingrelationship = $scope.ConditionLanguage.selectRelationship;
            $scope.selectingCondition = $scope.ConditionLanguage.selectCondition;
            $scope.pleaseselectareason = $scope.ConditionLanguage.pleaseselectreason;
            $scope.pleaseselectafollowup = $scope.ConditionLanguage.pleaseSelectFollowup;

        });

        if ($window.sessionStorage.language == 1) {
            $scope.modalTitle = 'Thank You';
        } else if ($window.sessionStorage.language == 2) {
            $scope.modalTitle = 'धन्यवाद';
        } else if ($window.sessionStorage.language == 3) {
            $scope.modalTitle = 'ಧನ್ಯವಾದ';
        } else if ($window.sessionStorage.language == 4) {
            $scope.modalTitle = 'நன்றி';
        }


        $scope.HideCreateButton = false;
        $scope.editDisable = true;
        $scope.confirmationModel = false;
        $scope.ConditionLanguage = {};
        $scope.UserLanguage = $window.sessionStorage.language;
        $scope.hideReason = false;
        $scope.disableCaseClosed = true;
        $scope.memberDisabled = true;
        $scope.disabledId = true;
        $scope.disablePatientAdd = false;
        //  $scope.hideFollowUp = false;


        Restangular.all('conditions?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (cn) {
            $scope.conditionsdsply = cn;
        });

        Restangular.all('reasonforcancellations?filter[where][deleteFlag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (rfc) {
            $scope.reasonforcancellations = rfc;
        });


        $scope.updatepatientrec = function () {
            $window.sessionStorage.fullName = $scope.condition.memberId;
            $window.sessionStorage.relation = $scope.condition.relation;
            window.location = '/patientrecord/create';

        };


        $scope.cancelCondition = function () {

            $window.sessionStorage.MemberId = '';
            $window.sessionStorage.fullName = '';
            $window.sessionStorage.relation = '';
            $window.sessionStorage.conditionId = '';
            $window.sessionStorage.conditionIdnew = '';
            $window.sessionStorage.previousRouteparamsId = '';
            $window.sessionStorage.previouspage = '';
            window.location = '/conditions-list';
        };

    
     if ($window.sessionStorage.roleId == 2) {
            $scope.hidePatient = false;
           
        } else {
            $scope.hidePatient = false;
        }
        $scope.trailerArray = [];

        $scope.myArray = [];

        if ($routeParams.id) {
            Restangular.one('conditionheaders', $routeParams.id).get().then(function (cnd) {
                $scope.original = cnd;
                $scope.condition = Restangular.copy($scope.original);

                $window.sessionStorage.conditionId = cnd.condition;
                $scope.site = cnd.site;

                Restangular.one('beneficiaries', cnd.memberId).get().then(function (memData) {
                    Restangular.one('conditions', cnd.condition).get().then(function (cnts) {
                        $scope.condition.membername = memData.fullname;
                        //$scope.conditionName = cnts.name;
                        $scope.memberName = memData.fullname;
                    });
                });

                Restangular.all('conditiontrailers?filter[where][deleteflag]=false' + '&filter[where][conditionHeaderId]=' + cnd.id).getList().then(function (trailers) {
                    //filter[where][deleteflag]=false' + '&
                    Restangular.all('conditionstatuses?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().then(function (csts) {

                        $scope.conditionstatuses = csts;

                        Restangular.all('conditionsteps?filter[where][language]=' + $window.sessionStorage.language + '&filter[where][deleteflag]=false').getList().then(function (cs) {
                            $scope.conditionsteps = cs;

                            angular.forEach(trailers, function (value, index) {
                                value.index = index;
                                // console.log('value', value);

                                $scope.myArray = [];

                                $scope.trailerArray.push({
                                    status: value.status,
                                    date: value.createdDate,
                                    id: value.step,
                                    enabled: value.enabled,
                                    hidelist: value.hidelist, 
                                    actionable: value.actionable,
                                    statuses: $scope.conditionstatuses,
                                    trailerid: value.id
                                });
                              //  console.log(' $scope.trailerArray', $scope.trailerArray);

                                if (value.enabled == true) {

                                    console.log('i m in if con');

                                    angular.forEach($scope.trailerArray[value.index].statuses, function (Arrdata, index) {
                                     
                                        var enabledFor = Arrdata.enabledFor.split(",");
                                        var myFlag = enabledFor.includes($scope.trailerArray[value.index].id.toString())

                                        // console.log('enabledFor', enabledFor);
                                        // console.log('myFlag', myFlag);
                                        if (myFlag == true) {
                                            Arrdata.enabled = false;
                                        } else if (myFlag == false) {
                                            Arrdata.enabled = false;
                                        }
                                        
                                       /* if (Arrdata.enabled == true) {
                                            console.log('If', tdata.enabled);
                                            Arrdata.enabled = false;
                                            console.log( tdata.enabled)
                                        } else if (Arrdata.enabled == false) {
                                          //  console.log('else', tdata.enabled);
                                            Arrdata.enabled = true;
                                        }*/

                                        $scope.myArray.push({
                                            name: Arrdata.name,
                                            orderNo: Arrdata.orderNo,
                                            enabledFor: Arrdata.enabledFor,
                                            deleteflag: Arrdata.deleteFlag,
                                            id: Arrdata.id,
                                            enabled: Arrdata.enabled,
                                            hidelist: Arrdata.hidelist,
                                            actionable: Arrdata.actionable,
                                            parentId: Arrdata.parentId
                                        });

                                        $scope.trailerArray[value.index].statuses = $scope.myArray;
                                    });
                                } else {
                                   // console.log('i m in else');

                                    angular.forEach($scope.trailerArray[value.index].statuses, function (ArrdataOne, index) {
                                        var enabledFor = ArrdataOne.enabledFor.split(",");
                                        var myFlag = enabledFor.includes($scope.trailerArray[value.index].id.toString())

                                        if (myFlag == true) {
                                            ArrdataOne.enabled = false;
                                        } else if (myFlag == false) {
                                            ArrdataOne.enabled = false;
                                        }

                                        $scope.myArray.push({
                                            name: ArrdataOne.name,
                                            orderNo: ArrdataOne.orderNo,
                                            enabledFor: ArrdataOne.enabledFor,
                                            deleteflag: ArrdataOne.deleteFlag,
                                            id: ArrdataOne.id,
                                            enabled: ArrdataOne.enabled,
                                            hidelist: ArrdataOne.hidelist,
                                            actionable: ArrdataOne.actionable,
                                            parentId: ArrdataOne.parentId
                                        });
                                        $scope.trailerArray[value.index].statuses = $scope.myArray;
                                    });
                                }

                                if (trailers.length == $scope.trailerArray.length) {
                                    angular.forEach($scope.trailerArray, function (tdata, index) {
                                        console.log('tdata', tdata);

                                        tdata.index = index;

                                         $scope.hideFollowUp = true;
                                            $scope.followupFlag = true;
                                        
                                        if (tdata.enabled == true) {
                                           // console.log('If', tdata.enabled);
                                            tdata.enabled = false;
                                          //  console.log( 'tdatassss', tdata)
                                        } else if (tdata.enabled == false) {
                                          //  console.log('else', tdata.enabled);
                                            tdata.enabled = true;
                                        }
                                        if (tdata.actionable == true ) {
                                          //  console.log('If', tdata.enabled);
                                            tdata.enabled1 = false;
                                           // console.log( 'tdatassss', tdata)
                                        } else if (tdata.actionable == false) {
                                          //  console.log('else', tdata.enabled);
                                            tdata.enabled1 = true;
                                        }
                                        

                                        if (tdata.id == 37 && tdata.status == 39) {
                                            tdata.enabled = true;
                                        }
                                        if (tdata.id == 37 && tdata.status == 39) {
                                            $scope.hideReason = true;
                                        }
                                        
                                         if (tdata.status == 40) {
                                            $scope.hideReason = true;
                                        }
                                        
                                        if (tdata.status == 39 || tdata.status == 0 ||tdata.status == 38) {
                                            console.log('followupFlag');
                                          
                                            $scope.hideFollowUp = false;
                                            $scope.followupFlag = false;
                                        } 
                                        if(tdata.status != 39 || tdata.status != 0 ||tdata.status != 38){
                                           $scope.hideFollowUp = true;
                                            $scope.followupFlag = true;
                                           }
                                        
                                           
                                       


                                        if (tdata.status == 0) {
                                            tdata.status = '';
                                        }

                                        if ($window.sessionStorage.language == 1) {
                                            Restangular.one('conditionsteps', tdata.id).get().then(function (ctp) {
                                                tdata.step = ctp.name;
                                            });
                                        } else {
                                            Restangular.one('conditionsteps/findOne?filter[where][parentId]=' + tdata.id + '&filter[where][language]=' + $window.sessionStorage.language).get().then(function (ctp) {
                                                tdata.step = ctp.name;
                                            });
                                        }
                                    });

                                    var last = $scope.trailerArray[$scope.trailerArray.length - 1];
                                }
                            });
                        });
                    });
                });
            });
        }

        $scope.getStep = function (id) {
            return Restangular.one('conditionsteps', id).get().$object;
        };



        $scope.$watch('condition.condition', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {
                Restangular.one('conditions', newValue).get().then(function (ctns) {
                    $scope.conditionName = ctns.name;
                });
            }
        });


        $scope.conditionfollowups = Restangular.all('conditionfollowups?filter[order]=orderNo%20ASC&filter[where][deleteflag]=false' + '&filter[where][language]=' + $window.sessionStorage.language).getList().$object;



        $scope.$watch('condition.followup', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '' || newValue == null) {
                return;
            } else {
                Restangular.one('conditionfollowups', newValue).get().then(function (cntn) {
                    $scope.followUpName = cntn.name;
                    var myDate = new Date();
                    myDate.setDate(myDate.getDate() + cntn.followUpDays);
                    $scope.condition.followupdate = myDate;
                });
            }
        });

        $scope.$watch('condition.reasonForCancel', function (newValue, oldValue) {
            if (newValue == oldValue || newValue == '') {
                return;
            } else {

                Restangular.one('reasonforcancellations', newValue).get().then(function (resn) {
                    $scope.reasonForCancelName = resn.name;
                });
            }
        });



        $scope.trailer = {};

        $scope.auditArray = [];

        $scope.mytArray = [];

        $scope.statusChange = function (id, index, status, statuses, step, oldvalue) {

            var currindex = index;

            Restangular.one('conditionstatuses', status).get().then(function (csts) {
                $scope.actionable = csts.actionable;

                if ($scope.actionable == 'No') {
                    angular.forEach($scope.trailerArray, function (member, index) {
                        member.index = index;
                        console.log('no', member);

                        if (member.enabled == true && member.index > currindex) {
                            member.hidelist = false;
                        }
                    });

                } else if ($scope.actionable == 'Yes') {
                    angular.forEach($scope.trailerArray, function (member, index) {
                        member.index = index;
                        console.log('yes', member);
                        if (member.enabled == true) {
                            member.hidelist = true;
                        }
                    });
                }
            });

            $scope.mytArray = [];

            $scope.indexValue = index;

            $scope.auditArray.push({
                stepId: id,
                oldStatusId: oldvalue,
                newStatusId: status
            });

            var indexVal = index + 1;
            var prevIndexVal = index - 1;

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];

            $scope.trailerArray[index].date = new Date();

            if (index != 0) {
                $scope.trailerArray[prevIndexVal].enabled = true;
            }

            if (status == 33) {
                Restangular.one('conditionstatuses', 33).get().then(function (resp) {
                    // console.log('resp', resp);
                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                         $scope.trailerArray[index].actionable = true;
                        $scope.hideFollowUp = true;
                        $scope.followupFlag = true;
                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        
                         $scope.trailerArray[index].actionable = false;
                        $scope.trailerArray[indexVal].hidelist = false;
                        $scope.hideFollowUp = false;
                        $scope.followupFlag = false;
                    }
                });

                $scope.hideReason = false;
                $scope.trailerArray[index].enabled = false;


            } else if (status == 34) {

                Restangular.one('conditionstatuses', 34).get().then(function (resp) {
                     console.log('resp', resp);
                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                          $scope.trailerArray[index].actionable = true;

                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                         $scope.trailerArray[index].actionable = false;

                    }
                });
                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;


            } else if (status == 35) {
                Restangular.one('conditionstatuses', 35).get().then(function (resp) {
                    // console.log('resp', resp);
                    if (resp.actionable == 'Yes') {
                        // $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                         $scope.trailerArray[index].actionable = true;

                    } else {
                        //  $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                         $scope.trailerArray[index].actionable = false;
                    }
                });
                if (id == 36) {

                    $scope.trailerArray[indexVal].enabled = false;
                    // $scope.trailerArray[index].actionable = true;
                } else {
                    $scope.trailerArray[indexVal].enabled = true;
                    // $scope.trailerArray[index].actionable = false;
                }

                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;
                // $scope.trailerArray[indexVal].enabled = false;
                // $scope.trailerArray[indexVal].hidelist = true;



            } else if (status == 36) {
                Restangular.one('conditionstatuses', 36).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        // $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                         $scope.trailerArray[index].actionable = true;

                    } else {
                        // $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                         $scope.trailerArray[index].actionable = false;

                    }
                });

                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;

                // $scope.trailerArray[indexVal].enabled = false;
                // $scope.trailerArray[indexVal].hidelist = true;
                //$scope.hideFollowUp = true;
                //$scope.followupFlag = true;
                if (id == 36) {

                    $scope.trailerArray[indexVal].enabled = false;
                } else {
                    $scope.trailerArray[indexVal].enabled = true;
                }

            } else if (status == 37) {
                Restangular.one('conditionstatuses', 37).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                         $scope.trailerArray[index].actionable = true;
                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                         $scope.trailerArray[index].actionable = false;
                    }
                });

                $scope.hideReason = false;
                $scope.hideFollowUp = true;
                $scope.followupFlag = true;
                $scope.trailerArray[index].enabled = false;




            } else if (status == 38) {
                Restangular.one('conditionstatuses', 38).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = true;
                         $scope.trailerArray[index].actionable = true;
                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                         $scope.trailerArray[index].actionable = false;
                    }
                });


                $scope.hideReason = false;
               $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.trailerArray[index].enabled = false;
                //$scope.trailerArray[indexVal].enabled = true;
                //$scope.trailerArray[indexVal].hidelist = false;




            } else if (status == 39) {
                Restangular.one('conditionstatuses', 39).get().then(function (resp) {
                    // console.log('resp', resp);

                    if (resp.actionable == 'Yes') {
                        $scope.trailerArray[indexVal].enabled = false;
                        $scope.trailerArray[indexVal].hidelist = true;
                         $scope.trailerArray[index].actionable = true;

                    } else {
                        $scope.trailerArray[indexVal].enabled = true;
                        $scope.trailerArray[indexVal].hidelist = false;
                         $scope.trailerArray[index].actionable = false;
                    }
                });

                if (id == 37) {

                    $scope.hideReason = true;
                } else {
                    $scope.hideReason = false;
                }

                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
                $scope.trailerArray[index].enabled = false;
                //$scope.trailerArray[indexVal].enabled = false;
                //$scope.trailerArray[indexVal].hidelist = true;
                // $scope.hideFollowUp = true;
                // $scope.followupFlag = true;
            } else {
                $scope.hideReason = true;
                $scope.trailerArray[index].enabled = false;
                 $scope.trailerArray[index].actionable = false;
                $scope.trailerArray[indexVal].hidelist = false;
                $scope.hideFollowUp = false;
                $scope.followupFlag = false;
            }
        };


        $scope.validatestring = '';

        $scope.okConfirm = function () {

            $scope.diagnosisDataArray = [];

            var last = $scope.trailerArray[$scope.trailerArray.length - 1];
            //                      
            //            console.log($scope.indexValue);
            //            console.log($scope.trailerArray.lastIndexOf(last));
            //            
            if ($scope.indexValue == $scope.trailerArray.lastIndexOf(last)) {

                $scope.condition.step = $scope.trailerArray[$scope.indexValue].id;
                $scope.condition.status = $scope.trailerArray[$scope.indexValue].status;

                Restangular.one('conditionstatuses', $scope.trailerArray[$scope.indexValue].status).get().then(function (sts) {
                    $scope.statusName = sts.name;
                });



                Restangular.one('conditionsteps', $scope.trailerArray[$scope.indexValue].id).get().then(function (stp) {
                    $scope.stepName = stp.name;
                });

            }

            for (var g = 0; g < $scope.trailerArray.length; g++) {
                if ($scope.trailerArray[g].enabled == false && $scope.trailerArray[g].status != '') {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;

                    Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });

                    Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });
                } else if ($scope.trailerArray[g].status == 35 || $scope.trailerArray[g].status == 40) {

                    $scope.condition.step = $scope.trailerArray[g].id;
                    $scope.condition.status = $scope.trailerArray[g].status;


                    Restangular.one('conditionstatuses', $scope.trailerArray[g].status).get().then(function (sts) {
                        $scope.statusName = sts.name;
                    });

                    Restangular.one('conditionsteps', $scope.trailerArray[g].id).get().then(function (stp) {
                        $scope.stepName = stp.name;
                    });

                }
            }


            if ($scope.hideReason === false) {
                $scope.condition.reasonForCancel = null;
                $scope.reasonForCancelName = '';
            }

            $scope.followUpDate = $filter('date')($scope.condition.followupdate, 'dd-MMM-yyyy');
            console.log('$scope.hideReason', $scope.hideReason);
            console.log('$scope.hideFollowUp', $scope.hideFollowUp);

            if ($scope.condition.membername == '' || $scope.condition.membername == null) {
                $scope.validatestring = $scope.validatestring + $scope.entermemberName;
                //$scope.ConditionLanguage.pleaseSelectMember;

            } else if ($scope.condition.tiId == '' || $scope.condition.tiId == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteryourId;
                //$scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.condition.relation == '' || $scope.condition.relation == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectingrelationship;
                //$scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.condition.condition == '' || $scope.condition.condition == null) {
                $scope.validatestring = $scope.validatestring + $scope.selectingCondition;
                //$scope.ConditionLanguage.pleaseSelectCondition;

            } else if ($scope.hideReason === true) {
                if ($scope.condition.reasonForCancel == '' || $scope.condition.reasonForCancel == null) {
                    $scope.validatestring = $scope.validatestring + $scope.pleaseselectareason;
                    //$scope.ConditionLanguage.reasonForCancelling;
                }
            } else if ($scope.hideFollowUp === true) {
                if ($scope.condition.followup == '' || $scope.condition.followup == null) {
                    $scope.validatestring = $scope.validatestring + $scope.pleaseselectafollowup;
                    //$scope.ConditionLanguage.pleaseSelectFollowup;
                }
            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
                //	$scope.stakeholderdataModal = !$scope.stakeholderdataModal;
            } else {

                $scope.confirmationModel = true;
            }

        };

        var tArray = 0;

        $scope.Save = function () {

            for (var h = 0; h < $scope.trailerArray.length; h++) {

                if ($scope.trailerArray[h].enabled == false && ($scope.trailerArray[h].status == 38 || $scope.trailerArray[h].status == 40)) {
                    //$scope.trailerArray[h].status == 39 ||
                    $scope.trailerArray[h].enabled = true;
                    tArray++;
                } else {
                    tArray++;
                }

                if (tArray == $scope.trailerArray.length) {
                    console.log('$scope.trailerArray', $scope.trailerArray);
                    $scope.saveFunc();
                }
            }
        };

        $scope.saveFunc = function () {

            $scope.confirmationModel = false;

            $scope.toggleLoading();

            // $scope.condition.associatedHF = $scope.associatedHF;
            Restangular.one('conditionheaders', $routeParams.id).customPUT($scope.condition).then(function (response) {
                // console.log(response);
                $scope.updateTrailer(response.id, response.memberId, response.condition);
            });
        };

        $scope.updateCount = 0;
        $scope.trailerupdate = {};

        $scope.updateTrailer = function (headerId, memberId, condition) {
            if ($scope.updateCount < $scope.trailerArray.length) {
                $scope.trailerupdate.memberId = memberId;
                $scope.trailerupdate.condition = condition;
                $scope.trailerupdate.conditionHeaderId = headerId;
                $scope.trailerupdate.step = $scope.trailerArray[$scope.updateCount].id;
                $scope.trailerupdate.status = $scope.trailerArray[$scope.updateCount].status;
                $scope.trailerupdate.enabled = $scope.trailerArray[$scope.updateCount].enabled;
                 $scope.trailerupdate.actionable = $scope.trailerArray[$scope.updateCount].actionable;
                $scope.trailerupdate.hidelist = $scope.trailerArray[$scope.updateCount].hidelist;
                $scope.trailerupdate.createdDate = $scope.trailerArray[$scope.updateCount].date;
                $scope.trailerupdate.countryId = $window.sessionStorage.countryId;
                $scope.trailerupdate.stateId = $window.sessionStorage.stateId;
                $scope.trailerupdate.districtId = $window.sessionStorage.districtId;
                $scope.trailerupdate.site = $scope.site;
                $scope.trailerupdate.createdBy = $window.sessionStorage.userId;
                $scope.trailerupdate.createdByRole = $window.sessionStorage.roleId;
                $scope.trailerupdate.lastModifiedDate = new Date();
                $scope.trailerupdate.lastModifiedBy = $window.sessionStorage.userId;
                $scope.trailerupdate.lastModifiedByRole = $window.sessionStorage.roleId;
                $scope.trailerupdate.associatedHF = $scope.condition.associatedHF;
                $scope.trailerupdate.facility = $window.sessionStorage.coorgId;
                $scope.trailerupdate.facilityId = $scope.condition.facilityId;
                $scope.trailerupdate.deleteflag = false;

                if ($scope.trailerupdate.enabled == true) {
                    $scope.trailerupdate.enabled = false;
                } else {
                    $scope.trailerupdate.enabled = true;
                }
                // console.log($scope.trailerupdate.enabled);

                Restangular.one('conditiontrailers', $scope.trailerArray[$scope.updateCount].trailerid).customPUT($scope.trailerupdate).then(function (resp) {
                    // console.log(response);
                    $scope.updateCount++;
                    $scope.updateTrailer(headerId, memberId, condition);
                });
            } else {
                $scope.saveAuditTrail(headerId, memberId, condition);
            }
        };



        $scope.auidtCount = 0;

        $scope.saveAuditTrail = function (headerId, memberId, condition) {
            if ($scope.auidtCount < $scope.auditArray.length) {

                $scope.auditArray[$scope.auidtCount].entityid = headerId;
                $scope.auditArray[$scope.auidtCount].description = 'condition Updated';
                $scope.auditArray[$scope.auidtCount].modifiedbyroleid = $window.sessionStorage.roleId;
                $scope.auditArray[$scope.auidtCount].modifiedby = $window.sessionStorage.UserEmployeeId;
                $scope.auditArray[$scope.auidtCount].lastmodifiedtime = new Date();
                $scope.auditArray[$scope.auidtCount].state = $window.sessionStorage.zoneId;
                $scope.auditArray[$scope.auidtCount].district = $window.sessionStorage.salesAreaId;
                $scope.auditArray[$scope.auidtCount].facility = $window.sessionStorage.coorgId;
                $scope.auditArray[$scope.auidtCount].facilityId = $scope.condition.facilityId;

                Restangular.all('auditlogs').post($scope.auditArray[$scope.auidtCount]).then(function (auditresp) {
                    $scope.auidtCount++;
                    $scope.saveAuditTrail(headerId, memberId, condition);
                });
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setTimeout(function () {

                    $window.sessionStorage.fullName = '';
                    $window.sessionStorage.relation = '';
                    $window.sessionStorage.conditionId = '';

                    window.location = "/conditions-list";
                }, 350);
            }
        };

        $scope.existCount = 0;

        $scope.checklistupdate = {};

        $scope.existingDateUpdate = function () {

            if ($scope.existCount < $scope.checklists.length) {

                $scope.checklists[$scope.existCount].status = $scope.checklists[$scope.existCount].statusFlag;

                Restangular.all('checklistheaders').customPUT($scope.checklists[$scope.existCount]).then(function (checkResp) {
                    //  console.log(checkResp);
                    $scope.existCount++;
                    $scope.existingDateUpdate();
                });
            } else {
                $scope.modalInstanceLoad.close();
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                console.log('reloading...');

                setTimeout(function () {
                    window.location = "/conditions-list";
                }, 350);
            }
        };



        $scope.showValidation = false;

        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        //Datepicker settings start

        $scope.today = function () {
            $scope.dt = $filter('date')(new Date(), 'y-MM-dd');
        };

        $scope.today();

        $scope.showWeeks = true;
        $scope.toggleWeeks = function () {
            $scope.showWeeks = !$scope.showWeeks;
        };

        //        $scope.clear = function () {
        //            $scope.dt = null;
        //        };

        $scope.dtmax = new Date();

        $scope.toggleMin = function () {
            $scope.minDate = ($scope.minDate) ? null : new Date();
        };

        $scope.toggleMin();
        $scope.picker = {};

        $scope.open = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened = true;
        };

        $scope.open1 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.opened1 = true;
        };

        $scope.open2 = function ($event, index) {
            $event.preventDefault();
            $event.stopPropagation();

            $timeout(function () {
                $('#datepickerfollowup' + index).focus();
            });
            $scope.condition.followupopened = true;
        };

        $scope.conditionfollowupopen = function ($event, index) {
            //$event.preventDefault();
            $event.stopPropagation();
            $timeout(function () {
                $('#datepicker' + index).focus();
            });
            $scope.condition.followupdatepick = true;
        };


        $scope.dateOptions = {
            'year-format': 'yy',
            'starting-day': 1
        };

        $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
        $scope.format = $scope.formats[0];
        //Datepicker settings end///

    });
