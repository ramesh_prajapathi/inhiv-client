'use strict';

angular.module('secondarySalesApp')
    .controller('nativedistrictCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
        $scope.OtherLang = false;
        /*********/
        if ($window.sessionStorage.roleId != 1) {
            window.location = "/";
        }

        $scope.showForm = function () {
            var visible = $location.path() === '/nativedistrict/create' || $location.path() === '/nativedistrict/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/nativedistrict/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/nativedistrict/create' || $location.path() === '/nativedistrict/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/nativedistrict/create' || $location.path() === '/nativedistrict/' + $routeParams.id;
            return visible;
        };
        /*********/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };

        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/nativedistrict") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /**********************************/
        if ($routeParams.id) {
            $scope.OtherLang = true;
            $scope.message = 'Native District has been Updated!';
            Restangular.one('nativedistricts', $routeParams.id).get().then(function (nativedistrict) {
                $scope.original = nativedistrict;
                $scope.nativedistrict = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Native District has been Updated!';
        }
        $scope.Search = $scope.name;

        /************************ INDEX *******************************************/
        $scope.zn = Restangular.all('nativedistricts?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.nativedistricts = zn;
            angular.forEach($scope.nativedistricts, function (member, index) {
                member.index = index + 1;
            });
        });

        /************ SAVE *******************************************/
        $scope.nativedistrict = {
            "name": '',
            "deleteflag": false
        };

        $scope.submitDisable = false;
        $scope.validatestring = '';
        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.nativedistrict.name == '' || $scope.nativedistrict.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District ';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.hnname == '' || $scope.nativedistrict.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.knname == '' || $scope.nativedistrict.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.taname == '' || $scope.nativedistrict.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.tename == '' || $scope.nativedistrict.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enterNative District in telugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.mrname == '' || $scope.nativedistrict.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.nativedistricts.post($scope.nativedistrict).then(function () {
                    console.log('nativedistrict Saved');
                    window.location = '/nativedistrict';
                });
            };
        };
        /************************************** UPDATE *******************************************/
        $scope.validatestring = '';
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('hnname').style.border = "";
            document.getElementById('knname').style.border = "";
            document.getElementById('taname').style.border = "";
            document.getElementById('tename').style.border = "";
            document.getElementById('mrname').style.border = "";
            if ($scope.nativedistrict.name == '' || $scope.nativedistrict.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District ';
                document.getElementById('name').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.hnname == '' || $scope.nativedistrict.hnname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District in hindi';
                document.getElementById('hnname').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.knname == '' || $scope.nativedistrict.knname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in kannada';
                document.getElementById('knname').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.taname == '' || $scope.nativedistrict.taname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in tamil';
                document.getElementById('taname').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.tename == '' || $scope.nativedistrict.tename == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in melugu';
                document.getElementById('tename').style.borderColor = "#FF0000";

            } else if ($scope.nativedistrict.mrname == '' || $scope.nativedistrict.mrname == null) {
                $scope.validatestring = $scope.validatestring + 'Please enter Native District  in marathi';
                document.getElementById('mrname').style.borderColor = "#FF0000";

            }
            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                $scope.submitDisable = true;
                $scope.nativedistricts.customPUT($scope.nativedistrict).then(function () {
                    console.log('nativedistrict Saved');
                    window.location = '/nativedistrict';
                });
            }
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /************************************ DELETE *******************************************/



        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }






        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('nativedistricts/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }

    });
