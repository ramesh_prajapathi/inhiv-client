'use strict';

angular.module('secondarySalesApp')
  .controller('SchemeDeptCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams,$timeout,baseUrl, $route, $window) {
    /*********/

    $scope.showForm = function(){
      var visible = $location.path() === '/schemedepartment/create' || $location.path() === '/schemedepartment/' + $routeParams.id;
      return visible;
    };
    
    $scope.isCreateView = function(){
      if($scope.showForm()){
        var visible = $location.path() === '/schemedepartment/create';
        return visible;
      }
    };
    $scope.hideCreateButton = function (){
        var visible = $location.path() === '/schemedepartment/create'|| $location.path() === '/schemedepartment/' + $routeParams.id;
        return visible;
      };

    
    $scope.hideSearchFilter = function (){
        var visible = $location.path() === '/schemedepartment/create'|| $location.path() === '/schemedepartment/' + $routeParams.id;
        return visible;
      };

    
    /*********/
   
    if($routeParams.id){
      Restangular.one('schemedepartments', $routeParams.id).get().then(function(schemedepartment){
				$scope.original = schemedepartment;
				$scope.schemedepartment = Restangular.copy($scope.original);
      });
    }
    $scope.Search = $scope.name;
    
/************************************************************************** INDEX *******************************************/
       $scope.zn = Restangular.all('schemedepartments').getList().then(function (zn) {
        $scope.schemedepartments = zn;
        angular.forEach($scope.schemedepartments, function (member, index) {
            member.index = index + 1;
        });
    });
    
/*************************************************************************** SAVE *******************************************/
     $scope.validatestring = '';
        $scope.Save = function () {
          
                    $scope.schemedepartments.post($scope.schemedepartment).then(function () {
                        console.log('schemedepartment Saved');
                        window.location = '/schemedepartment';
                    });
        };
/*************************************************************************** UPDATE *******************************************/ 
     $scope.validatestring = '';
        $scope.Update = function () {
                document.getElementById('name').style.border = "";
          if ($scope.schemedepartment.name == '' || $scope.schemedepartment.name == null) {
                $scope.schemedepartment.name = null;
                $scope.validatestring = $scope.validatestring + 'Plese enter your schemedepartment Type';
                document.getElementById('name').style.border = "1px solid #ff0000";
              
          } 
                if ($scope.validatestring != '') {
                    alert($scope.validatestring);
                    $scope.validatestring='';
                } else {
                    $scope.schemedepartments.customPUT($scope.schemedepartment).then(function () {
                        console.log('schemedepartment Saved');
                        window.location = '/schemedepartment';
                    });
                }


        };
/*************************************************************************** DELETE *******************************************/ 
   $scope.Delete = function (id) {
        if (confirm("Are you sure want to delete..!") == true) {
            Restangular.one('schemedepartments/' + id).remove($scope.schemedepartment).then(function () {
                $route.reload();
            });

        } else {

        }

    }
    
  });


