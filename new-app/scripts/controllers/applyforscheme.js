'use strict';
angular.module('secondarySalesApp').directive('validationmodal', function () {
    return {
        template: '<div class="modal fade" data-backdrop="static">' + '<div class="modal-dialog modal-danger modal-sm">' + '<div class="modal-content">' + '<div class="modal-header">' + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h4 class="modal-title">{{ title1 }}</h4>' + '</div>' + '<div class="modal-body" ng-transclude></div>' + '<div class="modal-footer">' + '<div align="left">' + '<button type="button" class="btn btn-success btn-sm btn-flat" data-dismiss="modal" aria-hidden="true">Ok</button>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title1 = attrs.title1;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });
            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });
            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
}).controller('ApplyForSchemeCtrl', function ($scope, Restangular, $window, $route, $location, $routeParams, $filter, $timeout, $modal) {
    if ($window.sessionStorage.roleId != 5 && $window.sessionStorage.roleId != 6) {
        window.location = "/";
    }
    $scope.HideCreateButton = false;
    $scope.schememaster = {
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        lastmodifiedtime: new Date(),
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId,
        deleteflag: false,
        memberId: []
    };
    $scope.schememaster.memberId.push($window.sessionStorage.fullName);
    $scope.auditlog = {
        modifiedbyroleid: $window.sessionStorage.roleId,
        modifiedby: $window.sessionStorage.UserEmployeeId,
        lastmodifiedtime: new Date(),
        entityroleid: 44,
        state: $window.sessionStorage.zoneId,
        district: $window.sessionStorage.salesAreaId,
        facility: $window.sessionStorage.coorgId,
        lastmodifiedby: $window.sessionStorage.UserEmployeeId
    };
    /****************************************** Member *******************************************/
    $scope.submitsurveyanswers = Restangular.all('surveyanswers');
    $scope.financialgoals = Restangular.all('financialgoals').getList().$object;
    if ($window.sessionStorage.roleId == 5) {
        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.comemberid = comember.id;
            $scope.schememaster.facilityId = comember.id;
            $scope.auditlog.facilityId = comember.id;
            $scope.getfacilityId = comember.id;
        });
        $scope.part = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId + '&filter[where][id]=' + $window.sessionStorage.fullName).getList().then(function (part1) {
            $scope.beneficiaries = part1;
        });
    } else if ($window.sessionStorage.roleId == 15) {
        Restangular.one('comembers', $window.sessionStorage.UserEmployeeId).get().then(function (comember) {
            $scope.comemberid = comember.id;
            $scope.schememaster.facilityId = comember.id;
            $scope.auditlog.facilityId = comember.id;
            $scope.getfacilityId = comember.id;
        });
        $scope.beneficiaries = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
    } else {
        Restangular.one('fieldworkers', $window.sessionStorage.UserEmployeeId).get().then(function (fw) {
            Restangular.one('comembers', fw.facilityId).get().then(function (comember) {
                $scope.comemberid = comember.id;
                $scope.schememaster.facilityId = comember.id;
                $scope.auditlog.facilityId = comember.id;
                $scope.getfacilityId = comember.id;
            });
            $scope.beneficiaries = Restangular.all('beneficiaries?filter={"where":{"and":[{"site":{"inq":[' + fw.sitesassigned + ']}},{"deleteflag":{"inq":[false]}}]}}').getList().$object;
        });
    }
    /***********************************************************************/
    $scope.UserLanguage = $window.sessionStorage.language;
    $scope.schemestages = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().$object;
    $scope.printschemes = Restangular.all('schemes?filter[where][deleteflag]=false&filter[where][state]=' + $window.sessionStorage.zoneId).getList().$object;
    $scope.responcedreceived = Restangular.all('responcedreceived?filter[where][deleteflag]=false').getList().$object;
    $scope.reasonforrejections = Restangular.all('reasonforrejections?filter[where][deleteflag]=false').getList().$object;
    $scope.reasonfordelayed = Restangular.all('reasonfordelayed?filter[where][deleteflag]=false').getList().$object;
    //$scope.noofmonthrepaid = Restangular.all('noofmonthrepay?filter[where][deleteflag]=false').getList().$object;

    Restangular.all('noofmonthrepay').getList().then(function (response) {
        var str = response[0].name;
        var arr = str.split('-');
        var a1 = parseInt(arr[0]);
        var a2 = parseInt(arr[1]);
        $scope.newnoofmonthrepay = [];
        for (var i = a1; i <= a2; i++) {
            $scope.newnoofmonthrepay.push(i);
            // console.log(' $scope.newntimesinyears', $scope.newntimesinyears);
        }

    });

    $scope.toggleValidation = function () {
        console.log('toggleValidation');
        $scope.showValidation = !$scope.showValidation;
    };
    //$scope.message = 'Scheme has been applied!';
    //$scope.loanapplied = false;

    /**********************************************************************************/

    $scope.mygenderstatus = false;
    $scope.myoccupationstatus = false;
    $scope.mysocialstatus = false;
    $scope.myagestatus = false;
    $scope.myhealthstatus = false;
    $scope.loanapplied = false;
    /*
    $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
       // console.log('schemeId',newValue);
        if (newValue == oldValue || newValue == undefined) {
            return;
        } else {
            $scope.schememaster.stage = '';
            $scope.toggleLoading();
            var Array = [];
            var Array2 = [];
            Restangular.one('beneficiaries', $scope.schememaster.memberId).get().then(function (brfcry) {
                $scope.newoccupation = brfcry.typology.toUpperCase();
                //console.log('$scope.newoccupation', $scope.newoccupation);
                if ($scope.newoccupation === 'MIGRANT' || $scope.newoccupation === 'IDU' || $scope.newoccupation === 'TRUCKERS' || $scope.newoccupation === 'GENERAL') {
                    $scope.modalInstanceLoad.close();
                    $scope.myoccupationstatus = true;
                    //return;
                    console.log('i m IF');
                } //else {
                    console.log('i m ELSE');
                    Restangular.one('occupationstatuses/findOne?filter[where][name]=' + $scope.newoccupation).get().then(function (occstatus) {
                        Restangular.one('schemes', newValue).get().then(function (schme) {
                            //console.log('occstatus', occstatus.id);
                            console.log('schme agegroup', schme.agegroup);
                            //console.log('brfcry', brfcry.gender);
                            Restangular.one('maritalstatuses', brfcry.maritalstatus).get().then(function (marstatus) {
                                //console.log('marstatus', marstatus);
                                Restangular.one('socialstatuses', schme.socialstatus).get().then(function (socstatus) {
                                    //console.log('socstatus', socstatus);
                                    //console.log('brfcry.maritalstatus', brfcry.maritalstatus);
                                    //console.log('socstatus.id', schme.socialstatus);
                                    if (socstatus.id == 1 || socstatus.id == 3 || marstatus.id == 6) {
                                        $scope.mysocialstatus = true;           
                                    } else if (marstatus.id == 3 && socstatus.id == 2) {
                                        $scope.mysocialstatus = true;    
                                    } else if (marstatus.name == socstatus.name) {
                                        $scope.mysocialstatus = true;
                                    } else {
                                        $scope.mysocialstatus = false;
                                    }
                                    $scope.modalInstanceLoad.close();
                                });
                            });


                            if (occstatus.id == schme.occupationstatus || schme.occupationstatus == 1 || schme.occupationstatus == 8) {
                                $scope.myoccupationstatus = true;
                            } 
                         
                            if (schme.healthstatus == 1 || schme.healthstatus == 2) {
                                $scope.myhealthstatus = true;

                            } else if (schme.healthstatus == 3) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.plhiv == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.plhiv == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 5) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.physicallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.physicallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 9) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.mentallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.mentallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 4) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 6).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);
                                    for (var o = 0; o < answeredquestions.length; o++) {

                                        if (answeredquestions[o].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            } else if (schme.healthstatus == 6) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 8).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);

                                    for (var m = 0; m < answeredquestions.length; m++) {
                                        if (answeredquestions[m].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            }


                            if (schme.agegroup == 1) {
                                $scope.myagestatus = true;
                            } else if (schme.agegroup == 3) {
                                for (var s = 18; s <= 59; s++) {
                                    if (brfcry.age == [s][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 4) {
                                for (var v = 60; v <= 79; v++) {
                                    if (brfcry.age == [v][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 5) {
                                for (var w = 0; w <= 17; w++) {
                                    if (brfcry.age == [w][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 6) {
                                for (var x = 80; x <= 99; x++) {
                                    if (brfcry.age == [x][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            }
                            $scope.modalInstanceLoad.close();
                            //console.log('$scope.myoccupationstatus', $scope.myoccupationstatus);
                            //console.log('$scope.mygenderstatus', $scope.mygenderstatus);
                        });
                    });
                //}
            });
        }
    });
/******************************************************NEW *****************************************/



    $scope.$watch('schememaster.memberId', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            Restangular.one('beneficiaries?filter[where][id]=' + newValue).get().then(function (benResp) {
                if (benResp.length > 0) {
                    $scope.selectedBeneficiary = benResp[0];
                } else {
                    $scope.selectedBeneficiary = {
                        stress_data: false
                    };
                }
            });
        }
    });

    $scope.$watch('schememaster.schemeId', function (newValue, oldValue) {
        console.log('schemeId', newValue);
        if (newValue == oldValue || newValue == undefined) {
            return;
        } else {
            $scope.schememaster.stage = '';
            $scope.toggleLoading();
            var Array = [];
            var Array2 = [];
            Restangular.one('beneficiaries', $scope.schememaster.memberId).get().then(function (brfcry) {
                $scope.newoccupation = brfcry.typology.toUpperCase();
                //console.log('$scope.newoccupation', $scope.newoccupation);
                if ($scope.newoccupation === 'MIGRANT' || $scope.newoccupation === 'IDU' || $scope.newoccupation === 'TRUCKERS' || $scope.newoccupation === 'GENERAL') {
                    $scope.modalInstanceLoad.close();
                    console.log('i m IF');
                    Restangular.one('occupationstatuses', 1).get().then(function (occstatus) {
                        Restangular.one('schemes', newValue).get().then(function (schme) {
                            //console.log('occstatus', occstatus.id);
                            console.log('schme agegroup', schme.agegroup);
                            //console.log('brfcry', brfcry.gender);
                            Restangular.one('maritalstatuses', brfcry.maritalstatus).get().then(function (marstatus) {
                                //console.log('marstatus', marstatus);
                                Restangular.one('socialstatuses', schme.socialstatus).get().then(function (socstatus) {
                                    //console.log('socstatus', socstatus);
                                    //console.log('brfcry.maritalstatus', brfcry.maritalstatus);
                                    //console.log('socstatus.id', schme.socialstatus);
                                    if (socstatus.id == 1 || socstatus.id == 3 || marstatus.id == 6) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.id == 3 && socstatus.id == 2) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.name == socstatus.name) {
                                        $scope.mysocialstatus = true;
                                    } else {
                                        $scope.mysocialstatus = false;
                                    }
                                    $scope.modalInstanceLoad.close();
                                });
                            });

                            Array = schme.category;
                            Array2 = Array.split(',');
                            //console.log('Array2', Array2.length);
                            for (var i = 0; i < Array2.length; i++) {
                                //console.log('Array.length',Array.length);
                                //console.log('Array', Array2[i]);
                                if (Array2[i] == 16) {
                                    $scope.loanapplied = true;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                } else {
                                    $scope.loanapplied = false;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                }
                            }


                            if (occstatus.id == schme.occupationstatus || schme.occupationstatus == 1 || schme.occupationstatus == 8) {
                                $scope.myoccupationstatus = true;
                            }

                            if (schme.healthstatus == 1 || schme.healthstatus == 2) {
                                $scope.myhealthstatus = true;

                            } else if (schme.healthstatus == 3) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.plhiv == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.plhiv == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 5) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.physicallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.physicallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 9) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.mentallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.mentallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 4) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 6).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);
                                    for (var o = 0; o < answeredquestions.length; o++) {

                                        if (answeredquestions[o].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            } else if (schme.healthstatus == 6) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 8).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);

                                    for (var m = 0; m < answeredquestions.length; m++) {
                                        if (answeredquestions[m].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            }


                            if (schme.agegroup == 1) {
                                $scope.myagestatus = true;
                            } else if (schme.agegroup == 3) {
                                for (var s = 18; s <= 59; s++) {
                                    if (brfcry.age == [s][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 4) {
                                for (var v = 60; v <= 79; v++) {
                                    if (brfcry.age == [v][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 5) {
                                for (var w = 0; w <= 17; w++) {
                                    if (brfcry.age == [w][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 6) {
                                for (var x = 80; x <= 99; x++) {
                                    if (brfcry.age == [x][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            }
                            $scope.modalInstanceLoad.close();

                        });
                        $scope.modalInstanceLoad.close();
                    });
                    $scope.modalInstanceLoad.close();
                } else {
                    console.log('i m ELSE');
                    Restangular.one('occupationstatuses/findOne?filter[where][name]=' + $scope.newoccupation).get().then(function (occstatus) {
                        Restangular.one('schemes', newValue).get().then(function (schme) {
                            //console.log('occstatus', occstatus.id);
                            console.log('schme agegroup', schme.agegroup);
                            //console.log('brfcry', brfcry.gender);
                            Restangular.one('maritalstatuses', brfcry.maritalstatus).get().then(function (marstatus) {
                                //console.log('marstatus', marstatus);
                                Restangular.one('socialstatuses', schme.socialstatus).get().then(function (socstatus) {
                                    //console.log('socstatus', socstatus);
                                    //console.log('brfcry.maritalstatus', brfcry.maritalstatus);
                                    //console.log('socstatus.id', schme.socialstatus);
                                    if (socstatus.id == 1 || socstatus.id == 3 || marstatus.id == 6) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.id == 3 && socstatus.id == 2) {
                                        $scope.mysocialstatus = true;
                                    } else if (marstatus.name == socstatus.name) {
                                        $scope.mysocialstatus = true;
                                    } else {
                                        $scope.mysocialstatus = false;
                                    }
                                    $scope.modalInstanceLoad.close();
                                });
                            });

                            Array = schme.category;
                            Array2 = Array.split(',');
                            console.log('Array2', Array2.length);
                            for (var i = 0; i < Array2.length; i++) {
                                //console.log('Array.length',Array.length);
                                //console.log('Array', Array2[i]);
                                if (Array2[i] == 16) {
                                    $scope.loanapplied = true;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                } else {
                                    $scope.loanapplied = false;
                                    console.log('$scope.loanapplied', $scope.loanapplied);
                                }
                            }



                            if (occstatus.id == schme.occupationstatus || schme.occupationstatus == 1 || schme.occupationstatus == 8) {
                                $scope.myoccupationstatus = true;
                            }
                            if (schme.healthstatus == 1 || schme.healthstatus == 2) {
                                $scope.myhealthstatus = true;

                            } else if (schme.healthstatus == 3) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.plhiv == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.plhiv == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 5) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.physicallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.physicallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 9) {
                                console.log('schme.healthstatus', schme.healthstatus);
                                if (brfcry.mentallydisabled == true) {
                                    $scope.myhealthstatus = true;
                                } else if (brfcry.mentallydisabled == false) {
                                    $scope.myhealthstatus = false;
                                }
                            } else if (schme.healthstatus == 4) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 6).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);
                                    for (var o = 0; o < answeredquestions.length; o++) {

                                        if (answeredquestions[o].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            } else if (schme.healthstatus == 6) {
                                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + $scope.schememaster.memberId + '&filter[where][questionid]=' + 8).getList().then(function (answeredquestions) {
                                    console.log('answeredquestions', answeredquestions);

                                    for (var m = 0; m < answeredquestions.length; m++) {
                                        if (answeredquestions[m].answer.toLowerCase() == 'yes') {
                                            $scope.myhealthstatus = true;
                                        }
                                    }
                                });
                            }


                            if (schme.agegroup == 1) {
                                $scope.myagestatus = true;
                            } else if (schme.agegroup == 3) {
                                for (var s = 18; s <= 59; s++) {
                                    if (brfcry.age == [s][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 4) {
                                for (var v = 60; v <= 79; v++) {
                                    if (brfcry.age == [v][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 5) {
                                for (var w = 0; w <= 17; w++) {
                                    if (brfcry.age == [w][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            } else if (schme.agegroup == 6) {
                                for (var x = 80; x <= 99; x++) {
                                    if (brfcry.age == [x][0]) {
                                        $scope.myagestatus = true;
                                        //  console.log('valid data'); 
                                    }
                                }
                            }
                            $scope.modalInstanceLoad.close();
                            //console.log('$scope.myoccupationstatus', $scope.myoccupationstatus);
                            //console.log('$scope.mygenderstatus', $scope.mygenderstatus);
                        });
                    });
                }
            });
        }
    });
    /********************************/
    $scope.Save1 = function () {
        console.log('$scope.myoccupationstatus', $scope.myoccupationstatus);
        //console.log('$scope.mygenderstatus', $scope.mygenderstatus);
        console.log('$scope.myhealthstatus', $scope.myhealthstatus);
        if ($scope.schememaster.memberId == '' || $scope.schememaster.memberId == null) {
            $scope.validatestring = $scope.validatestring + $scope.selmember; //'Please Select a Member';
        } else if ($scope.schememaster.schemeId == '' || $scope.schememaster.schemeId == null) {
            $scope.validatestring = $scope.validatestring + $scope.selscheme;
        } else if ($scope.myoccupationstatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.typologynotmatch; //'Typology is not Matching for This Scheme';
            /*    
            } else if ($scope.mygenderstatus == false) {
                $scope.validatestring = $scope.validatestring + $scope.gendernotmatch; //'Gender is not Matching for This Scheme';
            */
        } else if ($scope.myhealthstatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.healthstnotmatch; //'Health Status is not Matching for This Scheme';
        } else if ($scope.mysocialstatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.socialstnotmatch; //'Social Status is not Matching for This Scheme';
        } else if ($scope.myagestatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.agenotmatch; //'Age is not Matching for This Scheme';

        }
        /*
               
                } else if ($scope.schememaster.stage == '' || $scope.schememaster.stage == null) {
                    $scope.validatestring = $scope.validatestring + $scope.selstage;
                }*/

        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            alert('Alright')
        }

    }


    /******************************************************* Save ************************/
    $scope.validatestring = '';
    $scope.applyforschemedataModal = false;
    $scope.documentdataModal = false;
    $scope.CreateClicked = false;
    $scope.Save = function () {
        $scope.todo = {
            "todotype": 28,
            "pillarid": 1,
            "beneficiaryid": $scope.schememaster.memberId,
            "datetime": $scope.schememaster.datetime, //$filter('date')(new Date(), 'y-MM-dd'),
            "stage": $scope.schememaster.stage,
            "documentid": $scope.schememaster.schemeId,
            "facility": $window.sessionStorage.coorgId,
            "district": $window.sessionStorage.salesAreaId,
            "state": $window.sessionStorage.zoneId,
            "lastmodifiedby": $window.sessionStorage.UserEmployeeId,
            "lastmodifiedtime": new Date(),
            "site": $scope.beneficiarysite,
            "pillarid": 1,
            "facilityId": $scope.getfacilityId,
            "roleId": $window.sessionStorage.roleId
        };
        $scope.paidamounts = $scope.installment * $scope.studcheck.length;
        $scope.paidamount = $scope.schememaster.amount - $scope.paid;
        $scope.schememaster.paidamount = $scope.paidamounts;
        if ($scope.schememaster.memberId == '' || $scope.schememaster.memberId == null) {
            $scope.validatestring = $scope.validatestring + $scope.selmember; //'Please Select a Member';
        } else if ($scope.schememaster.schemeId == '' || $scope.schememaster.schemeId == null) {
            $scope.validatestring = $scope.validatestring + $scope.selscheme;
            /*} else if ($scope.mygenderstatus == false) {
                $scope.validatestring = $scope.validatestring + $scope.gendernotmatch; //'Gender is not Matching for This Scheme';
            */
        } else if ($scope.myoccupationstatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.typologynotmatch; //'Typology is not Matching for This Scheme';

        } else if ($scope.myagestatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.agenotmatch; //'Age is not Matching for This Scheme';

        } else if ($scope.myhealthstatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.healthstnotmatch; //'Health Status is not Matching for This Scheme';

        } else if ($scope.mysocialstatus == false) {
            $scope.validatestring = $scope.validatestring + $scope.socialstnotmatch; //'Social Status is not Matching for This Scheme';

        } else if ($scope.schememaster.stage == '' || $scope.schememaster.stage == null) {
            $scope.validatestring = $scope.validatestring + $scope.selstage;
        }
        if ($scope.schememaster.stage == 4) {
            if ($scope.schememaster.responserecieve == '' || $scope.schememaster.responserecieve == null) {
                $scope.validatestring = $scope.validatestring + $scope.selresponsererec;
            }
        }
        if ($scope.schememaster.stage == 5) {
            if ($scope.schememaster.delay == '' || $scope.schememaster.delay == null) {
                $scope.validatestring = $scope.validatestring + $scope.selreasondelay;
            }
        }
        if ($scope.schememaster.responserecieve === 'Rejected') {
            if ($scope.schememaster.rejection == '' || $scope.schememaster.rejection == null) {
                $scope.validatestring = $scope.validatestring + $scope.selreasonrej;
            }
        }
        if ($scope.schememaster.stage == 7 && $scope.loanapplied == true) {
            if ($scope.schememaster.purposeofloan == '' || $scope.schememaster.purposeofloan == null) {
                $scope.validatestring = $scope.validatestring + $scope.selpurposeofloan;
            } else if ($scope.schememaster.amount == '' || $scope.schememaster.amount == null) {
                $scope.validatestring = $scope.validatestring + $scope.enteramount;
            } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount >= 10000 && $scope.schememaster.noofmonthrepay == '' || $scope.schememaster.noofmonthrepay == null) {
                $scope.validatestring = $scope.validatestring + $scope.selmonthrepay;
            }
        }
        //$scope.paidamounts = $scope.installment * $scope.studcheck.length;
        if ($scope.schememaster.stage == 7 && $scope.loanapplied == true) {
            //console.log('check in if');
            if ($scope.schememaster.purposeofloan != 5) {
                $scope.todo.status = 3;
                //console.log('$scope.todo.status2', $scope.todo.status);
            } else if ($scope.schememaster.purposeofloan == 5 && $scope.schememaster.amount < 10000) {
                $scope.todo.status = 3;
                // console.log('$scope.todo.status1', $scope.todo.status);
            } else if ($scope.paidamounts == $scope.schememaster.amount) {
                $scope.todo.status = 3;
                //  console.log('$scope.todo.status3', $scope.todo.status);
            } else {
                $scope.todo.status = 1;
                //  console.log('$scope.todo.status0', $scope.todo.status);
            }
        } else {
            // console.log('check in else');
            if ($scope.schememaster.stage == 4 && $scope.schememaster.responserecieve === 'Rejected') {
                $scope.todo.status = 3;
                // console.log('$scope.todo.status6', $scope.todo.status);
            } else if ($scope.schememaster.stage == 9) {
                $scope.todo.status = 3;
                //  console.log('$scope.todo.status5', $scope.todo.status);
            } else {
                $scope.todo.status = 1;
                //  console.log('$scope.todo.status4', $scope.todo.status);
            }
        }
        if ($scope.validatestring != '') {
            $scope.toggleValidation();
            $scope.validatestring1 = $scope.validatestring;
            $scope.validatestring = '';
        } else {
            $scope.toggleLoading();
            Restangular.all('schememasters?filter[where][schemeId]=' + $scope.schememaster.schemeId + '&filter[where][memberId]=' + $scope.schememaster.memberId + '&filter[where][documentflag]=false').getList().then(function (schemaster) {
                if (schemaster.length > 0) {
                    $scope.schemeMasters = schemaster[0];
                }
                Restangular.all('schemes?filter[where][id]=' + $scope.schememaster.schemeId).getList().then(function (schems) {
                    if (schems.length > 0) {
                        $scope.schemes = schems[0];
                        Restangular.all('surveyquestions?filter[where][questiontype]=scheme&filter[where][yespopup]=dateforid&filter[where][schemeslno]=' + $scope.schemes.topscheme).getList().then(function (srvyqstns) {
                            if (srvyqstns.length > 0) {
                                $scope.surveyQuestions = srvyqstns[0];
                                Restangular.all('surveyanswers?filter[where][questionid]=' + $scope.surveyQuestions.id + '&filter[where][beneficiaryid]=' + $scope.schememaster.memberId).getList().then(function (srvyanswrs) {
                                    if (srvyanswrs.length > 0) {
                                        $scope.surveyAnswers = srvyanswrs[0];
                                    }
                                    $scope.ValidatingDocument();
                                });
                            } else {
                                $scope.ValidatingDocument();
                            }
                        });
                    } else {
                        $scope.ValidatingDocument();
                    }
                });
            });
            /////
        };
        $scope.OK = function () {
            $scope.documentdataModal = !$scope.documentdataModal;
            window.location = '/';
        };
    };
    /* $scope.openOneAlert = function () {
         console.log('Me Calling')
         $scope.modalOneAlert = $modal.open({
             animation: true
             , templateUrl: 'template/AlertModal.html'
             , scope: $scope
             , backdrop: 'static'
             , keyboard: false
             , size: 'sm'
             , windowClass: 'modal-danger'
         });
     };*/
    $scope.ValidatingDocument = function () {
        //console.log('$scope.schemes', $scope.schemes);
        //console.log('$scope.schemeMasters', $scope.schemeMasters);
        //console.log('$scope.surveyQuestions', $scope.surveyQuestions);
        //console.log('$scope.surveyAnswers', $scope.surveyAnswers);
        if ($scope.schemeMasters != undefined) {
            $scope.modalInstanceLoad.close();
            //$scope.toggleValidation();
            //alert("This Scheme is already been added");
            //$scope.openOneAlert();
            //$scope.toggleValidation1();
            $scope.modalOneAlert = $modal.open({
                templateUrl: 'template/AlertModal.html',
                controller: 'ApplyForSchemeCtrl',
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
            });
            $route.reload();
        } else if ($scope.schemeMasters == undefined && $scope.surveyQuestions == undefined && $scope.surveyAnswers == undefined) {
            $scope.saveScheme();
        } else if ($scope.schemeMasters == undefined && $scope.surveyQuestions != undefined && $scope.surveyAnswers == undefined) {
            $scope.SaveAnswers($scope.schememaster.memberId, "1", $scope.surveyQuestions.id, "yes", new Date(), $window.sessionStorage.UserEmployeeId, $scope.surveyQuestions.serialno, $scope.surveyQuestions.yesincrement, $scope.surveyQuestions.noincrement, $scope.surveyQuestions.yesdocumentflag, $scope.surveyQuestions.nodocumentflag, $scope.surveyQuestions.yesdocumentid, $scope.surveyQuestions.nodocumentid, new Date(), "", $window.sessionStorage.roleId);
        } else if ($scope.schemeMasters == undefined && $scope.surveyQuestions != undefined && $scope.surveyAnswers != undefined && $scope.surveyAnswers.answer.toLowerCase() != "yes") {
            $scope.SaveAnswers($scope.schememaster.memberId, "1", $scope.surveyQuestions.id, "yes", new Date(), $window.sessionStorage.UserEmployeeId, $scope.surveyQuestions.serialno, $scope.surveyQuestions.yesincrement, $scope.surveyQuestions.noincrement, $scope.surveyQuestions.yesdocumentflag, $scope.surveyQuestions.nodocumentflag, $scope.surveyQuestions.yesdocumentid, $scope.surveyQuestions.nodocumentid, new Date(), "", $window.sessionStorage.roleId);
        } else {
            $scope.modalInstanceLoad.close();
            // $scope.toggleValidation();
            //$scope.validatestring1 = "This Scheme is already been added i am second";
            //alert("This Scheme is already been added");
            //$scope.openOneAlert();
            $scope.modalOneAlert = $modal.open({
                animation: true,
                templateUrl: 'template/AlertModal.html',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
            });
            // $scope.AtertFunction();
            //$scope.validatestring = $scope$scope.AlertMessage =$scope.schemeadded;.validatestring + 'This Scheme is already been added';
            $route.reload();
        }
    }
    $scope.AtertFunction = function () {
            $scope.modalOneAlert = $modal.open({
                animation: true,
                templateUrl: 'template/AlertModal.html',
                controller: 'ApplyForSchemeCtrl'
                    // , scope: $scope

                ,
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                windowClass: 'modal-danger'
                    //, resolve: {}
            });
        }
        // $scope.modalOneAlert = false;
    $scope.okAlert = function () {
        //console.log('okAlert')
        $scope.modalOneAlert.close();
    };
    //$scope.AlertMessage = 'This Scheme is already been added';
    //'Document has been applied!';
    $scope.modalTitle = $scope.thankyou;
    $scope.SaveAnswers = function (beneficiaryid, pillarid, questionid, answer, modifieddate, modifiedby, serialno, yesincrement, noincrement, yesdocumentflag, nodocumentflag, yesdocumentid, nodocumentid, availeddate, referenceno, modifiedbyroleid) {
        $scope.surveyanswer = {};
        if (answer == 'yes') {
            $scope.surveyanswer.documentflag = yesdocumentflag;
            $scope.surveyanswer.documentid = yesdocumentid;
            console.log('$scope.surveyanswer.documentid1', $scope.surveyanswer.documentid);
            $scope.surveyanswer.schemeid = yesdocumentid;
        } else {
            $scope.surveyanswer.documentflag = nodocumentflag;
            $scope.surveyanswer.documentid = nodocumentid;
            $scope.surveyanswer.schemeid = nodocumentid;
            console.log('$scope.surveyanswer.documentid2', $scope.surveyanswer.documentid);
        }
        $scope.surveyanswer.beneficiaryid = beneficiaryid;
        $scope.surveyanswer.pillarid = pillarid;
        $scope.surveyanswer.questionid = questionid;
        $scope.surveyanswer.answer = answer;
        $scope.surveyanswer.lastupdatedtime = modifieddate;
        $scope.surveyanswer.lastupdatedby = modifiedby;
        $scope.surveyanswer.availeddate = availeddate;
        $scope.surveyanswer.referenceno = referenceno;
        $scope.surveyanswer.modifiedbyroleid = modifiedbyroleid;
        $scope.surveyanswer.documentflag = false;
        $scope.surveyanswer.stress_data = $scope.selectedBeneficiary.stress_data;
        $scope.submitsurveyanswers.post($scope.surveyanswer).then(function (resp) {
            $scope.saveScheme();
        }, function (error) {
            if (error.data.error.constraint == "pillar_question_beneficiary") {
                // console.log('error', error);
                Restangular.all('surveyanswers?filter[where][beneficiaryid]=' + beneficiaryid + '&filter[where][questionid]=' + questionid + '&filter[where][pillarid]=' + pillarid).getList().then(function (answered) {
                    if (answered.length > 0) {
                        $scope.surveyanswer.id = answered[0].id;
                        $scope.submitsurveyanswers.customPUT($scope.surveyanswer, answered[0].id).then(function (resp) {
                            $scope.saveScheme();
                        });
                    }
                });
            }
        });
    };
    $scope.saveScheme = function () {
        $scope.createClicked = true;
        $scope.schememaster.stress_data = $scope.selectedBeneficiary.stress_data;
        Restangular.all('schememasters').post($scope.schememaster).then(function (resp) {
            // console.log('schememasters', resp);
            $scope.todo.reportincidentid = resp.id;
            $scope.auditlog.entityid = resp.id;
            if (resp.responserecieve == null || resp.responserecieve == undefined) {
                var getresponserecieve = null;
            } else {
                getresponserecieve = resp.responserecieve;
            }
            if (resp.rejection == null || resp.rejection == undefined) {
                var getresponserejection = null;
            } else {
                getresponserejection = resp.rejection;
            }
            if (resp.delay == null || resp.delay == undefined) {
                var getresponsedelay = null;
            } else {
                getresponsedelay = resp.delay;
            }
            var respdate = $filter('date')(resp.datetime, 'dd-MMMM-yyyy');
            $scope.auditlog.description = 'Apply For Scheme Created With Following Details: ' + 'Member - ' + resp.memberId + ' , ' + 'Scheme  - ' + resp.schemeId + ' , ' + 'Stage  - ' + resp.stage + ' , ' + 'Response Recieve - ' + getresponserecieve + ' , ' + 'Respons For Rejection - ' + getresponserejection + ' , ' + 'Respons For Delay - ' + getresponsedelay + ' , ' + 'Date - ' + respdate;
            //$scope.beneficiaries = Restangular.all('beneficiaries?filter[where][deleteflag]=false' + '&filter[where][facility]=' + $window.sessionStorage.coorgId).getList().$object;
            $scope.memberupdate = {
                lastmeetingdate: new Date()
            }
            Restangular.all('beneficiaries/' + $window.sessionStorage.fullName).customPUT($scope.memberupdate).then(function (responsemember) {
                //console.log('responsemember', responsemember);
                Restangular.all('auditlogs').post($scope.auditlog).then(function (responseaudit) {
                    $scope.todo.stress_data = $scope.selectedBeneficiary.stress_data;
                    Restangular.all('todos').post($scope.todo).then(function (todores) {
                        //console.log('todores', todores);
                        $scope.printmember = Restangular.one('beneficiaries', resp.memberId).get().$object;
                        $scope.printdocumenttype = Restangular.one('schemes', resp.schemeId).get().$object;
                        $scope.printstage = Restangular.one('schemestages', resp.stage).get().$object;
                        $scope.printdate = resp.datetime;
                        $scope.modalInstanceLoad.close();
                        $scope.documentdataModal = !$scope.documentdataModal;
                    });
                });
            });
        });
    }
    var sevendays = new Date();
    sevendays.setDate(sevendays.getDate() + 7);
    $scope.schememaster.datetime = sevendays
        //Datepicker settings start
    $scope.today = function () {};
    $scope.today();
    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.dtmin = new Date();
    $scope.toggleMin = function () {
        $scope.minDate = ($scope.minDate) ? null : new Date();
    };
    $scope.toggleMin();
    $scope.picker = {};
    $scope.openstart = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerstart' + index).focus();
        });
        $scope.start.openedstart = true;
    };
    $scope.openstart1 = function ($event, index) {
        $event.preventDefault();
        $event.stopPropagation();
        $timeout(function () {
            $('#datepickerstart' + index).focus();
        });
        $scope.start.openedstart = true;
    };
    $scope.dateOptions = {
        'year-format': 'yy',
        'starting-day': 1
    };
    $scope.formats = ['dd-MMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    //Datepicker settings end
    $scope.datetimehide = true;
    $scope.reponserec = true;
    $scope.delaydis = true;
    $scope.collectedrequired = true;
    $scope.purposeofloanhide = true;
    $scope.$watch('schememaster.stage', function (newValue, oldValue) {
        console.log('schememaster.stage', newValue);
        //console.log('$scope.loanapplied', $scope.loanapplied);
        var fifteendays = new Date();
        fifteendays.setDate(fifteendays.getDate() + 15);
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        if (newValue === oldValue) {
            return;
        } else if (newValue === '4') {
            $scope.reponserec = false;
            $scope.delaydis = true;
            $scope.datetimehide = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '5') {
            $scope.delaydis = false;
            $scope.reponserec = true;
            $scope.datetimehide = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.datetime = fifteendays;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '2') {
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.datetimehide = true;
            $scope.collectedrequired = false;
            $scope.rejectdis = true;
            $scope.schememaster.datetime = fifteendays;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '3') {
            $scope.schememaster.datetime = fifteendays;
            $scope.datetimehide = true;
            $scope.collectedrequired = true;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '7') {
            var sixmonth = new Date();
            sixmonth.setDate(sixmonth.getDate() + 180);
            $scope.schememaster.datetime = new Date();
            $scope.datetimehide = false;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.delaydis = true;
            $scope.reponserec = true;
            //console.log('$scope.loanapplied', $scope.loanapplied);
            //console.log('newValue', newValue);
            if ($scope.loanapplied == true && newValue === '7') {
                $scope.purposeofloanhide = false;
                $scope.datetimehide = false;
                var onemonth = new Date();
                onemonth.setDate(onemonth.getDate() + 30);
                $scope.schememaster.datetime = onemonth;
            }
        } else if (newValue === '1') {
            $scope.schememaster.datetime = fifteendays;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '8') {
            $scope.schememaster.datetime = sevendays;
            $scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '6') {
            $scope.datetimehide = false;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else if (newValue === '9') {
            //$scope.datetimehide = true;
            $scope.delaydis = true;
            $scope.reponserec = true;
            $scope.collectedrequired = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.datetime = new Date();
            $scope.purposeofloanhide = true;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        } else {
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.purposeofloanhide = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.delay = null;
            //$scope.schememaster.datetime = '';
            $scope.datetimehide = true;
            $scope.rejectdis = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.purposeofloan = '';
            $scope.schememaster.amount = '';
        }
    });
    $scope.rejectdis = true;
    $scope.$watch('schememaster.responserecieve', function (newValue, oldValue) {
        console.log('responserecieve', newValue);
        var sevendays = new Date();
        sevendays.setDate(sevendays.getDate() + 7);
        if (newValue === oldValue) {
            return;
        } else if (newValue === null) {
            return;
        } else if (newValue === 'Approved') {
            var sixmonth = new Date();
            sixmonth.setDate(sixmonth.getDate() + 180);
            $scope.schememaster.datetime = sixmonth;
            $scope.rejectdis = true;
        } else if (newValue === 'Rejected') {
            $scope.rejectdis = false;
            $scope.schememaster.datetime = new Date();
            $scope.datetimehide = true;
        } else {
            $scope.schememaster.datetime = sevendays;
            $scope.rejectdis = true;
            $scope.schememaster.rejection = null;
            $scope.reponserec = true;
            $scope.delaydis = true;
            $scope.schememaster.responserecieve = null;
            $scope.schememaster.delay = null;
            //$scope.schememaster.datetime = '';
            $scope.datetimehide = false;
        }
    });
    $scope.showValidation = false;
    $scope.toggleValidation = function () {
        $scope.showValidation = !$scope.showValidation;
    };
    $scope.TestamountTaken = function (id) {
        $scope.schememaster.noofmonthrepay = '';
    }
    $scope.$watch('schememaster.noofmonthrepay', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            $scope.currMonthyear = [];
            for (var m = 1; m <= newValue; m++) {
                $scope.monthyear = new Date();
                $scope.monthyear.setDate($scope.monthyear.getDate() + 30 * m)
                $scope.monthyear = $filter('date')($scope.monthyear, 'MMMM-yyyy');
                //console.log('MonthYear',$scope.monthyear);
                $scope.currMonthyear.push($scope.monthyear);
                //console.log('$scope.currMonthyear', $scope.currMonthyear);
            }
        };
    });
    $scope.MonthRepay = function (monthrepay) {
        $scope.installment = $scope.schememaster.amount / monthrepay;
        //console.log('$scope.installment', $scope.installment);
    }
    $scope.studcheck = [];
    $scope.Payment = function () {
        // console.log('item',$scope.studcheck);
        //console.log('item',$scope.studcheck.length);
        $scope.paid = $scope.installment * $scope.studcheck.length;
        $scope.paidamount = $scope.schememaster.amount - $scope.paid;
        //console.log('$scope.installment', $scope.installment);
        //console.log('$scope.paid', $scope.paid);
        //console.log('$scope.paidamount', $scope.paidamount);
    }
    $scope.items = [];
    $scope.noofmonthrepay_Hide = true;
    /*  $scope.$watch('schememaster.purposeofloan', function(newValue, oldValue){
          if(newValue === oldValue){
              return;
          } else if(newValue == 5){
                     $scope.noofmonthrepay_Hide = false;
              
          } else {
             $scope.noofmonthrepay_Hide = true; 
          }
      });*/
    $scope.$watch('schememaster.purposeofloan', function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        } else {
            $scope.current_LoanPurpose = newValue;
            $scope.schememaster.amount = '';
            $scope.currMonthyear = '';
        }
    });
    $scope.$watch('schememaster.amount', function (newValue, oldValue) {
        //console.log('$scope.current_LoanPurpose',$scope.current_LoanPurpose);
        //console.log('$scope.amount',newValue);
        if (newValue === oldValue) {
            return;
        } else if ($scope.current_LoanPurpose == 5 && newValue >= 10000) {
            $scope.noofmonthrepay_Hide = false;
        } else {
            $scope.noofmonthrepay_Hide = true;
        }
    });
    /*************************** Language *********************************************/
    $scope.multiLang = Restangular.one('multilanguages', $window.sessionStorage.language).get().then(function (langResponse) {
        $scope.applyforscheme = langResponse.applyforscheme;
        $scope.mandatoryfield = langResponse.mandatoryfield;
        $scope.member = langResponse.member;
        $scope.scheme = langResponse.scheme;
        $scope.stage = langResponse.stage;
        $scope.responsereceive = langResponse.responsereceive;
        $scope.responserejection = langResponse.responserejection;
        $scope.responsedelay = langResponse.responsedelay;
        $scope.followupdate = langResponse.followupdate;
        $scope.purposeofloan = langResponse.purposeofloan;
        $scope.amounttaken = langResponse.amounttaken;
        $scope.noofmonthrepay = langResponse.noofmonthrepay;
        $scope.create = langResponse.create;
        $scope.update = langResponse.update;
        $scope.cancel = langResponse.cancel;
        $scope.okbutton = langResponse.ok;
        $scope.please = langResponse.please;
        $scope.enter = langResponse.enter;
        $scope.select = langResponse.select;
        $scope.palert = langResponse.alert;
        $scope.modalTitle = langResponse.createdetails;
        $scope.AlertMessage = langResponse.schemeadded;
        $scope.thankyou = langResponse.thankyou;
        $scope.selmember = langResponse.selmember;
        $scope.selscheme = langResponse.selscheme;
        $scope.selstage = langResponse.selstage;
        $scope.selresponsererec = langResponse.selresponsererec;
        $scope.selreasondelay = langResponse.selreasondelay;
        $scope.selpurposeofloan = langResponse.selpurposeofloan;
        $scope.selreasonrej = langResponse.selreasonrej;
        $scope.enteramount = langResponse.enteramount;
        $scope.selmonthrepay = langResponse.selmonthrepay;
        $scope.schemeadded = langResponse.schemeadded;
        $scope.gendernotmatch = langResponse.gendernotmatch;
        $scope.typologynotmatch = langResponse.typologynotmatch;
        $scope.agenotmatch = langResponse.agenotmatch;
        $scope.healthstnotmatch = langResponse.healthstnotmatch;
        $scope.socialstnotmatch = langResponse.socialstnotmatch;

    });

});