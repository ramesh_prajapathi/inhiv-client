'use strict';

angular.module('secondarySalesApp')
    .controller('artcenterEditCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {

        $scope.isCreateView = false;
        $scope.modalTitle = 'Thank You';
        $scope.disableLang = true;


        $scope.art = {
            lastModifiedDate: new Date(),
            lastModifiedByRole: $window.sessionStorage.roleId,
            usercreated: false
            // deleteflag: false,
        };

        $scope.countries = Restangular.all('countries?filter[where][deleteflag]=false').getList().$object;


        $scope.showenglishLang = true;
        $scope.$watch('art.languageId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {


                    $scope.art.name = '';
                    $scope.art.parentId = '';

                }


                if (newValue + "" != "1") {
                    $scope.disableorder = true;
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.disableorder = true;
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });

        $scope.$watch('art.parentId', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.disableorder = true;

                Restangular.one('artcenters', newValue).get().then(function (zn) {
                    console.log("zn", zn)
                    $scope.art.code = zn.code;
                    $scope.art.latitude = zn.latitude;
                    $scope.art.latitude = zn.latitude;
                    $scope.art.longitude = zn.longitude;
                    $scope.art.address = zn.address;
                    $scope.art.countryId = zn.countryId;
                    $scope.art.state = zn.state;
                    $scope.art.district = zn.district;
                    $scope.art.town = zn.town;

                });
            }
        });



        $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;


        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('artcenters?filter[where][languageId]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.artcenters = mt;
                angular.forEach($scope.artcenters, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }

                  /*  var data14 = $scope.Displaylanguages.filter(function (arr) {
                        return arr.id == member.languageId
                    })[0];

                    if (data14 != undefined) {
                        member.langName = data14.name;
                    }*/
                    member.index = index + 1;
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.medilanguages = sev;
            });

        } else {

            Restangular.all('artcenters').getList().then(function (mt) {
                $scope.artcenters = mt;
                angular.forEach($scope.artcenters, function (member, index) {

                    if (member.deleteflag + '' === 'true') {
                        member.colour = "#A20303";
                    } else {
                        member.colour = "#000000";
                    }

                    /*var data14 = $scope.Displaylanguages.filter(function (arr) {
                        return arr.id == member.languageId
                    })[0];

                    if (data14 != undefined) {
                        member.langName = data14.name;
                    }*/
                    member.index = index + 1;
                });
            });


            Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (mt) {
                if (mt.length == 0) {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;

                        angular.forEach($scope.medilanguages, function (member, index) {
                            member.index = index + 1;
                            if (member.index == 1) {
                                member.disabled = false;
                            } else {
                                member.disabled = true;
                            }
                            console.log('member', member);
                        });

                    });
                } else {
                    Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                        $scope.medilanguages = sev;
                    });
                }
            });
        }

   
    $("#code").keydown(function (e){
		var k = e.keyCode || e.which;
		var ok = k >= 65 && k <= 90 || // A-Z
			k >= 96 && k <= 105 || // a-z
			k >= 35 && k <= 40 || // arrows
			k == 9 || //tab
			k == 46 || //del
			k == 8 || // backspaces
			(!e.shiftKey && k >= 48 && k <= 57); // only 0-9 (ignore SHIFT options)

		if(!ok || (e.ctrlKey && e.altKey)){
			e.preventDefault();
		}
	});
    
    
        /***new changes*****/

        Restangular.all('artcenters?filter[where][deleteflag]=false').getList().then(function (zn) {
            $scope.disaplyactcenetrs = zn;
        });







        if ($routeParams.id) {
            //$scope.statedisable = true;
            // $scope.districdisable = true;
            $scope.disableorder = true;
            $scope.message = 'ART Center has been Updated!';
            Restangular.one('artcenters', $routeParams.id).get().then(function (art) {
                $scope.original = art;
                Restangular.all('zones?filter[where][deleteflag]=false').getList().then(function (respzone) {
                    $scope.zones = respzone;
                    Restangular.all('sales-areas?filter[where][deleteflag]=false').getList().then(function (respDist) {
                        $scope.salesareas = respDist;
                        Restangular.all('cities?filter[where][deleteflag]=false').getList().then(function (respCity) {
                            $scope.cities = respCity;
                            $scope.art = Restangular.copy($scope.original);
                            $scope.artname = $scope.art.name;
                        });
                    });
                });


            });
        } else {
            $scope.message = 'ART Center been Updated!';
        }


        /********************* WATCH ****************************/

        $scope.$watch('art.countryId', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.zones = Restangular.all('zones?filter[where][countryId]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
            $scope.countryid = +newValue;
        });

        $scope.$watch('art.state', function (newValue, oldValue) {
            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.salesareas = Restangular.all('sales-areas?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
            }
            $scope.stateid = +newValue;
        });

        $scope.$watch('art.district', function (newValue, oldValue) {

            if (newValue === oldValue || newValue == '') {
                return;
            } else if ($routeParams.id && (oldValue === "" || oldValue === undefined)) {
                return;
            } else {
                $scope.cities = Restangular.all('cities?filter[where][countryId]=' + $scope.countryid + '&filter[where][state]=' + $scope.stateid + '&filter[where][district]=' + newValue + '&filter[where][deleteflag]=false').getList().$object;
                // console.log('$scope.cities', $scope.cities);
            }
            $scope.districtid = +newValue;
        });

        /********************* WATCH ****************************/

        /************* SAVE *******************************************/
        $scope.validatestring = '';
        $scope.submitDisable = false;
        $scope.Update = function () {
            document.getElementById('name').style.border = "";
            document.getElementById('code').style.border = "";
            document.getElementById('latitude').style.border = "";
            document.getElementById('longitude').style.border = "";
            
            var regEmail = /^[a-zA-Z0-9](.*[a-zA-Z0-9])?$/;
             /*  if ($scope.art.languageId == '' || $scope.art.languageId == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';

            }*/ 
           // else if ($scope.art.languageId == 1) {
            
             if ($scope.art.countryId == '' || $scope.art.countryId == null) {
                $scope.art.countryId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            } else if ($scope.art.state == '' || $scope.art.state == null) {
                $scope.art.state == '';
                $scope.validatestring = $scope.validatestring + 'Please Select State';
            } else if ($scope.art.district == '' || $scope.art.district == null) {
                $scope.art.district == '';
                $scope.validatestring = $scope.validatestring + 'Please Select District';
            } else if ($scope.art.town == '' || $scope.art.town == null) {
                $scope.art.town == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Town';
            }
                  else if ($scope.artname == '' || $scope.artname == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter art Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            } else if (!regEmail.test($scope.artname)) {
                    $scope.artname = '';
                    $scope.validatestring = $scope.validatestring + ' Name Format is not correct';
                    document.getElementById('name').style.border = "1px solid #ff0000";
            }
            
            else if ($scope.art.code == '' || $scope.art.code == null) {
                document.getElementById('code').style.borderColor = "#FF0000";
                $scope.validatestring = $scope.validatestring + 'Please Enter art Code';

            } else if ($scope.art.latitude == '' || $scope.art.latitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter latitude';
                document.getElementById('latitude').style.borderColor = "#FF0000";

            } else if ($scope.art.longitude == '' || $scope.art.longitude == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter longitude';
                document.getElementById('longitude').style.borderColor = "#FF0000";

            } else if ($scope.art.countryId == '' || $scope.art.countryId == null) {
                $scope.art.countryId == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Country';
            } else if ($scope.art.state == '' || $scope.art.state == null) {
                $scope.art.state == '';
                $scope.validatestring = $scope.validatestring + 'Please Select State';
            } else if ($scope.art.district == '' || $scope.art.district == null) {
                $scope.art.district == '';
                $scope.validatestring = $scope.validatestring + 'Please Select District';
            } else if ($scope.art.town == '' || $scope.art.town == null) {
                $scope.art.town == '';
                $scope.validatestring = $scope.validatestring + 'Please Select Town';
            }
            //} 
              /*  else if ($scope.art.languageId != 1) {

                if ($scope.art.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select ART Name in English';

                } else if ($scope.art.name == '' || $scope.art.name == null) {
                $scope.validatestring = $scope.validatestring + 'Please Enter art Name';
                document.getElementById('name').style.borderColor = "#FF0000";
            }
            }*/

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';

            } else {

                var xyz = $scope.artname;
                $scope.art.name = xyz.toUpperCase();



                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;

                
                $scope.submitDisable = true;
                Restangular.one('artcenters', $routeParams.id).customPUT($scope.art).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('artcenters Saved');
                    setTimeout(function () {
                        window.location = '/artcenter';
                    }, 350);
                }, function (error) {
                    
                });
            }
        };
    
     var timeoutPromise;
        var delayInMs = 1000;
    
            $scope.CheckDuplicate = function (name) {
           // console.log('click')
            var CheckName = name.toUpperCase();
  
            $timeout.cancel(timeoutPromise);

            timeoutPromise = $timeout(function () {

                 var data =  $scope.artcenters.filter(function (arr) {
                    return arr.name == CheckName
                })[0];
               // console.log(data , CheckName);
                 if (data != undefined) {
                    $scope.artname = '';
                    $scope.toggleValidation();
                    $scope.validatestring1 = 'ART Name Already Exist';
                     
                 }
            }, delayInMs);
        };
    
    

        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************** Map Start ********************/

        $scope.mapdataModal = false;

        $scope.LocateMe = function () {
            $scope.mapdataModal = true;

            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 4,

                center: new google.maps.LatLng(12.9538477, 77.3507369),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            var marker, i;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(12.9538477, 77.3507369),
                map: map,
                html: ''
            });

            $scope.toggleMapModal();
        };

        $scope.toggleMapModal = function () {
            $scope.mapcount = 0;

            ///////////////////////////////////////////////////////MAP//////////////////////////

            var geocoder = new google.maps.Geocoder();

            function geocodePosition(pos) {
                geocoder.geocode({
                    latLng: pos
                }, function (responses) {
                    if (responses && responses.length > 0) {
                        updateMarkerAddress(responses[0].formatted_address);
                    } else {
                        updateMarkerAddress('Cannot determine address at this location.');
                    }
                });
            }

            function updateMarkerStatus(str) {
                document.getElementById('markerStatus').innerHTML = str;
            }

            function updateMarkerPosition(latLng) {
                //  console.log(latLng);
                $scope.art.latitude = latLng.lat();
                $scope.art.longitude = latLng.lng();

                //  console.log('$scope.updatepromotion', $scope.updatepromotion);

                document.getElementById('info').innerHTML = [
                   latLng.lat(),
                   latLng.lng()
                   ].join(', ');
            }

            function updateMarkerAddress(str) {
                document.getElementById('mapaddress').innerHTML = str;
               // $scope.art.address = str;
            }
            var map;

            function initialize() {

                $scope.latitude = 12.9538477;
                $scope.longitude = 77.3507369;
                navigator.geolocation.getCurrentPosition(function (location) {
                    //                    console.log(location.coords.latitude);
                    //                    console.log(location.coords.longitude);
                    //                    console.log(location.coords.accuracy);
                    $scope.latitude = location.coords.latitude;
                    $scope.longitude = location.coords.longitude;
                    //                });

                    // console.log('$scope.address', $scope.address);

                    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
                    map = new google.maps.Map(document.getElementById('mapCanvas'), {
                        zoom: 4,
                        center: new google.maps.LatLng($scope.latitude, $scope.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    });
                    var marker = new google.maps.Marker({
                        position: latLng,
                        title: 'Point A',
                        map: map,
                        draggable: true
                    });

                    // Update current position info.
                    updateMarkerPosition(latLng);
                    geocodePosition(latLng);

                    // Add dragging event listeners.
                    google.maps.event.addListener(marker, 'dragstart', function () {
                        updateMarkerAddress('Dragging...');
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        updateMarkerStatus('Dragging...');
                        updateMarkerPosition(marker.getPosition());
                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        updateMarkerStatus('Drag ended');
                        geocodePosition(marker.getPosition());
                    });
                });


            }

            // Onload handler to fire off the app.
            //google.maps.event.addDomListener(window, 'load', initialize);
            initialize();

            window.setTimeout(function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(new google.maps.LatLng($scope.latitude, $scope.longitude));
                map.setZoom(10);
            }, 1000);


            $scope.SaveMap = function () {
                $scope.showMapModal = !$scope.showMapModal;
                //  console.log($scope.reportincident);
            };

            //console.log('fdfd');
            $scope.showMapModal = !$scope.showMapModal;
        };

    });
