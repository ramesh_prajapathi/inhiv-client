'use strict';

angular.module('secondarySalesApp')
    .controller('TestNameCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window, $filter) {
        /*********/
        if ($window.sessionStorage.roleId != 1 && $window.sessionStorage.roleId != 4) {
            window.location = "/";
        }


        $scope.showForm = function () {
            var visible = $location.path() === '/testname/create' || $location.path() === '/testname/edit/' + $routeParams.id;
            return visible;
        };

        $scope.isCreateView = function () {
            if ($scope.showForm()) {
                var visible = $location.path() === '/testname/create';
                return visible;
            }
        };
        $scope.hideCreateButton = function () {
            var visible = $location.path() === '/testname/create' || $location.path() === '/testname/edit/' + $routeParams.id;
            return visible;
        };


        $scope.hideSearchFilter = function () {
            var visible = $location.path() === '/testname/create' || $location.path() === '/testname/edit/' + $routeParams.id;
            return visible;
        };


        /*********/
        /*********************************** Pagination *******************************************/
        if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
            $window.sessionStorage.myRoute = null;
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
        } else {
            $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
            $scope.currentpage = $window.sessionStorage.myRoute_currentPage;
            //console.log('$scope.countryId From Landing', $scope.pageSize);
        }

        $scope.currentPage = $window.sessionStorage.myRoute_currentPage;
        $scope.PageChanged = function (newPage, oldPage) {
            $scope.currentpage = newPage;
            $window.sessionStorage.myRoute_currentPage = newPage;
        };

        $scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
        $scope.pageFunction = function (mypage) {
            console.log('mypage', mypage);
            $scope.pageSize = mypage;
            $window.sessionStorage.myRoute_currentPagesize = mypage;
        };


        console.log('$window.sessionStorage.prviousLocation', $window.sessionStorage.prviousLocation);
        if ($window.sessionStorage.prviousLocation != "partials/testname-list") {
            $window.sessionStorage.myRoute = '';
            $window.sessionStorage.myRoute_currentPage = 1;
            $window.sessionStorage.myRoute_currentPagesize = 25;
            $scope.currentpage = 1;
            $scope.pageSize = 25;
        }

        /***new changes*****/
        $scope.showenglishLang = true;

        $scope.$watch('testname.language', function (newValue, oldValue) {
            console.log('newValue', newValue);
            if (newValue == '' || newValue == null) {
                return;
            } else {
                if (!$routeParams.id) {
                    $scope.testname.name = '';
                    $scope.testname.parentId = '';
                }
                if (newValue + "" != "1") {
                    $scope.showenglishLang = false;
                    $scope.OtherLang = true;
                } else {
                    $scope.showenglishLang = true;
                    $scope.OtherLang = false;
                }

            }
        });
        /***new changes*****/

        if ($routeParams.id) {
            $scope.message = 'Test Name has been Updated!';
            Restangular.one('testnames', $routeParams.id).get().then(function (testname) {
                $scope.original = testname;
                $scope.testname = Restangular.copy($scope.original);
            });
        } else {
            $scope.message = 'Test Name has been Created!';
        }

        //$scope.Search = $scope.name;
    
    //new changes for search box
        $scope.someFocusVariable = true;
        $scope.filterFields = ['index','langName','name'];
        $scope.Search = '';

        $scope.MeetingTodos = [];
    
     $scope.Displaylanguages = Restangular.all('languages?filter[where][deleteflag]=false').getList().$object;

        /******************************** INDEX *******************************************/


        if ($window.sessionStorage.roleId == 4) {

            Restangular.all('testnames?filter[where][language]=' + $window.sessionStorage.language).getList().then(function (mt) {
                $scope.testnames = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.testnames, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });

            Restangular.all('languages?filter[where][deleteflag]=false' + '&filter[where][id]=' + $window.sessionStorage.language).getList().then(function (sev) {
                $scope.testlanguages = sev;
            });

        } else {

            Restangular.all('testnames').getList().then(function (mt) {
                $scope.testnames = mt;
                Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (lang) {
                    $scope.Displanguage = lang;
                    angular.forEach($scope.testnames, function (member, index) {

                        if (member.deleteflag + '' === 'true') {
                            member.colour = "#A20303";
                        } else {
                            member.colour = "#000000";
                        }

                        for (var b = 0; b < $scope.Displanguage.length; b++) {
                            if (member.language == $scope.Displanguage[b].id) {
                                member.langName = $scope.Displanguage[b].name;
                                break;
                            }
                        }
                        member.index = index + 1;
                    });
                });
            });
           
            Restangular.all('testnames?filter[where][deleteflag]=false').getList().then(function (mt) {
                  if(mt.length == 0){
                      Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.testlanguages = sev;
           
                      angular.forEach($scope.testlanguages, function (member, index) {
                           member.index = index + 1;
                          if(member.index == 1){
                              member.disabled = false;
                          } else{
                              member.disabled = true;
                          }
                          console.log('member', member);
                           });
                          
                      });
                  } else {
                       Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
                $scope.testlanguages = sev;
                       });
                  }
              });
        }

        /***new changes*****/

        Restangular.all('testnames?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
            $scope.testdisply = zn;
        });

        $scope.getLanguage = function (languageId) {
            return Restangular.one('languages', languageId).get().$object;
        };

        /***new changes*****/



        //        Restangular.all('testnames?filter[where][deleteflag]=false').getList().then(function (test) {
        //            $scope.testnames = test;
        //            angular.forEach($scope.testnames, function (member, index) {
        //                member.index = index + 1;
        //
        //                //                Restangular.one('languages', member.language).get().then(function (lng) {
        //                //                    member.langname = lng.name;
        //                //                });
        //            });
        //        });
        //        /***new changes*****/
        //        Restangular.all('languages?filter[where][deleteflag]=false').getList().then(function (sev) {
        //            $scope.testlanguages = sev;
        //        });
        //
        //        Restangular.all('testnames?filter[where][deleteflag]=false&filter[where][language]=1').getList().then(function (zn) {
        //            $scope.testdisply = zn;
        //        });
        //
        //        $scope.getLanguage = function (languageId) {
        //            return Restangular.one('languages', languageId).get().$object;
        //        };
        //
        //        /***new changes*****/


        /********************************************* SAVE *******************************************/
        $scope.testname = {
            "name": '',
            createdDate: new Date(),
            createdBy: $window.sessionStorage.userId,
            createdByRole: $window.sessionStorage.roleId,
            lastModifiedDate: new Date(),
            lastModifiedBy: $window.sessionStorage.userId,
            lastModifiedByRole: $window.sessionStorage.roleId,
            "deleteflag": false
        };

        $scope.validatestring = '';
        $scope.submitDisable = false;

        $scope.Save = function () {
            document.getElementById('name').style.border = "";
            //            document.getElementById('hnname').style.border = "";
            //            document.getElementById('knname').style.border = "";
            //            document.getElementById('taname').style.border = "";
            //            document.getElementById('tename').style.border = "";
            //            document.getElementById('mrname').style.border = "";
            //            if ($scope.testname.language == '' || $scope.testname.language == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Select Language';
            //                //  document.getElementById('language').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.testname.name == '' || $scope.testname.name == null) {
            //                $scope.validatestring = $scope.validatestring + ' Please Enter Test Name';
            //                document.getElementById('name').style.borderColor = "#FF0000";
            //
            //            }

            if ($scope.testname.language == '' || $scope.testname.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.testname.language == 1) {
                if ($scope.testname.name == '' || $scope.testname.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Test Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            } else if ($scope.testname.language != 1) {
                if ($scope.testname.parentId == '') {
                    $scope.validatestring = $scope.validatestring + 'Please Select Test Name in English';

                } else if ($scope.testname.name == '' || $scope.testname.name == null) {
                    $scope.validatestring = $scope.validatestring + ' Please Enter Test Name';
                    document.getElementById('name').style.borderColor = "#FF0000";
                }

            }


            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {


                //                $scope.submitDisable = true;
                //                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                $scope.testnames.post($scope.testname).then(function () {
                //                    window.location = '/testname-list';
                //                });
                if ($scope.testname.parentId === '') {
                    delete $scope.testname['parentId'];
                }
                $scope.submitDisable = true;
                $scope.testname.parentFlag = $scope.showenglishLang;
                $scope.testnames.post($scope.testname).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    setTimeout(function () {
                        window.location = '/testname-list';
                    }, 350);
                }, function (error) {
                    $scope.submitDisable = false;
                    if (error.data.error.constraint === 'testname_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });

            };
        };

        $scope.modalTitle = 'Thank You';
        $scope.showValidation = false;
        $scope.toggleValidation = function () {
            $scope.showValidation = !$scope.showValidation;
        };

        /***************************************************** UPDATE *******************************************/
        $scope.Update = function () {
            document.getElementById('name').style.border = "";

            //            document.getElementById('hnname').style.border = "";
            //            document.getElementById('knname').style.border = "";
            //            document.getElementById('taname').style.border = "";
            //            document.getElementById('tename').style.border = "";
            //            document.getElementById('mrname').style.border = "";
            if ($scope.testname.language == '' || $scope.testname.language == null) {
                $scope.validatestring = $scope.validatestring + 'Please Select Language';
                //  document.getElementById('language').style.borderColor = "#FF0000";

            } else if ($scope.testname.name == '' || $scope.testname.name == null) {
                $scope.validatestring = $scope.validatestring + ' Please Enter Test Name';
                document.getElementById('name').style.borderColor = "#FF0000";

            }
            //            else if ($scope.testname.hnname == '' || $scope.testname.hnname == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Enter Test Name in Hindi';
            //                document.getElementById('hnname').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.testname.knname == '' || $scope.testname.knname == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Enter Test Name in Kannada';
            //                document.getElementById('knname').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.testname.taname == '' || $scope.testname.taname == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Enter Test Name in Tamil';
            //                document.getElementById('taname').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.testname.tename == '' || $scope.testname.tename == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Enter Test Name in Telugu';
            //                document.getElementById('tename').style.borderColor = "#FF0000";
            //
            //            } else if ($scope.testname.mrname == '' || $scope.testname.mrname == null) {
            //                $scope.validatestring = $scope.validatestring + 'Please Enter Test Name in Marathi';
            //                document.getElementById('mrname').style.borderColor = "#FF0000";
            //
            //            }

            if ($scope.validatestring != '') {
                $scope.toggleValidation();
                $scope.validatestring1 = $scope.validatestring;
                $scope.validatestring = '';
            } else {
                $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                //                Restangular.one('testnames', $routeParams.id).customPUT($scope.testname).then(function () {
                //                    console.log('Step Saved');
                //                    window.location = '/testname-list';
                //
                //
                //                });
                if ($scope.testname.parentId === '') {
                    delete $scope.testname['parentId'];
                }
                $scope.submitDisable = true;
                Restangular.one('testnames', $routeParams.id).customPUT($scope.testname).then(function () {
                    $scope.stakeholderdataModal = !$scope.stakeholderdataModal;
                    console.log('testname Saved');
                    setTimeout(function () {
                        window.location = '/testname-list';
                    }, 350);
                }, function (error) {
                    if (error.data.error.constraint === 'testname_lang_parenrid') {
                        alert('Value already exists for this language');
                    }
                });
            }
        };

        /**************************Sorting **********************************/
        $scope.sort = {
            active: '',
            descending: undefined
        }

        $scope.changeSorting = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                sort.descending = !sort.descending;

            } else {
                sort.active = column;
                sort.descending = false;
            }
        };

        $scope.getIcon = function (column) {

            var sort = $scope.sort;

            if (sort.active == column) {
                return sort.descending ? 'fa fa-sort-up' : 'fa fa-sort-desc';
            }
        }
        /******************************************************** DELETE *******************************************/
        $scope.Delete = function (id) {
            $scope.item = [{
                deleteflag: true
            }]
            Restangular.one('testnames/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        }
        
        /***************** Archive *****************/
        $scope.Archive = function (id) {
            $scope.item = [{
                deleteflag: false
            }]
            Restangular.one('testnames/' + id).customPUT($scope.item[0]).then(function () {
                $route.reload();
            });
        };

    });
