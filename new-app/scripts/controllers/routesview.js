'use strict';

angular.module('secondarySalesApp')
    .controller('RoutesViewCtrl', function ($scope, Restangular, $routeParams) {
        
        Restangular.one('distribution-routes', $routeParams.id).get().then(function (route) {
            $scope.routes = route;
            $scope.zones = Restangular.one('zones', route.zoneId).get().$object;
            $scope.salesAreas = Restangular.one('sales-areas', route.salesAreaId).get().$object;
            $scope.partners = Restangular.one('partners', route.partnerId).get().$object;
        });
    });
