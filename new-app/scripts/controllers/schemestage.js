'use strict';

angular.module('secondarySalesApp')
	.controller('schemestageCtrl', function ($scope, $rootScope, Restangular, $location, $routeParams, $timeout, baseUrl, $route, $window) {
		/*********/

		$scope.showForm = function () {
			var visible = $location.path() === '/schemestage/create' || $location.path() === '/schemestage/' + $routeParams.id;
			return visible;
		};

		$scope.isCreateView = function () {
			if ($scope.showForm()) {
				var visible = $location.path() === '/schemestage/create';
				return visible;
				$scope.CancelButtonHide = false;
			}
		};
		$scope.hideCreateButton = function () {
			var visible = $location.path() === '/schemestage/create' || $location.path() === '/schemestage/' + $routeParams.id;
			return visible;
		};


		$scope.hideSearchFilter = function () {
			var visible = $location.path() === '/schemestage/create' || $location.path() === '/schemestage/' + $routeParams.id;
			return visible;
		};


		/*********/
		if ($window.sessionStorage.myRoute == null || $window.sessionStorage.myRoute == undefined) {
			$window.sessionStorage.myRoute = null;
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		} else {
			$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
			$scope.currentpage = $window.sessionStorage.myRoute_currentPage;
			//console.log('$scope.countryId From Landing', $scope.pageSize);
		}


		if ($window.sessionStorage.prviousLocation != "partials/schemestage") {
			$window.sessionStorage.myRoute_currentPage = 1;
			$window.sessionStorage.myRoute_currentPagesize = 25;
		}

		$scope.currentPage = $window.sessionStorage.myRoute_currentPage;
		$scope.PageChanged = function (newPage, oldPage) {
			$scope.currentpage = newPage;
			$window.sessionStorage.myRoute_currentPage = newPage;
		};

		$scope.pageSize = $window.sessionStorage.myRoute_currentPagesize;
		$scope.pageFunction = function (mypage) {
			$scope.pageSize = mypage;
			$window.sessionStorage.myRoute_currentPagesize = mypage;
		};



		if ($routeParams.id) {
			$scope.message = 'Scheme Stage has been Updated!';
			Restangular.one('schemestages', $routeParams.id).get().then(function (schemestage) {
				$scope.original = schemestage;
				$scope.schemestage = Restangular.copy($scope.original);
			});
		} else {
			$scope.message = 'Scheme Stage has been Created!';
		}
		$scope.Search = $scope.name;

		/*************************** INDEX *******************************************/
		$scope.zn = Restangular.all('schemestages?filter[where][deleteflag]=false').getList().then(function (zn) {
			$scope.schemestages = zn;
			angular.forEach($scope.schemestages, function (member, index) {
				member.index = index + 1;
			});
		});
		$scope.schemestage = {
			name : '',
			deleteflag: false
		}
		/************************** SAVE *******************************************/
		$scope.validatestring = '';
		$scope.Save = function () {
			if ($scope.schemestage.name == '' || $scope.schemestage.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage';
			} else if ($scope.schemestage.hnname == '' || $scope.schemestage.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in hindi';
			} else if ($scope.schemestage.knname == '' || $scope.schemestage.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in kannada';
			} else if ($scope.schemestage.taname == '' || $scope.schemestage.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in tamil';
			} else if ($scope.schemestage.tename == '' || $scope.schemestage.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in telugu';
			} else if ($scope.schemestage.mrname == '' || $scope.schemestage.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in marathi';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.schemestagedataModal = !$scope.schemestagedataModal;
				$scope.schemestages.post($scope.schemestage).then(function () {
					console.log('schemestage Saved');
					window.location = '/schemestage';
				});
			}
		};
		/***********/
		$scope.validatestring = '';
		$scope.Update = function () {
			if ($scope.schemestage.name == '' || $scope.schemestage.name == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage';
			} else if ($scope.schemestage.hnname == '' || $scope.schemestage.hnname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in hindi';
			} else if ($scope.schemestage.knname == '' || $scope.schemestage.knname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in kannada';
			} else if ($scope.schemestage.taname == '' || $scope.schemestage.taname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in tamil';
			} else if ($scope.schemestage.tename == '' || $scope.schemestage.tename == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in telugu';
			} else if ($scope.schemestage.mrname == '' || $scope.schemestage.mrname == null) {
				$scope.validatestring = $scope.validatestring + 'Please enter scheme stage in marathi';
			}
			if ($scope.validatestring != '') {
				$scope.toggleValidation();
				$scope.validatestring1 = $scope.validatestring;
				$scope.validatestring = '';
			} else {
				$scope.submitDisable = true;
				$scope.schemestagedataModal = !$scope.schemestagedataModal;
				$scope.schemestages.customPUT($scope.schemestage).then(function () {
					console.log('schemestage Saved');
					window.location = '/schemestage';
				});
			}


		};

		$scope.modalTitle = 'Thank You';
		$scope.showValidation = false;
		$scope.toggleValidation = function () {
			$scope.showValidation = !$scope.showValidation;
		};
		/************************** DELETE *******************************************/
		/*$scope.Delete = function (id) {
			if (confirm("Are you sure want to delete..!") == true) {
				Restangular.one('schemestages/' + id).remove($scope.schemestage).then(function () {
					$route.reload();
				});

			} else {

			}

		}*/
		$scope.Delete = function (id) {
			$scope.item = [{
				deleteflag: true
            }]
			Restangular.one('schemestages/' + id).customPUT($scope.item[0]).then(function () {
				$route.reload();
			});
		}

	});
