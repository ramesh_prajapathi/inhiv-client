'use strict';

angular.module('secondarySalesApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'restangular',
    'ui.select2',
    'ui.bootstrap',
    'angularFileUpload',
    'angularUtils.directives.dirPagination',
    'ngIdle',
    'toggle-switch',
    'angucomplete',
    'services.breadcrumbs',
    'ngPageTitle',
    'confirmButton',
    'confirmButton2'
  ])
    .config(function ($routeProvider, $locationProvider, RestangularProvider, $idleProvider, $keepaliveProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/main1',
                controller: 'MainCtrl',
                label: 'Home',
                data: {
                    pageTitle: 'Home'
                }
            })
            .when('/login', {
                templateUrl: 'partials/login',
                controller: 'LoginCtrl',
                label: 'Login',
                data: {
                    pageTitle: 'Login'
                }
            })
            .when('/roles', {
                templateUrl: 'partials/roles',
                controller: 'RolesCtrl',
                label: 'Roles List',
                data: {
                    pageTitle: 'Roles List'
                }
            })
            .when('/roles/create', {
                templateUrl: 'partials/roles',
                controller: 'RolesCtrl',
                label: 'Roles Create',
                data: {
                    pageTitle: 'Roles Create'
                }
            })
            .when('/roles/:id', {
                templateUrl: 'partials/roles',
                controller: 'RolesCtrl',
                label: 'Roles Edit',
                data: {
                    pageTitle: 'Roles Edit'
                }
            })
            .when('/users', {
                templateUrl: 'partials/users',
                controller: 'UsersCtrl',
                label: 'Users List',
                data: {
                    pageTitle: 'Users List'
                }
            })
            .when('/users/create', {
                templateUrl: 'partials/users-form',
                controller: 'UsersCreateCtrl',
                label: 'Users Create',
                data: {
                    pageTitle: 'Users Create'
                }
            })
            .when('/users/:id', {
                templateUrl: 'partials/users-form',
                controller: 'UsersEditCtrl',
                label: 'Users Edit',
                data: {
                    pageTitle: 'Users Edit'
                }
            })
            .when('/groups', {
                templateUrl: 'partials/groups',
                controller: 'GroupsCtrl'
            })
            .when('/groups/create', {
                templateUrl: 'partials/groups',
                controller: 'GroupsCtrl'
            })
            .when('/groups/:id', {
                templateUrl: 'partials/groups',
                controller: 'GroupsCtrl'
            })
            .when('/countries', {
                templateUrl: 'partials/countries',
                controller: 'CountriesCtrl',
                label: 'Country List',
                data: {
                    pageTitle: 'Country List'
                }
            })
            .when('/countries/create', {
                templateUrl: 'partials/countries',
                controller: 'CountriesCtrl',
                label: 'Country Create',
                data: {
                    pageTitle: 'Country Create'
                }
            })
            .when('/countries/:id', {
                templateUrl: 'partials/countries',
                controller: 'CountriesCtrl',
                label: 'Country Edit',
                data: {
                    pageTitle: 'Country Edit'
                }
            })
            .when('/states', {
                templateUrl: 'partials/states',
                controller: 'StatesCtrl'
            })
            .when('/states/create', {
                templateUrl: 'partials/states',
                controller: 'StatesCtrl'
            })
            .when('/states/:id', {
                templateUrl: 'partials/states',
                controller: 'StatesCtrl'
            })
            .when('/town', {
                templateUrl: 'partials/cities',
                controller: 'CitiesCtrl',
                label: 'Town List',
                data: {
                    pageTitle: 'Town List'
                }
            })
            .when('/town/create', {
                templateUrl: 'partials/cities',
                controller: 'CitiesCtrl',
                label: 'Town Create',
                data: {
                    pageTitle: 'Town Create'
                }
            })
            .when('/town/:id', {
                templateUrl: 'partials/cities',
                controller: 'CitiesCtrl',
                label: 'Town Edit',
                data: {
                    pageTitle: 'Town Edit'
                }
            })
            .when('/uploadtown', {
                templateUrl: 'partials/uploadcities',
                controller: 'CityReadXlsCtrl',
                label: 'TGeography',
                data: {
                    pageTitle: 'Geography'
                }
            })
            .when('/state', {
                templateUrl: 'partials/zones',
                controller: 'ZonesCtrl',
                label: 'State List',
                data: {
                    pageTitle: 'State List'
                }
            })
            .when('/state/create', {
                templateUrl: 'partials/zones',
                controller: 'ZonesCtrl',
                label: 'State List',
                data: {
                    pageTitle: 'State Create'
                }
            })
            .when('/state/:id', {
                templateUrl: 'partials/zones',
                controller: 'ZonesCtrl',
                label: 'State List',
                data: {
                    pageTitle: 'State Edit'
                }
            })
            .when('/uploadstate', {
                templateUrl: 'partials/uploadzones',
                controller: 'ZoneReadXlsCtrl',
                label: 'Geography',
                data: {
                    pageTitle: 'Geography'
                }
            })
            .when('/district', {
                templateUrl: 'partials/sales-areas',
                controller: 'SalesAreasCtrl',
                label: 'District List',
                data: {
                    pageTitle: 'District List'
                }


            })
            .when('/district/create', {
                templateUrl: 'partials/sales-areas',
                controller: 'SalesAreasCtrl',
                label: 'District Create',
                data: {
                    pageTitle: 'District Create'
                }
            })
            .when('/district/:id', {
                templateUrl: 'partials/sales-areas',
                controller: 'SalesAreasCtrl',
                label: 'District Edit',
                data: {
                    pageTitle: 'District Edit'
                }
            })
            .when('/uploaddistrict', {
                templateUrl: 'partials/uploadsales-areas',
                controller: 'DistReadXlsCtrl',
                label: 'Geography',
                data: {
                    pageTitle: 'Geography'
                }
            })
            .when('/distribution-areas', {
                templateUrl: 'partials/distribution-areas',
                controller: 'DistributionAreasCtrl'
            })
            .when('/distribution-areas/create', {
                templateUrl: 'partials/distribution-areas',
                controller: 'DistributionAreasCtrl'
            })
            .when('/distribution-areas/:id', {
                templateUrl: 'partials/distribution-areas',
                controller: 'DistributionAreasCtrl'
            })
            .when('/distribution-subareas', {
                templateUrl: 'partials/distribution-subareas',
                controller: 'DistributionSubareasCtrl'
            })
            .when('/distribution-subareas/create', {
                templateUrl: 'partials/distribution-subareas',
                controller: 'DistributionSubareasCtrl'
            })
            .when('/distribution-subareas/:id', {
                templateUrl: 'partials/distribution-subareas',
                controller: 'DistributionSubareasCtrl'
            })
            /* .when('/employees', {
                 templateUrl: 'partials/employees',
                 controller: 'EmployeesCtrl'
             })*/
            .when('/facility', {
                templateUrl: 'partials/employees1',
                controller: 'EmployeesCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/facility/create', {
                templateUrl: 'partials/employees-form',
                controller: 'EmployeesCreateCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/facility/:id', {
                templateUrl: 'partials/employees-form',
                controller: 'EmployeesEditCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            /************New Added*********/
            .when('/facilitytype', {
                templateUrl: 'partials/facilitytype',
                controller: 'FacilityTypeCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/facilitytype/create', {
                templateUrl: 'partials/facilitytype',
                controller: 'FacilityTypeCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/facilitytype/:id', {
                templateUrl: 'partials/facilitytype',
                controller: 'FacilityTypeCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })

            .when('/areaofoperation', {
                templateUrl: 'partials/areaofoperation',
                controller: 'AreaofOpeartionCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/areaofoperation/create', {
                templateUrl: 'partials/areaofoperation',
                controller: 'AreaofOpeartionCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/areaofoperation/:id', {
                templateUrl: 'partials/areaofoperation',
                controller: 'AreaofOpeartionCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/12astatus', {
                templateUrl: 'partials/12astatus',
                controller: '12AStatusCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/12astatus/create', {
                templateUrl: 'partials/12astatus',
                controller: '12AStatusCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/12astatus/:id', {
                templateUrl: 'partials/12astatus',
                controller: '12AStatusCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/tirating', {
                templateUrl: 'partials/tirating',
                controller: 'TiRatingCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/tirating/create', {
                templateUrl: 'partials/tirating',
                controller: 'TiRatingCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            .when('/tirating/:id', {
                templateUrl: 'partials/tirating',
                controller: 'TiRatingCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }
            })
            /* .when('/80gstatus', {
                 templateUrl: 'partials/80gstatus',
                 controller: '80gStatusCtrl'
             })
             .when('/80gstatus/create', {
                 templateUrl: 'partials/80gstatus',
                 controller: '80gStatusCtrl'
             })
             .when('/80gstatus/:id', {
                 templateUrl: 'partials/80gstatus',
                 controller: '80gStatusCtrl'
             })
             .when('/fcrastaus', {
                 templateUrl: 'partials/fcrastaus',
                 controller: 'FcraStausCtrl'
             })
             .when('/fcrastaus/create', {
                 templateUrl: 'partials/fcrastaus',
                 controller: 'FcraStausCtrl'
             })
             .when('/fcrastaus/:id', {
                 templateUrl: 'partials/fcrastaus',
                 controller: 'FcraStausCtrl'
             })*/
            /*****************************/
            .when('/ti', {
                templateUrl: 'partials/co-org',
                controller: 'COgrCtrl',
                label: 'TI List',
                data: {
                    pageTitle: 'TI List'
                }
            })
            .when('/ti/create', {
                templateUrl: 'partials/co-org-form',
                controller: 'COCreateCtrl',
                label: 'TI Create',
                data: {
                    pageTitle: 'TI Create'
                }
            })
            .when('/ti/:id', {
                templateUrl: 'partials/co-org-form',
                controller: 'COEditCtrl',
                label: 'TI Edit',
                data: {
                    pageTitle: 'TI Edit'
                }
            })
            .when('/ictccenter', {
                templateUrl: 'partials/ictccenter',
                controller: 'IctcCtrl',
                label: 'ICTC List',
                data: {
                    pageTitle: 'ICTC List'
                }
            })
            .when('/ictccenter/create', {
                templateUrl: 'partials/ictccenter-form',
                controller: 'IctcCreateCtrl',
                label: 'ICTC Create',
                data: {
                    pageTitle: 'ICTC Create'
                }
            })
            .when('/ictccenter/:id', {
                templateUrl: 'partials/ictccenter-form',
                controller: 'IctcEditCtrl',
                label: 'ICTC Edit',
                data: {
                    pageTitle: 'ICTC Edit'
                }
            })


            .when('/artcenter', {
                templateUrl: 'partials/artcenter',
                controller: 'artcenterCtrl',
                label: 'ART List',
                data: {
                    pageTitle: 'ART List'
                }
            })
            .when('/artcenter/create', {
                templateUrl: 'partials/artcenter-form',
                controller: 'artcenterCreateCtrl',
                label: 'ART Create',
                data: {
                    pageTitle: 'ART Create'
                }
            })
            .when('/artcenter/:id', {
                templateUrl: 'partials/artcenter-form',
                controller: 'artcenterEditCtrl',
                label: 'ART Edit',
                data: {
                    pageTitle: 'ART Edit'
                }
            })

            .when('/upload-facilitymanager', {
                templateUrl: 'partials/upload-co-org',
                controller: 'CoOrgReadXlsCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/fieldworkers', {
                templateUrl: 'partials/f-work',
                controller: 'FWCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/fieldworker/create', {
                templateUrl: 'partials/f-work-form',
                controller: 'FWCreateCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/fieldworker/:id', {
                templateUrl: 'partials/f-work-form',
                controller: 'FWEditCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            //            .when('/fieldworkers/:id', {
            //                templateUrl: 'partials/f-work-form',
            //                controller: 'FWEditCtrl',
            //                label: 'Field Work Profile Edit',
            //                data: {
            //                    pageTitle: 'Field Worker'
            //                }
            //            })
            .when('/members', {
                templateUrl: 'partials/beneficiaries',
                controller: 'BnCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/members/create', {
                templateUrl: 'partials/beneficiaries-form',
                controller: 'BnCreateCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/members/:id', {
                templateUrl: 'partials/beneficiaries-form',
                controller: 'BnEditCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })

            /* .when('/coorgedit', {
                     templateUrl: 'partials/coorgedit',
                     controller: 'COOrgEditCtrl',
                     label: 'Facility Profile',
                     data: {
                         pageTitle: 'Facility Profile'
                     }
                 })
            .when('/profileedit/:id', {
                templateUrl: 'partials/coorgedit',
                controller: 'COOrgEditCtrl',
                label: 'Facility Profile',
                data: {
                    pageTitle: 'Facility Profile'
                }

            })*/
            .when('/sites', {
                templateUrl: 'partials/routes',
                controller: 'RoutesCtrl',
                label: 'Sites',
                data: {
                    pageTitle: 'Sites'
                }

            })
            .when('/site/create', {
                templateUrl: 'partials/routes-form',
                controller: 'RoutesCreateCtrl',
                label: 'Sites  ',
                data: {
                    pageTitle: 'Sites'
                }
            })
            .when('/site/:id', {
                templateUrl: 'partials/routes-form',
                controller: 'RoutesEditCtrl',
                label: 'Sites',
                data: {
                    pageTitle: 'Sites'
                }
            })
            .when('/uploadsite', {
                templateUrl: 'partials/uploadroutes',
                controller: 'RouteReadXlsCtrl',
                label: 'Sites',
                data: {
                    pageTitle: 'Sites'
                }
            })
            .when('/managersite', {
                templateUrl: 'partials/routesviewlist',
                controller: 'RoutesViewListCtrl',
                label: 'Sites',
                data: {
                    pageTitle: 'Sites'
                }

            })
            .when('/routesview/:id', {
                templateUrl: 'partials/routesview',
                controller: 'RoutesViewCtrl',
                label: 'Site Detials'
            })

            .when('/routelinks', {
                templateUrl: 'partials/routelinks',
                controller: 'RoutelinksCtrl'
            })
            .when('/routelinks/create', {
                templateUrl: 'partials/routelinks',
                controller: 'RoutelinksCtrl'
            })
            .when('/routelinks/:id', {
                templateUrl: 'partials/routelinks',
                controller: 'RoutelinksCtrl'
            })
            .when('/route_assignment', {
                templateUrl: 'partials/route_assignmentdisplays',
                controller: 'RouteassignmentDisplayCtrl'
            })
            .when('/route_assignment/create', {
                templateUrl: 'partials/route_assignment',
                controller: 'RouteassignmentCtrl'
            })
            .when('/route_assignment/:id', {
                templateUrl: 'partials/route_assignmentupdate',
                controller: 'RouteassignmentUpdateCtrl'
            })
            .when('/maps', {
                templateUrl: 'partials/maps',
                controller: 'TestCtrl'
            })
            .when('/maps/create', {
                templateUrl: 'partials/maps',
                controller: 'TestCtrl'
            })
            .when('/maps/:id', {
                templateUrl: 'partials/maps',
                controller: 'TestCtrl'
            })
            .when('/tourtypes', {
                templateUrl: 'partials/tourtypes',
                controller: 'TourtypeCtrl'
            })
            .when('/tourtypes/create', {
                templateUrl: 'partials/tourtypes',
                controller: 'TourtypeCtrl'
            })
            .when('/tourtypes/:id', {
                templateUrl: 'partials/tourtypes',
                controller: 'TourtypeCtrl'
            })
            .when('/assignfwtosite', {
                templateUrl: 'partials/agentroutelinks',
                controller: 'AgentRoutelinksCtrl',
                label: 'Assign FW To Site',
                data: {
                    pageTitle: 'Assign FW To Site'
                }
            })
            .when('/assignfwtosite/create', {
                templateUrl: 'partials/agentroutelinks',
                controller: 'AgentRoutelinksCtrl',
                label: 'Assign FW To Site',
                data: {
                    pageTitle: 'Assign FW To Site'
                }
            })
            .when('/assignfwtosite/:id', {
                templateUrl: 'partials/agentroutelinks',
                controller: 'AgentRoutelinksCtrl',
                label: 'Assign FW To Site',
                data: {
                    pageTitle: 'Assign FW To Site'
                }
            })
            /*.when('/imageupload', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })
			    .when('/imageupload/create', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })
			    .when('/imageupload/:id', {
			        templateUrl: 'partials/imageupload',
			        controller: 'ImageUploadCtrl'
			    })*/
            .when('/socialprotection', {
                templateUrl: 'partials/sdoctypes',
                controller: 'SDocTypCtrl',
                label: 'Apply For Document',
                data: {
                    pageTitle: 'Apply For Document'
                }

            })
            .when('/socialprotection/create', {
                templateUrl: 'partials/sdoctypes',
                controller: 'SDocTypCtrl',
                label: 'Apply For Document',
                data: {
                    pageTitle: 'Apply For Document'
                }
            })
            .when('/socialprotection/:id', {
                templateUrl: 'partials/sdoctypes',
                controller: 'SDocTypCtrl',
                label: 'Apply For Document',
                data: {
                    pageTitle: 'Apply For Document'
                }
            })
            .when('/fdocumenttypes', {
                templateUrl: 'partials/fdoctypes',
                controller: 'FDocTypCtrl'
            })
            .when('/fdocumenttypes/create', {
                templateUrl: 'partials/fdoctypes',
                controller: 'FDocTypCtrl'
            })
            .when('/fdocumenttypes/:id', {
                templateUrl: 'partials/fdoctypes',
                controller: 'FDocTypCtrl'
            })

            //        .when('/schemes', {
            //                templateUrl: 'partials/scheme',
            //                controller: 'SchCtrl'
            //            })
            //            .when('/schemes/create', {
            //                templateUrl: 'partials/scheme',
            //                controller: 'SchCtrl'
            //            })
            //            .when('/schemes/:id', {
            //                templateUrl: 'partials/scheme',
            //                controller: 'SchCtrl'
            //            })
            .when('/gender', {
                templateUrl: 'partials/gender',
                controller: 'GenCtrl',
                label: 'Gender List',
                data: {
                    pageTitle: 'Gender List'
                }
            })
            .when('/gender/create', {
                templateUrl: 'partials/gender',
                controller: 'GenCtrl',
                label: 'Gender Create',
                data: {
                    pageTitle: 'Gender Create'
                }
            })
            .when('/gender/:id', {
                templateUrl: 'partials/gender',
                controller: 'GenCtrl',
                label: 'Gender Edit',
                data: {
                    pageTitle: 'Gender Edit'
                }
            })
            .when('/typology', {
                templateUrl: 'partials/typology',
                controller: 'TypCtrl',
                label: 'Typology List',
                data: {
                    pageTitle: 'Typology List'
                }
            })
            .when('/typology/create', {
                templateUrl: 'partials/typology',
                controller: 'TypCtrl',
                label: 'Typology Create',
                data: {
                    pageTitle: 'Typology Create'
                }
            })
            .when('/typology/:id', {
                templateUrl: 'partials/typology',
                controller: 'TypCtrl',
                label: 'Typology Edit',
                data: {
                    pageTitle: 'Typology Edit'
                }
            })
            .when('/education', {
                templateUrl: 'partials/education',
                controller: 'EduCtrl',
                label: 'Education List',
                data: {
                    pageTitle: 'Education List'
                }
            })
            .when('/education/create', {
                templateUrl: 'partials/education',
                controller: 'EduCtrl',
                label: 'Education Create',
                data: {
                    pageTitle: 'Education Create'
                }
            })
            .when('/education/:id', {
                templateUrl: 'partials/education',
                controller: 'EduCtrl',
                label: 'Education Edit',
                data: {
                    pageTitle: 'Education Edit'
                }
            })
            .when('/maritalstatus', {
                templateUrl: 'partials/maritalstatus',
                controller: 'MaryCtrl',
                label: 'Marital Status List',
                data: {
                    pageTitle: 'Marital Status List'
                }
            })
            .when('/maritalstatus/create', {
                templateUrl: 'partials/maritalstatus',
                controller: 'MaryCtrl',
                label: 'Marital Status Create',
                data: {
                    pageTitle: 'Marital Status Create'
                }
            })
            .when('/maritalstatus/:id', {
                templateUrl: 'partials/maritalstatus',
                controller: 'MaryCtrl',
                label: 'Marital Status Edit',
                data: {
                    pageTitle: 'Marital Status Edit'
                }
            })
            .when('/employmentstatuses', {
                templateUrl: 'partials/employmentstatus',
                controller: 'EmpstCtrl',
                label: 'Employability Status List',
                data: {
                    pageTitle: 'Employability Status List'
                }
            })
            .when('/employmentstatuses/create', {
                templateUrl: 'partials/employmentstatus',
                controller: 'EmpstCtrl',
                label: 'Employability Status Create',
                data: {
                    pageTitle: 'Employability Status Create'
                }
            })
            .when('/employmentstatuses/:id', {
                templateUrl: 'partials/employmentstatus',
                controller: 'EmpstCtrl',
                label: 'Employability Status Edit',
                data: {
                    pageTitle: 'Employability Status Edit'
                }
            })
            .when('/physicalappearances', {
                templateUrl: 'partials/physicalappearance',
                controller: 'PhyApCtrl'
            })
            .when('/physicalappearances/create', {
                templateUrl: 'partials/physicalappearance',
                controller: 'PhyApCtrl'
            })
            .when('/physicalappearances/:id', {
                templateUrl: 'partials/physicalappearance',
                controller: 'PhyApCtrl'
            })
            .when('/emotionalstates', {
                templateUrl: 'partials/emotionalstate',
                controller: 'EmotionCtrl'
            })
            .when('/emotionalstates/create', {
                templateUrl: 'partials/emotionalstate',
                controller: 'EmotionCtrl'
            })
            .when('/emotionalstates/:id', {
                templateUrl: 'partials/emotionalstate',
                controller: 'EmotionCtrl'
            })

            .when('/cocategories', {
                templateUrl: 'partials/cocategories',
                controller: 'COCategoriesCtrl'
            })
            .when('/cocategories/create', {
                templateUrl: 'partials/cocategories',
                controller: 'COCategoriesCtrl'
            })
            .when('/cocategories/:id', {
                templateUrl: 'partials/cocategories',
                controller: 'COCategoriesCtrl'
            })
            .when('/reportincident', {
                templateUrl: 'partials/reportincident',
                controller: 'ReportIncidentCtrl',
                label: 'Report Incident Create',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            .when('/reportincident/:id', {
                templateUrl: 'partials/reportincident',
                controller: 'ReportIncidentEditCtrl',
                label: 'Report Incident Edit',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            //
            //        .when('/socialprotection', {
            //                templateUrl: 'partials/socialprotection',
            //                controller: 'SocialProtectionCtrl'
            //            })
            .when('/health', {
                templateUrl: 'partials/health',
                controller: 'HealthCtrl'
            })
            .when('/outreachactivity', {
                templateUrl: 'partials/outreachactivity',
                controller: 'OutReachActivityCtrl'
            })
            .when('/codashboard', {
                templateUrl: 'partials/codashboard',
                controller: 'CODashboardCtrl',
                label: 'CO Dashboard',
                data: {
                    pageTitle: 'CO Dashboard'
                }
            })
            .when('/applicationstage', {
                templateUrl: 'partials/applicationstages',
                controller: 'applicationstageCtrl'
            })
            .when('/applicationstage/create', {
                templateUrl: 'partials/applicationstages',
                controller: 'applicationstageCtrl'
            })
            .when('/applicationstage/:id', {
                templateUrl: 'partials/applicationstages',
                controller: 'applicationstageCtrl'
            })
            .when('/schemestage', {
                templateUrl: 'partials/schemestage',
                controller: 'schemestageCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemestage/create', {
                templateUrl: 'partials/schemestage',
                controller: 'schemestageCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemestage/:id', {
                templateUrl: 'partials/schemestage',
                controller: 'schemestageCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/coactivity', {
                templateUrl: 'partials/coactivity',
                controller: 'BnCreateCtrl'
            })
            .when('/resetpassword', {
                templateUrl: 'partials/resetpassword',
                controller: 'ResetPswdCtrl',
                label: 'Reset Password'
            })
            .when('/schemes', {
                templateUrl: 'partials/schemes',
                controller: 'SchemeMastersCtrl',
                label: 'Scheme List',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemes/create', {
                templateUrl: 'partials/schemes',
                controller: 'SchemeMastersCtrl',
                label: 'Scheme Create',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemes/:id', {
                templateUrl: 'partials/schemes',
                controller: 'SchemeMastersCtrl',
                label: 'Scheme Edit',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/fwarchieve', {
                templateUrl: 'partials/fieldworkerarchieve',
                controller: 'FWArchieveCtrl',
                label: 'Archived Members',
                data: {
                    pageTitle: 'Archived Members'
                }
            })
            .when('/coarchieve', {
                templateUrl: 'partials/coarchieve',
                controller: 'COArchieveCtrl',
                label: 'Archived Member',
                data: {
                    pageTitle: 'Archived Members'
                }
            })
            .when('/mysites', {
                templateUrl: 'partials/fieldworkersites',
                controller: 'FWSitesCtrl',
                label: 'Site List',
                data: {
                    pageTitle: 'Sites'
                }
            })
            .when('/groupmeetings', {
                templateUrl: 'partials/groupmeeting',
                controller: 'GroupMeetingsCtrl',
                label: 'Group Meetings',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/groupmeeting/create', {
                templateUrl: 'partials/groupmeeting',
                controller: 'GroupMeetingsCtrl',
                label: 'Group Meetings ',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/groupmeeting/:id', {
                templateUrl: 'partials/groupmeeting',
                controller: 'EditGroupMeetingsCtrl',
                label: 'Group Meetings ',
                data: {
                    pageTitle: 'Group Meetings '
                }
            })
            .when('/events', {
                templateUrl: 'partials/events',
                controller: 'EventsCtrl',
                label: 'Event List',
                data: {
                    pageTitle: 'Events'
                }
            })
            .when('/event/create', {
                templateUrl: 'partials/events',
                controller: 'EventsCtrl',
                label: 'Events',
                data: {
                    pageTitle: 'Events'
                }
            })
            .when('/event/:id', {
                templateUrl: 'partials/events',
                controller: 'EditEventsCtrl',
                label: 'Events',
                data: {
                    pageTitle: 'Events'
                }
            })

            .when('/applyforscheme', {
                templateUrl: 'partials/applyforscheme',
                controller: 'ApplyForSchemeCtrl',
                label: 'Apply For Scheme Create',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/applyforschemes/create', {
                templateUrl: 'partials/applyforscheme',
                controller: 'ApplyForSchemeCtrl',
                label: 'Apply For Scheme Create',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/applyforschemes/:id', {
                templateUrl: 'partials/applyforscheme',
                controller: 'EditSchemeCtrl',
                label: 'Apply For Scheme Edit',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/applyfordocument', {
                templateUrl: 'partials/applyfordocument',
                controller: 'ApplyForDocumentCtrl',
                label: 'Apply For Document Create',
                data: {
                    pageTitle: 'Apply For Document'
                }
            })
            .when('/applyfordocuments/create', {
                templateUrl: 'partials/applyfordocument',
                controller: 'ApplyForDocumentCtrl',
                label: 'Apply For Document Create',
                data: {
                    pageTitle: 'Apply For Document'
                }
            })
            .when('/applyfordocuments/:id', {
                templateUrl: 'partials/applyfordocument',
                controller: 'EditDocumentCtrl',
                label: 'Apply For Document Edit',
                data: {
                    pageTitle: 'Apply For Document'
                }
            })
            .when('/onetoone/:id', {
                templateUrl: 'partials/onetoone1',
                controller: 'OneToOneCtrl',
                label: 'OnetoOne',
                data: {
                    pageTitle: 'One To One'
                }
            })
            /*  .when('/surveyquestions', {
                  templateUrl: 'partials/surveyquestions',
                  controller: 'SurveyQuestionsCtrl'
              })
              .when('/surveyquestions/create', {
                  templateUrl: 'partials/surveyquestions',
                  controller: 'SurveyQuestionsCtrl'
              })
              .when('/surveyquestions/:id', {
                  templateUrl: 'partials/surveyquestions',
                  controller: 'SurveyQuestionsCtrl'
              })*/
            .when('/pillars', {
                templateUrl: 'partials/pillars',
                controller: 'PillarsCtrl',
                label: 'Pillars',
                data: {
                    pageTitle: 'Pillars'
                }

            })
            .when('/pillars/create', {
                templateUrl: 'partials/pillars',
                controller: 'PillarsCtrl',
                label: 'Pillars',
                data: {
                    pageTitle: 'Pillars'
                }
            })
            .when('/pillars/:id', {
                templateUrl: 'partials/pillars',
                controller: 'PillarsCtrl',
                label: 'Pillars',
                data: {
                    pageTitle: 'Pillars'
                }
            })
            .when('/topics', {
                templateUrl: 'partials/groupmeetingtopic',
                controller: 'groupmeetingtopicCtrl',
                label: 'Group Meetings',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/topics/create', {
                templateUrl: 'partials/groupmeetingtopic',
                controller: 'groupmeetingtopicCtrl',
                label: 'Group Meetings',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/topics/:id', {
                templateUrl: 'partials/groupmeetingtopic',
                controller: 'groupmeetingtopicCtrl',
                label: 'Group Meetings',
                data: {
                    pageTitle: 'Group Meetings'
                }
            })
            .when('/groupmeetingevents', {
                templateUrl: 'partials/groupmeetingevent',
                controller: 'groupmeetingeventCtrl'
            })
            .when('/groupmeetingevents/create', {
                templateUrl: 'partials/groupmeetingevent',
                controller: 'groupmeetingeventCtrl'
            })
            .when('/groupmeetingevents/:id', {
                templateUrl: 'partials/groupmeetingevent',
                controller: 'groupmeetingeventCtrl'
            })
            .when('/stakeholdermeetings', {
                templateUrl: 'partials/stakeholder',
                controller: 'StakeHolderCtrl',
                label: 'Stakeholder Meetings ',
                data: {
                    pageTitle: 'Stakeholder Meetings '
                }
            })
            .when('/stakeholdermeeting/create', {
                templateUrl: 'partials/stakeholder',
                controller: 'StakeHolderCtrl',
                label: 'Stakeholder Meetings',
                data: {
                    pageTitle: 'Stakeholder Meetings '
                }
            })
            .when('/stakeholdermeeting/:id', {
                templateUrl: 'partials/stakeholder',
                controller: 'EditStakeHolderCtrl',
                label: 'Stakeholder Meetings',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/eventtype', {
                templateUrl: 'partials/eventtypes',
                controller: 'EventTypesCtrl',
                label: 'Event Type',
                data: {
                    pageTitle: 'Event Type'
                }
            })
            .when('/eventtype/create', {
                templateUrl: 'partials/eventtypes',
                controller: 'EventTypesCtrl',
                label: 'Event Type Create',
                data: {
                    pageTitle: 'Event Type Create'
                }
            })
            .when('/eventtype/:id', {
                templateUrl: 'partials/eventtypes',
                controller: 'EventTypesCtrl',
                label: 'Event Type Edit',
                data: {
                    pageTitle: 'Event Type Edit'
                }
            })
            .when('/followuptypes', {
                templateUrl: 'partials/followuptypes',
                controller: 'FollowUpTypesCtrl'
            })
            .when('/followuptypes/create', {
                templateUrl: 'partials/followuptypes',
                controller: 'FollowUpTypesCtrl'
            })
            .when('/followuptypes/:id', {
                templateUrl: 'partials/followuptypes',
                controller: 'FollowUpTypesCtrl'
            })
            .when('/bulkupdates', {
                templateUrl: 'partials/bulkupdate',
                controller: 'BulkUpCtrl',
                label: 'Bulkupdate List',
                data: {
                    pageTitle: 'Bulk Updates'
                }
            })
            .when('/bulkupdates/create', {
                templateUrl: 'partials/bulkupdate',
                controller: 'BulkUpCtrl',
                label: 'Bulkupdate Create',
                data: {
                    pageTitle: 'Bulk Updates'
                }
            })
            .when('/bulkupdates/:id', {
                templateUrl: 'partials/bulkupdate',
                controller: 'BulkUPEditCtrl',
                label: 'Bulkupdate Edit',
                data: {
                    pageTitle: 'Bulk Updates'
                }
            })
            .when('/todotypes', {
                templateUrl: 'partials/todotype',
                controller: 'ToDoTypesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todotypes/create', {
                templateUrl: 'partials/todotype',
                controller: 'ToDoTypesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todotypes/:id', {
                templateUrl: 'partials/todotype',
                controller: 'ToDoTypesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todostatus', {
                templateUrl: 'partials/todostatuses',
                controller: 'ToDoStatusesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todostatus/create', {
                templateUrl: 'partials/todostatuses',
                controller: 'ToDoStatusesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todostatus/:id', {
                templateUrl: 'partials/todostatuses',
                controller: 'ToDoStatusesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todofilters', {
                templateUrl: 'partials/mytodostatuses',
                controller: 'MyToDoStatusesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todofilters/create', {
                templateUrl: 'partials/mytodostatuses',
                controller: 'MyToDoStatusesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })
            .when('/todofilters/:id', {
                templateUrl: 'partials/mytodostatuses',
                controller: 'MyToDoStatusesCtrl',
                label: '1-1 ToDos',
                data: {
                    pageTitle: '1-1 ToDos'
                }
            })

            .when('/workflows', {
                templateUrl: 'partials/workflow',
                controller: 'WorkFlowCtrl'
            })
            .when('/workflows/create', {
                templateUrl: 'partials/workflow',
                controller: 'WorkFlowCtrl'
            })
            .when('/workflows/:id', {
                templateUrl: 'partials/workflow',
                controller: 'WorkFlowCtrl'
            })
            .when('/trackapplications', {
                templateUrl: 'partials/trackapplications',
                controller: 'TrackApplicationCtrl',
                label: 'Applications',
                data: {
                    pageTitle: 'Applications'
                }
            })
            .when('/stakeholdertype', {
                templateUrl: 'partials/stakeholdertype',
                controller: 'stakeholdertypesCtrl',
                label: 'Stakeholder Meetings',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/stakeholdertype/create', {
                templateUrl: 'partials/stakeholdertype',
                controller: 'stakeholdertypesCtrl',
                label: 'Stakeholder Meetings',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/stakeholdertype/:id', {
                templateUrl: 'partials/stakeholdertype',
                controller: 'stakeholdertypesCtrl',
                label: 'Stakeholder Meetings',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })

            /********new added*******/
            .when('/financialgoal', {
                templateUrl: 'partials/financialgoal',
                controller: 'FinancialGoalCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialgoal/create', {
                templateUrl: 'partials/financialgoal',
                controller: 'FinancialGoalCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialgoal/:id', {
                templateUrl: 'partials/financialgoal',
                controller: 'FinancialGoalCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialproduct', {
                templateUrl: 'partials/financialproduct',
                controller: 'FinancialProductCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialproduct/create', {
                templateUrl: 'partials/financialproduct',
                controller: 'FinancialProductCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialproduct/:id', {
                templateUrl: 'partials/financialproduct',
                controller: 'FinancialProductCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/chartdropdown', {
                templateUrl: 'partials/chartdropdown',
                controller: 'ChartDropdownCtrl',
                label: 'Chart Dropdown',
                data: {
                    pageTitle: 'Chart Dropdown'
                }
            })
            .when('/chartdropdown/create', {
                templateUrl: 'partials/chartdropdown',
                controller: 'ChartDropdownCtrl',
                label: 'Chart Dropdown',
                data: {
                    pageTitle: 'Chart Dropdown'
                }
            })
            .when('/chartdropdown/:id', {
                templateUrl: 'partials/chartdropdown',
                controller: 'ChartDropdownCtrl',
                label: 'Chart Dropdown',
                data: {
                    pageTitle: 'Chart Dropdown'
                }
            })
            .when('/timeinyear', {
                templateUrl: 'partials/timeinyear',
                controller: 'TimeinYearCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/timeinyear/create', {
                templateUrl: 'partials/timeinyear',
                controller: 'TimeinYearCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/timeinyear/:id', {
                templateUrl: 'partials/timeinyear',
                controller: 'TimeinYearCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/budgetyear', {
                templateUrl: 'partials/budgetyear',
                controller: 'BudgetYearCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/budgetyear/create', {
                templateUrl: 'partials/budget',
                controller: 'BudgetYearCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/budgetyear/:id', {
                templateUrl: 'partials/budgetyear',
                controller: 'BudgetYearCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })

            .when('/loantracking', {
                templateUrl: 'partials/loantracking',
                controller: 'LoanTrackingCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/loantracking/create', {
                templateUrl: 'partials/loantracking',
                controller: 'LoanTrackingCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/loantracking/:id', {
                templateUrl: 'partials/loantracking',
                controller: 'LoanTrackingCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })


            /***********************/
            .when('/stakeholder', {
                templateUrl: 'partials/followupstack',
                controller: 'followupstacksCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/stakeholder/create', {
                templateUrl: 'partials/followupstack',
                controller: 'followupstacksCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/stakeholder/:id', {
                templateUrl: 'partials/followupstack',
                controller: 'followupstacksCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/followupbulkupdates', {
                templateUrl: 'partials/followupbulkupdate',
                controller: 'followupbulkupdatesCtrl'
            })
            .when('/followupbulkupdates/create', {
                templateUrl: 'partials/followupbulkupdate',
                controller: 'followupbulkupdatesCtrl'
            })
            .when('/followupbulkupdates/:id', {
                templateUrl: 'partials/followupbulkupdate',
                controller: 'followupbulkupdatesCtrl'
            })
            .when('/purposeofmeet', {
                templateUrl: 'partials/purposeofmeeting',
                controller: 'purposeofmeetingsCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/purposeofmeet/create', {
                templateUrl: 'partials/purposeofmeeting',
                controller: 'purposeofmeetingsCtrl',
                label: 'POtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/purposeofmeet/:id', {
                templateUrl: 'partials/purposeofmeeting',
                controller: 'purposeofmeetingsCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/followupgroup', {
                templateUrl: 'partials/followupgroup',
                controller: 'followupgroupsCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/followupgroup/create', {
                templateUrl: 'partials/followupgroup',
                controller: 'followupgroupsCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/followupgroup/:id', {
                templateUrl: 'partials/followupgroup',
                controller: 'followupgroupsCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/fwdashboard', {
                templateUrl: 'partials/fwdashboard',
                controller: 'CODashboardCtrl',
                label: 'FW Dashboard',
                data: {
                    pageTitle: 'FW Dashboard'
                }
            })
            .when('/memberdatamigration', {
                templateUrl: 'partials/memberdatamigration',
                controller: 'memberdatamigrationCtrl'
            })
            .when('/uploadfacility', {
                templateUrl: 'partials/uploadfacility',
                controller: 'FacilityReadXlsCtrl',
                label: 'Facility profile',
                data: {
                    pageTitle: 'Facility profile'
                }
            })
            .when('/uploadscheme', {
                templateUrl: 'partials/uploadscheme',
                controller: 'SchemeReadXlsCtrl',
                label: 'Scheme Upload',
                data: {
                    pageTitle: 'Scheme Upload'
                }
            })
            .when('/tiprofile', {
                templateUrl: 'partials/facilityprofileedit',
                controller: 'FacilityProfileEditCtrl',
                label: 'TI Profile',
                data: {
                    pageTitle: 'TI Profile'
                }
            })
            .when('/nodashboard', {
                templateUrl: 'partials/nodashboard',
                controller: 'CODashboardCtrl',
                label: 'NO Dashboard',
                data: {
                    pageTitle: 'NO Dashboard'
                }
            })
            .when('/rodashboard', {
                templateUrl: 'partials/rodashboard',
                controller: 'CODashboardCtrl',
                label: 'RO Dashboard',
                data: {
                    pageTitle: 'RO Dashboard'
                }
            })
            .when('/spmdashboard', {
                templateUrl: 'partials/spmdashboard',
                controller: 'CODashboardCtrl',
                label: 'SPM Dashboard',
                data: {
                    pageTitle: 'SPM Dashboard'
                }
            })
            .when('/fsmentordashboard', {
                templateUrl: 'partials/fsmentordashboard',
                controller: 'CODashboardCtrl',
                label: 'FS Mentor Dashboard',
                data: {
                    pageTitle: 'FS Mentor Dashboard'
                }
            })
            //            .when('/mentor', {
            //                templateUrl: 'partials/mentor',
            //                controller: 'MentorCtrl',
            //
            //            })
            //            .when('/mentor/create', {
            //                templateUrl: 'partials/mentor-form',
            //                controller: 'MentorCreateCtrl',
            //
            //
            //            })
            //            .when('/otherroles/:id', {
            //                templateUrl: 'partials/mentor-form',
            //                controller: 'MentorCreateCtrl',
            //                label: 'Other Roles',
            //                data: {
            //                    pageTitle: 'Other Roles'
            //                }
            //            })

            .when('/mentorassign', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl',

            })
            .when('/mentorassign/create', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl'
            })
            .when('/schemedepartment', {
                templateUrl: 'partials/schemedepartments',
                controller: 'SchemeDeptCtrl'
            })
            .when('/schemedepartment/create', {
                templateUrl: 'partials/schemedepartments',
                controller: 'SchemeDeptCtrl'
            })
            .when('/schemedepartment/:id', {
                templateUrl: 'partials/schemedepartments',
                controller: 'SchemeDeptCtrl'
            })
            .when('/agegroup', {
                templateUrl: 'partials/agegroups',
                controller: 'AgeGroupCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/agegroup/create', {
                templateUrl: 'partials/agegroups',
                controller: 'AgeGroupCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/agegroup/:id', {
                templateUrl: 'partials/agegroups',
                controller: 'AgeGroupCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/healthstatus', {
                templateUrl: 'partials/healthstatuses',
                controller: 'HealthStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }

            })
            .when('/healthstatus/create', {
                templateUrl: 'partials/healthstatuses',
                controller: 'HealthStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/healthstatus/:id', {
                templateUrl: 'partials/healthstatuses',
                controller: 'HealthStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/minoritystatus', {
                templateUrl: 'partials/minoritystatuses',
                controller: 'MinorityStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }

            })
            .when('/minoritystatus/create', {
                templateUrl: 'partials/minoritystatuses',
                controller: 'MinorityStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/minoritystatus/:id', {
                templateUrl: 'partials/minoritystatuses',
                controller: 'MinorityStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/socialstatus', {
                templateUrl: 'partials/socialstatuses',
                controller: 'SocialStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/socialstatus/create', {
                templateUrl: 'partials/socialstatuses',
                controller: 'SocialStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/socialstatus/:id', {
                templateUrl: 'partials/socialstatuses',
                controller: 'SocialStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemetypology', {
                templateUrl: 'partials/occupationstatuses',
                controller: 'OccupationStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemetypology/create', {
                templateUrl: 'partials/occupationstatuses',
                controller: 'OccupationStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemetypology/:id', {
                templateUrl: 'partials/occupationstatuses',
                controller: 'OccupationStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/locationtype', {
                templateUrl: 'partials/locationtypes',
                controller: 'LocationTypeCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/locationtype/create', {
                templateUrl: 'partials/locationtypes',
                controller: 'LocationTypeCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/locationtype/:id', {
                templateUrl: 'partials/locationtypes',
                controller: 'LocationTypeCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }

            })
            .when('/incomestatus', {
                templateUrl: 'partials/incomestatuses',
                controller: 'IncomeStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/incomestatus/create', {
                templateUrl: 'partials/incomestatuses',
                controller: 'IncomeStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/incomestatus/:id', {
                templateUrl: 'partials/incomestatuses',
                controller: 'IncomeStatusCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemecategory', {
                templateUrl: 'partials/schemecategories',
                controller: 'SchemeCategCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemecategory/create', {
                templateUrl: 'partials/schemecategories',
                controller: 'SchemeCategCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/schemecategory/:id', {
                templateUrl: 'partials/schemecategories',
                controller: 'SchemeCategCtrl',
                label: 'Apply For Scheme',
                data: {
                    pageTitle: 'Apply For Scheme'
                }
            })
            .when('/otherroles', {
                templateUrl: 'partials/mentor',
                controller: 'MentorCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/otherroles/create', {
                templateUrl: 'partials/mentor-form',
                controller: 'MentorCreateCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/otherroles/:id', {
                templateUrl: 'partials/mentor-form',
                controller: 'MentorCreateCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/ictcassign', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/ictcassign/create', {
                templateUrl: 'partials/mentorassign',
                controller: 'MentorAssignCtrl',
                label: 'Field Workers',
                data: {
                    pageTitle: 'Field Workers'
                }
            })
            .when('/upload-co-organisation', {
                templateUrl: 'partials/upload-co-org',
                controller: 'CoOrgReadXlsCtrl',

            })
            .when('/followupreportincident', {
                templateUrl: 'partials/followupreportincident',
                controller: 'followupreportincidentCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/followupreportincident/create', {
                templateUrl: 'partials/followupreportincident',
                controller: 'followupreportincidentCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/followupreportincident/:id', {
                templateUrl: 'partials/followupreportincident',
                controller: 'followupreportincidentCtrl',
                label: 'OtherToDos',
                data: {
                    pageTitle: 'OtherToDos'
                }
            })
            .when('/documenttypes', {
                templateUrl: 'partials/documenttype',
                controller: 'documenttypeCtrl'
            })
            .when('/documenttypes/create', {
                templateUrl: 'partials/documenttype',
                controller: 'documenttypeCtrl'
            })
            .when('/documenttypes/:id', {
                templateUrl: 'partials/documenttype',
                controller: 'documenttypeCtrl'
            })
            .when('/reasonforrejection', {
                templateUrl: 'partials/reasonforrejection',
                controller: 'reasonforrejectionCtrl',
                abel: 'Reason For Rejection',
                data: {
                    pageTitle: 'Reason For Rejection'
                }
            })
            .when('/reasonforrejection/:id', {
                templateUrl: 'partials/reasonforrejection',
                controller: 'reasonforrejectionCtrl',
                label: 'Reason For Rejection',
                data: {
                    pageTitle: 'Reason For Rejection'
                }
            })
            .when('/reasonforrejection/create', {
                templateUrl: 'partials/reasonforrejection',
                controller: 'reasonforrejectionCtrl',
                label: 'Reason For Rejection',
                data: {
                    pageTitle: 'Reason For Rejection'
                }
            })
            .when('/reasonfordelay', {
                templateUrl: 'partials/reasonfordelay',
                controller: 'reasonfordelayCtrl',
                abel: 'Reason For Delay',
                data: {
                    pageTitle: 'Reason For Delay'
                }
            })
            .when('/reasonfordelay/:id', {
                templateUrl: 'partials/reasonfordelay',
                controller: 'reasonfordelayCtrl',
                label: 'Reason For Delay',
                data: {
                    pageTitle: 'Reason For Delay'
                }
            })
            .when('/reasonfordelay/create', {
                templateUrl: 'partials/reasonfordelay',
                controller: 'reasonfordelayCtrl',
                label: 'Reason For Delay',
                data: {
                    pageTitle: 'Reason For Delay'
                }
            })

            .when('/servityofincident', {
                templateUrl: 'partials/servityofincident',
                controller: 'servityofincidentCtrl',
                abel: 'Report Incident',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            .when('/servityofincident/:id', {
                templateUrl: 'partials/servityofincident',
                controller: 'servityofincidentCtrl',
                label: 'Report Incident ',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            .when('/servityofincident/create', {
                templateUrl: 'partials/servityofincident',
                controller: 'servityofincidentCtrl',
                label: 'Report Incident',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            .when('/currentstatus', {
                templateUrl: 'partials/currentstatusofcase',
                controller: 'currentstatusofcaseCtrl',
                abel: 'Report Incident',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            .when('/currentstatus/:id', {
                templateUrl: 'partials/currentstatusofcase',
                controller: 'currentstatusofcaseCtrl',
                label: 'Report Incident',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            .when('/currentstatus/create', {
                templateUrl: 'partials/currentstatusofcase',
                controller: 'currentstatusofcaseCtrl',
                label: 'Report Incident',
                data: {
                    pageTitle: 'Report Incident'
                }
            })
            .when('/organiseby', {
                templateUrl: 'partials/organisebystakeholder',
                controller: 'organisebystakeholderCtrl',
                abel: 'Stakeholder Meetings',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/organiseby/:id', {
                templateUrl: 'partials/organisebystakeholder',
                controller: 'organisebystakeholderCtrl',
                label: 'Stakeholder Meetings ',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('organiseby/create', {
                templateUrl: 'partials/organisebystakeholder',
                controller: 'organisebystakeholderCtrl',
                label: 'Stakeholder Meetings',
                data: {
                    pageTitle: 'Stakeholder Meetings'
                }
            })
            .when('/responsereceive', {
                templateUrl: 'partials/responsereceive',
                controller: 'responsereceiveCtrl',
                label: 'Response Receive',
                data: {
                    pageTitle: 'Response Receive'
                }
            })
            .when('/responsereceive/:id', {
                templateUrl: 'partials/responsereceive',
                controller: 'responsereceiveCtrl',
                label: 'Response Receive',
                data: {
                    pageTitle: 'Response Receive'
                }
            })
            .when('/responsereceive/create', {
                templateUrl: 'partials/responsereceive',
                controller: 'responsereceiveCtrl',
                label: 'Response Receive',
                data: {
                    pageTitle: 'Response Receive'
                }
            })
            .when('/dynamicfields', {
                templateUrl: 'partials/dynamicfields',
                controller: 'DynamicFieldsCtrl',
                label: 'Dynamic Fields',
                data: {
                    pageTitle: 'Dynamic Fields'
                }
            })
            .when('/Languages/english', {
                templateUrl: 'partials/language-english',
                controller: 'EnglishCtrl',
                label: 'Languages',
                data: {
                    pageTitle: 'Languages'
                }
            })
            .when('/Languages/hindi', {
                templateUrl: 'partials/language-hindi',
                controller: 'HindiCtrl',
                label: 'Languages',
                data: {
                    pageTitle: 'Languages'
                }
            })
            .when('/Languages/kannada', {
                templateUrl: 'partials/language-kannda',
                controller: 'kanndaCtrl',
                label: 'Languages',
                data: {
                    pageTitle: 'Languages'
                }
            })
            .when('/Languages/tamil', {
                templateUrl: 'partials/language-tamil',
                controller: 'TamilCtrl',
                label: 'Languages',
                data: {
                    pageTitle: 'Languages'
                }
            })
            .when('/Languages/telugu', {
                templateUrl: 'partials/language-telagu',
                controller: 'TelaguCtrl',
                label: 'Languages',
                data: {
                    pageTitle: 'Languages'
                }
            })
            .when('/malyalam', {
                templateUrl: 'partials/language-malyalam',
                controller: 'MalyalamCtrl',
                label: 'Languages',
                data: {
                    pageTitle: 'Languages'
                }
            })
            .when('/Languages/marathi', {
                templateUrl: 'partials/language-marathi',
                controller: 'marathiCtrl',
                label: 'Languages',
                data: {
                    pageTitle: 'Languages'
                }
            })
            .when('/onetoonequestion', {
                templateUrl: 'partials/surveyquestion-mlanguage-View',
                controller: 'SurveyQuestionsCtrl',
                label: 'One to One Question',
                data: {
                    pageTitle: 'One to One Question'
                }

            })
            .when('/onetoonequestion/create', {
                templateUrl: 'partials/surveyquestion-mlanguage',
                controller: 'SurveyQuestionsCtrl',
                label: 'One to One Question Create',
                data: {
                    pageTitle: 'One to One Question'
                }
            })
            .when('/onetoonequestion/:id', {
                templateUrl: 'partials/surveyquestion-mlanguage',
                controller: 'SurveyQuestionsCtrl',
                label: 'One to One Question',
                data: {
                    pageTitle: 'One to One Question'
                }
            })
            /******************************
        
            .when('/consentform', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
                .when('/consentform/create', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
                .when('/consentform/:id', {
                    templateUrl: 'partials/consentform',
                    controller: 'ConsentFormCtrl'
                })
            *****************************/
            .when('/syncoutdata/:id', {
                templateUrl: 'partials/syncoutdata2',
                controller: 'SyncOutDataCtrl',
                label: 'Last Pushed Data',
                data: {
                    pageTitle: 'Last Pushed Data'
                }
            })
            .when('/phonetype', {
                templateUrl: 'partials/phonetypes',
                controller: 'PhoneCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/phonetype/create', {
                templateUrl: 'partials/phonetypes',
                controller: 'PhoneCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/phonetype/:id', {
                templateUrl: 'partials/phonetypes',
                controller: 'PhoneCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/noofterm', {
                templateUrl: 'partials/noofterms',
                controller: 'TermCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/noofterm/create', {
                templateUrl: 'partials/noofterms',
                controller: 'TermCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/noofterm/:id', {
                templateUrl: 'partials/noofterms',
                controller: 'TermCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            /*********/
            .when('/archivemember', {
                templateUrl: 'partials/archivemember',
                controller: 'ArchiveCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/archivemember/create', {
                templateUrl: 'partials/archivemember',
                controller: 'ArchiveCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/archivemember/:id', {
                templateUrl: 'partials/archivemember',
                controller: 'ArchiveCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            /************/
            .when('/financialPlanning', {
                templateUrl: 'partials/financialPlaning',
                controller: 'fPlanCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialPlanning/create', {
                templateUrl: 'partials/financialPlaning',
                controller: 'fPlanCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            .when('/financialPlanning:/id', {
                templateUrl: 'partials/financialPlaning',
                controller: 'fPlanCtrl',
                label: 'Financial Planning',
                data: {
                    pageTitle: 'Financial Planning'
                }
            })
            /*.when('/cobudgets', {
                templateUrl: 'partials/cobudgetsView',
                controller: 'cobudgetsViewCtrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/cobudgets/create', {
                templateUrl: 'partials/cobudgetsView',
                controller: 'cobudgetsViewCtrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/cobudgets:/id', {
                templateUrl: 'partials/cobudgetsView',
                controller: 'cobudgetsViewCtrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })*/
            .when('/spmbudgets', {
                templateUrl: 'partials/spmbudgetsView',
                controller: 'spmbudgetsViewCtrl',
                label: 'SPM Budgets',
                data: {
                    pageTitle: 'SPM Budgets'
                }
            })
            .when('/spmbudgets/create', {
                templateUrl: 'partials/spmbudgets',
                controller: 'spmbudgetsCtrl',
                label: 'SPM Budgets',
                data: {
                    pageTitle: 'SPM Budgets'
                }
            })
            .when('/spmbudgets/:id', {
                templateUrl: 'partials/spmbudgets',
                controller: 'spmbudgetsCtrl',
                label: 'SPM Budgets',
                data: {
                    pageTitle: 'SPM Budgets'
                }
            })

            .when('/months', {
                templateUrl: 'partials/months',
                controller: 'MonthCtrl'
            })
            .when('/months/create', {
                templateUrl: 'partials/months',
                controller: 'MonthCtrl'
            })
            .when('/months/:id', {
                templateUrl: 'partials/months',
                controller: 'MonthCtrl'
            })
            .when('/cobudget', {
                templateUrl: 'partials/cobudgetsView1',
                controller: 'cobudgetsViewNew2Ctrl',
                label: 'CO Budget',
                data: {
                    pageTitle: 'CO Budget'
                }
            })
            .when('/cobudgets/create', {
                templateUrl: 'partials/cobudgetsView1',
                controller: 'cobudgetsViewNew2Ctrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })
            .when('/cobudgets:/id', {
                templateUrl: 'partials/cobudgetsView1',
                controller: 'cobudgetsViewNew2Ctrl',
                label: 'CO Budgets',
                data: {
                    pageTitle: 'CO Budgets'
                }
            })

            .when('/nobudgets', {
                templateUrl: 'partials/nobudgets',
                controller: 'nobudgetsCtrl',
                label: 'NO Budgets',
                data: {
                    pageTitle: 'NO Budgets'
                }
            })
            .when('/learning', {
                templateUrl: 'partials/learning',
                controller: 'learningCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            })

            .when('/learning/create', {
                templateUrl: 'partials/learning',
                controller: 'learningCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            })

            .when('/learning/:id', {
                templateUrl: 'partials/learning',
                controller: 'learningCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            })
            .when('/learningtype', {
                templateUrl: 'partials/learningtype',
                controller: 'learningtypeCtrl',
                label: 'Learning ',
                data: {
                    pageTitle: 'Learning'
                }
            })
            .when('/learningtype/create', {
                templateUrl: 'partials/learningtype',
                controller: 'learningtypeCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            })
            .when('/learningtype/:id', {
                templateUrl: 'partials/learningtype',
                controller: 'learningtypeCtrl',
                label: 'Learning',
                data: {
                    pageTitle: 'Learning'
                }
            }).when('/transfer', {
                templateUrl: 'partials/transfer',
                controller: 'TransferCtrl',
                label: 'Transfer',
                data: {
                    pageTitle: 'Transfer'
                }
            })
            .when('/transfer_form/:id', {
                templateUrl: 'partials/transfer_form',
                controller: 'TransferFormCtrl',
                label: 'Transfer Member Completed',
                data: {
                    pageTitle: 'Transfer Member Completed'
                }
            })
            .when('/transfer_member_data/:id', {
                templateUrl: 'partials/transfer_member_data',
                controller: 'TrasnferMemberDataCtrl',
                label: 'Transferred Member Data',
                data: {
                    pageTitle: 'Transferred Member Data'
                }
            })
            /**********NEw Added***********/

            .when('/symptoms-list', {
                templateUrl: 'partials/symptoms',
                controller: 'SymptomsCtrl',
                label: 'Symptoms List',
                data: {
                    pageTitle: 'Symptoms List'
                }
            })
            .when('/symptom/create', {
                templateUrl: 'partials/symptoms',
                controller: 'SymptomsCtrl',
                label: 'Symptom Create',
                data: {
                    pageTitle: 'Symptom Create'
                }
            })
            .when('/symptom/edit/:id', {
                templateUrl: 'partials/symptoms',
                controller: 'SymptomsCtrl',
                label: 'Symptom Edit',
                data: {
                    pageTitle: 'Symptom Edit'
                }
            })
            .when('/illness-list', {
                templateUrl: 'partials/illness',
                controller: 'IllnessCtrl',
                label: 'Illness List',
                data: {
                    pageTitle: 'Illness List'
                }
            })
            .when('/illness/create', {
                templateUrl: 'partials/illness',
                controller: 'IllnessCtrl',
                label: 'Illness Create',
                data: {
                    pageTitle: 'Illness Create'
                }
            })
            .when('/illness/edit/:id', {
                templateUrl: 'partials/illness',
                controller: 'IllnessCtrl',
                label: 'Illness Edit',
                data: {
                    pageTitle: 'Illness Edit'
                }
            })

            .when('/condition-list', {
                templateUrl: 'partials/condition',
                controller: 'ConditionCtrl',
                label: 'Condition List',
                data: {
                    pageTitle: 'Condition List'
                }
            })
            .when('/conditionlist/create', {
                templateUrl: 'partials/condition',
                controller: 'ConditionCtrl',
                label: 'Condition Create',
                data: {
                    pageTitle: 'Condition Create'
                }
            })
            .when('/conditionlist/edit/:id', {
                templateUrl: 'partials/condition',
                controller: 'ConditionCtrl',
                label: 'Condition Edit',
                data: {
                    pageTitle: 'Condition Edit'
                }
            })
            .when('/conditionstep-list', {
                templateUrl: 'partials/conditionstep',
                controller: 'ConditionStepCtrl',
                label: 'Condition Step List',
                data: {
                    pageTitle: 'Condition Step List'
                }
            })
            .when('/conditionstep/create', {
                templateUrl: 'partials/conditionstep',
                controller: 'ConditionStepCtrl',
                label: 'Condition Step Create',
                data: {
                    pageTitle: 'Condition Step Create'
                }
            })
            .when('/conditionstep/edit/:id', {
                templateUrl: 'partials/conditionstep',
                controller: 'ConditionStepCtrl',
                label: 'Condition Step Edit',
                data: {
                    pageTitle: 'Condition Step Edit'
                }
            })
            .when('/conditionstatus-list', {
                templateUrl: 'partials/conditionstatus',
                controller: 'ConditionStatusCtrl',
                label: 'Condition Status List',
                data: {
                    pageTitle: 'Condition Status List'
                }
            })
            .when('/conditionstatus/create', {
                templateUrl: 'partials/conditionstatus',
                controller: 'ConditionStatusCtrl',
                label: 'Condition Status Create',
                data: {
                    pageTitle: 'Condition Status Create'
                }
            })
            .when('/conditionstatus/edit/:id', {
                templateUrl: 'partials/conditionstatus',
                controller: 'ConditionStatusCtrl',
                label: 'Condition Status Edit',
                data: {
                    pageTitle: 'Condition Status Edit'
                }
            })
            .when('/conditionfollowup-list', {
                templateUrl: 'partials/conditionfollowup',
                controller: 'ConditionFollowupCtrl',
                label: 'Condition Followup List',
                data: {
                    pageTitle: 'Condition Followup List'
                }
            })
            .when('/conditionfollowup/create', {
                templateUrl: 'partials/conditionfollowup',
                controller: 'ConditionFollowupCtrl',
                label: 'Condition Followup Create',
                data: {
                    pageTitle: 'Condition Followup Create'
                }
            })
            .when('/conditionfollowup/edit/:id', {
                templateUrl: 'partials/conditionfollowup',
                controller: 'ConditionFollowupCtrl',
                label: 'Condition Followup Edit',
                data: {
                    pageTitle: 'Condition Followup Edit'
                }
            })
            .when('/conditionchecklist-list', {
                templateUrl: 'partials/conditionchecklist',
                controller: 'ConditionCheckListCtrl',
                label: 'Condition Checklist List',
                data: {
                    pageTitle: 'Condition Checklist List'
                }
            })
            .when('/conditionchecklist/create', {
                templateUrl: 'partials/conditionchecklist',
                controller: 'ConditionCheckListCtrl',
                label: 'Condition Checklist Create',
                data: {
                    pageTitle: 'Condition Checklist Create'
                }
            })
            .when('/conditions-list', {
                templateUrl: 'partials/conditionslist',
                controller: 'ConditionListCtrl',
                label: 'Conditions List',
                data: {
                    pageTitle: 'Conditions List'
                }
            })
            .when('/condition/create', {
                templateUrl: 'partials/condition-form',
                controller: 'ConditionCreateCtrl',
                label: 'Condition Create',
                data: {
                    pageTitle: 'Condition Create'
                }
            })
            .when('/condition/edit/:id', {
                templateUrl: 'partials/condition-form',
                controller: 'ConditionEditCtrl',
                label: 'Condition Edit',
                data: {
                    pageTitle: 'Condition Edit'
                }
            })
            .when('/conditionchecklist/edit/:id', {
                templateUrl: 'partials/conditionchecklist',
                controller: 'ConditionCheckListCtrl',
                label: 'Condition Checklist Edit',
                data: {
                    pageTitle: 'Condition Checklist Edit'
                }
            })
            .when('/reasonforcancelling-list', {
                templateUrl: 'partials/reasonforcancelling',
                controller: 'ReasonForCancellingCtrl',
                label: 'Reason For Cancelling List',
                data: {
                    pageTitle: 'Reason For Cancelling List'
                }
            })
            .when('/reasonforcancelling/create', {
                templateUrl: 'partials/reasonforcancelling',
                controller: 'ReasonForCancellingCtrl',
                label: 'Reason For Cancelling Create',
                data: {
                    pageTitle: 'Reason For Cancelling Create'
                }
            })
            .when('/reasonforcancelling/edit/:id', {
                templateUrl: 'partials/reasonforcancelling',
                controller: 'ReasonForCancellingCtrl',
                label: 'Reason For Cancelling  Edit',
                data: {
                    pageTitle: 'Reason For Cancelling  Edit'
                }
            })
            .when('/conditiondiagnosis-list', {
                templateUrl: 'partials/conditiondiagnosis',
                controller: 'ConditionDiagnosisCtrl',
                label: 'Condition Diagnosis List',
                data: {
                    pageTitle: 'Condition Diagnosis List'
                }
            })
            .when('/conditiondiagnosis/create', {
                templateUrl: 'partials/conditiondiagnosis',
                controller: 'ConditionDiagnosisCtrl',
                label: 'Condition Diagnosis Create',
                data: {
                    pageTitle: 'Condition Diagnosis Create'
                }
            })
            .when('/conditiondiagnosis/edit/:id', {
                templateUrl: 'partials/conditiondiagnosis',
                controller: 'ConditionDiagnosisCtrl',
                label: 'Condition Diagnosis Edit',
                data: {
                    pageTitle: 'Condition Diagnosis Edit'
                }
            })

            .when('/patientrecord-list', {
                templateUrl: 'partials/patientrecord-view',
                controller: 'PatientRecordListCtrl',
                label: 'Patient Record List',
                data: {
                    pageTitle: 'Patient Record List'
                }
            })
            .when('/patientrecord/create', {
                templateUrl: 'partials/patientrecord',
                controller: 'PatientRecordCreateCtrl',
                label: 'Add Patient Record',
                data: {
                    pageTitle: 'Add Patient Record'
                }
            })
            .when('/patientrecord/edit/:id', {
                templateUrl: 'partials/patientrecord',
                controller: 'PatientRecordEditCtrl',
                label: 'Edit Patient Record',
                data: {
                    pageTitle: 'Edit Patient Record'
                }
            })
            .when('/drugs-list', {
                templateUrl: 'partials/medicines',
                controller: 'MedicineCtrl',
                label: 'Drug List',
                data: {
                    pageTitle: 'Drug List'
                }
            })
            .when('/drugs/edit/:id', {
                templateUrl: 'partials/medicines',
                controller: 'MedicineCtrl',
                label: 'Drug Edit',
                data: {
                    pageTitle: 'Drug Edit'
                }
            })
            .when('/drugs/create', {
                templateUrl: 'partials/medicines',
                controller: 'MedicineCtrl',
                label: 'Drug Create',
                data: {
                    pageTitle: 'Drug Create'
                }
            })
            .when('/purposeofvisit-list', {
                templateUrl: 'partials/seenby',
                controller: 'seenByCtrl',
                label: 'Purpose List',
                data: {
                    pageTitle: 'Purpose List'
                }
            })
            .when('/purposeofvisit/edit/:id', {
                templateUrl: 'partials/seenby',
                controller: 'seenByCtrl',
                label: 'Purpose Edit',
                data: {
                    pageTitle: 'Purpose Edit'
                }
            })
            .when('/purposeofvisit/create', {
                templateUrl: 'partials/seenby',
                controller: 'seenByCtrl',
                label: 'Purpose Create',
                data: {
                    pageTitle: 'Purpose Create'
                }
            })

            .when('/patientflow-list', {
                templateUrl: 'partials/referto',
                controller: 'ReferToCtrl',
                label: 'Patient List',
                data: {
                    pageTitle: 'Patient List'
                }
            })
            .when('/patientflow/edit/:id', {
                templateUrl: 'partials/referto',
                controller: 'ReferToCtrl',
                label: 'Patient Edit',
                data: {
                    pageTitle: 'Patient Edit'
                }
            })
            .when('/patientflow/create', {
                templateUrl: 'partials/referto',
                controller: 'ReferToCtrl',
                label: 'Patient Create',
                data: {
                    pageTitle: 'Patient Create'
                }
            })
            .when('/testname-list', {
                templateUrl: 'partials/testname',
                controller: 'TestNameCtrl',
                label: 'Test List',
                data: {
                    pageTitle: 'Test List'
                }
            })
            .when('/testname/edit/:id', {
                templateUrl: 'partials/testname',
                controller: 'TestNameCtrl',
                label: 'Test Edit',
                data: {
                    pageTitle: 'Test Edit'
                }
            })
            .when('/testname/create', {
                templateUrl: 'partials/testname',
                controller: 'TestNameCtrl',
                label: 'Test Create',
                data: {
                    pageTitle: 'Test Create'
                }
            })

            .when('/vitalrecord-list', {
                templateUrl: 'partials/vitalrecord',
                controller: 'VitalRecordCtrl',
                label: 'Vital Record List',
                data: {
                    pageTitle: 'Vital Record List'
                }
            })
            .when('/vitalrecord/edit/:id', {
                templateUrl: 'partials/vitalrecord',
                controller: 'VitalRecordCtrl',
                label: 'Vital Record Edit',
                data: {
                    pageTitle: 'Vital Record Edit'
                }
            })
            .when('/vitalrecord/create', {
                templateUrl: 'partials/vitalrecord',
                controller: 'VitalRecordCtrl',
                label: 'Vital Record Create',
                data: {
                    pageTitle: 'Vital Record Create'
                }
            })
            .when('/profile', {
                templateUrl: 'partials/ti-manager',
                controller: 'TiManagerCtrl',
                label: 'Profile List',
                data: {
                    pageTitle: 'Profile List'
                }
            })
            .when('/profile/create', {
                templateUrl: 'partials/ti-manager-form',
                controller: 'TiManagerCreateCtrl',
                label: 'Profile Create',
                data: {
                    pageTitle: 'Profile Create'
                }
            })
            .when('/profile/:id', {
                templateUrl: 'partials/ti-manager-form',
                controller: 'TiManagerEditCtrl',
                label: 'Profile Edit',
                data: {
                    pageTitle: 'Profile Edit'
                }
            })
            .when('/subtypology', {
                templateUrl: 'partials/subtypology',
                controller: 'SubTypCtrl',
                label: 'Subtypology List',
                data: {
                    pageTitle: 'Subtypology List'
                }
            })
            .when('/subtypology/create', {
                templateUrl: 'partials/subtypology',
                controller: 'SubTypCtrl',
                label: 'Subtypology Create',
                data: {
                    pageTitle: 'Subtypology Create'
                }
            })
            .when('/subtypology/:id', {
                templateUrl: 'partials/subtypology',
                controller: 'SubTypCtrl',
                label: 'Subtypology Edit',
                data: {
                    pageTitle: 'Subtypology Edit'
                }
            })
            .when('/employabilitystatus', {
                templateUrl: 'partials/employabilitystatus',
                controller: 'EmployeeStatusCtrl',
                label: 'Employability Status List',
                data: {
                    pageTitle: 'Employability Status List'
                }
            })
            .when('/employabilitystatus/create', {
                templateUrl: 'partials/employabilitystatus',
                controller: 'EmployeeStatusCtrl',
                label: 'Employability Status Create',
                data: {
                    pageTitle: 'Employability Status Create'
                }
            })
            .when('/employabilitystatus/:id', {
                templateUrl: 'partials/employabilitystatus',
                controller: 'EmployeeStatusCtrl',
                label: 'Employability Status Edit',
                data: {
                    pageTitle: 'Employability Status Edit'
                }
            })
            .when('/regularpartners', {
                templateUrl: 'partials/regular',
                controller: 'RegularCtrl',
                label: 'Regular Partner List',
                data: {
                    pageTitle: 'Regular Partner List'
                }
            })
            .when('/regularpartners/create', {
                templateUrl: 'partials/regular',
                controller: 'RegularCtrl',
                label: 'Regular Partner Create',
                data: {
                    pageTitle: 'Regular Partner Create'
                }
            })
            .when('/regularpartners/:id', {
                templateUrl: 'partials/regular',
                controller: 'RegularCtrl',
                label: 'Regular Partner Edit',
                data: {
                    pageTitle: 'Regular Partner Edit'
                }
            })

            .when('/vocation', {
                templateUrl: 'partials/vocation',
                controller: 'vocationCtrl',
                label: 'Vocation List',
                data: {
                    pageTitle: 'Vocation List'
                }
            })
            .when('/vocation/create', {
                templateUrl: 'partials/vocation',
                controller: 'vocationCtrl',
                label: 'Vocation Create',
                data: {
                    pageTitle: 'Vocation Create'
                }
            })
            .when('/vocation/:id', {
                templateUrl: 'partials/vocation',
                controller: 'vocationCtrl',
                label: 'Vocation Edit',
                data: {
                    pageTitle: 'Vocation Edit'
                }
            })


            .when('/reasonformoving', {
                templateUrl: 'partials/nativestate',
                controller: 'nativestateCtrl',
                label: 'Reason for Moving List',
                data: {
                    pageTitle: 'Reason for Moving List'
                }
            })
            .when('/reasonformoving/create', {
                templateUrl: 'partials/nativestate',
                controller: 'nativestateCtrl',
                label: 'Reason for Moving Create',
                data: {
                    pageTitle: 'Reason for Moving Create'
                }
            })
            .when('/reasonformoving/:id', {
                templateUrl: 'partials/nativestate',
                controller: 'nativestateCtrl',
                label: 'Reason for Moving Edit',
                data: {
                    pageTitle: 'Reason for Moving Edit'
                }
            })


            .when('/nativedistrict', {
                templateUrl: 'partials/nativedistrict',
                controller: 'nativedistrictCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/nativedistrict/create', {
                templateUrl: 'partials/nativedistrict',
                controller: 'nativedistrictCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })
            .when('/nativedistrict/:id', {
                templateUrl: 'partials/nativedistrict',
                controller: 'nativedistrictCtrl',
                label: 'Members',
                data: {
                    pageTitle: 'Members'
                }
            })

            .when('/archiving', {
                templateUrl: 'partials/archiving',
                controller: 'archiveCtrl',
                label: 'Reason for Archiving List',
                data: {
                    pageTitle: 'Reason for Archiving List'
                }
            })
            .when('/archiving/create', {
                templateUrl: 'partials/archiving',
                controller: 'archiveCtrl',
                label: 'Reason for Archiving Create',
                data: {
                    pageTitle: 'Reason for Archiving Create'
                }
            })
            .when('/archiving/:id', {
                templateUrl: 'partials/archiving',
                controller: 'archiveCtrl',
                label: 'Reason for Archiving Edit',
                data: {
                    pageTitle: 'Reason for Archiving Edit'
                }
            })
            .when('/kitprescribed-list', {
                templateUrl: 'partials/kitprescribed',
                controller: 'kitprescribedCtrl',
                label: 'Kit List',
                data: {
                    pageTitle: 'Kit List'
                }
            })
            .when('/kitprescribed/edit/:id', {
                templateUrl: 'partials/kitprescribed',
                controller: 'kitprescribedCtrl',
                label: 'Kit Edit',
                data: {
                    pageTitle: 'Kit Edit'
                }
            })
            .when('/kitprescribed/create', {
                templateUrl: 'partials/kitprescribed',
                controller: 'kitprescribedCtrl',
                label: 'Kit Create',
                data: {
                    pageTitle: 'Kit Create'
                }
            })
            .when('/language-list', {
                templateUrl: 'partials/language',
                controller: 'languagectrl',
                label: 'Language List',
                data: {
                    pageTitle: 'Language List'
                }
            })
            .when('/language/edit/:id', {
                templateUrl: 'partials/language',
                controller: 'languagectrl',
                label: 'Language Edit',
                data: {
                    pageTitle: 'Language Edit'
                }
            })
            .when('/language/create', {
                templateUrl: 'partials/language',
                controller: 'languagectrl',
                label: 'Language Create',
                data: {
                    pageTitle: 'Language Create'
                }
            })

            .when('/LangMemberList', {
                templateUrl: 'partials/lang-member',
                controller: 'LangMemberctrl',
                label: ' Member Language List',
                data: {
                    pageTitle: ' Member Language List'
                }
            })
            .when('/LangMember/create', {
                templateUrl: 'partials/lang-member',
                controller: 'LangMemberctrl',
                label: ' Member Language Create',
                data: {
                    pageTitle: 'Member Language Create'
                }
            })

            .when('/LangMember/edit/:id', {
                templateUrl: 'partials/lang-member',
                controller: 'LangMemberctrl',
                label: 'Member Language Edit',
                data: {
                    pageTitle: ' Member Language Edit'
                }
            })

            .when('/LangOrwList', {
                templateUrl: 'partials/lang-orw',
                controller: 'LangOrwctrl',
                label: ' ORW Language List',
                data: {
                    pageTitle: ' ORW Language List'
                }
            })
            .when('/LangOrw/create', {
                templateUrl: 'partials/lang-orw',
                controller: 'LangOrwctrl',
                label: ' ORW Language Create',
                data: {
                    pageTitle: 'ORW Language Create'
                }
            })

            .when('/LangOrw/edit/:id', {
                templateUrl: 'partials/lang-orw',
                controller: 'LangOrwctrl',
                label: 'ORW Language Edit',
                data: {
                    pageTitle: ' ORW Language Edit'
                }
            })
            .when('/LangTIList', {
                templateUrl: 'partials/langtilist',
                controller: 'LangTictrl',
                label: ' TI Language List',
                data: {
                    pageTitle: ' TI Language List'
                }
            })
            .when('/LangTI/create', {
                templateUrl: 'partials/langtilist',
                controller: 'LangTictrl',
                label: ' TI Language Create',
                data: {
                    pageTitle: 'TI Language Create'
                }
            })

            .when('/LangTI/edit/:id', {
                templateUrl: 'partials/langtilist',
                controller: 'LangTictrl',
                label: 'TI Language Edit',
                data: {
                    pageTitle: ' TI Language Edit'
                }
            })
            .when('/LangOnetooneList', {
                templateUrl: 'partials/lang-onetoone',
                controller: 'LangOnetoonectrl',
                label: ' Onetoone Language List',
                data: {
                    pageTitle: ' Onetoone Language List'
                }
            })
            .when('/LangOnetoone/create', {
                templateUrl: 'partials/lang-onetoone',
                controller: 'LangOnetoonectrl',
                label: ' Onetoone Language Create',
                data: {
                    pageTitle: 'OnetooneLanguage Create'
                }
            })

            .when('/LangOnetoone/edit/:id', {
                templateUrl: 'partials/lang-onetoone',
                controller: 'LangOnetoonectrl',
                label: 'Onetoone Language Edit',
                data: {
                    pageTitle: ' Onetoone Language Edit'
                }
            })
            .when('/LangMainList', {
                templateUrl: 'partials/lang-main',
                controller: 'LangMainctrl',
                label: ' Main Language List',
                data: {
                    pageTitle: ' Main Language List'
                }
            })
            .when('/LangMain/create', {
                templateUrl: 'partials/lang-main',
                controller: 'LangMainctrl',
                label: ' Main Language Create',
                data: {
                    pageTitle: 'MainLanguage Create'
                }
            })

            .when('/LangMain/edit/:id', {
                templateUrl: 'partials/lang-main',
                controller: 'LangMainctrl',
                label: 'Main Language Edit',
                data: {
                    pageTitle: ' Main Language Edit'
                }
            })
            .when('/LangConditionList', {
                templateUrl: 'partials/langcondition',
                controller: 'LangConditionctrl',
                label: ' Condition Language List',
                data: {
                    pageTitle: ' Condition Language List'
                }
            })
            .when('/LangCondition/create', {
                templateUrl: 'partials/langcondition',
                controller: 'LangConditionctrl',
                label: ' Condition Language Create',
                data: {
                    pageTitle: 'Condition Language Create'
                }
            })

            .when('/LangCondition/edit/:id', {
                templateUrl: 'partials/langcondition',
                controller: 'LangConditionctrl',
                label: 'Condition Language Edit',
                data: {
                    pageTitle: ' Condition Language Edit'
                }
            })
            .when('/LangPRList', {
                templateUrl: 'partials/lang-pr',
                controller: 'LangPatientRecordctrl',
                label: ' PR Language List',
                data: {
                    pageTitle: ' PR Language List'
                }
            })
            .when('/LangPR/create', {
                templateUrl: 'partials/lang-pr',
                controller: 'LangPatientRecordctrl',
                label: ' PR Language Create',
                data: {
                    pageTitle: 'PR Language Create'
                }
            })

            .when('/LangPR/edit/:id', {
                templateUrl: 'partials/lang-pr',
                controller: 'LangPatientRecordctrl',
                label: 'PR Language Edit',
                data: {
                    pageTitle: ' PR Language Edit'
                }
            })
            .when('/LangTodo', {
                templateUrl: 'partials/langmy-todo',
                controller: 'LangMyTodoctrl',
                label: ' ToDo Language List',
                data: {
                    pageTitle: ' ToDo Language List'
                }
            })
            .when('/LangTodo/create', {
                templateUrl: 'partials/langmy-todo',
                controller: 'LangMyTodoctrl',
                label: ' ToDo Language Create',
                data: {
                    pageTitle: 'ToDo Language Create'
                }
            })

            .when('/LangTodo/edit/:id', {
                templateUrl: 'partials/langmy-todo',
                controller: 'LangMyTodoctrl',
                label: 'ToDo Language Edit',
                data: {
                    pageTitle: ' ToDo Language Edit'
                }
            })
            .when('/profileedit/:id', {
                templateUrl: 'partials/profileedit',
                controller: 'ProfileEditCtrl',
                label: 'User Profile',
                data: {
                    pageTitle: 'User Profile'
                }
                })



            /**********NEw Added***********/



            .otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
        RestangularProvider.setBaseUrl('//inhiv-test-api.herokuapp.com/api/v1');
        // RestangularProvider.setBaseUrl('//spis-api.herokuapp.com/api/v1');
        RestangularProvider.addElementTransformer('users', true, function (user) {
            // This will add a method called login that will do a POST to the path login
            // signature is (name, operation, path, params, headers, elementToPost)

            user.addRestangularMethod('login', 'post', 'login');

            return user;
        });
        $idleProvider.idleDuration(60 * 30);
        $idleProvider.warningDuration(5);
        $keepaliveProvider.interval(10);
    })

    .factory('AnalyticsRestangular', function (Restangular) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl('//swastianalyticstest.herokuapp.com/api/v1/');
        });
    })
    .run(function ($rootScope, $window, $location, Restangular, $idle, $modal, AnalyticsRestangular) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            //            if (current != undefined) {
            //                $window.sessionStorage.prviousLocation = current.loadedTemplateUrl;
            //                // console.log('$window.sessionStorage.prviousLocation when current != undefined',$window.sessionStorage.prviousLocation);
            //            } else {
            //                $window.sessionStorage.prviousLocation = null;
            //                //console.log('$window.sessionStorage.prviousLocation when else',$window.sessionStorage.prviousLocation);
            //            }

            if (current != undefined) {
                $window.sessionStorage.prviousLocation = current.loadedTemplateUrl;
                $window.sessionStorage.current = next.originalPath;
                $window.sessionStorage.previous = current.originalPath;

            } else {
                $window.sessionStorage.prviousLocation = null;
            }
            //console.log('$window.sessionStorage.userId', $window.sessionStorage.userId);
            // console.log('$location.path()', $location.path());
            if (!$window.sessionStorage.userId && $location.path() !== '/login') {
                $location.path('/login');
            }
            if ($location.path() == '/login') {
                $rootScope.showMe = true;
            } else {
                $rootScope.showMe = false;
            }
            Restangular.setDefaultHeaders({
                'authorization': $window.sessionStorage.accessToken
            });

            AnalyticsRestangular.setDefaultHeaders({
                'authorization': $window.sessionStorage.analyticsAccessToken
            });
        });

        $rootScope.started = false;
        $rootScope.pageSize = 25;

        function closeModals() {
            if ($rootScope.warning) {
                $rootScope.warning.close();
                $rootScope.warning = null;
            }

            if ($rootScope.timedout) {
                $rootScope.timedout.close();
                $rootScope.timedout = null;
            }
        }

        function logout() {
            // alert('alert');
            //$location.path("/login");

            //$http.post(baseUrl + '/users/logout?access_token='+$window.sessionStorage.accessToken).success(function(logout) {
            Restangular.one('users/logout?access_token=' + $window.sessionStorage.accessToken).post().then(function (logout) {
                $window.sessionStorage.userId = '';
                console.log('Logout');
            }).then(function (redirect) {
                $window.location.href = '/login';
                $window.location.reload();
                $idle.unwatch();
            });
        }

        $rootScope.$on('$idleStart', function () {
            closeModals();

            $rootScope.warning = $modal.open({
                templateUrl: 'partials/warning-dialog.html',
                windowClass: 'modal-danger'
            });
        });

        $rootScope.$on('$idleEnd', function () {
            closeModals();
        });

        $rootScope.$on('$idleTimeout', function () {
            closeModals();
            logout();
            $rootScope.timedout = $modal.open({
                templateUrl: 'partials/timedout-dialog.html',
                windowClass: 'modal-danger'
            });
        });

        $rootScope.start = function () {
            closeModals();
            $idle.watch();
            $rootScope.started = true;
        };
        if (!$window.sessionStorage.userId == '') {
            $rootScope.start();
        }

        $rootScope.stop = function () {
            closeModals();
            $idle.unwatch();
            $rootScope.started = false;

        };
        $rootScope.style = {
            "font-size": 14
        }
    })

    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    // this next if is necessary for when using ng-required on your input. 
                    // In such cases, when a letter is typed first, this parser will be called
                    // again, and the 2nd time, the value will be undefined
                    if (inputValue == undefined) return ''
                    var transformedInput = inputValue.replace(/[^0-9]/g, '');
                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    })
    .directive('numbersOnlyNegtive', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    // this next if is necessary for when using ng-required on your input. 
                    // In such cases, when a letter is typed first, this parser will be called
                    // again, and the 2nd time, the value will be undefined
                    if (inputValue == undefined) return ''
                    var transformedInput = inputValue.replace(/[^0-9 -]/g, '');
                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    })

    .controller('crumbCtrl', ['$scope', '$location', '$route', 'breadcrumbs',
  function ($scope, $location, $route, breadcrumbs) {
            $scope.location = $location;
            $scope.breadcrumbs = breadcrumbs;

}])
